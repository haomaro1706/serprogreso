<?php  

class mValidar{
    
    public function validar_login($user){
        $orm = new ORM();
        $sql = $orm	->select()
					->from("op_session","opSe")
					->join(array("opUs"=>"op_usuarios"),"opUs.id = opSe.id_user")
					->where("opSe.sesion = ?",$user);
		
		$return = $orm->exec()->numRows();
		return $return > 0 ? true : false;
    }

    public function infoLogin($user){
        $orm = new ORM();
        $sql = $orm	->select()
					->from("op_session","opSe")
					->join(array("opUs"=>"op_usuarios"),"opUs.id = opSe.id_user")
					->where("opSe.sesion = ?",$user);
        
		$return = $orm->exec()->fetchArray();
		return $return;
    }
    
    public function paises(){
        $orm = new ORM();
        $sql = $orm	->select()
                        ->from("op_paises");
        $return = $sql->exec()->fetchArray();
	return $return;
    }
	public function departamentos(){
        $orm = new ORM();
        $sql = $orm	->select()
                        ->from("tn_sx_world_city");
        $return = $sql->exec()->fetchArray();
	return $return;
    }
    
    public function dep($pais){
        $orm = new ORM();
        $sql = $orm	->select()
                        ->from("op_ciudades")
                        ->where("CountryCode=?",$pais);
        $return = $sql->exec()->fetchArray();
	return $return;
    }
    
    public function barr($ciudad){
        $orm = new ORM();
        $sql = $orm	->select()
                        ->from("op_barrios")
                        ->where("id_ciudad=?",$ciudad);
        $return = $sql->exec()->fetchArray();
	return $return;
    }
    
}// class
