<?php 
/*Variables globales para el modulo lang/*/
define('SOLICITUDES_NOMBRE','Crear solicitudes');
define('SOLICITUDES_APRO','Aprobar solicitud');
define('SOLICITUDES_RECHAZAR','Rechazar solicitud');
define('SOLICITUDES_DES','Desembolsar solicitud');
define('SOLICITUDES_LISTA','Ver solicitudes');
define('SOLICITUDES_VER','Informacion de la solicitud');
define('SOLICITUDES_VER_CUPO','Informacion de la solicitud de cupo');
define('SOLICITUDES_EDITAR','Editar solicitud');
define('SOLICITUDES_DESEMBOLSO','Ver aprobadas');
define('SOLICITUDES_RECHAZO','Ver rechazadas');
define('SOLICITUDES_DESEMBOLSOLST','Lista Desembolsos');
define('SOLICITUDES_CUPO','Solicitudes cupo');
define('SOLICITUDES_CUPO_EDITAR','Editar cupo');
define('SOLICITUDES_CUPO_RECHAZAR','Rechazar cupo');
define('SOLICITUDES_CUPO_APRO','Aprobar cupo');
define('SOLICITUDES_DOC','Documentos');
define('SOLICITUDES_PLAN','Plan de pagos');
define('SOLICITUDES_ESTUDIO','Concepto del estudio');
