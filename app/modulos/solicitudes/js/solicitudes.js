/*javascript para el modulo js/*/
$(document).ready(function(){
    $("#adj_solicitud").hide();
    $("#lcupo").hide();
    $("#lempresa").hide();
    $("#vehiculos").hide();
    
    $.post('index.php/clientes/lstClientes',{jquery:1,estado:1},function(data){
    var availableTags = data.split(',');
	$( "#lstClientes" ).autocomplete({
	    source: availableTags
	});
	$( "#lstCodeudores" ).autocomplete({
	    source: availableTags
	});
    });
    
    $("#solicitudes").validate({
     rules:{
        lstClientes: {required:true},
        /*lstCodeudores: {required:true},*/
        zona: {required:true},
        sucursal: {required:true},
        responsable: {required:true},
        cartera: {required:true},
        lineaCredito: {required:true},
        amortizacion: {required:true},
        fecha_corte: {required:true},
        plazo: {required:true,number:true},
        valor_solicitado: {required:true,number:true,min:1},
        factura_ante: {number:true,min:0},
        valorUltimo: {number:true,min:0},
        nombres: {required:true,lettersonly:true},
        fechaNaci: {required:true},
        lugarNaci: {required:true,lettersonly:true},
        dir: {required:true},
        telefono: {number:true,minlength: 7, maxlength: 9,min:0},
        celular: {required:true,number:true,minlength: 10, maxlength: 12,min:1},
        tipoRecibo: {required:true},
        empresaRecibo: {required:true},
        valorRecibo: {required:true,number:true,min:0},
        fechaPagoRecibo: {required:true},
        nombresCodeudor: {lettersonly:true},
        lugarNaciCo: {lettersonly:true},
        telefonoCo: {number:true,min:1},
        celularCo: {number:true,min:1},
        nombresr: {required:true,lettersonly:true},
        telefonor: {required:true,number:true,min:1},
        comentariosr: {lettersonly:true},
        estado: {required:true},
        nombresr1: {required:true,lettersonly:true},
        nombresr2: {required:true,lettersonly:true},
        nombresr3: {lettersonly:true},
        telefonor1: {required:true,number:true},
        telefonor2: {required:true,number:true},
        telefonor3: {number:true},
        comentariosr1: {required:true,number:true},
        comentariosr2: {required:true,number:true},
        comentariosr3: {number:true},
        adjuntos1: {accept: "jpg|jpeg|ico|bmp|pdf|tiff"},
        adjuntos2: {accept: "jpg|jpeg|ico|bmp|pdf|tiff"}
     }
  });
  
    $("#editar_solicitudes").validate({
     rules:{
        lstClientes: {required:true},
        zona: {required:true},
        sucursal: {required:true},
        lineaCredito: {required:true,number:true},
        plazo: {required:true,number:true},
        monto: {required:true},
        factura_ante: {number:true,min:0},
        nombres: {required:true,lettersonly:true},
        fechaNaci: {required:true},
        lugarNaci: {required:true,lettersonly:true},
        dir: {required:true},
        telefono: {minlength: 7, maxlength: 9,min:0},
        celular: {minlength: 10, maxlength: 12,min:0},
        tipoRecibo: {required:true},
        empresaRecibo: {required:true},
        valorRecibo: {required:true,number:true,min:0},
        fechaPagoRecibo: {required:true},
        nombresCodeudor: {lettersonly:true},
        lugarNaciCo: {lettersonly:true},
        telefonoCo: {number:true,min:1},
        celularCo: {number:true,min:1},
        nombresr: {required:true,lettersonly:true},
        telefonor: {required:true,number:true,min:1},
        comentariosr: {lettersonly:true},
        estado: {required:true},
        nombresr1: {lettersonly:true},
        nombresr2: {lettersonly:true},
        nombresr3: {lettersonly:true},
        telefonor1: {number:true},
        telefonor2: {number:true},
        telefonor3: {number:true},
        adjuntos1: {accept: "jpg|jpeg|ico|bmp|pdf|tiff"},
        adjuntos2: {accept: "jpg|jpeg|ico|bmp|pdf|tiff"}
     }
  });
    
    
    $("#monto").keyup(function(){
	id = $("#lstClientes").val().split('-');
	monto=parseInt($(this).val());
     $.post('index.php/solicitudes/valMonto',{jquery:1,id:id[0]},function(data){
	// alert(data+'-'+monto);
	 if(data<monto){
	     alert('El monto solicitado es mayor al cupo disponible del usuario');
	     $("#monto").val(0);
	 }
     });

    });
    
    zona = $("#lazona").val();
    sucur = $("#lasucur").val();
    produc = $("#elproducto").val();
    elplazo = $("#elplazo").val();
    $('#zona option[value='+zona+']').attr('selected','selected');
   
     	     $.post('index.php/solicitudes/listarCredito',{jquery:1,id:zona},function(data4){
	 		$("#lcredito").html(data4);
	 		$('#lineaCredito option[value='+produc+']').attr('selected','selected');
     		});
                $.post('index.php/solicitudes/listarCredito2',{jquery:1,id:zona},function(data4){
	 		$("#lcredito2").html(data4);
	 		$('#lineaCredito2 option[value='+produc+']').attr('selected','selected');
     		});
     	/*$.post('solicitudes.php/plazos',{jquery:1,producto:produc},function(data5){
		 $("#ver_plazos").html(data5);
		 $('#plazo option[value='+elplazo+']').attr('selected','selected');
	     });*/
     		 
   // }
    
    $("#zona").change(function(){
     		$.post('index.php/solicitudes/listarSucur',{jquery:1,id_zona:$(this).val()},function(data3){
	 		$("#verSucur").html(data3);
     		});
     		$.post('index.php/solicitudes/listarCredito',{jquery:1,id:$(this).val()},function(data4){
	 		$("#lcredito").html(data4);
     		});
    	});
    	
    $("#empresaRecibo").change(function(){
    	    	$.post('index.php/solicitudes/listarServicios2',{jquery:1,id:$(this).val()},function(datos){
	 		$("#tipo_recibo").html(datos);
	 		//$('#sucursal option[value='+sucur+']').attr('selected','selected');
     		});
    });
    
    /* $.post('index.php/solicitudes/listarTasa2',{jquery:1,producto:$(this).val()},function(data4){
	 		//$("#tasas").html(data4);
                        $("#tasas2").html(data4);
     		});*/

$("#valcupo").keyup(function(){
    cupo=$(this).val().replace("$","").replace(",","").replace(".","");
    $("#valcupo").val(toMoney(cupo));
    $("#cupo").val(cupo);
});

});
function plazos(e){

    valor = $(e).val().split('-');
    //var nombre = $("input[value='"+$(e).val()+"']").text();
    var nombre = $( "#lineaCredito option:selected" ).text();

        $.post('index.php/solicitudes/listarTasa2',{jquery:1,producto:$(e).val()},function(data4){
            //$("#tasas").html(data4);
            $("#tasas2").html(data4);
        });
        cupo = 0;
        if(parseInt(valor[1])===2){
            id = $("#lstClientes").val().split('-');
            $.post('index.php/solicitudes/listarCupo',{jquery:1,id:id[0]},function(cupo){
                $("#elcupo").val(cupo);
            });
            
            //$('#valReferencia').val('n');
            $("#referencias").hide();
            $("#adj_solicitud").show();
            $("#lcupo").show();
            $("#lempresa").hide();
            
        }else if(parseInt(valor[1])===1){
            $.post('index.php/solicitudes/listarEmpresa',{jquery:1},function(data4){
                $("#lempresa").show();
                 $("#lcupo").hide();
                $("#lempresa").html(data4);
            });
        }else{
            $("#referencias").show();
            $("#lcupo").hide();
            //$('#valReferencia').val('s');
        }
        
        if( (nombre.indexOf('vehi') > 0) || (nombre.indexOf('moto') > 0) || (nombre.indexOf('carro') > 0)){
            $("#vehiculos").show();
            $("#valVehiculo").val("s");
        }else{
            $("#vehiculos").hide();
            $("#valVehiculo").val("n");
        }
        
}

function cuotas(e){
    $(function(){
        k = parseInt($("#valor_solicitado").val());
        tasa = parseFloat($("#latasa").val());
        tasa = tasa/100;
        per = parseInt($(e).val());
       
        seguroVida = parseFloat($("#lstTasas2").val());
        seguroVida = seguroVida/100;
       
        num = k * tasa;
        den = 1 - Math.pow((1+tasa),-per);
        cuota = num/den;
        seguroVida = k*seguroVida;
        
        if(isNaN(cuota)) cuota =0;
        
        $("#cuota").val(toMoney(cuota.toFixed(0)));
        $("#seguro").val(toMoney(seguroVida.toFixed(0)));
    });
}

function rechazar(){
    s=prompt('Por que se rechaza','');
    $("#rechaza").val(s)
     }