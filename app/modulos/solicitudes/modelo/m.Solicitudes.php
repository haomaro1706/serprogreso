<?php 
class mSolicitudes{
  
  public function crearSolicitud(
                                $sucursal,
                                $lineaCredito,
                                $responsable,
                                $cartera,
                                $cliente,
                                $codeudor,
                                $valor_solicitado,
                                $amortizacion,
                                $fecha_corte,
                                $plazo,
                                $desc,
                                $observaciones
                                ){
      //$orm='';
    $orm = new ORM();
	  $orm	-> insert("op_solicitudes")
				  -> values(
                                                array(
                                                0,
                                                $sucursal,
                                                $lineaCredito,
                                                $responsable,
                                                $cartera,
                                                $cliente,
                                                $codeudor,
                                                $valor_solicitado,
                                                $amortizacion,
                                                $fecha_corte,
                                                $plazo,
                                                '0',
                                                $desc,
                                                null,
                                                null,
                                                null,
                                                $observaciones,
                                                null,
                                                opDate(),
"0000-00-00"
                                                )
				   );
	  //die($orm->verSql());
	  $return = $orm-> exec()-> afectedRows();
	  return $return > 0 ? true : false;
  }
  
  public function crearLibranza(
                                $sol,
                                $empresa
                                ){
    $orm = new ORM();
	  $orm	-> insert("op_solicitudes_libranza")
				  -> values(
                                                array(
                                                0,
                                                $sol,
                                                $empresa,
                                                '0'
                                                )
				   );
	  //die($orm->verSql());
	  $return = $orm-> exec()-> afectedRows();
	  return $return > 0 ? true : false;
  }
  
  public function ultimo($tabla){
	  $orm = new ORM();
	  $sql = $orm-> select(array("max(id) As id"))-> from($tabla);
	 // die($sql->verSql());
	  $return = $sql	-> exec()
				-> fetchArray();
	  return $return[0];
  } // ultimo
  
  public function borrar($id,$tabla){
    $orm = new ORM();
	  $sql = $orm -> delete()
				  -> from($tabla)
				  -> where("id = ? ", $id);
	  $sql -> exec();
	  $retorno = $sql -> afectedRows() > 0 ? true : false;
    return $retorno;
  }
  
  public function byId($id,$tabla){
	  $orm = new ORM();
	  $sql = $orm	-> select()
				  -> from($tabla)
				  -> where('id =?',$id);
	  $return = $orm	-> exec()
					  -> fetchArray();
	  return $return;
  } // listar
  
    public function bycampo($campo,$id,$tabla){
	  $orm = new ORM();
	  $sql = $orm	-> select()
				  -> from($tabla)
				  -> where($campo.' =?',$id);
	  $return = $orm	-> exec()
					  -> fetchArray();
	  return $return;
  } // listar
  
   public function crearInformacion(
					    $id_solicitud,
					    $fecha,
					    $lugar
					  ){
    $orm = new ORM();
	  $sql = $orm	-> insert("op_solicitudes_informacion")
				  -> values(
						array(
							0,
							$id_solicitud,
							$fecha,
							$lugar
							)
				   );
	  $return = $sql	-> exec() -> afectedRows();
	  return $return > 0 ? true : false;
  }//crearInformacion
  
  public function crearServicios(
					    $id_solicitud,
					    $tipo,
					    $empresa,
					    $valor,
					    $fecha
					  ){
    $orm = new ORM();
	  $sql = $orm	-> insert("op_solicitudes_servicios")
				  -> values(
						array(
							0,
							$id_solicitud,
							$tipo,
							$empresa,
							$valor,
							$fecha
							)
				   );
	  $return = $sql	-> exec() -> afectedRows();
	  return $return > 0 ? true : false;
  }//crearServicios
  
  public function crearRefencias(
                            $referencia,
                            $vinculo,
                            $nombrer,
                            $apellidor,
                            $ccr,
                            $correor,
                            $fechar,
                            $dirr,
                            $telr,
                            $celr,
                            $empresar,
                            $cargor,
                            $id_solicitud
					  ){
    $orm = new ORM();
	  $sql = $orm	-> insert("op_solicitudes_referencias")
				  -> values(
                                            array(
                                                    0,
                                                    $referencia,
                                                    $vinculo,
                                                    $nombrer,
                                                    $apellidor,
                                                    $ccr,
                                                    $correor,
                                                    $fechar,
                                                    $dirr,
                                                    $telr,
                                                    $celr,
                                                    $empresar,
                                                    $cargor,
                                                    $id_solicitud
                                                )
				   );
	  $return = $sql	-> exec() -> afectedRows();
	  return $return > 0 ? true : false;
  }
  
  public function crearVehiculo(
                            $placa,
                            $marca,
                            $lineaV,
                            $cilindraje,
                            $modelo,
                            $precioV,
                            $estadoV,
                            $nombreV,
                            $numeroV,
                            $concesionario,
                            $telV,
                            $id_sol
                                ){
    $orm = new ORM();
	  $sql = $orm-> insert("op_solicitudes_vehiculo")
                    -> values(
                              array(
                                  0,
                                  $placa,
                                  $marca,
                                  $lineaV,
                                  $cilindraje,
                                  $modelo,
                                  $precioV,
                                  $estadoV,
                                  $nombreV,
                                  $numeroV,
                                  $concesionario,
                                  $telV,
                                  $id_sol
                                  )
                        );
	  $return = $sql-> exec()-> afectedRows();
	  return $return > 0 ? true : false;
  }
  
    public function crearRefencias2(
                            $referencia,
                            $vinculo,
                            $nombrer,
                            $apellidor,
                            $ccr,
                            $correor,
                            $fechar,
                            $dirr,
                            $telr,
                            $celr,
                            $empresar,
                            $cargor,
                            $id_solicitud
					  ){
    $orm = new ORM();
	  $sql = $orm	-> insert("op_solicitudes_cupo_referencias")
				  -> values(
                                            array(
                                                    0,
                                                    $referencia,
                                                    $vinculo,
                                                    $nombrer,
                                                    $apellidor,
                                                    $ccr,
                                                    $correor,
                                                    $fechar,
                                                    $dirr,
                                                    $telr,
                                                    $celr,
                                                    $empresar,
                                                    $cargor,
                                                    $id_solicitud
                                                )
				   );
	  $return = $sql	-> exec() -> afectedRows();
	  return $return > 0 ? true : false;
  }
  
  public function all($estado=1){
    	$orm = new ORM();
        $orm ->select(array("sol.*","cli.nombre","pro.producto as elpro"))
			 -> from("op_solicitudes","sol")
			 -> join(array("pro"=>"op_productos"),'sol.producto=pro.id')
                        -> join(array("cli"=>"vista_clientes"),'sol.cliente=cli.id');/*
                        -> join(array("cli"=>"op_clientes"),'sol.cliente=cli.id OR sol.cliente=emp.nit')
			 -> join(array("ref"=>"op_solicitudes_referencias"),"sol.id=ref.id_solicitud")*/
                         if($estado == 1){
                             $orm->where('sol.estado<=?',$estado);
                         }else{
                            $orm->where('sol.estado=?',$estado);
                         }
                        //->groupBy(array('sol.cliente'));
        //die($orm->verSql());
		$return=$orm->exec()->fetchArray(); 
		
	    return $return;
    } // all	
    
      public function all2($estado=0){
    	$orm = new ORM();
		$query=$orm->select(array("sol.id as idSol","sol.*","des.id as idDes","des.total","des.total","des.fecha_desembolso","des.fecha_pago as fPago","inf.*","serv.*","ref.*","cli.nombre","cli.apellido","pro.producto As elpro"))
				   ->from("op_solicitudes","sol")
				   ->join(array("des"=>"op_solicitudes_desembolsos"),'sol.id = des.id_solicitud',"INNER")
				   ->join(array("inf"=>"op_solicitudes_informacion"),'sol.id = inf.id_solicitud',"LEFT")
				   ->join(array("serv"=>"op_solicitudes_servicios"),'sol.id = serv.id_solicitud',"LEFT")
				   ->join(array("ref"=>"op_solicitudes_referencias"),'sol.id = ref.id_solicitud',"LEFT")
				   ->join(array("cli"=>"op_clientes"),'sol.cliente = cli.id',"INNER")
				   ->join(array("pro"=>"op_productos"),'sol.producto = pro.id',"INNER")
				   ->where('sol.estado=?',$estado);
		//die ($query->versql());	  
		$return=$query->exec()->fetchArray(); 
		
	    return $return;
    } // all	
    
    public function allId($id){
        //no cargar en juridica
    	$orm = new ORM();
        $query=$orm->select(
                array(
                    "usu1.nombre as nombreRes",
                    "usu2.nombre as nombreCar",
                    "cli.id as cliente",
                    "cli.nombre as nombre",
                    "sol.monto",
                    "sol.motivo",
                    "sol.amortizacion",
                    "sol.observaciones2",
                    "sol.fecha_corte",
                    "sol.fecha",
                    "pro.id As producto",
                    "pro.producto As nproducto",
                    "pro.logica",
                    "sol.plazo",
                    "cli2.nombre as nombreCod",
                    //"cli2.apellido as apellidoCod",
                    "sol.id As elid",
                    "cupo.cupo_disponible",
                    "ref.*"
                    )
                )
			 -> from("op_solicitudes","sol")
			 -> join(array("suc"=>"op_sucursales"),'sol.id_sucursal=suc.id')
			 -> join(array("usu1"=>"vista_clientes"),'sol.responsable=usu1.id')
			 -> join(array("usu2"=>"op_usuarios"),'sol.cartera=usu2.id')
			 -> join(array("cli"=>"vista_clientes"),'sol.cliente=cli.id')
			 -> join(array("cli2"=>"vista_clientes"),'sol.codeudor=cli2.id')
			 -> join(array("cupo"=>"op_clientes_cupo"),'sol.cliente=cupo.cliente')
			 -> join(array("pro"=>"op_productos"),'substring_index(sol.producto,"-",1)=pro.id')
			 -> join(array("zon"=>"op_zonas"),'suc.id_zona=zon.id')
			 -> join(array("ref"=>"op_solicitudes_referencias"),"sol.id=ref.id_solicitud");
			 $query-> where('sol.id=?',$id);
       // die($query->verSql());
		$return=$query->exec()->fetchArray(); 
		
	    return $return;
    } // allId
    
    public function allIdSincupo($id){
    	$orm = new ORM();
        $query=$orm->select(
                array(
                    "usu1.nombre as nombreRes",
                    "usu2.nombre as nombreCar",
                    "cli.id as cliente",
                    "cli.nombre as nombre",
                    "sol.monto",
                    "sol.motivo",
                    "sol.amortizacion",
                    "sol.observaciones2",
                    "sol.fecha_corte",
                    "sol.fecha",
                    "sol.tasaM",
                    "pro.id As producto",
                    "pro.producto As nproducto",
                    "pro.logica",
                    "sol.plazo",
                    "cli2.id as codeudor",
                    "cli2.nombre as nombreCod",
                    //"cli2.apellido as apellidoCod",
                    "sol.id As elid",
                    "ref.*"
                    )
                )
			 -> from("op_solicitudes","sol")
			 -> join(array("suc"=>"op_sucursales"),'sol.id_sucursal=suc.id')
			 -> join(array("usu1"=>"op_usuarios"),'sol.responsable=usu1.id')
			 -> join(array("usu2"=>"op_usuarios"),'sol.cartera=usu2.id')
			 -> join(array("cli"=>"vista_clientes"),'sol.cliente=cli.id')
			 -> join(array("cli2"=>"vista_clientes"),'sol.codeudor=cli2.id')
			 -> join(array("pro"=>"op_productos"),'sol.producto=pro.id')
			 -> join(array("zon"=>"op_zonas"),'suc.id_zona=zon.id')
			 -> join(array("ref"=>"op_solicitudes_referencias"),"sol.id=ref.id_solicitud");
			 $query-> where('sol.id=?',$id);
    //    die($query->verSql());
		$return=$query->exec()->fetchArray(); 
		
	    return $return;
    } // allId
    
    public function solCupos($id){
    	$orm = new ORM();
        $query=$orm->select(
                array(
                    "sol.*",
                    "ref.*"
                    )
                )
                -> from("op_solicitudes_cupo","sol")
                -> join(array("ref"=>"op_solicitudes_cupo_referencias"),'sol.id=ref.id_solicitud_cupo');
                $query-> where('sol.id=?',$id);
                //die($query->getSql());
		$return=$query->exec()->fetchArray(); 
		
	    return $return;
    } // allId
    
        public function allId2($id){
    	$orm = new ORM();
        $query=$orm->select()
			 -> from("op_solicitudes","sol")
			 -> join(array("suc"=>"op_sucursales"),'sol.id_sucursal=suc.id')
			 -> join(array("usu1"=>"op_usuarios"),'sol.responsable=usu1.id')
			 -> join(array("cli"=>"op_clientes"),'sol.cliente=cli.id')
			 -> join(array("pro"=>"op_productos"),'sol.producto=pro.id')
			 //-> join(array("zon"=>"op_zonas"),'pro.zona=zon.id')
			 -> where('sol.id=?',$id);
      //  die($query->verSql());
		$return=$query->exec()->fetchArray(); 
		
	    return $return;
    } // allId2
    
    public function aprobar($id,$estado){
		$orm = new ORM();
		$query = $orm	-> update("op_solicitudes")
						-> set (array(
								  "estado"=>$estado
								  )
							  )
						-> where("id=?",$id);
		$return = $query->exec() -> afectedRows();
		
		return $return;				  
	}
	
	public function lstTasas($producto){
	  $orm = new ORM();
		  $orm->select(array("par.*","pro.producto"))
				->from("op_parametros_tasas","par")
				->join(array("pro"=>"op_productos"),'par.id_producto = pro.id',"INNER")
				->where('pro.id=?',$producto);
                  //die($orm->verSql());
		  $return=$orm->exec()->fetchArray(); 
		return $return;
	} // lstTasas	
	
	public function lstAdministrativos($producto){
		$orm = new ORM();
		$orm->select(array("admin.*","pro.producto","zon.zona"))
				   ->from("op_parametros_administrativos","admin")
				   ->join(array("pro"=>"op_productos"),'admin.id_producto = pro.id',"INNER")
				   ->join(array("zon"=>"op_zonas"),'admin.id_zona = zon.id',"INNER")
				   ->where('pro.id=?',$producto)
				   ->where('admin.id_zona',$producto);
		$return=$orm->exec()->fetchArray(); 
	    return $return;
	}
	
	public function crearDesembolso(
					    $k,
					    $mora,
					    $total,
					    $amortizacion,
					    $fecha_corte,
					    $fecha_desembolso,
					    $tasa,
					    $tasa_ea,
					    $tasa_nominal,
					    $tasa_mensual,
					    $iva,
					    $plazo,
                                            $id,
                                            $otros,
                                            $otros_iva,
                                            $otros_des
					  ){
    	  $orm = new ORM();
	  $sql = $orm	-> insert("op_solicitudes_desembolsos")
				  -> values(
                                            array(
                                                0,
                                                $k,
                                                $mora,
                                                $total,
                                                $amortizacion,
                                                $fecha_corte,
                                                $fecha_desembolso,
                                                $tasa,
                                                $tasa_ea,
                                                $tasa_nominal,
                                                $tasa_mensual,
                                                $iva,
                                                $plazo,
                                                $id,
                                                $otros,
                                                $otros_iva,
                                                $otros_des,
                                                date("H:i:s")
                                            )
				   );
				   //die($sql->versql());
	  $return = $sql	-> exec() -> afectedRows();
	  /*
	  $orm = new ORM();
	  $sql = $orm	-> insert("op_solicitudes_desembolsos_backup")
				  -> values(
						array(
							0,
							$k,
							$ic,
							$admin1,
							$admin2,
							$recaudo,
							$mora,
							$cobranza,
							$total,
							$fecha_pago,
							$fecha_desembolso,
							$tasa,
							$tasa_ea,
						    $tasa_nominal,
						    $tasa_mensual,
						    $iva,
						    $plazo,
							$solicitud
							)
				   );
				   //die($sql->versql());
	  $return = $sql	-> exec() -> afectedRows();*/
	  return $return > 0 ? true : false;
  }//crearServicios
  
   public function desembolsos($id = ''){
	$orm = new ORM();
	
        $query=$orm->select(
                array(
                    "sol.*",
                    "cli.nombre",
                    //"cli.apellido",
                    "pro.producto As elpro",
                    "des.*"
                    )
                )
                ->from("op_solicitudes","sol")
                //->join(array("inf"=>"op_solicitudes_informacion"),'sol.id = inf.id_solicitud',"INNER")
                //->join(array("cli"=>"op_clientes"),'sol.cliente = cli.id',"INNER")
                ->join(array("cli"=>"vista_clientes"),'sol.cliente = cli.id',"INNER")
                ->join(array("pro"=>"op_productos"),'sol.producto = pro.id',"INNER")
                ->join(array("des"=>"op_solicitudes_desembolsos"),'sol.id = des.id_solicitud',"INNER");
	//die($query->versql());
	$return=$query->exec()->fetchArray(); 
		
	    return $return;
    } // all
    
    public function desembolsos2($id = ''){
    $orm = new ORM();
    $query=$orm->select(array("sol.*","inf.*","cli.nombre","cli.apellido","cli.direccion","cli.telefono","cli.celular","pro.producto As elpro","des.*","des.id As id_des","sucu.*"))
				   ->from("op_solicitudes","sol")
				   ->join(array("inf"=>"op_solicitudes_informacion"),'sol.id = inf.id_solicitud',"INNER")
				   ->join(array("cli"=>"op_clientes"),'sol.cliente = cli.id',"INNER")
				   ->join(array("pro"=>"op_productos"),'sol.producto = pro.id',"INNER")
				   ->join(array("des"=>"op_solicitudes_desembolsos"),'sol.id = des.id_solicitud',"INNER")
				   ->join(array("sucu"=>"op_sucursales"),'sol.id_sucursal = sucu.id',"INNER")
				   ->where('des.id_solicitud=?',$id);
    //die($query->verSql());
    $return=$orm->exec()->fetchArray(); 
		
	    return $return;
  }
  
    public function updSolicitud(
					    $id,		
						 $sucursal,
                                $lineaCredito,
                                $responsable,
                                $cartera,
                                $cliente,
                                $codeudor,
                                $valor_solicitado,
                                $amortizacion,
                                $fecha_corte,
                                $plazo,
                                $latasa
					   ){
		$orm = new ORM();
		$query = $orm	-> update("op_solicitudes")
						-> set (array(
								  "id_sucursal"=>$sucursal,
								  "producto"=>$lineaCredito,
								  /*"responsable"=>$responsable,
								  "cartera"=>$cartera,
								  "cliente"=>$cliente,
								  "codeudor"=>$codeudor,*/
								  "monto"=>$valor_solicitado,
								  "amortizacion"=>$amortizacion,
								  "fecha_corte"=>$fecha_corte,
								  "plazo"=>$plazo,
								  "tasaM"=>$latasa
								  )
							  )
						-> where("id=?",$id);
                //die($query->getSql());
		$return = $query->exec();
		
		return $return;		
    }
    
    public function updInformacion(
					    $id,
					    $id_solicitud,
					    $fecha,
					    $lugar
					   ){
		$orm = new ORM();
		$query = $orm	-> update("op_solicitudes_informacion")
						-> set (array(
								  "id_solicitud"=>$id_solicitud,
								  "fecha_nacimiento"=>$fecha,
								  "lugar"=>$lugar
								  )
							  )
						-> where("id=?",$id);
		$return = $query->exec();
		
		return $return;		
    }
    
    
    public function updServicios(
					    $id,				    
					    $id_solicitud,
					    $tipo,
					    $empresa,
					    $valor,
					    $fecha
					   ){
		$orm = new ORM();
		$query = $orm	-> update("op_solicitudes_servicios")
						-> set (array(
								  "id_solicitud"=>$id_solicitud,
								  "tipo_recibo"=>$tipo,
								  "empresa"=>$empresa,
								  "valor"=>$valor,
								  "fecha"=>$fecha
								  )
							  )
						-> where("id=?",$id);
		$return = $query->exec()->afectedRows();
		
		return $return;		
    }
    
    public function updRefencias(
					    $id,
					     $referencia,
                            $vinculo,
                            $nombrer,
                            $apellidor,
                            $ccr,
                            $correor,
                            $fechar,
                            $dirr,
                            $telr,
                            $celr,
                            $empresar,
                            $cargor
					   ){
		$orm = new ORM();
		$query = $orm	-> update("op_solicitudes_referencias")
						-> set (array(
								  "referencia"=>$referencia,
								  "vinculo"=>$vinculo,
								  "nombrer"=>$nombrer,
								  "apellidor"=>$apellidor,
								  "ccr"=>$ccr,
								  "correor"=>$correor,
								  "fechar"=>$fechar,
								  "dirr"=>$dirr,
								  "telr"=>$telr,
								  "celr"=>$celr,
								  "empresar"=>$empresar,
								  "cargor"=>$cargor
								  )
							  )
						-> where("id_solicitud=?",$id);
		$return = $query->exec();
		
		return $return;		
    }
    
    public function lstPlazos($producto){
    	$orm = new ORM();
		$orm->select(array("pla.*","pro.producto"))
				   ->from("op_parametros_plazos","pla")
				   ->join(array("pro"=>"op_productos"),'pla.id_producto = pro.id',"INNER")
				   ->where('pla.id_producto=?',$producto);
	    $return=$orm->exec()->fetchArray(); 
	    return $return;
    } // listar Plazos	
    
    public function infoDesembolso($id){
		$orm =  new ORM();
		$orm    ->select(array("des.*","sol.tasaM"))
                        ->from("op_solicitudes_desembolsos","des")
                        ->join(array("sol"=>"op_solicitudes"),"des.id_solicitud=sol.id")
                        ->where('sol.id = ?', $id);
                //die($orm->getSql());
	    $return=$orm->exec()->fetchArray(); 
	    return $return;
	}
        
    public function AllDesembolso(){
            $orm =  new ORM();
            $orm    ->select(array("sol.monto","sol.tasaM","des.*"))
                    ->from("op_solicitudes","sol")
                    ->join(array("des"=>"op_solicitudes_desembolsos"),"sol.id=des.id_solicitud")
                    ->where('sol.estado = ?',4);
        $return=$orm->exec()->fetchArray(); 
        return $return;
    }
    
    public function diario($id){
        $orm =  new ORM();
        $orm    ->select()
                ->from("op_solicitudes_diaria")
                ->where('id_sol = ?',$id);
        //die($orm->getSql());
        $return=$orm->exec()->fetchArray(); 
        return $return;
    }
        
    public function saveDiario(
                                $sol,
                                $per,
                                $fecha_corte,
                                $monto,
                                $interes_dia,
                                $interes,
                                $otros2,
                                $mora,
                                $fecha_hoy,
                                $cuota,
                                $dias_c            
                                ){
		$orm = new ORM();
		$sql = $orm -> insert("op_solicitudes_diaria")
                            -> values(
                                    array(
                                        0,
                                        $sol,
                                        $per,
                                        $fecha_corte,
                                        $monto,
                                        $interes_dia,
                                        $interes,
                                        $otros2,
                                        $mora,
                                        $fecha_hoy,
                                        $cuota,
                                        $dias_c
                                        )
                                );
		//die($sql->getSql());
		return $sql -> exec() -> afectedRows();
                
	} // newAcuerdo
        
            public function updDiario(
                                $sol,
                                $fecha_corte,
                                $monto,
                                $interes_dia,
                                $interes,
                                $otros2,
                                $mora,
                                $fecha_hoy,
                                $cuota
                                ){
		$orm = new ORM();
		$sql = $orm ->update("op_solicitudes_diaria")
                            ->set(
                                    array(
                                        "fecha_corte"   =>  $fecha_corte,
                                        "saldo_capital" =>  $monto,
                                        "ajuste_interes"=>  $interes_dia,
                                        "interes"       =>  $interes,
                                        "otros"         =>  $otros2,
                                        "mora"          =>  $mora,
                                        "fecha_crom"    =>  $fecha_hoy,
                                        "cuota"         =>  $cuota
                                        )
                                )
                        ->where("id_sol = ?", $sol);
                
		//die($sql->getSql());
                
		return $sql -> exec() -> afectedRows();
                
	} // newAcuerdo
    
    public function infoDesembolsoDesc($id){
        $orm =  new ORM();
        $orm    ->select(array("sol.*","des.*","cli.id As cc","cli.nombre","cli.correo","cli.dir"))
                ->from("op_solicitudes","sol")
                ->join(array("des"=>"op_solicitudes_desembolsos"),'sol.id = des.id_solicitud')
                ->join(array("cli"=>"vista_clientes"),'sol.cliente = cli.id')
                ->where('sol.id = ?', $id);
        //die($orm->getSql());
        $return=$orm->exec()->fetchArray(); 
        return $return;
    }
	
    public function infoCuotas($id){
        $orm = new ORM();
        $orm    ->select(array("cuotas.*","des.capital As elcapital"))
                ->from("op_solicitudes_cuotas2","cuotas")
                ->join(array('des'=>'op_solicitudes_desembolsos'),'cuotas.id_desembolso=des.id_solicitud')
                ->where('cuotas.id_desembolso = ?', $id);
                //die($orm->getSql());
	    $return=$orm->exec()->fetchArray(); 
	    return $return;
    }

        public function infoCuotas2($id){
		$orm = new ORM();
		$orm    ->select(array("cuotas.*","des.capital As elcapital"))
                        ->from("op_solicitudes_cuotas","cuotas")
                        ->join(array('des'=>'op_solicitudes_desembolsos'),'cuotas.id_desembolso=des.id_solicitud')
                        ->where('month(cuotas.fechaCorte) = ?',10)
                        ->where('year(cuotas.fechaCorte) <= ?',date('Y'))
                        ->where('cuotas.id_desembolso = ?', $id);
                       //->where('cuotas.estado = ?','1');
                //die($orm->getSql());
	    $return=$orm->exec()->fetchArray(); 
	    return $return;
	}

        
    public function infoPorDesembolso($id){
		$orm = new ORM();
		$query=$orm->select(array("sol.*","inf.*","cli.*","pro.producto As elpro","des.*","des.id as idDes","SUM(acu.abono) as abono","des.recaudo as recDes","adm.*","adm.recaudo as recPro","tas.*","cob.*"))
				   ->from("op_solicitudes","sol")
				   ->join(array("inf"=>"op_solicitudes_informacion"),'sol.id = inf.id_solicitud',"INNER")
				   ->join(array("cli"=>"op_clientes"),'sol.cliente = cli.id',"INNER")
				   ->join(array("pro"=>"op_productos"),'sol.producto = pro.id',"INNER")
				   ->join(array("des"=>"op_solicitudes_desembolsos"),'sol.id = des.id_solicitud',"INNER")
				   ->join(array("acu"=>"op_solicitudes_abonosDesembolsos"),'acu.id_desembolso = des.id',"LEFT")
				   ->join(array("tas"=>"op_parametros_tasas"),'pro.id = tas.id_producto',"INNER")
				   ->join(array("adm"=>"op_parametros_administrativos"),'pro.id = adm.id_producto',"INNER")
				   ->join(array("cob"=>"op_parametros_cobranza"),'pro.zona = cob.id_zona',"INNER")
				   ->where("des.id = ?", $id);
	    $return=$orm->exec()->fetchArray(); 
	    return $return;
	}
	
	
	public function updDesembolso(
                    $id,
                    $k,
                    $k2,
                    $ic,
                    $ic2,
                    $otros,
                    $mora,
                    $cobranza
                ){
		$orm = new ORM();
		$query=$orm->update("op_solicitudes_desembolsos")
                            ->set(array(	
                                "aporteCapital" => $k,
                                "capital"       => $k2,
                                "interes"       => $ic,
                                "interes_dia"   => $ic2,
                                "otros_total"   => $otros,
                                "interes_mora"  => $mora,
                                "cobranza"      => $cobranza,
                                "total"         => ($k+$k2+$ic+$ic2+$otros+$mora+$cobranza)
                            ))
                            ->where("id = ?", $id);
	    $return=$orm->exec()->afectedRows(); 
	    return $return;
	} // updDesembolso
	
	public function abonarDesembolso($id, $k, $ic, $adm1, $adm2, $rec, $im, $cob, $total, $abono,$iva){
		$orm = new ORM();
		$query=$orm->update("op_solicitudes_desembolsos")
				   ->set(array(	"capital"=> $k,
								"interes_corriente"=> $ic,
								"admi1"=> $adm1,
								"admi2"=> $adm2,
								"recaudo"=> $rec,
								"interes_mora"=> $im,
								"cobranza"=> $cob,
								"total"=> $total,
								"iva"=>$iva
							))
				   ->where("id = ?", $id);
				  // die($query->versql());
	    $return=$orm->exec()->afectedRows(); 
		
		$orm = new ORM();
		$query=$orm->insert("op_solicitudes_abonosDesembolsos")
				   ->values(array(0, $abono, $id, opDate(false,2)));
	    $return=$orm->exec()->afectedRows(); 
	    return $return;
	} // abonarDesembolso
	
	public function infoUser($user){
		$orm = new ORM();
		$orm->select()
				   ->from("op_solicitudes")
				   ->where('cliente = ?', $user)
				   ->where('estado = ?', '3');
	    $return=$orm->exec()->fetchArray(); 
	    return $return;
	}
        
        public function saveCuota(
                                $idAcuerdo,
                                $aporteCapital,
                                $capital,
                                $interes,
                                $saldo,
                                $total,
                                $fechaPago,
                                $fechaCorte,
                                $interes_dia,
                                $otros_total,
                                $otros_concepto
                                ){
		$orm = new ORM();
		$sql = $orm -> insert("op_solicitudes_cuotas")
                            -> values(
                                    array(
                                        0,
                                        $idAcuerdo,
                                        $aporteCapital,
                                        $capital,
                                        $interes,
                                        $saldo,
                                        $total,
                                        $fechaCorte,
                                        $fechaPago,
                                        $interes_dia,
                                        $otros_total,
                                        $otros_concepto,
                                        0,
                                        0,
                                        1
                                        )
                                );
		//die($sql->getSql());
		return $sql -> exec() -> afectedRows();
                
	} // newAcuerdo
        
        public function saveCuota2(
                                $idAcuerdo,
                                $aporteCapital,
                                $capital,
                                $interes,
                                $saldo,
                                $total,
                                $fechaPago,
                                $fechaCorte,
                                $interes_dia,
                                $otros_total,
                                $otros_concepto
                                ){
		$orm = new ORM();
		$sql = $orm -> insert("op_solicitudes_cuotas2")
                            -> values(
                                    array(
                                        0,
                                        $idAcuerdo,
                                        $aporteCapital,
                                        $capital,
                                        $interes,
                                        $saldo,
                                        $total,
                                        $fechaCorte,
                                        $fechaPago,
                                        $interes_dia,
                                        $otros_total,
                                        $otros_concepto,
                                        0,
                                        0,
                                        1
                                        )
                                );
		
		return $sql -> exec() -> afectedRows();
                
	} // newAcuerdo
  
    public function crearCupo(
                            $cliente_id,
                            $cliente_nombre,
                            $cupo,
                            $estudio,
                            $concepto
                            ){
        $orm = new ORM();
        $orm	-> insert("op_solicitudes_cupo")
                                -> values(
                                    array(
                                    0,
                                    $cupo,
                                    $cliente_id,
                                    $cliente_nombre, 
                                    opDate(),
                                    '0',
                                     null,
                                    $estudio,
                                    $concepto,
                                    null
                                      )
                                  );
        $return = $orm-> exec()-> afectedRows();
        return $return > 0 ? true : false;
    }
    
    public function infoCupo($id=false){
            $orm = new ORM();
            $orm->select()
                ->from("op_solicitudes_cupo");
                
            if($id){
                $orm->where('id=?',$id);
            }
            //die($orm->verSql());
        $return=$orm->exec()->fetchArray(); 
        return $return;
    }
    
        public function infoCupo2($id=false){
            $orm = new ORM();
            $orm->select()
                ->from("op_clientes_cupo");
            if($id){
                $orm->where('cliente=?',$id);
            }
            //die($orm->verSql());
        $return=$orm->exec()->fetchArray(); 
        return $return;
    }
    
    public function infoCliente($id=false){
            $orm = new ORM();
            $orm->select()
                ->from("vista_clientes")
                ->where('id=?',$id);
            //die($orm->verSql());
        $return=$orm->exec()->fetchArray(); 
        return $return;
    }
    
    public function updCupo($id,$monto){
		$orm = new ORM();
		$query=$orm->update("op_solicitudes_cupo")
				   ->set(
                                        array("cupo"=> $monto)
                                        )
				   ->where("id = ?", $id);
	    $return=$query->exec()->afectedRows();
	    return $return;
	} // abonarDesembolso
        
        public function updCupoRecha($id,$motivo){
		$orm = new ORM();
		$query=$orm->update("op_solicitudes_cupo")
				   ->set(
                                        array("motivo"=> $motivo)
                                        )
				   ->where("id = ?", $id);
	    $return=$query->exec()->afectedRows();
	    return $return;
	} // abonarDesembolso
        
        public function updSolicitudRecha($id,$motivo){
		$orm = new ORM();
		$query=$orm->update("op_solicitudes")
				   ->set(
                                        array(
                                            "motivo"=> $motivo,
                                            "fecha2"=> opDate()
                                            )
                                        )
				   ->where("id = ?", $id);
	    $return=$query->exec()->afectedRows();
	    return $return;
	} // abonarDesembolso
        
        public function aprobarCupo($id, $cupo){
            $orm = new ORM();
            $sql = $orm	-> insert("op_clientes_cupo")
				  -> values(
					    array(
                                                0,
						$cupo,
						$cupo,
						0,
                                                $id
					    )
					  );
            //die($sql->verSql());
            $retorno = $sql -> exec() -> afectedRows() > 0 ? true : false;
           
            return $retorno;
        }
        
        public function aprobarCupo2($id){
            $orm = new ORM();
             $query = $orm->update("op_solicitudes_cupo")
				   ->set(
                                        array(
                                            "estado"=> 1,
                                            "fecha"=>  opDate(),
                                            "fecha_apro"=>opDate(true)
                                            )
                                        )
				   ->where("id = ?", $id);
	    $return=$query->exec()->afectedRows();
	    return $return;
        }
        
        public function rechazarCupo($id){
            $orm = new ORM();
             $query = $orm->update("op_solicitudes_cupo")
				   ->set(
                                        array(
                                            "estado"=> 2,
                                            "fecha"=>  opDate()
                                            )
                                        )
				   ->where("id = ?", $id);
	    $return=$query->exec()->afectedRows();
	    return $return;
        }
        
        public function cupoCliente($id){
            $orm = new ORM();
            $orm->select()
                ->from("op_clientes_cupo")
                ->where('cliente=?',$id);
            //die($orm->verSql());
        $return=$orm->exec()->fetchArray(); 
        return $return;
    }
    
    public function verconcepto($id){
        $orm = new ORM();
        $sql=$orm    ->select()
                ->from('op_solicitudes_estudio')
                ->where('id_solicitud=?',$id);
        $return = $sql->exec()->fetchArray();
        return $return;
    }
    
     public function guardarEstudio($id, $estudio,$concepto){
            $orm = new ORM();
            $sql = $orm	-> insert("op_solicitudes_estudio")
				  -> values(
					    array(
                                                0,
						$id,
						$estudio,
						$concepto,
                                                opDate(),
                                                0
					    )
					  );
            //die($sql->verSql());
            $retorno = $sql -> exec() -> afectedRows() > 0 ? true : false;
           
            return $retorno;
        }
        
        public function updEstudio($id,$est,$con){
            $orm = new ORM();
             $query = $orm->update("op_solicitudes_estudio")
				   ->set(
                                        array(
                                            "estudio"=> $est,
                                            "concepto"=>  $con,
                                            "fecha"=>  opDate()
                                            )
                                        )
				   ->where("id_solicitud = ?", $id);
	    $return=$query->exec()->afectedRows();
	    return $return;
        }
        
        public function cerrarEstudio($id){
            $orm = new ORM();
             $query = $orm->update("op_solicitudes_estudio")
				   ->set(
                                        array(
                                            "cerrado"=> 1
                                            )
                                        )
				   ->where("id_solicitud = ?", $id);
	    $return=$query->exec()->afectedRows();
	    return $return;
        }
        
        public function infoSolicitud($id){
            $orm = new ORM();
            $orm->select()
                ->from("op_solicitudes","sol")
                -> join(array("cli"=>"op_clientes"),'sol.cliente=cli.id')
                ->where('sol.id = ?', $id);
	    $return=$orm->exec()->fetchArray(); 
	    return $return;
	}
        
        public function infodesembolsar($id,$obser,$medio){
            $orm = new ORM();
             $query = $orm->update("op_solicitudes")
				   ->set(
                                        array(
                                            "observaciones"=> $obser,
                                            "medio"=> $medio
                                            )
                                        )
				   ->where("id = ?", $id);
	    $return=$query->exec()->afectedRows();
	    return $return;
        }
        
} // class
