<?php
class vSolicitudes{
    
    public function main($productos=''){
      echo getMensaje();
        ?>
           <h2><?php  echo SOLICITUDES_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  SOLICITUDES_NOMBRE        =>'#',
            		  SOLICITUDES_LISTA         =>setUrl('solicitudes','lst'),
            		  SOLICITUDES_DESEMBOLSO    =>setUrl('solicitudes','desembolso'),
            		  SOLICITUDES_RECHAZO       =>setUrl('solicitudes','lstRechazadas'),
                          SOLICITUDES_DESEMBOLSOLST =>setUrl('solicitudes','lstDesembolsos'),
                          SOLICITUDES_CUPO          =>setUrl('solicitudes','cupo')
            		 ),
            	    SOLICITUDES_NOMBRE
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  SOLICITUDES_NOMBRE=>'#'
            		 ),
            	    SOLICITUDES_NOMBRE, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo SOLICITUDES_NOMBRE?></h4>
       <div class='container'>
	   <div class='span10 offset1'>
	    <?php
	    if(getGrupoUsuario()=='1'):
		$datos='';
		$sucursales = mSucursales::lstSucursales();
		foreach($sucursales As $su){
		  $datos[$su['id']] = $su['nombre_suc'];
		}
		$datos2='';
		$zonas = mZonas::listar();
		foreach($zonas As $su2){
		  $datos2[$su2['id']] = $su2['zona'];
		}
	    endif;
	    ?>
		<?php Html::openForm("solicitudes",setUrl("solicitudes","solicitudProcess"));?>
                <!--<fieldset>
                    <legend>Informaci&oacute;n interna</legend>
                    <?php
                           echo selectZonas();
                           echo '<div id="verSucur"></div>';
                            echo selectEmpleados('','','responsable');
                            echo selectEmpleados('','','cartera');
                            /*Html::newSelect("estado", "Estado *", array(
                                                                        "1"=>"En estudio",
                                                                        "2"=>"Estudio cerrado",
                                                                        "3"=>"Aprobado desembolsado",
                                                                        "4"=>"Rechazado",
                                                                        "5"=>"Aprobado no desembolsado"
                                                                        )
                                        );*/
                    ?>
                </fieldset>-->
                <fieldset>
                    <legend>Informaci&oacute;n solicitud</legend>
                    <?php
                    Html::newInput("lstClientes","Cliente*");
                    Html::newInput("lstCodeudores","Codeudor *");
                    ?>
                    <!--<div id="lcredito2">
                        <div class="control-group">
                            <label for="lineaCredito2" class="control-label">Linea * : </label>
                            <div class="controls" id="ver_plazos">
                                    <select id="lineaCredito2" name="lineaCredito2">
                                      <option value=""> - Seleccione - </option>
                                    </select>
                            </div>
                        </div>
                    </div>-->
                    <div id="lcredito">
                        <div class="control-group">
                            <label for="lineaCredito" class="control-label">Producto * : </label>
                            <div class="controls" id="ver_plazos">
                                    <select id="lineaCredito" name="lineaCredito">
                                      <option value=""> - Seleccione - </option>
                                    </select>
                            </div>
                        </div>
                    </div>
                    <div id="tasas2">
                    <div class="control-group">
                            <label for="tasas" class="control-label">Tasa * : </label>
                            <div class="controls" id="ver_plazos">
                                    <input type="text" name="tasa" id="latasa" readonly>
                            </div>
                        </div>
                    </div>
                    <div id="lcupo">
                        <div class="control-group">
                            <label for="elcupo" class="control-label">Cupo * : </label>
                            <div class="controls" id="ver_plazos">
                                <input type="text" name="elcupo" id="elcupo" readonly>
                            </div>
                        </div>
                    </div>
                    <div id="lempresa">
                        <div class="control-group">
                            <label for="empresa" class="control-label">Empresa * : </label>
                            <div class="controls" id="ver_empresa">
                                    <select id="empresa" name="empresa">
                                      <option value=""> - Seleccione - </option>
                                    </select>
                            </div>
                        </div>
                    </div>
                    <?php
                    Html::newInput("valor_solicitado","Valor solicitado *");
                    Html::newSelect("amortizacion", "Amortizacion *", array(
                                                                        "2"=>"MENSUAL"
                                                                        )
                                        );
                    Html::newSelect("fecha_corte", "Fecha de corte *", array(
                                                                        "25"=>"25",
                                                                        "10"=>"10"
                                                                        )
                                        );
                    ?>
                    
                    <div class="control-group">
                        <label for="plazo" class="control-label">Plazos * : </label>
                        <div class="controls">
                            <input type="number" name="plazo" id="plazo" onkeyup="cuotas(this)">
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label for="cuota" class="control-label">Valor aproximado cuota * : </label>
                        <div class="controls">
                            <input name="cuota" id="cuota" type="text" disabled>
                        </div>
                    </div>
                    
                    <?php
                        Html::newSelect("bajar_tasa", "Solicitud cambio tasa *", array(
                                                                        "1"=>"Si",
                                                                        "2"=>"No"
                                                                        )
                                        );
                        Html::newTextarea('observaciones','Observaciones');
                    ?>
                    
                    <div id="lstOtros">
                        
                    </div>
                    
                </fieldset>
                <input name="valVehiculo" id="valVehiculo" value="n" type="hidden">
                <input name="valReferencia" id="valReferencia" value="s" type="hidden">
                <fieldset id="vehiculos">
                    <legend>Vehiculo</legend>
                    <?php
                        Html::newInput("placa","Placa *");
                        Html::newInput("marca","Marca *");
                        Html::newInput("vLinea","Linea *");
                        Html::newInput("cilindraje","Cilindraje *");
                        Html::newInput("modelo","Modelo *");
                        Html::newInput("precioV","Precio de venta *");
                        Html::newSelect("estadoV", "Estado *",
                                array(
                                    "n"=>"Nuevo",
                                    "u"=>"Usado"
                                    )
                        );
                        Html::newInput("nombreV","Nombre del vendedor *");
                        Html::newInput("numeroV","No documento *");
                        Html::newInput("concesionario","Concesionario *");
                        Html::newInput("telV","Telefono *");
                    ?>
                </fieldset>
                
                <fieldset id="referencias">
                    <legend>Referencias</legend>
                    <?php
                        echo selectReferencia("","","referencia[]");
                        echo selectVinculo("","","vinculo[]");
                        Html::newInput("nombrer[]","Nombre referencia *");
                        Html::newInput("apellidor[]","Apelldio referencia *");
                        Html::newInput("ccr[]","Documento referencia *");
                        Html::newInput("correor[]","Correo referencia *");
                        echo inputDate('fechar');
                        //Html::newInput("fechar[]","Fecha nacimineto referencia *");
                        ?>
                        <div class="control-group">
                            <label id="fechar" for="fechar" class="control-label">Fecha nacimineto referencia * : </label>
                            <div class="controls">
                            <input type="text" autocomplete="off" placeholder="Fecha nacimineto referencia  " id="fechar" name="fechar[]">
                            </div>
			</div>
                        <?php
                        Html::newInput("dirr[]","Direccion referencia *");
                        Html::newInput("telr[]","Telefono referencia *");
                        Html::newInput("celr[]","Celular referencia *");
                        Html::newInput("empresar[]","Empresa donde labora referencia *");
                        Html::newInput("cargor[]","Cargo referencia *");
                        echo '<hr>';
                        echo selectReferencia("","","referencia[]");
                        echo selectVinculo("","","vinculo[]");
                        Html::newInput("nombrer[]","Nombre referencia *");
                        Html::newInput("apellidor[]","Apelldio referencia *");
                        Html::newInput("ccr[]","Documento referencia *");
                        Html::newInput("correor[]","Correo referencia *");
                       // echo inputDateMenores('fechar[]');
                        Html::newInput("fechar[]","Fecha nacimineto referencia *");
                        Html::newInput("dirr[]","Direccion referencia *");
                        Html::newInput("telr[]","Telefono referencia *");
                        Html::newInput("celr[]","Celular referencia *");
                        Html::newInput("empresar[]","Empresa donde labora referencia *");
                        Html::newInput("cargor[]","Cargo referencia *");
                    ?>
                </fieldset>
                <div id="tasas"></div>
                <!--<fieldset>
                    <div class="control-group" id="adj">
                        <label for="adjuntos1" class="control-label">Adjuntos : </label>
                        <div class="controls" id="nuevo">
                            <input type="file" name="adjuntos1[]" id="adjuntos1">
                            <input type="text" name="nadjunto[]" id="nadjunto" placeholder="Nombre archivo">
                            <a class="btn clonar" onclick="clonar(this)">+</a>
                            <a class="btn remover" onclick="remover(this)">-</a>
                        </div>
                    </div>
                </fieldset>-->
                
		<?php
		  Html::newHidden("lazona",getZona());
		  Html::newButton("enviar", "Enviar", "submit");
		  Html::closeForm();
		?>
        </div>
       </div>
       <?php  
    }
    
    public function lst($info){
      echo getMensaje();
        ?>
           <h2><?php  echo SOLICITUDES_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  SOLICITUDES_NOMBRE        =>setUrl('solicitudes'),
            		  SOLICITUDES_LISTA         =>'#',
            		  SOLICITUDES_DESEMBOLSO    =>setUrl('solicitudes','desembolso'),
                          SOLICITUDES_RECHAZO       =>setUrl('solicitudes','lstRechazadas'),
			  SOLICITUDES_DESEMBOLSOLST =>setUrl('solicitudes','lstDesembolsos'),
                          SOLICITUDES_CUPO          =>setUrl('solicitudes','cupo')
            		 ),
            	    SOLICITUDES_LISTA
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  SOLICITUDES_LISTA=>'#'
            		 ),
            	    SOLICITUDES_LISTA, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo SOLICITUDES_LISTA?></h4>
	  <div class='container'>
	    <?php echo datatable('lst')?>
	    <table class="table table-hover" id="lst">
		<thead>
		  <tr>
		    <th>#</th>
		    <th>Cliente</th>
		    <th>Producto</th>	
		    <th>Monto</th>
		    <th>Estado</th>
                    <th>Fecha solicitud</th>
                    <th>Dìa corte</th>
		    <th>Adjuntos</th>
		    <th>Subir adjuntos</th>
		    <th>Ver</th>
                    <?php if(getGrupoUsuario()==1){ ?>
                        <th>Editar</th>
                    <?php } ?>
		    <th>Aprobar</th>
		    <th>Rechazar</th>
		  </tr>
		</thead>
		<tbody>
		  <?php
			if(is_array($info)){
		    foreach ($info As $i):
		  ?>
		  <tr>
		    <td><?php echo $i['id']?></td>
		    <td><?php echo $i['nombre']?></td>
		    <td><?php echo $i['elpro']?></td>
		    <td><?php echo toMoney($i['monto'])?></td>
		    <td>
			<?php
			  if($i['estado']=='0'){
			    echo 'En estudio';
			  }else if($i['estado']=='1'){
			    echo 'Estudio cerrado';
			  }else{
			    echo 'Rechazado';
			  }
			 ?>
		    </td>
                    <td><?php echo $i['fecha']?></td>
                    <td><?php echo $i['fecha_corte']?></td>
		    <td>
		    <a href="<?php echo setUrl('solicitudes','archivos',array('cliente'=>trim($i["cliente"]),'solicitud'=>$i["id"]))?>"><img src="app/img/download.png" alt="adjuntos" title="adjuntos"></a>
		    </td>
		    <td>
		    <a href="<?php echo setUrl('solicitudes','cargarDocs',array('cliente'=>trim($i["cliente"]),'solicitud'=>$i["id"]))?>"><img src="app/img/folder.png" alt="adjuntos" title="adjuntos"></a>
		    </td>
		    <td>
			<a href="<?php echo setUrl('solicitudes','ver',array("id"=>$i["id"]))?>"><img src="app/img/ver.png" alt="ver" title="Ver"></a>
		    </td>
                    <?php if(getGrupoUsuario()==1){ ?>
                        <td>
                            <a href="<?php echo setUrl('solicitudes','editar',array("id"=>$i["id"]))?>"><img src="app/img/editar.png" alt="ver" title="Ver"></a>
                        </td>
                    <?php }?>
		    <td>
                        <?php
                        if($i['estado']=='1'){
                        ?>
			<a onclick="return confirm('Desea aprobar esta solicitud?')" href="<?php echo setUrl('solicitudes','aprobar',array("id"=>$i["id"],"estado"=>"3"))?>"><img src="app/img/use.png" alt="aprobar" title="Aprobar"></a>
                        <?php
                        }else{
                            echo 'En estudio';
                        }
                        ?>
		    </td>
		    <td>
		    <?php if($i["estado"]=="1"){ ?>
			<a onclick="return confirm('Desea rechazar esta solicitud ?')" href="<?php echo setUrl('solicitudes','rechazar',array("id"=>$i["id"],"estado"=>"2"))?>"><img src="app/img/inactivo.png" alt="rechazar" title="Rechazar"></a>
                    <?php } ?>
		    </td>
		  </tr>
		  <?php
		    endforeach;
			}else{
			  echo 'No hay datos';
			}
		  ?>
		</tbody>
	    </table>
	  </div>
       <?php  
    }
    
    public function ver(
                        $id,
                        $info,
                        $referencia=null,
                        $vinculo=null,
                        $nombrer=null,
                        $apellidor=null,
                        $ccr=null,
                        $correor=null,
                        $fechar=null,
                        $dirr=null,
                        $telr=null,
                        $celr=null,
                        $empresa=null,
                        $cargor=null,
                        $ficheros,
            $cuota
                        ){
      echo getMensaje();
        ?>
           <h2><?php  echo SOLICITUDES_VER?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('solicitudes','lst'), 
            		  SOLICITUDES_VER=>"#",
            		 ),
            	    SOLICITUDES_VER
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('solicitudes','lst'), 
            		  SOLICITUDES_VER=>'#'
            		 ),
            	    SOLICITUDES_VER, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo SOLICITUDES_VER?></h4>
       <div class='container'>
	   <!--<div class="span7 offset0">-->
		<?php Html::openForm("solicitudes",setUrl("solicitudes","solicitudProcess"));?>
		<!--<fieldset>
		  <legend><?php echo SOLICITUDES_NOMBRE?></legend>
		  <?php
		    Html::newInput("lstClientes","Buscar cliente *",$info['cliente'].'-'.$info['nombre'].' '.$info['apellido'],'readonly');
		  ?>
		</fieldset>-->
		
		<fieldset>
		 <legend>Informaci&oacute;n solicitud</legend>
                    <?php
                    Html::newInput("lstClientes","Cliente*",$info['nombre'],'readonly');
                    Html::newInput("lstCodeudores","Codeudor *",$info['nombreCod'],'readonly');
                    //echo selectProductos();
                    Html::newInput("valor_solicitado","Valor solicitado *",  toMoney($info['monto']),'readonly');
                    Html::newSelect("amortizacion", "Amortizacion *", array(
                                                                        "1"=>"QUINCENAL",
                                                                        "2"=>"MENSUAL"
                                                                        ),$info['amortizacion'],'disabled'
                                        );
                    Html::newSelect("fecha_corte", "Fecha de corte *", array(
                                                                        "25"=>"25",
                                                                        "10"=>"10"
                                                                        ),$info["fecha_corte"],'disabled'
                                        );
                    ?>
                    <div >
                    <div class="control-group">
                        <label for="lineaCredito" class="control-label">Credito * : </label>
                        <div class="controls" id="ver_plazos">
                                <?php echo $info["nproducto"] ?>
                        </div>
                    </div>
                    </div>
                    <div class="control-group">
                        <label for="plazo" class="control-label">Plazos * : </label>
                        <div class="controls" id="ver_plazos">
                                <?php echo $info["plazo"] ?>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="plazo" class="control-label">Cuota * : </label>
                        <div class="controls" id="ver_plazos">
                                <?php echo toMoney($cuota) ?>
                        </div>
                    </div>
                 
                    <?php
                       Html::newInput('Fecha','fecha',$info['fecha'],'disabled');
                    Html::newTextarea('observaciones','Observaciones',$info['observaciones2'],'disabled');
                    ?>
                </fieldset>
           <?php if(!isset($info['elpro'])){ ?>
                <fieldset>
                    <legend>Referencias</legend>
                    <?php
                        echo selectReferencia($referencia[0],'disabled',"referencia[]");
                        echo selectVinculo($vinculo[0],'disabled',"vinculo[]");
                        Html::newInput("nombrer[]","Nombre referencia *",$nombrer[0],'readonly');
                        Html::newInput("apellidor[]","Apelldio referencia *",$apellidor[0],'readonly');
                        Html::newInput("ccr[]","Documento referencia *",$ccr[0],'readonly');
                        Html::newInput("correor[]","Correo referencia *",$correor[0],'readonly');
                        echo inputDateMenores('fechar[]');
                        Html::newInput("fechar[]","Fecha nacimineto referencia *",$fechar[0],'readonly');
                        Html::newInput("dirr[]","Direccion referencia *",$dirr[0],'readonly');
                        Html::newInput("telr[]","Telefono referencia *",$telr[0],'readonly');
                        Html::newInput("celr[]","Celular referencia *",$celr[0],'readonly');
                        Html::newInput("empresar[]","Empresa donde labora referencia *",$empresa[0],'readonly');
                        Html::newInput("cargor[]","Cargo referencia *",$cargor[0],'readonly');
                        echo '<hr>';
                        echo selectReferencia($referencia[1],'disabled',"referencia[]");
                        echo selectVinculo($vinculo[1],'disabled',"vinculo[]");
                        Html::newInput("nombrer[]","Nombre referencia *",$nombrer[1],'readonly');
                        Html::newInput("apellidor[]","Apelldio referencia *",$apellidor[1],'readonly');
                        Html::newInput("ccr[]","Documento referencia *",$ccr[1],'readonly');
                        Html::newInput("correor[]","Correo referencia *",$correor[1],'readonly');
                        echo inputDateMenores('fechar[]');
                        Html::newInput("fechar[]","Fecha nacimineto referencia *",$fechar[1],'readonly');
                        Html::newInput("dirr[]","Direccion referencia *",$dirr[1],'readonly');
                        Html::newInput("telr[]","Telefono referencia *",$telr[1],'readonly');
                        Html::newInput("celr[]","Celular referencia *",$celr[1],'readonly');
                        Html::newInput("empresar[]","Empresa donde labora referencia *",$empresa[1],'readonly');
                        Html::newInput("cargor[]","Cargo referencia *",$cargor[1],'readonly');
                    ?>
                </fieldset>
                <fieldset>
                     <?php
	$i=0;
    	$val=0;
    	$cadena ='';
    	$cadena .= '<ul class="nav nav-tabs">';
    	foreach($ficheros As $f){
        if($i>1){
            //echo "uno";
                if($f){
                    //echo "dos";
                        $val++;
                        $ext = explode('.',$f);
                        if($ext[1]=='pdf'){
                                $img = $ext[1].'.png';
                        }else if($ext[1]=='jpg' || $ext[1]=='png' || $ext[1]=='gif' || $ext[1]=='tif' || $ext[1]=='docx' || $ext[1]=='xlsx'){
                                $img = 'imagen.png';
                        }else{
                                $img = 'file.png';
                        }
                        if($img == 'imagen.png'){
                        $cadena .='<li>
                                <a target="_blank" href="app/files/clientes/'.$info['cliente'].'/solicitudes/'.$id.'/'.$f.'">
                                        <img src="app/img/'.$img.'" height="22" width="22"><br/>'.$f.'
                                </a>
                                <a title="Eliminar" onclick="return confirm(\'Desea eliminar este archivo '.$f.'\')" href="'.setUrl('solicitudes','eliminarDoc',array('id'=>$id,'cc'=>$info['cliente_id'],'file'=>$f,'folder'=>$id)).'">Eliminar <img src="app/img/eliminar_min.png" alt="Eliminar"></a>
                              <li/>';
                              }
                      }else{
                        $cadena .= 'No hay datos';
                      }
        }
        $i++;
    	}
    	$cadena .= '</ul>';
    	/*ficheros cliente*/
    	echo $cadena;
	  ?>
                </fieldset>
		<?php
           }
		// Html::newButton("enviar", "Enviar", "submit");
		  Html::closeForm();
		?>
           <a href="<?php echo setUrl('solicitudes','concepto',array('id'=>$id))?>" class="btn btn-primary">Concepto de estudio</a>
        <!--</div>-->
       </div>
       <?php  
    }
    
        public function ver_re(
                        $id,
                        $info,
                        $referencia=null,
                        $vinculo=null,
                        $nombrer=null,
                        $apellidor=null,
                        $ccr=null,
                        $correor=null,
                        $fechar=null,
                        $dirr=null,
                        $telr=null,
                        $celr=null,
                        $empresa=null,
                        $cargor=null,
                        $ficheros,
            $cuota
                        ){
      echo getMensaje();
        ?>
           <h2><?php  echo SOLICITUDES_VER?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('solicitudes','lstRechazadas'), 
            		  SOLICITUDES_VER=>"#",
            		 ),
            	    SOLICITUDES_VER
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('solicitudes','lstRechazadas'), 
            		  SOLICITUDES_VER=>'#'
            		 ),
            	    SOLICITUDES_VER, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo SOLICITUDES_VER?></h4>
       <div class='container'>
	   <!--<div class="span7 offset0">-->
		<?php Html::openForm("solicitudes",setUrl("solicitudes","solicitudProcess"));?>
		<!--<fieldset>
		  <legend><?php echo SOLICITUDES_NOMBRE?></legend>
		  <?php
		    Html::newInput("lstClientes","Buscar cliente *",$info['cliente'].'-'.$info['nombre'].' '.$info['apellido'],'readonly');
		  ?>
		</fieldset>-->
		
		<fieldset>
		 <legend>Informaci&oacute;n solicitud</legend>
                    <?php
                    Html::newInput("lstClientes","Cliente*",$info['nombre'],'readonly');
                    Html::newInput("lstCodeudores","Codeudor *",$info['nombreCod'],'readonly');
                    //echo selectProductos();
                    Html::newInput("valor_solicitado","Valor solicitado *",  toMoney($info['monto']),'readonly');
                    Html::newSelect("amortizacion", "Amortizacion *", array(
                                                                        "1"=>"QUINCENAL",
                                                                        "2"=>"MENSUAL"
                                                                        ),$info['amortizacion'],'disabled'
                                        );
                    Html::newSelect("fecha_corte", "Fecha de corte *", array(
                                                                        "25"=>"25",
                                                                        "10"=>"10"
                                                                        ),$info["fecha_corte"],'disabled'
                                        );
                    ?>
                    <div >
                    <div class="control-group">
                        <label for="lineaCredito" class="control-label">Credito * : </label>
                        <div class="controls" id="ver_plazos">
                                <?php echo $info["nproducto"] ?>
                        </div>
                    </div>
                    </div>
                    <div class="control-group">
                        <label for="plazo" class="control-label">Plazos * : </label>
                        <div class="controls" id="ver_plazos">
                                <?php echo $info["plazo"] ?>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="plazo" class="control-label">Cuota * : </label>
                        <div class="controls" id="ver_plazos">
                                <?php echo toMoney($cuota) ?>
                        </div>
                    </div>
                 
                    <?php
                    Html::newTextarea('observaciones','Observaciones',$info['observaciones2'],'disabled');
                    ?>
                </fieldset>
           <?php if(!isset($info['elpro'])){ ?>
                <fieldset>
                    <legend>Referencias</legend>
                    <?php
                        echo selectReferencia($referencia[0],'disabled',"referencia[]");
                        echo selectVinculo($vinculo[0],'disabled',"vinculo[]");
                        Html::newInput("nombrer[]","Nombre referencia *",$nombrer[0],'readonly');
                        Html::newInput("apellidor[]","Apelldio referencia *",$apellidor[0],'readonly');
                        Html::newInput("ccr[]","Documento referencia *",$ccr[0],'readonly');
                        Html::newInput("correor[]","Correo referencia *",$correor[0],'readonly');
                        echo inputDateMenores('fechar[]');
                        Html::newInput("fechar[]","Fecha nacimineto referencia *",$fechar[0],'readonly');
                        Html::newInput("dirr[]","Direccion referencia *",$dirr[0],'readonly');
                        Html::newInput("telr[]","Telefono referencia *",$telr[0],'readonly');
                        Html::newInput("celr[]","Celular referencia *",$celr[0],'readonly');
                        Html::newInput("empresar[]","Empresa donde labora referencia *",$empresa[0],'readonly');
                        Html::newInput("cargor[]","Cargo referencia *",$cargor[0],'readonly');
                        echo '<hr>';
                        echo selectReferencia($referencia[1],'disabled',"referencia[]");
                        echo selectVinculo($vinculo[1],'disabled',"vinculo[]");
                        Html::newInput("nombrer[]","Nombre referencia *",$nombrer[1],'readonly');
                        Html::newInput("apellidor[]","Apelldio referencia *",$apellidor[1],'readonly');
                        Html::newInput("ccr[]","Documento referencia *",$ccr[1],'readonly');
                        Html::newInput("correor[]","Correo referencia *",$correor[1],'readonly');
                        echo inputDateMenores('fechar[]');
                        Html::newInput("fechar[]","Fecha nacimineto referencia *",$fechar[1],'readonly');
                        Html::newInput("dirr[]","Direccion referencia *",$dirr[1],'readonly');
                        Html::newInput("telr[]","Telefono referencia *",$telr[1],'readonly');
                        Html::newInput("celr[]","Celular referencia *",$celr[1],'readonly');
                        Html::newInput("empresar[]","Empresa donde labora referencia *",$empresa[1],'readonly');
                        Html::newInput("cargor[]","Cargo referencia *",$cargor[1],'readonly');
                        Html::newTextarea('motivo','motivo',$info['motivo']);
                    ?>
                </fieldset>
                <fieldset>
                     <?php
	$i=0;
    	$val=0;
    	$cadena ='';
    	$cadena .= '<ul class="nav nav-tabs">';
    	foreach($ficheros As $f){
        if($i>1){
            //echo "uno";
                if($f){
                    //echo "dos";
                        $val++;
                        $ext = explode('.',$f);
                        if($ext[1]=='pdf'){
                                $img = $ext[1].'.png';
                        }else if($ext[1]=='jpg' || $ext[1]=='png' || $ext[1]=='gif' || $ext[1]=='tif' || $ext[1]=='docx' || $ext[1]=='xlsx'){
                                $img = 'imagen.png';
                        }else{
                                $img = 'file.png';
                        }
                        if($img == 'imagen.png'){
                        $cadena .='<li>
                                <a target="_blank" href="app/files/clientes/'.$info['cliente'].'/solicitudes/'.$id.'/'.$f.'">
                                        <img src="app/img/'.$img.'" height="22" width="22"><br/>'.$f.'
                                </a>
                                <a title="Eliminar" onclick="return confirm(\'Desea eliminar este archivo '.$f.'\')" href="'.setUrl('solicitudes','eliminarDoc',array('id'=>$id,'cc'=>$info['cliente_id'],'file'=>$f,'folder'=>$id)).'">Eliminar <img src="app/img/eliminar_min.png" alt="Eliminar"></a>
                              <li/>';
                              }
                      }else{
                        $cadena .= 'No hay datos';
                      }
        }
        $i++;
    	}
    	$cadena .= '</ul>';
    	/*ficheros cliente*/
    	echo $cadena;
	  ?>
                </fieldset>
		<?php
           }
		// Html::newButton("enviar", "Enviar", "submit");
		  Html::closeForm();
		?>
           <!--<a href="<?php echo setUrl('solicitudes','concepto',array('id'=>$id))?>" class="btn btn-primary">Concepto de estudio</a>-->
        <!--</div>-->
       </div>
       <?php  
    }
    
    
    
        public function ver2(
                        $id,
                        $info,
                        $referencia=null,
                        $vinculo=null,
                        $nombrer=null,
                        $apellidor=null,
                        $ccr=null,
                        $correor=null,
                        $fechar=null,
                        $dirr=null,
                        $telr=null,
                        $celr=null,
                        $empresa=null,
                        $cargor=null,
                        $ficheros,
            $cuota
                        ){
      echo getMensaje();
        ?>
           <h2><?php  echo SOLICITUDES_VER?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('solicitudes','desembolso'), 
            		  SOLICITUDES_VER=>"#",
            		 ),
            	    SOLICITUDES_VER
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('solicitudes','desembolso'), 
            		  SOLICITUDES_VER=>'#'
            		 ),
            	    SOLICITUDES_VER, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo SOLICITUDES_VER?></h4>
       <div class='container'>
	   <!--<div class="span7 offset0">-->
		<?php Html::openForm("solicitudes",setUrl("solicitudes","solicitudProcess"));?>
		<!--<fieldset>
		  <legend><?php echo SOLICITUDES_NOMBRE?></legend>
		  <?php
		    Html::newInput("lstClientes","Buscar cliente *",$info['cliente'].'-'.$info['nombre'].' '.$info['apellido'],'readonly');
		  ?>
		</fieldset>-->
		
		<fieldset>
		 <legend>Informaci&oacute;n solicitud</legend>
                    <?php
                    Html::newInput("lstClientes","Cliente*",$info['nombre'],'readonly');
                    Html::newInput("lstCodeudores","Codeudor *",$info['nombreCod'],'readonly');
                    //echo selectProductos();
                    Html::newInput("valor_solicitado","Valor solicitado *",  toMoney($info['monto']),'readonly');
                    Html::newSelect("amortizacion", "Amortizacion *", array(
                                                                        "1"=>"QUINCENAL",
                                                                        "2"=>"MENSUAL"
                                                                        ),$info['amortizacion'],'disabled'
                                        );
                    Html::newSelect("fecha_corte", "Fecha de corte *", array(
                                                                        "25"=>"25",
                                                                        "10"=>"10"
                                                                        ),$info["fecha_corte"],'disabled'
                                        );
                    ?>
                    <div >
                    <div class="control-group">
                        <label for="lineaCredito" class="control-label">Credito * : </label>
                        <div class="controls" id="ver_plazos">
                                <?php echo $info["nproducto"] ?>
                        </div>
                    </div>
                    </div>
                    <div class="control-group">
                        <label for="plazo" class="control-label">Plazos * : </label>
                        <div class="controls" id="ver_plazos">
                                <?php echo $info["plazo"] ?>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="plazo" class="control-label">Cuota * : </label>
                        <div class="controls" id="ver_plazos">
                                <?php echo toMoney($cuota) ?>
                        </div>
                    </div>
                 
                    <?php
                    Html::newTextarea('observaciones','Observaciones',$info['observaciones2'],'disabled');
                    ?>
                </fieldset>
           <?php if(!isset($info['elpro'])){ ?>
                <fieldset>
                    <legend>Referencias</legend>
                    <?php
                        echo selectReferencia($referencia[0],'disabled',"referencia[]");
                        echo selectVinculo($vinculo[0],'disabled',"vinculo[]");
                        Html::newInput("nombrer[]","Nombre referencia *",$nombrer[0],'readonly');
                        Html::newInput("apellidor[]","Apelldio referencia *",$apellidor[0],'readonly');
                        Html::newInput("ccr[]","Documento referencia *",$ccr[0],'readonly');
                        Html::newInput("correor[]","Correo referencia *",$correor[0],'readonly');
                        echo inputDateMenores('fechar[]');
                        Html::newInput("fechar[]","Fecha nacimineto referencia *",$fechar[0],'readonly');
                        Html::newInput("dirr[]","Direccion referencia *",$dirr[0],'readonly');
                        Html::newInput("telr[]","Telefono referencia *",$telr[0],'readonly');
                        Html::newInput("celr[]","Celular referencia *",$celr[0],'readonly');
                        Html::newInput("empresar[]","Empresa donde labora referencia *",$empresa[0],'readonly');
                        Html::newInput("cargor[]","Cargo referencia *",$cargor[0],'readonly');
                        echo '<hr>';
                        echo selectReferencia($referencia[1],'disabled',"referencia[]");
                        echo selectVinculo($vinculo[1],'disabled',"vinculo[]");
                        Html::newInput("nombrer[]","Nombre referencia *",$nombrer[1],'readonly');
                        Html::newInput("apellidor[]","Apelldio referencia *",$apellidor[1],'readonly');
                        Html::newInput("ccr[]","Documento referencia *",$ccr[1],'readonly');
                        Html::newInput("correor[]","Correo referencia *",$correor[1],'readonly');
                        echo inputDateMenores('fechar[]');
                        Html::newInput("fechar[]","Fecha nacimineto referencia *",$fechar[1],'readonly');
                        Html::newInput("dirr[]","Direccion referencia *",$dirr[1],'readonly');
                        Html::newInput("telr[]","Telefono referencia *",$telr[1],'readonly');
                        Html::newInput("celr[]","Celular referencia *",$celr[1],'readonly');
                        Html::newInput("empresar[]","Empresa donde labora referencia *",$empresa[1],'readonly');
                        Html::newInput("cargor[]","Cargo referencia *",$cargor[1],'readonly');
                    ?>
                </fieldset>
                <fieldset>
                     <?php
	$i=0;
    	$val=0;
    	$cadena ='';
    	$cadena .= '<ul class="nav nav-tabs">';
    	foreach($ficheros As $f){
        if($i>1){
            //echo "uno";
                if($f){
                    //echo "dos";
                        $val++;
                        $ext = explode('.',$f);
                        if($ext[1]=='pdf'){
                                $img = $ext[1].'.png';
                        }else if($ext[1]=='jpg' || $ext[1]=='png' || $ext[1]=='gif' || $ext[1]=='tif' || $ext[1]=='docx' || $ext[1]=='xlsx'){
                                $img = 'imagen.png';
                        }else{
                                $img = 'file.png';
                        }
                        if($img == 'imagen.png'){
                        $cadena .='<li>
                                <a target="_blank" href="app/files/clientes/'.$info['cliente'].'/solicitudes/'.$id.'/'.$f.'">
                                        <img src="app/img/'.$img.'" height="22" width="22"><br/>'.$f.'
                                </a>
                                <a title="Eliminar" onclick="return confirm(\'Desea eliminar este archivo '.$f.'\')" href="'.setUrl('solicitudes','eliminarDoc',array('id'=>$id,'cc'=>$info['cliente_id'],'file'=>$f,'folder'=>$id)).'">Eliminar <img src="app/img/eliminar_min.png" alt="Eliminar"></a>
                              <li/>';
                              }
                      }else{
                        $cadena .= 'No hay datos';
                      }
        }
        $i++;
    	}
    	$cadena .= '</ul>';
    	/*ficheros cliente*/
    	echo $cadena;
	  ?>
                </fieldset>
		<?php
           }
		// Html::newButton("enviar", "Enviar", "submit");
		  Html::closeForm();
		?>
           <a href="<?php echo setUrl('solicitudes','concepto',array('id'=>$id))?>" class="btn btn-primary">Concepto de estudio</a>
        <!--</div>-->
       </div>
       <?php  
    }
    
    
    public function verCupo(
                        $id,
                        $info,
                        $referencia=null,
                        $vinculo=null,
                        $nombrer=null,
                        $apellidor=null,
                        $ccr=null,
                        $correor=null,
                        $fechar=null,
                        $dirr=null,
                        $telr=null,
                        $celr=null,
                        $empresa=null,
                        $cargor=null,
                        $ficheros
                        ){
      echo getMensaje();
        ?>
           <h2><?php  echo SOLICITUDES_VER_CUPO?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('solicitudes','cupo'), 
            		  SOLICITUDES_VER_CUPO=>"#",
            		 ),
            	    SOLICITUDES_VER_CUPO
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('solicitudes','cupo'), 
            		  SOLICITUDES_VER_CUPO=>'#'
            		 ),
            	    SOLICITUDES_VER_CUPO, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo SOLICITUDES_VER?></h4>
       <div class='container'>
            <div class="span7 offset0">
		<fieldset>
                    <legend><?=SOLICITUDES_CUPO;?></legend>
                    <?php
                        Html::newInput("lstClientes","Cliente*",$info['cliente_id'].'-'.$info['cliente_nombre'],'disabled');
                        Html::newInput("valcupo","Cupo*",toMoney($info['cupo']),'disabled');
                        Html::newHidden("cupo",$info['cupo']);
                        Html::newTextarea('estudio','Estudio de credito',$info['estudio'],'disabled');
                        Html::newTextarea('concepto','Concepto del estudio',$info['credito'],'disabled');
                    ?>
                </fieldset>
                <fieldset>
                    <legend>Referencias</legend>
                    <?php
                        echo selectReferencia($referencia[0],'disabled',"referencia[]");
                        echo selectVinculo($vinculo[0],'disabled',"vinculo[]");
                        Html::newInput("nombrer[]","Nombre referencia *",$nombrer[0],'readonly');
                        Html::newInput("apellidor[]","Apelldio referencia *",$apellidor[0],'readonly');
                        Html::newInput("ccr[]","Documento referencia *",$ccr[0],'readonly');
                        Html::newInput("correor[]","Correo referencia *",$correor[0],'readonly');
                        echo inputDateMenores('fechar[]');
                        Html::newInput("fechar[]","Fecha nacimineto referencia *",$fechar[0],'readonly');
                        Html::newInput("dirr[]","Direccion referencia *",$dirr[0],'readonly');
                        Html::newInput("telr[]","Telefono referencia *",$telr[0],'readonly');
                        Html::newInput("celr[]","Celular referencia *",$celr[0],'readonly');
                        Html::newInput("empresar[]","Empresa donde labora referencia *",$empresa[0],'readonly');
                        Html::newInput("cargor[]","Cargo referencia *",$cargor[0],'readonly');
                        echo '<hr>';
                        echo selectReferencia($referencia[1],'disabled',"referencia[]");
                        echo selectVinculo($vinculo[1],'disabled',"vinculo[]");
                        Html::newInput("nombrer[]","Nombre referencia *",$nombrer[1],'readonly');
                        Html::newInput("apellidor[]","Apelldio referencia *",$apellidor[1],'readonly');
                        Html::newInput("ccr[]","Documento referencia *",$ccr[1],'readonly');
                        Html::newInput("correor[]","Correo referencia *",$correor[1],'readonly');
                        echo inputDateMenores('fechar[]');
                        Html::newInput("fechar[]","Fecha nacimineto referencia *",$fechar[1],'readonly');
                        Html::newInput("dirr[]","Direccion referencia *",$dirr[1],'readonly');
                        Html::newInput("telr[]","Telefono referencia *",$telr[1],'readonly');
                        Html::newInput("celr[]","Celular referencia *",$celr[1],'readonly');
                        Html::newInput("empresar[]","Empresa donde labora referencia *",$empresa[1],'readonly');
                        Html::newInput("cargor[]","Cargo referencia *",$cargor[1],'readonly');
                    ?>
                </fieldset>
                <fieldset>
                     <?php
	$i=0;
    	$val=0;
    	$cadena ='';
    	$cadena .= '<ul class="nav nav-tabs">';
    	foreach($ficheros As $f){
        if($i>1){
                if($f){
                        $val++;
                        $ext = explode('.',$f);
                        if($ext[1]=='pdf'){
                                $img = $ext[1].'.png';
                        }else if($ext[1]=='jpg' || $ext[1]=='png' || $ext[1]=='gif' || $ext[1]=='tif'){
                                $img = 'imagen.png';
                        }else{
                                $img = 'file.png';
                        }
                        if($img == 'imagen.png'){
                        $cadena .='<li>
                                <a target="_blank" href="app/files/clientes/'.$info['cliente_id'].'/cupos'.$id.'/'.$f.'">
                                        <img src="app/img/'.$img.'" height="22" width="22"><br/>'.$f.'
                                </a>
                                <a title="Eliminar" onclick="return confirm(\'Desea eliminar este archivo '.$f.'\')" href="'.setUrl('solicitudes','eliminarDoc',array('id'=>$id,'cc'=>$info['cliente_id'],'file'=>$f,'folder'=>$id)).'">Eliminar <img src="app/img/eliminar_min.png" alt="Eliminar"></a>
                              <li/>';
                              }
                      }else{
                        $cadena .= 'No hay datos';
                      }
        }
        $i++;
    	}
    	$cadena .= '</ul>';
    	/*ficheros cliente*/
    	echo $cadena;
	  ?>
                </fieldset>
                <?php
                if($info['estado']>0){
                   ?>
                <fieldset>
                    Concepto de la solicitud:<br/>
                    <?php echo $info['motivo']; ?>
                </fieldset>
                   <?php
                }
                ?>
            </div>
       </div>
       <?php  
    }
    
    public function desembolso($info=null){
      echo getMensaje();
        ?>
           <h2><?php  echo SOLICITUDES_DESEMBOLSO?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  SOLICITUDES_NOMBRE        =>setUrl('solicitudes'),
            		  SOLICITUDES_LISTA         =>setUrl('solicitudes','lst'),
            		  SOLICITUDES_DESEMBOLSO    =>'#',
                          SOLICITUDES_RECHAZO       =>setUrl('solicitudes','lstRechazadas'),
		          SOLICITUDES_DESEMBOLSOLST =>setUrl('solicitudes','lstDesembolsos'),
                          SOLICITUDES_CUPO          =>setUrl('solicitudes','cupo')
            		 ),
            	    SOLICITUDES_DESEMBOLSO
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  SOLICITUDES_DESEMBOLSO=>'#'
            		 ),
            	    SOLICITUDES_DESEMBOLSO, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo SOLICITUDES_DESEMBOLSO?></h4>
	  <div class='container'>
	    <?php echo datatable('lst')?>
	    <table class="table table-hover" id="lst">
		<thead>
		  <tr>
		    <th>#</th>
		    <th>Cliente</th>
		    <th>Producto</th>
		    <th>Monto</th>
		    <th>Estado</th>
		    <th>Fecha solicitud</th>
		    <th>Fecha aprobación</th>
		    <th>Dìa corte</th>
                    <th>Adjuntos</th>
		    <th>Subir adjuntos</th>
                    <th>Ver</th>
		    <th>Desembolsar</th>
		  </tr>
		</thead>
		<tbody>
		  <?php
			if(is_array($info)){
		    foreach ($info As $i):
		  ?>
		  <tr>
		    <td><?php echo $i['id']?></td>
		    <td><?php echo $i['nombre']?></td>
		    <td><?php echo $i['elpro']?></td>
		    <td><?php echo toMoney($i['monto'])?></td>
		    <td>
                    <?php
                        echo 'Aprobado';
                     ?>
		    </td>
                    <td><?php echo $i['fecha']?></td>
                    <td><?php echo $i['fecha2']?></td>
                    <td><?php echo $i['fecha_corte']?></td>
                    
                    <td>
		    <a href="<?php echo setUrl('solicitudes','archivos3',array('cliente'=>trim($i["cliente"]),'solicitud'=>$i["id"]))?>"><img src="app/img/download.png" alt="adjuntos" title="adjuntos"></a>
		    </td>
		    <td>
		    <a href="<?php echo setUrl('solicitudes','cargarDocs3',array('cliente'=>trim($i["cliente"]),'solicitud'=>$i["id"]))?>"><img src="app/img/folder.png" alt="adjuntos" title="adjuntos"></a>
		    </td>
                    
		    <td>
			<a href="<?php echo setUrl('solicitudes','ver2',array("id"=>$i["id"]))?>"><img src="app/img/ver.png" alt="ver" title="Ver"></a>
                    </td>
                    <td>
			<?php if($i['estado']=='3'){ ?>
			<a onclick="return confirm('Desea desembolsar este credito ?')" href="<?php echo setUrl('solicitudes','desembolsar',array("id"=>$i["id"],"estado"=>"4"))?>"><img src="app/img/use.png" alt="Desembolsar" title="Desembolsar"></a>
			<?php } ?>
		    </td>
		  </tr>
		  <?php
		    endforeach;
			}else{
			  echo 'No hay datos';
			}
		  ?>
		</tbody>
	    </table>
	  </div>
       <?php  
    }
    
    
        public function lstRechazadas($info=null){
      echo getMensaje();
        ?>
           <h2><?php  echo SOLICITUDES_DESEMBOLSO?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  SOLICITUDES_NOMBRE        =>setUrl('solicitudes'),
            		  SOLICITUDES_LISTA         =>setUrl('solicitudes','lst'),
            		  SOLICITUDES_DESEMBOLSO    =>setUrl('solicitudes','desembolso'),
                          SOLICITUDES_RECHAZO       =>'#',
		          SOLICITUDES_DESEMBOLSOLST =>setUrl('solicitudes','lstDesembolsos'),
                          SOLICITUDES_CUPO          =>setUrl('solicitudes','cupo')
            		 ),
            	    SOLICITUDES_RECHAZO
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  SOLICITUDES_RECHAZO=>'#'
            		 ),
            	    SOLICITUDES_RECHAZO, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo SOLICITUDES_RECHAZO?></h4>
	  <div class='container'>
	    <?php echo datatable('lst')?>
	    <table class="table table-hover" id="lst">
		<thead>
		  <tr>
		    <th>#</th>
		    <th>Cliente</th>
		    <th>Producto</th>
		    <th>Monto</th>
                    <th>Fecha Solicitud</th>
                    <th>Fecha rechazo</th>
		    <th>Estado</th>
                    <!--<th>Adjuntos</th>
		    <th>Subir adjuntos</th>-->
                    <th>Ver</th>
                    <?php if(getGrupoUsuario()=='1'){ ?>
		    <th>Eliminar</th>
                    <?php } ?>
		  </tr>
		</thead>
		<tbody>
		  <?php
			if(is_array($info)){
		    foreach ($info As $i):
		  ?>
		  <tr>
		    <td><?php echo $i['id']?></td>
		    <td><?php echo $i['nombre']?></td>
		    <td><?php echo $i['elpro']?></td>
		    <td><?php echo toMoney($i['monto'])?></td>
		    <td><?php echo ($i['fecha'])?></td>
		    <td><?php echo ($i['fecha2'])?></td>
		    <td>
                    <?php
                        echo 'Rechazado';
                     ?>
		    </td>
                    
                    <!--<td>
		    <a href="<?php echo setUrl('solicitudes','archivos3',array('cliente'=>trim($i["cliente"]),'solicitud'=>$i["id"]))?>"><img src="app/img/download.png" alt="adjuntos" title="adjuntos"></a>
		    </td>
		    <td>
		    <a href="<?php echo setUrl('solicitudes','cargarDocs3',array('cliente'=>trim($i["cliente"]),'solicitud'=>$i["id"]))?>"><img src="app/img/folder.png" alt="adjuntos" title="adjuntos"></a>
		    </td>-->
                    
		    <td>
			<a href="<?php echo setUrl('solicitudes','ver_re',array("id"=>$i["id"]))?>"><img src="app/img/ver.png" alt="ver" title="Ver"></a>
                    </td>
                    <td>
                    <?php if(getGrupoUsuario()=='1'){ ?>
                    <a onclick="return confirm('Desea eliminar esta solicitud ?')" href="<?php echo setUrl('solicitudes','delSolicitud',array("id"=>$i["id"]))?>"><img src="app/img/eliminar.png" alt="Eliminar" title="Eliminar"></a>
                    <?php } ?>
		    </td>
		  </tr>
		  <?php
		    endforeach;
			}else{
			  echo 'No hay datos';
			}
		  ?>
		</tbody>
	    </table>
	  </div>
       <?php  
    }
    
    
    public function editar(
                            $info,
                            $referencia,
                            $vinculo,
                            $nombrer,
                            $apellidor,
                            $ccr,
                            $correor,
                            $fechar,
                            $dirr,
                            $telr,
                            $celr,
                            $empresa,
                            $cargor,
                            $celr,
                            $tasa
                            ){
        
        echo getMensaje();
        ?>
           <h2><?php  echo SOLICITUDES_EDITAR?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  SOLICITUDES_LISTA=>setUrl('solicitudes','lst'),
            		  SOLICITUDES_EDITAR=>"#",
            		 ),
            	    SOLICITUDES_EDITAR
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  SOLICITUDES_EDITAR=>'#'
            		 ),
            	    SOLICITUDES_EDITAR, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo SOLICITUDES_EDITAR?></h4>
       <div class='container'>
	   <!--<div class="span7 offset0">-->
		<?php Html::openForm("editar_solicitudes",setUrl("solicitudes","editarProcess"));?>
		<!--<fieldset>
                    <legend>Informaci&oacute;n interna</legend>
                    <?php
                            echo selectZonas($info['idZona']);
                            echo '<div id="verSucur"></div>';
                            echo selectEmpleados($info['responsable'],'','responsable');
                            echo selectEmpleados($info['cartera'],'','cartera');
                            /*Html::newSelect("estado", "Estado *", array(
                                                                        "1"=>"En estudio",
                                                                        "2"=>"Estudio cerrado",
                                                                        "3"=>"Aprobado desembolsado",
                                                                        "4"=>"Rechazado",
                                                                        "5"=>"Aprobado no desembolsado"
                                                                        )
                                        );*/
                    ?>
                </fieldset>-->
		<fieldset>
		 <legend>Informaci&oacute;n solicitud</legend>
                    <?php
                    Html::newInput("lstClientes","Cliente*",$info['cliente'].' - '.$info['nombre'].' '.$info['apellido'],'readonly');
Html::newInput("lstCodeudores","Codeudor *",$info['codeudor'].' - '.$info['nombreCod'].' '.$info['apellidoCod'],'readonly');
                    Html::newInput("valor_solicitado","Valor solicitado *",$info['monto']);
                    Html::newSelect("amortizacion", "Amortizacion *", array(
                                                                        "1"=>"QUINCENAL",
                                                                        "2"=>"MENSUAL"
                                                                        ),$info['amortizacion']
                                        );
                    Html::newSelect("fecha_corte", "Fecha de corte *", array(
                                                                        "25"=>"25",
                                                                        "10"=>"10"
                                                                        ),$info["fecha_corte"]
                                        );
                    echo selectProductos($info['producto']);
                    ?>
                    <!--<div id="lcredito">
                    <div class="control-group">
                        <label for="lineaCredito" class="control-label">Producto * : </label>
                        <div class="controls" id="ver_plazos">
                                <select id="lineaCredito" name="lineaCredito">
                                  <option value=""> - Seleccione - </option>
                                </select>
                        </div>
                    </div>
                    <input type="hidden" id="producto" value="<?php echo $info['producto']?>">
                    </div>-->
                    
                    <div class="control-group">
                        <label for="plazo" class="control-label">Plazos * : </label>
                        <div class="controls" id="ver_plazos">
                            <input type="text" name="plazo" value="<?php echo $info['plazo'] ?>">
                        </div>
                    </div>
                 
                 <div class="control-group">
                        <label for="plazo" class="control-label">tasa * : </label>
                        <div class="controls">
                            <input type="text" name="latasa" value="<?php echo $tasa ?>">
                            <input type="hidden" name="latasa2" value="<?php echo $tasa ?>">
                        </div>
                    </div>
                 
                </fieldset>
                <fieldset>
                    <legend>Referencias</legend>
                    <?php
                        echo selectReferencia($referencia[0],'',"referencia[]");
                        echo selectVinculo($vinculo[0],'',"vinculo[]");
                        Html::newInput("nombrer[]","Nombre referencia *",$nombrer[0],'');
                        Html::newInput("apellidor[]","Apelldio referencia *",$apellidor[0],'');
                        Html::newInput("ccr[]","Documento referencia *",$ccr[0],'');
                        Html::newInput("correor[]","Correo referencia *",$correor[0],'');
                        echo inputDateMenores('fechar[]');
                        Html::newInput("fechar[]","Fecha nacimineto referencia *",$fechar[0],'');
                        Html::newInput("dirr[]","Direccion referencia *",$dirr[0],'');
                        Html::newInput("telr[]","Telefono referencia *",$telr[0],'');
                        Html::newInput("celr[]","Celular referencia *",$celr[0],'');
                        Html::newInput("empresar[]","Empresa donde labora referencia *",$empresa[0],'');
                        Html::newInput("cargor[]","Cargo referencia *",$cargor[0],'');
                        echo '<hr>';
                        echo selectReferencia($referencia[1],'',"referencia[]");
                        echo selectVinculo($vinculo[1],'',"vinculo[]");
                        Html::newInput("nombrer[]","Nombre referencia *",$nombrer[1],'');
                        Html::newInput("apellidor[]","Apelldio referencia *",$apellidor[1],'');
                        Html::newInput("ccr[]","Documento referencia *",$ccr[1],'');
                        Html::newInput("correor[]","Correo referencia *",$correor[1],'');
                        echo inputDateMenores('fechar[]');
                        Html::newInput("fechar[]","Fecha nacimineto referencia *",$fechar[1],'');
                        Html::newInput("dirr[]","Direccion referencia *",$dirr[1],'');
                        Html::newInput("telr[]","Telefono referencia *",$telr[1],'');
                        Html::newInput("celr[]","Celular referencia *",$celr[1],'');
                        Html::newInput("empresar[]","Empresa donde labora referencia *",$empresa[1],'');
                        Html::newInput("cargor[]","Cargo referencia *",$cargor[1],'');
                    ?>
                </fieldset>
		<?php
		  Html::newHidden("id",$info['elid']);
		  Html::newButton("editar", "Editar", "submit");
		  Html::closeForm();
		?>
        <!--</div>-->
       </div>
       <?php  
    }
    
    public function lstDesembolsos($info=null){
      echo getMensaje();
        ?>
           <h2><?php  echo SOLICITUDES_DESEMBOLSOLST?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  SOLICITUDES_NOMBRE        =>setUrl('solicitudes'),
            		  SOLICITUDES_LISTA         =>setUrl('solicitudes','lst'),
            		  SOLICITUDES_DESEMBOLSO    =>setUrl('solicitudes','desembolso'),
                          SOLICITUDES_RECHAZO       =>setUrl('solicitudes','lstRechazadas'),
            		  SOLICITUDES_DESEMBOLSOLST =>'#',
                          SOLICITUDES_CUPO          =>setUrl('solicitudes','cupo')
            		 ),
            	    SOLICITUDES_DESEMBOLSOLST
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  SOLICITUDES_DESEMBOLSOLST=>'#'
            		 ),
            	    SOLICITUDES_DESEMBOLSOLST, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo SOLICITUDES_DESEMBOLSOLST?></h4>
	  <div class='container'>
	    <?php echo datatable('lst')?>
	    <table class="table table-hover" id="lst">
		<thead>
		  <tr>
		    <th>Solicitud</th>
		    <th>Cliente</th>
		    <th>Producto</th>
		    <th>Dìa corte</th>
		    <th>Fecha solicitud</th>
		    <th>Fecha aprobaciòn</th>
		    <th>Fecha desembolso</th>
		    <th>Monto</th>
		    <th>Saldo total</th>
		    <th>Estado</th>
		    <!--<th>PDF</th>-->
                    <th>Plan de pagos</th>
                    <th>Adjuntos</th>
		    <th>Subir adjuntos</th>
		  </tr>
		</thead>
		<tbody>
		  <?php
		if(is_array($info)){
		    foreach ($info As $i):
		  ?>
		  <tr>
		    <td><?php echo $i['id_solicitud']?></td>
		    <td><?php echo $i['nombre'].' '.$i['apellido']?></td>
                    <td><?php echo ($i['elpro'])?></td>
		    <td><?php echo $i['fecha_corte']?></td>
		    <td><?php echo $i['fecha']?></td>
		    <td><?php echo $i['fecha2']?></td>
		    <td><?php echo $i['fecha_desembolso']?></td>
		    <td><?php echo toMoney($i['monto'])?></td>
                    <td> 
                    <?php
                    include_once 'app/modulos/cajas/modelo/m.Cajas.php';
                    
                    $info2 = mCajas::corte($i['id_solicitud']);

                    $total =   0;

                    $fecha = opDate();
                    //$fecha = $fecha[2].'-'.$fecha[1].'-'.$fecha[0];
                    $otros2 = 0;
                    $otros = unserialize($info2[0]['otros']);
                    if(is_array($otros)){
                        foreach ($otros As $ot){
                            $otros2 += $ot;
                        }
                    }

    $total = (
            $info2[0]['ajuste_interes']+
            $info2[0]['interes']+
            $info2[0]['saldo_capital']+
            $info2[0]['mora']+
            $otros2
            );
    
                    echo toMoney($total);
                    ?>
                    </td>
		    <td><?php echo ($i['estado']>'4')?'Pagado':'Desembolsado'?></td>
		    <!--<td>
			<a href="<?php echo setUrl('solicitudes','lstDesembolsos',array('id'=>$i['id_solicitud']))?>"><img src="app/img/pdf.png" alt="Pdf" title="Pdf"></a>
		    </td>-->
		    <td>
                        <a target="_blanck" href="<?php echo setUrl('solicitudes','planpagos',array('id'=>$i['id_solicitud'],'jquery'=>1))?>">
                            <img src="app/img/pdf.png">
                        </a>
		    </td>
                    <!--<td>
                        <a target="_blanck" href="<?php echo setUrl('solicitudes','ecuenta',array('id'=>$i['id_solicitud'],'jquery'=>1))?>">
                            <img src="app/img/pdf.png">
                        </a>
		    </td>-->
                    <td>
		    <a href="<?php echo setUrl('solicitudes','archivos2',array('cliente'=>trim($i["cliente"]),'solicitud'=>$i["id_solicitud"]))?>"><img src="app/img/download.png" alt="adjuntos" title="adjuntos"></a>
		    </td>
		    <td>
		    <a href="<?php echo setUrl('solicitudes','cargarDocs2',array('cliente'=>trim($i["cliente"]),'solicitud'=>$i["id_solicitud"]))?>"><img src="app/img/folder.png" alt="adjuntos" title="adjuntos"></a>
		    </td>
		  </tr>
		  <?php
		    endforeach;
			}else{
			  echo 'No hay datos';
			}
		  ?>
		</tbody>
	    </table>
	  </div>
       <?php  
    }
    
    public function archivos($ficheros=null,$cc=null,$id=null,$ficheros2=null){
      echo getMensaje();
        ?>
           <h2><?php  echo SOLICITUDES_VER?></h2>

            <?php 
            navTabs(array('Regresar'=>setUrl('solicitudes','lst'), 
            		  SOLICITUDES_DOC=>'#'
            		 ),
            	    SOLICITUDES_DOC
            	   );
            
            navTabs(array('Regresar'=>setUrl('solicitudes','lst'), 
            		  SOLICITUDES_DOC=>'#'
            		 ),
            	    SOLICITUDES_DOC, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo SOLICITUDES_DOC?></h4>
       <div class='container'>
        <div class='span10 offset1'> 
	  <?php
	$i=0;
    	$val=0;
    	$cadena ='';
    	$cadena .= '<ul class="nav nav-tabs">';
    	foreach($ficheros As $f){
    		if($i>1){
	    		if($f){
	    			$val++;
	    			$ext = explode('.',$f);
	    			if($ext[1]=='pdf'){
					$img = $ext[1].'.png';
	    			}else if($ext[1]=='jpg' || $ext[1]=='png' || $ext[1]=='gif' || $ext[1]=='tif'){
	    				$img = 'imagen.png';
	    			}else{
	   				$img = 'file.png';
	    			}
	    			$cadena .='<li>
	    				<a target="_blank" href="app/files/clientes/'.$cc.'/solicitudes/'.$id.'/'.$f.'">
	    					<img src="app/img/'.$img.'" height="22" width="22"><br/>'.$f.'
	    				</a>
	    				<a title="Eliminar" onclick="return confirm(\'Desea eliminar este archivo '.$f.'\')" href="'.setUrl('solicitudes','eliminarDoc',array('id'=>$id,'cc'=>$cc,'file'=>$f,'folder'=>$id)).'">Eliminar <img src="app/img/eliminar_min.png" alt="Eliminar"></a>
	    			      <li/>';
    			      }else{
    			      	$cadena .= 'No hay datos';
    			      }
    		}
    		$i++;
    	}
    	$cadena .= '</ul>';
    	/*ficheros cliente*/
    		$i=0;
    	$val=0;
    	$cadena .= '<ul class="nav nav-tabs">';
    	foreach($ficheros2 As $f2){
    		if($i>1){
	    		if($f2){
	    			$val++;
	    			$ext = explode('.',$f2);
	    			if($ext[1]=='pdf'){
					$img = $ext[1].'.png';
	    			}else if($ext[1]=='jpg' || $ext[1]=='png' || $ext[1]=='gif' || $ext[1]=='tif'){
	    				$img = 'imagen.png';
	    			}else{
	    			//die('entra');
	   				$img = 'file.png';
	    			}
	    			$cadena .='<li>
	    				<a target="_blank" href="app/files/clientes/'.$cc.'/'.$f2.'">
	    					<img src="app/img/'.$img.'" height="22" width="22"><br/>'.$f2.'
	    				</a>
	    				<a title="Eliminar" onclick="return confirm(\'Desea eliminar este archivo '.$f.'\')" href="'.setUrl('solicitudes','eliminarDoc',array('id'=>$id,'cc'=>$cc,'file'=>$f,'folder'=>$id)).'">Eliminar <img src="app/img/eliminar_min.png" alt="Eliminar"></a>
	    			      <li/>';
    			      }else{
    			      	$cadena .= 'No hay datos';
    			      }
    		}
    		$i++;
    	}
    	$cadena .= '</ul>';
    	echo $cadena;
	  ?>
        </div>
       </div>
       <?php  
    }
    
    
        public function archivos2($ficheros=null,$cc=null,$id=null,$ficheros2=null){
      echo getMensaje();
        ?>
           <h2><?php  echo SOLICITUDES_VER?></h2>

            <?php 
            navTabs(array('Regresar'=>setUrl('solicitudes','lstDesembolsos'), 
            		  SOLICITUDES_DOC=>'#'
            		 ),
            	    SOLICITUDES_DOC
            	   );
            
            navTabs(array('Regresar'=>setUrl('solicitudes','lstDesembolsos'), 
            		  SOLICITUDES_DOC=>'#'
            		 ),
            	    SOLICITUDES_DOC, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo SOLICITUDES_DOC?></h4>
       <div class='container'>
        <div class='span10 offset1'> 
	  <?php
	$i=0;
    	$val=0;
    	$cadena ='';
    	$cadena .= '<ul class="nav nav-tabs">';
    	foreach($ficheros As $f){
    		if($i>1){
	    		if($f){
	    			$val++;
	    			$ext = explode('.',$f);
	    			if($ext[1]=='pdf'){
					$img = $ext[1].'.png';
	    			}else if($ext[1]=='jpg' || $ext[1]=='png' || $ext[1]=='gif' || $ext[1]=='tif'){
	    				$img = 'imagen.png';
	    			}else{
	   				$img = 'file.png';
	    			}
	    			$cadena .='<li>
	    				<a target="_blank" href="app/files/clientes/'.$cc.'/solicitudes/'.$id.'/'.$f.'">
	    					<img src="app/img/'.$img.'" height="22" width="22"><br/>'.$f.'
	    				</a>
	    				<a title="Eliminar" onclick="return confirm(\'Desea eliminar este archivo '.$f.'\')" href="'.setUrl('solicitudes','eliminarDoc2',array('id'=>$id,'cc'=>$cc,'file'=>$f,'folder'=>$id)).'">Eliminar <img src="app/img/eliminar_min.png" alt="Eliminar"></a>
	    			      <li/>';
    			      }else{
    			      	$cadena .= 'No hay datos';
    			      }
    		}
    		$i++;
    	}
    	$cadena .= '</ul>';
    	/*ficheros cliente*/
    		$i=0;
    	$val=0;
    	$cadena .= '<ul class="nav nav-tabs">';
    	foreach($ficheros2 As $f2){
    		if($i>1){
	    		if($f2){
	    			$val++;
	    			$ext = explode('.',$f2);
	    			if($ext[1]=='pdf'){
					$img = $ext[1].'.png';
	    			}else if($ext[1]=='jpg' || $ext[1]=='png' || $ext[1]=='gif' || $ext[1]=='tif'){
	    				$img = 'imagen.png';
	    			}else{
	    			//die('entra');
	   				$img = 'file.png';
	    			}
	    			$cadena .='<li>
	    				<a target="_blank" href="app/files/clientes/'.$cc.'/'.$f2.'">
	    					<img src="app/img/'.$img.'" height="22" width="22"><br/>'.$f2.'
	    				</a>
	    				<a title="Eliminar" onclick="return confirm(\'Desea eliminar este archivo '.$f.'\')" href="'.setUrl('solicitudes','eliminarDoc2',array('id'=>$id,'cc'=>$cc,'file'=>$f,'folder'=>$id)).'">Eliminar <img src="app/img/eliminar_min.png" alt="Eliminar"></a>
	    			      <li/>';
    			      }else{
    			      	$cadena .= 'No hay datos';
    			      }
    		}
    		$i++;
    	}
    	$cadena .= '</ul>';
    	echo $cadena;
	  ?>
        </div>
       </div>
       <?php  
    }
    
    
            public function archivos3($ficheros=null,$cc=null,$id=null,$ficheros2=null){
      echo getMensaje();
        ?>
           <h2><?php  echo SOLICITUDES_VER?></h2>

            <?php 
            navTabs(array('Regresar'=>setUrl('solicitudes','desembolso'), 
            		  SOLICITUDES_DOC=>'#'
            		 ),
            	    SOLICITUDES_DOC
            	   );
            
            navTabs(array('Regresar'=>setUrl('solicitudes','desembolso'), 
            		  SOLICITUDES_DOC=>'#'
            		 ),
            	    SOLICITUDES_DOC, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo SOLICITUDES_DOC?></h4>
       <div class='container'>
        <div class='span10 offset1'> 
	  <?php
	$i=0;
    	$val=0;
    	$cadena ='';
    	$cadena .= '<ul class="nav nav-tabs">';
    	foreach($ficheros As $f){
    		if($i>1){
	    		if($f){
	    			$val++;
	    			$ext = explode('.',$f);
	    			if($ext[1]=='pdf'){
					$img = $ext[1].'.png';
	    			}else if($ext[1]=='jpg' || $ext[1]=='png' || $ext[1]=='gif' || $ext[1]=='tif'){
	    				$img = 'imagen.png';
	    			}else{
	   				$img = 'file.png';
	    			}
	    			$cadena .='<li>
	    				<a target="_blank" href="app/files/clientes/'.$cc.'/solicitudes/'.$id.'/'.$f.'">
	    					<img src="app/img/'.$img.'" height="22" width="22"><br/>'.$f.'
	    				</a>
	    				<a title="Eliminar" onclick="return confirm(\'Desea eliminar este archivo '.$f.'\')" href="'.setUrl('solicitudes','eliminarDoc3',array('id'=>$id,'cc'=>$cc,'file'=>$f,'folder'=>$id)).'">Eliminar <img src="app/img/eliminar_min.png" alt="Eliminar"></a>
	    			      <li/>';
    			      }else{
    			      	$cadena .= 'No hay datos';
    			      }
    		}
    		$i++;
    	}
    	$cadena .= '</ul>';
    	/*ficheros cliente*/
    		$i=0;
    	$val=0;
    	$cadena .= '<ul class="nav nav-tabs">';
    	foreach($ficheros2 As $f2){
    		if($i>1){
	    		if($f2){
	    			$val++;
	    			$ext = explode('.',$f2);
	    			if($ext[1]=='pdf'){
					$img = $ext[1].'.png';
	    			}else if($ext[1]=='jpg' || $ext[1]=='png' || $ext[1]=='gif' || $ext[1]=='tif'){
	    				$img = 'imagen.png';
	    			}else{
	    			//die('entra');
	   				$img = 'file.png';
	    			}
	    			$cadena .='<li>
	    				<a target="_blank" href="app/files/clientes/'.$cc.'/'.$f2.'">
	    					<img src="app/img/'.$img.'" height="22" width="22"><br/>'.$f2.'
	    				</a>
	    				<a title="Eliminar" onclick="return confirm(\'Desea eliminar este archivo '.$f.'\')" href="'.setUrl('solicitudes','eliminarDoc3',array('id'=>$id,'cc'=>$cc,'file'=>$f,'folder'=>$id)).'">Eliminar <img src="app/img/eliminar_min.png" alt="Eliminar"></a>
	    			      <li/>';
    			      }else{
    			      	$cadena .= 'No hay datos';
    			      }
    		}
    		$i++;
    	}
    	$cadena .= '</ul>';
    	echo $cadena;
	  ?>
        </div>
       </div>
       <?php  
    }


    public function archivos_cupo($ficheros=null,$cc=null,$id=null,$ficheros2=null){
      echo getMensaje();
        ?>
           <h2><?php  echo SOLICITUDES_VER?></h2>

            <?php 
            navTabs(array('Regresar'=>setUrl('solicitudes','cupo'), 
            		  SOLICITUDES_DOC=>'#'
            		 ),
            	    SOLICITUDES_DOC
            	   );
            
            navTabs(array('Regresar'=>setUrl('solicitudes','cupo'), 
            		  SOLICITUDES_DOC=>'#'
            		 ),
            	    SOLICITUDES_DOC, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo SOLICITUDES_DOC?></h4>
       <div class='container'>
        <div class='span10 offset1'> 
	  <?php
	$i=0;
    	$val=0;
    	$cadena ='';
    	$cadena .= '<ul class="nav nav-tabs">';
    	foreach($ficheros As $f){
    		if($i>1){
	    		if($f){
	    			$val++;
	    			$ext = explode('.',$f);
	    			if($ext[1]=='pdf'){
					$img = $ext[1].'.png';
	    			}else if($ext[1]=='jpg' || $ext[1]=='png' || $ext[1]=='gif' || $ext[1]=='tif'){
	    				$img = 'imagen.png';
	    			}else{
	   				$img = 'file.png';
	    			}
	    			$cadena .='<li>
	    				<a target="_blank" href="app/files/clientes/'.$cc.'/solicitudes/'.$id.'/'.$f.'">
	    					<img src="app/img/'.$img.'" height="22" width="22"><br/>'.$f.'
	    				</a>
	    				<!--<a title="Eliminar" onclick="return confirm(\'Desea eliminar este archivo '.$f.'\')" href="'.setUrl('solicitudes','eliminarDoc',array('id'=>$id,'cc'=>$cc,'file'=>$f,'folder'=>$id)).'">Eliminar <img src="app/img/eliminar_min.png" alt="Eliminar"></a>-->
	    			      <li/>';
    			      }else{
    			      	$cadena .= 'No hay datos';
    			      }
    		}
    		$i++;
    	}
    	$cadena .= '</ul>';
    	/*ficheros cliente*/
    		$i=0;
    	$val=0;
    	$cadena .= '<ul class="nav nav-tabs">';
    	foreach($ficheros2 As $f2){
    		if($i>1){
	    		if($f2){
	    			$val++;
	    			$ext = explode('.',$f2);
	    			if($ext[1]=='pdf'){
					$img = $ext[1].'.png';
	    			}else if($ext[1]=='jpg' || $ext[1]=='png' || $ext[1]=='gif' || $ext[1]=='tif'){
	    				$img = 'imagen.png';
	    			}else{
	    			//die('entra');
	   				$img = 'file.png';
	    			}
	    			$cadena .='<li>
	    				<a target="_blank" href="app/files/clientes/'.$cc.'/'.$f2.'">
	    					<img src="app/img/'.$img.'" height="22" width="22"><br/>'.$f2.'
	    				</a>
	    				<!--<a title="Eliminar" onclick="return confirm(\'Desea eliminar este archivo '.$f.'\')" href="'.setUrl('solicitudes','eliminarDoc',array('id'=>$id,'cc'=>$cc,'file'=>$f,'folder'=>$id)).'">Eliminar <img src="app/img/eliminar_min.png" alt="Eliminar"></a>-->
	    			      <li/>';
    			      }else{
    			      	$cadena .= 'No hay datos';
    			      }
    		}
    		$i++;
    	}
    	$cadena .= '</ul>';
    	echo $cadena;
	  ?>
        </div>
       </div>
       <?php  
    }
    
    
    public function cargarDocs($cliente,$solicitud){
     echo getMensaje();
        ?>
           <h2><?php  echo SOLICITUDES_DOC?></h2>

            <?php 
            navTabs(array('Regresar'=>setUrl('solicitudes','lst'), 
            		  SOLICITUDES_DOC=>'#'
            		 ),
            	    SOLICITUDES_DOC
            	   );
            
            navTabs(array('Regresar'=>setUrl('solicitudes','lst'), 
            		  SOLICITUDES_DOC=>'#'
            		 ),
            	    SOLICITUDES_DOC, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo SOLICITUDES_DOC?></h4>
       <div class='container'>
        <div class='span10 offset1'> 
	 <?php Html::openForm("solicitudes",setUrl("solicitudes","cargarDocPro"));?>
	  	
            <fieldset>
                <div class="control-group" id="adj">
                    <label for="adjuntos1" class="control-label">Adjuntos : </label>
                    <div class="controls" id="nuevo">
                        <input type="file" name="adjuntos1[]" id="adjuntos1">
                        <input type="text" name="nadjunto[]" id="nadjunto" placeholder="Nombre archivo">
                        <a class="btn clonar" onclick="clonar(this)">+</a>
                        <a class="btn remover" onclick="remover(this)">-</a>
                    </div>
                </div>
            </fieldset>
            <?php
              Html::newButton("enviar", "Enviar", "submit");
              Html::newHidden("cliente",$cliente);
              Html::newHidden("solicitud",$solicitud);
              Html::newHidden("url","lst");
              Html::closeForm();
            ?>
        </div>
       </div>
       <?php  

    }
    
        public function cargarDocs2($cliente,$solicitud){
     echo getMensaje();
        ?>
           <h2><?php  echo SOLICITUDES_DOC?></h2>

            <?php 
            navTabs(array('Regresar'=>setUrl('solicitudes','lstDesembolsos'), 
            		  SOLICITUDES_DOC=>'#'
            		 ),
            	    SOLICITUDES_DOC
            	   );
            
            navTabs(array('Regresar'=>setUrl('solicitudes','lstDesembolsos'), 
            		  SOLICITUDES_DOC=>'#'
            		 ),
            	    SOLICITUDES_DOC, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo SOLICITUDES_DOC?></h4>
       <div class='container'>
        <div class='span10 offset1'> 
	 <?php Html::openForm("solicitudes",setUrl("solicitudes","cargarDocPro"));?>
	  	
            <fieldset>
                <div class="control-group" id="adj">
                    <label for="adjuntos1" class="control-label">Adjuntos : </label>
                    <div class="controls" id="nuevo">
                        <input type="file" name="adjuntos1[]" id="adjuntos1">
                        <input type="text" name="nadjunto[]" id="nadjunto" placeholder="Nombre archivo">
                        <a class="btn clonar" onclick="clonar(this)">+</a>
                        <a class="btn remover" onclick="remover(this)">-</a>
                    </div>
                </div>
            </fieldset>
            <?php
              Html::newButton("enviar", "Enviar", "submit");
              Html::newHidden("cliente",$cliente);
              Html::newHidden("solicitud",$solicitud);
              Html::newHidden("url","lstDesembolsos");
              Html::closeForm();
            ?>
        </div>
       </div>
       <?php  

    }
    
            public function cargarDocs3($cliente,$solicitud){
     echo getMensaje();
        ?>
           <h2><?php  echo SOLICITUDES_DOC?></h2>

            <?php 
            navTabs(array('Regresar'=>setUrl('solicitudes','desembolso'), 
            		  SOLICITUDES_DOC=>'#'
            		 ),
            	    SOLICITUDES_DOC
            	   );
            
            navTabs(array('Regresar'=>setUrl('solicitudes','desembolso'), 
            		  SOLICITUDES_DOC=>'#'
            		 ),
            	    SOLICITUDES_DOC, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo SOLICITUDES_DOC?></h4>
       <div class='container'>
        <div class='span10 offset1'> 
	 <?php Html::openForm("solicitudes",setUrl("solicitudes","cargarDocPro"));?>
	  	
            <fieldset>
                <div class="control-group" id="adj">
                    <label for="adjuntos1" class="control-label">Adjuntos : </label>
                    <div class="controls" id="nuevo">
                        <input type="file" name="adjuntos1[]" id="adjuntos1">
                        <input type="text" name="nadjunto[]" id="nadjunto" placeholder="Nombre archivo">
                        <a class="btn clonar" onclick="clonar(this)">+</a>
                        <a class="btn remover" onclick="remover(this)">-</a>
                    </div>
                </div>
            </fieldset>
            <?php
              Html::newButton("enviar", "Enviar", "submit");
              Html::newHidden("cliente",$cliente);
              Html::newHidden("solicitud",$solicitud);
              Html::newHidden("url","desembolso");
              Html::closeForm();
            ?>
        </div>
       </div>
       <?php  

    }
    
            public function cargarDocs_cupo($cliente,$solicitud){
     echo getMensaje();
        ?>
           <h2><?php  echo SOLICITUDES_DOC?></h2>

            <?php 
            navTabs(array('Regresar'=>setUrl('solicitudes','cupo'), 
            		  SOLICITUDES_DOC=>'#'
            		 ),
            	    SOLICITUDES_DOC
            	   );
            
            navTabs(array('Regresar'=>setUrl('solicitudes','cupo'), 
            		  SOLICITUDES_DOC=>'#'
            		 ),
            	    SOLICITUDES_DOC, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo SOLICITUDES_DOC?></h4>
       <div class='container'>
        <div class='span10 offset1'> 
	 <?php Html::openForm("solicitudes",setUrl("solicitudes","cargarDocPro2"));?>
	  	
            <fieldset>
                <div class="control-group" id="adj">
                    <label for="adjuntos1" class="control-label">Adjuntos : </label>
                    <div class="controls" id="nuevo">
                        <input type="file" name="adjuntos1[]" id="adjuntos1">
                        <input type="text" name="nadjunto[]" id="nadjunto" placeholder="Nombre archivo">
                        <a class="btn clonar" onclick="clonar(this)">+</a>
                        <a class="btn remover" onclick="remover(this)">-</a>
                    </div>
                </div>
            </fieldset>
            <?php
              Html::newButton("enviar", "Enviar", "submit");
              Html::newHidden("cliente",$cliente);
              Html::newHidden("solicitud",$solicitud);
              Html::newHidden("url","cupo");
              Html::closeForm();
            ?>
        </div>
       </div>
       <?php  

    }
    
    public function planPagos($info=null,$info2=null,$id){
      //echo getMensaje();
       // print_r($info2);
        //print_r($info);
       //die();
       $producto = explode("-",$info2[0]['producto']);
       $producto = trim($producto[0]);
       
    $cadena='
                  <html>
                  <head>
                  <meta http-equiv="Content-Type" content="text/html">

                  <title>Plan de pagos</title>
                  <base href="http://serprogreso.com/serprogreso/">
                  <style type="text/css">
                      body{
                          font-size: 12px;
                          font-family: "Helvetica";
                          margin-right: 30px;
                          margin-bottom: 20px;
                          margin-left: 30px;
                          text-align: justify;
                      }
                      table{
                      width: 100%;
                      }
                      thead tr td {
                            font-size: 10px;
                            background: #ccc;
                            font-weight: bold;
                            text-align: center;
                        }
                        tbody tr td {
                            /*padding: 1px;*/
                            font-size: 12px;
                            text-align: center;
                        }
                      .box {
                        display: block;
                        float: left;
                        font-size: 12px;
                        margin: 1px;
                        
                        position: relative;
                        width: 100%;
                        padding: 2px;
                        }
                      .box #logo{float:left;width: 280px;}
                      .box #principal{
                      float:right;
                      font-size: 12px;
                      }
                      
                  </style>
                  </head>
          <body>
            <div class="box">
                <img src="app/img/logo.png" id="logo"><br/><br/>

                <div id="principal">
                <p>'.opDate(true,2).'<br /><br />
                    Oficina '.getSucursal2().'-'.getNombreSucursal().'
                </div>
            </div>
    <div class="box">
    NIT: 900.649.069-9
    <div id="principal">
        REPORTE DE CR&Eacute;DITO<br/>
        Cr&eacute;dito No. '.getSucursal2().'-'.cerosNum($producto).'-'.cerosNum($id).' - '.nombreProducto($producto).'
    </div>
    </div>';
    
$cadena .= '
<div class="box">
    Se&ntilde;or(es):<br/>
     '.$info2[0]['nombre'].'<br/><br/>
     Ciudad<br/><br/>
     Estimado Sr(a):<br/>
     Nos permitimos informarle que el '.$info2[0]['fecha_desembolso'].' a las '.$info2[0]['hora'].'
     hemos aprobado y desembolsado un cr&eacute;dito, el
     cual contiene las siguientes caracter&iacute;sticas:
</div>
       
<div class="box">
    <table border="1">
        <tr>
            <td>Nombre:</td>
            <td>'.$info2[0]['nombre'].'</td>
        </tr>  
        <tr>
            <td>N&uacute;mero de identificaci&oacute;n:</td>
            <td>'.$info2[0]['cc'].'</td>
        </tr>  
        <tr>
            <td>Cr&eacute;dito No:</td>
            <td>'.getSucursal2().'-'.cerosNum($producto).'-'.cerosNum($id).'</td>
        </tr>  
        <tr>
            <td>Valor aprobado:</td>
            <td>'.toMoney($info2[0]['monto']).'</td>
        </tr>  
        <tr>
            <td>Fecha de corte:</td>
            <td>'.$info2[0]['fecha_corte'].'</td>
        </tr>  
        <tr>
            <td>Plazo (En Meses):</td>
            <td>'.$info2[0]['plazo'].'</td>
        </tr>  
        <tr>
            <td>Valor cuota:</td>
            <td>'.toMoney($info[0]['cuotaFija']).'</td>
        </tr>  
    </table>
    <br/>
    <b>Nota:</b> Cuando se vaya a realizar el pago de cada cuota en el banco, se debe poner el No.'.getSucursal2().'-'.cerosNum($producto).'-'.cerosNum($id).' en el campo
Referencia del formato que el banco le suministre.
</div>
           <div class="box">
            <table border="1">
                <thead>
                    <tr><td colspan="3">
                            Liquidaci&oacute;n desembolso
                    </td></tr>
                    <tr>
                        <td>CONCEPTO</td>
                        <td>VALOR</td>
                        <td>I.V.A</td>
                    </tr>
                </thead>
                <tbody>';

                    if(is_array($info2)){
                    $j=1;
                    foreach ($info2 As $i2):
                    $nombre = unserialize($i2['otros']);
                    $iva = unserialize($i2['otros_iva']);
                    $des = unserialize($i2['otros_des']);
                    /*print_r($nombre);
                    print_r($des);
                    die();*/
                    if(is_array($nombre)){
                        $k=0;
                        $monto = 0;
                        $teliva = 0;
                        
                        $cadena .= '<tr>
                        <td>Valor Solicitado</td>
                        <td>'.toMoney($info2[0]['monto']).'</td>
                        <td>'.toMoney(0).'</td>
                        </tr>';
                        
                        foreach ($nombre As $key=>$o):
                            $cadena .= '<tr>';
                            if($des[$k]=="2"){
                                //echo '<tr>';
                                $cadena .= '<td>'.$key.'</td>';
                                if($key=="GMF"){
                                    $cadena .= '<td>'.toMoney($o).'</td>';
                                    $eliva = ($iva[$k]=="2")?(($o*($info2[0]['monto']/100))*0.16):0;
                                    $monto += $o;
                                }else{
                                    $cadena .= '<td>'.toMoney($o).'</td>';
                                    $eliva = ($iva[$k]=="2")?($o*0.16):0;
                                    $monto += $o;
                                }
                                
                                $cadena .= '<td>'.toMoney($eliva).'</td>';
                                $teliva += $eliva;
                                //echo '<tr>';
                            }
                            $cadena .= '<tr>';
                            $k++;
                        endforeach;
                        $cadena .= '<tr><td>Total descuentos</td><td>'.toMoney($monto).'</td><td>'.toMoney($teliva).'</td></tr>';
                        $cadena .= '<tr><td>Total neto a desembolsar</td><td>'.toMoney($info2[0]['monto']-($monto+$teliva)).'</td></tr>';
                    }
                    
                  $j++;
                  endforeach;
                }else{
                    $cadena .= 'No hay datos';
                }
                
            $cadena .= '</tbody>
            </table>
           </div>
       
       <div class="box">
           Plan de pagos : <br/>
	    <table border="1">
		<thead>
		  <tr>
                    <td>Cuota</td>
		    <td>Fecha corte</td>
		    <td>Fecha pago</td>
		    <td>Cuota fija</td>
		    <td>Saldo a capital</td>
		    <td>Interes corriente</td>
		    <td>Interes dias</td>
		    <td>Aporte a capital</td>';

                    if(is_array($info)){
                        $otros = unserialize($info2[0]['otros']);
                        $des = unserialize($info2[0]['otros_des']);
                        //print_r($info2);
                        //print_r($des);
                        //die();
                        if(is_array($otros)){
                            $i=0;
                            foreach ($otros As $key=>$o):
                                if($des[$i]=="1"){
                                    $cadena .= '<td>'.htmlentities(ucwords($key)).'</td>';
                                }
                                $i++;
                            endforeach;
                        }
                    }
                $cadena .= '
                    <td>Total cuota</td>
		  </tr>
		</thead>
		<tbody>';
			if(is_array($info)){
                            $j=1;
		    foreach ($info As $i):
                  $cadena .= '
		  <tr>
                    <td>'.$j.'</td>
		    <td>'.$i['fechaCorte'].'</td>
		    <td>'.$i['fechaPago'].'</td>
		    <td>'.toMoney($i['cuotaFija']).'</td>
		    <td>'.toMoney($i['capital']).'</td>
		    <td>'.toMoney($i['interes']).'</td>
		    <td>'.toMoney($i['interes_dia']).'</td>
		    <td>'.toMoney($i['aporteCapital']).'</td>';

                    $otros = unserialize($info2[0]['otros']);
                    $des = unserialize($info2[0]['otros_des']);
                    
                    if(is_array($otros)){
                         $l=0;
                        foreach ($otros As $o):
                            if($des[$l]=="1"){
                                $cadena .= '<td>'.toMoney($o*($i['elcapital']/100)).'</td>';
                                //$cadena .= '<td>'.toMoney($o*($i['elcapital']/100)).'</td>';
                            }
                            $l++;
                        endforeach;
                    }

		    $cadena .= '<td>'.toMoney($i['cuotaFija']+$i['otros_total']+$i['interes_dia']).'</td>';

                    $j++;
		    endforeach;
			}else{
			  $cadena .= 'No hay datos';
			}
                  $cadena .='
		</tbody>
	    </table>
           </div>';
       
       $cadena .= '<div class="box"><p><b><center>Calle 6 Norte # 2N - 36 Centro Comercial Campanario. Oficina 104. Tel: (2) 370 98 88
    servicioalcliente@serprogreso.com</center></b></p></div>
		    </body>
		    </html>';
    echo ($cadena);
    //pdf($cadena,'app/files/clientes/'.$info2[0]['cc'].'/cartadesembolso'.$info2[0]['id']);
    //return location('descargar.php?file=app/files/clientes/'.$info2[0]['cc'].'/cartadesembolso'.$info2[0]['id'].'.pdf');
    }
    
        public function ecuenta($info=null,$info2=null){
      //echo getMensaje();
       // print_r($info2);
        //print_r($info);
       //die();
       $producto = explode("-",$info2[0]['producto']);
       $producto = trim($producto[0]);
       $info = $info[0];
       
    $cadena='
                  <html>
                  <head>
                  <meta http-equiv="Content-Type" content="text/html">

                  <title>Estado de cuenta</title>
                  <base href="http://serprogreso.com/serprogreso/">
                  <style type="text/css">
                      body{
                          font: 12px arial, sans-serif;
                          margin: 15px;
                          text-align: justify;
                      }
                      table{
                        font-size: 12px;
                        width: 100%;
                      }
                      thead tr td {
                            background: #ccc;
                            font-weight: bold;
                            text-align: center;
                        }
                        tbody tr td {
                            padding: 2px;
                        }
                      .box {
                        display: block;
                        float: left;
                        font-size: 21px;
                        margin: 1px;
                        position: relative;
                        width: auto;
                        padding: 2px;
                        }
                        .cabeza{
                        float: right;
                        }
                        .cabeza2{
                        float: right;
                        width: 35%;
                        }
                        .cuadro{
                        border-radius: 20px;
                        border: 2px solid #000000;
                        float: left;
                        padding: 8px;
                        width: 15%;
                        margin: 10px;
                        }
                  </style>
                  </head>
          <body>
          <header>
        <p class="cabeza">'.opDate(true,2).'<br/>
        Oficina '.getSucursal2().'-'.getNombreSucursal().'    
        </p>
          
    <nav>
    <img src="app/img/logo.png" id="logo" width="200px"><br/>
    NIT: 900.649.069-9<br/><br/>
    
    <p class="cabeza">
    Estado de cuenta<br/>
    Cr&eacute;dito No. '.getSucursal2().'-'.cerosNum($producto).'-'.cerosNum($info2[0]['id']).' - '.nombreProducto($producto).'<br/>
    Fecha de corte : '.$info['fechaCorte'].'
    </p>
    
    <div class="cuadro">
     <b>'.$info2[0]['nombre'].'</b><br/>
    '.$info2[0]['dir'].'<br/>
    Cali - Valle
     </div>
     
     <br/><br/>
     <p class="cabeza2">
        P&aacute;guese antes de '.$info['fechaPago'].'<br/>
        Valor a pagar : '.toMoney($info['cuotaFija']+$info['interes_dia']).'<br/>
        Valor cuotas vencias : '.toMoney(0).'
    </p>
        </nav>
    </header>

<section>
   
    <table border="1">
        <tr>
            <td>Periodo liquidado:</td>
            <td>'.date($info['fechaCorte'],strtotime('-30 day')).' - '.$info['fechaCorte'].'</td>
        </tr>  
        <tr>
            <td>N&uacute;mero de identificaci&oacute;n:</td>
            <td>'.$info2[0]['cc'].'</td>
        </tr>  
        <tr>
            <td>Plazo en meses:</td>
            <td>'.$info2[0]['plazo'].'</td>
        </tr>  
        <tr>
            <td>Valor aprobado:</td>
            <td>'.toMoney($info2[0]['monto']).'</td>
        </tr>  
        <tr>
            <td>Fecha de corte:</td>
            <td>'.$info2[0]['fecha_corte'].'</td>
        </tr>  
        <tr>
            <td>Plazo (En Meses):</td>
            <td>'.$info2[0]['plazo'].'</td>
        </tr>  
        <tr>
            <td>Valor couta:</td>
            <td>'.toMoney($info[0]['cuotaFija']).'</td>
        </tr>  
    </table>
    <br/>
    <b>Nota:</b> Cuando se vaya a realizar el pago de cada cuota en el banco, se debe poner el No.'.cerosNum($info2[0]['id']).' en el campo
Referencia del formato que el banco le suministre.

            <table border="1">
                <thead>
                    <tr><td colspan="3">
                            Liquidaci&oacute;n desembolso
                    </td></tr>
                    <tr>
                        <td>CONCEPTO</td>
                        <td>VALOR</td>
                        <td>I.V.A</td>
                    </tr>
                </thead>
                <tbody>';

                    if(is_array($info2)){
                    $j=1;
                    foreach ($info2 As $i2):
                    $nombre = unserialize($i2['otros']);
                    $iva = unserialize($i2['otros_iva']);
                    $des = unserialize($i2['otros_des']);

                    if(is_array($nombre)){
                        $k=0;
                        $monto = 0;
                        $teliva = 0;
                        
                        $cadena .= '<tr>
                        <td>Valor Solicitad</td>
                        <td>'.toMoney($info2[0]['monto']).'</td>
                        <td>'.toMoney(0).'</td>
                        </tr>';
                        
                        foreach ($nombre As $key=>$o):
                            $cadena .= '<tr>';
                            if($des[$k]=="2"){
                                //echo '<tr>';
                                $cadena .= '<td>'.$key.'</td>';
                                if($key=="GMF"){
                                    $cadena .= '<td>'.toMoney($o*($info2[0]['monto']/100)).'</td>';
                                    $eliva = ($iva[$k]=="2")?(($o*($info2[0]['monto']/100))*0.16):0;
                                    $monto += $o*($info2[0]['monto']/100);
                                }else{
                                    $cadena .= '<td>'.toMoney($o).'</td>';
                                    $eliva = ($iva[$k]=="2")?($o*0.16):0;
                                    $monto += $o;
                                }
                                
                                $cadena .= '<td>'.toMoney($eliva).'</td>';
                                $teliva += $eliva;
                                //echo '<tr>';
                            }
                            $cadena .= '<tr>';
                            $k++;
                        endforeach;
                        $cadena .= '<tr><td>Total descuentos</td><td>'.toMoney($monto).'</td><td>'.toMoney($teliva).'</td></tr>';
                        $cadena .= '<tr><td>Total neto a desembolsar</td><td>'.toMoney($info2[0]['monto']-($monto+$teliva)).'</td></tr>';
                    }
                    
                  $j++;
                  endforeach;
                }else{
                    $cadena .= 'No hay datos';
                }
                
            $cadena .= '</tbody>
            </table>
           
           Plan de pagos : <br/>
	    <table border="1">
		<thead>
		  <tr>
                    <td>Cuota</td>
		    <td>Fecha corte</td>
		    <td>Fecha pago</td>
		    <td>Cuota fija</td>
		    <td>Saldo a capital</td>
		    <td>Interes corriente</td>
		    <td>Interes dias</td>
		    <td>Aporte a capital</td>';

                    if(is_array($info)){
                        $otros = unserialize($info2[0]['otros']);
                        $des = unserialize($info2[0]['otros_des']);
                        //print_r($info2);
                        //print_r($des);
                        //die();
                        if(is_array($otros)){
                            $i=0;
                            foreach ($otros As $key=>$o):
                                if($des[$i]=="1"){
                                    $cadena .= '<td>'.ucwords($key).'</td>';
                                }
                                $i++;
                            endforeach;
                        }
                    }
                $cadena .= '
                    <td>Total cuota</td>
		  </tr>
		</thead>
		<tbody>';
			if(is_array($info)){
                            $j=1;
		    foreach ($info As $i):
                  $cadena .= '
		  <tr>
                    <td>'.$j.'</td>
		    <td>'.$i['fechaCorte'].'</td>
		    <td>'.$i['fechaPago'].'</td>
		    <td>'.toMoney($i['cuotaFija']).'</td>
		    <td>'.toMoney($i['capital']).'</td>
		    <td>'.toMoney($i['interes']).'</td>
		    <td>'.toMoney($i['interes_dia']).'</td>
		    <td>'.toMoney($i['aporteCapital']).'</td>';

                    $otros = unserialize($info2[0]['otros']);
                    $des = unserialize($info2[0]['otros_des']);
                    
                    if(is_array($otros)){
                         $l=0;
                        foreach ($otros As $o):
                            if($des[$l]=="1"){
                                $cadena .= '<td>'.toMoney($o*($i['elcapital']/100)).'</td>';
                                //$cadena .= '<td>'.toMoney($o*($i['elcapital']/100)).'</td>';
                            }
                            $l++;
                        endforeach;
                    }

		    $cadena .= '<td>'.toMoney($i['cuotaFija']+$i['otros_total']+$i['interes_dia']).'</td>';

                    $j++;
		    endforeach;
			}else{
			  $cadena .= 'No hay datos';
			}
                  $cadena .='
		</tbody>
	    </table>
           </section>';
       
       $cadena .= '<footer><p><b><center>Calle 6 Norte # 2N - 36 Centro Comercial Campanario. Oficina 104. Tel: (2) 370 98 88
    servicioalcliente@serprogreso.com</center></b></p></footer>
		    </body>
		    </html>';
    echo $cadena;
    //pdf($cadena,'app/files/clientes/'.$info2[0]['cc'].'/cartadesembolso'.$info2[0]['id']);
    //return location('descargar.php?file=app/files/clientes/'.$info2[0]['cc'].'/cartadesembolso'.$info2[0]['id'].'.pdf');
    }
    
    public function cupo($info=null){
      echo getMensaje();
        ?>
           <h2><?php  echo SOLICITUDES_DESEMBOLSOLST?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  SOLICITUDES_NOMBRE        =>setUrl('solicitudes'),
            		  SOLICITUDES_LISTA         =>setUrl('solicitudes','lst'),
            		  SOLICITUDES_DESEMBOLSO    =>setUrl('solicitudes','desembolso'),
                          SOLICITUDES_RECHAZO       =>setUrl('solicitudes','lstRechazadas'),
            		  SOLICITUDES_DESEMBOLSOLST =>setUrl('solicitudes','lstDesembolsos'),
                          SOLICITUDES_CUPO          =>'#'
            		 ),
            	    SOLICITUDES_CUPO
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  SOLICITUDES_CUPO=>'#'
            		 ),
            	    SOLICITUDES_CUPO, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo SOLICITUDES_CUPO?></h4>
	  <div class='container'>
              <?php Html::openForm("solicitudes",setUrl("solicitudes","cupoprocess"));?>
                <fieldset>
                    <legend><?=SOLICITUDES_CUPO;?></legend>
                    <?php
                        Html::newInput("lstClientes","Cliente*");
                        Html::newInput("valcupo","Cupo*");
                        Html::newHidden("cupo");
                        Html::newTextarea("estudio","Estudio de credito",null,"required");
                        Html::newTextarea("concepto","Concepto del estudio",null,"required");
                    ?>
                </fieldset>
                <fieldset>
                    <legend>Referencias</legend>
                    <?php
                        echo selectReferencia("","","referencia[]");
                        echo selectVinculo("","","vinculo[]");
                        Html::newInput("nombrer[]","Nombre referencia *");
                        Html::newInput("apellidor[]","Apellido referencia *");
                        Html::newInput("ccr[]","Documento referencia *");
                        Html::newInput("correor[]","Correo referencia *");
                        echo inputDateMenores('fechar[]');
                        Html::newInput("fechar[]","Fecha nacimiento referencia *");
                        Html::newInput("dirr[]","Direccion referencia *");
                        Html::newInput("telr[]","Telefono referencia *");
                        Html::newInput("celr[]","Celular referencia *");
                        Html::newInput("empresar[]","Empresa donde labora referencia *");
                        Html::newInput("cargor[]","Cargo referencia *");
                        echo '<hr>';
                        echo selectReferencia("","","referencia[]");
                        echo selectVinculo("","","vinculo[]");
                        Html::newInput("nombrer[]","Nombre referencia *");
                        Html::newInput("apellidor[]","Apellido referencia *");
                        Html::newInput("ccr[]","Documento referencia *");
                        Html::newInput("correor[]","Correo referencia *");
                        echo inputDateMenores('fechar[]');
                        Html::newInput("fechar[]","Fecha nacimiento referencia *");
                        Html::newInput("dirr[]","Direccion referencia *");
                        Html::newInput("telr[]","Telefono referencia *");
                        Html::newInput("celr[]","Celular referencia *");
                        Html::newInput("empresar[]","Empresa donde labora referencia *");
                        Html::newInput("cargor[]","Cargo referencia *");
                    ?>
                </fieldset>
                <!--<fieldset>
                    <div class="control-group" id="adj">
                        <label for="adjuntos1" class="control-label">Adjuntos : </label>
                        <div class="controls" id="nuevo">
                            <input type="file" name="adjuntos1[]" id="adjuntos1">
                            <input type="text" name="nadjunto[]" id="nadjunto" placeholder="Nombre archivo">
                            <a class="btn clonar" onclick="clonar(this)">+</a>
                            <a class="btn remover" onclick="remover(this)">-</a>
                        </div>
                    </div>
                </fieldset>-->
                    <?php
                        Html::newButton("enviar", "Solicitar", "submit");
                        
                        Html::closeForm();
                        
                            if(!empty($info)){
                                    echo datatable("tabla");
                                    ?>
              <table id="tabla" class="table table-hover table-striped">
                  <thead>
                      <tr>
                          <th>#</th>
                          <th>Cliente</th>
                          <th>Adjuntos</th>
                          <th>Subir Adjuntos</th>
                          <th>Cupo solicitado</th>
                          <th>Cupo aprobado</th>
                          <th>Cupo disponible</th>
                          <th>Cupo utilizado</th>
                          <th>Fecha</th>
                          <th>Ver</th>
                          <th>Editar cupo</th>
                          <th>Aprobar</th>
                          <th>Rechazar</th>
                      </tr>
                  </thead>
                  <tbody>
                      <?php
                        $j=1;
                        foreach ($info As $i){
                            //if($i['estado']=='1'){
                                $cupos = mSolicitudes::infoCupo2($i['cliente_id']);
                                $cupos = $cupos[0];
                            //}
                            ?>
                            <tr>
                                <td><?php echo $j;?></td>
                                <td><?php echo $i['cliente_nombre'];?></td>
                                
                                <td><a href="<?php echo setUrl("solicitudes","archivos_cupo",array("id"=>$i['id']));?>"><img src="app/img/download.png"></a></td>
                                <td><a href="<?php echo setUrl("solicitudes","cargarDocs_cupo",array("id"=>$i['id']));?>"><img src="app/img/folder.png"></a></td>
                                
                                <td><?php echo toMoney($i['cupo']);?></td>
                                <?php if($i['estado']=='1'){?>
                                <td><?php echo toMoney($cupos['cupo']);?></td>
                                <td><?php echo toMoney($cupos['cupo_disponible']);?></td>
                                <td><?php echo toMoney($cupos['cupo_utilizado']);?></td>
                                <?php }else{
                                ?>
                                <td><?php echo toMoney(0);?></td>
                                <td><?php echo toMoney(0);?></td>
                                <td><?php echo toMoney(0);?></td>
                                <?php
                                } ?>
                                <td><?php echo $i['fecha'];?></td>
                                <td><a href="<?php echo setUrl("solicitudes","verCupo",array("id"=>$i['id']));?>"><img src="app/img/ver.png"></a></td>
                                <?php if($i['estado']=='0'){ ?>
                                <td><a href="<?php echo setUrl("solicitudes","editCupo",array("id"=>$i['id']));?>"><img src="app/img/editar.png"></a></td>
                                <td><a href="<?php echo setUrl("solicitudes","aprobarCupo",array("id"=>$i['id']));?>"><img src="app/img/use.png"></a></td>
                                <td><a href="<?php echo setUrl("solicitudes","delCupo",array("id"=>$i['id']))?>"><img src="app/img/eliminar.png"></td>
                                <?php }else{?>
                                <td>&nbsp;</td>
                                <?php if($i['estado']=='1'){ ?>
                                <td><a href="<?php echo setUrl("solicitudes","cupo",array('id'=>$i['cliente_id'],'id_so'=>$i['id']));?>"><img src="app/img/file.png"></a></td>
                                <?php }else{?>
                                <td>Rechazado</td>
                                <?php } ?>
                                <td>&nbsp;</td>
                                <?php } ?>
                            </tr>
                            <?php
                            $j++;
                        }
                      ?>
                  </tbody>
              </table>
                                    <?php
                            }
                    ?>
	  </div>
       <?php  
    }
    
    public function editCupo($info=null){
      echo getMensaje();
        ?>
           <h2><?php  echo SOLICITUDES_DESEMBOLSOLST?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('solicitudes','cupo'),
                          SOLICITUDES_CUPO_EDITAR          =>'#'
            		 ),
            	    SOLICITUDES_CUPO_EDITAR
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('solicitudes','cupo'), 
            		  SOLICITUDES_CUPO_EDITAR=>'#'
            		 ),
            	    SOLICITUDES_CUPO_EDITAR, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo SOLICITUDES_CUPO_EDITAR?></h4>
	  <div class='container'>
              <?php Html::openForm("solicitudes",setUrl("solicitudes","ediatCupopro"));?>
                <fieldset>
                    <legend><?=SOLICITUDES_CUPO_EDITAR;?></legend>
                    <?php
                        Html::newInput("lstClientes","Cliente*",$info['cliente_id'].' - '.$info['cliente_nombre'],"readonly");
                        Html::newInput("cupo","Cupo*",$info['cupo']);
                    ?>
                </fieldset>
                    <?php
                    Html::newHidden('id',$info['id']);
                        Html::newButton("enviar", "Editar", "submit");
                        Html::closeForm();
                    ?>
	  </div>
       <?php  
    }
    
    public function delCupo($info=null){
      echo getMensaje();
        ?>
           <h2><?php  echo SOLICITUDES_DESEMBOLSOLST?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('solicitudes','cupo'),
                          SOLICITUDES_CUPO_RECHAZAR=>'#'
            		 ),
            	    SOLICITUDES_CUPO_RECHAZAR
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('solicitudes','cupo'), 
            		  SOLICITUDES_CUPO_RECHAZAR=>'#'
            		 ),
            	    SOLICITUDES_CUPO_RECHAZAR, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo SOLICITUDES_CUPO_RECHAZAR?></h4>
	  <div class='container'>
              <?php Html::openForm("solicitudes",setUrl("solicitudes","delCupoPro"));?>
                <fieldset>
                    <legend><?=SOLICITUDES_CUPO_RECHAZAR;?></legend>
                    <?php
                        Html::newTextarea('por','Por que se rechazo la solicitud','','required');
                    ?>
                </fieldset>
                    <?php
                    Html::newHidden('id',$info);
                        Html::newButton("enviar", "Rechazar", "submit");
                        Html::closeForm();
                    ?>
	  </div>
       <?php  
    }
    
    public function aprobarCupo($info=null){
      echo getMensaje();
        ?>
           <h2><?php  echo SOLICITUDES_DESEMBOLSOLST?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('solicitudes','cupo'),
                          SOLICITUDES_CUPO_APRO=>'#'
            		 ),
            	    SOLICITUDES_CUPO_APRO
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('solicitudes','cupo'), 
            		  SOLICITUDES_CUPO_APRO=>'#'
            		 ),
            	    SOLICITUDES_CUPO_APRO, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo SOLICITUDES_CUPO_APRO?></h4>
	  <div class='container'>
              <?php Html::openForm("solicitudes",setUrl("solicitudes","aprobarCupoPro"));?>
                <fieldset>
                    <legend><?=SOLICITUDES_CUPO_APRO;?></legend>
                    <?php
                        Html::newTextarea('por','Por que se aprobo la solicitud','','required');
                    ?>
                </fieldset>
                    <?php
                    Html::newHidden('id',$info);
                        Html::newButton("enviar", "Aprobar", "submit");
                        Html::closeForm();
                    ?>
	  </div>
       <?php  
    }
  

    public function aprobar($info,$estado){
      echo getMensaje();
        ?>
           <h2><?php  echo SOLICITUDES_DESEMBOLSOLST?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('solicitudes','cupo'),
                          SOLICITUDES_APRO=>'#'
            		 ),
            	    SOLICITUDES_APRO
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('solicitudes','cupo'), 
            		  SOLICITUDES_APRO=>'#'
            		 ),
            	    SOLICITUDES_APRO, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo SOLICITUDES_APRO?></h4>
	  <div class='container'>
              <?php Html::openForm("solicitudes",setUrl("solicitudes","aprobarPro"));?>
                <fieldset>
                    <legend><?=SOLICITUDES_APRO;?></legend>
                    <?php
                        Html::newTextarea('por','Motivo de la aprobacìon','','required rows="6"');
                    ?>
                </fieldset>
                    <?php
                        Html::newHidden('id',$info);
                        Html::newHidden('estado',$estado);
                        Html::newButton("enviar", "Aprobar", "submit");
                        Html::closeForm();
                    ?>
	  </div>
       <?php  
    }

    public function desembolsar($info){
      echo getMensaje();
        ?>
           <h2><?php  echo SOLICITUDES_DESEMBOLSOLST?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('solicitudes','cupo'),
                          SOLICITUDES_DES=>'#'
            		 ),
            	    SOLICITUDES_DES
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('solicitudes','cupo'), 
            		  SOLICITUDES_DES=>'#'
            		 ),
            	    SOLICITUDES_DES, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo SOLICITUDES_DES?></h4>
	  <div class='container'>
              <?php Html::openForm("solicitudes",setUrl("solicitudes","desembolsarPro"));?>
                <fieldset>
                    <legend><?=SOLICITUDES_DES;?></legend>
                    <?php
                    echo selectMedios();
                        /*Html::newSelect('medio','Medio de desembolso',
                                array(
                                    'Efectivo'=>'Efectivo',
                                    'Consignaci&oacute;n'=>'Consignaci&oacute;n',
                                    'Transferencia'=>'Transferencia'
                                )
                                );*/
                        Html::newTextarea('observaciones','Observaciones','','required rows="6"');
                    ?>
                </fieldset>
                    <?php
                        Html::newHidden('id',$info);
                        Html::newButton("enviar", "Desembolsar", "submit");
                        Html::closeForm();
                    ?>
	  </div>
       <?php  
    }
    
    public function rechazar($info=null){
      echo getMensaje();
        ?>
           <h2><?php  echo SOLICITUDES_DESEMBOLSOLST?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('solicitudes','cupo'),
                          SOLICITUDES_RECHAZAR=>'#'
            		 ),
            	    SOLICITUDES_RECHAZAR
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('solicitudes','cupo'), 
            		  SOLICITUDES_RECHAZAR=>'#'
            		 ),
            	    SOLICITUDES_RECHAZAR, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo SOLICITUDES_RECHAZAR?></h4>
	  <div class='container'>
              <?php Html::openForm("solicitudes",setUrl("solicitudes","rechazarPro"));?>
                <fieldset>
                    <legend><?php echo SOLICITUDES_RECHAZAR;?></legend>
                    <?php
                        Html::newTextarea('por','Por que se rechaza la solicitud','','required');
                    ?>
                </fieldset>
                    <?php
                        Html::newHidden('id',$info);
                        Html::newButton("enviar", "Rechazar", "submit");
                        Html::closeForm();
                    ?>
	  </div>
       <?php  
    }
    
    public function concepto($info,$url,$id){
      echo getMensaje();
        ?>
           <h2><?php  echo SOLICITUDES_ESTUDIO?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('solicitudes','ver',array('id'=>$id)), 
            		  SOLICITUDES_LISTA=> setUrl('solicitudes','lst'),
            		  SOLICITUDES_ESTUDIO=>"#",
            		 ),
            	    SOLICITUDES_ESTUDIO
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('solicitudes','ver',array('id'=>$id)),
                            SOLICITUDES_LISTA=> setUrl('solicitudes','lst'),
                            SOLICITUDES_ESTUDIO=>'#'
            		 ),
            	    SOLICITUDES_ESTUDIO, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo SOLICITUDES_ESTUDIO?></h4>
       <div class='container'>
		<?php Html::openForm("editar_solicitudes",setUrl("solicitudes",$url));?>
		<fieldset>
                    <legend><?php  echo SOLICITUDES_ESTUDIO?></legend>
                    <?php
                    $readonly = $info['cerrado']==1?'readonly':'';
                    Html::newTextarea('estudio', 'Estudio Realizado',$info['estudio'],$readonly.' rows=10 cols=200');
                    Html::newTextarea('concepto', 'Concepto del Estudio',$info['concepto'],$readonly.' rows=10 cols=200');
                    ?>
                </fieldset>
		<?php
                  $btn = is_array($info)?'Editar':'Guardar';
		  Html::newHidden("id",$id);
                  if($info['cerrado']==0){
		  Html::newButton("editar", $btn, "submit");
                  }
		  Html::closeForm();
                  if($info['cerrado']==0 && is_array($info)){
		?>
                <a href="<?php echo setUrl('solicitudes','cerrarconcepto',array('id'=>$id))?>" class="btn btn-primary">Cerrar concepto</a>
                <?php
                  }
                  ?>
       </div>
       <?php  
    }
    
} // class
