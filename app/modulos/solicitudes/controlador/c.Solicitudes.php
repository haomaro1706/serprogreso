<?php
include_once 'app/modulos/solicitudes/vista/v.Solicitudes.php';
include_once 'app/modulos/solicitudes/modelo/m.Solicitudes.php';
include_once 'app/modulos/sucursales/modelo/m.Sucursales.php';
include_once 'app/modulos/productos/modelo/m.Productos.php';
include_once 'app/modulos/parametros/modelo/m.Parametros.php';
include_once 'app/modulos/clientes/modelo/m.Clientes.php';
include_once 'app/modulos/cartera/modelo/m.Cartera.php';
include_once 'app/modulos/zonas/modelo/m.Zonas.php';

class Solicitudes extends vSolicitudes{


    public function main(){
	validar_usuario();
	parent::main();    
    }
    
    public function listarSucur($array){
    	$productos = mSucursales::sucurbyId($array['id_zona']);
        $datos = array();
	if(is_array($productos)){
            foreach($productos As $pro){
              $datos[$pro['id']] = $pro['nombre_suc'];
            }
        }
	Html::newSelect("sucursal", "Sucursal *",$datos);
    }
    
     public function listarCredito($array=null){
         
    	$productos = mProductos::listarZona(getSucursal());
        $productos =$productos[0];
        $pro2 = unserialize($productos['productos']);
	/*foreach($pro2 As $pro){
	  //$datos[$pro['id']] = $pro['producto'];
            echo $pro.'<br/>';
	}*/
        $cadena ='<div class="control-group">
                        <label class="control-label" for="pro">Productos:</label>
                        <div class="controls">';
                            
                            include_once 'app/modulos/productos/modelo/m.Productos.php';
                            $sucursales = mProductos::listar();
                            $sucs='';
                            if (is_array($sucursales)){
                                    foreach($sucursales as $key){
                                            if(in_array($key["id"],$pro2)){
                                            //$cadena .= '<input type="checkbox" name="producto[]" value="'.$key["id"].'"> '.$key["producto"].'<br>';
                                            $datos[$key['id'].'-'.$key['logica']] = $key['producto'];
                                            }
                                    }
                            }else{
                                    $cadena .= 'No hay productos creados';
                            }
                            //echo $sucs;
                       $cadena .' </div>
                    </div>';
      //  echo $cadena;
        Html::newSelect("lineaCredito", "Producto *",$datos,'','onchange="plazos(this)"'); 
        
    }
    
    public function listarCredito2($array=null){
         
    	$productos = mProductos::listarZona(getSucursal());
        $productos =$productos[0];
        $pro2 = unserialize($productos['productos']);
	/*foreach($pro2 As $pro){
	  //$datos[$pro['id']] = $pro['producto'];
            echo $pro.'<br/>';
	}*/
        $cadena ='<div class="control-group">
                        <label class="control-label" for="pro">Productos:</label>
                        <div class="controls">';
                            
                            include_once 'app/modulos/productos/modelo/m.Productos.php';
                            $sucursales = mProductos::listarLineas2();
                            $sucs='';
                            if (is_array($sucursales)){
                                    foreach($sucursales as $key){
                                            if(in_array($key["id"],$pro2)){
                                                $datos[$key['id']] = $key['nombre'];
                                            }
                                    }
                            }else{
                                    $cadena .= 'No hay productos creados';
                            }
                            //echo $sucs;
                       $cadena .' </div>
                    </div>';
      //  echo $cadena;
        Html::newSelect("lineaCredito2", "Linea *",$datos);
        
    }
    
    public function listarTasa($array=null){
        $tasas = mParametros::lstTasas2($array['producto']);
        //die(print_r($tasas[0]));
        //Html::newHidden('lstTasas2', $tasas[0]['seguroVida']);
        Html::newHidden('lstTasas22', $tasas[0]['tasa_mensual']);
    }
    
    public function listarTasa2($array=null){
        $tasas = mParametros::lstTasas2($array['producto']);
        Html::newInput('latasa','Tasa', $tasas[0]['tasa_mensual'],'readonly');
    }
    
    
    public function listarEmpresa($array=null){
        $empresas = mParametros::lstEmpre();

        $cadena ='<div class="control-group">
                        <label class="control-label" for="pro">Productos:</label>
                        <div class="controls">'; 
                        if (is_array($empresas)){
                            foreach($empresas as $key){
                                $datos[$key['nit']] = $key['razon'];
                            }
                        }else{
                            $cadena .= 'No hay productos creados';
                        }
                       $cadena .' </div>
                    </div>';
      //  echo $cadena;
        Html::newSelect("empresa", "Empresa *",$datos); 
    }

    public function solicitudProcess($array=null){
	validar_usuario();
        //die(print_r($array));
	$id= explode('-', $array['lstClientes']);
	$id2= explode('-', $array['lstCodeudores']);
        $id2 = empty($id2[0])?null:$id2[0];
	$info=0;
	$error_info='';
        $sucursal = getSucursal();
        
        $credito = explode('-',$array['lineaCredito']);
        
        if($credito[1]==2){
            /*
            solicitud del rotativo
             *              */
            $cupo = str_replace(array('$','.'),array('',''),$array['elcupo']);

            if($array['valor_solicitado'] > $cupo){
		setMensaje('El monto solicitado es superior al cupo disponible','info');
		location(setUrl('solicitudes'));
            }
            	$crear1=mSolicitudes::crearSolicitud(
                                            $sucursal,
                                            $array["lineaCredito"],
                                            getIdUsuario(),
                                            getIdUsuario(),
                                            trim($id[0]),
                                            trim($id2),
                                            $array["valor_solicitado"],
                                            $array["amortizacion"],
                                            $array["fecha_corte"],
                                            $array["plazo"],
                                            $array["bajar_tasa"],
                                            $array["observaciones"]
                                            );
                //die('');
                if($crear1){
                    $ultimo = mSolicitudes::ultimo('op_solicitudes');
                    $id_solicitud=  $ultimo['id'];
                    
                    //if($array['valReferencia'] == 's'){
                        $crear2= mSolicitudes::crearRefencias(
                                serialize($array["referencia"]),
                                serialize($array["vinculo"]),
                                serialize($array["nombrer"]),
                                serialize($array["apellidor"]),
                                serialize($array["ccr"]),
                                serialize($array["correor"]),
                                serialize($array["fechar"]),
                                serialize($array["dirr"]),
                                serialize($array["telr"]),
                                serialize($array["celr"]),
                                serialize($array["empresar"]),
                                serialize($array["cargor"]),
                                $id_solicitud
                        );
                    /*}else{
                        $crear2= true;
                    }*/
                    if($crear2){
                        $desc = ($array['bajar_tasa']=='1')?'<br/>Nota: '.$array["observaciones"]:'';
                        notificaciones(1,'Se ha creado la solicitud para el cliente '.$id[1].' por valor de '.  toMoney($array['valor_solicitado']).$desc);
                        setMensaje("Solicitud creada correctamente","success");
                    }else{
                    setMensaje('error al generar la solicitud', 'error');
                    }
                }else{
                    setMensaje('error al generar la solicitud', 'error');
                }
                //location(setUrl('solicitudes'));
        }
        else{
            //die(print_r($array));
	$crear1=mSolicitudes::crearSolicitud(
                                        $sucursal,
                                        $array['lineaCredito'],
                                        getIdUsuario(),
                                        getIdUsuario(),
                                        trim($id[0]),
                                        $id2,
                                        $array["valor_solicitado"],
                                        $array["amortizacion"],
                                        $array["fecha_corte"],
                                        $array["plazo"],
                                        $array["bajar_tasa"],
                                        $array["observaciones"]
                                        );
	if($crear1){            
            $ultimo = mSolicitudes::ultimo('op_solicitudes');
            $id_solicitud=  $ultimo['id'];
            
            /*guardar info libranza*/
            mSolicitudes::crearLibranza($id_solicitud,$array['empresa']);
            
            //if($array['valReferencia'] == 's'){
                $crear2= mSolicitudes::crearRefencias(
                                serialize($array["referencia"]),
                                serialize($array["vinculo"]),
                                serialize($array["nombrer"]),
                                serialize($array["apellidor"]),
                                serialize($array["ccr"]),
                                serialize($array["correor"]),
                                serialize($array["fechar"]),
                                serialize($array["dirr"]),
                                serialize($array["telr"]),
                                serialize($array["celr"]),
                                serialize($array["empresar"]),
                                serialize($array["cargor"]),
                                $id_solicitud
                        );
            /*}else{
                $crear2 = true;
            }*/
            
            if($crear2){
                
                //vehiculo
                if($array["valVehiculo"]=="s"){
                    mSolicitudes::crearVehiculo(
                            $array["placa"],
                            $array["marca"],
                            $array["vLinea"],
                            $array["cilindraje"],
                            $array["modelo"],
                            $array["precioV"],
                            $array["estadoV"],
                            $array["nombreV"],
                            $array["numeroV"],
                            $array["concesionario"],
                            $array["telV"],
                            $id_solicitud
                            );
                }
                
                
                setMensaje("Solicitud creada correctamente","success");
                  //enviar correo
                $desc = ($array['bajar_tasa']=='1')?'<br/>Nota: '.$array["observaciones"]:'';
                $produc = explode('-',$array['lineaCredito']);
                notificaciones(1,'Se ha creado la solicitud de '.nombreProducto($produc[0]).' para el cliente '.$id[1].' por valor de '.  toMoney($array['valor_solicitado']).$desc);
                
            }else{
                setMensaje('error al generar la solicitud', 'error');
                //location(setUrl('solicitudes'));
            }
	}else{
	  setMensaje('error al generar la solicitud', 'error');
	  //location(setUrl('solicitudes','lst'));
	}
    }
        //die('adjuntos');
        /*guardar adjuntos*/
        $total = count($array["adjuntos1"]["name"]);
        include_once 'app/sistema/ftp.php';
        $ftp=new ftp();

        if(!is_readable('app/files/clientes/'.trim($id[0]).'/solicitudes')){
            mkdir('app/files/clientes/'.trim($id[0]).'/solicitudes',0777);
        }
        //$id_solicitud
        if(!is_readable('app/files/clientes/'.trim($id[0]).'/solicitudes/'.$id_solicitud)){
            mkdir('app/files/clientes/'.trim($id[0]).'/solicitudes/'.$id_solicitud,0777);
        }
        
        if($total>0){
            for($i = 0; $i < $total; $i++){
                $ext = explode('.',$array['adjuntos1']['name'][$i]);
                $local1 = limpiaradjuntos($array["nadjunto"][$i]).'.'.$ext[1];// el nombre del archivo	
                $remoto1 = $array["adjuntos1"]["tmp_name"][$i];// Este es el nombre temporal del archivo mientras dura la transmision	
                $tama1 = $array["adjuntos1"]["size"][$i];
                $ftp->SubirArchivo($local1,$remoto1,$tama1,'app/files/clientes/'.trim($id[0]).'/solicitudes/'.$id_solicitud,'solicitud'.($i+1));
            }
        }
       // die("<br/>Fin..");
        location(setUrl('solicitudes'));
    }
    
    public function cargarDocPro($array=null){
        //die(print_r($array));
        $total = count($array["adjuntos1"]["name"]);
        include_once 'app/sistema/ftp.php';
        $ftp=new ftp();

        if(!is_readable('app/files/clientes/'.$array['cliente'].'/solicitudes')){
            mkdir('app/files/clientes/'.$array['cliente'].'/solicitudes',0777);
        }
        //$id_solicitud
        if(!is_readable('app/files/clientes/'.$array['cliente'].'/solicitudes/'.$array['solicitud'])){
            mkdir('app/files/clientes/'.$array['cliente'].'/solicitudes/'.$array['solicitud'],0777);
        }
        
        if($total>0){
            for($i = 0; $i < $total; $i++){
                $ext = explode('.',$array['adjuntos1']['name'][$i]);
                $local1 = limpiaradjuntos($array["nadjunto"][$i]).'.'.$ext[1];// el nombre del archivo	
                $remoto1 = $array["adjuntos1"]["tmp_name"][$i];// Este es el nombre temporal del archivo mientras dura la transmision	
                $tama1 = $array["adjuntos1"]["size"][$i];
                $ftp->SubirArchivo($local1,$remoto1,$tama1,'app/files/clientes/'.$array['cliente'].'/solicitudes/'.$array['solicitud'],'solicitud'.($i+1));
            }
            setMensaje("Archivos cargados correctamente","success");
        }else{
            setMensaje("No se adjuntaros archivos","info");
        }
        //die('fin');
        location(setUrl('solicitudes',$array['url']));
        
    }
    
    
       public function cargarDocPro2($array=null){
        //die(print_r($array));
        $total = count($array["adjuntos1"]["name"]);
        include_once 'app/sistema/ftp.php';
        $ftp=new ftp();

        if(!is_readable('app/files/clientes/'.$array['cliente'].'/cupos'.$array['solicitud'])){
            mkdir('app/files/clientes/'.$array['cliente'].'/cupos'.$array['solicitud'],0777);
        }
        //$id_solicitud
        /*if(!is_readable('app/files/clientes/'.$array['cliente'].'/cupo'.$array['solicitud'])){
            mkdir('app/files/clientes/'.$array['cliente'].'/cupo'.$array['solicitud'],0777);
        }*/
        //die('app/files/clientes/'.$array['cliente'].'/cupos'.$array['solicitud']);
        if($total>0){
            for($i = 0; $i < $total; $i++){
                $ext = explode('.',$array['adjuntos1']['name'][$i]);
                $local1 = limpiaradjuntos($array["nadjunto"][$i]).'.'.$ext[1];// el nombre del archivo	
                $remoto1 = $array["adjuntos1"]["tmp_name"][$i];// Este es el nombre temporal del archivo mientras dura la transmision	
                $tama1 = $array["adjuntos1"]["size"][$i];
                $ftp->SubirArchivo($local1,$remoto1,$tama1,'app/files/clientes/'.$array['cliente'].'/cupos'.$array['solicitud'],'solicitud'.($i+1));
            }
            setMensaje("Archivos cargados correctamente","success");
        }else{
            setMensaje("No se adjuntaros archivos","info");
        }
        //die('fin');
        location(setUrl('solicitudes',$array['url']));
        
    }


    public function lst(){
	validar_usuario();
	$all = mSolicitudes::all();
	parent::lst($all);
    }
    
    public function ver($array=null){
	validar_usuario();
        include_once 'app/sistema/ftp.php';
        
        $pro = mSolicitudes::byid($array['id'],"op_solicitudes");
        $pro = $pro[0]['producto'];
        
        $pro = explode("-",$pro);
        if($pro[1]==2){
            //no cargar juridico
            $all = mSolicitudes::allId($array['id']);
        }else{
            $all = mSolicitudes::allIdSincupo($array['id']);
        }
        $all = $all[0];
        $all['elid']=$array['id'];
        
        //die(print_r($all));
        
        $referencia=unserialize($all['referencia']);
        $vinculo=unserialize($all['vinculo']);
        $nombrer=unserialize($all['nombrer']);
        $apellidor=unserialize($all['apellidor']);
        $ccr=unserialize($all['ccr']);
        $correor=unserialize($all['correor']);
        $fechar=unserialize($all['fechar']);
        $dirr=unserialize($all['dirr']);
        $telr=unserialize($all['telr']);
        $celr=unserialize($all['celr']);
        $empresa=unserialize($all['empresar']);
        $cargor=unserialize($all['cargor']);
        $celr=unserialize($all['celr']);
        
        
        $ftp=new ftp();
    	$info1 = $ftp->ficheros('./app/files/clientes/'.$all['cliente'].'/solicitudes/'.$array['id'].'/');
        //die('./app/files/clientes/'.$all['cliente'].'/solicitudes/'.$array['id'].'/');
        //die(print_r($info1));
        
        $tasas = mSolicitudes::lstTasas($all['producto']);
        $tasas = $tasas[0];
        //die(print_r($tasas));
        $k      = $all['monto'];
        $tasa   = $tasas['tasa_mensual']/100;
        $tasa2  = $tasas['tasa_diaria']/100;
        $plazo  = $all['plazo'];
        $per = $plazo;
        //calculo de cuota
        $num = $k * $tasa;
        $den = 1 - pow((1+$tasa),-$per);
        $cuota = $num/$den;

        //die('='.$cuota.' '.$k);
        
        parent::ver(
                $array['id'],
                $all,
                $referencia,
                $vinculo,
                $nombrer,
                $apellidor,
                $ccr,
                $correor,
                $fechar,
                $dirr,
                $telr,
                $celr,
                $empresa,
                $cargor,
                $info1,
                $cuota
                );
    }
    
    public function ver2($array=null){
	validar_usuario();
        include_once 'app/sistema/ftp.php';
        
        $pro = mSolicitudes::byid($array['id'],"op_solicitudes");
        $pro = $pro[0]['producto'];
        
        $pro = explode("-",$pro);
        if($pro[1]==2){
            //no cargar juridico
            $all = mSolicitudes::allId($array['id']);
        }else{
            $all = mSolicitudes::allIdSincupo($array['id']);
        }
        $all = $all[0];
        $all['elid']=$array['id'];
        
        //die(print_r($all));
        
        $referencia=unserialize($all['referencia']);
        $vinculo=unserialize($all['vinculo']);
        $nombrer=unserialize($all['nombrer']);
        $apellidor=unserialize($all['apellidor']);
        $ccr=unserialize($all['ccr']);
        $correor=unserialize($all['correor']);
        $fechar=unserialize($all['fechar']);
        $dirr=unserialize($all['dirr']);
        $telr=unserialize($all['telr']);
        $celr=unserialize($all['celr']);
        $empresa=unserialize($all['empresar']);
        $cargor=unserialize($all['cargor']);
        $celr=unserialize($all['celr']);
        
        
        $ftp=new ftp();
    	$info1 = $ftp->ficheros('./app/files/clientes/'.$all['cliente'].'/solicitudes/'.$array['id'].'/');
        //die('./app/files/clientes/'.$all['cliente'].'/solicitudes/'.$array['id'].'/');
        //die(print_r($info1));
        
        $tasas = mSolicitudes::lstTasas($all['producto']);
        $tasas = $tasas[0];
        //die(print_r($tasas));
        $k      = $all['monto'];
        $tasa   = $tasas['tasa_mensual']/100;
        $tasa2  = $tasas['tasa_diaria']/100;
        $plazo  = $all['plazo'];
        $per = $plazo;
        //calculo de cuota
        $num = $k * $tasa;
        $den = 1 - pow((1+$tasa),-$per);
        $cuota = $num/$den;

        //die('='.$cuota.' '.$k);
        
        parent::ver(
                $array['id'],
                $all,
                $referencia,
                $vinculo,
                $nombrer,
                $apellidor,
                $ccr,
                $correor,
                $fechar,
                $dirr,
                $telr,
                $celr,
                $empresa,
                $cargor,
                $info1,
                $cuota
                );
    }
    
     public function ver_re($array=null){
	validar_usuario();
        include_once 'app/sistema/ftp.php';
        
        $pro = mSolicitudes::byid($array['id'],"op_solicitudes");
        $pro = $pro[0]['producto'];
        
        $pro = explode("-",$pro);
        if($pro[1]==2){
            //no cargar juridico
            $all = mSolicitudes::allId($array['id']);
        }else{
            $all = mSolicitudes::allIdSincupo($array['id']);
        }
        $all = $all[0];
        $all['elid']=$array['id'];
        
        //die(print_r($all));
        
        $referencia=unserialize($all['referencia']);
        $vinculo=unserialize($all['vinculo']);
        $nombrer=unserialize($all['nombrer']);
        $apellidor=unserialize($all['apellidor']);
        $ccr=unserialize($all['ccr']);
        $correor=unserialize($all['correor']);
        $fechar=unserialize($all['fechar']);
        $dirr=unserialize($all['dirr']);
        $telr=unserialize($all['telr']);
        $celr=unserialize($all['celr']);
        $empresa=unserialize($all['empresar']);
        $cargor=unserialize($all['cargor']);
        $celr=unserialize($all['celr']);
        
        
        $ftp=new ftp();
    	$info1 = $ftp->ficheros('./app/files/clientes/'.$all['cliente'].'/solicitudes/'.$array['id'].'/');
        //die('./app/files/clientes/'.$all['cliente'].'/solicitudes/'.$array['id'].'/');
        //die(print_r($info1));
        
        $tasas = mSolicitudes::lstTasas($all['producto']);
        $tasas = $tasas[0];
        //die(print_r($tasas));
        $k      = $all['monto'];
        $tasa   = $tasas['tasa_mensual']/100;
        $tasa2  = $tasas['tasa_diaria']/100;
        $plazo  = $all['plazo'];
        $per = $plazo;
        //calculo de cuota
        $num = $k * $tasa;
        $den = 1 - pow((1+$tasa),-$per);
        $cuota = $num/$den;

        //die('='.$cuota.' '.$k);
        
        parent::ver_re(
                $array['id'],
                $all,
                $referencia,
                $vinculo,
                $nombrer,
                $apellidor,
                $ccr,
                $correor,
                $fechar,
                $dirr,
                $telr,
                $celr,
                $empresa,
                $cargor,
                $info1,
                $cuota
                );
    }
    
    public function verCupo($array=null){
	validar_usuario();

        include_once 'app/sistema/ftp.php';
    	
        $all = mSolicitudes::solCupos($array['id']);
        $all = $all[0];
        
        $ftp=new ftp();
    	$info1 = $ftp->ficheros('./app/files/clientes/'.$all['cliente_id'].'/cupos'.$array['id'].'/');
        //die(print_r($info1));
        //die(print_r($all));
        
        $referencia=unserialize($all['referencia']);
        $vinculo=unserialize($all['vinculo']);
        $nombrer=unserialize($all['nombrer']);
        $apellidor=unserialize($all['apellidor']);
        $ccr=unserialize($all['ccr']);
        $correor=unserialize($all['correor']);
        $fechar=unserialize($all['fechar']);
        $dirr=unserialize($all['dirr']);
        $telr=unserialize($all['telr']);
        $celr=unserialize($all['celr']);
        $empresa=unserialize($all['empresar']);
        $cargor=unserialize($all['cargor']);
        $celr=unserialize($all['celr']);
        
        parent::verCupo(
                $array['id'],
                $all,
                $referencia,
                $vinculo,
                $nombrer,
                $apellidor,
                $ccr,
                $correor,
                $fechar,
                $dirr,
                $telr,
                $celr,
                $empresa,
                $cargor,
                $info1
                );
    }
    
    public function aprobar($array=null){
        validar_usuario();
        parent::aprobar($array['id'],$array['estado']);
    }
    
    public function aprobarPro($array=null){
	validar_usuario();
        $upd = mSolicitudes::aprobar($array['id'],$array['estado']);
        
        $info = mSolicitudes::infoSolicitud($array['id']);
        $info = $info[0];
        mSolicitudes::updSolicitudRecha($array['id'],$array['por']);
        
	if($upd){
		//envio correo
                notificaciones(4,'Se aprobo la solicitud al cliente '.$info['nombre'].' '.$info['apellido'].' por valor de '.toMoney($info['monto']));
		setMensaje('Solicitud aprobada correctamente','success');
	}else{
	  setMensaje('Error al aprobar la solicitud','error');
	}
	
    	location(setUrl('solicitudes','lst'));
    }
    
    
    public function rechazar($array=null){
        validar_usuario();
        parent::rechazar($array['id']);
    }
    
    public function rechazarPro($array=null){
	validar_usuario();
	
	$upd = mSolicitudes::aprobar($array['id'],'2');
	mSolicitudes::updSolicitudRecha($array['id'],$array['por']);
        
        $info = mSolicitudes::infoSolicitud($array['id']);
        $info = $info[0];
        
	if($upd){
		setMensaje('Solicitud rechazada con exito','success');
		notificaciones(8,'Se rechazo la solicitud para el cliente '.$info['nombre'].' '.$info['apellido'].' por valor de '.toMoney($info['monto'])); 
	}else{
		setMensaje('Error al rechazar Solicitud','error');
	}
       // die();
    location(setUrl('solicitudes','lst'));
    }
    
    public function desembolso(){
	validar_usuario();
	
	$all = mSolicitudes::all('3');
	//die(print_r($all));
	parent::desembolso($all);
    }
    
    public function lstRechazadas(){
	validar_usuario();
	
	$all = mSolicitudes::all('2');
	//die(print_r($all));
	parent::lstRechazadas($all);
    }
    
    public function delSolicitud($array=null){
        validar_usuario();
        
        if(mSolicitudes::borrar($array["id"],"op_solicitudes")){
            setMensaje("Solicitud eliminada correctamente.","success");
        }else{
            setMensaje("Error al eliminar la solicitud.","error");
        }
        
        location(setUrl("solicitudes","lstRechazadas"));
    }
    
    public function desembolsar($array=null){
        validar_usuario();
        parent::desembolsar($array['id']);
    }
    
    public function desembolsarPro($array=null){
	validar_usuario();
        
        
        $pro = mSolicitudes::byid($array['id'],"op_solicitudes");
        $pro = $pro[0]['producto'];
        
        $pro = explode("-",$pro);
        if($pro[1]==2){
            $validar_cupo = mSolicitudes::allId($array['id']);
        }else{
            $validar_cupo = mSolicitudes::allIdSincupo($array['id']);
        }

        $validar_cupo = $validar_cupo[0];

        
        //$dia = 25;
            
        $dia = date('d');
        $mes_fin = date('m');
        $ano=date('Y');
        $j=0;
        $festivos=0;
            
            
        
        $cupos = false;
        if($validar_cupo['logica']=='1' or $validar_cupo['logica']=='3'){
           //die('uno'); 
            //$upd = true;
           mSolicitudes::aprobar($array['id'],"4");

            include 'app/librerias/php/festivos.php';

            
            /*calculo de credito*/
            $solicitud = mSolicitudes::byId($array['id'],'op_solicitudes');

            $tasas = mSolicitudes::lstTasas($solicitud[0]['producto']);
            $otros_nombre =         unserialize($tasas[0]['nombre']);
            $otros_valor =          unserialize($tasas[0]['valor']);
            $otros_descontable =    unserialize($tasas[0]['descontable']);
            $otros_iva =            unserialize($tasas[0]['iva']);
            
            $k=$solicitud[0]['monto'];
            
            $tasa = empty($validar_cupo['tasaM'])?($tasas[0]['tasa_mensual']/100):($validar_cupo['tasaM']/100);
            //die($k."=".$tasa);
            $tasa2 =$tasas[0]['tasa_diaria']/100;
            $plazo = $solicitud[0]['plazo']+$festivos;
            
            $i=0;
            $desc=0;
            $iva=0;
            $add_cuota=0;
            $add_concep=null;
            foreach ($otros_valor As $valor){
                if(strpos($valor,'%')){
                    $porcentajes[$i] = str_replace('%','',$valor);
                    if($otros_descontable[$i]=='2'){
                        $desc += ($k*($porcentajes[$i]/100));
                        $otros[$otros_nombre[$i]]= ($k*($porcentajes[$i]/100));
                        $add_concep[$otros_nombre[$i]]= ($k*($porcentajes[$i]/100));
                    }else{
                        $otros[$otros_nombre[$i]]= $porcentajes[$i];
                        $add_cuota += ($k*($porcentajes[$i]/100));
                        $add_concep[$otros_nombre[$i]]= $porcentajes[$i];
                    }
                }else{
                    $restar[$i] = $valor;
                    if($otros_descontable[$i]=='2'){
                        $desc += ($restar[$i]);
                        $otros[$otros_nombre[$i]]=($restar[$i]);
                        $add_concep[$otros_nombre[$i]]= ($restar[$i]);
                        if($otros_iva[$i]=='2'){
                            $iva += ($restar[$i])*0.16;
                        }
                    }else{
                        $otros[$otros_nombre[$i]]= ($restar[$i]);
                        $add_concep[$otros_nombre[$i]]= ($restar[$i]);
                        $add_cuota += ($restar[$i]);
                    }
                }
                $i++;
            }
            
            //die(print_r($add_concep));
            
            $total = $k - $desc - $iva;
            //die ('total='.$total.' iva='.$iva.' desc='.$desc);
            $desembolsar=true;
        }
        else if($validar_cupo['logica']=='2'){
            //die("llega2");
            //valido cupo
            //if($validar_cupo['monto']){
            if($validar_cupo['monto']<=$validar_cupo['cupo_disponible']){
                mSolicitudes::aprobar($array['id'],4);

                mSolicitudes::infodesembolsar($array['id'],$array['observaciones'],$array['medio']);
                
                include 'app/librerias/php/festivos.php';
                    /*
                //$dia = 10;
                $dia = date('d');
                //$mes_fin = 07;
                $mes_fin = date('m');
                $ano=date('Y');
                $j=0;
                $festivos=0;*/

                /*calculo de credito*/
                $solicitud = mSolicitudes::byId($array['id'],'op_solicitudes');

                $tasas = mSolicitudes::lstTasas($solicitud[0]['producto']);
                $otros_nombre =         unserialize($tasas[0]['nombre']);
                $otros_valor =          unserialize($tasas[0]['valor']);
                $otros_descontable =    unserialize($tasas[0]['descontable']);
                $otros_iva =            unserialize($tasas[0]['iva']);

                $k=$solicitud[0]['monto'];
                $tasa =$tasas[0]['tasa_mensual']/100;
                $tasa2 =$tasas[0]['tasa_diaria']/100;
                $plazo = $solicitud[0]['plazo']+$festivos;

                $i=0;
                $desc=0;
                $iva=0;
                $add_cuota=0;
                $add_concep=null;
                foreach ($otros_valor As $valor){
                    if(strpos($valor,'%')){
                        $porcentajes[$i] = str_replace('%','',$valor);
                        if($otros_descontable[$i]=='2'){
                            $desc += ($k*($porcentajes[$i]/100));
                            $otros[$otros_nombre[$i]]= ($k*($porcentajes[$i]/100));
                        }else{
                            $otros[$otros_nombre[$i]]= $porcentajes[$i];
                            $add_cuota += ($k*($porcentajes[$i]/100));
                            $add_concep[$otros_nombre[$i]]= $porcentajes[$i];
                        }
                    }else{
                        $restar[$i] = $valor;
                        if($otros_descontable[$i]=='2'){
                            $desc += ($restar[$i]);
                            $otros[$otros_nombre[$i]]=($restar[$i]);
                            if($otros_iva[$i]=='2'){
                                $iva += ($restar[$i])*0.16;
                            }
                        }else{
                            $otros[$otros_nombre[$i]]= ($restar[$i]);
                            $add_concep[$otros_nombre[$i]]= ($restar[$i]);
                            $add_cuota += ($restar[$i]);
                        }
                    }
                    $i++;
                }

                //die(print_r($add_concep));

                $total = $k - $desc - $iva;
                //die ('total='.$total.' k='.$k.' ic='.$ic.' admin1='.$admin1.' recaudo='.$recaudo. ' iva='.$iva.' 4xmil='.$cuatro_mil);
                $desembolsar=true;
                $cupos = true;
            }else{
 		setMensaje('<b>El valor solicitado supera el cupo disponible</b>','info');
                location(setUrl('solicitudes','desembolso'));
            }
        }
	
        if($desembolsar){
               $desembolso = mSolicitudes::crearDesembolso(
					    $k,
					    ($tasas[0]['tasaMora']/100),
					    $total,
					    $solicitud[0]["amortizacion"],
					    $solicitud[0]["fecha_corte"],
					    opDate(),
                                            //"2016-10-25",
					    $tasa,
					    $tasas[0]['tasa_ea'],
					    $tasas[0]['tasa_nominal'],
					    $tasas[0]['tasa_mensual'],
					    $iva,
					    $plazo,
                                            $array['id'],
                                            serialize($otros),
                                            serialize($otros_iva),
                                            serialize($otros_descontable)
					  );
         
         
        //$desembolso=true; //volver a desembolsar y cascarle al crom
        }
        //$desembolso=true;
	if($desembolso){
            if($cupos){
                $infoCli = mClientes::listarId2($solicitud[0]['cliente']);
                 mClientes::updClienteMonto(
                    $infoCli[0]['cliente'],
                    ($infoCli[0]['cupo_disponible']-$k),
                    ($infoCli[0]['cupo_utilizado']+$k)
                );
            }
            
            $total = $k;
            $per = $plazo;
            //calculo de cuota
            $num = $k * $tasa;
            $den = 1 - pow((1+$tasa),-$per);
            $cuota = $num/$den;
               
            //calculo fechas
            $saldo = $total;
            $i = 0;
            $c = 0;
            $ac = 0;
            
            //fin calculo fecha
            //echo ('='.$mes_fin);
            
            if($dia>($solicitud[0]["fecha_corte"]+1)){
                $dias_cuadre = cal_days_in_month(CAL_GREGORIAN,$mes_fin,$ano);
                $diastotal = difFechas($ano.'-'.$mes_fin.'-'.$dia, $ano.'-'.($mes_fin+1).'-'.$solicitud[0]["fecha_corte"]);
                $diastotal = $diastotal +1;
                
            }else if($dia==($solicitud[0]["fecha_corte"])+1){
                $dias_cuadre    = 0;
                $diastotal      = 0;
            }else if($dia==($solicitud[0]["fecha_corte"])){
                $dias_cuadre = cal_days_in_month(CAL_GREGORIAN,$mes_fin,$ano);
                $diastotal   = 1;
                    //$diastotal = cal_days_in_month(CAL_GREGORIAN,$mes_fin,$ano);
            }else{
                $dias_cuadre = cal_days_in_month(CAL_GREGORIAN,$mes_fin,$ano);
                $diastotal = difFechas($ano.'-'.$mes_fin.'-'.$dia, $ano.'-'.($mes_fin).'-'.$solicitud[0]["fecha_corte"]);
                $diastotal = $diastotal +1;
            }
            //die('='.$tasa);
            $interes_dia = (($k*$tasa)/$dias_cuadre)*$diastotal;
            
            //die($dia.' '.$tasa.'='.$interes_dia.'='.$dias_cuadre.'='.$diastotal);
            
            /*valido fecha de corte*/
            if($solicitud[0]["fecha_corte"]>=$dia){
               // die("uno");
                $mes_fin=$mes_fin+1;
            }else{
                //die("dos");
                $mes_fin=$mes_fin+2;
            }
            
            $mes = (strlen($mes_fin)==1)?'0'.($mes_fin):($mes_fin);
            //ojo
            $fechaIni = $ano.'-'.$mes.'-'.$solicitud[0]["fecha_corte"];
            $fechaIni2 = $fechaIni;
            //$mes2 = (strlen($mes_fin+1)==1)?'0'.($mes_fin+1):($mes_fin+1);
            $fechaLim = $ano.'-'.$mes.'-'.$solicitud[0]["fecha_corte"];
            
            $fechaLim = strtotime ( '+7 day' , strtotime ( $fechaLim ) ) ;
            $fechaLim = date ( 'Y-m-d' , $fechaLim );
            
            
            for($j = 1; $j <= $per; $j++){
                $i = ($saldo * $tasa);
                $ac = ($cuota-$i);
                $c = ($saldo - $ac);
                $saldo = ($c);
                
                if($j!=1){
                   $interes_dia=0; 
                }
                
                $saveCuota = mSolicitudes::saveCuota(
                        $array['id'],
                        $ac, 
                        $c, 
                        $i, 
                        $saldo, 
                        $cuota, 
                        $fechaIni,
                        $fechaLim,
                        $interes_dia,
                        $add_cuota,
                        serialize($add_concep)
                        );
                
                mSolicitudes::saveCuota2(
                        $array['id'],
                        $ac, 
                        $c, 
                        $i, 
                        $saldo, 
                        $cuota, 
                        $fechaIni,
                        $fechaLim,
                        $interes_dia,
                        $add_cuota,
                        serialize($add_concep)
                        );
                
                $expl = explode("-", $fechaIni);
                $dia2 = $solicitud[0]["fecha_corte"];
                $mes = $expl[1];
                $ano = $expl[0];
                $mes = ($mes+1) == 13 ? '01' : $mes+1 ;
                $mes = (strlen($mes)==1)?'0'.$mes:$mes;
                $mes2 = (strlen($mes2)==1)?'0'.($mes2+1):$mes2+1;
                
                if(($mes2) == 13){
                    $mes2 = '01';
                }else{
                    $mes2 = (strlen($mes2)==1)?'0'.($mes2):$mes2;
                }
                
                $ano2 = (($mes) == 01) ? $ano+1 : $ano;
                $ano = ($mes) == 01 ? $ano+1 : $ano;
                
                $fechaIni = $ano.'-'.($mes).'-'.$dia2;
                
                $fechaLim = $ano2.'-'.$mes.'-'.$dia2;
                $fechaLim = strtotime ( '+7 day' , strtotime ( $fechaLim ) ) ;
                $fechaLim = date ( 'Y-m-d' , $fechaLim );
                $dias_cuadre =0;
            }
            
            //die('fin');
            setMensaje('Desembolso creado correctamente','success');
            
	   //envio correos
            $info = mSolicitudes::byId($array['id'],"op_solicitudes");
            $info = $info[0];
             
            //generar liquidacion diaria
            $infoDes = mSolicitudes::infoDesembolso($array['id']);
            $infoDes = $infoDes[0];
            
            if(empty($infoDes['tasaM'])){
                $tasa = $infoDes['tasa_mensual'];
            }else{
                $tasa = $infoDes['tasaM'];
            }

            $fecha_des = formatoFecha($infoDes['fecha_desembolso']);
            $k = $infoDes['capital'];

            /*valido fecha de corte*/
            $mes_fin = date("m");
            //die($solicitud[0]["fecha_corte"].'='.$dia);
            
            $dias_cuadre = cal_days_in_month(CAL_GREGORIAN,$mes_fin,$ano);
            $diastotal = 0;
            
            $corte = (int) $solicitud[0]["fecha_corte"];
            $dia = (int) $dia;
            $cuota = 0;
            
            if($dia>($corte+1)){
                //die('uno');
                $dias_cuadre = cal_days_in_month(CAL_GREGORIAN,$mes_fin,$ano);
                $diastotal = difFechas($ano.'-'.$mes_fin.'-'.$dia, $ano.'-'.($mes_fin+1).'-'.$solicitud[0]["fecha_corte"]);
                $diastotal = $diastotal +1;
                
            }
            
            if($dia==($corte)+1){
                //die('dos');
                $dias_cuadre    = 0;
                $diastotal      = 0;//que paso??
            }
            
            if($dia==$corte){
                $cuota = 1;
                $dias_cuadre = cal_days_in_month(CAL_GREGORIAN,$mes_fin,$ano);
                $diastotal   = 0;
            }
            
            /*else{
                $dias_cuadre = cal_days_in_month(CAL_GREGORIAN,$mes_fin,$ano);
                $diastotal = 0;
            }*/
            //die($k.' '.($tasa/100).' '.$dias_cuadre);
            $interes_dia2 = (($k*($tasa/100))/$dias_cuadre);            
            $interes2     = ($k*($tasa/100))/$diastotal;
            
            //die('interes_dia='.$interes_dia2.'='.$k.'='.($tasa/100).'='.$dias_cuadre);
            
            /*calcular seguros*/
            //$otros  = unserialize($infoDes['otros']);
            $dias_c = ($dias_cuadre>0)?$dias_cuadre:$diastotal;
            $descon = $otros_descontable;

            $i=0;
            $j=0;
            foreach ($otros As $key=>$ot):
                if($descon[$i]=="1"){
                    $otros2[$key] = $k*($ot/100);
                    $j++;
                }
                $i++;
            endforeach;
            //die('fin');  
            mSolicitudes::saveDiario(
                    $array['id'],
                    $per,
                    $fechaIni2,
                    $k,
                    $interes_dia2,
                    $interes2,
                    serialize($otros2),
                    0,
                    opDate(),
                    //"2016-10-25",
                    $cuota,//cuota
                    $dias_c
                    );
            //fin diario
          
             notificaciones(5,'Se ha generado el desembolso para el cliente '.$infoCli[0]['nombre'].' '.$infoCli[0]['apellido'].' por valor de '.toMoney($info['monto']).' via '.$array['medio']);
	}else{
	  setMensaje('Error al crear el desembolso','error');
	}
 	
    location(setUrl('solicitudes','desembolso'));
    }
  
    
    public function crom ($ar=null){
        //die('pausa');
        $fecha_hoy = opDate();
        //$fecha_hoy = "2016-10-28";
        //die($fecha_hoy);
        /*for($ii=1;$ii<=22;$ii++){
            
        $valdia = dia($fecha_hoy)+$ii;
        $valdia = (strlen($valdia)==1)?"0".$valdia:$valdia;

        $fecha_hoy = "2016-08-".($valdia);*/
        //die('='.$fecha_hoy);
        
        $dia = dia($fecha_hoy);
        $mes_fin = mes($fecha_hoy);
        $ano = ano($fecha_hoy);
        /*
        consulto todos los desembolsos
         */
        $desembolsos = mSolicitudes::AllDesembolso();
        //die(print_r($desembolsos));
        $ok = 0;
        $errores = 0;
        foreach ($desembolsos As $des):
            //die($des['id']);
            //if($des['id_solicitud']=='17'){
            $infoDes = mSolicitudes::diario($des['id_solicitud']);
            $infoDes = $infoDes[0];
            //die(print_r($infoDes));
            //valido que tenga datos
            if(is_array($infoDes)){
                //valido que no sea el mismo dia
                //die($fecha_hoy." ".$infoDes['fecha_crom']);
                if($fecha_hoy>$infoDes['fecha_crom']){
                    //die('ok');
                    $cuota = $infoDes['cuota'];
                    //tasa
                    if(empty($des['tasaM'])){
                        $tasa = $des['tasa_mensual'];
                    }else{
                        $tasa = $des['tasaM'];
                    }
                    
                    $save       = false;
                    $cal_otros  = false;
                    $k = $infoDes['saldo_capital'];
                    //valido fecha para subir numero cuota
                    if($des['fecha_corte']==$dia){
                        //die('otro');
                        $cuota = $cuota +1;
                        if($cuota>1){
                            $cal_otros  = TRUE;
                            $save       = TRUE;
                            //valido saldo capital
                            $per        = $des['plazo_fin'];
                            //calculo de cuota
                            $num        = $k * $tasa;
                            $den        = 1 - pow((1+$tasa),-$per);
                            $cuota_fija = $num/$den;
                            //die('='.$infoDes['saldo_capital'].'-'.$cuota_fija.'-'.$infoDes['interes']);
                            $k2 = $infoDes['saldo_capital']-($cuota_fija-$infoDes['interes']);

                        }
                        $cuota_res = $des['cuota_res']-$cuota;
                    }
                   //die('='.$cuota);

                   $fecha_des = formatoFecha($des['fecha_desembolso']);
                   
                    /*valido fecha de corte*/
                    //die($dia.'-'.$des['fecha_corte']);
                    if($cuota==0){
                        if($des["fecha_corte"]==$dia){
                            //die('jum');
                            $dias_cuadre = 0;
                            $dias_interes = cal_days_in_month(CAL_GREGORIAN,$mes_fin,$ano);
                        }else{
                            //die('por ak');
                            $mes_fin = (strlen($mes_fin)<2)?"0".($mes_fin):($mes_fin);
                            $dias_interes = 0;
                            $dias_cuadre= $infoDes['dias_cal'];
                        }
                    }else{
                        if(($des["fecha_corte"]+1)==$dia){
                            //die('uno');
                            $dias_interes = cal_days_in_month(CAL_GREGORIAN,$mes_fin,$ano);
                            $dias_cuadre  = 0;
                        }else{
                            //die('dos');
                            //$dias_interes   = cal_days_in_month(CAL_GREGORIAN,$mes_fin,$ano);
                            $dias_interes   = $infoDes['dias_cal'];
                            $dias_cuadre    = 0;
                            //die('='.$dias_interes);
                        }
                    }
                    
                    //die('paso');
                    $interes_dia = ($k*($tasa/100))/$dias_cuadre;
                    if($dias_interes!=0){
                        $interes = ($k*($tasa/100))/$dias_interes;
                    }else{
                        $interes = 0;
                    }
                    //die("ajuste interes=".$interes_dia." interes=".$interes);
                   
                    /*calcular seguros*/
                    $otros  = unserialize($des['otros']);
                    
                    $descon = unserialize($des['otros_des']);
                    //die('='.print_r($descon));
                    $i=0;
                    $j=0;
                    foreach ($otros As $key=>$ot):
                        if($descon[$i]=="1"){
                            $otros2[$key] = $k*($ot/100);
                            $j++;
                        }
                        $i++;
                    endforeach;
                    
                    //die(print_r($otros2));
                    
                    $fecha_pago = $infoDes['fecha_corte'];
                    
                    /*if($save){
                        //nueva fecha de corte
                        $fecha_pago = strtotime ( '+1 month' , strtotime ( $fecha_pago ) ) ;
                        $fecha_pago = date ( 'Y-m-j' , $fecha_pago );
                        
                        die('='.$k2);
                        
                        mSolicitudes::saveDiario(
                        $des['id_solicitud'],
                        $cuota_res,
                        $fecha_pago,
                        $k2,
                        $interes_dia,
                        $interes,
                        serialize($otros2),
                        0,
                        opDate(),
                        0,
                        $dias_c
                        );
                    }*/

                    
                    
                    //guardo en la bd
                    mSolicitudes::updDiario(
                        $des['id_solicitud'],
                        $fecha_pago,
                        $k,
                        ($interes_dia+$infoDes['ajuste_interes']),
                        ($interes+$infoDes['interes']),
                        serialize($otros2),
                        0,
                        $fecha_hoy,
                        empty($cuota)?0:$cuota
                        );
                    $ok++;
                    //die('fin');
                }//if validar que ya esta reliquidado
                //die("ok no");
                unset($otros2);
                
            }else{
                $errores++;
                echo 'error';
            }
            
            //}//if by id
            
        endforeach;
            
        //enviar correo
        include_once 'app/sistema/mail.php';
        $correo=new mail();
        $correo->enviarCorreo(
        "haomaro1706@gmail.com,daromero51@gmail.com",
        "Liquidacion del crom",
        "Crom ejecutado con ".$ok." creditos reliquidados y ".$errores." errores en creditos fecha = ".  $fecha_hoy.' '.  opDate(TRUE)
        );
         
        
       
        //}//fin for
        
    }


    
     public function editar($array=null){
	validar_usuario();
        
        $pro = mSolicitudes::byid($array['id'],"op_solicitudes");
        $pro = $pro[0]['producto'];
        
        $pro = explode("-",$pro);
        if($pro[1]==2){
            $all = mSolicitudes::allId($array['id']);
        }else{
            $all = mSolicitudes::allIdSincupo($array['id']);
        }
	//$all = mSolicitudes::allId($array['id']);
	$all = $all[0];
	$all['elid']= $array['id'];
        
	$referencia=unserialize($all['referencia']);
	$vinculo=unserialize($all['vinculo']);
	$nombrer=unserialize($all['nombrer']);
	$apellidor=unserialize($all['apellidor']);
	$ccr=unserialize($all['ccr']);
	$correor=unserialize($all['correor']);
	$fechar=unserialize($all['fechar']);
	$dirr=unserialize($all['dirr']);
	$telr=unserialize($all['telr']);
	$celr=unserialize($all['celr']);
	$empresa=unserialize($all['empresar']);
	$cargor=unserialize($all['cargor']);
	$celr=unserialize($all['celr']);
        
        $tasa = mSolicitudes::bycampo("id_producto",$all["producto"],"op_parametros_tasas");
        $tasa = $tasa[0]["tasa_mensual"];
        
        if(!empty($all['tasaM'])){
           // die('entra');
           $tasa = $all['tasaM'];
        }
        //die('='.$tasa);
	parent::editar($all,$referencia,$vinculo,$nombrer,$apellidor,$ccr,$correor,$fechar,$dirr,
	$telr,$celr,$empresa,$cargor,$celr,$tasa);
    }
    
    public function editarProcess($array=null){
	validar_usuario();
	//die(print_r($array));
	//$producto= explode('-', $array['producto']);
	$id= explode('-', $array['lstClientes']);
	$id2= explode('-', $array['lstCodeudores']);
	$sucursal = empty($array['sucursal'])?getSucursal():$array['sucursal'];
	
	$crear1=mSolicitudes::updSolicitud(		
                $array['id'],
                $sucursal,
                $array['producto'],
                $array["responsable"],
                $array["cartera"],
                trim($id[0]),
                trim($id2[0]),
                $array["valor_solicitado"],
                $array["amortizacion"],
                $array["fecha_corte"],
                $array["plazo"],
                $array["latasa"]
                //($array["latasa"]!=$array["latasa2"])?$array["latasa"]:null
        );
        
	if($crear1){
		$crear2= mSolicitudes::updRefencias(
                            $array['id'],
                            serialize($array["referencia"]),
                            serialize($array["vinculo"]),
                            serialize($array["nombrer"]),
                            serialize($array["apellidor"]),
                            serialize($array["ccr"]),
                            serialize($array["correor"]),
                            serialize($array["fechar"]),
                            serialize($array["dirr"]),
                            serialize($array["telr"]),
                            serialize($array["celr"]),
                            serialize($array["empresar"]),
                            serialize($array["cargor"])
                    );
		if($crear2){
                    
                //enviar correo
                setMensaje("Solicitud editada correctamente","success");
                location(setUrl('solicitudes','lst'));
            }else{
                setMensaje('error al editar la solicitud', 'error');
                location(setUrl('solicitudes','lst'));
            }
		}else{
		  setMensaje('error al generar la solicitud', 'error');
		  location(setUrl('solicitudes','lst'));
		}
	
	}
	 
	  
    
    public function plazos($array=null){
	validar_usuario();
        
        $info = '';
        $tasas = '';
        if(!empty($array['producto'])){
	$info = mSolicitudes::lstPlazos($array['producto']);
        $tasas = mSolicitudes::lstTasas($array['producto']);
        }
	//print_r($tasas);
        $cadena ='<select name="plazo" id="plazo" onchange="cuotas(this)"><option value="">-Seleccione</option>';
        if(is_array($info)){
            foreach ($info As $i):
              $cadena .='<option value="'.$i['plazo'].'">'.$i['plazo'].'</option>';
            endforeach;
        }
	$cadena .= '</select>';
        if(is_array($info)){
        $cadena .='<input type="hidden" name="latasa" id="latasa" value="'.($tasas[0]["tasa_mensual"]).'">';
        $cadena .='<input type="hidden" name="seguroVida" id="seguroVida" value="'.($tasas[0]["seguroVida"]).'">';
        }
        echo $cadena;
    }
    
    public function valMonto($array=null){
	validar_usuario();
	$info = mClientes::listarId2($array['id']);
	//print_r($info);
	echo $info[0]['cupo_disponible'];
    }
    
    public function lstDesembolsos($array=''){
	validar_usuario();
	$info = mSolicitudes::desembolsos();
	//die(print_r($info));
	parent::lstDesembolsos($info);
	
	$id = empty($array['id'])?'':$array['id'];
	if(is_numeric($id)){
		$this->pdfs(array("id"=>$id,"extracto"=>true,"cheque"=>true));
	}
    }
    
    public function pdfs($array=null){
    	 $info3 = mSolicitudes::desembolsos2($array["id"]);
	  $info3 = $info3[0];
	  //die(print_r($info3));
	  $cheque = (isset($array["cheque"]) && $array["cheque"]) ? $this->cheque($array["id"]):'';
	  $extracto = (isset($array["extracto"]) && $array["extracto"]) ? $this->extracto($array["id"]):'';
		 // $cadena='';
	  	  $cadena='
			<html>

			<head>
			<meta http-equiv="Content-Type" content="text/html">
			
			<title>Cheque pago</title>
			<base href="http://prestarmas.co/prestarmas/">
			<style type="text/css">
				body{font-size: 11px; font-family: "Helvetica";}
				/*hr{border: 1px dashed #000000;}*/
				table{font-size: 7.5px;boder-collapse}
				#content {
				margin: 0.25in;
				width: 100%;
			}
			#inactivo td{background-color: #bbb;}
			.caja {
			 padding: 1em 3em;
			 margin: 1em 25%;
			}
			</style>
			</head>
			<body>';
		  $cadena .= ($cheque);
		    $cadena .= '<br/><hr><br/>';
		 $cadena .= ($extracto);
		    $cadena .= '
		    </body>
		    </html>';
		//echo($cadena);
	pdf($cadena,'app/files/clientes/'.trim($info3['cliente']).'/'.$info3['id_solicitud'].'/chequepago');
	return location('descargar.php?file=app/files/clientes/'.trim($info3['cliente']).'/'.$info3['id_solicitud'].'/chequepago.pdf');
    }
    
    public function cheque($id){
	
	$info2 = mSolicitudes::desembolsos2($id);
	$info2 = $info2[0];
	// $parametros = 
	$abono = mCartera::lstAbonos($info2["id_des"]);
	$abono = $abono[0];
	
	  $cadena ='';
	  $cadena.='<div style="float:left"><img src="app/img/logo.jpg" height="250px" width="250px"><br/>Informaci&oacute;n general de obligaci&oacute;n</div>';
	  $cadena.='<div ><table style="width:600px;">';
	  $cadena.='<tr>';
	  $cadena .='<td colspan="4" bgcolor="#D8D8D8"><b>Cliente:</b> <br/>'.$info2['nombre'].' '.$info2['apellido'].' '.$info2['cliente'].'</td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td bgcolor="#D8D8D8"><b>Credito</b></td>';
	  $cadena.='<td bgcolor="#D8D8D8"><b>Oficina</b></td>';
	  $cadena.='<td colspan="2" bgcolor="#D8D8D8"><b>Pague antes de </b></td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td>'.$id.'</td>';
	  $cadena.='<td>'.$info2['nombre_suc'].'</td>';
	  $cadena.='<td>'.$info2['fecha_pago'].'</td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td bgcolor="#D8D8D8"><b>Linea de credito</b></td>';
	  $cadena.='<td bgcolor="#D8D8D8"><b>Fecha desembolso</b></td>';
	  $cadena.='<td colspan="2" bgcolor="#D8D8D8"><b>Plazo</b></td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td>'.$info2['elpro'].'</td>';
	  $cadena.='<td>'.$info2['fecha_desembolso'].'</td>';
	  $cadena.='<td>'.$info2['plazo_fin'].'</td>';
	  
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td bgcolor="#D8D8D8"><b>Tasa EA</b></td>';
	  $cadena.='<td bgcolor="#D8D8D8"><b>Tasa nominal</b></td>';
	  $cadena.='<td colspan="2" bgcolor="#D8D8D8"><b>Tasa mensual</b></td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';

	  $cadena.='<td>'.round($info2['tasa_ea'],2).' %</td>';
	  $cadena.='<td>'.round($info2['tasa_nominal'],2).' %</td>';
	  $cadena.='<td>'.round($info2['tasa_mensual'],2).' %</td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td bgcolor="#D8D8D8"><b>Tasa mora</b></td>';
	  $cadena.='<td bgcolor="#D8D8D8"><b>Total a pagar</b></td>';
	  //$cadena.='<td><b>Tasa cobranza</b></td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td>3.5 %</td>';
	  $cadena.='<td><b><font size="+3">'.toMoney($info2['total']).'</font><b/></td>';
	  $cadena.='</tr>';
	  $cadena.='</table>';
	  
	  $cadena.='<table class="table" >';
	  $cadena.='<tr colspan="2">';
	  $cadena .='<td><b>Detalle del pago a realizar</b></td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td><b>Descripci&oacute;n</b></td>';
	  $cadena.='<td><b>Valor</b></td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td>Capital</td>';
	  $cadena.='<td>'.toMoney($info2['capital']).'</td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td>Costos administrativos</td>';
	  $cadena.='<td>'.toMoney($info2['admi1']+$info2['admi2']).'</td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td>Inter&eacute;s Corriente</td>';
	  $cadena.='<td>'.toMoney($info2['interes_corriente']).'</td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td>Inter&eacute;s Mora</td>';
	  $cadena.='<td>'.toMoney($info2['interes_mora']).'</td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td>Honorarios cobranza</td>';
	  $cadena.='<td>'.toMoney($info2['cobranza']).'</td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td>Recaudo proveedor</td>';
	  $cadena.='<td>'.toMoney($info2['recaudo']).'</td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td>I.V.A</td>';
	  $cadena.='<td>'.toMoney($info2['iva']).'</td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td>Valor abonos</td>';
	  $cadena.='<td>'.toMoney($abono['abono']).'</td>';
	  $cadena.='</tr>';
	  $cadena.='</table></div>';
	  
	/*  $cadena.='<br/><br/><center>* Voucher emitido por Prestarmas el cual debe ser utilizado en su totalidad no se entregara dinero en efectivo por valores menores, el valor restante debera
ser asumido por el solicitante</center>
	    ';*/
	  
	  $fecha= explode('/',$info2['fecha_pago']);
	  $total_pagar = explode('.',$info2['total']);
	  
	  if(strlen($total_pagar[0])<10){
	    $ceros=10-(strlen($total_pagar[0]));
	    $ceros1='';
	    for($i=0;$i<$ceros;$i++){
		$ceros1=$ceros1.'0';
	    }
	  }
	  if(strlen($info2['id_des'])<10){
	    $ceros=10-(strlen($info2['id_des']));
	    $ceros2='';
	    for($i=0;$i<$ceros;$i++){
		$ceros2=$ceros2.'0';
	    }
	  }
	  
	  $barras2 = '41579012345678908020'.$ceros2.$info2['id_des'].'3900'.$ceros1.$total_pagar[0].'96'.$fecha[2].$fecha[1].$fecha[0];
	 
	  $cadena .= '<br/><br/><center><img src="http://www.prestarmas.co/prestarmas/app/librerias/php/ImgBarcode/generador.php?code='.$barras2.'"></center>';
	  return $cadena;
	 
    }
    
    public function extracto($id){
    
    	  $info2 = mSolicitudes::desembolsos2($id);
	  $info2 = $info2[0];
	  
	  
	  /*creo el pdf*/
	  
	  $cadena.='<div style="float:left"><img src="app/img/logo.jpg" height="250px" width="250px"></div>';
	  $cadena.='<div ><table class="table" style="width:600px;">';
	  $cadena.='<tr>';
	  $cadena.='<td bgcolor="#D8D8D8"><b>Nombre cliente: </b></td>';
	  $cadena.='<td>'.$info2['nombre'].' '.$info2['apellido'].'</td>';
	  $cadena.='<td bgcolor="#D8D8D8"><b>Credito : </b></td>';
	  $cadena.='<td>'.$id.'</td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td bgcolor="#D8D8D8"><b>Identificaci&oacute;n: </b></td>';
	  $cadena.='<td>'.$info2['cliente'].'</td>';
	  $cadena.='<td bgcolor="#D8D8D8"><b>Fecha desembolso : </b></td>';
	  $cadena.='<td>'.$info2['fecha_desembolso'].'</td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td bgcolor="#D8D8D8"><b>Direcci&oacute;n: </b></td>';
	  $cadena.='<td>'.$info2['direccion'].'</td>';
	  $cadena.='<td></td>';
	  $cadena.='<td></td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td bgcolor="#D8D8D8"><b>Telefono: </b></td>';
	  $cadena.='<td>'.$info2['telefono'].'</td>';
	  $cadena.='<td bgcolor="#D8D8D8"><b>Tasa : </b></td>';
	  $cadena.='<td>'.round(($info2['tasa']*100),2).'%</td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td bgcolor="#D8D8D8"><b>Celular: </b></td>';
	  $cadena.='<td>'.$info2['celular'].'</td>';
	  $cadena.='<td bgcolor="#D8D8D8"><b>Plazo : </b></td>';
	  $cadena.='<td>'.$info2['plazo_fin'].'</td>';
	  $cadena.='</tr>';
	  /*$cadena.='<tr>';
	  $cadena.='<td bgcolor="#D8D8D8">I.V.A</td>';
	  $cadena.='<td>'.toMoney($info2['iva']).'</td>';
	  $cadena.='<td></td>';
	  $cadena.='<td></td>';
	  $cadena.='</tr>';*/
	  $cadena.='</table></div>';
	  $cadena.='<br/><br/><center><b>Monto autorizado '.toMoney($info2['monto']).'</b></center>';
	  
	   $fecha= explode('/',$info2['fecha_pago']);
	  $total_pagar = explode('.',$info2['total']);
	  
	  if(strlen($total_pagar[0])<10){
	    $ceros=10-(strlen($total_pagar[0]));
	    $ceros1='';
	    for($i=0;$i<$ceros;$i++){
		$ceros1=$ceros1.'0';
	    }
	  }
	  if(strlen($info2['id_des'])<10){
	    $ceros=10-(strlen($info2['id_des']));
	    $ceros2='';
	    for($i=0;$i<$ceros;$i++){
		$ceros2=$ceros2.'0';
	    }
	  }
	  
	  $barras2 = '41579012345678908020'.$ceros2.$info2['id_des'].'3900'.$ceros1.$total_pagar[0].'96'.$fecha[2].$fecha[1].$fecha[0];
	 
	  $cadena .= '<br/><br/><center><img src="http://www.prestarmas.co/prestarmas/app/librerias/php/ImgBarcode/generador.php?code='.$barras2.'"></center>';
	  
	  return $cadena;

    }
    
    
    public function generarPdf($array=null){
    	$id = empty($array['id'])?'':$array['id'];
	
	  $info3 = mSolicitudes::desembolsos2($id);
	  $info3 = $info3[0];
	 
	  $cheque = $this->cheque($id);
	  $extracto = $this->extracto($id);
		  $cadena='';
	  	  $cadena.='<!DOCTYPE html>
 
			<html lang="es">

			<head>
			<style>
				body{font-size: 11px; font-family: "Helvetica";}
				hr{border: 1px dashed #000000;}
				table{font-size: 7.5px;}
				#content {
				margin: 0.25in;
				width: 100%;
			}
			#inactivo td{background-color: #bbb;}
			</style>
			<title>Cheque pago</title>
			<base href="http://localhost:7979/prestarmas/">
			
			<!--<link rel="stylesheet" href="app/librerias/bootstrap/css/bootstrap.css" />-->
			<body><div id="#content">';
		    $cadena .= $cheque;
		    $cadena .= '<br/><hr><br/>';
		    $cadena .= $extracto;
		    $cadena .= '</div>
		    </body>
		    </html>';
		   // echo 'omarrr '.$cadena;
		  //  die('app/files/clientes/'.trim($info3['cliente']).'/'.$info3['id_solicitud'].'/chequepago');
	pdf($cadena,'app/files/clientes/'.trim($info2['cliente']).'/'.$info2['id_solicitud'].'/chequepago');
	location('descargar.php?file=app/files/clientes/'.trim($info2['cliente']).'/'.$info2['id_solicitud'].'/chequepago.pdf');
	
    }
    
    public function archivos($array=null){
        //die(print_r($array));
    	include_once 'app/sistema/ftp.php';
    	
    	$ftp=new ftp();
    	$info1 = $ftp->ficheros('./app/files/clientes/'.$array['cliente'].'/solicitudes/'.$array['solicitud'].'/');
    	
    	parent::archivos($info1,$array['cliente'],$array['solicitud']);
    	
    }
    
    public function archivos2($array=null){
        //die(print_r($array));
    	include_once 'app/sistema/ftp.php';
    	
    	$ftp=new ftp();
    	$info1 = $ftp->ficheros('./app/files/clientes/'.$array['cliente'].'/solicitudes/'.$array['solicitud'].'/');
    	
    	parent::archivos2($info1,$array['cliente'],$array['solicitud']);
    	
    }
    
        public function archivos3($array=null){
        //die(print_r($array));
    	include_once 'app/sistema/ftp.php';
    	
    	$ftp=new ftp();
    	$info1 = $ftp->ficheros('./app/files/clientes/'.$array['cliente'].'/solicitudes/'.$array['solicitud'].'/');
    	
    	parent::archivos3($info1,$array['cliente'],$array['solicitud']);
    	
    }
    
        public function archivos_cupo($array=null){
        //die(print_r($array));
    	include_once 'app/sistema/ftp.php';
    	
        $info = mSolicitudes::byId($array['id'],"op_solicitudes_cupo");
        $info = $info[0];
        
        //die('./app/files/clientes/'.$info['cliente_id'].'/cupos'.$array['id'].'/');
        
    	$ftp=new ftp();
    	$info1 = $ftp->ficheros('./app/files/clientes/'.$info['cliente_id'].'/cupos'.$array['id'].'/');
    	
    	parent::archivos_cupo($info1,$info['cliente_id'],$array['solicitud']);
    	
    }
    
    public function pruebacorreo(){
	include_once 'app/sistema/mail.php';
		    $correo=new mail();
		    $correos = mParametros::lstCorreos();
		    //die(print_r($correos));
		   /* $cadena='';
		    foreach($correos As $c){
		    	$cadena[]= $c['email'];
		    }*/
		  //$cadena = implode(",",$cadena);
$correo->enviarCorreo('haomaro1706@gmail.com','Nueva solicitud de credito',' este es un correo de prueba');
		  
    }
    
    public function eliminarDoc($array){
    //die("app/files/clientes/".$array['cc'].'/solicitudes/'.$array['folder'].'/'.$array['file']);
    	unlink("app/files/clientes/".$array['cc'].'/solicitudes/'.$array['folder'].'/'.$array['file']);
		setMensaje('Archivo eliminado correctamente','success');
		location(setUrl('solicitudes','archivos',array('cliente'=>$array['cc'],'solicitud'=>$array['folder'])));
    }
    
        public function eliminarDoc2($array){
    //die("app/files/clientes/".$array['cc'].'/solicitudes/'.$array['folder'].'/'.$array['file']);
    	unlink("app/files/clientes/".$array['cc'].'/solicitudes/'.$array['folder'].'/'.$array['file']);
		setMensaje('Archivo eliminado correctamente','success');
		location(setUrl('solicitudes','archivos2',array('cliente'=>$array['cc'],'solicitud'=>$array['folder'])));
    }
    
            public function eliminarDoc3($array){
    //die("app/files/clientes/".$array['cc'].'/solicitudes/'.$array['folder'].'/'.$array['file']);
    	unlink("app/files/clientes/".$array['cc'].'/solicitudes/'.$array['folder'].'/'.$array['file']);
		setMensaje('Archivo eliminado correctamente','success');
		location(setUrl('solicitudes','archivos3',array('cliente'=>$array['cc'],'solicitud'=>$array['folder'])));
    }
  
    public function eliminarDocCli($array){
    //die("app/files/clientes/".$array['folder'].'/'.$array['file']);
    	unlink("app/files/clientes/".$array['folder'].'/'.$array['file']);
		setMensaje('Archivo eliminado correctamente','success');
		location(setUrl('clientes','verCliente',array('id'=>$array['folder'])));
    }
  
    
  public function listarServicios2($array=null){
  	validar_usuario();
  	$info = mParametros::servicioId($array['id']);
  	$cadena='<div class="control-group">
					<label class="control-label" for="tipoRecibo">Tipo de recibo * : </label>
					<div class="controls">
						<select name="tipoRecibo" id="tipoRecibo">
						<option value="">-Seleccione-</option>';
	foreach(unserialize($info[0]['servicios']) As $key=>$i){
		$cadena.='<option value="'.$i.'">'.strtoupper($i).'</option>';
	}
	echo $cadena.'</select></div></div>';
  }
  
  public function cargarDocs($array=null){
  	validar_usuario();
  	$cliente=$array["cliente"];
  	$solicitud=$array["solicitud"];
  	parent::cargarDocs($cliente,$solicitud);
  }
  
    public function cargarDocs2($array=null){
  	validar_usuario();
  	$cliente=$array["cliente"];
  	$solicitud=$array["solicitud"];
  	parent::cargarDocs2($cliente,$solicitud);
  }
  
  public function cargarDocs3($array=null){
  	validar_usuario();
  	$cliente=$array["cliente"];
  	$solicitud=$array["solicitud"];
  	parent::cargarDocs3($cliente,$solicitud);
  }

  public function cargarDocs_cupo($array=null){
  	validar_usuario();
        
        $info = mSolicitudes::byId($array['id'],"op_solicitudes_cupo");
        $info = $info[0];
        
  	$cliente=$info["cliente_id"];
  	$solicitud=$array["id"];
  	parent::cargarDocs_cupo($cliente,$solicitud);
  }
  
  public function clienteId($array=null){
  	validar_usuario();
  	
  	$info = mSolicitudes::infoUser($array['id']);
  	$cadena = '<div class="control-group">
				<label for="lstClientes" class="control-label">Credito a abonar : </label>
				<div class="controls">
  	<select name="creditos"><option value="">-Seleccione-</option>';
  	foreach($info As $i){
  		$cadena .= '<option value="'.$i['id'].'">'.$i['id'].'</option>';
  	}
  	$cadena .= '</select></div>
			</div>';
  	echo $cadena;
  }
  
  public function planpagos($array=null){
      validar_usuario();
      $info = mSolicitudes::infoCuotas($array["id"]);
      $info2 = mSolicitudes::infoDesembolsoDesc($array["id"]);
      //die(print_r($info));
      parent::planPagos($info,$info2,$array['id']);
  }
  
    public function ecuenta($array=null){
        validar_usuario();
        $info = mSolicitudes::infoCuotas2($array["id"]);
        $info2 = mSolicitudes::infoDesembolsoDesc($array["id"]);
        //die(print_r($info));
        parent::ecuenta($info,$info2);
    }
  
  public function cupo($array=null){
      validar_usuario();
      $info = mSolicitudes::infoCupo();
      parent::cupo($info);
      
      if(isset($array['id'])){
          $info2 = mSolicitudes::infoCupo($array['id_so']);
          $info2 = $info2[0];
          $fecha = explode('-',$info2['fecha']);
          $fecha = $fecha[2].'/'.$fecha[1].'/'.$fecha[0];
         // die(print_r($info2));
          $cli = mSolicitudes::infoCliente($info2['cliente_id']);
          $cli = $cli[0];
          
          $largo = strlen($info2['id']);
          
          if($largo==1){
              $id = '0000'.$info2['id'];
          }
          if($largo==2){
              $id = '000'.$info2['id'];
          }
          if($largo==3){
              $id = '00'.$info2['id'];
          }
          if($largo==4){
              $id = '0'.$info2['id'];
          }
          if($largo>4){
              $id = $info2['id'];
          }
          
          $cadena='
			<html>
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-2">
			
			<title>Cheque pago</title>
			<base href="http://localhost/serprogreso/">
			<style type="text/css">
                            .cuerpo {
                                font-size: 10px;
                                font-family: "Helvetica";
                                margin-top: 80px;
                                margin-right: 70px;
                                margin-bottom: 20px;
                                margin-left: 50px;
                                text-align: justify !important;
                                /*background-color: yellow;*/
                                padding: 2px 2px 2px 2px;
                            }
                            #logo{float:left;}
			</style>
			</head>
		<body><div class="cuerpo">';
		  
        $cadena .= '
<img src="app/img/logo2.png" id="logo">
<p>'.$info2['fecha_apro'].'<br /><br />
<b>    
Se&ntilde;or(es):<br />
'.$info2['cliente_nombre'].'<br />

Ciudad<br /><br /><br />
Estimado Sr(a):<br /><br />
</b>
Nos permitimos informarles que el '.$info2['fecha_apro'].' hemos aprobado un '
. 'cupo de cr&eacute;dito rotativo, por <br/>valor de '.toMoney($info2['cupo']).' ('.numeroLetras($info2['cupo']).' Mcte) el cual podr&aacute; ser utilizado en cualquier<br/> momento, previa autorizaci&oacute;n
del representante legal o titular del cr&eacute;dito. Todos lo desembolsos que se realicen se pondr&aacute;n a un plazo m&iacute;nimo de 36 meses, pero en cualquier momento podran realizar el pago total del saldo.<br /><br />

El procedimiento para el uso de este cupo es el siguiente:<br /><br />
<ol>
1. Diligenciar el formato de solicitud de desembolso. Debe venir firmado por la persona autorizada seg&uacute;n el formato "control de firmas autorizadas".<br />
    a. El n&uacute;mero de aprobaci&oacute;n del cupo rotativo es el <b>'.$id.'</b> (Este n&uacute;mero siempre se debe identificar en la solicitud de desembolso).<br />
2. Firmar el pagar&eacute; asociado a la solicitud de desembolso.<br />
3. Enviar f&iacute;sicamente la solicitud a nuestra oficina.<br />
</ol>
<br /><br />
Los datos de env&iacute;o de correspondencia y estados de cuenta son:<br />
<ol TYPE="circle">
<li> Tipo de documento: '.(is_numeric($cli['tipo_doc'])?'NIT':$cli['tipo_doc']).'.
<li> No. de documento de identidad: '.$info2['cliente_id'].'
<li> Nombre del titular : '.$info2['cliente_nombre'].'
<li> Tel&eacute;fono: '.$cli['tel'].'
<li> Correo electr&oacute;nico: '.$cli['correo'].'
</ol>
<br /><br />

Cordialmente,<br />
Servicio al cliente</p><br/><br/>

<p><b><center>Calle 6 Norte # 2N – 36 Centro Comercial Campanario. Oficina 104. Tel: (2) 370 98 88
servicioalcliente@serprogreso.com</center></b></p>';
          
		    $cadena .= '
		    </div>
                    </body>
		    </html>';
		   //die($cadena); 

	pdf($cadena,'app/files/clientes/'.trim($array['id']).'/solicitud_cupo');
        return location('descargar.php?file=app/files/clientes/'.trim($array['id']).'/solicitud_cupo.pdf');
      }
      
  }
  
public function cupoprocess($array=null){
    validar_usuario();

    $cliente = explode('-',$array['lstClientes']);
    $cupo =  str_replace('.','',$array['cupo']);
    
    $val_cli = mSolicitudes::byId($cliente,"op_solicitudes_cupo");
    $val_cli = $val_cli[0];
    
    if($val_cli['cliente_id']==$cliente){
        $mensaje = ($val_cli['estado']=='0')?'solicitado':'aprobado';
        setMensaje("Ya tiene un solicitud de cupo en estado ".$mensaje,"info");
        location(setUrl("solicitudes","cupo"));               
    }
    
    
    $save = mSolicitudes::crearCupo(
            trim($cliente[0]),
            trim($cliente[1]),
            $cupo,
            $array['estudio'],
            $array['concepto']
            );
      
    $ultimo = mSolicitudes::ultimo('op_solicitudes_cupo');
    $id_solicitud = $ultimo['id'];
//die('prueba');
    if($save){
        $save2= mSolicitudes::crearRefencias2(
                          serialize($array["referencia"]),
                          serialize($array["vinculo"]),
                          serialize($array["nombrer"]),
                          serialize($array["apellidor"]),
                          serialize($array["ccr"]),
                          serialize($array["correor"]),
                          serialize($array["fechar"]),
                          serialize($array["dirr"]),
                          serialize($array["telr"]),
                          serialize($array["celr"]),
                          serialize($array["empresar"]),
                          serialize($array["cargor"]),
                          $id_solicitud
                  );
        if($save2){
            //subir archivos
            $total = count($array["adjuntos1"]["name"]);
            //$total2 = count($array["adjuntos1"]["name"]);
            
            if($total > 0){
                include_once 'app/sistema/ftp.php';
                $ftp    = new ftp();
                
                for($i=0;$i<$total;$i++){
                $local1 = limpiaradjuntos(trim($array["adjuntos1"]["name"][$i]));// el nombre del archivo	
                $remoto1 = $array["adjuntos1"]["tmp_name"][$i];// Este es el nombre temporal del archivo mientras dura la transmision	
                $tama1 = $array["adjuntos1"]["size"][$i];

                $url    = 'app/files/clientes/'.trim($cliente[0]).'/cupos'.$id_solicitud;
                mkdir($url,0777);
                $ftp->SubirArchivo($local1,$remoto1,$tama1,$url,strtolower($id_solicitud));
                }
            }
            //die('validar');
            //enviar correos
            notificaciones(2,'El cliente '.$cliente[1].' Realizo una solicitud de cupo por valor de '.toMoney($cupo));
            setMensaje('Solicitud de cupo creada correctamente.','success');
        }
    }else{
        setMensaje('Error al crear la solicitud de cupo.','error');
    }
    location(setUrl('solicitudes','cupo'));
  }
  
  public function editCupo($array = null) {
      validar_usuario();
      $info = mSolicitudes::infoCupo($array['id']);
      parent::editCupo($info[0]);
  }
  
  public function ediatCupopro($array=null){
      validar_usuario();
      $upd = mSolicitudes::updCupo($array['id'],$array['cupo']);
      
      if($upd){
          setMensaje('Solicitud de cupo editada correctamente.','success');
      }else{
          setMensaje('Error al editar la solicitud de cupo.','error');
      }
      location(setUrl('solicitudes','cupo'));
  }
  
  public function delCupo($array=null){
      validar_usuario();
      parent::delCupo($array['id']);
  }
  
  public function delCupoPro($array=null){
      validar_usuario();
      
      mSolicitudes::updCupoRecha($array['id'],$array['por']);
      $del = mSolicitudes::rechazarCupo($array['id']);

      if($del){
          $info = mSolicitudes::infoCupo($array['id']);
          $info = $info[0];

          //notificaciones(10, 'Se rechazo la solicitud del cliente '.$info['cliente_nombre'].' Por valor de '.toMoney($info['cupo']));
          
          setMensaje('Solicitud de cupo rechazada correctamente.','success');
      }else{
          setMensaje('Error al rechazar la solicitud de cupo.','error');
      }
      location(setUrl('solicitudes','cupo'));
  }
  
  public function aprobarCupo($array=null){
      validar_usuario();
      parent::aprobarCupo($array['id']);
  }
  
  public function aprobarCupoPro($array=null){
      validar_usuario();
      $info = mSolicitudes::infoCupo($array['id']);
      $info = $info[0];
      
      if($info['estado']=="0"){
        mSolicitudes::updCupoRecha($array['id'],$array['por']);

        $upd = mSolicitudes::aprobarCupo($info['cliente_id'],$info['cupo']);

        if($upd){
            mSolicitudes::aprobarCupo2($array['id']);
            notificaciones(9,'Se aprobo el cupo del cliente '.$info['cliente_nombre'].' por valor de '.  toMoney($info['cupo']));
            setMensaje('Solicitud de cupo aprobada correctamente.','success');
        }else{
            setMensaje('Error al aprobar la solicitud de cupo.','error');
        }
      }else{
          setMensaje('El cliente ya tiene un cupo aprobado.','info');
    }
    location(setUrl('solicitudes','cupo'));
  
  }
  
  public function listarCupo($array=null){
      $id = trim($array['id']);
      
      $info = mSolicitudes::cupoCliente($id);
      $info = $info[0];
      
      if(is_array($info)){
          echo toMoney($info['cupo_disponible']);
      }else{
          echo toMoney('0');
      }
  }
  
  public function concepto($array=null){
      validar_usuario();
      $info = mSolicitudes::verconcepto($array['id']);
      $id = $array['id'];
      if(is_array($info)){
          $url = 'editarEstudio';
      }else{
         $url = 'crearEstudio';
         $info = null;
      }
      parent::concepto($info[0], $url, $id);
  }
  
    public function crearEstudio($array=null){
        validar_usuario();

        $save = mSolicitudes::guardarEstudio($array['id'],$array['estudio'],$array['concepto']);
      
        if($save){
          setMensaje('Estudio guardado correctamente.','success');
        }else{
            setMensaje('Error al guardar el estudio.','error');
        }
        location(setUrl('solicitudes','ver',array('id'=>$array['id'])));
    }
  
    public function editarEstudio($array=null){
        validar_usuario();
        
        $upd = mSolicitudes::updEstudio($array['id'],$array['estudio'],$array['concepto']);
        
        if($upd){
          setMensaje('Estudio actualizado correctamente.','success');
        }else{
            setMensaje('Error al actualizar el estudio.','error');
        }
        location(setUrl('solicitudes','ver',array('id'=>$array['id'])));
    }
    
    public function cerrarconcepto($array=null){
        validar_usuario();
        
        $cerrar = mSolicitudes::cerrarEstudio($array['id']);
        $info = mSolicitudes::infoSolicitud($array['id']);
        $info = $info[0];
        
        if($cerrar){
            mSolicitudes::aprobar($array['id'],1);
            //envio correo
            notificaciones(7,'Concepto del estudio de credito numero '.$array['id'].' cerrado');
          setMensaje('Estudio cerrado correctamente.','success');
        }else{
            setMensaje('El estudio ya esta cerrado.','info');
        }
        location(setUrl('solicitudes','concepto',array('id'=>$array['id'])));
    }
    
} // class
													
