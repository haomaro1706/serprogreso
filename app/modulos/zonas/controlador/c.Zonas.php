<?php 

include_once 'app/modulos/zonas/vista/v.Zonas.php';
include_once 'app/modulos/zonas/modelo/m.Zonas.php';

class Zonas extends vZonas{

    public function main(){
		validar_usuario();
		$lista = mZonas::listar();
		if(empty($lista)){
			setMensaje("Actualmente no existen zonas creadas","info");
		}
		
        parent::main($lista);    
    } //  main
	
	
	public function saveZona($array){
		validar_usuario();
		if(!empty($array["zona"])){
			$info=mZonas::selectbyZona($array["zona"]);
			if(!empty($info)){
				setMensaje("Esta zona ".$info[0]["zona"]." ya existe, inténtelo de nuevo con nuevos datos.","error");
			}else{
				$save = mZonas::saveZona($array["zona"], $array["estado"]);
			if($save)
				setMensaje("Zona guardada exitosamente","success");
			else
				setMensaje("Error interno al intentar guardar la zona","error");
			}
		}else{
			setMensaje("Debe completar toda la informaci&oacute;n","error");
		}
		location(setUrl("zonas"));
	}
	
	public function editZona($array=null){
	  validar_usuario();
	  
	  $lista = mZonas::listarId($array['id']);
	  if(empty($lista)){
		  setMensaje("Actualmente no existen zonas creadas","info");
	  }
	  parent::editZona($lista[0]);
	}//editZona
	
	public function editZonapro($array=null){
	  validar_usuario();

		if(!empty($array["zona"]) && !empty($array["id"])){
			$info=mZonas::selectbyZona($array["zona"]);
			if(!empty($info)){
				if($info[0]["id"]!=$array["id"]){
					setMensaje("Esta zona ".$info[0]['zona']." ya existe, intentelo de nuevo con nuevos datos." ,"error");
				}else{
					$upd = mZonas::updZona($array['id'], $array["zona"], $array["estado"]);
					if($upd)
						setMensaje("Zona editada exitosamente","success");
					else
						setMensaje("Error interno al intentar editar la zona","error");
				}
			}else{
				$upd = mZonas::updZona($array['id'], $array["zona"], $array["estado"]);
				if($upd)
					setMensaje("Zona editada exitosamente","success");
				else
						setMensaje("Error interno al intentar editar la zona","error");
			}
		}else{
			setMensaje("Debe completar toda la informaci&oacute;n","error");
		}
		location(setUrl("zonas"));
	}//editZonapro
	
	public function delZona($array=null){
	  validar_usuario();
	  valId($array['id'],'productos');

	  $del = mZonas::delZona($array['id']);

	  if($del)
	    setMensaje("Zona eliminada exitosamente","success");
	  else
	    setMensaje("Error interno al intentar eliminar la zona","error");

	  location(setUrl("zonas"));
	}
    
} // class
?>
