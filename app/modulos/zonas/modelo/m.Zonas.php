<?php 

class mZonas{
	
	public function listar(){
		$orm = new ORM();
		$sql = $orm	->select()
					-> from ("op_zonas");
		$return = $orm	->exec()
						->fetchArray();
		return $return;
	} // listar
	
	
	public function saveZona($zona, $estado){
		$orm = new ORM();
		$sql = $orm	-> insert("op_zonas")
					-> values(array(0, $zona, $estado));
		$return = $orm	-> exec()
						-> afectedRows();
		return $return > 0 ? true : false;
		
	}
	
	public function updZona($id,$zona,$estado){
	  $orm = new ORM();
		$sql = $orm	-> update("op_zonas")
					-> set(
						  array(
						   "zona"	  => $zona,
						   "estado"	  => $estado
						  )
						)
					-> where("id = ?", $id);
        $orm -> exec();
		$retorno = $orm -> afectedRows() > 0 ? true : false;
        return $retorno;
	}
	
	public function listarId($id){
		$orm = new ORM();
		$sql = $orm	->select()
				-> from ("op_zonas")
				->where('id =?',$id);
		$return = $orm	->exec()
						->fetchArray();
		return $return;
	} // listarId
	
	public function delZona($id){
	  $orm = new ORM();
		$sql = $orm -> delete()
					-> from("op_zonas")
					-> where("id = ? ", $id);
		$orm -> exec();
		$retorno = $orm -> afectedRows() > 0 ? true : false;
        return $retorno;
	}//delZona
			
	public function selectbyZona($zona){
		$orm = new ORM();
		$orm	->select()
				->from("op_zonas")
				->where("zona =?",$zona);
		$return = $orm->exec()->fetchArray();
		return $return;
	}		
	
        	public function listarZonaId($id){
		$orm = new ORM();
		$sql = $orm	->select()
				-> from ("op_sucursales")
				->where('id =?',$id);
		$return = $orm	->exec()
						->fetchArray();
		return $return;
	} // listarId
        
} // class
