<?php 

class vZonas{
    public function main($zonas){
      echo getMensaje();
        ?>
           <h2><?php  echo ZONAS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  ZONAS_NOMBRE=>'#'
            		 ),
            	    ZONAS_NOMBRE
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  ZONAS_NOMBRE=>'#'
            		 ),
            	    ZONAS_NOMBRE, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php echo ZONAS_NOMBRE?></h4>
       <div class='container'>
        <div class='span8 offset1'> 
			<fieldset>
				<legend><?php echo ZONAS_CREAR?></legend>
			<?php
				Html::openForm("zonas",setUrl("zonas","saveZona"));
				Html::newInput("zona","Zona *");
				Html::newSelect("estado", "Estado *", array("1"=>"Activo", "0"=>"Inactivo"));
			?>
			</fieldset>
			<?php
				Html::newButton("enviar", "Enviar", "submit");
				Html::closeForm();
			?>
			<?php
				if(!empty($zonas)){
					echo datatable("tabla");
					Html::tabla(	array("Zona","Estado","Editar","Eliminar"),
									$zonas,
									array("zona","estado"),
									array("editar"=>setUrl("zonas","editZona"),"eliminar"=>setUrl("zonas","delZona"))
								);
				}
			?>
        </div>
       </div>
       <?php  
    }
    
    public function editZona($zonas){
      echo getMensaje();
        ?>
           <h2><?php  echo ZONAS_EDITAR?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('zonas'), 
            		  ZONAS_EDITAR=>'#'
            		 ),
            	    ZONAS_EDITAR
            	   );
            
            navTabs(array(OP_BACK=>setUrl('zonas'), 
            		  ZONAS_EDITAR=>'#'
            		 ),
            	    ZONAS_EDITAR, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php echo ZONAS_EDITAR?></h4>
       <div class='container'>
        <div class='span8 offset1'> 
			<fieldset>
				<legend><?php echo ZONAS_EDITAR?></legend>
			<?php
				Html::openForm("zonas",setUrl("zonas","editZonapro"));
				Html::newInput("zona","Zona *",$zonas['zona']);
				Html::newSelect("estado", "Estado *", array("1"=>"Activo", "0"=>"Inactivo"),$zonas['estado']);
			?>
			</fieldset>
			<?php
				Html::newHidden("id",$zonas['id']);
				Html::newButton("enviar", "Enviar", "submit");
				Html::closeForm();
			?>
			
        </div>
       </div>
       <?php  
    }//editZona
    
} // class
        ?>
