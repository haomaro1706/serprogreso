<?php 
/*Variables globales para el modulo lang/*/
define('BANCOS_NOMBRE','Bancos');
define('BANCOS_CREAR','Crear Banco');
define('BANCOS_EDITAR','Bancos');
define('BANCOS_MEDIOS','Medios de pago');
define('BANCOS_CREAR_MEDIOS','Crear medio de pago');
define('BANCOS_EDITAR_MEDIOS','Editar medio de pago');
define('BANCOS_TC','Tipos de cuenta');
define('BANCOS_TC_CREAR','Crear tipo de cuenta');
define('BANCOS_TC_EDITAR','Editar tipo de cuenta');
