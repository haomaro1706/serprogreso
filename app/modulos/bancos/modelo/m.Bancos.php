<?php 

class mBancos{
    
    public function crearBanco($banco){
        $orm = new ORM();
        $sql = $orm -> insert("op_bancos")
                                -> values(array(0, $banco,opDate()));
        return $sql -> exec() -> afectedRows();
    }
    
    public function bancos(){
        $orm = new ORM();
        $sql = $orm -> select()
                    -> from ("op_bancos");
        return $sql -> exec() -> fetchArray();
    }
    
    public function bancosId($id){
        $orm = new ORM();
        $sql = $orm -> select()
                    -> from ("op_bancos")
                    -> where("id = ? ",$id);
        return $sql -> exec() -> fetchArray();
    }
    
    public function updBanco($id,$banco){
	  $orm = new ORM();
		$query = $orm	-> update("op_bancos")
                                -> set (array(
                                            "banco"=>$banco
                                            )
                                        )
                                -> where("id=?",$id);
		$return = $query->exec() -> afectedRows();
		
		return $return;		
    }
    
    public function delBanco($id){
        $orm = new ORM();
        $orm	-> delete()
                -> from("op_bancos")
                -> where("id=?",$id);		
        $return = $orm->exec();
        return $return;
    }
    
    public function crearMedio($medio){
        $orm = new ORM();
        $sql = $orm -> insert("op_bancos_medios")
                                -> values(array(0,$medio,opDate()));
        return $sql -> exec() -> afectedRows();
    }
    
    public function medios(){
        $orm = new ORM();
        $sql = $orm -> select()
                    -> from ("op_bancos_medios");
        return $sql -> exec() -> fetchArray();
    }
    
    public function mediosId($id){
        $orm = new ORM();
        $sql = $orm -> select()
                    -> from ("op_bancos_medios")
                    -> where('id=?',$id);
        return $sql -> exec() -> fetchArray();
    }
    
    public function updMedio($id,$medio){
	  $orm = new ORM();
		$query = $orm	-> update("op_bancos_medios")
                                -> set (array(
                                            "medio"=>$medio
                                            )
                                        )
                                -> where("id=?",$id);
		$return = $query->exec() -> afectedRows();
		
		return $return;		
    }
    
    public function delMedio($id){
        $orm = new ORM();
        $orm	-> delete()
                -> from("op_bancos_medios")
                -> where("id=?",$id);		
        $return = $orm->exec();
        return $return;
    }
    
    public function crearTipo($tipo){
        $orm = new ORM();
        $sql = $orm -> insert("op_bancos_tipos")
                                -> values(array(0,$tipo,opDate()));
        return $sql -> exec() -> afectedRows();
    }
    
    public function tipos(){
        $orm = new ORM();
        $sql = $orm -> select()
                    -> from ("op_bancos_tipos");
        return $sql -> exec() -> fetchArray();
    }
    
    public function tiposId($id){
        $orm = new ORM();
        $sql = $orm -> select()
                    -> from ("op_bancos_tipos")
                    -> where('id=?',$id);
        return $sql -> exec() -> fetchArray();
    }
    
        public function updTc($id,$tc){
	  $orm = new ORM();
		$query = $orm	-> update("op_bancos_tipos")
                                -> set (array(
                                            "tipo"=>$tc
                                            )
                                        )
                                -> where("id=?",$id);
		$return = $query->exec() -> afectedRows();
		
		return $return;		
    }
    
    public function delTipo($id){
        $orm = new ORM();
        $orm	-> delete()
                -> from("op_bancos_tipos")
                -> where("id=?",$id);		
        $return = $orm->exec();
        return $return;
    }
    
} // class
