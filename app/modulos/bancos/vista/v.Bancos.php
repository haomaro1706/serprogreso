<?php 

class vBancos{
    public function main($info=null){
      echo getMensaje();
        ?>
           <h2><?php  echo BANCOS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('parametros'), 
            		  BANCOS_NOMBRE=>'#',
            		  BANCOS_MEDIOS=>  setUrl('bancos','medios'),
            		  BANCOS_TC=>  setUrl('bancos','tc')
            		 ),
            	    BANCOS_NOMBRE
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  BANCOS_NOMBRE=>'#'
            		 ),
            	    BANCOS_NOMBRE, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo BANCOS_NOMBRE?></h4>
       <div class='container'>
        <div class='span10 offset1'> 
        
            <fieldset>
			  <legend><?php echo BANCOS_CREAR?></legend>
		  <?php
			  Html::openForm("bancos",setUrl("bancos","saveBanco"));
			  Html::newInput("banco","Nombre del banco *");
		  ?>
	  </fieldset>
        <?php
		  Html::newButton("crear", "Crear", "submit");
		  Html::closeForm();
	  ?>
            
            <br/><br/>
            
            <?php echo datatable('lst')?>
	    <table class="table table-hover" id="lst">
		<thead>
		  <tr>
		    <th>#</th>
		    <th>Banco</th>
		    <th>Fecha registro</th>
		    <th>Acciones</th>
		  </tr>
		</thead>
		<tbody>
                <?php
                if(is_array($info)){
                  $j=1;
		    foreach ($info As $i):
		  ?>
		  <tr>
		    <td><?php echo $j?></td>
		    <td><?php echo $i['banco']?></td>
		    <td><?php echo $i['fecha']?></td>
		    <td>
		    <a href="<?php echo setUrl('bancos','editarBanco',array('id'=>trim($i["id"])))?>"><img src="app/img/editar.png" alt="editar" title="editar"></a>
                    <a href="<?php echo setUrl('bancos','del',array('id'=>trim($i["id"])))?>" onclick="return confirm('Desea eliminar el banco ?')"><img src="app/img/eliminar.png" alt="eliminar" title="eliminar"></a>
		    </td>
		  </tr>
		  <?php
			$j++;
		    endforeach;
			}else{
			  echo 'No hay datos';
			}
		  ?>
		</tbody>
	    </table>
            
        </div>
       </div>
       <?php  
    }//main 

    public function editarBanco($info=null){
      echo getMensaje();
        ?>
           <h2><?php  echo BANCOS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('bancos'), 
            		  BANCOS_NOMBRE=>'#'
            		 ),
            	    BANCOS_NOMBRE
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('bancos'), 
            		  BANCOS_NOMBRE=>'#'
            		 ),
            	    BANCOS_NOMBRE, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo BANCOS_NOMBRE?></h4>
       <div class='container'>
        <div class='span10 offset1'> 
        
            <fieldset>
			  <legend><?php echo BANCOS_CREAR?></legend>
		  <?php
			  Html::openForm("bancos",setUrl("bancos","updBanco"));
			  Html::newInput("banco","Nombre del banco *",$info['banco']);
		  ?>
	  </fieldset>
        <?php
            Html::newButton("actualizar", "Actualizar", "submit");
            Html::newHidden("id",$info['id']);
            Html::closeForm();
	?>
        </div>
       </div>
       <?php  
    }//editarBanco
    
    
    public function medios($info=null){
      echo getMensaje();
        ?>
           <h2><?php  echo BANCOS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('parametros'),
            		  BANCOS_NOMBRE =>   setUrl('bancos'),
            		  BANCOS_MEDIOS =>   '#',
            		  BANCOS_TC=>  setUrl('bancos','tc')
            		 ),
            	    BANCOS_MEDIOS
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  BANCOS_MEDIOS=>'#'
            		 ),
            	    BANCOS_MEDIOS, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo BANCOS_MEDIOS?></h4>
       <div class='container'>
        <div class='span10 offset1'> 
        
            <fieldset>
			  <legend><?php echo BANCOS_CREAR_MEDIOS?></legend>
		  <?php
			  Html::openForm("medios",setUrl("bancos","saveMedio"));
			  Html::newInput("medio","Nombre del medio *");
		  ?>
	  </fieldset>
        <?php
		  Html::newButton("crear", "Crear", "submit");
		  Html::closeForm();
	  ?>
            
            <br/><br/>
            
            <?php echo datatable('lst')?>
	    <table class="table table-hover" id="lst">
		<thead>
		  <tr>
		    <th>#</th>
		    <th>Medio de pago</th>
		    <th>Fecha registro</th>
		    <th>Acciones</th>
		  </tr>
		</thead>
		<tbody>
                <?php
                if(is_array($info)){
                  $j=1;
		    foreach ($info As $i):
		  ?>
		  <tr>
		    <td><?php echo $j?></td>
		    <td><?php echo $i['medio']?></td>
		    <td><?php echo $i['fecha']?></td>
		    <td>
		    <a href="<?php echo setUrl('bancos','editarMedio',array('id'=>trim($i["id"])))?>"><img src="app/img/editar.png" alt="editar" title="editar"></a>
                    <a href="<?php echo setUrl('bancos','delMedio',array('id'=>trim($i["id"])))?>" onclick="return confirm('Desea eliminar el medio ?')"><img src="app/img/eliminar.png" alt="eliminar" title="eliminar"></a>
		    </td>
		  </tr>
		  <?php
			$j++;
		    endforeach;
			}else{
			  echo 'No hay datos';
			}
		  ?>
		</tbody>
	    </table>
            
        </div>
       </div>
       <?php  
    }//medios
    
    
    public function editarMedio($info=null){
      echo getMensaje();
        ?>
           <h2><?php  echo BANCOS_MEDIOS?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('bancos','medios'), 
            		  BANCOS_MEDIOS=>'#'
            		 ),
            	    BANCOS_MEDIOS
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('bancos','medios'), 
            		  BANCOS_MEDIOS=>'#'
            		 ),
            	    BANCOS_MEDIOS, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo BANCOS_MEDIOS?></h4>
       <div class='container'>
        <div class='span10 offset1'> 
        
            <fieldset>
			  <legend><?php echo BANCOS_EDITAR_MEDIOS?></legend>
		  <?php
                    Html::openForm("medios",setUrl("bancos","updMedio"));
                    Html::newInput("medio","Nombre del medio *",$info['medio']);
		  ?>
	  </fieldset>
        <?php
            Html::newButton("actualizar", "Actualizar", "submit");
            Html::newHidden("id",$info['id']);
            Html::closeForm();
	?>
        </div>
       </div>
       <?php  
    }//editarBanco
    
    public function tc($info=null){
      echo getMensaje();
        ?>
           <h2><?php  echo BANCOS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('parametros'),
            		  BANCOS_NOMBRE =>  setUrl('bancos'),
            		  BANCOS_MEDIOS =>  setUrl('bancos','medios'),
            		  BANCOS_TC =>  '#'
            		 ),
            	    BANCOS_TC
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  BANCOS_TC=>'#'
            		 ),
            	    BANCOS_TC, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo BANCOS_TC?></h4>
       <div class='container'>
        <div class='span10 offset1'> 
        
            <fieldset>
			  <legend><?php echo BANCOS_TC_CREAR?></legend>
		  <?php
			  Html::openForm("tipo",setUrl("bancos","saveTc"));
			  Html::newInput("tc","Nombre tipo de cuenta *");
		  ?>
	  </fieldset>
        <?php
		  Html::newButton("crear", "Crear", "submit");
		  Html::closeForm();
	  ?>
            
            <br/><br/>
            
            <?php echo datatable('lst')?>
	    <table class="table table-hover" id="lst">
		<thead>
		  <tr>
		    <th>#</th>
		    <th>Tipo cuenta</th>
		    <th>Fecha registro</th>
		    <th>Acciones</th>
		  </tr>
		</thead>
		<tbody>
                <?php
                if(is_array($info)){
                  $j=1;
		    foreach ($info As $i):
		  ?>
		  <tr>
		    <td><?php echo $j?></td>
		    <td><?php echo $i['tipo']?></td>
		    <td><?php echo $i['fecha']?></td>
		    <td>
		    <a href="<?php echo setUrl('bancos','editarTc',array('id'=>trim($i["id"])))?>"><img src="app/img/editar.png" alt="editar" title="editar"></a>
                    <a href="<?php echo setUrl('bancos','delTc',array('id'=>trim($i["id"])))?>" onclick="return confirm('Desea eliminar el tipo de cuenta ?')"><img src="app/img/eliminar.png" alt="eliminar" title="eliminar"></a>
		    </td>
		  </tr>
		  <?php
			$j++;
		    endforeach;
			}else{
			  echo 'No hay datos';
			}
		  ?>
		</tbody>
	    </table>
            
        </div>
       </div>
       <?php  
    }//main 
    
    public function editarTc($info=null){
      echo getMensaje();
        ?>
           <h2><?php  echo BANCOS_TC?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('bancos','tc'), 
            		  BANCOS_TC_EDITAR=>'#'
            		 ),
            	    BANCOS_TC_EDITAR
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('bancos','tc'), 
            		  BANCOS_TC_EDITAR=>'#'
            		 ),
            	    BANCOS_TC_EDITAR, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo BANCOS_TC?></h4>
       <div class='container'>
        <div class='span10 offset1'> 
        
            <fieldset>
			  <legend><?php echo BANCOS_TC_EDITAR?></legend>
		  <?php
                    Html::openForm("tipo",setUrl("bancos","updTc"));
                    Html::newInput("tc","Nombre del tipo *",$info['tipo']);
		  ?>
	  </fieldset>
        <?php
            Html::newButton("actualizar", "Actualizar", "submit");
            Html::newHidden("id",$info['id']);
            Html::closeForm();
	?>
        </div>
       </div>
       <?php  
    }//editarBanco
} // class
