<?php
include_once 'app/modulos/bancos/vista/v.Bancos.php';
include_once 'app/modulos/bancos/modelo/m.Bancos.php';

class Bancos extends vBancos{

    public function main(){
        /*Funcion Principal*/ 
        validar_usuario();
        $info = mBancos::bancos();
        parent::main($info);    
    }
    
    public function saveBanco($array=null){
        validar_usuario();
        $save = mBancos::crearBanco($array['banco']);
        if($save){
            setMensaje('Banco creado correctamente','success');
        }else{
            setMensaje('Error al crear el banco','error');
        }
        location(setUrl('bancos'));
    }
    
    public function editarBanco($array=null){
        /*Funcion Principal*/ 
        validar_usuario();
        $info = mBancos::bancosId($array['id']);
        
        parent::editarBanco($info[0]);    
    }
    
    public function updBanco($array=null){
        validar_usuario();
        $upd = mBancos::updBanco($array['id'],$array['banco']);
        
        if($upd){
            setMensaje('Banco actualizado correctamente','success');
        }else{
            setMensaje('Error al actualizar el banco','error');
        }
        location(setUrl('bancos'));
    }
    
    public function del($array=null){
        validar_usuario();
        $upd = mBancos::delBanco($array['id']);
        
        if($upd){
            setMensaje('Banco eliminado correctamente','success');
        }else{
            setMensaje('Error al eliminar el banco','error');
        }
        location(setUrl('bancos'));
    }
    
    public function medios(){
        validar_usuario();
        $info = mBancos::medios();
        parent::medios($info);
    }
    
    public function saveMedio($array=null){
        validar_usuario();
        
        $save = mBancos::crearMedio($array['medio']);
        
        if($save){
            setMensaje('Medio de pago creado correctamente','success');
        }else{
            setMensaje('Error al crear medio de pago','error');
        }
        location(setUrl('bancos','medios'));
    }
    
    public function editarMedio($array=null){
        validar_usuario();

        $info = mBancos::mediosId($array['id']);
        
        parent::editarMedio($info[0]);
    }
    
    public function updMedio($array=null){
        validar_usuario();
        
        $upd = mBancos::updMedio($array['id'],$array['medio']);
        
        if($upd){
            setMensaje('Medio de pago actualizado correctamente','success');
        }else{
            setMensaje('Error al actualizar el medio de pago','error');
        }
        location(setUrl('bancos','medios'));
    }
              
    public function delMedio($array=null){
        validar_usuario();
        $upd = mBancos::delMedio($array['id']);
        
        if($upd){
            setMensaje('Medio de pago eliminado correctamente','success');
        }else{
            setMensaje('Error al eliminar el medio de pago','error');
        }
        location(setUrl('bancos','medios'));
    }
    
    public function tc($array=null){
        validar_usuario();
        $info = mBancos::tipos();
        parent::tc($info);
    }
    
    public function saveTc($array=null){
        validar_usuario();
        
        $save = mBancos::crearTipo($array['tc']);
        
        if($save){
            setMensaje('Tipo de cuenta creada correctamente','success');
        }else{
            setMensaje('Error al crear el tipo de cuenta','error');
        }
        location(setUrl('bancos','tc'));
    }
    
    public function editarTc($array=null){
        validar_usuario();
        $info = mBancos::tiposId($array['id']);
        parent::editarTc($info[0]);
    }
    
    public function updTc($array=null){
        validar_usuario();
        $upd = mBancos::updTc($array['id'],$array['tc']);
        
        if($upd){
            setMensaje('Tipo de cuenta actualizada correctamente','success');
        }else{
            setMensaje('Error al actualizar el tipo de cuenta','error');
        }
        location(setUrl('bancos','tc'));
    }
    
    public function delTc($array=null){
        validar_usuario();
        
        $upd = mBancos::delTipo($array['id']);
        
        if($upd){
            setMensaje('Tipo de cuenta eliminada correctamente','success');
        }else{
            setMensaje('Error al eliminar el tipo de cuenta','error');
        }
        location(setUrl('bancos','tc'));
    }
} // class
