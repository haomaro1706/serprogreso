<?php 
include_once 'app/modulos/soporte/vista/v.Soporte.php';
include_once 'app/modulos/soporte/modelo/m.Soporte.php';

class Soporte extends vSoporte{

    public function main($array = ""){
		validar_usuario();
		if(isset($array["send"])){
			if($array["send"] != ""){
				setMensaje("Su solicitud ha sido registrada exitosamente, pronto estaremos en contacto con usted.","success");
			}else{
				setMensaje("Error interno al intentar enviar su solicitud. por favor comuniquese con el administrador del sistema.","error");
			}
		}
			
		parent::main();
    } // main
	
	public function newSupport($array){
		validar_usuario();
		$return = clienteSoap("saveSupport", array("name"=>$array));
        	location(setUrl("soporte","main",array("send"=>$return)));
	} // newSupport
	
	
	public function saveSupport($array){
		$mSop = new mSoporte();
		$save = false;
		if($array["sitio"] && $array["email"]){
			$save = $mSop->saveSupport(	$array["sitio"], $array["nombre"], $array["email"], $array["telef"], 
										$array["proble"], opDate(), $array["fecha"], $array["dir"], $array["ciudad"], 
										$array["pais"], $array["contac"], $array["serv"], $array["txtServ"], $array["reqSol"]);
		}
		
		if($save){
			return "ok";
		}else{
			return "";
		}
	} // saveSupport
	
	
	public function estado(){
		validar_usuario();
		parent::estado();
	} // estado
	
	
	public function lstEstado($array){
		if($array["email"]){
			$lst = clienteSoap("consulEstado", array("name"=>$array));
		}
		$cad = '';
		if(is_array($lst) && count($lst) > 0){
			$i = 1;
			foreach($lst as $key => $val){
				$cad .= '<tr><td>'.$i.'</td>';
				$cad .= '<td>'.$val["fechaSol"].'</td>';
				$cad .= '<td>'.$val["proble"].'</td>';
				switch($val["estado"]){
					case "EN":
						$img = "enviado";
					break;
					case "LE":
						$img = "leido";
					break;
					case "PR":
						$img = "procesando";
					break;
					case "RE":
						$img = "realizado";
					break;
				}
				$cad .= '<td><img src="app/img/'.$img.'.png" alt="'.$img.'" title="'.$img.'"/></td></tr>';
				$i++;
			}
		}
		parent::lstEstado($cad);
	} // lstEstado
	
	
	public function consulEstado($array){
		$mSop = new mSoporte();
		if($array["email"]){
			$lst = $mSop->consulEstado($array["email"]);
		}else if($array["email"] && $array["nRad"]){
			$lst = $mSop->consulEstado($array["email"], $array["nRad"]);
		}
		return $lst;
	} // consulEstado
	
} // class
?>
