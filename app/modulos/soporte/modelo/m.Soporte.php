<?php 

class mSoporte{
	
	public function saveSupport($sitio, $nombre, $email, $telef, $proble, $fechaAct, $fechaProbl, $dir, $ciudad, $pais, $contac, $serv, $txtServ, $reqSol){
		$orm = new ORM();
		$sql = $orm -> insert("op_soporte")
					-> values(array(0, $sitio, $nombre, $email, $telef, $proble, $fechaAct, $fechaProbl, $dir, $ciudad, $pais, $contac, $serv, $txtServ, $reqSol, "EN"));
		$orm -> exec();
		return $orm -> afectedRows() > 0 ?  true : false;
	} // saveSupport
	
	
	public function consulEstado($email, $nRad = ''){
		$orm = new ORM();
		
		if($nRad != ''){
			$sql = $orm -> select()
						-> from("op_soporte")
						-> where("correo = ? ", $email)
						-> where("id_soporte = ? ", $nRad)
						-> orderBy(array("fechaSol DESC"));
		}else{
			$sql = $orm -> select()
						-> from("op_soporte")
						-> where("correo = ? ", $email)
						-> orderBy(array("fechaSol DESC"));
		}
		return $orm -> exec() -> fetchArray();
	} // consulEstado
} // class
?>
