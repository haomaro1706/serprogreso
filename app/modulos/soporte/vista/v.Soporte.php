<?php 
class vSoporte{
    public function main(){
		echo getMensaje();
        ?>
           <h2><?php  echo SOPORTE_NOMBRE?></h2>
            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  SOPORTE_CREAR=>"#",
            		  SOPORTE_ESTADO=>setUrl("soporte","estado"),
            		 ),
            	    SOPORTE_CREAR
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  SOPORTE_CREAR=>'#'
            		 ),
            	    SOPORTE_CREAR, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo SOPORTE_CREAR?></h4>
       <div class='container'>
        <div class='span7 offset2'> 
        <form class="form-horizontal" name="soporte" method="post" enctype="multipart/form-data" id="soporte" action="<?php echo setUrl("soporte","newSupport")?>">
			<fieldset>
				<legend>Informaci&oacute;n b&aacute;sica de la solicitud</legend>
				<div class="control-group">
					<label class="control-label" for="nombre"> Nombre del Solicitante (*) :</label>
					<div class="controls">
						<input type="text" name="nombre" id="nombre" placeholder="Nombre del solicitante">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="email"> Correo electr&oacute;nico (*) :</label>
					<div class="controls">
						<input type="text" name="email" id="email" placeholder="Correo electr&oacute;nico">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="telef"> N&uacute;mero celular o tel&eacute;fono (*) :</label>
					<div class="controls">
						<input type="text" name="telef" id="telef" placeholder="N&uacute;mero celular o tel&eacute;fono">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="proble"> Describa claramente el problema encontrado (*) :</label>
					<div class="controls">
						<textarea name="proble" id="proble"></textarea>
					</div>
				</div>
			</fieldset>
			<fieldset>
				<legend>Fechas</legend>
				<div class="control-group">
					<label class="control-label" for="fecha"> Fecha de ocurrencia:</label>
					<div class="controls">
						<?php echo inputDate("fecha")?>
						<input type="text" name="fecha" id="fecha">
					</div>
				</div>
			</fieldset>
			<fieldset>
				<legend>Datos adicionales del solicitante</legend>
				<div class="control-group">
					<label class="control-label" for="dir"> Direcci&oacute;n :</label>
					<div class="controls">
						<input type="text" name="dir" id="dir" placeholder="Direcci&oacute;n">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="ciudad"> Ciudad :</label>
					<div class="controls">
						<input type="text" name="ciudad" id="ciudad" placeholder="Ciudad">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="pais"> Pais (*) :</label>
					<div class="controls">
						<input type="text" name="pais" id="pais" placeholder="Pais">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="contac"> Nombre del cont&aacute;cto, si es diferente al solicitante :</label>
					<div class="controls">
						<input type="text" name="contac" id="contac" placeholder="Nombre del cont&aacute;cto">
					</div>
				</div>
			</fieldset>
			<fieldset>
				<legend>Descripci&oacute;n del producto o servicio</legend>
				<div class="control-group">
					<label class="control-label" for="serv"> Nombre del servicio prestado :</label>
					<div class="controls">
						<input type="text" name="serv" id="serv" placeholder="Nombre del servicio prestado">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="textServ"> Descripci&oacute;n del servicio :</label>
					<div class="controls">
						<textarea name="txtServ" id="txtServ"></textarea>
					</div>
				</div>
			</fieldset>
			<fieldset>
				<legend>¿Solicita una soluci&oacute;n?</legend>
				<div class="control-group">
					<label class="control-label" for="reqSol"> ¿Solicita una soluci&oacute;n? (*) :</label>
					<div class="controls">
						<select name="reqSol" id="reqSol">
							<option value=""> - Selecione - </option>
							<option value="SI"> Si </option>
							<option value="NO"> No </option>
						</select>
					</div>
				</div>
			</fieldset>
				<div class="control-group">
					<div class="controls">
						<input type="hidden" name="sitio" id="sitio" value="<?php echo URLSITIO()?>">
						<button type="submit" name="send" class="btn btn-primary">Enviar solicitud</button>
					</div>
				</div>
		</form>
        </div>
       </div>
       <?php  
    } // main
	
	
	public function estado(){
      echo getMensaje();
        ?>
           <h2><?php  echo SOPORTE_NOMBRE?></h2>
            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  SOPORTE_CREAR=>setUrl("soporte"),
            		  SOPORTE_ESTADO=>"#"
            		 ),
            	    SOPORTE_ESTADO
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  SOPORTE_ESTADO=>'#'
            		 ),
            	    SOPORTE_ESTADO, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo SOPORTE_ESTADO?></h4>
       <div class='container'>
        <div class='span7 offset2'> 
        <form class="form-horizontal" name="soporte" method="post" enctype="multipart/form-data" id="soporte" action="<?php echo setUrl("soporte","lstEstado")?>">
			<fieldset>
				<legend>Datos de la b&uacute;squeda</legend>
				<div class="control-group">
					<label class="control-label" for="numRad"> N&uacute;mero de radicaci&oacute;n (*):</label>
					<div class="controls">
						<input type="text" name="numRad" id="numRad" placeholder="N&uacute;mero de radicaci&oacute;n">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="email"> Correo electr&oacute;nico (*) :</label>
					<div class="controls">
						<input type="text" name="email" id="email" placeholder="Correo electr&oacute;nico">
					</div>
				</div>
			</fieldset>
				<div class="control-group">
					<div class="controls">
						<button type="submit" name="send" class="btn btn-primary">Enviar Consultar</button>
					</div>
				</div>
		</form>
		
		<form class="form-horizontal" name="allSup" method="post" enctype="multipart/form-data" id="allSup" action="<?php echo setUrl("soporte","lstEstado")?>">
			<fieldset>
				<legend>Consultar todas mis solicitudes</legend>
				<div class="control-group">
					<label class="control-label" for="email"> Correo electr&oacute;nico (*) :</label>
					<div class="controls">
						<input type="text" name="email" id="email" placeholder="Correo electr&oacute;nico">
					</div>
				</div>
			</fieldset>
				<div class="control-group">
					<div class="controls">
						<input type="hidden" name="sitio" id="sitio" value="<?php echo URLSITIO()?>">
						<button type="submit" name="send" class="btn btn-primary">Enviar Consultar</button>
					</div>
				</div>
		</form>
        </div>
       </div>
       <?php  
    } // estado
	
	
	public function lstEstado($array){
		echo getMensaje();
        ?>
		<h2><?php  echo SOPORTE_NOMBRE?></h2>
		<?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  SOPORTE_CREAR=>setUrl("soporte"),
            		  SOPORTE_ESTADO=>setUrl("soporte","estado"),
            		  SOPORTE_ESTADO_LISTA=>"#",
            		 ),
            	    SOPORTE_ESTADO_LISTA
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  SOPORTE_ESTADO=>setUrl("soporte","estado"),
					  SOPORTE_ESTADO_LISTA=>'#'
            		 ),
            	    SOPORTE_ESTADO_LISTA, 
            	    'breadcrumb'
            	   );
		?>
		<h4><?php  echo SOPORTE_CREAR?></h4>
		<div class='container'>
			<div class='span9 offset1'> 
				<?php echo datatable("lista")?>
				<table class="table table-hover" id="lista">
					<thead>
						<tr>
							<th> # </th>
							<th> Fecha de solicitud </th>
							<th> Problema </th>
							<th> Estado </th>
						</tr>
					</thead>
					<tbody>
						<?php echo $array?>
					</tbody>
				</table>
			</div>
		</div>
       <?php
	}
} // class
        ?>
