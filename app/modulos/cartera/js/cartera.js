/*javascript para el modulo js/*/
$(function(){
	
	$("#periodo").change(function(){
		calcularCuota($(this).val(), $("#tasa").val())
	})
	
	$("#tasa").bind("keyup blur paste", function(){
		calcularCuota($("#periodo").val(), $(this).val())
	})
	
	$("#acuerdos").validate({
		rules:{
			periodo:{required:true},
			fechaIni:{required:true},
			tasa:{required:true, min: 0, max: 100, minlength: 1, maxlength:5}
		}
	})
	
	$("#acuerdos").validate({
		rules:{
			abono:{required:true, min: 0}
		}
	})
	
	$("#abonos").submit(function(){
		if(parseInt($("#abono").val()) > parseInt($("#total").val())){
			alert("El monto maximo a abonar es de " + toMoney($("#total").val()))
			return false;
		}
	})
	
	
	    $.post('clientes.php/lstClientes',{jquery:1},function(data){
    var availableTags = data.split(',');
	
	$( "#lstClientes" ).autocomplete({
	    source: availableTags,
	    select: function( event, ui ) {
		id = ui.item.value.split('-');
		$.post('solicitudes.php/clienteId',{jquery:1,id:id[0]},function(data2){
			
		    $("#prestamos").html(data2);
		});
	    }
	});
    });
	
})


function calcularCuota(periodo, tasa){
	if(periodo && tasa){
		c = parseInt($("#total").val());
		i = tasa/100;
		n = periodo
		num = c * i;
		den = 1 - Math.pow((1+i),-n)
		$("#vCuota").val(toMoney((num/den).toFixed()));
		$("#cuota").val((num/den).toFixed());
	}
		}