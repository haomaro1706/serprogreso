<?php 

include_once 'app/modulos/cartera/vista/v.Cartera.php';
include_once 'app/modulos/cartera/modelo/m.Cartera.php';
include_once 'app/modulos/solicitudes/modelo/m.Solicitudes.php';
include_once 'app/modulos/acuerdos/modelo/m.Acuerdos.php';
include_once 'app/modulos/cajas/modelo/m.Cajas.php';

class Cartera extends vCartera{

    public function main($array = ''){
	validar_usuario();
	$lista = mSolicitudes::all2(3);
	$cad = '';
	$i=1;
	if(!empty($lista)){
		foreach($lista as $key => $val){
			$abono = mCartera::lstAbonos($val["idDes"]);
			$abono = $abono[0];

			if($val["idSol"] == '' && $val["idDes"] == '')
				continue;
			$cad .= '<tr><td>'.$val["idSol"].'</td>';
			$cad .= '<td>'.$val["elpro"].'</td>';
			$cad .= '<td>'.$val["cliente"].'</td>';
			$cad .= '<td>'.$val["nombre"]." ".$val["apellido"].'</td>';
			$cad .= '<td>'.$val["fecha_desembolso"].'</td>';
			$cad .= '<td>'.$val["fPago"].'</td>';

			$cad .= '<td>'.toMoney($val["total"]).'</td>';
			$cad .= '<td><a href="'.setUrl("cartera","main",array("id"=>$val["idSol"])).'"><img src="'.SISTEMA_RUTAIMG.'pdf.png"></a></td>';
		 	//$cad .= '<td><a href="'.setUrl("cartera","main",array("id"=>$val["idSol"],"extracto"=>true)).'"><img src="'.SISTEMA_RUTAIMG.'pdf.png"></a></td>';
			$cad .= '<td><a href="'.setUrl("cartera","pagoParcial",array("id"=>$val["idDes"])).'"><img src="'.RUTAMODULOS.'cartera/img/parciales.png"></a></td>';
			$cad .= '<td><a href="'.setUrl("cartera","reliquidar",array("id"=>$val["idDes"])).'"><img src="'.SISTEMA_RUTAIMG.'editar.png"></a></td>';
			$cad .= '<td><a href="'.setUrl("cartera","pagar",array("id"=>$val["idDes"])).'"><img src="'.SISTEMA_RUTAIMG.'use.png"></a></td>';
			$cad .= '<td><a href="'.setUrl("cartera","acuerdos",array("id"=>$val["idSol"])).'"><img src="'.SISTEMA_RUTAIMG.'acuerdos.png"></a></td></tr>';
			$i++;
		}
	}
        parent::main($cad);    
        
        $id = empty($array['id']) ? '' : $array['id'];
	if(is_numeric($id)){
		include_once 'app/modulos/solicitudes/controlador/c.Solicitudes.php';
		$cSol = new Solicitudes();
		if($array['extracto']){
		//die('uno');
			$cSol->pdfs(array("id"=>$id,"extracto"=>true,"cheque"=>false));
		}else{
		//die('dos');
			$cSol->pdfs(array("id"=>$id,"extracto"=>false,"cheque"=>true));
		}
	}
		
    } // main
	
	public function boucher($array=null){
		validar_usuario();
		parent::boucher();
		if(isset($array['creditos'])){
					$cadena='
			<html>

			<head>
			<meta http-equiv="Content-Type" content="text/html">
			
			<title>Cheque pago</title>
			<base href="http://prestarmas.co/prestarmas/">
			<style type="text/css">
				body{font-size: 11px; font-family: "Helvetica";}
				/*hr{border: 1px dashed #000000;}*/
				table{font-size: 7.5px;boder-collapse}
				#content {
				margin: 0.25in;
				width: 100%;
			}
			#inactivo td{background-color: #bbb;}
			.caja {
			 padding: 1em 3em;
			 margin: 1em 25%;
			}
			</style>
			</head>
			<body>';
		  
	$info2 = mSolicitudes::desembolsos2($array['creditos']);
	$info2 = $info2[0];
	$parametros = 
	$abono = mCartera::lstAbonos($val["idDes"]);
	$abono = $abono[0]["abono"];
	
	  $cadena ='';
	  $cadena.='<div style="float:left"><img src="app/img/logo.jpg" height="250px" width="250px"><br/>Informaci&oacute;n general de obligaci&oacute;n</div>';
	  $cadena.='<div ><table style="width:600px;">';
	  $cadena.='<tr>';
	  $cadena .='<td colspan="4" bgcolor="#D8D8D8"><b>Cliente:</b> <br/>'.$info2['nombre'].' '.$info2['apellido'].' '.$info2['cliente'].'</td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td bgcolor="#D8D8D8"><b>Credito</b></td>';
	  $cadena.='<td bgcolor="#D8D8D8"><b>Oficina</b></td>';
	  $cadena.='<td colspan="2" bgcolor="#D8D8D8"><b>Pague antes de </b></td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td>'.$array['creditos'].'</td>';
	  $cadena.='<td>'.$info2['nombre_suc'].'</td>';
	  $cadena.='<td>'.opDate().'</td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td bgcolor="#D8D8D8"><b>Linea de credito</b></td>';
	  $cadena.='<td bgcolor="#D8D8D8"><b>Fecha desembolso</b></td>';
	  $cadena.='<td colspan="2" bgcolor="#D8D8D8"><b>Plazo</b></td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td>'.$info2['elpro'].'</td>';
	  $cadena.='<td>'.$info2['fecha_desembolso'].'</td>';
	  $cadena.='<td>'.$info2['plazo_fin'].'</td>';
	  
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td bgcolor="#D8D8D8"><b>Tasa EA</b></td>';
	  $cadena.='<td bgcolor="#D8D8D8"><b>Tasa nominal</b></td>';
	  $cadena.='<td colspan="2" bgcolor="#D8D8D8"><b>Tasa mensual</b></td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';

	  $cadena.='<td>'.round($info2['tasa_ea'],2).' %</td>';
	  $cadena.='<td>'.round($info2['tasa_nominal'],2).' %</td>';
	  $cadena.='<td>'.round($info2['tasa_mensual'],2).' %</td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td bgcolor="#D8D8D8"><b>Tasa mora</b></td>';
	  $cadena.='<td bgcolor="#D8D8D8"><b>Total a pagar</b></td>';
	  //$cadena.='<td><b>Tasa cobranza</b></td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td>3.5 %</td>';
	  $cadena.='<td><b><font size="+3">'.toMoney($array['abono']).'</font><b/></td>';
	  $cadena.='</tr>';
	  $cadena.='</table>';
	  
	 /* $cadena.='<table class="table" >';
	  $cadena.='<tr colspan="2">';
	  $cadena .='<td><b>Detalle del pago a realizar</b></td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td><b>Descripci&oacute;n</b></td>';
	  $cadena.='<td><b>Valor</b></td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td>Capital</td>';
	  $cadena.='<td>'.toMoney($info2['capital']).'</td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td>Costos administrativos</td>';
	  $cadena.='<td>'.toMoney($info2['admi1']+$info2['admi2']).'</td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td>Inter&eacute;s Corriente</td>';
	  $cadena.='<td>'.toMoney($info2['interes_corriente']).'</td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td>Inter&eacute;s Mora</td>';
	  $cadena.='<td>'.toMoney($info2['interes_mora']).'</td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td>Honorarios cobranza</td>';
	  $cadena.='<td>'.toMoney($info2['cobranza']).'</td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td>Recaudo proveedor</td>';
	  $cadena.='<td>'.toMoney($info2['recaudo']).'</td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td>I.V.A</td>';
	  $cadena.='<td>'.toMoney($info2['iva']).'</td>';
	  $cadena.='</tr>';
	  $cadena.='</table></div>';*/
	  $cadena .= '</div>';
	
	  
	  $fecha= explode('-',opDate());
	  $total_pagar = $array['abono'];
	  
	  if(strlen($total_pagar)<10){
	    $ceros=10-(strlen($total_pagar));
	    $ceros1='';
	    for($i=0;$i<$ceros;$i++){
		$ceros1=$ceros1.'0';
	    }
	  }
	  if(strlen($info2['id_des'])<10){
	    $ceros=10-(strlen($info2['id_des']));
	    $ceros2='';
	    for($i=0;$i<$ceros;$i++){
		$ceros2=$ceros2.'0';
	    }
	  }
	  
	  $barras2 = '41579012345678908020'.$ceros2.$info2['id_des'].'3900'.$ceros1.$total_pagar.'96'.$fecha[0].$fecha[1].$fecha[2];
	 
	  $cadena .= '<br/><br/><center><img src="http://www.prestarmas.co/prestarmas/app/librerias/php/ImgBarcode/generador.php?code='.$barras2.'"></center>';
		  
		    $cadena .= '
		    </body>
		    </html>';
		//echo($cadena);
	pdf($cadena,'app/files/clientes/'.trim($info2['cliente']).'/'.$info2['id_solicitud'].'/abono_'.opdate());
	//$url='descargar.php?file=app/files/clientes/'.trim($info2['cliente']).'/'.$info2['id_solicitud'].'/abono_'.opDate().'.pdf';
	return location('descargar.php?file=app/files/clientes/'.trim($info2['cliente']).'/'.$info2['id_solicitud'].'/abono_'.opDate().'.pdf');
		}
	}
	
	public function reliquidar_crom($array){
            $all = mCartera::creditos();
            foreach ($all As $a):
                //llamo funcion reliquidar
                $this->reliquidar($a['id']);
            endforeach;
	} //reliquidar_crom
	
        public function reliquidar($array){
            //die('='.$array);
            $info = mCajas::verCuotas($array);
            $info2  = mCajas::cobranza(getZona());

            $cuota          =   0;
            $meses_vencido  =   0;
            $total          =   0;
            if(is_array($info)){
                    $i=1;
                    foreach($info as $key){
                        /*se calcula le mora*/
                        $fecha_pago = explode('-',$key['fechaPago']);
                        $dia = $fecha_pago[2];
                        $mes = $fecha_pago[1];
                        $ano = $fecha_pago[0];
                        $fecha_pago2 = $dia.'-'.$mes.'-'.$ano;
                        $vmora=false;
                        $meses_vencido      =  0;
                        $meses_vencido2     =  0;
                        $interes_mora       =  0;
                        $cuotaFija          =  0;
                        $dias               =  0;
                        $seguro             =  0;
                        $cobranza           =  0;
                        
                        if($key['estado']=='1'){
                            if($ano<date("Y")){
                                $vmora  =   true;
                                $meses_vencido += $meses_vencido++;
                            }else if($ano==date("Y") && $mes<date("m")){
                                $vmora  =   true;
                                $meses_vencido += $meses_vencido++;
                            }else if($ano<=date('Y') && $mes<=date("m") && date("d")>$dia){
                                $vmora  =   true;
                                $meses_vencido += $meses_vencido++;
                            }else{
                                $vmora  =   false;
                                $meses_vencido=0;
                            }
                            /*fin calculo mora*/
                        
                            $dias               = difFechas($fecha_pago2,opDate());
                            $meses_vencido2     = round(difFechas($fecha_pago2,opDate())/30,2);
                            $meses_vencido22    = $meses_vencido2<=0?0:$meses_vencido2;

                            $meses_vencido2 = ($meses_vencido2<=1 && $meses_vencido2>=0)?1:$meses_vencido2; 
                            $meses_vencido2 = ($meses_vencido2<0)?0:$meses_vencido2;

                            $interes_mora       = ($key['aporteCapital']*($key['interes_mora'])*round($meses_vencido2));
                            $cuotaFija = $key['cuotaFija']+($key['capital']*($key['seguroVida']/100))+$key['interes_dia'];
                            $cuota = $key['cuotaFija']+($key['capital']*($key['seguroVida']/100))+$key['interes_dia'];
                            $seguro = $key['vida'];

                        }//if estado

                        /*cobranza*/
                        $diaMoraI 	= unserialize($info2[0]['dia_mora_ini']);
                        $diaMoraF 	= unserialize($info2[0]['dia_mora_fin']);
                        $honorario 	= unserialize($info2[0]['honorario']);
                        $tCob = 0;

                        foreach($diaMoraI as $key2 => $val){
                                if($dias>=$val && $dias<= $diaMoraF[$key2]){
                                        $tCob = $honorario[$key2];
                                }
                        }
                        $cobranza = $cuotaFija*($tCob/100);
                        //echo $i.'-'.$cobranza.'<br/>';
                        /*cobranza*/
                        
                        $meses_vencido2 = ($meses_vencido2<1 && $meses_vencido2>=0)?0:$meses_vencido2; 
                        $meses_vencido2 = ($meses_vencido2<0)?0:$meses_vencido2;
                        //$cadena.='<td>'.round($meses_vencido2,0,PHP_ROUND_HALF_UP).'</td>';
                        
                        /*restar abonos*/
                        $abono  =   mCajas::verAbono($key['elid']);
                        $abono  =   $abono[0];
                        /*fin restar*/
                        
                        /*actualizar cuota*/
                        $upd = mCartera::updCuota(
                                    $interes_mora,
                                    $cobranza,
                                    $key['elid']
                                );
                        /*fin actualizar*/
                    }

            }else{
                   // $cadena.='No se hallo informacion sobre esta solicitud, pruebe de nuevo...';
            }
            
        }


        public function pagoParcial($array){
		if(is_numeric($array["id"])){
			$info = mSolicitudes::infoDesembolso($array["id"]);
			
			$abono = mCartera::lstAbonos($array["id"]);
			$abono = $abono[0];

			$fechaHoy = opDate();
			$fechaPago = explode("/",$info[0]["fecha_pago"]);
			$fechaPago = $fechaPago[2].'-'.$fechaPago[1].'-'.$fechaPago[0];
			$diasRestantes = difFechas($fechaPago, $fechaHoy);
			
			if($diasRestantes > 0){
				setMensaje("La cartera no ha sido reliquidada","info");
				parent::pendienteReliquidar($array["id"]);
			}else{
				parent::pagoParcial($array["id"], $info[0]["total"]- $abono["abono"]);
			}
		}else
			setMensaje("Debe seleccionar una cartera valida","error");
	} // pagoParcial
	
	
	public function saveAbono($array){
		if(is_numeric($array["id"])){
			$info = mSolicitudes::infoDesembolso($array["id"]);
			//die(print_r($info));
			$abono = $array["abono"];
			
			$pdte = $abono - $info[0]["total"];

			if($pdte <= 0){
				$k 		= $info[0]["capital"];
				$ic 		= $info[0]["interes_corriente"];
				$admin1 	= $info[0]['admi1'];
				$admin2 	= $info[0]["admi2"];
				$recaudo 	= $info[0]['recaudo'];
				$mora	 	= $info[0]['interes_mora'];
				$cobranza 	= $info[0]['cobranza'];
				$iva 		= $info[0]['iva'];
				//print_r($info);
				// restando cobranza
			//	die('='.$cobranza);
				if(($abono - $iva)<0){
					$iva = $iva -$abono;
				}else{
					$saldo = $abono - $iva;
					$iva = 0;
					$abono = $saldo;
					
					if(($abono - $cobranza) < 0){
				
					$cobranza = $cobranza - $abono;
				}else{
					$saldo = $abono - $cobranza;
					$cobranza = 0;
					$abono = $saldo;
					
					// restando recaudo
					if(($abono - $recaudo) < 0){
						$recaudo = $recaudo - $abono;
					}else{
						$saldo = $abono - $recaudo;
						
						$recaudo = 0;
						$abono = $saldo;
						
						// restando admin2
						if(($abono - $admin2) < 0){
							$admin2 = $admin2 - $abono;
						}else{
							$saldo = $abono - $admin2;
							$admin2 = 0;
							$abono = $saldo;

							// restando admin1
							if(($abono - $admin1) < 0){
								$admin1 = $admin1 - $abono;
							}else{
								$saldo = $abono - $admin1;
								$admin1 = 0;
								$abono = $saldo;
								
								// restando mora
								if(($abono - $mora) < 0){
									$mora = $mora - $abono;
								}else{
									$saldo = $abono - $mora;
									$mora = 0;
									$abono = $saldo;
									
									// restando ic
									if(($abono - $ic) < 0){
										$ic = $ic - $abono;
									}else{
										$saldo = $abono - $ic;
										$ic = 0;
										$abono = $saldo;
										
										// restando capital
										if(($abono - $k) < 0){
											$k = $k - $abono;
										}else{
											$saldo = $abono - $k;
											$k = 0;
											$abono = $saldo;
										}
									}
									
								}
								
							}
							
						}
						
					}
				}
					
			}
				
				
				
				$total = $k + $ic + $admin1 + $admin2 + $recaudo + $mora + $cobranza;
				//die($total .'='. $k .'+'. $ic .'+'. $admin1 .'+'. $admin2 .'+'. $recaudo .'+'. $mora .'+'. $cobranza);
				$upd = mSolicitudes::abonarDesembolso($array["id"], $k, $ic, $admin1, $admin2, $recaudo, $mora, $cobranza, $total, $array["abono"],$iva);
				if($upd)
					setMensaje("Abono aplicado correctamente","success");
				else
					setMensaje("Error interno al intentar aplicar este abono","error");
				
				location(setUrl("cartera"));
			}
			else{
				die('jum');
				setMensaje("El valor del pago parcial es mayor al valor total a pagar","error");
				location(setUrl("cartera","pagoParcial",array("id"=>$array["id"])));
			}
		}else{
			setMensaje("Debe seleccionar una cartera valida","error");
			location(setUrl("cartera"));
		}
	} // pagoParcial
	
	
	public function pagar($array){
		validar_usuario();
		if(is_numeric($array["id"])){
			
			$info = mSolicitudes::infoDesembolso($array["id"]);
			$abono = mCartera::lstAbonos($array["id"]);
			$abono = $abono[0];
			parent::pagar($info[0], $abono);
		}else{
			setMensaje("Debe seleccionar una cartera valida","error");
			location(setUrl("cartera"));
		}
	} // pagar
	
	public function savePagoTotal($array){
		validar_usuario();
		if(is_numeric($array["id"])){
			$info = mSolicitudes::infoDesembolso($array["id"]);
			
			$fechaHoy = $array["fechaPago"]; //opDate();
			$fechaPago = $info[0]["fecha_pago"];
			// $fechaPago = $fechaPago[2].'-'.$fechaPago[1].'-'.$fechaPago[0];
			$diasRestantes = difFechas($fechaPago, $fechaHoy);
			
			if($diasRestantes != 0){
				setMensaje("La cartera no ha sido reliquidada","info");
				parent::pendienteReliquidar($array["id"]);
			}else{
				
				$fechaDesem = $info[0]["fecha_desembolso"];
				// $fechaDesem = $fechaDesem[2].'-'.$fechaDesem[1].'-'.$fechaDesem[0];
								
				// $fechaHoy = opDate();
	
				if($diasRestantes <= 0)
					$dias = difFechas($fechaDesem, $fechaHoy);
				else
					$dias = difFechas($fechaDesem, $fechaPago);
					
				$infoT = mSolicitudes::infoPorDesembolso($array["id"]);
				
				$k = $info[0]["capital"];
				$admin1 	= $infoT[0]['admi1'] == 0 ? 0: $infoT[0]['admin1'];
				$tasaDiaria = $infoT[0]["tasa"];
				$tasaAdmin2 = $infoT[0]["tasaAdmin2"];
				
				if($diasRestantes < 0)
					$ic = $infoT[0]['interes_corriente'] == 0 ? 0 : $k * $tasaDiaria * $dias;
				else
					$ic = $infoT[0]['interes_corriente'] == 0 ? 0 : $infoT[0]['interes_corriente'];
				
				$admin2 	= $infoT[0]['admi2'] == 0 ? 0 : $infoT[0]['admi2'];
				$recaudo 	= $infoT[0]['recDes'] == 0 ? 0 : $infoT[0]['recDes'];
				$mora = 0;
				$cobr = 0;
	
				if($diasRestantes > 0){
					$diaMoraI 	= unserialize($infoT[0]['dia_mora_ini']);
					$diaMoraF 	= unserialize($infoT[0]['dia_mora_fin']);
					$honorario 	= unserialize($infoT[0]['honorario']);
					// $cobroJur 	= unserialize($infoT[0]['cobro_juridico']);
					$tMora = $infoT[0]['tasaMora'];
					$tCob = 0;
					foreach($diaMoraI as $key => $val){
						if($val <= $diasRestantes && $diasRestantes <= $diaMoraF[$key])
							$tCob = $honorario[$key];
					}
					
					$mora = $k * round(($tMora/100)/30,4) * $diasRestantes;
					$cobr = round(($k + $ic + $admin1 + $admin2 + $recaudo + $mora) * ($tCob/100),4);
				}
				
				$total = $k + $ic + $admin1 + $admin2 + $recaudo + $mora + $cobr;
				
				$upd = mSolicitudes::updDesembolso($array["id"], $k, $ic, $admin1, $admin2, $recaudo, $mora, $cobr, $total, $fechaHoy);
				
				$save = mCartera::savePago($info[0]["id_solicitud"]);
				if($save){
					setMensaje("La cartera ha sido pagada exitosamente.","success");
					
					include_once 'app/sistema/mail.php';
				        $correo=new mail();
				        $correos = mParametros::lstCorreos();
				    
				        $cliente = mClientes::listarId($id[0]);
				        $cliente=$cliente[0];
				        foreach($correos As $c):
				        	$lista[] = $c["email"];
				        endforeach;
				        	$lista = implode(",",$lista);
					    $correo->enviarCorreo($lista,'Pago de credito',
					    'Credito No. '.$array["id"].' Id No: '.$infoT[0]["numero"].' Cliente: '.$infoT[0]["nombre"].' '.$infoT[0]["apellido"].' Valor Pagado: '.toMoney($array["total"]).' Fecha transaccion: '.$array["fechaHoy"]);
				        
				}
				else
					setMensaje("Error interno al intentar pagar esta cartera","error");
				location(setUrl("cartera"));
			}
		}else{
			setMensaje("Debe seleccionar una cartera valida","error");
			location(setUrl("cartera"));
		}
	} // pagar
	
	
	public function acuerdos($array){
		validar_usuario();
		if(is_numeric($array["id"])){
			$info = mSolicitudes::infoPorDesembolso($array["id"]);
			/*$tasa = mSolicitudes::lstTasas($info[0]['producto']);
			$tasaEA = $tasa[0]['tasa_ea'];
			echo $tasaEA;
			$tem = round((pow((1+ $tasaEA),(1/12))) - 1, 4); 
			$tasa = $tasa[0]['tasa_mensual'];*/
			parent::acuerdos($array["id"], $info[0]["total"] - $info[0]["abono"]);
		}else{
			setMensaje("Debe seleccionar una cartera valida","error");
			location(setUrl("cartera"));
		}
	} // acuerdos
	
	public function saveAcuerdos($array){
		validar_usuario();
		
		if(is_numeric($array["id"])){
			$upd = mSolicitudes::aprobar($array["id"],5);
			
			$total = $array["total"];
			$tasa = $array["tasa"];
			$per = $array["periodo"];
			$cuota = $array["cuota"];
			$fechaIni = $array["fechaIni"];
			$saldo = $total;
			$saldoCap = 0;
			
			$save = mAcuerdos::newAcuerdo($total, $per, $tasa, $array["id"]);
			if($save){				
				$idAc = mAcuerdos::lastAcuerdo($array["id"]);
				$idAc = $idAc[0]["id"];
				
				for($j = 1; $j <= $per; $j++){
					$i = round($saldo * ($tasa/100));
					$c = round($cuota - $i);
					$saldo = round($saldo - $c);
					$saveCuota = mAcuerdos::saveCuota($idAc, $c, $i, $saldo, $total, $fechaIni);
					$expl = explode("/", $fechaIni);
					$dia = $expl[0];
					$mes = $expl[1];
					$ano = $expl[2];
					$mes = ($mes+1) == 13 ? 1 : $mes+1;
					$ano = ($mes+1) == 13 ? $ano+1 : $ano;
					$fechaIni = $dia.'/'.$mes.'/'.$ano;
				}
				setMensaje("Acuerdo generado exitosamente","success");
			}
			else
				setMensaje("Error interno al intentar generar la nueva cartera","error");
			
		}else{
			setMensaje("Debe seleccionar una cartera valida","error");
		}
		location(setUrl("cartera"));
	} // acuerdos
	
	public function generarBou($array){
		validar_usuario();
		//die(print_r($array));
		$cadena='
			<html>

			<head>
			<meta http-equiv="Content-Type" content="text/html">
			
			<title>Cheque pago</title>
			<base href="http://prestarmas.co/prestarmas/">
			<style type="text/css">
				body{font-size: 11px; font-family: "Helvetica";}
				/*hr{border: 1px dashed #000000;}*/
				table{font-size: 7.5px;boder-collapse}
				#content {
				margin: 0.25in;
				width: 100%;
			}
			#inactivo td{background-color: #bbb;}
			.caja {
			 padding: 1em 3em;
			 margin: 1em 25%;
			}
			</style>
			</head>
			<body>';
		  
	$info2 = mSolicitudes::desembolsos2($array['creditos']);
	$info2 = $info2[0];
	$parametros = 
	$abono = mCartera::lstAbonos($val["idDes"]);
	$abono = $abono[0]["abono"];
	
	  $cadena ='';
	  $cadena.='<div style="float:left"><img src="app/img/logo.jpg" height="250px" width="250px"><br/>Informaci&oacute;n general de obligaci&oacute;n</div>';
	  $cadena.='<div ><table style="width:600px;">';
	  $cadena.='<tr>';
	  $cadena .='<td colspan="4" bgcolor="#D8D8D8"><b>Cliente:</b> <br/>'.$info2['nombre'].' '.$info2['apellido'].' '.$info2['cliente'].'</td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td bgcolor="#D8D8D8"><b>Credito</b></td>';
	  $cadena.='<td bgcolor="#D8D8D8"><b>Oficina</b></td>';
	  $cadena.='<td colspan="2" bgcolor="#D8D8D8"><b>Pague antes de </b></td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td>'.$id.'</td>';
	  $cadena.='<td>'.$info2['nombre_suc'].'</td>';
	  $cadena.='<td>'.opDate().'</td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td bgcolor="#D8D8D8"><b>Linea de credito</b></td>';
	  $cadena.='<td bgcolor="#D8D8D8"><b>Fecha desembolso</b></td>';
	  $cadena.='<td colspan="2" bgcolor="#D8D8D8"><b>Plazo</b></td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td>'.$info2['elpro'].'</td>';
	  $cadena.='<td>'.$info2['fecha_desembolso'].'</td>';
	  $cadena.='<td>'.$info2['plazo_fin'].'</td>';
	  
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td bgcolor="#D8D8D8"><b>Tasa EA</b></td>';
	  $cadena.='<td bgcolor="#D8D8D8"><b>Tasa nominal</b></td>';
	  $cadena.='<td colspan="2" bgcolor="#D8D8D8"><b>Tasa mensual</b></td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';

	  $cadena.='<td>'.round($info2['tasa_ea'],2).' %</td>';
	  $cadena.='<td>'.round($info2['tasa_nominal'],2).' %</td>';
	  $cadena.='<td>'.round($info2['tasa_mensual'],2).' %</td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td bgcolor="#D8D8D8"><b>Tasa mora</b></td>';
	  $cadena.='<td bgcolor="#D8D8D8"><b>Total a pagar</b></td>';
	  //$cadena.='<td><b>Tasa cobranza</b></td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td>3.5 %</td>';
	  $cadena.='<td><b><font size="+3">'.toMoney($array['abono']).'</font><b/></td>';
	  $cadena.='</tr>';
	  $cadena.='</table>';
	  
	 /* $cadena.='<table class="table" >';
	  $cadena.='<tr colspan="2">';
	  $cadena .='<td><b>Detalle del pago a realizar</b></td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td><b>Descripci&oacute;n</b></td>';
	  $cadena.='<td><b>Valor</b></td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td>Capital</td>';
	  $cadena.='<td>'.toMoney($info2['capital']).'</td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td>Costos administrativos</td>';
	  $cadena.='<td>'.toMoney($info2['admi1']+$info2['admi2']).'</td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td>Inter&eacute;s Corriente</td>';
	  $cadena.='<td>'.toMoney($info2['interes_corriente']).'</td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td>Inter&eacute;s Mora</td>';
	  $cadena.='<td>'.toMoney($info2['interes_mora']).'</td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td>Honorarios cobranza</td>';
	  $cadena.='<td>'.toMoney($info2['cobranza']).'</td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td>Recaudo proveedor</td>';
	  $cadena.='<td>'.toMoney($info2['recaudo']).'</td>';
	  $cadena.='</tr>';
	  $cadena.='<tr>';
	  $cadena.='<td>I.V.A</td>';
	  $cadena.='<td>'.toMoney($info2['iva']).'</td>';
	  $cadena.='</tr>';
	  $cadena.='</table></div>';*/
	  $cadena .= '</div>';
	
	  
	  $fecha= explode('-',opDate());
	  $total_pagar = $array['abono'];
	  
	  if(strlen($total_pagar[0])<10){
	    $ceros=10-(strlen($total_pagar[0]));
	    $ceros1='';
	    for($i=0;$i<$ceros;$i++){
		$ceros1=$ceros1.'0';
	    }
	  }
	  if(strlen($info2['id_des'])<10){
	    $ceros=10-(strlen($info2['id_des']));
	    $ceros2='';
	    for($i=0;$i<$ceros;$i++){
		$ceros2=$ceros2.'0';
	    }
	  }
	  
	  $barras2 = '41579012345678908020'.$ceros2.$info2['id_des'].'3900'.$ceros1.$total_pagar[0].'96'.$fecha[0].$fecha[1].$fecha[2];
	 
	  $cadena .= '<br/><br/><center><img src="http://www.prestarmas.co/prestarmas/app/librerias/php/ImgBarcode/generador.php?code='.$barras2.'"></center>';
		  
		    $cadena .= '
		    </body>
		    </html>';
		//echo($cadena);
	pdf($cadena,'app/files/clientes/'.trim($info2['cliente']).'/'.$info2['id_solicitud'].'/abono_'.opdate());
	//$url='descargar.php?file=app/files/clientes/'.trim($info2['cliente']).'/'.$info2['id_solicitud'].'/abono_'.opDate().'.pdf';
	return location('descargar.php?file=app/files/clientes/'.trim($info2['cliente']).'/'.$info2['id_solicitud'].'/abono_'.opDate().'.pdf');
	}
			
} // class
?>
