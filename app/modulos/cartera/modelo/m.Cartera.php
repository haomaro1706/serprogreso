<?php 
class mCartera{	

	public function savePago($id){
		$orm = new ORM();
		$sql = $orm	-> update("op_solicitudes")
					-> set(array("estado"=>4))
					-> where("id = ?",$id);
		$return = $orm -> exec() -> afectedRows();
		return $return;
	} // savePago
	
	
	public function lstAbonos($id){
		$orm = new ORM();
		$sql = $orm	-> select(array("sum(abono) as abono"))
				-> from ("op_solicitudes_abonosDesembolsos")
					-> where("id_desembolso = ?",$id);
		$return = $orm -> exec() -> fetchArray();
		return $return;
	} // lstAbonos
	
	public function lstAbonos2($id){
		$orm = new ORM();
		$sql = $orm	-> select()
				-> from ("op_solicitudes_abonosDesembolsos")
					-> where("id_desembolso = ?",$id)
					-> orderBy(array("id DESC"))
					-> limit("0","1");
					//die($sql->versql());
		$return = $orm -> exec() -> fetchArray();
		return $return;
	} // lstAbonos2
        
        public function creditos(){
            $orm = new ORM();
            $sql = $orm	-> select()
                        -> from ("op_solicitudes_cuotas");
                        //die($sql->versql());
            $return = $sql -> exec() -> fetchArray();
            return $return;
	} // lstAbonos2
        
         public function updCuota($mora,$cobranza,$id){
		$orm = new ORM();
		$orm	-> update("op_solicitudes_cuotas")
                        -> set(
                            array(
                                "mora"      =>  $mora,
                                "cobranza"  =>  $cobranza
                            )
                        )
                        -> where("id=?",$id);
		return $orm->exec() -> afectedRows();	
	}
	
} // class
