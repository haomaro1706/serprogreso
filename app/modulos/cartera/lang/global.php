<?php 
/*Variables globales para el modulo lang/*/
define('CARTERA_NOMBRE','Cartera');
define('CARTERA_CREAR','Crear Cartera');
define('CARTERA_EDITAR','Editar Cartera');
define('CARTERA_PAGO_TOTAL','Pago total');
define('CARTERA_RELIQUIDAR','Reliquidar Cartera');
define('CARTERA_PAGOPARCIAL','Pagos parciales');
define('CARTERA_ACUERDOS','Acuerdos de Pago');
define('CARTERA_BOUCHER','Generar extracto');
?>
