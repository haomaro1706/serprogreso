<?php 

class mReportes{
	public function listarVencidos($fecha,$dias='7'){
		$orm=new ORM();
		$orm	->select(array("des.*","sol.*","pro.producto as nombreproducto","cli.nombre","cli.apellido"))
				->from("op_solicitudes_desembolsos","des")
				->join(array("sol"=>"op_solicitudes"),'des.id_solicitud = sol.id',"INNER")
				->join(array("pro"=>"op_productos"),'sol.producto = pro.id',"INNER")
				->join(array("cli"=>"op_clientes"),'sol.cliente = cli.numero',"INNER")
				->where2('sol.plazo-'.$dias.'<= ?','datediff("'.date('Y-m-d',strtotime($fecha)).'",STR_TO_DATE(des.fecha_desembolso,\'%d/%m/%Y\'))',"OR")	//a una semana o menos de vencerse
				->where("sol.estado != ?","4");
		//die(print_r($orm->verSql()));
		$return=$orm->exec()->fetchArray(); 
		return $return;
	}
	
	public function listarDesembolso($fechaIni,$fechaFin){
		$orm=new ORM();
		$orm	-> select(array("des.*","sol.*","pro.producto as nombreproducto","cli.nombre","cli.apellido"))
				-> from("op_solicitudes_desembolsos","des")
				->join(array("sol"=>"op_solicitudes"),'des.id_solicitud = sol.id',"INNER")
				->join(array("pro"=>"op_productos"),'sol.producto = pro.id',"INNER")
				->join(array("cli"=>"op_clientes"),'sol.cliente = cli.numero',"INNER")
				->where("fecha_desembolso >= ?",date('d/m/Y', strtotime($fechaIni)))
				->where("fecha_desembolso <= ?",date('d/m/Y', strtotime($fechaFin)));
		$return=$orm->exec()->fetchArray(); 
		return $return;
	}
	
	public function listarActivos($fechaIni,$fechaFin){
		$orm=new ORM();
		$orm	-> select(array("sol.*","pro.producto as nombreproducto","cli.nombre","cli.apellido"))
				-> from("op_solicitudes","sol")
				->join(array("pro"=>"op_productos"),'sol.producto = pro.id',"INNER")
				->join(array("cli"=>"op_clientes"),'sol.cliente = cli.numero',"INNER")
				->where("fecha_pago >= ?",date('d/m/Y', strtotime($fechaIni)))
				->where("fecha_pago <= ?",date('d/m/Y', strtotime($fechaFin)))
				->where("sol.estado = ?",3)
				->where("sol.estado = ?",1,'OR');//->orderby(array("fecha_pago AS"));
		$return=$orm->exec()->fetchArray();
		return $return;
	}
	
	public function listarPagados($fechaIni,$fechaFin){
		$orm=new ORM();
		$orm	-> select(array("des.*","des.fecha_pago as fechaPago","sol.*","pro.producto as nombreproducto","cli.nombre","cli.apellido"))
				-> from("op_solicitudes_desembolsos","des")
				->join(array("sol"=>"op_solicitudes"),'des.id_solicitud = sol.id',"INNER")
				->join(array("pro"=>"op_productos"),'sol.producto = pro.id',"INNER")
				->join(array("cli"=>"op_clientes"),'sol.cliente = cli.numero',"INNER")
				->where('STR_TO_DATE(des.fecha_pago,\'%d/%m/%Y\') BETWEEN ?',date('Y-m-d', strtotime($fechaIni)).'\' AND \''.date('Y-m-d', strtotime($fechaFin)))
				->where("sol.estado = ?","4");
		//die(print($orm->verSql()));
		$return=$orm->exec()->fetchArray(); 
		//die(print_r($return));
		return $return;
	}
	
	public function listarFlujoCaja($fechaIni, $fechaFin){
		$orm=new ORM();
		$orm	-> select(array("SUM(capital) as desembolsos","SUM(recaudo) as pagos"))
				-> from("op_solicitudes_desembolsos")
				->where("fecha_desembolso >= ?",date('d/m/Y', strtotime($fechaIni)))
				->where("fecha_desembolso <= ?",date('d/m/Y', strtotime($fechaFin)));
		$returno1=$orm->exec()->fetchArray(); 
		
		$orm1=new ORM();
		$orm1	-> select(array("SUM(valor) as costos"))
				-> from("op_costos")
				->where("fecha >= ?",date('d/m/Y', strtotime($fechaIni)))
				->where("fecha <= ?",date('d/m/Y', strtotime($fechaFin)))
				->where("tipo = ?", "CO");
		$returno2=$orm1->exec()->fetchArray(); 
		
		$orm3=new ORM();
		$orm3	-> select(array("SUM(valor) as gastos"))
				-> from("op_costos")
				->where("fecha >= ?",date('d/m/Y', strtotime($fechaIni)))
				->where("fecha <= ?",date('d/m/Y', strtotime($fechaFin)))
				->where("tipo = ?", "GA");
		$returno3=$orm3->exec()->fetchArray(); 
		
		$orm4=new ORM();
		$orm4	-> select(array("SUM(valor) as egresos"))
				-> from("op_costos")
				->where("fecha >= ?",date('d/m/Y', strtotime($fechaIni)))
				->where("fecha <= ?",date('d/m/Y', strtotime($fechaFin)))
				->where("tipo = ?", "OE");
		$returno4=$orm4->exec()->fetchArray(); 
		
		$orm5=new ORM();
		$orm5	-> select(array("SUM(valor) as ingresos"))
				-> from("op_costos")
				->where("fecha >= ?",date('d/m/Y', strtotime($fechaIni)))
				->where("fecha <= ?",date('d/m/Y', strtotime($fechaFin)))
				->where("tipo = ?", "IN");
		$returno5=$orm5->exec()->fetchArray();
		
		$return[]=null;
		$return[0]['desembolsos']=$returno1[0]['desembolsos'];
		$return[0]['pagos']=$returno1[0]['pagos'];
		$return[0]['costos']=$returno2[0]['costos'];
		$return[0]['gastos']=$returno3[0]['gastos'];
		$return[0]['egresos']=$returno4[0]['egresos'];
		$return[0]['ingresos']=$returno5[0]['ingresos'];
		return $return;
	}
} // class
?>
