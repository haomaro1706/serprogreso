<?php 
/*Variables globales para el modulo lang/*/
define('REPORTES_NOMBRE','Reporte de Creditos');
define('REPORTES_CREAR','Crear Reporte');
define('REPORTES_EDITAR','Editar Busqueda Reportes');
define('ACTIVOS_NOMBRE','Creditos Activos');
define('ACTIVOS_LISTA','Lista de Creditos Activos');
define('VENCIMIENTO_NOMBRE','Creditos Vencidos y Proximos a Vencer');
define('VENCIMIENTO_LISTA','Lista de Creditos Vencidos y Prox. a vencer');
define('DESEMBOLSOS_NOMBRE','Creditos Desembolsados');
define('DESEMBOLSOS_LISTA','Lista de Desembolsos');
define('PAGADOS_NOMBRE','Creditos Pagados');
define('PAGADOS_LISTA','Lista de Creditos Pagados');
define('FLUJO_NOMBRE','Flujo de Caja');
define('FLUJO_LISTA','Lista de Operaciones Flujo de Caja');
define('BALANCE_NOMBRE','Balance de Operaciones');
define('BALANCE_LISTA','Movimientos del Balance');
define('PYG_NOMBRE','Ganancias y Perdidas');
define('PYG_LISTA','Movimientos del Estado de Ganancias y Pérdidas');
?>