<?php 

include_once 'app/modulos/reportes/vista/v.Reportes.php';
include_once 'app/modulos/reportes/modelo/m.Reportes.php';

class Reportes extends vReportes{

    public function main(){
		validar_usuario();
        parent::vMain();    
    }
	
	public function getReporteActivos($array){
		validar_usuario();
		$date1=$array['id'];
		$date2=$array['id2'];
		$fechaIni = str_replace('/', '-', $date1);
		$fechaFin = str_replace('/', '-', $date2);
		$info=mReportes::listarActivos($fechaIni,$fechaFin);
		$activos='';
		$i=1;
		if(!empty($info)){
			foreach($info as $activo){
					$activos .= "<tr><td>{$i}</td>";
					$activos .= "<td>{$activo['nombre']}";
					$activos .=  " {$activo['apellido']}</td>";
					$activos .= "<td>{$activo['plazo']}</td>";
					$activos .= "<td>{$activo['nombreproducto']}</td>";
					$activos .= "<td>".toMoney($activo['monto'])."</td></tr>";
					$i++;
			}
		}else{
			$activos="<tr><td>Sin informacion</td> <td>de Creditos</td><td> Activos, </td><td>intente de nuevo.</td><td></td></tr>";
		}
		echo $activos;
	}
	
	public function vencidos(){
		validar_usuario();
        parent::vVencidos();    
    }
	
	public function getReporteVencidos($array){
		validar_usuario();
		$date1=$array['id'];
		$dias=$array['id2']-1;
		$fechaIni = str_replace('/', '-', $date1);
		$info=mReportes::listarVencidos($fechaIni,$dias);
		$vencidos='';
		$i=1;
		if(!empty($info)){
			foreach($info as $vencido){
					$vencidos .= "<tr><td>{$i}</td>";
					$vencidos .= "<td>{$vencido['nombre']}";
					$vencidos .=  " {$vencido['apellido']}</td>";
					$vencidos .=  "<td> {$vencido['fecha_desembolso']}</td>";
					$vencidos .= "<td>{$vencido['plazo']} d&iacute;as</td>";
					$vencidos .= "<td>{$vencido['nombreproducto']}</td>";
					$vencidos .= "<td>".toMoney($vencido['monto'])."</td></tr>";
					$i++;
			}
		}else{
			$vencidos="<tr><td>Sin informacion</td> <td>de Creditos</td><td> Vencidos, </td><td>intente de nuevo.</td><td></td></tr>";
		}
		echo $vencidos;
	}
	
	public function desembolso(){
		validar_usuario();
        parent::vDesembolsados();    
    }
	
	public function getReporteDesembolso($array){
		validar_usuario();
		$date1=$array['id'];
		$date2=$array['id2'];
		$fechaIni = str_replace('/', '-', $date1);
		$fechaFin = str_replace('/', '-', $date2);
		$info=mReportes::listarDesembolso($fechaIni,$fechaFin);
		$desembolsados='';
		$i=1;
		if(!empty($info)){
			foreach($info as $desembolso){
					$desembolsados .= "<tr><td>{$i}</td>";
					$desembolsados .= "<td>{$desembolso['nombre']}";
					$desembolsados .=  " {$desembolso['apellido']}</td>";
					$desembolsados .= "<td>{$desembolso['plazo']}</td>";
					$desembolsados .= "<td>{$desembolso['nombreproducto']}</td>";
					$desembolsados .= "<td>".toMoney($desembolso['monto'])."</td></tr>";
					$i++;
			}
		}else{
			$desembolsados="<tr><td>Sin informacion</td> <td>de Creditos</td><td> Desembolsados, </td><td>intente de nuevo.</td><td></td></tr>";
		}
		echo $desembolsados;
	}
	
	public function pagados(){
		validar_usuario();
        parent::vPagados();    
    }
	
	public function getReportePagados($array){
		validar_usuario();
		$date1=$array['id'];
		$date2=$array['id2'];
		$fechaIni = str_replace('/', '-', $date1);
		$fechaFin = str_replace('/', '-', $date2);
		$info=mReportes::listarPagados($fechaIni,$fechaFin);
		$pagado='';
		$i=1;
		if(!empty($info)){
			foreach($info as $pagados){
					$pagado .= "<tr><td>{$i}</td>";
					$pagado .= "<td>{$pagados['nombre']}";
					$pagado .=  " {$pagados['apellido']}</td>";
					$pagado .= "<td>{$pagados['plazo']} d&iacute;as</td>";
					$pagado .= "<td>{$pagados['fechaPago']}</td>";
					$pagado .= "<td>{$pagados['nombreproducto']}</td>";
					$pagado .= "<td>".toMoney($pagados['monto'])."</td></tr>";
					$i++;
			}
		}else{
			$pagado="<tr><td>Sin informacion</td> <td>de Creditos</td><td> Pagados, </td><td>intente de nuevo.</td><td></td></tr>";
		}
		echo $pagado;
	}
	
	public function flujoCaja(){
		validar_usuario();
        parent::vFlujoCaja();    
    }

	public function getReporteFlujo($array){
		validar_usuario();
		$date1=$array['id'];
		$date2=$array['id2'];
		$fechaIni = str_replace('/', '-', $date1);
		$fechaFin = str_replace('/', '-', $date2);
		$info=mReportes::listarFlujoCaja($fechaIni,$fechaFin);
		$flujos='';
		$total=0;
		if(!empty($info)){
					$flujo=$info[0];
					$flujos .= "<tr><td>".toMoney($flujo['desembolsos'])."</td>";
					$flujos .= "<td>".toMoney($flujo['pagos'])."</td>";
					$flujos .= "<td>".toMoney($flujo['costos'])."</td>";
					$flujos .= "<td>".toMoney($flujo['gastos'])."</td>";
					$flujos .= "<td>".toMoney($flujo['ingresos'])."</td>";
					$flujos .= "<td>".toMoney($flujo['egresos'])."</td>";
					$total = $flujo['desembolsos']-$flujo['pagos']-$flujo['costos']-$flujo['gastos']+$flujo['ingresos']-$flujo['egresos'];
					$flujos .= "<td>".toMoney($total)."</td></tr>";
		}else{
			$flujos="<tr><td></td><td>Sin informacion</td> <td>de Flujo</td><td> de Caja, </td><td>intente de nuevo.</td><td></td><td></td></tr>";
		}
		echo $flujos;
	}
	
	public function balance(){
		validar_usuario();
        parent::vBalance();    
    }
	
	public function getBalance($array){
		validar_usuario();
	}
} // class
?>
