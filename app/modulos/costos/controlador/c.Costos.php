<?php 

include_once 'app/modulos/costos/vista/v.Costos.php';
include_once 'app/modulos/costos/modelo/m.Costos.php';
include_once 'app/sistema/ftp.php';

class Costos extends vCostos{

    public function main(){
        validar_usuario();
		$lista = mCostos::listarCostos();
		$info = '';
		if(!empty($lista)){
			$i = 1;
			foreach($lista as $key=> $val){
				$info .= "<tr><td>{$i}</td>";
				switch($val["tipo"]){
					case 'CO':
						$tipo = 'Costo';
					break;
					case 'GA':
						$tipo = 'Gasto';
					break;
					case 'OE':
						$tipo = 'Otros Egresos';
					break;
				}
				$info .= "<td>{$tipo}</td>";
				$info .= "<td>{$val["fecha"]}</td>";
				$info .= "<td>".toMoney($val["valor"])."</td>";
				$info .= "<td> <a href='".setUrl('costos','verCosto',array("id"=>$val["id"]))."'><img src='".SISTEMA_RUTAIMG."/ver.png' alt='ver'></a></td>";
				$info .= ($val["soporte"] != "") ? "<td> <a href='app/files/soportes/{$val["soporte"]}' target='_blank'><img src='".SISTEMA_RUTAIMG."/download.png'  alt='descargar'></a></td></tr>" : '<td>&nbsp;</td>';
				$i++;
			}
		}
        parent::main($info);    
    } // main
	
	
	public function crearCosto(){
        validar_usuario();
        parent::crearCosto();    
    } // crearCosto
	
	
	public function saveCosto($array){
		validar_usuario();
		if($array["tipo"]!="" && $array["fecha"]!="" && $array["valorR"]!="" && $array["descripcion"]!=""){
			if($_FILES["soporte"]["error"] == 0){
				$ftp = new ftp();
				if(!is_dir('app/files/soportes'))
					mkdir('app/files/soportes');
				if(!file_exists('app/files/soportes/index.html')){
					$file = fopen('app/files/soportes/index.html','a+');
					fclose($file);
				}
					
				$ftp->SubirArchivo($_FILES["soporte"]["name"], $_FILES["soporte"]["tmp_name"], $_FILES["soporte"]["size"], 'app/files/soportes');
			}
			$save = mCostos::saveCosto($array["tipo"], $array["fecha"], $array["valorR"], $array["descripcion"], $_FILES["soporte"]["name"]);
			if($save){
				setMensaje("Costo/gasto guardado exitosamente","success");
				if($array["location"] != "")
					location(setUrl("costos",$array["location"]));
				else
					location(setUrl("costos"));
			}else{
				setMensaje("Error interno al intentar guardar el costo/gasto","error");
				if($array["location"] != "")
					location(setUrl("costos",$array["location"]));
				else
					location(setUrl("costos","crearCosto"));
			}
			
		}else{
			setMensaje("Debe completar todos los campos","error");
			if($array["location"] != "")
					location(setUrl("costos",$array["location"]));
				else
					location(setUrl("costos","crearCosto"));
		}
	}
	
	public function verCosto($array){
		validar_usuario();
		if(is_numeric($array["id"])){
			$info = mCostos::verCosto($array["id"]);
			if(!empty($info))
				parent::verCosto($info[0]);
			else{
				setMensaje("No se ha encontrado informaci&oacute;n del costo/gasto","error");
				location(setUrl("costos"));
			}
		}else{
			setMensaje("Debe seleccionar un costo/gasto valido","error");
			location(setUrl("costos"));
		}
	} // verCosto
	
	
	public function filtroFechas($array){
		validar_usuario();
		if($array["fechaI"] != "" && $array["fechaF"] != ""){
			$lista = mCostos::filtroFechas($array["fechaI"],$array["fechaF"]);
			if(!empty($lista)){
				$info = '';
				$i = 1;
				foreach($lista as $key=> $val){
					$info .= "<tr><td>{$i}</td>";
					switch($val["tipo"]){
						case 'CO':
							$tipo = 'Costo';
						break;
						case 'GA':
							$tipo = 'Gasto';
						break;
						case 'OE':
							$tipo = 'Otros Egresos';
						break;
					}
					$info .= "<td>{$tipo}</td>";
					$info .= "<td>{$val["fecha"]}</td>";
					$info .= "<td>".toMoney($val["valor"])."</td>";
					$info .= "<td> <a href='".setUrl('costos','verCosto',array("id"=>$val["id"]))."'><img src='".SISTEMA_RUTAIMG."/ver.png' alt='ver'></a></td>";
					$info .= ($val["soporte"] != "") ? "<td> <a href='app/files/soportes/{$val["soporte"]}' target='_blank'><img src='".SISTEMA_RUTAIMG."/download.png'  alt='descargar'></a></td></tr>" : '<td>&nbsp;</td>';
					$i++;
				}
				parent::filtroFechas($info); 
			}else{
				setMensaje("No se ha encontrado informaci&oacute;n relacionada con las fechas","info");
				echo getMensaje();
			}
		}
	} // filtroFechas
	
	
	public function ingresos(){
        validar_usuario();
		$lista = mCostos::listarIngresos("IN");
		$info = '';
		if(!empty($lista)){
			$i = 1;
			foreach($lista as $key=> $val){
				$info .= "<tr><td>{$i}</td>";
				$info .= "<td>{$val["fecha"]}</td>";
				$info .= "<td>".toMoney($val["valor"])."</td>";
				$info .= "<td> <a href='".setUrl('costos','verIngreso',array("id"=>$val["id"]))."'><img src='".SISTEMA_RUTAIMG."/ver.png' alt='ver'></a></td>";
				$info .= ($val["soporte"] != "") ? "<td> <a href='app/files/soportes/{$val["soporte"]}' target='_blank'><img src='".SISTEMA_RUTAIMG."/download.png'  alt='descargar'></a></td></tr>" : '<td>&nbsp;</td>';
				$i++;
			}
		}
        parent::ingresos($info);    
    } // main
	
	
	public function crearIngreso(){
        validar_usuario();
        parent::crearIngreso();    
    } // crearIngreso
	
	
	public function verIngreso($array){
		validar_usuario();
		if(is_numeric($array["id"])){
			$info = mCostos::verCosto($array["id"]);
			if(!empty($info))
				parent::verIngreso($info[0]);
			else{
				setMensaje("No se ha encontrado informaci&oacute;n del Ingreso","error");
				location(setUrl("costos","ingresos"));
			}
		}else{
			setMensaje("Debe seleccionar un Ingreso valido","error");
			location(setUrl("costos","ingresos"));
		}
	} // verCosto
	
	public function filtroIngresosFechas($array){
		validar_usuario();
		if($array["fechaI"] != "" && $array["fechaF"] != ""){
			$lista = mCostos::filtroFechas($array["fechaI"],$array["fechaF"]);
			if(!empty($lista)){
				$info = '';
				$i = 1;
				foreach($lista as $key=> $val){
					$info .= "<tr><td>{$i}</td>";
					switch($val["tipo"]){
						case 'CO':
							$tipo = 'Costo';
						break;
						case 'GA':
							$tipo = 'Gasto';
						break;
						case 'OE':
							$tipo = 'Otros Egresos';
						break;
					}
					$info .= "<td>{$val["fecha"]}</td>";
					$info .= "<td>".toMoney($val["valor"])."</td>";
					$info .= "<td> <a href='".setUrl('costos','verIngreso',array("id"=>$val["id"]))."'><img src='".SISTEMA_RUTAIMG."/ver.png' alt='ver'></a></td>";
					$info .= ($val["soporte"] != "") ? "<td> <a href='app/files/soportes/{$val["soporte"]}' target='_blank'><img src='".SISTEMA_RUTAIMG."/download.png'  alt='descargar'></a></td></tr>" : '<td>&nbsp;</td>';
					$i++;
				}
				parent::filtroIngresosFechas($info); 
			}else{
				setMensaje("No se ha encontrado informaci&oacute;n relacionada con las fechas","info");
				echo getMensaje();
			}
		}
	} // filtroIngresosFechas
	
} // class
?>
