/*javascript para el modulo js/*/
$(function(){
	$("#valor").keyup(function(){
		format($(this))
		valor = $(this).val().replace(/\./g,'');
		$("#valorR").val(valor)
	});
	
	$("#costos").validate({
		rules:{
			tipo:{required:true},
			fecha:{required:true},
			valor:{required:true, min:0},
			descripcion:{required:true, maxlength:200},
		}
	})
	
	$("#fechas").submit(function(){
		fIni = $("#fechaI").val();
		fFin = $("#fechaF").val();
		dias = restaFechas(fIni, fFin)
		if( dias >= 0){
			$.post("costos.php/filtroFechas",{fechaI: fIni, fechaF: fFin, jquery:1},function(data){
				if(data){
					$("#divTabla").html('').html(data)
				}
			})
		}else{
			alert(" La fecha Inicial debe ser menor o igual a la fecha final");
		}
		return false;
	})
	
	$("#fechas2").submit(function(){
		fIni = $("#fechaI").val();
		fFin = $("#fechaF").val();
		dias = restaFechas(fIni, fFin)
		if( dias >= 0){
			$.post("costos.php/filtroIngresosFechas",{fechaI: fIni, fechaF: fFin, jquery:1},function(data){
				if(data){
					$("#divTabla").html('').html(data)
				}
			})
		}else{
			alert(" La fecha Inicial debe ser menor o igual a la fecha final");
		}
		return false;
	})
})

 function restaFechas(f1,f2){
	 var aFecha1 = f1.split('/'); 
	 var aFecha2 = f2.split('/'); 
	 var fFecha1 = Date.UTC(aFecha1[2],aFecha1[1]-1,aFecha1[0]); 
	 var fFecha2 = Date.UTC(aFecha2[2],aFecha2[1]-1,aFecha2[0]); 
	 var dif = fFecha2 - fFecha1;
	 var dias = Math.floor(dif / (1000 * 60 * 60 * 24)); 
	 
	 return dias;
		}
