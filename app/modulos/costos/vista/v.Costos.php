<?php 

class vCostos{
    public function main($info){
      echo getMensaje();
        ?>
           <h2><?php  echo COSTOS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  COSTOS_NOMBRE=>'#',
            		  COSTOS_CREAR=>setUrl('costos','crearCosto'),
            		  COSTOS_INGRESOS_NOMBRE=>setUrl('costos','ingresos'),
					  COSTOS_INGRESOS_CREAR=>setUrl('costos','crearIngreso'),
            		 ),
            	    COSTOS_NOMBRE
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  COSTOS_NOMBRE=>'#'
            		 ),
            	    COSTOS_NOMBRE, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo COSTOS_NOMBRE?></h4>
       <div class='container'>
        <div class='span10 offset0'>
			<fieldset>
				<legend>Filtro por fechas</legend>
				<?php
					Html::openForm("fechas","");
					
					echo inputDate("fechaI");
					Html::newInput("fechaI","Fecha Inicial");
					
					echo inputDate("fechaF");
					Html::newInput("fechaF","Fecha Final");
				?>
			</fieldset>
				<?php				
					html::newButton("consultar","Consultar");
					Html::closeForm();
				?>
				
				
			<div id="divTabla">
			<?php echo datatable('costos');?>
			<table id="costos" class="table table-hover table-striped">
				<thead>
					<tr>
						<th>#</th>
						<th>Tipo</th>
						<th>Fecha</th>
						<th>valor</th>
						<th>Ver Informaci&oacute;n</th>
						<th>Soporte</th>
					</tr>
				</thead>
				<tbody>
					<?php echo $info?>
				</tbody>
			</table>
			</div>
        </div>
       </div>
       <?php  
    } // main
	
	public function crearCosto(){
		echo getMensaje();
        ?>
           <h2><?php echo COSTOS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  COSTOS_NOMBRE=>setUrl('costos'),
            		  COSTOS_CREAR=>'#',
					  COSTOS_INGRESOS_NOMBRE=>setUrl('costos','ingresos'),
					  COSTOS_INGRESOS_CREAR=>setUrl('costos','crearIngreso'),
            		 ),
            	    COSTOS_CREAR
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  COSTOS_NOMBRE=>setUrl('costos'),
            		  COSTOS_CREAR=>'#',
            		 ),
            	    COSTOS_CREAR, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo COSTOS_CREAR?></h4>
       <div class='container'>
        <div class='span6 offset2'> 
			<fieldset>
				<legend><?php  echo COSTOS_CREAR?></legend>
			<?php 
				Html::openForm("costos",setUrl("costos","saveCosto"));
				Html::newSelect("tipo", "Tipo *",array("CO"=>"Costo",
														"GA"=>"Gasto",
														"OE"=>"Otros Egresos"));
				echo inputDate("fecha",false);
				Html::newInput("fecha", "Fecha *",opDate(false,2));
				
				Html::newInput("valor", "Valor *");
				Html::newTextarea("descripcion", "Descripcion *");
				Html::newFile("soporte", "Soporte");
			?>
			</fieldset>
			<?php
				Html::newHidden("valorR");
				Html::newButton("enviar","Enviar");
				Html::closeForm();
			?>
        </div>
       </div>
       <?php  
	} // crearCosto
	
	
	public function verCosto($info){
		echo getMensaje();
        ?>
           <h2><?php echo COSTOS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  COSTOS_NOMBRE=>setUrl('costos'),
            		  COSTOS_VER=>"#",
            		  COSTOS_CREAR=>setUrl('costos',"crearCosto"),
            		  COSTOS_INGRESOS_NOMBRE=>setUrl('costos',"ingresos"),
            		 ),
            	    COSTOS_VER
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  COSTOS_NOMBRE=>setUrl('costos'),
            		  COSTOS_VER=>'#',
            		 ),
            	    COSTOS_VER, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo COSTOS_VER?></h4>
       <div class='container'>
        <div class='span6 offset2'> 
		<form class="form-horizontal" action="<?php echo setUrl('costos')?>">
			<fieldset>
				<legend><?php  echo COSTOS_VER?></legend>
				<div class="control-group">
					<label class="control-label">Tipo:</label>
					<div class="controls">
						<span><?php echo(isset($info["tipo"]) && $info["tipo"] == 'CO')? 'Costo':'Gasto'?></span>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Fecha:</label>
					<div class="controls">
						<span><?php echo(isset($info["fecha"]))? $info["fecha"]:''?></span>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Valor:</label>
					<div class="controls">
						<span><?php echo(isset($info["valor"]))? toMoney($info["valor"]):''?></span>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Descripci&oacute;n:</label>
					<div class="controls">
						<span><?php echo(isset($info["descripcion"]))? $info["descripcion"]:''?></span>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Soporte:</label>
					<div class="controls">
						<span>
						<?php 
							echo ($info["soporte"] != "") ? "<a href='app/files/soportes/{$info["soporte"]}' target='_blank'><img src='".SISTEMA_RUTAIMG."/download.png'  alt='descargar'></a>" : '-';
						?></span>
					</div>
				</div>
			</fieldset>
				<div class="control-group">
					<div class="controls">
						<button class="btn btn-primary" type="submit">Regresar</button>
					</div>
				</div>
		</form>
        </div>
       </div>
       <?php  
	} // verCosto
	
	
	public function filtroFechas($info){
		echo datatable('costos');
	   ?>
		<table id="costos" class="table table-hover table-striped">
			<thead>
				<tr>
					<th>#</th>
					<th>Tipo</th>
					<th>Fecha</th>
					<th>valor</th>
					<th>Ver Informaci&oacute;n</th>
					<th>Soporte</th>
				</tr>
			</thead>
			<tbody>
				<?php echo $info?>
			</tbody>
		</table>
       <?php  
    } // filtroFechas
	
	
	public function ingresos($info){
      echo getMensaje();
        ?>
           <h2><?php  echo COSTOS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  COSTOS_NOMBRE=>setUrl('costos'),
            		  COSTOS_CREAR=>setUrl('costos','crearCosto'),
            		  COSTOS_INGRESOS_NOMBRE=>'#',
            		  COSTOS_INGRESOS_CREAR=>setUrl('costos','crearIngreso'),
            		 ),
            	    COSTOS_INGRESOS_NOMBRE
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  COSTOS_INGRESOS_NOMBRE=>'#'
            		 ),
            	    COSTOS_INGRESOS_NOMBRE, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo COSTOS_INGRESOS_NOMBRE?></h4>
       <div class='container'>
        <div class='span10 offset0'>
			<fieldset>
				<legend>Filtro por fechas</legend>
				<?php
					Html::openForm("fechas2","");
					
					echo inputDate("fechaI");
					Html::newInput("fechaI","Fecha Inicial");
					
					echo inputDate("fechaF");
					Html::newInput("fechaF","Fecha Final");
				?>
			</fieldset>
				<?php				
					html::newButton("consultar","Consultar");
					Html::closeForm();
				?>
				
				
			<div id="divTabla">
			<?php echo datatable('costos');?>
			<table id="costos" class="table table-hover table-striped">
				<thead>
					<tr>
						<th>#</th>
						<th>Fecha</th>
						<th>valor</th>
						<th>Ver Informaci&oacute;n</th>
						<th>Soporte</th>
					</tr>
				</thead>
				<tbody>
					<?php echo $info?>
				</tbody>
			</table>
			</div>
        </div>
       </div>
       <?php  
    } // ingresos
	
	
	public function crearIngreso(){
		echo getMensaje();
        ?>
           <h2><?php echo COSTOS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  COSTOS_NOMBRE=>setUrl('costos'),
            		  COSTOS_CREAR=>setUrl('costos','crearCosto'),
            		  COSTOS_INGRESOS_NOMBRE=>setUrl('costos','ingresos'),
            		  COSTOS_INGRESOS_CREAR=>'#',
            		 ),
            	    COSTOS_INGRESOS_CREAR
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  COSTOS_INGRESOS_NOMBRE=>setUrl('costos','ingresos'),
            		  COSTOS_INGRESOS_CREAR=>'#',
            		 ),
            	    COSTOS_INGRESOS_CREAR, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo COSTOS_INGRESOS_CREAR?></h4>
       <div class='container'>
        <div class='span6 offset2'> 
			<fieldset>
				<legend><?php  echo COSTOS_INGRESOS_CREAR?></legend>
			<?php 
				Html::openForm("costos",setUrl("costos","saveCosto"));
				echo inputDate("fecha",false);
				Html::newInput("fecha", "Fecha *",opDate(false,2));
				
				Html::newInput("valor", "Valor *");
				Html::newTextarea("descripcion", "Descripcion *");
				Html::newFile("soporte", "Soporte");
			?>
			</fieldset>
			<?php
				Html::newHidden("valorR");
				Html::newHidden("tipo","IN");
				Html::newHidden("location","crearIngreso");
				Html::newButton("enviar","Enviar");
				Html::closeForm();
			?>
        </div>
       </div>
       <?php  
	} // crearIngreso
	
	
	
	public function verIngreso($info){
		echo getMensaje();
        ?>
           <h2><?php echo COSTOS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  COSTOS_NOMBRE=>setUrl('costos'),
            		  COSTOS_CREAR=>setUrl('costos',"crearCosto"),
            		  COSTOS_INGRESOS_NOMBRE=>setUrl('costos',"ingresos"),
            		  COSTOS_INGRESOS_VER=>"#",
            		  COSTOS_INGRESOS_CREAR=>setUrl('costos',"crearIngreso"),
            		 ),
            	    COSTOS_INGRESOS_VER
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  COSTOS_INGRESOS_NOMBRE=>setUrl('costos','ingresos'),
            		  COSTOS_INGRESOS_VER=>'#',
            		 ),
            	    COSTOS_INGRESOS_VER, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo COSTOS_INGRESOS_VER?></h4>
       <div class='container'>
        <div class='span6 offset2'> 
		<form class="form-horizontal" action="<?php echo setUrl('costos','ingresos')?>">
			<fieldset>
				<legend><?php  echo COSTOS_INGRESOS_VER?></legend>
				<div class="control-group">
					<label class="control-label">Fecha:</label>
					<div class="controls">
						<span><?php echo(isset($info["fecha"]))? $info["fecha"]:''?></span>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Valor:</label>
					<div class="controls">
						<span><?php echo(isset($info["valor"]))? toMoney($info["valor"]):''?></span>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Descripci&oacute;n:</label>
					<div class="controls">
						<span><?php echo(isset($info["descripcion"]))? $info["descripcion"]:''?></span>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Soporte:</label>
					<div class="controls">
						<span>
						<?php 
							echo ($info["soporte"] != "") ? "<a href='app/files/soportes/{$info["soporte"]}' target='_blank'><img src='".SISTEMA_RUTAIMG."/download.png'  alt='descargar'></a>" : '-';
						?></span>
					</div>
				</div>
			</fieldset>
				<div class="control-group">
					<div class="controls">
						<button class="btn btn-primary" type="submit">Regresar</button>
					</div>
				</div>
		</form>
        </div>
       </div>
       <?php  
	} // verIngreso
	
	
	public function filtroIngresosFechas($info){
		echo datatable('costos');
	   ?>
		<table id="costos" class="table table-hover table-striped">
			<thead>
				<tr>
					<th>#</th>
					<th>Fecha</th>
					<th>valor</th>
					<th>Ver Informaci&oacute;n</th>
					<th>Soporte</th>
				</tr>
			</thead>
			<tbody>
				<?php echo $info?>
			</tbody>
		</table>
       <?php  
    } // filtroFechas
	
} // class
        ?>
