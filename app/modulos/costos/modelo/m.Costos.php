<?php 

class mCostos{
	
	public function listarCostos(){
		$orm = new ORM();
		$sql = $orm	-> select()
					-> from("op_costos")
					-> where("tipo IN (?)","CO','GA','OE");
		return $orm -> exec() -> fetchArray();
	} // listar
	
	
	public function listarIngresos(){
		$orm = new ORM();
		$sql = $orm	-> select()
					-> from("op_costos")
					-> where("tipo = ?","IN");
		return $orm -> exec() -> fetchArray();
	} // listar
	
	public function saveCosto($tipo, $fecha, $valor, $desc, $soporte){
		$orm = new ORM();
		$sql = $orm	-> insert("op_costos")
					-> values(array(0, $tipo, $fecha, $valor, $desc, $soporte));
		return $orm ->exec() -> afectedRows();
	} // saveCosto
	
	public function verCosto($id){
		$orm = new ORM();
		$sql = $orm	-> select()
					-> from("op_costos")
					-> where("id = ?",$id);
		return $orm -> exec() -> fetchArray();
	} // saveCosto
	
	
	public function filtroFechas($fechaI, $fechaF){
		$orm = new ORM();
		$sql = $orm	-> select()
					-> from("op_costos")
					-> where("fecha >= ?",$fechaI)
					-> where("fecha <= ?",$fechaF);
		return $orm -> exec() -> fetchArray();
	} // filtroFechas
	
	
} // class

?>
