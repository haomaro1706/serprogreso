<?php 

class mClienteparametros{
    
    	public function listar($id=false){
            $orm = new ORM();
            $sql = $orm	->select()
                        -> from ("op_clienteparametros_ref");
            if($id){
                $sql->where('id=?',$id);
            }
            $return = $sql  ->exec() ->fetchArray();
            return $return;
	} // listar
	
	
	public function saveRef($ref){
            $orm = new ORM();
            $sql = $orm	-> insert("op_clienteparametros_ref")
                            -> values(array(0, $ref, opDate()));
            $return = $sql	-> exec()-> afectedRows();
            return $return > 0 ? true : false;
	}
        
        public function updReferencia($id,$ref){
	  $orm = new ORM();
		$sql = $orm-> update("op_clienteparametros_ref")
                            -> set(
                                array(
                                 "referencia" => $ref
                                )
                              )
                            -> where("id = ?", $id);
        $sql -> exec();
		$retorno = $orm -> afectedRows() > 0 ? true : false;
        return $retorno;
	}
        
        public function delReferencia($id){
            $orm = new ORM();
		$sql = $orm -> delete()
                            -> from("op_clienteparametros_ref")
                            -> where("id = ? ", $id);
		$sql -> exec();
		$retorno = $sql -> afectedRows() > 0 ? true : false;
            return $retorno;
	}//delZona
        
        public function listarVinculo($id=false){
            $orm = new ORM();
            $sql = $orm	->select()
                        -> from ("op_clienteparametros_vin");
            if($id){
                $sql->where('id=?',$id);
            }
            $return = $sql  ->exec() ->fetchArray();
            return $return;
	} // listar
	
	
	public function saveVinculo($vinculo){
            $orm = new ORM();
            $sql = $orm	-> insert("op_clienteparametros_vin")
                            -> values(array(0, $vinculo, opDate()));
            $return = $sql	-> exec()-> afectedRows();
            return $return > 0 ? true : false;
	}
        
        public function updVinculo($id,$vin){
	  $orm = new ORM();
		$sql = $orm-> update("op_clienteparametros_vin")
                            -> set(
                                array(
                                 "vinculo" => $vin
                                )
                              )
                            -> where("id = ?", $id);
        $sql -> exec();
		$retorno = $orm -> afectedRows() > 0 ? true : false;
        return $retorno;
	}
        
        public function delVinculo($id){
            $orm = new ORM();
		$sql = $orm -> delete()
                            -> from("op_clienteparametros_vin")
                            -> where("id = ? ", $id);
		$sql -> exec();
		$retorno = $sql -> afectedRows() > 0 ? true : false;
            return $retorno;
	}//delZona
    
} // class
