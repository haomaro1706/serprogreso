<?php
include_once 'app/modulos/clienteparametros/vista/v.Clienteparametros.php';
include_once 'app/modulos/clienteparametros/modelo/m.Clienteparametros.php';

class Clienteparametros extends vClienteparametros{

    public function main(){
        validar_usuario();
        $info = mClienteparametros::listar();
        parent::main($info);    
    }
    
    public function saveReferencia($array=null){
        validar_usuario();
        $save = mClienteparametros::saveRef($array["referencia"]);
        
        if($save){
            setMensaje("Referencia creada correctamente","success");
        }else{
           setMensaje("Error al crear las referencia","error"); 
        }
        location(setUrl('clienteparametros'));
    }
    
    public function editRef($array=null){
        validar_usuario();
        $info = mClienteparametros::listar($array["id"]);
        parent::editRef($info[0]);
    }
    
    public function editReferencia($array=null){
        validar_usuario();
        $upd = mClienteparametros::updReferencia($array["id"],$array["referencia"]);
        if($upd){
            setMensaje("Referencia actualizada correctamente","success");
        }else{
           setMensaje("Error al actualizar la referencia","error"); 
        }
        location(setUrl('clienteparametros'));
    }
    
    public function delRef($array=null){
        validar_usuario();
        $del = mClienteparametros::delReferencia($array["id"]);
        if($del){
            setMensaje("Referencia eliminada correctamente","success");
        }else{
           setMensaje("Error al eliminar la referencia","error"); 
        }
        location(setUrl('clienteparametros'));
    }
    
    public function vinculos($array=null) {
        validar_usuario();
        $info = mClienteparametros::listarVinculo();
        parent::vinculos($info);
    }
    
    public function saveVinculo($array=null){
        validar_usuario();
        $save = mClienteparametros::saveVinculo($array["vinculo"]);
        if($save){
            setMensaje("Vinculo creado correctamente","success");
        }else{
           setMensaje("Error al crear el vinculo","error"); 
        }
        location(setUrl('clienteparametros','vinculos'));
    }
    
    public function editVin($array=null){
        validar_usuario();
        $info = mClienteparametros::listarVinculo($array["id"]);
        parent::editVin($info[0]);
    }
    
    public function editVinculo($array=null){
        validar_usuario();
        $upd = mClienteparametros::updVinculo($array["id"],$array["vinculo"]);
        if($upd){
            setMensaje("Vinculo actualizado correctamente","success");
        }else{
           setMensaje("Error al actualizar el vinculo","error"); 
        }
        location(setUrl('clienteparametros','vinculos'));
    }
    
    public function delVin($array=null){
        validar_usuario();
        $del = mClienteparametros::delVinculo($array["id"]);
        if($del){
            setMensaje("Vinculo eliminado correctamente","success");
        }else{
           setMensaje("Error al eliminar el vinculo","error"); 
        }
        location(setUrl('clienteparametros','vinculos'));
    }
                            
} // class
