<?php 

class vClienteparametros{
    public function main($ref){
      echo getMensaje();
        ?>
           <h2><?php  echo CLIENTEPARAMETROS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('parametros'), 
            		  CLIENTEPARAMETROS_NOMBRE=>'#',
            		  CLIENTEPARAMETROS_CREAR_VINCULOS=>setUrl('clienteparametros','vinculos'),
            		 ),
            	    CLIENTEPARAMETROS_NOMBRE
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('parametros'),
            		  CLIENTEPARAMETROS_NOMBRE=>'#'
            		 ),
            	    CLIENTEPARAMETROS_NOMBRE, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo CLIENTEPARAMETROS_NOMBRE?></h4>
       <div class='container'>
        <div class='span10 offset1'> 
            <fieldset>
                    <legend><?php echo CLIENTEPARAMETROS_CREAR_REF?></legend>
			<?php
                        Html::openForm("referencias",setUrl("clienteparametros","saveReferencia"));
                        Html::newInput("referencia","Nombre referencia *");
                        //Html::newSelect("estado", "Estado *", array("1"=>"Activo", "0"=>"Inactivo"));
			?>
			</fieldset>
			<?php
                        Html::newButton("crear", "Crear", "submit");
                        Html::closeForm();
			?>
			<?php
				if(!empty($ref)){
					echo datatable("tabla");
					Html::tabla(	
                                                array("Referencia","Fecha","Editar","Eliminar"),
                                                    $ref,
                                                    array("referencia","fecha"),
                                                    array("editar"=>setUrl("clienteparametros","editRef"),"eliminar"=>setUrl("clienteparametros","delRef"))
                                                    );
				}
			?>
        </div>
       </div>
       <?php  
    }//main
    
    public function editRef($info){
      echo getMensaje();
        ?>
           <h2><?php  echo CLIENTEPARAMETROS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('clienteparametros'), 
            		  CLIENTEPARAMETROS_EDITAR_REF=>'#'
            		 ),
            	    CLIENTEPARAMETROS_EDITAR_REF
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('clienteparametros'), 
            		  CLIENTEPARAMETROS_EDITAR_REF=>'#'
            		 ),
            	    CLIENTEPARAMETROS_EDITAR_REF, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo CLIENTEPARAMETROS_NOMBRE?></h4>
       <div class='container'>
        <div class='span10 offset1'> 
            <fieldset>
                    <legend><?php echo CLIENTEPARAMETROS_EDITAR_REF?></legend>
			<?php
                        Html::openForm("referencias",setUrl("clienteparametros","editReferencia"));
                        Html::newInput("referencia","Nombre referencia *",$info["referencia"]);
                        //Html::newSelect("estado", "Estado *", array("1"=>"Activo", "0"=>"Inactivo"));
			?>
			</fieldset>
			<?php
                        Html::newHidden("id",$info["id"]);
                        Html::newButton("actualizar", "Actualizar", "submit");
                        Html::closeForm();
			?>
        </div>
       </div>
       <?php  
    }//main
    
     public function vinculos($vin=null){
      echo getMensaje();
        ?>
           <h2><?php  echo CLIENTEPARAMETROS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('parametros'),
            		  CLIENTEPARAMETROS_NOMBRE=>setUrl('clienteparametros'),
            		  CLIENTEPARAMETROS_CREAR_VINCULOS=>'#',
            		 ),
            	    CLIENTEPARAMETROS_CREAR_VINCULOS
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('parametros'),
            		  CLIENTEPARAMETROS_CREAR_VINCULOS=>'#'
            		 ),
            	    CLIENTEPARAMETROS_CREAR_VINCULOS, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo CLIENTEPARAMETROS_NOMBRE?></h4>
       <div class='container'>
        <div class='span10 offset1'> 
            <fieldset>
                    <legend><?php echo CLIENTEPARAMETROS_CREAR_VINCULOS?></legend>
			<?php
                        Html::openForm("vinculos",setUrl("clienteparametros","saveVinculo"));
                        Html::newInput("vinculo","Nombre vinculo *");
			?>
			</fieldset>
			<?php
                        Html::newButton("crear", "Crear", "submit");
                        Html::closeForm();
			?>
			<?php
				if(!empty($vin)){
					echo datatable("tabla");
					Html::tabla(	
                                                array("Vinculo","Fecha","Editar","Eliminar"),
                                                    $vin,
                                                    array("vinculo","fecha"),
                                                    array("editar"=>setUrl("clienteparametros","editVin"),"eliminar"=>setUrl("clienteparametros","delVin"))
                                                    );
				}
			?>
        </div>
       </div>
       <?php  
    }//vinculos
    
    public function editVin($info){
      echo getMensaje();
        ?>
           <h2><?php  echo CLIENTEPARAMETROS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('clienteparametros'), 
            		  CLIENTEPARAMETROS_EDITAR_VINCULOS=>'#'
            		 ),
            	    CLIENTEPARAMETROS_EDITAR_VINCULOS
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('clienteparametros'), 
            		  CLIENTEPARAMETROS_EDITAR_VINCULOS=>'#'
            		 ),
            	    CLIENTEPARAMETROS_EDITAR_VINCULOS, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo CLIENTEPARAMETROS_NOMBRE?></h4>
       <div class='container'>
        <div class='span10 offset1'> 
            <fieldset>
                    <legend><?php echo CLIENTEPARAMETROS_EDITAR_VINCULOS?></legend>
			<?php
                        Html::openForm("vinculos",setUrl("clienteparametros","editVinculo"));
                        Html::newInput("vinculo","Nombre referencia *",$info["vinculo"]);
			?>
			</fieldset>
			<?php
                        Html::newHidden("id",$info["id"]);
                        Html::newButton("actualizar", "Actualizar", "submit");
                        Html::closeForm();
			?>
        </div>
       </div>
       <?php  
    }//main

    
} // class
