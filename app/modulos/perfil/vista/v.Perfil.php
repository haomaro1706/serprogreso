<?php             

class vPerfil{

    public function main($infoUsuario){

      echo getMensaje(); 

      ?>

           <h2><?php  echo MODULO_PERFIL?></h2>



            <?php 

            navTabs(array(OP_BACK2=>setUrl("parametros"), 

            		  MODULO_PERFIL=>"#",

            		  PERFIL_CAMBIAR_PASS=>setUrl("perfil","cambiarPassword"),

            		 ),

            	    MODULO_PERFIL

            	   );

            

            navTabs(array(OP_BACK2=>setUrl("parametros"), 

            		  MODULO_PERFIL=>"#"

            		 ),

            	    MODULO_PERFIL, 

            	    "breadcrumb"

            	   );

            ?>

       <h4><?php  echo MODULO_PERFIL?></h4>

       <div class="container">

        <div class="span7 offset2">            

        <form class="form-horizontal" enctype="multipart/form-data" name="modificarDatos" id="modificarDatos" action="<?php  echo setUrl("perfil","modificarDatos")?>" method="post">

			<fieldset>

				<legend><?php  echo MODULO_PERFIL?></legend>

                <div class="control-group">

                    <label class="control-label" for="login">Login:</label>

                    <div class="controls">

                        <p class="form-control-static"><?php  echo $infoUsuario["login"]?></p>

                    </div>

                </div>

                <div class="control-group">

                    <label class="control-label" for="nombre">Nombre:</label>

                    <div class="controls">

                        <input type="text" id="nombre" name="nombre" placeholder="Nombre" value="<?php  echo $infoUsuario["nombre"]?>">

                    </div>

                </div>

                <div class="control-group">

                    <label class="control-label" for="email">E-mail:</label>

                    <div class="controls">

                        <input type="email" name="email" id="email" placeholder="E-mail" value="<?php  echo $infoUsuario["correo"]?>">

                    </div>

                </div>

			</fieldset>

                <div class="control-group">

                    <div class="controls">   

                        <input type="hidden" id="id" name="id" value="<?php  echo $infoUsuario["id"]?>">                

                        <button type="submit" class="btn btn-primary">Modificar</button>

                    </div>

                </div>

            </form>

            </div>

        </div>

       <?php 

    } // main





    public function cambiarPassword($idUser){

      echo getMensaje(); 

      ?>

           <h2><?php  echo MODULO_PERFIL?></h2>



            <?php 

            navTabs(array(OP_BACK2=>setUrl("parametros"), 

            		  MODULO_PERFIL=>setUrl("perfil"),

            		  PERFIL_CAMBIAR_PASS=>"#",

            		 ),

            	    PERFIL_CAMBIAR_PASS

            	   );

            

            navTabs(array(OP_BACK2=>setUrl("parametros"), 

            		  MODULO_PERFIL=>setUrl("perfil"),

            		  PERFIL_CAMBIAR_PASS=>"#",

            		 ),

            	    PERFIL_CAMBIAR_PASS, 

            	    "breadcrumb"

            	   );

            ?>

       <h4><?php  echo PERFIL_CAMBIAR_PASS?></h4>

       <div class="container">

        <div class="span7 offset2">            

        <form class="form-horizontal" enctype="multipart/form-data" name="modificarPass" id="modificarPass" action="<?php  echo setUrl("perfil","modificarPass")?>" method="post">

			<fieldset>

				<legend><?php  echo PERFIL_CAMBIAR_PASS?></legend>

                <div class="control-group">

                    <label class="control-label" for="password">Contrase&ntilde;a Actual:</label>

                    <div class="controls">

                        <input type="password" id="password" name="password" placeholder="Contrase&ntilde;a Actual">

                    </div>

                </div>

                <div class="control-group">

                    <label class="control-label" for="password1">Nueva Contrase&ntilde;a:</label>

                    <div class="controls">

                        <input type="password" id="password1" name="password1" placeholder="Nueva Contrase&ntilde;a">

                    </div>

                </div>

                <div class="control-group">

                    <label class="control-label" for="password2">Repetir Contrase&ntilde;a:</label>

                    <div class="controls">

                        <input type="password" name="password2" id="password2" placeholder="Repetir Contrase&ntilde;a">

                    </div>

                </div>

			</fieldset>

                <div class="control-group">

                    <div class="controls">   

                        <input type="hidden" id="id" name="id" value="<?php  echo $idUser?>">                

                        <button type="submit" class="btn btn-primary">Modificar</button>

                    </div>

                </div>



            </form>

            </div>

        </div>

       <?php 

    } // cambiarPassword

} // class
