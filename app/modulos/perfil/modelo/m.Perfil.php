<?php 

include_once 'app/modulos/usuarios/modelo/m.Usuarios.php';



class mPerfil{



	public function myInfo($id){

		$mUsuarios = new mUsuarios();

		$retorno = $mUsuarios->infoUsuarioId($id);

		return $retorno;

	} // myInfo





	public function infoPass($id, $password){

		$orm = new ORM();

		$sql = $orm -> select()

					-> from("op_usuarios")

					-> where("id = ? ", $id)

					-> where("pass = ? ", $password);

		$retorno = $orm -> exec() -> fetchArray();

		return $retorno;

	} // infoPass







	public function modificarDatos($id, $nombre, $email){

		$orm = new ORM();

		$sql = $orm -> update("op_usuarios")

					-> set(array("nombre"=>$nombre, "correo"=>$email))

					-> where("id = ? ", $id);

		$orm -> exec();

		$retorno = $orm -> afectedRows();

		return $retorno;

	} // modificarDatos





	public function modificarPass($id, $password){

		$orm = new ORM();

		$sql = $orm -> update("op_usuarios")

					-> set(array("pass"=>$password))

					-> where("id = ? ", $id);

		$orm -> exec();

		$retorno = $orm -> afectedRows();

		return $retorno;

	} // modificarPassword

  

} // class

?>
