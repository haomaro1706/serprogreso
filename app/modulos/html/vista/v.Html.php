<?php 

class vHtml{
	
	public static function tabla($titulos, $registros, $camposAmostrar, $operaciones=''){
		
		?>
			<table class="table table-hover" id="tabla">
				<thead>
					<tr>
						<th>#</th>
						<?php
							foreach($titulos as $key => $val){
								echo '<th>'.$val.'</th>';
							}
						?>
					</tr>
				</thead>
				<tbody>
					<?php
						$i = 1;
						
						foreach($registros as $key => $val){
							echo '<tr><td>'.$i.'</td>';
							foreach ($camposAmostrar as $pos)
								if($pos == 'estado'){
									if($val[$pos] == 1){
										$estado = 'activo';
									}else{
										$estado = 'inactivo';
									}
									echo '<td><img src="app/img/'.$estado.'.png"></td>';
                                                                }else{
									echo '<td>'.( (substr($pos,0,4) == "cupo") ? toMoney($val[$pos]):$val[$pos]).'</td>';
                                                                        //echo ('='.$val[$pos]);
                                                                }
							if(!empty($operaciones))
								foreach($operaciones as $key2 =>$oper){
									if($key2=="eliminar")
										echo '<td><a onclick="return confirm(\'Seguro que desea eliminar este registro y toda su informacion relacionada?\')" href="'.$oper.'/id/'.$val["id"].'"><img src="app/img/'.$key2.'.png"></a></td>';
									else
										echo '<td><a href="'.$oper.'/id/'.$val["id"].'"><img src="app/img/'.$key2.'.png"></a></td>';
								}
							echo '</tr>';
							$i++;
						}
					?>
				</tbody>
			</table>
		<?php
	}
	
	public static function openForm($name, $action){   
    ?>
		<form action="<?php echo $action; ?>" method="post" name="<?php echo $name; ?>" id="<?php echo $name; ?>" enctype="multipart/form-data" class="form-horizontal <?php echo $name?>" >
	<?php
	} // newForm


	public static function closeForm(){
	?>
		</form>
	<?php
	} // closeForm

	/*
		Crea un span en un formulario
			parametros:
				name -> el name, id del campo
				place -> el placeholder del campo y nombre del label
				val -> el valor del campo
		ejemplo 
			<?php echo newInput("rol","Rol");?>
	*/
	public static function newSpanForm($place, $val="", $id=""){
		?>
			<div class="control-group">
				<label class="control-label"><?php echo $place; ?> : </label>
				<div class="controls">
					<span id="<?php echo $id?>"> <?php echo $val; ?></span>
				</div>
			</div>
		<?php
	} // newInput

	/*
		Crea un campo de texto
			parametros:
				name -> el name, id del campo
				place -> el placeholder del campo y nombre del label
				val -> el valor del campo
		ejemplo 
			<?php echo newInput("rol","Rol");?>
	*/
	public static function newNumber($name, $place, $val=""){
		?>
			<div class="control-group">
				<label class="control-label" for="<?php echo $name; ?>"><?php echo $place; ?> : </label>
				<div class="controls">
					<input type="number" name="<?php echo $name; ?>" id="<?php echo $name; ?>" placeholder="<?php echo str_replace("*","",$place); ?> " value="<?php echo is_null($val)?null:$val; ?>" autocomplete="off">
				</div>
			</div>
		<?php
	} // newInput


	/*
		Crea un campo de texto
			parametros:
				name -> el name, id del campo
				place -> el placeholder del campo y nombre del label
				val -> el valor del campo
		ejemplo 
			<?php echo newInput("rol","Rol");?>
	*/
	public static function newInput($name, $place, $val="",$otro=''){
		?>
			<div class="control-group">
				<label class="control-label" for="<?php echo $name; ?>" id="<?php echo $name.'2'; ?>"><?php echo $place; ?> : </label>
				<div class="controls">
					<input type="text" name="<?php echo $name; ?>" id="<?php echo $name; ?>" placeholder="<?php echo str_replace("*","",$place); ?> " value="<?php echo is_null($val)?null:$val; ?>" autocomplete="off" <?php echo $otro?>>
				</div>
			</div>
		<?php
	} // newInput

	
	public static function newReadonly($name, $place, $type="text", $val=""){
		?>
			<div class="control-group">
				<label class="control-label" for="<?php echo $name; ?>"><?php echo $place; ?> : </label>
				<div class="controls">
					<input type="<?php echo $type; ?>" name="<?php echo $name; ?>" id="<?php echo $name; ?>" placeholder="<?php echo str_replace("*","",$place); ?> " value="<?php echo is_null($val)?null:$val; ?>" readonly="readonly">
				</div>
			</div>
		<?php
	} 

	
	/*
		Crea un campo de password
			parametros:
				name -> el name, id del campo
				place -> el placeholder del campo y nombre del label
				val -> el valor del campo
		ejemplo 
			<?php echo newInput("rol","Rol");?>
	*/
	public static function newEmail($name, $place, $val=""){
		?>
			<div class="control-group">
					<label class="control-label" for="<?php echo $name; ?>"><?php echo $place; ?> : </label>
					<div class="controls">
						<input type="email" name="<?php echo $name; ?>" id="<?php echo $name; ?>" placeholder="<?php echo str_replace("*","",$place); ?> " value="<?php echo is_null($val)?null:$val; ?>" autocomplete="off">

					</div>
			</div>
		<?php
	} // newInput

	/*
		Crea un campo de password
			parametros:
				name -> el name, id del campo
				place -> el placeholder del campo y nombre del label
				val -> el valor del campo
		ejemplo 
			<?php echo newInput("rol","Rol");?>
	*/
	public static function newPassword($name, $place, $val=""){
		?>
			<div class="control-group">
				<label class="control-label" for="<?php echo $name; ?>"><?php echo $place; ?> : </label>
				<div class="controls">
					<input type="password" name="<?php echo $name; ?>" id="<?php echo $name; ?>" placeholder="<?php echo str_replace("*","",$place); ?> " value="<?php echo is_null($val)?null:$val; ?>" autocomplete="off">
				</div>
			</div>
		<?php
	} // newInput

	/*
		Crea un campo de checkbox
			parametros:
				name -> el name, id del campo
				place -> el placeholder del campo y nombre del label
				val -> el valor del campo
		ejemplo 
			<?php echo newInput("rol","Rol");?>
	*/
	public static function newCheckbox($array, $arrayNames, $place, $checked=""){
		?>
			<div class="control-group">
					<label class="control-label" for="<?php echo $name; ?>"><?php echo $place; ?> : </label>
					<div class="controls">
						<?php
							$i = 0;
							foreach($array as $key=>$value){
								?>
									<input type="checkbox" name="<?php echo $arrayNames[$i]; ?>" value="<?php echo $key; ?>" <?php echo $checked == $key? 'checked="checked"':''?>>
									<span><?php echo $value?></span>
								<?php
								$i++;
							}
						?>
					</div>
			</div>
		<?php
	} // newInput
	
	public static function newCheckbox2($array, $name, $place, $checked=array()){
		?>
			<div class="control-group">
					<label class="control-label" for="<?php echo $name; ?>"><?php echo $place; ?> : </label>
					<div class="controls">
						<?php
							//$i = 0;
							foreach($array as $key=>$value){
							  $ch='';
							  if(in_array($key,$checked)){
							    $ch='checked';
							  }
							  
							?>
							  <input type="checkbox" name="<?php echo $name; ?>[]" value="<?php echo $key; ?>" <?php echo $ch;?>>
							  <span><?php echo $value?></span>
							  <br/>
							  <?php
							  //$i++;
							}
						?>
					</div>
			</div>
		<?php
	} // newInput


	/*
		Crea un campo de checkbox
			parametros:
				name -> el name, id del campo
				place -> el placeholder del campo y nombre del label
				val -> el valor del campo
		ejemplo 
			<?php echo newInput("rol","Rol");?>
	*/
	public static function newRadio($array, $name, $place, $checked=""){
		?>
			<div class="control-group">
					<label class="control-label" for="<?php echo $name; ?>"><?php echo $place; ?> : </label>
					<div class="controls">
						<?php
							foreach($array as $key=>$value){
								?>
									<input type="radio" name="<?php echo $name; ?>" value="<?php echo $key; ?>" <?php echo $checked==$key? 'checked="checked"':''?>>
									<span><?php echo $value?></span>
								<?php
							}
						?>
					</div>
			</div>
		<?php
	} // newInput

	/*
		Crea un campo tipo FILE
			parametros:
				name -> el name, id del campo
				place -> el placeholder del campo y nombre del label
		ejemplo 
			<?php echo newInput("logo","Logo de la Empresa");?>
	*/
	public static function newFile($name, $place,$multiple=false){
		?>
		<div class="control-group">
			<label class="control-label" for="<?php echo $name; ?>"><?php echo $place; ?> : </label>
			<div class="controls">
				<input type="file" id="<?php echo $name; ?>" name="<?php echo $name; ?>" <?php echo ($multiple==true)?"multiple":""?>>
			</div>
		</div>
		<?php
	} // newInput

	/*
		Crea un campo hidden
			parametros:
				name -> el name, id del campo
				val -> el value del campo
	*/
	public static function newHidden($name, $val=""){
		?>
			<input type="hidden" name="<?php echo $name; ?>" id="<?php echo $name; ?>" value="<?php echo $val; ?>">
		<?php
	} // newHidden

	/*
		crea un campo select
			name -> for, nombre del select
			place -> el nombre del label
			arr -> el arreglo que crea los option
			sel -> el option seleccionado
			
			ejemplo 
				<?php echo newSelect("rol","Rol",array("CO"=>"Comun","SI"=>"Simplificado"),"SI");?>
	*/
	public static function newSelect($name, $place, $arr, $sel="",$otro=''){
		$cad = '';

		foreach($arr as $key=>$val){
			$cad .= '<option value="'.$key.'" '.(( !empty($sel) && $key == $sel)?'selected="selected"':'').'> '.$val.' </option>';
		}
		?>
			<div class="control-group">
					<label class="control-label" for="<?php echo $name; ?>"><?php echo $place; ?> : </label>
					<div class="controls">
                                            <select name="<?php echo $name; ?>" id="<?php echo str_replace(array('[]','[0]'),array('',''),$name); ?>" <?php echo $otro?>>
							<option value=""> - Seleccione - </option>
							<?php echo $cad; ?>
						</select>
					</div>
			</div>
		<?php
	} // newSelect



	public static function newTextarea($name, $place, $val="",$otro=''){
		?>
			<div class="control-group">
				<label class="control-label" for="<?php echo $place?>"><?php echo $place?>:</label>
				<div class="controls">
					<textarea name="<?php echo $name?>" id="<?php echo $name?>" placeholder="<?php echo $place?>" <?php echo $otro ?>><?php echo $val?></textarea>
				</div>
			</div> 
		<?php
	}
	
	/*
		Crea un Botón
			name -> nombre del botón
			val -> El value del Boton
			type -> tipo de botón (POR DEFECTO, SUBMIT)
			class -> la clase de botón (POR DEFECTO, btn-primary)
			
			ejemplo
				<?php echo newBtn("rol","Crear rol");?>
	*/
    public static function newButton($name, $val, $type="submit", $class = "btn-primary"){
        ?>
        <div class="control-group">
            <div class="controls">
                    <button type="<?php echo $type; ?>" name="<?php echo $name; ?>" id="<?php echo $name; ?>" class="btn <?php echo $class; ?>"> <?php echo $val; ?> </button>
            </div>
        </div>
        <?php
    } // newBtn
	
} // class
