<?php 

            

class vGrupos{

    public function main($info=""){

      echo getMensaje();

        ?>

           <h2><?php  echo GRUPOS_NOMBRE?></h2>



            <?php 

            navTabs(array(OP_BACK2=>setUrl('parametros'), 

            		  GRUPOS_NOMBRE=>'#'

            		 ),

            	    GRUPOS_NOMBRE

            	   );

            

            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 

            		  GRUPOS_NOMBRE=>'#'

            		 ),

            	    GRUPOS_NOMBRE, 

            	    'breadcrumb'

            	   );

            ?>

       <h4><?php  echo GRUPOS_NOMBRE?></h4>

       <div class='container'>

        <div class='span10 offset0'> 

        <form class="form-horizontal" name="new_grupo" id="new_grupo" action="<?php  echo setUrl("grupos","crearGrupo")?>" method="post">                    

			<fieldset>

				<legend> <?php  echo GRUPOS_CREAR?></legend>

				<div class="control-group">

					<label class="control-label" for="grupo">Grupo:</label>

					<div class="controls">

						<input type="text" id="grupo" name="grupo" placeholder="Grupo">

					</div>

				</div>  

				<div class="control-group">

					<label class="control-label" for="estado">Estado:</label>

					<div class="controls">

						<select name="estado" id="estado">

							<option value="">-Seleccione-</option>

							<option value="1">Activo</option>

							<option value="0">Inactivo</option>

						</select>

					</div>

				</div>  

				<div class="control-group">

					<div class="controls">                   

						<button type="submit" class="btn btn-primary">Crear</button>

					</div>

				</div>				

			</fieldset>				

		</form>

		<?php 

			if($info){

				echo datatable("lstGrupos");

		?>

			<table id="lstGrupos" class="table">

            <thead>

                <tr>

                    <th>#</th>

                    <th>Categoria</th>

                    <th>Permisos</th>

                    <th>Activo</th>

                    <th>Editar</th>

                    <th>Eliminar</th>

                </tr>

            </thead>

            <tbody>

                <?php  echo $info?>

            </tbody>

        </table>

		<?php 

			}else{

				echo "No existen grupos creados";

			}

		?>

        </div>

       </div>

       <?php  

    } // main

	

	

	public function editGrupo($info){

      echo getMensaje();

        ?>

           <h2><?php  echo GRUPOS_NOMBRE?></h2>



            <?php 

            navTabs(array(OP_BACK2=>setUrl('grupos'),

            		  GRUPOS_NOMBRE=>setUrl('grupos'),

            		  GRUPOS_EDITAR=>"#"

            		 ),

            	    GRUPOS_EDITAR

            	   );

            

            navTabs(array(OP_BACK2=>setUrl('grupos'),

            		  GRUPOS_NOMBRE=>setUrl('grupos'),

            		  GRUPOS_EDITAR=>'#'

            		 ),

            	    GRUPOS_EDITAR, 

            	    'breadcrumb'

            	   );

            ?>

       <h4><?php  echo GRUPOS_EDITAR?></h4>

       <div class='container'>

        <div class='span7 offset1'> 

        <form class="form-horizontal" name="new_grupo" id="new_grupo" action="<?php  echo setUrl("grupos","updateGrupo")?>" method="post">                    

			<fieldset>

				<legend><?php  echo GRUPOS_EDITAR?></legend>

				<div class="control-group">

					<label class="control-label" for="grupo">Grupo:</label>

					<div class="controls">

						<input type="text" id="grupo" name="grupo" placeholder="Grupo" value="<?php  echo $info["grupo"]?>">

					</div>

				</div>  

				<div class="control-group">

					<label class="control-label" for="estado">Estado:</label>

					<div class="controls">

						<select name="estado" id="estado">

							<option value="">-Seleccione-</option>

							<option value="1" <?php  echo $info["activo"]==1? 'selected="selected"':''?>>Activo</option>

							<option value="0" <?php  echo $info["activo"]==0? 'selected="selected"':''?>>Inactivo</option>

						</select>

					</div>

				</div>  

			</fieldset>

				<div class="control-group">

					<div class="controls">                   

						<input type="hidden" id="id" name="id" value="<?php  echo $info["id_grupos"]?>">

						<button type="submit" class="btn btn-primary">Modificar</button>

					</div>

				</div>				

		</form>

        </div>

       </div>

       <?php  

    } // updGrupo



	

    public function gestionPermisos($listaModulos,$idGrupo, $grupo){

    	echo getMensaje();

        ?>

           <h2><?php  echo GRUPOS_NOMBRE?></h2>



            <?php 

            navTabs(array(OP_BACK2=>setUrl("grupos"), 

            		  GRUPOS_NOMBRE=>setUrl("grupos"),

    				  GRUPOS_GESTION_PERMISOS.$grupo=>"#"

            		 ),

            	    GRUPOS_GESTION_PERMISOS.$grupo

            	   );

            

            navTabs(array(OP_BACK2=>setUrl("parametros"), 

            		  GRUPOS_NOMBRE=>setUrl("grupos"),

					  GRUPOS_GESTION_PERMISOS.$grupo=>"#"

            		 ),

            	    GRUPOS_GESTION_PERMISOS.$grupo, 

            	    "breadcrumb"

            	   );

            ?>

       <h4><?php  echo GRUPOS_GESTION_PERMISOS.$grupo?></h4>

	   <br />

       <div class="container">

        <div class="span7 offset1">

		

        <form class="form-horizontal" name="formPerms" id="formPerms" action="<?php  echo setUrl("grupos","registrarPermisos")?>" method="post">

		<table class="table table-striped table-hover table-condensed" id="permisos">

    			<thead><tr> 

    				<th> # </th>

    				<th> M&oacute;dulo </th>

    				<th> Acceso </th>

    				<th> M&aacute;s opciones </th>

    			</tr> </thead> <tbody>

					<?php  echo $listaModulos; ?>

				</tbody> </table>

        <div class="control-group">

             <div class="controls">                   

				<input type="hidden" name="idUser" value="<?php  echo $idGrupo?>">	

                                <button type="submit" class="btn btn-primary">Enviar</button>

             </div>

        </div>

        </form>

        </div>

       </div>

        <?php 

    } // gestionPermisos



	

	public function masPermisos($idModulo, $idGrupo, $modulo, $grupo, $funcs, $lista){

		echo getMensaje();

        ?>

           <h2><?php  echo GRUPOS_NOMBRE?></h2>



            <?php 

            navTabs(array(OP_BACK=>setUrl("usuarios","panelAdmin"), 

            		  GRUPOS_NOMBRE=>setUrl("grupos"),

    				  GRUPOS_GESTION_PERMISOS.$grupo=>setUrl("grupos","gestionPermisos",array("id"=>$idGrupo)),

    				  GRUPOS_MAS_PERMISOS.$grupo." sobre ".$modulo=>"#"

            		 ),

            	    GRUPOS_MAS_PERMISOS.$grupo." sobre ".$modulo

            	   );

            

            navTabs(array(OP_BACK=>setUrl("usuarios","panelAdmin"), 

            		  GRUPOS_NOMBRE=>setUrl("grupos"),

					  GRUPOS_MAS_PERMISOS.$grupo." sobre ".$modulo=>"#"

            		 ),

            	    GRUPOS_MAS_PERMISOS.$grupo." sobre ".$modulo, 

            	    "breadcrumb"

            	   );

			

			?>

			<div class="container">

				<div class="offset1 span8">

					<form action="<?php echo setUrl("grupos","saveFuncionesPermisos")?>" method="post" id="" class="form-horizontal">

					<table class="table table-hover table-striped">

						<thead>

							<tr>

								<th>#</th>

								<th>Funci&oacute;n</th>

								<th>Acceso</th>

							</tr>

						</thead>

						<tbody>

				<?php

				$i = 1;

				

				foreach($funcs as $key => $val){

					echo "<tr><td>{$i}</td><td>{$val}</td><td><input type='checkbox' name='func[]' value={$val} ".( in_array($val,$lista) ? "checked = 'checked'":"")."></td></tr>";

					$i++;

				} 

				?>

						</tbody>

					</table>

					<div class="control-group">

						<div class="controls">

							<input type="hidden" name="idModulo" id="id" value="<?php echo $idModulo?>">

							<input type="hidden" name="idGrupo" value="<?php  echo $idGrupo?>">

							<button type="submit" name="enviar" class="btn btn-primary"> Enviar </button>

						</div>

					</div>

					</form>

				</div>

			</div>

			<?php

	} // masPermisos

	

} // class

        ?>
