<?php 

            

include_once 'app/modulos/grupos/vista/v.Grupos.php';

include_once 'app/modulos/grupos/modelo/m.Grupos.php';

include_once 'app/modulos/modulos/modelo/m.Modulos.php';



class Grupos extends vGrupos{



    public function main(){

        validar_usuario();

		$mGrupos = new mGrupos();

		$lst = $mGrupos->lstGrupos();

		$cadena = "";

		if(is_array($lst)){

			$i=1;

			foreach($lst as $grupo){

				$cadena .= '<tr>';

				$cadena .= '<td>'.$i.'</td>';

				$cadena .= '<td>'.$grupo["grupo"].'</td>';

				$estado = $grupo["activo"] == 1? '<img src="">':'<img src="">';

				$activo = $grupo["activo"] == 1? 'inactivo':'activo';

				$cadena .= '<td><a href="'.setUrl("grupos","gestionPermisos", array("id"=>$grupo["id"])).'" title="Gestionar permisos"> <img src="'.SISTEMA_RUTAIMG.'permisos.png" alt="Editar"> </a> </td>';

				$cadena .= $grupo["id"]==1? '<td> </td>':'<td><a href="'.setUrl("grupos","updEstado",array("id"=>$grupo["id"], "estado"=>$grupo["activo"])).'" title="Dejar '.$activo.'"><img src="'.SISTEMA_RUTAIMG.$activo.'.png"></a></td>';

				$cadena .= $grupo["id"]==1? '<td> </td>':'<td><a href="'.setUrl("grupos","editGrupo",array("id"=>$grupo["id"])).'"><img src="'.SISTEMA_RUTAIMG.'editar.png"></a></td>';

				$cadena .= $grupo["id"]==1? '<td> </td>':'<td><a href="'.setUrl("grupos","delGrupo",array("id"=>$grupo["id"])).'"><img src="'.SISTEMA_RUTAIMG.'eliminar.png"></a></td>';

				$cadena .= '</tr>';

				$i++;

			}

		}else{

			setMensaje("No existen grupos creados.","info");

		}

        parent::main($cadena);    

    } // main

	

	

	public function crearGrupo($array){

		validar_usuario();

		if($array["grupo"] && $array["estado"]){

			$mGrupos = new mGrupos();

			$new = $mGrupos->newGrupo($array["grupo"], $array["estado"]);

			if($new){

				setMensaje("Grupo creado exitosamente","success");

			}else{

				setMensaje("Error interno al crear el Grupo","error");

			}

		}else{

			setMensaje("Debe completar todos los campos","error");

		}

		location(setUrl("grupos"));

	} // crearGrupos

	

	

	public function editGrupo($array){

		validar_usuario();

		if(is_numeric($array["id"]) && $array["id"]!=1){

			$mGrupos = new mGrupos();

			$info = $mGrupos->infoGrupo($array["id"]);

			$cadena = $info[0];

		}else{

			setMensaje("No se ha enviado informaci&oacute;n correcta del grupo","error");

			location(setUrl("grupos"));

		}

		

		parent::editGrupo($cadena);

	} // editGrupo

	

	

	public function updateGrupo($array){

		validar_usuario();

		if($array["id"] && $array["grupo"] && $array["estado"]){

			$mGrupos = new mGrupos();

			$upd = $mGrupos->updateGrupo($array["id"],$array["grupo"],$array["estado"]);

			if($upd){

				setMensaje("Grupo modificado exitosamente","success");

			}else{

				setMensaje("Error interno al intentar modificar el grupo","error");

			}

		}else{

			setMensaje("Debe completar todos los campos","error");

		}

		location(setUrl("grupos"));

	} //updateGrupo

	

	

	function delGrupo($array){

		if(is_numeric($array["id"]) || $$array["id"] == 1){

			$mGrupos = new mGrupos();

			$del = $mGrupos->delGrupo($array["id"]);

			if($del) setMensaje("El Grupo ha sido eliminado exitosamente","success");

			else setMensaje("Error interno al intentar eliminar el Grupo","error");

		}else{

			setMensaje("No se ha encontrado la información del Grupo","error");

		}

		location(setUrl("grupos"));

	} // delGrupo

    

	

	function gestionPermisos($arrayParams){

        validar_usuario();

        $idGrupo = $arrayParams["id"];

        if(!$idGrupo){

            setMensaje ("No se ha encontrado la informaci&oacute;n del Grupo", "error");

			location(setUrl("grupos"));

        }

		

    	$modulo=new mModulos();

        $modulos=$modulo->cargarModulos(1);



        $mUsuarios = new mUsuarios();

        $mGrupos = new mGrupos();

        $grupo = $mGrupos->infoGrupo($idGrupo);

		$grupo = $grupo[0]["grupo"];

		$arrayPerms = $mUsuarios->permisosActuales($idGrupo);

        // $arrayPerms = unserialize($permisosActuales["modulos"]);

	

	

        $cadena = '';

		$i=1;

    	foreach($modulos as $key=>$modulo){

			$checked = '';

			if(is_array($arrayPerms)){

					foreach($arrayPerms as $key => $val){

						if(in_array($modulo["id"], $val)){

							$checked = 'checked="checked"';

							break;

						}

						

					}

			}

    	     

    		$cadena .= '<tr> <td>'.$i.'</td>';

    		$cadena .= '<td>'.$modulo["titulo"].'</td>';

    		$cadena .= '<td> <input type="checkbox" name="modulos[]" value="'.$modulo["nombre"].'"'.$checked.'> </td>';

    		$cadena .= '<td> <a href="'.setUrl("grupos","masPermisos",array("id"=>$modulo["id"], "grupo"=>$idGrupo)).'"><img src="'.SISTEMA_RUTAIMG.'/editar.png"></a> </td>';

    		$i++;

    	}

    	$cadena = utf8_encode($cadena);



        parent::gestionPermisos($cadena, $idGrupo, $grupo);

    } // gestionPermisos



    

    function registrarPermisos($arrayCampos){

        validar_usuario();

        $idUser = $arrayCampos["idUser"];



      	if(!$idUser){

            setMensaje ("No se ha encontrado la informaci&oacute;n del Grupo", "error");

			location(setUrl("grupos"));

        }



        $arrayPerms = serialize($arrayCampos["modulos"]);



    	$mUsuarios = new mUsuarios();



        $permisosActuales = $mUsuarios->permisosActuales($idUser);

       // die(print_r($permisosActuales));

        if($permisosActuales[0]["id_grupo"] != $idUser){

      	    $permisos = $mUsuarios->registrarPermisos($idUser, $arrayPerms);

        }else{

            $permisos = $mUsuarios->actualizarPermisos($idUser, $arrayPerms);

        }

        

        if($permisos){

            setMensaje ("Se han registrado los permisos con exito", "success");

        }

        else{

            setMensaje ("No se han podido registrar los permisos correctamente", "error");

        }

		location(setUrl("grupos"));

    } // registrarPermisos

	

	

	public function updEstado($array){

		if($array["id"]){

			$estado = $array["estado"] == 1? 0:1;

			$mGrupos = new mGrupos();

			$upd = $mGrupos->updEstado($array["id"],$estado);

			setMensaje("Grupo actualizado exitosamente","success");

		}else{

			setMensaje("No se ha enviado informacion correctamente","error");

		}

		location(setUrl("grupos"));

	} // updEstado

	

	

	public function masPermisos($array){

		if(is_numeric($array["id"])){

			$grupo = mGrupos::infoGrupo($array["grupo"]);

			$grupo = $grupo[0]["grupo"];

			

			$modulo = mModulos::infoModulo($array["id"]);

			$titulo = $modulo[0]["titulo"];

			$nombre = $modulo[0]["nombre"];

			

			$lista = mGrupos::findInfo($array["id"], $array["grupo"]);

			

			if(!empty($lista))

				$lista = unserialize($lista[0]["funciones"]);

			else

				$lista = array();

			include_once("app/modulos/{$nombre}/controlador/c.".ucfirst($nombre).".php");

			parent::masPermisos($array["id"], $array["grupo"], $titulo, $grupo, get_class_methods(ucfirst($nombre)), $lista);

		}else{

			setMensaje("Debe seleccionar un modulo valido","warning");

			location(setUrl("grupos","gestionPermisos",array("id"=>$array["grupo"])));

		}

	} // masPermisos

	

	

	public function saveFuncionesPermisos($array){

		validar_usuario();

		if($array["idModulo"] != "" && $array["idGrupo"] != ""){

			$funcs = serialize($array["func"]);

			$save = mGrupos::findInfo($array["idModulo"], $array["idGrupo"]);

			if(!empty($save))

				$save = mGrupos::updFuncionesPerms($array["idModulo"], $array["idGrupo"], $funcs);

			else

				$save = mGrupos::saveFuncionesPerms($array["idModulo"], $array["idGrupo"], $funcs);

				

			if($save)

				setMensaje("Cambios guardados exitosamente","success");

			else

				setMensaje("Debe seleccionar un modulo valido","error");

			location(setUrl("grupos","masPermisos",array("id"=>$array["idModulo"], "grupo"=>$array["idGrupo"])));

		}else{

			setMensaje("Debe seleccionar un modulo valido","error");

			location(setUrl("grupos"));

		}

	}

                            

} // class

?>
