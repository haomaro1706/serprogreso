<?php             

class mGrupos{

	

	public function lstGrupos(){

		$orm = new ORM();

		$sql = $orm -> select()

					-> from("op_grupos")

					-> orderBy(array("grupo"));

		$return = $orm -> exec() ->fetchArray();

		return $return;

	} // lstGrupos

	

	

	public function newGrupo($grupo, $estado){

		$orm = new ORM();

		$sql = $orm -> insert("op_grupos")

					-> values(array(0,$grupo,$estado));

		$orm -> exec();

		$return = $orm -> afectedRows() > 0 ? true : false;

		return $return;

	} // lstGrupos	

	

	

	public function infoGrupo($id){

		$orm = new ORM();

		$sql = $orm -> select()

					-> from("op_grupos")

					-> where("id = ? ", $id);

		$return = $orm -> exec() -> fetchArray();

		return $return;

	} // lstGrupos

	

	

	public function updateGrupo($id, $grupo, $estado){

		$orm = new ORM();

		$sql = $orm -> update("op_grupos")

					-> set(array("grupo"=>$grupo, "activo"=>$estado))

					-> where("id = ? ", $id);

		$orm -> exec();

		$return = $orm -> afectedRows() > 0 ? true : false;

		return $return;

	} // updateGrupos

	

	

	public function delGrupo($id){

		$orm = new ORM();

		$sql = $orm -> delete()

					-> from("op_grupos")

					-> where("id = ? ", $id);

		$orm -> exec();

		$return = $orm -> afectedRows() > 0 ? true : false;

		return $return;

	}

	

	

	public function updEstado($id, $estado){

		$orm = new ORM();

		$sql = $orm -> update("op_grupos")

					-> set(array("activo"=>$estado))

					-> where("id = ? ", $id);

		$orm -> exec();

		return $orm -> afectedRows() > 0 ? true : false;

	} // updEstado

	

	

	public function findInfo($idModulo, $idGrupo){

		$orm = new ORM();

        $sql = $orm -> select()

					-> from ("op_modulos_funcionperms")

					-> where("id_modulo = ? ", $idModulo)

					-> where("id_grupo = ? ", $idGrupo);                  

		

		$retorno = $orm -> exec() -> fetchArray();

        return $retorno;

    } // registrarPermisos



	

	public function saveFuncionesPerms($idModulo, $idGrupo, $funcs){

		$orm = new ORM();

        $sql = $orm -> insert("op_modulos_funcionperms")

					-> values(array(0, $idModulo, $idGrupo, $funcs));                      



		$retorno = $orm -> exec() -> afectedRows() > 0 ? true : false;

        return $retorno;

    } // registrarPermisos





    public function updFuncionesPerms($idModulo, $idGrupo, $funcs){

		$orm = new ORM();

        $sql = $orm -> update("op_modulos_funcionperms")

					-> set(array("funciones"=>$funcs))

					-> where("id_modulo = ? ", $idModulo)

					-> where("id_grupo = ? ", $idGrupo);

					

		$retorno = $orm -> exec() -> afectedRows() > 0 ? true : false;

        return $retorno;

    } // actualizarPermisos

} // class

?>
