<?php 

class vConfiguracion{

    

    public function main($sitio){

      

        echo getMensaje();

        ?><h2><?php  echo MODULO_configuracion?></h2><?php                 

        navTabs(array(OP_BACK2=>setUrl("parametros","main"), 

            		  MODULO_configuracion_SITIO=>setUrl("configuracion","main"),

            		  MODULO_configuracion_SERVIDOR=>setUrl("configuracion","servidor")/*,

            		  MODULO_configuracion_EMPRESA=>setUrl("configuracion","empresa")*/

            		 ),

            	    MODULO_configuracion_SITIO

            	   );

            

            navTabs(array(OP_BACK2=>setUrl("parametros","main"), 

            		  MODULO_configuracion_SITIO=>"#"

            		 ),

            	    MODULO_configuracion_SITIO, 

            	    "breadcrumb"

            	   );                

        ?>

        <div class="container">

		<div class="span8 offset1">

        <form class="form-horizontal" name="sitio" id="sitio" action="<?php  echo setUrl("configuracion","config_process")?>" method="post" enctype="multipart/form-data">                    

		<fieldset>

			<legend>Configuración del sitio</legend>

					

					<div class="control-group">

                        <label class="control-label" for="titulo">Titulo del sitio:</label>

                        <div class="controls">

                            <input type="text" id="titulo" name="titulo" placeholder="Titulo del sitio" value="<?php  echo $sitio["titulo"]?>">

                        </div>                        

                    </div>

		</fieldset>

		<fieldset>

            <legend>Parámetros de los metadatos</legend>

            

                    <div class="control-group">

                        <label class="control-label" for="meta_des">Meta-descripción del sitio:</label>

                        <div class="controls">

                            <textarea id="meta_des" name="meta_des" placeholder="Opencode framework"><?php  echo $sitio["meta_des"]?></textarea>                            

                        </div>

                    </div>

            

                    <div class="control-group">

                        <label class="control-label" for="meta_claves">Meta-palabras clave del sitio:</label>

                        <div class="controls">

                            <textarea id="meta_claves" name="meta_claves" placeholder="cms-framework-html5"><?php  echo $sitio["meta_claves"]?></textarea>                            

                        </div>

                    </div>

            

			</fieldset>

				<div class="control-group">

					<div class="controls">                   

						<button type="submit" class="btn btn-primary">Guardar configuración</button>

						<input type="hidden" name="config" value="sitio">

					</div>

				</div>

			</fieldset>

            </form>

            </div>

        </div>

        <?php 

        

    }//main

    

    

    public function servidor($sitio){

        //die(print_r($sitio));

        echo getMensaje();

        ?><h2><?php  echo MODULO_configuracion?></h2><?php                 

        navTabs(array(OP_BACK2=>setUrl("parametros","main"), 

            		  //MODULO_configuracion_SITIO=>setUrl("configuracion","main"),

            		  MODULO_configuracion_SERVIDOR=>"#",

            		  MODULO_configuracion_correos=>setUrl("configuracion","correos")

            		 ),

            	    MODULO_configuracion_SERVIDOR

            	   );

            

            navTabs(array(OP_BACK2=>setUrl("parametros","main"), 

            		  MODULO_configuracion_SERVIDOR=>"#"

            		 ),

            	    MODULO_configuracion_SERVIDOR, 

            	    "breadcrumb"

            	   );                

        ?>

        <div class="container">

            <div class="span8 offset1">

        <form class="form-horizontal" name="servidor" id="servidor" action="<?php  echo setUrl("configuracion","config_process")?>" method="post">                    

			<fieldset>

				<legend>Párametros ftp</legend>

                    <div class="control-group">

                        <label class="control-label" for="servidor">Servidor:</label>

                        <div class="controls">

                            <input type="text" id="servidor" name="servidor" placeholder="Servidor" value="<?php  echo $sitio["servidor"]?>">

                        </div>                        

                    </div>

                    

                    <div class="control-group">

                        <label class="control-label" for="puerto">Puerto:</label>

                        <div class="controls">

                            <input type="text" id="puerto" name="puerto" placeholder="Puerto" value="<?php  echo $sitio["puerto"]?>">

                        </div>

                    </div>

                    

                    <div class="control-group">

                        <label class="control-label" for="usuario">Usuario:</label>

                        <div class="controls">

                             <input type="text" id="Usuario" name="usuario" placeholder="Usuario" value="<?php  echo $sitio["usuario"]?>">                               

                        </div>

                    </div>

            

                    <div class="control-group">

                        <label class="control-label" for="pass">Contraseña:</label>

                        <div class="controls">

                             <input type="text" id="pass" name="pass" placeholder="Contraseña" value="<?php  echo $sitio["pass"]?>">                               

                        </div>

                    </div>

			</fieldset>

			<fieldset>

                <legend>Configuración de correo</legend>

                

                    <div class="control-group">

                        <label class="control-label" for="dir_remitente">Dirección remitente:</label>

                        <div class="controls">

                             <input type="text" id="dir_remitente" name="dir_remitente" placeholder="Dirección remitente" value="<?php  echo $sitio["dir_remitente"]?>">

                        </div>

                    </div>

                

                    <div class="control-group">

                        <label class="control-label" for="nombre_remitente">Nombre remitente:</label>

                        <div class="controls">

                             <input type="text" id="nombre_remitente" name="nombre_remitente" placeholder="Nombre remitente" value="<?php  echo $sitio["nombre_remitente"]?>">

                        </div>

                    </div>

                

                    <div class="control-group">

                        <label class="control-label" for="pass_correo">Contraseña:</label>

                        <div class="controls">

                             <input type="text" id="pass_correo" name="pass_correo" placeholder="Contraseña" value="<?php  echo $sitio["pass_correo"]?>">

                        </div>

                    </div>

                

                    <div class="control-group">

                        <label class="control-label" for="puerto_correo">Puerto:</label>

                        <div class="controls">

                             <input type="text" id="puerto_correo" name="puerto_correo" placeholder="Puerto" value="<?php  echo $sitio["puerto_correo"]?>">

                        </div>

                    </div>

                

                    <div class="control-group">

                        <label class="control-label" for="server">Servidor:</label>

                        <div class="controls">

                             <input type="text" id="server" name="server" placeholder="Servidor" value="<?php  echo $sitio["server"]?>">

                        </div>

                    </div>

                    

                    <div class="control-group">

                        <label class="control-label" for="server">Dominio:</label>

                        <div class="controls">

                             <input type="text" id="dominio" name="dominio" placeholder="Dominio" value="<?php  echo $sitio["dominio"]?>">

                        </div>

                    </div>

                </fieldset>

                    <div class="control-group">

                        <div class="controls">                   

                            <button type="submit" class="btn btn-primary">Guardar configuración</button>

                            <input type="hidden" name="config" value="servidor">

                        </div>

                    </div>



            </form>

            </div>

        </div>

        <?php       

    }//servidor

    

    public function empresa($info=null){

        //die(print_r($sitio));

        echo getMensaje();

        ?><h2><?php  echo MODULO_configuracion?></h2><?php                 

        navTabs(array(OP_BACK=>setUrl("usuarios","panelAdmin"), 

            		  MODULO_configuracion_SITIO=>setUrl("configuracion","main"),

            		  MODULO_configuracion_SERVIDOR=>setUrl("configuracion","servidor"),

            		  MODULO_configuracion_EMPRESA=>'#'

            		 ),

            	    MODULO_configuracion_EMPRESA

            	   );

            

            navTabs(array(OP_BACK=>setUrl("usuarios","panelAdmin"), 

            		  MODULO_configuracion_EMPRESA=>"#"

            		 ),

            	    MODULO_configuracion_SERVIDOR, 

            	    "breadcrumb"

            	   );                

        ?>

        <div class="container">

            <div class="span8 offset1">

        <form class="form-horizontal" name="empresa" id="empresa" action="<?php  echo setUrl("configuracion","config_process")?>" method="post">                    

			<fieldset>

				<legend><?php echo MODULO_configuracion_SERVIDOR?></legend>

                    <div class="control-group">

                        <label class="control-label" for="nombre">Nombre:</label>

                        <div class="controls">

                            <input type="text" id="nombre" name="nombre" placeholder="Nombre" value="<?php echo $info['nombre']?>">

                        </div>                        

                    </div>

                    

                    <div class="control-group">

                        <label class="control-label" for="nit">Nit:</label>

                        <div class="controls">

                            <input type="text" id="nit" name="nit" placeholder="Nit" value="<?php echo $info['nit']?>">

                        </div>

                    </div>

                    

                    <div class="control-group">

                        <label class="control-label" for="contacto">Contacto:</label>

                        <div class="controls">

                             <input type="text" id="contacto" name="contacto" placeholder="Contacto" value="<?php echo $info['contacto']?>">

                        </div>

                    </div>

            

                    <div class="control-group">

                        <label class="control-label" for="correo">Correo:</label>

                        <div class="controls">

                             <input type="text" id="correo" name="correo" placeholder="Correo" value="<?php echo $info['correo']?>">

                        </div>

                    </div>

                                

                    <div class="control-group">

                        <label class="control-label" for="pais">Pais:</label>

                        <div class="controls">

                            <select name="pais" id="pais">

                                <option value="">-Seleccione-</option>

                                <?php echo paises($info['pais'])?>

                            </select>

                        </div>

                    </div>

                                

                    <div class="control-group">

                        <label class="control-label" for="dep">Departamento:</label>

                        <div class="controls">

                             <select name="dep" id="dpe">

                                <option value="">-Seleccione-</option>

                            </select>

                        </div>

                    </div>

                                

                    <div class="control-group">

                        <label class="control-label" for="ciudad">Ciudad:</label>

                        <div class="controls">

                             <select name="ciudad" id="ciudad">

                                <option value="">-Seleccione-</option>

                            </select>

                        </div>

                    </div>

            </fieldset>

			

                    <div class="control-group">

                        <div class="controls">                   

                            <button type="submit" class="btn btn-primary">Guardar configuración</button>

                            <input type="hidden" name="config" value="sistema">

                        </div>

                    </div>



            </form>

            </div>

        </div>

        <?php       

    }//servidor

    

    

    

    public function correos($datos=null){

      echo getMensaje();

        ?>

           <h2><?php  echo MODULO_configuracion_correos?></h2>



            <?php 

            navTabs(array(OP_BACK2=>setUrl("parametros","main"), 

                            MODULO_configuracion_SERVIDOR=>setUrl("configuracion","servidor"),

                            MODULO_configuracion_correos=>"#"

            		 ),

            	    MODULO_configuracion_correos

            	   );

            

            navTabs(array(OP_BACK2=>setUrl("parametros","main"), 

            		  MODULO_configuracion_correos=>'#'

            		 ),

            	    MODULO_configuracion_correos, 

            	    'breadcrumb'

            	   );

            ?>

       <h4><?php  echo "Lista de Correos Electronicos"?></h4>

       <div class='container'>

        <div class='span10 offset0'> 

		

                        <?php

                        Html::openForm("correos",setUrl("configuracion","guardarCorreo"));

                        echo selectZonas();

                        ?>

                        <div id="verSucur"></div>

			<?php  

			  Html::newInput("lstClientes","Usuario *");

                          $fun = funcionescorreos();

                          foreach ($fun As $key=>$f):

                              	echo '<input type="checkbox" name="funcion[]" value="'.$key.'">'.$f.'<br>';

                          endforeach;

		  ?>

	  </fieldset>

        <?php

		  Html::newButton("enviar", "Guardar");

		  Html::closeForm();

		if(!empty($datos)){

			echo datatable("tabla");

                        ?>

                    <table id="tabla" class="table table-hover">

                        <thead>

                                <tr>

                                <th>#</th>

                                <th>Sucursal</th>

                                <th>Nombre</th>

                                <th>E-mail</th>

                                <th>Descripcion</th>

                                <th>Editar</th>

                                <th>Eliminar</th>

                                </tr>

                        </thead>

                        <tbody>

                            <?php

                            $cadena = '';

                            $funciones = funcionescorreos();

                            $i=1;

                            foreach ($datos as $d):

                                $cadena .= '<tr>';

                                $cadena .= '<td>'.$i++.'</td>';

                                $cadena .= '<td>'.$d['nombre_suc'].'</td>';

                                $cadena .= '<td>'.$d['nombre'].'</td>';

                                $cadena .= '<td>'.$d['correo'].'</td>';

                                $cadena .= '<td>';

                                $fun=unserialize($d['funcionalidad']);

                                foreach ($fun as $f):

                                    $cadena .= '-'.$funciones[$f].'<br/>';

                                endforeach;

                                $cadena .='</td>';

                                $cadena .= '<td><a href="'.setUrl('configuracion','editar',array('id'=>$d['id'])).'">Editar</a></td>';

                                $cadena .= '<td><a href="'.setUrl('configuracion','eliminar',array('id'=>$d['id'])).'">Eliminar</a></td>';

                                $cadena .= '</tr>';

                            endforeach;

                            echo $cadena;

                            ?>

			</tbody>

			</table>

                        <?php

		}else{

		  ?>

		  <div class="alert alert-info">No hay prodcutos disponibles.</div>

		  <?php

		}

	?>

        </div>

       </div>

       <?php  

    }

	

	public function crearCorreo($zonas){

      echo getMensaje();

        ?>

           <h2><?php  echo PARAMETROS_NOMBRE?></h2>



            <?php 

            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 

            		  TASAS_NOMBRE=>setUrl('parametros'),

					  CONTACTOS_NOMBRE=>setUrl('parametros','correos'),

            		  CONTACTOS_CREAR=>'#',

					  COBRANZAS_NOMBRE=>setUrl('parametros','cobranzas'),

					  PLAZOS_NOMBRE=>setUrl('parametros','plazos'),

					  ADMINISTRATIVOS_NOMBRE=>setUrl('parametros','administrativos'),

                                          SERVICIOS=>setUrl('parametros','servicios'),

					  SERVICIOS_DIAS_MORA=>'#'

            		 ),

            	    CONTACTOS_CREAR

            	   );

            

            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 

            		  CONTACTOS_NOMBRE=>'#'

            		 ),

            	    CONTACTOS_CREAR, 

            	    'breadcrumb'

            	   );

            ?>

       <h4><?php  echo CONTACTOS_CREAR?></h4>

       <div class='container'>

        <div class='span8 offset1'> 

	  <fieldset>

			  <legend><?php echo "Informacion Correo Electronico"?></legend>

		  <?php

			  Html::openForm("correos",setUrl("parametros","guardarCorreo"));

			  ?>

			  <div class="control-group">

					<label class="control-label" for="id_zona">Zona :</label>

					<div class="controls">

						<select name="id_zona" id="id_zona">

						<option value="">-Seleccione-</option>

						<?php  echo $zonas?>

					</select>

					</div>

				</div>

			<?php  

			  Html::newInput("nombre","Nombre *");

			  Html::newInput("apellido","Apellido *");

			  Html::newInput("email","E-mail *");

			  Html::newInput("descripcion","Descripcion *");

		  ?>

	  </fieldset>

        <?php

		  Html::newButton("enviar", "Guardar", "submit");

		  Html::closeForm();

	  ?>

	   </div>

       </div>

       <?php 

	}

	

	public function editar($info=null){

      echo getMensaje();

        ?>

           <h2><?php  echo MODULO_configuracion_correos?></h2>



            <?php 

            navTabs(array(OP_BACK2=>setUrl('configuracion','correos'), 

            		  MODULO_configuracion_correos=>"#"

            		 ),

            	    MODULO_configuracion_correos

            	   );

            

            navTabs(array(OP_BACK2=>setUrl('configuracion','correos'), 

            		  MODULO_configuracion_correos=>'#'

            		 ),

            	    MODULO_configuracion_correos, 

            	    'breadcrumb'

            	   );

            ?>

       <h4><?php  echo MODULO_configuracion_correos?></h4>

       <div class='container'>

        <div class='span8 offset1'> 

	  <fieldset>

                        <legend><?php echo "Informacion Correo Electronico"?></legend>

                        <?php

			  Html::openForm("correos",setUrl("configuracion","updCorreo"));

			  echo selectZonas($info['id_zona']);

                        ?>

                        <div id="verSucur"></div>

			<?php  

			  Html::newInput("lstClientes","Usuario *",$info['id_usu'].'-'.$info['nombre']);

			  $fun = funcionescorreos();

                          $funUsu = unserialize($info['funcionalidad']);

                          foreach ($fun As $key=>$f):

                                $cheked = ($funUsu[$key]=$f)?'checked':'';

                              	echo '<input type="checkbox" name="funcion[]" value="'.$key.'" '.$cheked.'>'.$f.'<br>';

                          endforeach;

                        ?>

	  </fieldset>

        <?php

		  Html::newHidden("id",$info['elid']);

		  Html::newButton("enviar", "Editar", "submit");

		  Html::closeForm();

	  ?>

	   </div>

       </div>

       <?php 

	}

	

    

} // class
