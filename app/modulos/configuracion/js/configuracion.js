/*javascript para el modulo js/*/
$(document).ready(function(){
    $("#sitio").validate({
        rules:{
            titulo:{required:true,lettersonly:true},
            activo:{required:true},
            mensaje:{required:true,minlength:10,maxlength:100},
            meta_des:{required:true,minlength:10,maxlength:100},
            meta_claves:{required:true,minlength:10,maxlength:100}
        }
    });
    
    $("#sistema").validate({
        rules:{
            sesion:{required:true,number:true},
            registro:{required:true},
            ruta_img:{required:true}
        }
    });
    
    $("#servidor").validate({
        rules:{
            servidor:{required:true},
            tipo:{required:true,lettersonly:true},
            puerto:{required:true,number:true},
            usuario:{required:true,email:true},
            pass:{required:true},
            db_servidor:{required:true,lettersonly:true},
            db_usuario:{required:true,lettersonly:true},
            bd_pass:{required:true},
            bd_nombre:{required:true,lettersonly:true},
            dir_remitente:{required:true,email:true},
            nombre_remitente:{required:true,lettersonly:true}
        }
    });
    
    $.post('index.php/clientes/lstClientes2',{jquery:1,estado:1},function(data){
    var availableTags = data.split(',');
	$( "#lstClientes" ).autocomplete({
	    source: availableTags
	});
	$( "#lstCodeudores" ).autocomplete({
	    source: availableTags
	});
    });
    
    $("#zona").change(function(){
            $.post('index.php/solicitudes/listarSucur',{jquery:1,id_zona:$(this).val()},function(data3){
                    $("#verSucur").html(data3);
            });
    });
    
    $.post('index.php/solicitudes/listarSucur',{jquery:1,id_zona:$("#zona").val()},function(data3){
                    $("#verSucur").html(data3);
            });
    
});
