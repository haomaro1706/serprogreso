<?php             

include_once 'app/modulos/configuracion/vista/v.Configuracion.php';

include_once 'app/modulos/configuracion/modelo/m.Configuracion.php';

include_once 'app/modulos/parametros/modelo/m.Parametros.php';



class Configuracion extends vConfiguracion{



    public function main(){

        validar_usuario();

        include_once 'app/modelo/m.Global.php';

        $variables=new globales();

        //$sitio=$variables->datos("sitio");

        $sitio=$variables->datos("servidor");

        $sitio2=unserialize($sitio[0]["param"]);       

        parent::servidor($sitio2);

    }

    

    public function sistema(){

        validar_usuario();

        include_once 'app/modelo/m.Global.php';

        $variables=new globales();

        $sitio=$variables->datos("sistema");

        $sitio2=unserialize($sitio[0]["param"]);       

        parent::sistema($sitio2);

    }

    

    public function servidor(){

        validar_usuario();

        include_once 'app/modelo/m.Global.php';

        $variables=new globales();

        $sitio=$variables->datos("servidor");

        $sitio2=unserialize($sitio[0]["param"]);

        parent::servidor($sitio2);

    }



        public function config_process($arryParam=null){

        validar_usuario();

        $serial=  serialize($arryParam);

        $config=new mConfiguracion();

        $val=$config->updConfig($arryParam["config"],$serial);

        

        if(!$val) setMensaje ("No se pudo guardar la configuración","error");

        else      setMensaje ("Configuración guardada correctamente","success");

        

        location(setUrl("empresa"));

        

    }

    

    public function empresa($array=null){

        validar_usuario();

        include_once 'app/modelo/m.Global.php';

        $variables=new globales();

        $sitio=$variables->datos("sistema");

        $sitio2=unserialize($sitio[0]["param"]);

        parent::empresa($sitio2);

    }

    

    

    public function correos(){

        $datos = mParametros::lstCorreos();

        //die(print_r($datos));

        parent::correos($datos);    

    }

	

	public function crearCorreo (){

		validar_usuario();

		$lista = mZonas::listar();

		$zonas = '';

	

		if(is_array($lista)){

			foreach($lista as $zona){

				$zonas .= '<option value="'.$zona["id"].'">'.$zona["zona"].'</option>';

			}

		}else{

			$zonas = '<option>Sin datos de zonas</option>';

		}

    	parent::crearCorreo($zonas);

	}

	

	public function guardarCorreo($array=null){

            validar_usuario();

            //die(print_r($array));

            $usuario = explode("-",$array["lstClientes"]);

            $usuario = trim($usuario[0]);

            

            $guardar = mParametros::saveCorreo(

                                            $array['sucursal'],

                                            $usuario,

                                            serialize($array['funcion'])

                                            );

            if($guardar){

                    setMensaje('Correo Electronico creado correctamente.','success');

            }else{

                    setMensaje('Error al crear el correo electronico','error');

            }

            location(setUrl("configuracion","correos"));

	}

	

	public function eliminarCorreo($array){

		validar_usuario();

		if(is_numeric($array["id"])){

			$del = mParametros::delCor($array["id"]);

			if($del){

				setMensaje("La informacion de correo electronico eliminada satisfactoriamente","success");

			}else{

				setMensaje("Error al eliminar la informacion","error");

			}

			location(setUrl("parametros","correos"));

		}

		

	}

	

	public function editar($array){

		$info=mParametros::selectbyId($array['id']);

		parent::editar($info=$info[0]);

	}// PARENT::EDITAR //

	

	public function updCorreo($array){

		validar_usuario();

                

                $usuario = explode('-',$array['lstClientes']);

                $usuario = trim($usuario[0]);

                

		$upd=mParametros::updCor(	

                        $array['id'],

                        $usuario,

                        $array['sucursal'],

                        serialize($array['funcion'])

                        );

		if($upd){

			setMensaje("La informacion de correo electronico ha sido  modificada satisfactoriamente","success");

			location(setUrl("configuracion","correos"));

		}else{

			setMensaje("Error interno al intentar modificar la informacion","error");

			location(setUrl("configuracion","correos"));

		}	

	}

    

} // class
