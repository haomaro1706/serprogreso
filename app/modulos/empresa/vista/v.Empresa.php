<?php 

class vEmpresa{
    public function main($info=null){
      //die(print_r($sitio));
        echo getMensaje();
        ?><h2><?php  echo EMPRESA_NOMBRE?></h2><?php                 
        navTabs(array(OP_BACK2=>setUrl("usuarios","panelAdmin"),
            		  EMPRESA_INFO=>'#',
            		  EMPRESA_REGIONAL=>setUrl("empresa","regional"),
            		  EMPRESA_SUCURSAL=>setUrl('empresa','sucursal')
            		 ),
            	    EMPRESA_INFO
            	   );
            
            navTabs(array(OP_BACK2=>setUrl("usuarios","panelAdmin"), 
            		  EMPRESA_INFO=>"#"
            		 ),
            	    EMPRESA_INFO, 
            	    "breadcrumb"
            	   );                
        ?>
        <div class="container">
            <div class="span8 offset1">
        <form class="form-horizontal" name="empresa" id="empresa" action="<?php  echo setUrl("configuracion","config_process")?>" method="post">                    
			<fieldset>
				<legend><?php echo EMPRESA_INFO?></legend>
                    <div class="control-group">
                        <label class="control-label" for="nombre">Nombre:</label>
                        <div class="controls">
                            <input type="text" id="nombre" name="nombre" placeholder="Nombre" value="<?php echo $info['nombre']?>">
                        </div>                        
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label" for="nit">Nit:</label>
                        <div class="controls">
                            <input type="text" id="nit" name="nit" placeholder="Nit" value="<?php echo $info['nit']?>">
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label" for="contacto">Contacto:</label>
                        <div class="controls">
                             <input type="text" id="contacto" name="contacto" placeholder="Contacto" value="<?php echo $info['contacto']?>">
                        </div>
                    </div>
            
                    <div class="control-group">
                        <label class="control-label" for="correo">Correo:</label>
                        <div class="controls">
                             <input type="text" id="correo" name="correo" placeholder="Correo" value="<?php echo $info['correo']?>">
                        </div>
                    </div>
                       <!--         
                    <div class="control-group">
                        <label class="control-label" for="pais">Pais:</label>
                        <div class="controls">
                            <select name="pais" id="pais">
                                <option value="">-Seleccione-</option>
                                <?php echo paises($info['pais'])?>
                            </select>
                        </div>
                    </div>
                                
                    <div class="control-group">
                        <label class="control-label" for="dep">Departamento:</label>
                        <div class="controls">
                             <select name="dep" id="dpe">
                                <option value="">-Seleccione-</option>
                            </select>
                        </div>
                    </div>
                                
                    <div class="control-group">
                        <label class="control-label" for="ciudad">Ciudad:</label>
                        <div class="controls">
                             <select name="ciudad" id="ciudad">
                                <option value="">-Seleccione-</option>
                            </select>
                        </div>
                    </div>-->
            </fieldset>
			
                    <div class="control-group">
                        <div class="controls">                   
                            <button type="submit" class="btn btn-primary">Guardar configuración</button>
                            <input type="hidden" name="config" value="sistema">
                        </div>
                    </div>

            </form>
            </div>
        </div>
        <?php       
    }//main
    
    public function regional($zonas){
      echo getMensaje();
        ?>
           <h2><?php  echo EMPRESA_REGIONAL?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('parametros'), 
            		  EMPRESA_INFO=>setUrl('empresa'),
            		  EMPRESA_REGIONAL=>'#',
            		  EMPRESA_SUCURSAL=>setUrl('empresa','sucursal')
            		 ),
            	    EMPRESA_REGIONAL
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('parametros'), 
            		  EMPRESA_REGIONAL=>'#'
            		 ),
            	    EMPRESA_REGIONAL, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php echo EMPRESA_REGIONAL?></h4>
       <div class='container'>
        <div class='span8 offset1'> 
			<fieldset>
				<legend><?php echo EMPRESA_REGIONAL?></legend>
			<?php
				Html::openForm("zonas",setUrl("empresa","saveZona"));
				Html::newInput("zona","Regional *");
				Html::newSelect("estado", "Estado *", array("1"=>"Activo", "0"=>"Inactivo"));
			?>
			</fieldset>
			<?php
				Html::newButton("enviar", "Enviar", "submit");
				Html::closeForm();
			?>
			<?php
				if(!empty($zonas)){
					echo datatable("tabla");
					Html::tabla(	array("Regional","Estado","Editar","Eliminar"),
									$zonas,
									array("zona","estado"),
									array("editar"=>setUrl("empresa","editZona"),"eliminar"=>setUrl("empresa","delZona"))
								);
				}
			?>
        </div>
       </div>
       <?php  
    }
    
    public function editZona($zonas){
      echo getMensaje();
        ?>
           <h2><?php  echo EMPRESA_EDITAR_REGIONAL?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('empresa','regional'), 
            		  EMPRESA_EDITAR_REGIONAL=>'#'
            		 ),
            	    EMPRESA_EDITAR_REGIONAL
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('empresa','regional'), 
            		  EMPRESA_EDITAR_REGIONAL=>'#'
            		 ),
            	    EMPRESA_EDITAR_REGIONAL, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php echo EMPRESA_EDITAR_REGIONAL?></h4>
       <div class='container'>
        <div class='span8 offset1'> 
			<fieldset>
				<legend><?php echo EMPRESA_EDITAR_REGIONAL?></legend>
			<?php
				Html::openForm("zonas",setUrl("empresa","editZonapro"));
				Html::newInput("zona","Zona *",$zonas['zona']);
				Html::newSelect("estado", "Estado *", array("1"=>"Activo", "0"=>"Inactivo"),$zonas['estado']);
			?>
			</fieldset>
			<?php
				Html::newHidden("id",$zonas['id']);
				Html::newButton("enviar", "Enviar", "submit");
				Html::closeForm();
			?>
			
        </div>
       </div>
       <?php  
    }//editZona
    
    public function sucursal($listaSucursales,$zonas){
		echo getMensaje();
		?>
		<h2><?php echo EMPRESA_SUCURSAL?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('parametros'),
                        EMPRESA_INFO=>setUrl("empresa"),
                        EMPRESA_REGIONAL=>setUrl("empresa","regional"),
                        EMPRESA_SUCURSAL=>'#',
            		 ),
            	    EMPRESA_SUCURSAL
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('parametros'),
            		  EMPRESA_SUCURSAL=>'#'
            		 ),
            	    EMPRESA_SUCURSAL, 
            	    'breadcrumb'
            	   );
            ?>
		<h4><?php echo EMPRESA_SUCURSAL?></h4>
		<div class="container">
        <div class="span7 offset2">            
        <fieldset >
				<legend><?php echo "Información de Sucursal"?></legend>
				<?php
			  Html::openForm("sucursales",setUrl("empresa","ingresarSucursal"));
			  Html::newInput("nombre","Nombre *");
			  Html::newInput("dir","Direccion *");
			  ?>
                        <div class="control-group">
                                <label class="control-label" for="zona">Regional *:</label>
                                <div class="controls">
                                        <select name="zona">
                                                <option value="">-Seleccione-</option>
                                                <?php  echo $zonas?>
                                        </select>
                                </div>
                        </div>
			  <?php
			  Html::newInput("tel","Teléfono Fijo *");
			  Html::newInput("cel","Teléfono Celular *");
			  Html::newInput("fax","Fax *");
                          echo paises();
			  ?>
                                <div id="ciudades"></div>
                            <div class="control-group" id="email">
                                <label class="control-label" for="prov">E-mail:</label>
                                <div class="controls">
                                    <input type="email" name="email" id="email" placeholder="E-mail" autocomplete="off"  >
                                </div>
                            </div>  
				
				<div class="control-group" id="observaciones">
					<label class="control-label" for="obser">Observaciones:</label>
					<div class="controls">
						<textarea name="obser" id="obser" placeholder="Observaciones" autocomplete="off"></textarea>
					</div>
				</div>
                                
                                
                    <div class="control-group">
                        <label class="control-label" for="pro">Productos:</label>
                        <div class="controls">
                            <?php
                            include_once 'app/modulos/productos/modelo/m.Productos.php';
                            $sucursales = mProductos::listar();
                            $sucs='';
                            if (is_array($sucursales)){
                                    foreach($sucursales as $key){
                                            $sucs .= '<input type="checkbox" name="producto[]" value="'.$key["id"].'"> '.$key["producto"].'<br>';
                                    }
                            }else{
                                    $sucs .= 'No hay productos creados';
                            }
                            echo $sucs;
                            ?>
                        </div>
                    </div>
                                
				</fieldset>		
			  <?php
			  Html::newButton("enviar", "Registrar Sucursal", "submit");
			  Html::closeForm();
			?>
            <?php echo datatable("listaSucursales")?>
		<table class="table table-striped table-hover table-condensed" id="listaSucursales">
    			<thead>
                            <tr> 
    				<th> # </th>
                                <th> Nombre </th>
                                <th> Zona </th>
                                <th> Dirección </th>
                                <th> Telefono </th>
                                <th> Ciudad </th>
                                <th> Email </th>
                                <th> Observaciones </th>
                                <th> Ver </th>
                                <th> Editar </th>
                                <th> Eliminar </th>
                            </tr>
                        </thead>
                        <tbody>
			<?php echo $listaSucursales; ?>
                        </tbody>
                </table>
            
		</div>
        </div>
		<?php
	}//sucursal
        
        
    public function vEditar($infoSucursal,$zonas){
		echo getMensaje();
		?>
		<h2><?php echo EMPRESA_SUCURSAL_EDITAR?></h2>
            <?php 
            navTabs(array(OP_BACK2=>setUrl("empresa","sucursal"),  
                        EMPRESA_SUCURSAL_EDITAR=>"#"
            		 ),
                    EMPRESA_SUCURSAL_EDITAR
            	   );
            
            navTabs(array(OP_BACK2=>setUrl("empresa","sucursal"),
            		EMPRESA_SUCURSAL_EDITAR=>"#"
            		),
            	    EMPRESA_SUCURSAL_EDITAR, 
            	    "breadcrumb"
            	   );
				   ?>
            <h4><?php echo EMPRESA_SUCURSAL_EDITAR?></h4>
		<div class="container">
        <div class="span7 offset1">            
        <form class="form-horizontal" enctype="multipart/form-data" name="crearSucursal2" id="crearSucursal2" action="<?php echo setUrl("empresa","editarSucursal")?>" method="post">
			<fieldset>
				<legend><?php echo EMPRESA_SUCURSAL_EDITAR?></legend>
				
				<?php
			 // Html::openForm("sucursales",setUrl("empresa","editarSucursal"));
			  Html::newInput("nombre","Nombre *",$infoSucursal["nombre_suc"]);
			  Html::newInput("dir","Direccion *",$infoSucursal["dir"]);
			  ?>
			  <div class="control-group">
													<label class="control-label" for="zona">Zona *:</label>
													<div class="controls">
														<select name="zona">
															<option value="">-Seleccione-</option>
															<?php  echo $zonas?>
														</select>
													</div>
												</div>
			  <?php
			  Html::newInput("tel","Teléfono Fijo *",$infoSucursal["tel_fijo"]);
			  Html::newInput("cel","Teléfono Celular *",$infoSucursal["tel_cel"]);
			  Html::newInput("fax","Fax *",$infoSucursal["fax"]);
			  Html::newInput("ciudad","Ciudad *",$infoSucursal["ciudad"]);
			  Html::newInput("pais","Pais *",$infoSucursal["pais"]);
			  ?>
			  <div class="control-group" id="email">
                    <label class="control-label" for="prov">E-mail:</label>
                    <div class="controls">
                        <input type="email" name="email" id="email" placeholder="E-mail" autocomplete="off"  value="<?php echo $infoSucursal["email"]?>">
                    </div>
               </div>  
				
				<div class="control-group" id="observaciones">
					<label class="control-label" for="obser">Observaciones:</label>
					<div class="controls">
						<textarea name="obser" id="obser" placeholder="Observaciones" autocomplete="off"><?php echo $infoSucursal["obser"]?></textarea>
				</div>
				</div>	
                <div class="control-group">
                        <label class="control-label" for="pro">Productos:</label>
                        <div class="controls">
                            <?php
                            include_once 'app/modulos/productos/modelo/m.Productos.php';
                            $prodcutos = unserialize($infoSucursal["productos"]);
                            //print_r($prodcutos);
                            $sucursales = mProductos::listar();
                            $sucs='';
                            if (is_array($sucursales)){
                                $i=0;
                                foreach($sucursales as $key){
                                    $check = ($key["id"]==$prodcutos[$i])?'checked':''; 
                                    $sucs .= '<input '.$check.' type="checkbox" name="producto[]" value="'.$key["id"].'"> '.$key["producto"].'<br>';
                                    $i++;
                                }
                            }else{
                                    $sucs .= 'No hay productos creados';
                            }
                            echo $sucs;
                            ?>
                        </div>
                    </div>
			  <?php
			  Html::newHidden("id",$infoSucursal['id']);
			  Html::newButton("enviar", "Modificar Sucursal", "submit");
			  Html::closeForm();
			?>
        </div>
        </div>		
		<?php
	}
        
        
        public function ver($infoSucursal,$zonas){
		echo getMensaje();
		?>
		<h2><?php echo EMPRESA_SUCURSAL_EDITAR?></h2>
            <?php 
            navTabs(array(OP_BACK2=>setUrl("empresa","sucursal"),  
                        EMPRESA_SUCURSAL_EDITAR=>"#"
            		 ),
                    EMPRESA_SUCURSAL_EDITAR
            	   );
            
            navTabs(array(OP_BACK2=>setUrl("empresa","sucursal"),
            		EMPRESA_SUCURSAL_EDITAR=>"#"
            		),
            	    EMPRESA_SUCURSAL_EDITAR, 
            	    "breadcrumb"
            	   );
				   ?>
            <h4><?php echo EMPRESA_SUCURSAL_EDITAR?></h4>
		<div class="container">
        <div class="span7 offset1">            
        <form class="form-horizontal" enctype="multipart/form-data" name="crearSucursal2" id="crearSucursal2" action="<?php echo setUrl("empresa","editarSucursal")?>" method="post">
			<fieldset>
				<legend><?php echo EMPRESA_SUCURSAL_EDITAR?></legend>
				
				<?php
			 // Html::openForm("sucursales",setUrl("empresa","editarSucursal"));
			  Html::newInput("nombre","Nombre *",$infoSucursal["nombre_suc"],'disabled');
			  Html::newInput("dir","Direccion *",$infoSucursal["dir"],'disabled');
			  ?>
			  <div class="control-group">
                                <label class="control-label" for="zona">Zona *:</label>
                                <div class="controls">
                                        <select name="zona" disabled>
                                                <option value="">-Seleccione-</option>
                                                <?php  echo $zonas?>
                                        </select>
                                </div>
                        </div>
			  <?php
			  Html::newInput("tel","Teléfono Fijo *",$infoSucursal["tel_fijo"],'disabled');
			  Html::newInput("cel","Teléfono Celular *",$infoSucursal["tel_cel"],'disabled');
			  Html::newInput("fax","Fax *",$infoSucursal["fax"],'disabled');
			  Html::newInput("ciudad","Ciudad *",$infoSucursal["ciudad"],'disabled');
			  Html::newInput("pais","Pais *",$infoSucursal["pais"],'disabled');
			  ?>
			  <div class="control-group" id="email">
                    <label class="control-label" for="prov">E-mail:</label>
                    <div class="controls">
                        <input disabled type="email" name="email" id="email" placeholder="E-mail" autocomplete="off"  value="<?php echo $infoSucursal["email"]?>">
                    </div>
               </div>  
				
				<div class="control-group" id="observaciones">
					<label class="control-label" for="obser">Observaciones:</label>
					<div class="controls">
						<textarea disabled name="obser" id="obser" placeholder="Observaciones" autocomplete="off"><?php echo $infoSucursal["obser"]?></textarea>
				</div>
				</div>	
                <div class="control-group">
                        <label class="control-label" for="pro">Productos:</label>
                        <div class="controls">
                            <?php
                            include_once 'app/modulos/productos/modelo/m.Productos.php';
                            $prodcutos = unserialize($infoSucursal["productos"]);
                            //print_r($prodcutos);
                            $sucursales = mProductos::listar();
                            $sucs='';
                            if (is_array($sucursales)){
                                $i=0;
                                foreach($sucursales as $key){
                                    $check = ($key["id"]==$prodcutos[$i])?'checked':''; 
                                    $sucs .= '<input disabled '.$check.' type="checkbox" name="producto[]" value="'.$key["id"].'"> '.$key["producto"].'<br>';
                                    $i++;
                                }
                            }else{
                                    $sucs .= 'No hay productos creados';
                            }
                            echo $sucs;
                            ?>
                        </div>
                    </div>
			  <?php
			  /*Html::newHidden("id",$infoSucursal['id']);
			  Html::newButton("enviar", "Modificar Sucursal", "submit");*/
			  Html::closeForm();
			?>
        </div>
        </div>		
		<?php
	}
        
    
} // class
