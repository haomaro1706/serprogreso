<?php 

class mParametros{

	public function lstCorreos(){
    	$orm = new ORM();
		$orm	->select(array("cor.id","cor.funcionalidad","suc.nombre_suc","usu.correo","usu.nombre"))
                        ->from("op_parametros_correos","cor")
                        ->join(array("usu"=>"op_usuarios"),'cor.usuario=usu.id',"INNER")
                        ->join(array("suc"=>"op_sucursales"),'cor.id_sucursal=suc.id',"INNER");
		$return=$orm->exec()->fetchArray(); 
	    return $return;
    } // listar Cliente	

    public function lstCorreos2($func){
    	$orm = new ORM();
		$orm	->select(array("cor.funcionalidad","usu.correo"))
                        ->from("op_parametros_correos","cor")
                        ->join(array("usu"=>"op_usuarios"),'cor.usuario=usu.id',"INNER")
                        ->where("cor.funcionalidad like ?",'%"'.$func.'";%')
                        ->where("cor.id_sucursal=?",getSucursal());
                //die($orm->getSql());
		$return=$orm->exec()->fetchArray(); 
	    return $return;
    } // listar Cliente	
	public function saveCorreo($sucur,$usuario,$func){
		$orm = new ORM();
		$orm	-> insert("op_parametros_correos")
			-> values(array(0,$sucur,$usuario,$func));
		$return = $orm->exec();
		return $return;
	}
	
	public function selectbyIdEmail($email,$id_zona){
    	$orm = new ORM();
		$orm	-> select()
						-> from("op_parametros_correos")
						-> where("email=?",$email)
						-> where("id_zona=?",$id_zona);
		$return = $orm->exec()->fetchArray();
    	return $return;
    } // LISTAR POR ID //	
	
	public function delCor($id){
		$orm = new ORM();
		$orm	-> delete()
						-> from("op_parametros_correos")
						-> where("id=?",$id);		
		$return = $orm	->exec();
		return $return;
	}
	
	public function selectbyId($id){
    	$orm = new ORM();
		$orm	->select(
                        array(
                            "cor.id as elid",
                            "cor.funcionalidad",
                            "suc.nombre_suc",
                            "usu.correo",
                            "usu.nombre",
                            "usu.id As id_usu",
                            "suc.id_zona"
                            )
                        )
                        ->from("op_parametros_correos","cor")
                        ->join(array("usu"=>"op_usuarios"),'cor.usuario=usu.id',"INNER")
                        ->join(array("suc"=>"op_sucursales"),'cor.id_sucursal=suc.id',"INNER")
                        ->where("cor.id=?",$id);
		$return=$orm->exec()->fetchArray(); 
	    return $return;
    } // LISTAR POR ID //	
	
	public function updCor($id,$nombre,$sucur,$funcion){
		$orm = new ORM();
		$orm	-> update("op_parametros_correos")
						-> set (
                                                        array(
                                                            "usuario"=>$nombre,
                                                            "id_sucursal"=>$sucur,
                                                            "funcionalidad"=>$funcion
                                                            )
                                                        )
						-> where("id=?",$id);
		$return = $orm->exec();
		return $return;				  
	}
	
	public function lstTasas(){
    	$orm = new ORM();
		$orm->select(array("par.*","pro.producto","li.nombre as nombreli"))
			->from("op_parametros_tasas","par")
			->join(array("pro"=>"op_productos"),'par.id_producto = pro.id',"INNER")
                        ->join(array("li"=>'op_productos_linea2'),'pro.linea=li.id');
               // die($orm->verSql());
		$return=$orm->exec()->fetchArray(); 
	    return $return;
    } // listar Cliente	
    
    public function lstTasas2($pro){
    	$orm = new ORM();
		$orm->select(array("par.*","pro.producto"))
				   ->from("op_parametros_tasas","par")
				   ->join(array("pro"=>"op_productos"),'par.id_producto = pro.id',"INNER");
                $orm->where("pro.id=?",$pro);
                //die($orm->verSql());
		$return=$orm->exec()->fetchArray(); 
	    return $return;
    } // listar Cliente	
    
    public function lstEmpre(){
    	$orm = new ORM();
        $orm->select(array("emp.*"))
            ->from("op_empresas","emp")
            ->join(array("conv"=>"op_convenio"),"emp.id=conv.id_empresa");
        $return=$orm->exec()->fetchArray(); 
	    return $return;
    } // listar Cliente	
	
	public function saveTasas(
                                $id_zona,
                                $id_producto,
                                $tasa_ea,
                                $tasa_nominal,
                                $tasa_mensual,
                                $tasa_diaria,
                                $tasa_mora,
                                $nombre,
                                $valor,
                                $descontable,
                                $iva
                                ){
		$orm = new ORM();
		$orm	-> insert("op_parametros_tasas")
						-> values(
                                                        array(
                                                            0,
                                                            $id_zona,
                                                            $id_producto,
                                                            $tasa_ea,
                                                            $tasa_nominal,
                                                            $tasa_mensual,
                                                            $tasa_diaria,
                                                            $tasa_mora,
                                                            $nombre,
                                                            $valor,
                                                            $descontable,
                                                            $iva
                                                        )
                                                        );
                //die($orm->verSql());
		$return = $orm->exec();
		return $return;
	}
	
	public function delTas($id){
		$orm = new ORM();
		$orm	-> delete()
						-> from("op_parametros_tasas")
						-> where("id=?",$id);		
		$return = $orm->exec();
		return $return;
	}
	
	public function selectbyIdTasa($id){
    	$orm = new ORM();
        
		$orm	-> select(array("(select producto from op_productos where id = tasas.id_producto) As producto","tasas.*"))
                        -> from("op_parametros_tasas","tasas")
                        //-> join(array("pro"=>"op_productos"),"pro.id=tasas.id_producto")
                        -> where("tasas.id=?",$id);
                //die('='.$orm->getSql());
		$return = $orm->exec()->fetchArray();
    	return $return;
    } // LISTAR POR ID //	
	
	public function updTas(
                                $id,
                                $tasa_ea,
                                $tasa_nominal,
                                $tasa_mensual,
                                $tasa_diaria,
                                $tasaMora,
                                $nombre,
                                $valor,
                                $descontable,
                                $iva,
                                $producto
                                ){
		$orm = new ORM();
		$orm	-> update("op_parametros_tasas")
                        -> set (
                                array(	
                                    "id_producto"       =>$producto,
                                    "tasa_ea"           =>$tasa_ea,
                                    "tasa_nominal"      =>$tasa_nominal,
                                    "tasa_mensual"      =>$tasa_mensual,
                                    "tasa_diaria"       =>$tasa_diaria,
                                    "tasaMora"          =>$tasaMora,
                                    "nombre"            =>$nombre,
                                    "valor"             =>$valor,
                                    "descontable"       =>$descontable,
                                    "iva"               =>$iva
                                    )
                                )
                        -> where("id=?",$id);
		$return = $orm->exec();
		return $return;				  
	}
	
	public function lstCobranzas(){
    	$orm = new ORM();
		$orm->select(array("cob.*","zon.zona"))
				   ->from("op_parametros_cobranza","cob")
				   ->join(array("zon"=>"op_zonas"),'cob.id_zona = zon.id');
		$return=$orm->exec()->fetchArray(); 
	    return $return;
    } // listar Cliente	
	
	
	public function selectbyIdZonaCob($id_zona){
		$orm = new ORM();
		$orm	-> select()
						-> from("op_parametros_cobranza")
						-> where("id_zona=?",$id_zona);
		$return = $orm->exec()->fetchArray();
    	return $return;
	}
	
	public function saveCobranzas($id_zona,$dia_ini,$dia_fin,$honorario,$cobro_juridico){
		$orm = new ORM();
		$orm	-> insert("op_parametros_cobranza")
						-> values(array(0,$id_zona,$dia_ini,$dia_fin,$honorario,$cobro_juridico));
		$return = $orm->exec();
		return $return;
	}
	
	public function delCob($id){
		$orm = new ORM();
		$orm	-> delete()
						-> from("op_parametros_cobranza")
						-> where("id=?",$id);		
		$return = $orm->exec();
		return $return;
	}
	
	public function selectbyIdCobranza($id){
    	$orm = new ORM();
		$orm	-> select()
						-> from("op_parametros_cobranza")
						-> where("id=?",$id);
		$return = $orm->exec()->fetchArray();
    	return $return;
    } // LISTAR POR ID //	
	
	public function updCob($id,$id_zona,$dia_ini,$dia_fin,$honorario,$cobro_juridico){
		$orm = new ORM();
		$orm	-> update("op_parametros_cobranza")
						-> set (array(	"id_zona"=>$id_zona,
										"dia_mora_ini"=>$dia_ini,
										"dia_mora_fin"=>$dia_fin,
										"honorario"=>$honorario,
										"cobro_juridico"=>$cobro_juridico))
						-> where("id=?",$id);
		$return = $orm->exec();
		return $return;				  
	}
	
	public function lstPlazos(){
    	$orm = new ORM();
		$orm->select(array("pla.*","pro.producto"))
				   ->from("op_parametros_plazos","pla")
				   ->join(array("pro"=>"op_productos"),'pla.id_producto = pro.id',"INNER");
		$return=$orm->exec()->fetchArray(); 
	    return $return;
    } // listar Plazos	
	
	public function savePlazo($id_producto,$plazo){
		$orm = new ORM();
		$orm	-> insert("op_parametros_plazos")
						-> values(array(0,$id_producto,$plazo));
		$return = $orm->exec();
		return $return;
	}
	
	public function selectbyIdProductoPlazo($id_producto,$plazo){
		$orm = new ORM();
		$orm	-> select()
						-> from("op_parametros_plazos")
						-> where("id_producto=?",$id_producto)
						-> where("plazo=?",$plazo);
		$return = $orm->exec()->fetchArray();
    	return $return;
	}
	
	public function delPla($id){
		$orm = new ORM();
		$orm	-> delete()
						-> from("op_parametros_plazos")
						-> where("id=?",$id);		
		$return = $orm->exec();
		return $return;
	}
	
	public function selectbyIdPlazo($id){
    	$orm = new ORM();
		$orm	-> select()
						-> from("op_parametros_plazos")
						-> where("id=?",$id);
		$return = $orm->exec()->fetchArray();
    	return $return;
    } // LISTAR POR ID //	
	
	public function updPla($id,$id_producto,$plazo){
		$orm = new ORM();
		$orm	-> update("op_parametros_plazos")
						-> set (array(	"id_producto"=>$id_producto,
										"plazo"=>$plazo))
						-> where("id=?",$id);
		$return = $orm->exec();
		return $return;				  
	}
	
	public function lstAdministrativos(){
		$orm = new ORM();
		$orm->select(array("admin.*","pro.producto","zon.zona"))
				   ->from("op_parametros_administrativos","admin")
				   ->join(array("pro"=>"op_productos"),'admin.id_producto = pro.id',"INNER")
				   ->join(array("zon"=>"op_zonas"),'admin.id_zona = zon.id',"INNER");
		$return=$orm->exec()->fetchArray(); 
	    return $return;
	}
	
	public function selectbyIdProductoZona($id_producto,$id_zona){
    	$orm = new ORM();
		$orm	-> select()
						-> from("op_parametros_administrativos")
						-> where("id_producto=?",$id_producto)
						-> where("id_zona=?",$id_zona);
		$return = $orm->exec()->fetchArray();
    	return $return;
    } // LISTAR POR ID //	
		
	public function saveAdministrativo($id_zona,$id_producto,$admin1,$tasaAdmin2,$recaudo){
		$orm = new ORM();
		$orm->insert("op_parametros_administrativos")
			->values(array(0,$id_zona,$id_producto,$admin1,$tasaAdmin2,$recaudo));
		$return=$orm->exec(); 
	    return $return;
	}
	
	public function delAdmin($id){
		$orm = new ORM();
		$orm	-> delete()
						-> from("op_parametros_administrativos")
						-> where("id=?",$id);		
		$return = $orm->exec();
		return $return;
	}
	
	public function selectbyIdAdministrativo($id){
    	$orm = new ORM();
		$orm	-> select(array("par.*","pro.producto as nProducto"))
				-> from("op_parametros_administrativos","par")
				->join(array("pro"=>"op_productos"),'par.id_producto = pro.id',"INNER")
				-> where("par.id=?",$id);
		$return = $orm->exec()->fetchArray();
    	return $return;
    } 
	
	public function updAdmin($id,$id_zona,$id_producto,$admin1,$tasaAdmin2,$recaudo){
		$orm = new ORM();
		$orm	-> update("op_parametros_administrativos")
						-> set (array(	"id_zona"=>$id_zona,
										"id_producto"=>$id_producto,
										"admin1"=>$admin1,
										"tasaAdmin2"=>$tasaAdmin2,
										"recaudo"=>$recaudo))
						-> where("id=?",$id);
		$return = $orm->exec();
		return $return;				  
	}
	
	public function saveServicio($empresa,$servico){
		$orm = new ORM();
		$orm	-> insert("op_parametros_servicios")
						-> values(array(0,$empresa,serialize($servico)));
		$return = $orm->exec();
		return $return;
	}
	
	public function lstServicios(){
		$orm = new ORM();
		$orm	-> select()->from("op_parametros_servicios");
		$return=$orm->exec()->fetchArray(); 
	    return $return;
	}
	
	public function updservicioId($id,$empresa,$servicio){
		$orm = new ORM();
		$orm	-> update("op_parametros_servicios")
						-> set (
							array(	"empresa"=>$empresa,
								"servicios"=>$servicio
							)
							)
						-> where("id=?",$id);
		$return = $orm->exec();
		return $return;				  
	}
	
	public function servicioId($id){
		$orm = new ORM();
		$orm	-> select()->from("op_parametros_servicios")->where("empresa=?",$id);
		$return = $orm->exec()->fetchArray();
		return $return;				  
	}
	
	public function delservicioId($id){
		$orm = new ORM();
		$orm	-> delete()
			-> from("op_parametros_servicios")
			-> where("id=?",$id);		
		$return = $orm->exec();
		return $return;
	}
        
        public function cargos(){
            $orm = new ORM();
            $orm	-> select()->from("op_parametros_cargos");
            $return = $orm->exec()->fetchArray();
            return $return;				  
	}
        
        public function saveCargo($cargo){
		$orm = new ORM();
		$orm	-> insert("op_parametros_cargos")
                        -> values(array(0,$cargo));
		$return = $orm->exec();
		return $return;
	}
        
        public function cargosId($id){
            $orm = new ORM();
            $orm	-> select()
                        ->from("op_parametros_cargos")
                        ->where('id','=',$id);
            $return = $orm->exec()->fetchArray();
            return $return;				  
	}
        
        public function updcargoId($id,$cargo){
		$orm = new ORM();
		$orm	-> update("op_parametros_cargos")
						-> set (
                                                    array(	
                                                        "cargo"=>$cargo
                                                    )
							)
						-> where("id=?",$id);
		$return = $orm->exec();
		return $return;				  
	}
        
        	public function delcargoId($id){
		$orm = new ORM();
		$orm	-> delete()
			-> from("op_parametros_cargos")
			-> where("id=?",$id);		
		$return = $orm->exec();
		return $return;
	}
        
         public function lstContratos(){
            $orm = new ORM();
            $orm	-> select()
                        ->from("op_parametros_contratos");
            $return = $orm->exec()->fetchArray();
            return $return;				  
	}
        
        public function saveContrato($contrato){
		$orm = new ORM();
		$orm	-> insert("op_parametros_contratos")
                        -> values(array(0,$contrato));
		$return = $orm->exec();
		return $return;
	}
        
        public function contratosId($id){
		$orm = new ORM();
		$orm	-> select()
                        ->from("op_parametros_contratos")
                        ->where('id','=',$id);
		$return = $orm->exec()->fetchArray();
		return $return;
	}
	
        public function updcontratoId($id,$cargo){
		$orm = new ORM();
		$orm	-> update("op_parametros_contratos")
						-> set (
                                                    array(	
                                                        "contrato"=>$cargo
                                                    )
							)
						-> where("id=?",$id);
		$return = $orm->exec();
		return $return;				  
	}
        
        public function delcontratoId($id){
		$orm = new ORM();
		$orm	-> delete()
			-> from("op_parametros_contratos")
			-> where("id=?",$id);		
		$return = $orm->exec();
		return $return;
	}
        
} // class