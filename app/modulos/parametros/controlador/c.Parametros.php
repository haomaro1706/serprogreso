<?php 
include_once 'app/modulos/parametros/vista/v.Parametros.php';
include_once 'app/modulos/parametros/modelo/m.Parametros.php';
include_once 'app/modulos/clientes/modelo/m.Clientes.php';
include_once 'app/modulos/productos/modelo/m.Productos.php';
include_once 'app/modulos/zonas/modelo/m.Zonas.php';
include_once 'app/modulos/parametros/modulos.php';

class Parametros extends vParametros{
	public function main(){
		validar_usuario();
        /*$datos=mParametros::lstTasas();
		$s=0;
		
		if(is_array($datos)){
			$cadena='<ul>';
			foreach($datos as $campo){
				$i=0;
				if(is_array($campo)){
					$nombres=unserialize($campo["nombre"]);
					$valores=unserialize($campo["valor"]);
					if(!empty($nombres) || !empty($valores))
					foreach($nombres as $nombre){
						$cadena.= "<li>{$nombre}:{$valores[$i]}</li>";
						$i++;
					}
				}
				$datos[$s]["nombre"]=$cadena.'</ul>';
				$cadena='<ul>';
				$s++;
			}
		}*/
            $lista=lstmodulos();
            $modulo=new mModulos();
            $modulos=$modulo->cargarModulos2($lista);
            
            $mUsuarios = new mUsuarios();
            $idUser = getGrupoUsuario();
            $arrayPerms = $mUsuarios->permisosActuales($idUser);
            // $arrayPerms = unserialize($permisosActuales["modulos"]);
			
			$cadena='';
            $cadena.='<div class="menuAdmin">';
            if (is_array($arrayPerms)){
	            foreach ($modulos As $valor){ 
					$checked = false;
					foreach($arrayPerms as $key => $val){
						if(in_array($valor["nombre"], $val)){
							$checked = true;
							break;
						}
						
					}
	            	if($valor["nombre"]!= "perfil" && !$checked) continue;
	
	                $cadena.='<div>';
	                $cadena.='<a title="'.$valor["titulo"].'" href="'.setUrl($valor["nombre"]).'">';
	                $cadena.='<img alt="'.$valor["titulo"].'" src="app/modulos/'.$valor["nombre"].'/img/'.$valor["img"].'"><br>
	                '.$valor["titulo"].'</a><br>
	                '.$valor["descripcion"].'</div>';
	            }
            }
            else{
            	setMensaje ("Usted no presenta m&oacute;dulos habilitados", "info");
            }
            $cadena.='</div>';
            $cadena_c= utf8_encode($cadena);
            //die($cadena_c);
        parent::main($cadena_c);   
    }
    
	public function crearTasa(){
		validar_usuario();
		$lista = mZonas::listar();
		$zonas = '';
	
		if(is_array($lista)){
			foreach($lista as $zona){
				$zonas .= '<option value="'.$zona["id"].'">'.$zona["zona"].'</option>';
			}
		}else{
			$zonas = '<option>Sin datos de zonas</option>';
		}
    	parent::vCrearTasas($zonas);
	}
	
    public function correos(){
		$datos = mParametros::lstCorreos();
        parent::vListarCorreos($datos);    
    }
	
	public function crearCorreo (){
		validar_usuario();
		$lista = mZonas::listar();
		$zonas = '';
	
		if(is_array($lista)){
			foreach($lista as $zona){
				$zonas .= '<option value="'.$zona["id"].'">'.$zona["zona"].'</option>';
			}
		}else{
			$zonas = '<option>Sin datos de zonas</option>';
		}
    	parent::vCrearCorreo($zonas);
	}
	
	public function guardarCorreo($array=null){
        validar_usuario();
		$info=mParametros::selectbyIdEmail($array['email'],$array['id_zona']);
		
		if(!empty($info)){
			setMensaje("Este E-mail ya existe como ".$info[0]['nombre']." ".$info[0]['apellido'].", intentelo de nuevo con nuevos datos o modifique el contacto" ,"error");
			location(setUrl("parametros","correos"));
		}else{
			$guardar = mParametros::saveCorreo(
										$array['id_zona'],
										$array['nombre'],
										$array['apellido'],
										$array['email'],
										$array['descripcion']);
			if($guardar){
				setMensaje('Correo Electronico creado correctamente.','success');
			}else{
				setMensaje('Error al crear el correo electronico','error');
			}
			location(setUrl("parametros","correos"));
		}
	}
	
	public function eliminarCorreo($array){
		validar_usuario();
		if(is_numeric($array["id"])){
			$del = mParametros::delCor($array["id"]);
			if($del){
				setMensaje("La informacion de correo electronico eliminada satisfactoriamente","success");
			}else{
				setMensaje("Error al eliminar la informacion","error");
			}
			location(setUrl("parametros","correos"));
		}
		
	}
	
	public function editar($array){
		$info=mParametros::selectbyId($array['id']);
		parent::vEditar($info[0]);
	}// PARENT::EDITAR //
	
	public function updCorreo($array){
		validar_usuario();
		$upd=mParametros::updCor(	$array['id'],
									$array['nombre'],
									$array['apellido'],
									$array['email'],
									$array['descripcion']);
		if($upd){
			setMensaje("La informacion de correo electronico ha sido  modificada satisfactoriamente","success");
			location(setUrl("parametros","correos"));
		}else{
			setMensaje("Error interno al intentar modificar la informacion","error");
			location(setUrl("parametros","correos"));
		}	
	}
	
	public function cobranzas(){
		validar_usuario();
        $info=mParametros::lstCobranzas();
		$s=0;
		if(is_array($info)){
			$cadena1='<ul>';
			$cadena2='<ul>';
			$cadena3='<ul>';
			foreach($info as $campo){
				$i=0;
				if(is_array($campo)){
					$dias_ini=unserialize($campo["dia_mora_ini"]);
					$dias_fin=unserialize($campo["dia_mora_fin"]);
					$honorarios=unserialize($campo["honorario"]);
					if(!empty($dias_ini) || !empty($dias_fin) || !empty($honorarios))
					foreach($dias_ini as $dias){
						if($campo["cobro_juridico"]==$i)
							$cobro="Si";
						else
							$cobro="No";
						$cadena1.= "<li>{$dias} - {$dias_fin[$i]} días     </li>";
						$cadena2.= "<li>{$honorarios[$i]}%</li>";
						$cadena3.= "<li>{$cobro}</li>";
						$i++;
					}
				}
				$info[$s]["dia_mora_ini"]=$cadena1.'</ul>';
				$info[$s]["honorario"]=$cadena2.'</ul>';
				$info[$s]["cobro_juridico"]=$cadena3.'</ul>';
				$cadena1='<ul>';
				$cadena2='<ul>';
				$cadena3='<ul>';
				$s++;
			}
		}
		
        parent::vListarCobranzas($info);   
    }
    
	public function crearCobranzas (){
		validar_usuario();
		$lista=mZonas::listar();
		$zonas = '';

		
		if(is_array($lista)){
			foreach($lista as $zona){
				$zonas .= '<option value="'.$zona["id"].'">'.$zona["zona"].'</option>';
			}
		}else{
			$zonas = '<option>Sin datos de zonas</option>';
		}
    	parent::vCrearCobranzas($zonas);
	}
	
	public function guardarCobranzas($array=null){
        validar_usuario();
		$info=mParametros::selectbyIdZonaCob($array['id_zona']);
		
		if(!empty($info)){
			setMensaje("La Zona ya posee datos de cobranza","error");
			location(setUrl("parametros","cobranzas"));
		}else{
			$save = mParametros::saveCobranzas(
									$array['id_zona'],
									serialize($array['dia_mora_ini']),
									serialize($array['dia_mora_fin']),
									serialize($array['honorario']),
									$array['cobro_juridico']);
			if($save){
				setMensaje('Cobranza creada correctamente.','success');
			}else{
				setMensaje('Error al crear la cobranza','error');
			}
			location(setUrl("parametros","cobranzas"));
		}
        
	}
	
	public function eliminarCobranzas($array){
		validar_usuario();
		if(is_numeric($array["id"])){
			$del = mParametros::delCob($array["id"]);
			if($del){
				setMensaje("La informacion de la cobranza eliminada satisfactoriamente","success");
			}else{
				setMensaje("Error al eliminar la informacion","error");
			}
			location(setUrl("parametros","cobranzas"));
		}
		
	}
	
	public function editarCobranzas($array){
		validar_usuario();
		$info=mParametros::selectbyIdCobranza($array['id']);
		
		if($info){
			$lista= mZonas::listar();
			$zonas = '';
			
			if(is_array($lista)){
				foreach($lista as $zona){
					$zonas .= '<option value="'.$zona["id"].'" '.(($zona["id"] == $info[0]["id_zona"])? 'selected="selected"':'').'>'.$zona["zona"].'</option>';
				}
			}else{
				$clientes = '<option>Sin datos de clientes</option>';
			}
			
		$cadena= '';
		$s=0;
		$data1=unserialize($info[0]["dia_mora_ini"]);
		$data2=unserialize($info[0]["dia_mora_fin"]);
		$data3=unserialize($info[0]["honorario"]);
		$data='';
		if(is_array($data1)){
			foreach($data1 as $dia){
				$cadena .= '<tr><td><input type="text" class="dia_mora_ini" name="dia_mora_ini[]" placeholder="Dia Inicio Mora" value='.$dia.'></td>';
				$cadena .= '<td><input type="text" class="dia_mora_fin" name="dia_mora_fin[]" placeholder="Dia Fin Mora" value='.$data2[$s].'></td>';
				$cadena .= '<td><input type="text" class="honorario" name="honorario[]" placeholder="Dia Fin Mora" value='.$data3[$s].'></td>';
				$cadena .= '<td><div class="control-group">
						<label class="control-label" for="cobro_juridico"></label>
							<input type="radio" class="cobro_juridico" name="cobro_juridico" value="'.$s.'" '.($info[0]["cobro_juridico"]==$s ?  'checked="checked"':"").'> 
							<span>Si</span>
						</div></td>';
				$cadena .= '<td><button type="button" onclick="clonar1(this)">Nuevo</button>';
				$cadena .=	($s==0)?  '':'<button type="button" class="borrar" onclick= "Borrar(this)">Eliminar</button></td></tr>';
				$s++;
			}
		}else{
			$cadena .= '<tr><td><input type="text" class="dia_mora_ini number" id="dia_mora_ini" name="dia_mora_ini[]" placeholder="Dia Inicio Mora"></td>';
			$cadena .= '<td><input type="text" class="dia_mora_fin number" id="dia_mora_fin" name="dia_mora_fin[]" placeholder="Dia Fin Mora" ></td>';
			$cadena .= '<td><input type="text" class="honorario number" id="honorario" name="honorario[]" placeholder="Dia Fin Mora" ></td>';
			$cadena .= '<td><div class="control-group">
						<label class="control-label" for="cobro_juridico"></label>
							<input type="radio" class="cobro_juridico" name="cobro_juridico" value="0">
							<span>Si</span>
					</div></td>';
			$cadena .= '<td><button type="button" onclick="clonar1(this)">Nuevo</button>';
		}
		
		$cadena = utf8_encode($cadena);
		parent::vEditarCobranza($info[0]["id"],$cadena,$zonas);
		}
		else{
			setMensaje ("No se ha podido encontrar la informaci&oacute;n de la cobranza", "error");
			location(setUrl("parametros","cobranzas"));
		}
	}// PARENT::EDITAR //
	
	public function updCobranzas($array){
		validar_usuario();
		$info=mParametros::selectbyIdZonaCob($array['id_zona']);
		
		if(!empty($info)){
			if($array['id']!=$info[0]['id']){
				setMensaje("La Zona ya posee datos de cobranza","error");
				location(setUrl("parametros","cobranzas"));
			}else{
				$upd=mParametros::updCob(	$array['id'],
										$array['id_zona'],
										serialize($array['dia_mora_ini']),
										serialize($array['dia_mora_fin']),
										serialize($array['honorario']),
										$array['cobro_juridico']);
				if($upd){
					setMensaje("La informacion de cobranza ha sido  modificada satisfactoriamente","success");
				}else{
					setMensaje("Error interno al intentar modificar la informacion","error");
				}			
				location(setUrl("parametros","cobranzas"));
			}
			
		}else{
			$upd=mParametros::updCob(	$array['id'],
										$array['id_zona'],
										serialize($array['dia_mora_ini']),
										serialize($array['dia_mora_fin']),
										serialize($array['honorario']),
										serialize($array['cobro_juridico']));
			if($upd){
				setMensaje("La informacion de cobranza ha sido  modificada satisfactoriamente","success");
			}else{
				setMensaje("Error interno al intentar modificar la informacion","error");
			}			
			location(setUrl("parametros","cobranzas"));
		}
	}
	
	
	public function plazos(){
		$datos = mParametros::lstPlazos();
        parent::vListarPlazos($datos);    
    }
	
	public function crearPlazo(){
		validar_usuario();
		$lista = mProductos::listar();
		$productos = '';
		
		if(is_array($lista)){
			foreach($lista as $producto){
				$productos .= '<option value="'.$producto["id"].'">'.$producto["producto"].'</option>';
			}
		}else{
			$productos = '<option>Sin datos de productos</option>';
		}
    	parent::vCrearPlazo($productos);
	}
	
	public function guardarPlazo($array=null){
        validar_usuario();
		$lookInfo = mParametros::selectbyIdProductoPlazo($array['id_producto'],$array['plazo']);
		
		if(!empty($lookInfo)){
				setMensaje('Este plazo en esta producto ya existe, inténtelo de nuevo con nuevos datos.','error');
				location(setUrl("parametros","plazos"));
	
		}else{
			$guardar = mParametros::savePlazo(
										$array['id_producto'],
										$array['plazo']);
			if($guardar){
				setMensaje('Plazo creado correctamente.','success');
			}else{
				setMensaje('Error al crear el plazo','error');
			}
			location(setUrl("parametros","plazos"));
		}
	}
	
	public function eliminarPlazo($array){
		validar_usuario();
		if(is_numeric($array["id"])){
			$del = mParametros::delPla($array["id"]);
			if($del){
				setMensaje("La informacion del plazo eliminada satisfactoriamente","success");
			}else{
				setMensaje("Error al eliminar la informacion","error");
			}
			location(setUrl("parametros","plazos"));
		}
		
	}
	
	public function editarPlazo($array){
		$info=mParametros::selectbyIdPlazo($array['id']);
		
		if($info){
			$lista = mProductos::listar();
			$productos = '';
			if(is_array($lista)){
				foreach($lista as $producto){
					$productos .= '<option value="'.$producto["id"].'" '.(($producto["id"] == $info[0]["id_producto"])? 'selected="selected"':'').'>'.$producto["producto"].'</option>';
				}
			}else{
				$productos = '<option>Sin datos de productos</option>';
			}
		}
		else{
			setMensaje ("No se ha podido encontrar la informaci&oacute;n de los plazos", "error");
			location(setUrl("parametros","plazos"));
		}
		parent::vEditarPlazo($info=$info[0],$productos);
	}// PARENT::EDITAR //
	
	public function updPlazo($array){
		validar_usuario();
		$lookInfo = mParametros::selectbyIdProductoPlazo($array['id_producto'],$array['plazo']);
		
		if(!empty($lookInfo)){
				setMensaje('Este plazo en esta producto ya existe, inténtelo de nuevo con nuevos datos.','error');
				location(setUrl("parametros","plazos"));
	
		}else{
			$upd=mParametros::updPla(	$array['id'],
										$array['id_producto'],
										$array['plazo']);
			if($upd){
				setMensaje("La informacion del plazo ha sido  modificada satisfactoriamente","success");
			}else{
				setMensaje("Error interno al intentar modificar la informacion","error");
			}			
			location(setUrl("parametros","plazos"));
		}
	}
	
	
	
	public function administrativos(){
		validar_usuario();
		$datos = mParametros::lstAdministrativos();
        parent::vListarAdministrativos($datos);    
	}
	
	public function crearAdministrativo(){
		validar_usuario();
		$listaZ=mZonas::listar();
		
		$zonas = '';
		
		if(is_array($listaZ)){
			foreach($listaZ as $zona){
				$zonas .= '<option value="'.$zona["id"].'">'.$zona["zona"].'</option>';
			}
		}else{
			$zonas = '<option>Sin datos de zonas</option>';
		}
		parent ::vCrearAdministrativo($zonas);
	}
	
	public function getProductoZona($array=null){
		validar_usuario();
		$listaP=mProductos::listarZona($array["id"]);
		$productos='<option value="">-Seleccione-</option>';
		if(!empty($listaP)){
			foreach($listaP as $producto){
					$productos .= "<option value='{$producto['id']}'>{$producto['producto']}</option>";
			}
		}
		echo $productos;
	}
	
	
	public function ingresarAdministrativo($array){
		validar_usuario();
		$lookInfo = mParametros::selectbyIdProductoZona($array['id_producto'],$array['id_zona']);
		
		if(!empty($lookInfo)){
			//if($look[0]['id_zona']==$array['id_zona'] && $look[0]['id_producto']==$array['id_producto']){
				setMensaje('Este producto en esta zona ya existe, inténtelo de nuevo con nuevos datos.','error');
				location(setUrl("parametros","administrativos"));
	
		}else{
			$guardar = mParametros::saveAdministrativo(
										$array['id_zona'],
										$array['id_producto'],
										$array['admin1'],
										$array['tasaAdmin2'],
										$array['recaudo']);
			if($guardar){
				setMensaje('Administrativo creado correctamente.','success');
			}else{
				setMensaje('Error al crear el administrativo','error');
			}
			location(setUrl("parametros","administrativos"));
		}
	}
	

	
	public function eliminarAdministrativo($array){
		validar_usuario();
		if(is_numeric($array["id"])){
			$del = mParametros::delAdmin($array["id"]);
			if($del){
				setMensaje("La informacion del administrativo eliminada satisfactoriamente","success");
			}else{
				setMensaje("Error al eliminar la informacion","error");
			}
			location(setUrl("parametros","administrativos"));
		}
		
	}
	
	public function editarAdministrativo($array){
		validar_usuario();
		$info=mParametros::selectbyIdAdministrativo($array['id']);
		
		if($info){
			$lista = mZonas::listar();
			$zonas = '';
			if(is_array($lista)){
				foreach($lista as $zona){
					$zonas .= '<option value="'.$zona["id"].'" '.(($zona["id"] == $info[0]["id_zona"])? 'selected="selected"':'').'>'.$zona["zona"].'</option>';
				}
			}else{
				$zonas = '<option>Sin datos de zona</option>';
			}
			
			$lista = mProductos::listarZona($info[0]["id_zona"]);
			$productos = '';
			if(is_array($lista)){
				foreach($lista as $producto){
					$productos .= '<option value="'.$producto["id"].'" '.(($producto["id"] == $info[0]["id_producto"])? 'selected="selected"':'').'>'.$producto["producto"].'</option>';
				}
			}else{
				$productos = '<option>Sin datos de zona</option>';
			}
		}
		else{
			setMensaje ("No se ha podido encontrar la informaci&oacute;n del administrativo", "error");
			location(setUrl("parametros","administrativos"));
		}
		parent::vEditarAdministrativo($info=$info[0],$zonas,$productos);
	}
	
	public function updAdministrativo($array){
		validar_usuario();
		$lookInfo2 = mParametros::selectbyIdProductoZona($array['id_producto'],$array['id_zona']);

		if(!empty($lookInfo2) ){
			if($lookInfo2[0]['id']!=$array['id']){
				setMensaje('Estos datos ya existen, intente con nuevos datos.','error');
			}else{
				$upd=mParametros::updAdmin(	$array['id'],
									$array['id_zona'],
									$array['id_producto'],
									$array['admin1'],
									$array['tasaAdmin2'],
									$array['recaudo']);
				if($upd){
					setMensaje("La informacion del administrativo ha sido  modificada satisfactoriamente","success");
				}else{
					setMensaje("Error interno al intentar modificar la informacion","error");
				}		
			}
			location(setUrl("parametros","administrativos"));
		}else{
		
			$upd=mParametros::updAdmin(	$array['id'],
										$array['id_zona'],
										$array['id_producto'],
										$array['admin1'],
										$array['tasaAdmin2'],
										$array['recaudo']);
			if($upd){
				setMensaje("La informacion del administrativo ha sido  modificada satisfactoriamente","success");
			}else{
				setMensaje("Error interno al intentar modificar la informacion","error");
			}			
			location(setUrl("parametros","administrativos"));
		}
	}
	
	public function servicios($array=null){
		validar_usuario();
		$info=mParametros::lstServicios();
		//die(print_r($info));
		parent::vListarServicios($info);
	}
	
	public function servicios_crear(){
		validar_usuario();
		parent::servicios_crear();
	}
	
	public function crearServicios($array=null){
		validar_usuario();
		//die(print_r($array));
		$save = mParametros::saveServicio(ucwords(strtolower($array['empresa'])),$array['servicios']);
		
		if($save){
			setMensaje('Servcio creado correctamente','success');
		}else{
			setMensaje('Error al crear un servicio ','error');
		}
		location(setUrl("parametros","servicios"));
	}
	
	public function editarServicios($array){
		validar_usuario();
		$info = mParametros::servicioId($array['id']);
		//die(print_r($info));
		parent::editarServicios($info[0]);
	}
	
	public function updServicios($array=null){
		validar_usuario();
		//die(print_r($info));
		$upd = mParametros::updservicioId($array['id'],$array['empresa'],serialize($array['servicios']));
		
		if($upd){
			setMensaje('Servcio actualizado correctamente','success');
		}else{
			setMensaje('Error al actualizar el servicio ','error');
		}
		location(setUrl("parametros","servicios"));
	}
	
	public function eliminarServicios($array){
		validar_usuario();
		
		$del = mParametros::delservicioId($array['id']);
		
		if($del){
			setMensaje('Servcio eliminado correctamente','success');
		}else{
			setMensaje('Error al eliminar el servicio ','error');
		}
		location(setUrl("parametros","servicios"));
	}
        
        public function cargos(){
		validar_usuario();
                $info=mParametros::cargos();
		parent::vcargos($info);
	}

        public function crearcargo(){
		validar_usuario();
		parent::vcrearcargo();
	}
        
        public function cargospro($array=null){
            validar_usuario();
            $save=mParametros::saveCargo($array['cargo']);
            
            if($save){
                setMensaje('Cargo creado correctamente','success');
            }else{
                setMensaje('Error al crear el cargo','error');
            }
            location(setUrl('usuarios','cargos'));
        }
        
        public function editarCargos($array){
		validar_usuario();
		$info = mParametros::cargosId($array['id']);
		//die(print_r($info));
		parent::editarCargos($info[0]);
	}
        
        public function editarcargopro($array=null){
            validar_usuario();
            $upd=mParametros::updcargoId($array['id'],$array['cargo']);
            
            if($upd){
                setMensaje('Cargo actualizado correctamente','success');
            }else{
                setMensaje('Error al actualizar el cargo','error');
            }
            location(setUrl('usuarios','cargos'));
        }
        
        public function eliminarCargos($array=null){
            validar_usuario();
            $del=mParametros::delcargoId($array['id']);
            
            if($del){
                setMensaje('Cargo eliminado correctamente','success');
            }else{
                setMensaje('Error al eliminar el cargo','error');
            }
            location(setUrl('usuarios','cargos'));
        }
        
        public function contratos(){
            validar_usuario();
            $info = mParametros::lstContratos();
            //die(print_r($info[0]));
            parent::contratos($info);
        }
        
        public function contratoscrear(){
            validar_usuario();
            parent::contratoscrear();
        }
        
        public function contratopro($array=null){
            validar_usuario();
            
            $save = mParametros::saveContrato($array['contrato']);
            if($save){
                setMensaje('Contrato creado correctamente','success');
            }else{
                setMensaje('Error al crear el contrato','error');
            }
            location(setUrl('parametros','contratos'));
        }
        
        public function editarContratos($array=null){
            validar_usuario();
            $info = mParametros::contratosId($array['id']);
            parent::editarContratos($info[0]);
        }
        
        public function editarcontratopro($array=null){
            validar_usuario();
            
            $upd= mParametros::updcontratoId($array['id'],$array['contrato']);
            if($upd){
                setMensaje('Contrato editado correctamente','success');
            }else{
                setMensaje('Error al editar el contrato','error');
            }
            location(setUrl('parametros','contratos'));
        }
        
        public function delcontrato($array=null){
            validar_usuario();
            
            $upd= mParametros::delcontratoId($array['id']);
            if($upd){
                setMensaje('Contrato eliminado correctamente','success');
            }else{
                setMensaje('Error al eliminar el contrato','error');
            }
            location(setUrl('parametros','contratos'));
        }
        
        public function infoTasa($array){
		$info=mParametros::selectbyIdTasa($array['id']);
		if(empty($info)){
                    $info = array();
                }
                header("Access-Control-Allow-Origin: *");
		echo json_encode($info);
	} // infoTasa
        
} // class