<?php 

class vParametros{
    
	public function vMain($datos){
      echo getMensaje();
        ?>
           <h2><?php  echo PARAMETROS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
					  TASAS_NOMBRE=>'#',
					  TASAS_CREAR=>setUrl('parametros','crearTasa'),
					  CONTACTOS_NOMBRE=>setUrl('parametros','correos'),					  
					  COBRANZAS_NOMBRE=>setUrl('parametros','cobranzas'),
					  PLAZOS_NOMBRE=>setUrl('parametros','plazos'),
					  ADMINISTRATIVOS_NOMBRE=>setUrl('parametros','administrativos'),
					  SERVICIOS=>setUrl('parametros','servicios'),
					  CARGOS=>setUrl('parametros','cargos'),
					  CONTRATOS=>setUrl('parametros','contratos')
            		 ),
            	    TASAS_NOMBRE
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  PARAMETROS_NOMBRE=>'#'
            		 ),
            	    PARAMETROS_NOMBRE, 
            	    'breadcrumb'
            	   );
            ?>
		<h4><?php  echo "Lista de Tasas Y Valores"?></h4>
		<div class='container'>
        <div class='span0 offset0'> 
		
        <?php
        if(!empty($datos)){
                echo datatable("tabla");
                Html::tabla(
                                array("Producto","Tasa EA","Tasa Nominal","Tasa Mensual","Tasa Diaria","Estudio Credito","Tasa de mora","Otros", "Editar","Eliminar"),
                                                $datos,
                                                array("producto","tasa_ea","tasa_nominal","tasa_mensual","tasa_diaria","estudio_credito","tasaMora","nombre"),
                                                array("editar"=>setUrl("parametros","editarTasas"),"eliminar"=>setUrl("parametros","eliminarTasas"))
                                        );
        }else{
          ?>
          <div class="alert alert-info">No hay Par&aacute;metros disponibles.</div>
          <?php
        }
	?>
        </div>
       </div>
       <?php  
    }

    public function main($modulos){
      echo getMensaje();
        ?>
           <h2><?php  echo PARAMETROS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('usuarios','panelAdmin')
            		 ),
            	    ''
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('usuarios','panelAdmin')
            		 ),
            	    '', 
            	    'breadcrumb'
            	   );
            ?>
            <h4><?php  echo "Par&aacute;metros del sistema ".SITIO?></h4>
            <div class='container'>
                <?php echo $modulos;?>
            </div>
       <?php  
    }
    
	public function vCrearTasas($zonas){
      echo getMensaje();
        ?>
           <h2><?php  echo PARAMETROS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  TASAS_NOMBRE=>setUrl('parametros'),
					  TASAS_CREAR=>'#',
					  CONTACTOS_NOMBRE=>setUrl('parametros','correos'),
					  COBRANZAS_NOMBRE=>setUrl('parametros','cobranzas'),
					  PLAZOS_NOMBRE=>setUrl('parametros','plazos'),
					  ADMINISTRATIVOS_NOMBRE=>setUrl('parametros','administrativos'),
					  SERVICIOS=>setUrl('parametros','servicios'),
					  CARGOS=>setUrl('parametros','cargos'),
					  CONTRATOS=>setUrl('parametros','contratos')
            		 ),
            	    TASAS_CREAR
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  TASAS_NOMBRE=>'#'
            		 ),
            	    TASAS_CREAR, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo TASAS_CREAR?></h4>
       <div class='container'>
        <div class='span6 offset1'> 
	  <fieldset>
			  <legend><?php echo "Informacion Tasas y Valores"?></legend>
			<?php
			  Html::openForm("tasas",setUrl("parametros","guardarTasas"));
			?>
			<div class="control-group">
					<label class="control-label" for="id_zona">Zona :</label>
					<div class="controls">
						<select name="id_zona" id="id_zona">
						<option value="">-Seleccione-</option>
						<?php  echo $zonas?>
					</select>
					</div>
				</div>
			
			<div class="control-group">
					<label class="control-label" for="id_producto">Producto :</label>
					<div class="controls">
						<select name="id_producto" id="id_producto">
					</select>
					</div>
				</div>
			<?php
			  Html::newInput("tasa_ea","Tasa EA %");
			 ?>
			  <div class="control-group">
				<label class="control-label" for="tasa_nominal"> Tasa Nominal Anual %: </label>
				<div class="controls">
					<input type="text" name="tasa_nominal" id="tasa_nominal" value="" readonly="readonly">
				</div>
			  </div>
			  
			  <div class="control-group">
				<label class="control-label" for="tasa_mensual"> Tasa Nominal Mensual %: </label>
				<div class="controls">
					<input type="text" name="tasa_mensual" id="tasa_mensual" value="" readonly="readonly">
				</div>
			  </div>
			  
			  <div class="control-group">
				<label class="control-label" for="tasa_diaria"> Tasa Nominal Diaria %: </label>
				<div class="controls">
					<input type="text" name="tasa_diaria" id="tasa_diaria" value="" readonly="readonly">
				</div>
			  </div>
			  
			  <?php
			  Html::newInput("estudio_credito","Estudio de Credito *");
			  Html::newInput("tasaMora","Tasa de mora %");
                          Html::newInput("seguroVida","Seguro de vida %");
		  ?>
	  </fieldset>
	  <fieldset>
	    <legend>Otros Intereses</legend>
	    <table class="table table-hover table-striped" name ="nombre" id="nombre" >
			<thead>
				<tr>
					<th>Nombre</th>
					<th>Valor</th>
					<th>Operación</th>
					
			</thead>
			<tbody>
				<tr>
				  <td><input type="text" class="nombre" id="nombre" name="nombre[]" placeholder="Nombre" form="tasas"></td>
				  <td><input type="text"  class="number" id="valorr" name="valor[]" placeholder="Valor" form="tasas"></td>
				  <td><button type="button" onclick="clonar(this)">Nuevo</button></td>
				</tr>
			</tbody>
	    </table>
	  </fieldset>
        <?php
		
		  Html::newButton("enviar", "Guardar", "submit");
		  Html::closeForm();
	  ?>
	   </div>
	   
       </div>
       <?php 
	}
	
	public function vEditarTasa($info=null,$cadena,$producto,$zona){
      echo getMensaje();
        ?>
           <h2><?php  echo PARAMETROS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  TASAS_NOMBRE=>setUrl('parametros'),
            		  TASAS_EDITAR=>'#',
					  TASAS_CREAR=>setUrl('parametros','crearTasa'),
					  CONTACTOS_NOMBRE=>setUrl('parametros','correos'),
					  COBRANZAS_NOMBRE=>setUrl('parametros','cobranzas'),
					  PLAZOS_NOMBRE=>setUrl('parametros','plazos'),
					  ADMINISTRATIVOS_NOMBRE=>setUrl('parametros','administrativos'),
            		 ),
            	    TASAS_EDITAR
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  TASAS_NOMBRE=>'#'
            		 ),
            	    TASAS_EDITAR, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo "Editar Tasas   producto: ".$producto."  zona: ".$zona;?></h4>
       <div class='container'>
        <div class='span6 offset1'> 
		
		
	  <fieldset>
			  <legend><?php echo "Informacion Tasas y Valores"?></legend>
			  
		  <?php
			  Html::openForm("tasas",setUrl("parametros","updTasas"));
			  Html::newInput("tasa_ea","Tasa EA %",$info['tasa_ea']);
			?>
			<div class="control-group">
				<label class="control-label" for="tasa_nominal"> Tasa Nominal Anual %: </label>
				<div class="controls">
					<input type="text" name="tasa_nominal" id="tasa_nominal" value="" readonly="readonly">
				</div>
			  </div>
			  
			  <div class="control-group">
				<label class="control-label" for="tasa_mensual"> Tasa Nominal Mensual %: </label>
				<div class="controls">
					<input type="text" name="tasa_mensual" id="tasa_mensual" value="" readonly="readonly">
				</div>
			  </div>
			  
			  <div class="control-group">
				<label class="control-label" for="tasa_diaria"> Tasa Nominal Diaria %: </label>
				<div class="controls">
					<input type="text" name="tasa_diaria" id="tasa_diaria" value="" readonly="readonly">
				</div>
			  </div>
			  <?php
			  Html::newInput("estudio_credito","Estudio de Credito ",$info['estudio_credito']);
			  Html::newInput("tasaMora","Tasa de mora %",$info['tasaMora']);
			  Html::newInput("seguroVida","Seguro de vida %",$info['seguroVida']);
		  ?>
	  </fieldset>
	  <fieldset>
	    <legend>Nombre y Valor</legend>
	    <table class="table table-hover table-striped" id="nombre" name="nombre">
			<thead>
				<tr>
					<th>#</th>
					<th>Nombre</th>
					<th>Valor</th>
					<th>Operacion</th>
				</tr>
			</thead>
			<tbody>
			
			<?php	echo $cadena;	?>
			</tbody>
	    </table>
	  </fieldset>
        <?php
			Html::newHidden("id",$info['id']);
			Html::newButton("enviar", "Guardar", "submit");
			Html::closeForm();
	  ?>
	   </div>
       </div>
       <?php 
	}
	
        public function vListarCorreos($datos){
      echo getMensaje();
        ?>
           <h2><?php  echo PARAMETROS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  TASAS_NOMBRE=>setUrl('parametros'),
					  CONTACTOS_NOMBRE=>'#',
            		  CONTACTOS_CREAR=>setUrl('parametros','crearCorreo'),
					  COBRANZAS_NOMBRE=>setUrl('parametros','cobranzas'),
					  PLAZOS_NOMBRE=>setUrl('parametros','plazos'),
					  ADMINISTRATIVOS_NOMBRE=>setUrl('parametros','administrativos'),
					  SERVICIOS=>setUrl('parametros','servicios'),
					  CARGOS=>setUrl('parametros','cargos'),
					  CONTRATOS=>setUrl('parametros','contratos')
            		 ),
            	    CONTACTOS_NOMBRE
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  PARAMETROS_NOMBRE=>'#'
            		 ),
            	    PARAMETROS_NOMBRE, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo "Lista de Correos Electronicos"?></h4>
       <div class='container'>
        <div class='span10 offset0'> 
		
	  <?php
		if(!empty($datos)){
			echo datatable("tabla");
			Html::tabla(
					array("Zona","Nombre","","E-mail","Descripcion","Editar","Eliminar"),
							$datos,
							array("zona","nombre","apellido","email","descripcion"),
							array("editar"=>setUrl("parametros","editar"),"eliminar"=>setUrl("parametros","eliminarCorreo"))
						);
		}else{
		  ?>
		  <div class="alert alert-info">No hay prodcutos disponibles.</div>
		  <?php
		}
	?>
        </div>
       </div>
       <?php  
    }
	
	public function vCrearCorreo($zonas){
      echo getMensaje();
        ?>
           <h2><?php  echo PARAMETROS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  TASAS_NOMBRE=>setUrl('parametros'),
					  CONTACTOS_NOMBRE=>setUrl('parametros','correos'),
            		  CONTACTOS_CREAR=>'#',
					  COBRANZAS_NOMBRE=>setUrl('parametros','cobranzas'),
					  PLAZOS_NOMBRE=>setUrl('parametros','plazos'),
					  ADMINISTRATIVOS_NOMBRE=>setUrl('parametros','administrativos'),
					  SERVICIOS=>setUrl('parametros','servicios'),
					  CARGOS=>setUrl('parametros','cargos'),
					  CONTRATOS=>setUrl('parametros','contratos')
            		 ),
            	    CONTACTOS_CREAR
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  CONTACTOS_NOMBRE=>'#'
            		 ),
            	    CONTACTOS_CREAR, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo CONTACTOS_CREAR?></h4>
       <div class='container'>
        <div class='span8 offset1'> 
	  <fieldset>
			  <legend><?php echo "Informacion Correo Electronico"?></legend>
		  <?php
			  Html::openForm("correos",setUrl("parametros","guardarCorreo"));
			  ?>
			  <div class="control-group">
					<label class="control-label" for="id_zona">Zona :</label>
					<div class="controls">
						<select name="id_zona" id="id_zona">
						<option value="">-Seleccione-</option>
						<?php  echo $zonas?>
					</select>
					</div>
				</div>
			<?php  
			  Html::newInput("nombre","Nombre *");
			  Html::newInput("apellido","Apellido *");
			  Html::newInput("email","E-mail *");
			  Html::newInput("descripcion","Descripcion *");
		  ?>
	  </fieldset>
        <?php
		  Html::newButton("enviar", "Guardar", "submit");
		  Html::closeForm();
	  ?>
	   </div>
       </div>
       <?php 
	}
	
	public function vEditar($info=null){
      echo getMensaje();
        ?>
           <h2><?php  echo PARAMETROS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('usuarios','panelAdmin'), 
            		  MODULO_configuracion_correos=>"#"
            		 ),
            	    MODULO_configuracion_correos
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('usuarios','panelAdmin'), 
            		  MODULO_configuracion_correos=>'#'
            		 ),
            	    MODULO_configuracion_correos, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo MODULO_configuracion_correos?></h4>
       <div class='container'>
        <div class='span8 offset1'> 
	  <fieldset>
			  <legend><?php echo "Informacion Correo Electronico"?></legend>
		  <?php
			  Html::openForm("correos",setUrl("parametros","updCorreo"));
			  Html::newReadonly("zona","Zona *","",$info['zona']);
			  Html::newInput("nombre","Nombre *",$info['nombre']);
			  Html::newInput("apellido","Apellido *",$info['apellido']);
			  Html::newInput("email","E-mail *",$info['email']);
			  Html::newInput("descripcion","Descripcion *",$info['descripcion']);
		  ?>
	  </fieldset>
        <?php
			Html::newHidden("id",$info['id']);
			Html::newButton("enviar", "Guardar", "submit");
			Html::closeForm();
	  ?>
	   </div>
       </div>
       <?php 
	}
	
	public function vListarCobranzas($datos){
      echo getMensaje();
        ?>
           <h2><?php  echo PARAMETROS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  TASAS_NOMBRE=>setUrl('parametros'), 
					  CONTACTOS_NOMBRE=>setUrl('parametros','correos'),
					  COBRANZAS_NOMBRE=>'#', 
					  COBRANZAS_CREAR=>setUrl('parametros','crearCobranzas'),
					  PLAZOS_NOMBRE=>setUrl('parametros','plazos'),
					  ADMINISTRATIVOS_NOMBRE=>setUrl('parametros','administrativos'),
					  SERVICIOS=>setUrl('parametros','servicios'),
					  CARGOS=>setUrl('parametros','cargos'),
					  CONTRATOS=>setUrl('parametros','contratos')
            		 ),
            	    COBRANZAS_NOMBRE
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  COBRANZAS_NOMBRE=>'#'
            		 ),
            	    PARAMETROS_NOMBRE, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo "Lista de Cobranzas"?></h4>
		<div class='container'>
        <div class='span10 offset0'> 
		
		
	  <?php
		if(!empty($datos)){
			echo datatable("tabla");
			Html::tabla(
							array("Zona","Día Inicio - Día Fin","Honorarios (%)","Cobro Juridico","Editar","Eliminar"),
							$datos,
							array("zona","dia_mora_ini","honorario","cobro_juridico"),
							array("editar"=>setUrl("parametros","editarCobranzas"),"eliminar"=>setUrl("parametros","eliminarCobranzas"))
						);
		}else{
		  ?>
		  <div class="alert alert-info">No hay productos disponibles.</div>
		  <?php
		}
	?>
        </div>
       </div>
       <?php  
    }
	
	public function vCrearCobranzas($zonas){
      echo getMensaje();
        ?>
           <h2><?php  echo PARAMETROS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  TASAS_NOMBRE=>setUrl('parametros'),
					  CONTACTOS_NOMBRE=>setUrl('parametros','correos'),
					  COBRANZAS_NOMBRE=>setUrl('parametros','cobranzas'),
					  COBRANZAS_CREAR=>'#',
					  PLAZOS_NOMBRE=>setUrl('parametros','plazos'),
					  ADMINISTRATIVOS_NOMBRE=>setUrl('parametros','administrativos'),
					  SERVICIOS=>setUrl('parametros','servicios'),
					  CARGOS=>setUrl('parametros','cargos'),
					  CONTRATOS=>setUrl('parametros','contratos')
            		 ),
            	    COBRANZAS_CREAR
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  COBRANZAS_NOMBRE=>'#'
            		 ),
            	    COBRANZAS_CREAR, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo COBRANZAS_CREAR?></h4>
       <div class='container'>
        <div class='span8 offset1'> 
	  <fieldset>
			  <legend><?php echo "Informacion Cobranza"?></legend>
		  <?php
			  Html::openForm("cobranzas",setUrl("parametros","guardarCobranzas"));
			  ?>
			  <div class="control-group">
                    <label class="control-label" for="id_cliente">Zona * :</label>
                    <div class="controls">
                        <select name="id_zona">
							<option value="">-Seleccione-</option>
							<?php  echo $zonas?>
						</select>
                    </div>
                </div>
			  <table class="table table-hover table-striped" name ="nombre1" id="nombre1" >
			<thead>
				</div>
				
				<tr>
					<th>Días Mora Inicio</th>
					<th>Días Mora Fin</th>
					<th>Honorario (%)</th>
					<th>Cobro Juridico (%)</th>
					<th>Operacion</th>
			</thead>
			<tbody>
				<tr>
					<td><input type="text" class="dia_mora_ini" id="dia_mora_ini" name="dia_mora_ini[]" placeholder="Día Inicio Mora"></td>
					<td><input type="text"  class="dia_mora_fin " id="dia_mora_fin" name="dia_mora_fin[]" placeholder="Dia Fin Mora"></td>
					<td><input type="text"  class="honorario" id="honorario" name="honorario[]" placeholder="Honorario %"></td>
					<td><div class="control-group">
						<label class="control-label" for="cobro_juridico"></label>
							<input type="radio" class="cobro_juridico" name="cobro_juridico" value="0">
							<span>Si</span>
					</div></td>
					<td><button type="button" onclick="clonar1(this)">Nuevo</button></td>
				</tr>
				
			</tbody>
	    </table>
	  </fieldset>
        <?php
		  Html::newButton("enviar", "Guardar", "submit");
		  Html::closeForm();
	  ?>
	   </div>
       </div>
       <?php 
	}
	
	public function vEditarCobranza($id,$cadena,$zonas=null){
      echo getMensaje();
        ?>
           <h2><?php  echo PARAMETROS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  TASAS_NOMBRE=>setUrl('parametros'),
					  CONTACTOS_NOMBRE=>setUrl('parametros','correos'),
            		  COBRANZAS_NOMBRE=>setUrl('parametros','cobranzas'),
            		  COBRANZAS_EDITAR=>'#',
					  COBRANZAS_CREAR=>setUrl('parametros','crearCobranzas'),
					  PLAZOS_NOMBRE=>setUrl('parametros','plazos'),
					  ADMINISTRATIVOS_NOMBRE=>setUrl('parametros','administrativos'),
            		 ),
            	    COBRANZAS_EDITAR
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  COBRANZAS_NOMBRE=>'#'
            		 ),
            	    COBRANZAS_EDITAR, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo COBRANZAS_EDITAR?></h4>
       <div class='container'>
        <div class='span8 offset1'> 
		
		 <fieldset>
			  <legend><?php echo "Informacion Cobranza"?></legend>
		  <?php
			  Html::openForm("cobranzas",setUrl("parametros","updCobranzas"));
			  ?>
			  <div class="control-group">
                    <label class="control-label" for="id_zona">Zona * :</label>
                    <div class="controls">
                        <select name="id_zona">
							<option value="">-Seleccione-</option>
							<?php  echo $zonas?>
						</select>
                    </div>
                </div>
			  <table class="table table-hover table-striped" name ="nombre1" id="nombre1" >
			<thead>
				<tr>
					<th>Días Mora Inicio</th>
					<th>Días Mora Fin</th>
					<th>Honorario (%)</th>
					<th>Cobro Juridico (%)</th>
					<th>Operacion</th>
			</thead>
			<tbody>
				<?php  echo $cadena;  ?>
			</tbody>
	    </table>
	  </fieldset>
        <?php
		  Html::newHidden("id",$id);
		  Html::newButton("enviar", "Guardar", "submit");
		  Html::closeForm();
	  ?>
	   </div>
       </div>
       <?php 
	}
	
	public function vListarPlazos($datos){
      echo getMensaje();
        ?>
           <h2><?php  echo PARAMETROS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  TASAS_NOMBRE=>setUrl('parametros'), 
					  CONTACTOS_NOMBRE=>setUrl('parametros','correos'),
					  COBRANZAS_NOMBRE=>setUrl('parametros','cobranzas'),
					  PLAZOS_NOMBRE=>'#', 
					  PLAZOS_CREAR=>setUrl('parametros','crearPlazo'),
					  ADMINISTRATIVOS_NOMBRE=>setUrl('parametros','administrativos'),
					  SERVICIOS=>setUrl('parametros','servicios'),
					  CARGOS=>setUrl('parametros','cargos'),
					  CONTRATOS=>setUrl('parametros','contratos')
            		 ),
            	    PLAZOS_NOMBRE
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  PLAZOS_NOMBRE=>'#'
            		 ),
            	    PARAMETROS_NOMBRE, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo "Lista de Plazos"?></h4>
		<div class='container'>
        <div class='span10 offset0'> 
		
		
	  <?php
		if(!empty($datos)){
			echo datatable("tabla");
			Html::tabla(
							array("Producto *","Plazo (días) ","Editar","Eliminar"),
							$datos,
							array("producto","plazo"),
							array("editar"=>setUrl("parametros","editarPlazo"),"eliminar"=>setUrl("parametros","eliminarPlazo"))
						);
		}else{
		  ?>
		  <div class="alert alert-info">No hay plazos disponibles.</div>
		  <?php
		}
	?>
        </div>
       </div>
       <?php  
    }
	
	public function vCrearPlazo($productos=null){
      echo getMensaje();
        ?>
           <h2><?php  echo PARAMETROS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  TASAS_NOMBRE=>setUrl('parametros'),
					  CONTACTOS_NOMBRE=>setUrl('parametros','correos'),
					  COBRANZAS_NOMBRE=>setUrl('parametros','cobranzas'),
					  PLAZOS_NOMBRE=>setUrl('parametros','plazos'),
					  PLAZOS_CREAR=>'#',
					  ADMINISTRATIVOS_NOMBRE=>setUrl('parametros','administrativos'),
					  SERVICIOS=>setUrl('parametros','servicios'),
					  CARGOS=>setUrl('parametros','cargos'),
					  CONTRATOS=>setUrl('parametros','contratos')
            		 ),
            	    PLAZOS_CREAR
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  PLAZOS_NOMBRE=>'#'
            		 ),
            	    PLAZOS_CREAR, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo PLAZOS_CREAR?></h4>
       <div class='container'>
        <div class='span8 offset1'> 
	  <fieldset>
			  <legend><?php echo "Informacion Plazo"?></legend>
		  <?php
			  Html::openForm("plazos",setUrl("parametros","guardarPlazo"));
			  ?>
				<div class="control-group">
					<label class="control-label" for="id_producto">Producto *:</label>
					<div class="controls">
						<select name="id_producto">
						<option value="">-Seleccione-</option>
						<?php  echo $productos?>
					</select>
					</div>
				</div>
				<?php
			  Html::newInput("plazo","Plazo (días)");
		  ?>
	  </fieldset>
        <?php
		  Html::newButton("enviar", "Guardar", "submit");
		  Html::closeForm();
	  ?>
	   </div>
       </div>
       <?php 
	}
	
	public function vEditarPlazo($info,$productos=null){
      echo getMensaje();
        ?>
           <h2><?php  echo PARAMETROS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  TASAS_NOMBRE=>setUrl('parametros'),
					  CONTACTOS_NOMBRE=>setUrl('parametros','correos'),
            		  COBRANZAS_NOMBRE=>setUrl('parametros','cobranzas'),
            		  PLAZOS_NOMBRE=>setUrl('parametros','plazos'),
            		  PLAZOS_EDITAR=>'#',
					  PLAZOS_CREAR=>setUrl('parametros','crearPlazo'),
					  ADMINISTRATIVOS_NOMBRE=>setUrl('parametros','administrativos'),
            		 ),
            	    PLAZOS_EDITAR
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  PLAZOS_NOMBRE=>'#'
            		 ),
            	    PLAZOS_EDITAR, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo PLAZOS_EDITAR?></h4>
       <div class='container'>
        <div class='span8 offset1'> 
	  <fieldset>
			  <legend><?php echo "Informacion Plazos"?></legend>
		  <?php
			  Html::openForm("plazos",setUrl("parametros","updPlazo"));
			  ?>
				<div class="control-group">
                    <label class="control-label" for="id_producto">Producto * :</label>
                    <div class="controls">
                        <select name="id_producto">
							<option value="">-Seleccione-</option>
							<?php  echo $productos?>
						</select>
                    </div>
                </div>
			  <?php
			  Html::newInput("plazo","Plazo (dias) ",$info['plazo']);
		  ?>
	  </fieldset>
        <?php
			Html::newHidden("id",$info['id']);
			Html::newButton("enviar", "Guardar", "submit");
			Html::closeForm();
	  ?>
	   </div>
       </div>
       <?php 
	}
	
	public function vListarAdministrativos($datos){
		echo getMensaje();
        ?>
           <h2><?php  echo PARAMETROS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
					  TASAS_NOMBRE=>setUrl('parametros'),	
					  CONTACTOS_NOMBRE=>setUrl('parametros','correos'),					  
					  COBRANZAS_NOMBRE=>setUrl('parametros','cobranzas'),
					  PLAZOS_NOMBRE=>setUrl('parametros','plazos'),
					  ADMINISTRATIVOS_NOMBRE=>'#',
					  ADMINISTRATIVOS_CREAR=>setUrl('parametros','crearAdministrativo'),
					  SERVICIOS=>setUrl('parametros','servicios'),
					  CARGOS=>setUrl('parametros','cargos'),
					  CONTRATOS=>setUrl('parametros','contratos')
            		 ),
            	    ADMINISTRATIVOS_NOMBRE
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  ADMINISTRATIVOS_NOMBRE=>'#'
            		 ),
            	    ADMINISTRATIVOS_NOMBRE, 
            	    'breadcrumb'
            	   );
            ?>
		<h4><?php  echo "Lista de Administrativos"?></h4>
		<div class='container'>
        <div class='span8 offset0'> 
		
		<?php
		if(!empty($datos)){
			echo datatable("tabla");
			Html::tabla(
					array("Zona","Producto","Administrativo 1","Tasa Administartivo 2 (%)","Recaudo","Editar","Eliminar"),
							$datos,
							array("zona","producto","admin1","tasaAdmin2","recaudo"),
							array("editar"=>setUrl("parametros","editarAdministrativo"),"eliminar"=>setUrl("parametros","eliminarAdministrativo"))
						);
		}else{
		  ?>
		  <div class="alert alert-info">No hay Administrativos disponibles.</div>
		  <?php
		}
	?>
        </div>
       </div>
       <?php 
	}

	public function vCrearAdministrativo($zonas=null){
      echo getMensaje();
        ?>
           <h2><?php  echo PARAMETROS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  TASAS_NOMBRE=>setUrl('parametros'),
					  CONTACTOS_NOMBRE=>setUrl('parametros','correos'),
					  COBRANZAS_NOMBRE=>setUrl('parametros','cobranzas'),
					  PLAZOS_NOMBRE=>setUrl('parametros','plazos'),
					  ADMINISTRATIVOS_NOMBRE=>setUrl('parametros','administrativos'),
					  ADMINISTRATIVOS_CREAR=>'#',
					  SERVICIOS=>setUrl('parametros','servicios'),
					  CARGOS=>setUrl('parametros','cargos'),
					  CONTRATOS=>setUrl('parametros','contratos')
            		 ),
            	    ADMINISTRATIVOS_CREAR
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  ADMINISTRATIVOS_NOMBRE=>'#'
            		 ),
            	    ADMINISTRATIVOS_CREAR, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo ADMINISTRATIVOS_CREAR?></h4>
       <div class='container'>
        <div class='span8 offset1'> 
	  <fieldset>
			  <legend><?php echo "Informacion Administrativo"?></legend>
		  <?php
			  Html::openForm("administrativo",setUrl("parametros","ingresarAdministrativo"));
			  ?>
				<div class="control-group">
					<label class="control-label" for="id_zona">Zonas *:</label>
					<div class="controls">
						<select name="id_zona" id="id_zona">
						<option value="">-Seleccione-</option>
						<?php  echo $zonas?>
					</select>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label" for="id_zona">Productos *:</label>
					<div class="controls">
						<select name="id_producto" id="id_producto">
							<option value="">-Seleccione-</option>
						</select>
					</div>
				</div>
				<?php
				Html::newInput("admin1","Costo administrativo");
				Html::newInput("tasaAdmin2","Tasa administrativo %");
				Html::newInput("recaudo","Recaudo");
		  ?>
	  </fieldset>
        <?php
		  Html::newButton("enviar", "Guardar", "submit");
		  Html::closeForm();
	  ?>
	   </div>
       </div>
       <?php 
	}
	
	public function vEditarAdministrativo($info,$zonas=null,$productos){
      echo getMensaje();
        ?>
           <h2><?php  echo PARAMETROS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  TASAS_NOMBRE=>setUrl('parametros'),
					  CONTACTOS_NOMBRE=>setUrl('parametros','correos'),
					  COBRANZAS_NOMBRE=>setUrl('parametros','cobranzas'),
					  PLAZOS_NOMBRE=>setUrl('parametros','plazos'),
					  PLAZOS_CREAR=>'#',
					  ADMINISTRATIVOS_NOMBRE=>setUrl('parametros','administrativos'),
					  ADMINISTRATIVOS_EDITAR=>'#',
					  ADMINISTRATIVOS_CREAR=>setUrl('parametros','crearAdministrativo'),
            		 ),
            	    ADMINISTRATIVOS_EDITAR
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  ADMINISTRATIVOS_NOMBRE=>'#'
            		 ),
            	    ADMINISTRATIVOS_EDITAR, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo ADMINISTRATIVOS_EDITAR?></h4>
       <div class='container'>
        <div class='span8 offset1'> 
	  <fieldset>
			  <legend><?php echo "Informacion Administrativo"?></legend>
		  <?php
			  Html::openForm("administrativo",setUrl("parametros","updAdministrativo"));
			  ?>
				<div class="control-group">
					<label class="control-label" for="id_zona">Zonas *:</label>
					<div class="controls">
						<select name="id_zona" id="id_zona">
							<option value="">-Seleccione-</option>
							<?php  echo $zonas?>
						</select>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label" for="id_zona">Productos *:</label>
					<div class="controls">
						<select name="id_producto" id="id_producto">
							<?php echo $productos ?>
						</select>
					</div>
				</div>
				<?php
				Html::newInput("admin1","Costo administrativo",$info['admin1']);
				Html::newInput("tasaAdmin2","Tasa administrativo %",$info['tasaAdmin2']);
				Html::newInput("recaudo","Recaudo",$info['recaudo']);
		  ?>
	  </fieldset>
        <?php
		  Html::newHidden("id",$info['id']);
		  Html::newButton("enviar", "Guardar", "submit");
		  Html::closeForm();
	  ?>
	   </div>
       </div>
       <?php 
	}
	
        public function vListarServicios($datos=''){
		echo getMensaje();
        ?>
           <h2><?php  echo PARAMETROS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
					  TASAS_NOMBRE=>setUrl('parametros'),	
					  CONTACTOS_NOMBRE=>setUrl('parametros','correos'),					  
					  COBRANZAS_NOMBRE=>setUrl('parametros','cobranzas'),
					  PLAZOS_NOMBRE=>setUrl('parametros','plazos'),
					  ADMINISTRATIVOS_NOMBRE=>setUrl('parametros','administrativos'),
					  SERVICIOS=>'#',
					  SERVICIOS_CREAR=>setUrl('parametros','servicios_crear'),
					  SERVICIOS=>setUrl('parametros','servicios'),
					  CARGOS=>setUrl('parametros','cargos'),
					  CONTRATOS=>setUrl('parametros','contratos')
            		 ),
            	    SERVICIOS
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  SERVICIOS=>'#'
            		 ),
            	    SERVICIOS, 
            	    'breadcrumb'
            	   );
            ?>
		<h4><?php  echo "Lista de Servicios"?></h4>
		<div class='container'>
        <div class='span8 offset0'> 
		
		<?php
		if(!empty($datos)){
			echo datatable("tabla");
			Html::tabla(
					array("Empresa","Editar","Eliminar"),
							$datos,
							array("empresa"),
							array("editar"=>setUrl("parametros","editarServicios"),"eliminar"=>setUrl("parametros","eliminarServicios"))
						);
		}else{
		  ?>
		  <div class="alert alert-info">No hay Servicios disponibles.</div>
		  <?php
		}
	?>
        </div>
       </div>
       <?php 
	}

        public function servicios_crear($datos=null){
		echo getMensaje();
        ?>
           <h2><?php  echo PARAMETROS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
					  TASAS_NOMBRE=>setUrl('parametros'),	
					  CONTACTOS_NOMBRE=>setUrl('parametros','correos'),					  
					  COBRANZAS_NOMBRE=>setUrl('parametros','cobranzas'),
					  PLAZOS_NOMBRE=>setUrl('parametros','plazos'),
					  ADMINISTRATIVOS_NOMBRE=>setUrl('parametros','administrativos'),
					  SERVICIOS=>setUrl('parametros','servicios'),
					  SERVICIOS_CREAR=>'#',
					  SERVICIOS=>setUrl('parametros','servicios'),
					  CARGOS=>setUrl('parametros','cargos'),
					  CONTRATOS=>setUrl('parametros','contratos')
            		 ),
            	    SERVICIOS_CREAR
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  SERVICIOS_CREAR=>'#'
            		 ),
            	    SERVICIOS_CREAR, 
            	    'breadcrumb'
            	   );
            ?>
		<h4><?php  echo SERVICIOS_CREAR?></h4>
		<div class='container'>
        <div class='span8 offset0'> 
		<fieldset>
			  <legend><?php echo "Informacion Servicio"?></legend>
		  <?php
			Html::openForm("servicios",setUrl("parametros","crearServicios"));

			Html::newInput("empresa","Nombre empresa: *");
			Html::newCheckbox2(array('agua'=>'Agua','energia'=>'Energia','gas'=>'Gas','telefono'=>'Telefono','internet'=>'Internet','tv'=>'Tv'),"servicios","Servicios: *");
		  ?>
	  </fieldset>
        <?php
		 // Html::newHidden("id",$info['id']);
		  Html::newButton("enviar", "Guardar", "submit");
		  Html::closeForm();
	  ?>
        </div>
       </div>
       <?php 
	}
	
	public function editarServicios($info){
		echo getMensaje();
        ?>
           <h2><?php  echo PARAMETROS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('parametros','panelAdmin'), 
					  SERVICIOS_EDITAR=>'#'
            		 ),
            	    SERVICIOS_EDITAR
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  SERVICIOS_EDITAR=>'#'
            		 ),
            	    SERVICIOS_EDITAR, 
            	    'breadcrumb'
            	   );
            ?>
		<h4><?php  echo SERVICIOS_EDITAR?></h4>
		<div class='container'>
        <div class='span8 offset0'> 
		<fieldset>
			  <legend><?php echo "Informacion Servicio"?></legend>
		  <?php
			Html::openForm("servicios",setUrl("parametros","updServicios"));

			Html::newInput("empresa","Nombre empresa: *",$info['empresa']);
			Html::newCheckbox2(
			array('agua'=>'Agua','energia'=>'Energia','gas'=>'Gas','telefono'=>'Telefono','internet'=>'Internet','tv'=>'Tv'),
			"servicios",
			"Servicios: *",
			unserialize($info['servicios'])
			);
		  ?>
	  </fieldset>
        <?php
		  Html::newHidden("id",$info['id']);
		  Html::newButton("enviar", "Editar", "submit");
		  Html::closeForm();
	  ?>
        </div>
       </div>
       <?php 
	}
        
        public function vcargos($datos=null){
          echo getMensaje();
            ?>
               <h2><?php  echo PARAMETROS_NOMBRE?></h2>

                <?php 
                navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
                                              TASAS_NOMBRE=>setUrl('parametros'),
                                              TASAS_CREAR=>setUrl('parametros','crearTasa'),
                                              CONTACTOS_NOMBRE=>setUrl('parametros','correos'),					  
                                              COBRANZAS_NOMBRE=>setUrl('parametros','cobranzas'),
                                              PLAZOS_NOMBRE=>setUrl('parametros','plazos'),
                                              ADMINISTRATIVOS_NOMBRE=>setUrl('parametros','administrativos'),
                                              SERVICIOS=>setUrl('parametros','servicios'),
                                              CARGOS=>'#',
                                              CARGOS_CREAR=>setUrl('parametros','crearcargo'),
                                                CONTRATOS=>setUrl('parametros','contratos')
                             ),
                        CARGOS
                       );

                navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
                              PARAMETROS_NOMBRE=>'#'
                             ),
                        PARAMETROS_NOMBRE, 
                        'breadcrumb'
                       );
                ?>
                    <h4><?php  echo "Lista de Cargos"?></h4>
                    <div class='container'>
            <div class='span8 offset1'> 

                    <?php
                    if(!empty($datos)){
                            echo datatable("tabla");
                            Html::tabla(
                                            array("Cargo","Editar","Eliminar"),
                                                            $datos,
                                                            array("cargo"),
                                                            array("editar"=>setUrl("parametros","editarCargos"),"eliminar"=>setUrl("parametros","eliminarCargos"))
                                                    );
                    }else{
                      ?>
                      <div class="alert alert-info">No hay Cargos creados.</div>
                      <?php
                    }
            ?>
            </div>
           </div>
           <?php  
        }

        public function vcrearcargo($datos=null){
          echo getMensaje();
            ?>
               <h2><?php  echo PARAMETROS_NOMBRE?></h2>

                <?php 
                navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
                                              TASAS_NOMBRE=>setUrl('parametros'),
                                              TASAS_CREAR=>setUrl('parametros','crearTasa'),
                                              CONTACTOS_NOMBRE=>setUrl('parametros','correos'),					  
                                              COBRANZAS_NOMBRE=>setUrl('parametros','cobranzas'),
                                              PLAZOS_NOMBRE=>setUrl('parametros','plazos'),
                                              ADMINISTRATIVOS_NOMBRE=>setUrl('parametros','administrativos'),
                                              SERVICIOS=>setUrl('parametros','servicios'),
                                              CARGOS=>setUrl('parametros','cargos'),
                                              CARGOS_CREAR=>'#',
                                                CONTRATOS=>setUrl('parametros','contratos')
                             ),
                        CARGOS_CREAR
                       );

                navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
                              CARGOS_CREAR=>'#'
                             ),
                        CARGOS_CREAR, 
                        'breadcrumb'
                       );
                ?>
                    <h4><?php  echo "Crear cargo"?></h4>
                    <div class='container'>
            <div class='span8 offset1'> 
                    <fieldset>
                            <legend><?php echo "Crear cargo"?></legend>
                      <?php
                            Html::openForm("cargos",setUrl("parametros","cargospro"));

                            Html::newInput("cargo","Cargo: *");
                      ?>
                    </fieldset>
                    <?php
                      Html::newButton("crear", "Crear", "submit");
                      Html::closeForm();
                    ?>
            </div>
           </div>
           <?php  
        }

        public function editarCargos($datos=null){
      echo getMensaje();
        ?>
           <h2><?php  echo PARAMETROS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('parametros','cargos'), 
					  CARGOS_EDITAR=>'#'
            		 ),
            	    CARGOS_EDITAR
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('parametros','cargos'), 
            		  CARGOS_EDITAR=>'#'
            		 ),
            	    CARGOS_EDITAR, 
            	    'breadcrumb'
            	   );
            ?>
		<h4><?php  echo "Editar cargo"?></h4>
		<div class='container'>
        <div class='span8 offset1'> 
		<fieldset>
                        <legend><?php echo "Editar cargo"?></legend>
		  <?php
			Html::openForm("cargos",setUrl("parametros","editarcargopro"));

			Html::newInput("cargo","Cargo: *",$datos['cargo']);
		  ?>
                </fieldset>
                <?php
		  Html::newHidden("id",$datos['id']);
		  Html::newButton("editar", "Editar", "submit");
		  Html::closeForm();
                ?>
        </div>
       </div>
       <?php  
    }

        public function contratos($datos=null){
          echo getMensaje();
            ?>
               <h2><?php  echo PARAMETROS_NOMBRE?></h2>

                <?php 
                navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
                                              TASAS_NOMBRE=>setUrl('parametros'),
                                              TASAS_CREAR=>setUrl('parametros','crearTasa'),
                                              CONTACTOS_NOMBRE=>setUrl('parametros','correos'),					  
                                              COBRANZAS_NOMBRE=>setUrl('parametros','cobranzas'),
                                              PLAZOS_NOMBRE=>setUrl('parametros','plazos'),
                                              ADMINISTRATIVOS_NOMBRE=>setUrl('parametros','administrativos'),
                                              SERVICIOS=>setUrl('parametros','servicios'),
                                              CARGOS=>setUrl('parametros','cargos'),
                                              CONTRATOS=>'#',
                                              CONTRATOS_CREAR=>setUrl('parametros','contratoscrear')
                             ),
                        CONTRATOS
                       );

                navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
                              CONTRATOS=>'#'
                             ),
                        CONTRATOS, 
                        'breadcrumb'
                       );
                ?>
                    <h4><?php  echo "Lista de Contratos"?></h4>
                    <div class='container'>
            <div class='span8 offset1'> 

                    <?php
                    if(!empty($datos)){
                            echo datatable("tabla");
                            Html::tabla(
                                        array("Contrato","Editar","Eliminar"),
                                        $datos,
                                        array("contrato"),
                                        array("editar"=>setUrl("parametros","editarContratos"),"eliminar"=>setUrl("parametros","delcontrato"))
                                        );
                    }else{
                      ?>
                      <div class="alert alert-info">No hay contratos creados.</div>
                      <?php
                    }
            ?>
            </div>
           </div>
           <?php  
        }
    
        public function contratoscrear($datos=null){
          echo getMensaje();
            ?>
               <h2><?php  echo PARAMETROS_NOMBRE?></h2>

                <?php 
                navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
                                              TASAS_NOMBRE=>setUrl('parametros'),
                                              TASAS_CREAR=>setUrl('parametros','crearTasa'),
                                              CONTACTOS_NOMBRE=>setUrl('parametros','correos'),					  
                                              COBRANZAS_NOMBRE=>setUrl('parametros','cobranzas'),
                                              PLAZOS_NOMBRE=>setUrl('parametros','plazos'),
                                              ADMINISTRATIVOS_NOMBRE=>setUrl('parametros','administrativos'),
                                              SERVICIOS=>setUrl('parametros','servicios'),
                                              CARGOS=>setUrl('parametros','cargos'),
                                              CONTRATOS=>setUrl('parametros','contratos'),
                                              CONTRATOS_CREAR=>'#'
                             ),
                        CONTRATOS_CREAR
                       );

                navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
                              CONTRATOS_CREAR=>'#'
                             ),
                        CONTRATOS_CREAR, 
                        'breadcrumb'
                       );
                ?>
                    <h4><?php  echo "Lista de Contratos"?></h4>
                    <div class='container'>
            <div class='span8 offset1'> 
                <fieldset>
                        <legend><?php echo "Crear contrato"?></legend>
                      <?php
                        Html::openForm("contratos",setUrl("parametros","contratopro"));

                        Html::newInput("contrato","Contrato: *");
                      ?>
                </fieldset>
                    <?php
                      Html::newButton("crear", "Crear", "submit");
                      Html::closeForm();
                    ?>
            </div>
           </div>
           <?php  
        }
        
        public function editarContratos($datos=null){
          echo getMensaje();
            ?>
               <h2><?php  echo PARAMETROS_NOMBRE?></h2>

                <?php 
                navTabs(array(OP_BACK2=>setUrl('parametros','contratos'), 
                                              CONTRATOS_CREAR=>'#'
                             ),
                        CONTRATOS_CREAR
                       );

                navTabs(array(OP_BACK2=>setUrl('parametros','contratos'), 
                              CONTRATOS_CREAR=>'#'
                             ),
                        CONTRATOS_CREAR, 
                        'breadcrumb'
                       );
                ?>
                    <h4><?php  echo "Lista de Contratos"?></h4>
                    <div class='container'>
            <div class='span8 offset1'> 
                <fieldset>
                        <legend><?php echo "Editar contrato"?></legend>
                      <?php
                        Html::openForm("contratos",setUrl("parametros","editarcontratopro"));

                        Html::newInput("contrato","Contrato: *",$datos['contrato']);
                      ?>
                </fieldset>
                    <?php
                      Html::newHidden("id",$datos['id']);
                      Html::newButton("actualizar", "Actualizar", "submit");
                      Html::closeForm();
                    ?>
            </div>
           </div>
           <?php  
        }
        
} // class
