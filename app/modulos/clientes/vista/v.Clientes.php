<?php 
class vClientes{
    
    public function main(){
      echo getMensaje();
        ?>
           <h2><?php  echo CLIENTES_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  CLIENTES_CREAR=>'#',
            		  CLIENTES_CREAR_EMPRESA=>setUrl('clientes','empresa'),
            		  CLIENTES_LISTAR=>setUrl('clientes','lst'),
                          CLIENTES_LISTAR_EMP=>setUrl('clientes','lstEmpresa'),
            		  CLIENTES_REFERENCIA=>setUrl('clientes','convenio'),
            		  CLIENTES_CONVENIOS=>setUrl('clientes','lstConvenios')
            		 ),
            	    CLIENTES_CREAR
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  CLIENTES_CREAR=>'#'
            		 ),
            	    CLIENTES_CREAR, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo CLIENTES_CREAR?></h4>
       <div class='container'>
        <div class='span10 offset1'> 
            <?php Html::openForm("clientes",setUrl("clientes","saveCliente")); ?>
            
            <!--<fieldset>
                <legend>Informaci&oacute;n interna</legend>
                
                <?php
                     Html::newSelect("tipo_cli", "Tipo Cliente *", array(
                                                                        "1"=>"Natural",
                                                                        "2"=>"Juridica"
                                                                        )
                                        );
                ?>
                
            </fieldset>-->
            
            <fieldset>
                <legend>Informaci&oacute;n personal</legend>
                        <div id="tipo_doc2">
                          <?php
			  Html::newSelect("tipo_doc", "Tipo documento *", array(
                                                                                "cc"=>"Cedula",
                                                                                "ce"=>"Cedula de extranjeria",
                                                                                "pp"=>"Pasaporte",
                                                                                "nit"=>"Nit"
                                                                                )
                                        );
                          ?>
                        </div>
                        <?php
			  Html::newInput("ndocumento","Numero Identificacion *");
			  Html::newInput("nombre","Nombre *");
                          ?>
                        <div id="ver_apellido">
                        <?php
			  Html::newInput("apellido","Apellido *");
                        ?>
                        </div>
                        <?php
                        echo '<div id="fechaNaci2">';
                          echo inputDateMenores('fechaNaci');
                          Html::newInput("fechaNaci","Fecha nacimiento *","","readonly");
                          echo '</div>';
			  Html::newInput("correo","Correo *");
                          echo departamentos();
                          echo barrios();
			  Html::newInput("telefono","T&eacute;lefono *");
			  Html::newInput("celular","Celular *");
                          Html::newInput("direccion","Dirección *");
                          /*Html::newInput("cupo","Cupo disponible *");
                          echo selectZonas();*/
                          Html::newInput("dir2","Direcci&oacute;n correspondencia *");
		  ?>
            </fieldset>
            <fieldset>
                <legend id="ipersonal">Informaci&oacute;n Laboral</legend>
                <?php
                Html::newInput("empresa","Nombre empresa *");
                Html::newInput("cargo","Cargo *");
                Html::newInput("dirempresa","Direccion empresa *");
                Html::newInput("telempresa","Telefono empresa *");
                Html::newInput("actividadEconomica","Actividad economica *");
                Html::newInput("ciiu","Codigo CIIU*");
                ?>
            </fieldset>
            <fieldset>
                <legend>Informaci&oacute;n Financiera</legend>
                <?php
                Html::newInput("ingresoMensual2","Ingreso mensual *");
                Html::newHidden("ingresoMensual");
                Html::newInput("otroMensual2","Otros ingresos *");
                Html::newHidden("otroMensual");
                Html::newInput("otroDesc","Descripci&oacute;n ingresos *");
                Html::newInput("totalIngresos2","Total ingresos *");
                Html::newHidden("totalIngresos");
                Html::newInput("totalEgresos2","Total egresos *");
                Html::newHidden("totalEgresos");
                Html::newInput("totalActivos2","Total activos *");
                Html::newHidden("totalActivos");
                Html::newInput("totalPasivos2","Total pasivos *");
                Html::newHidden("totalPasivos");
                Html::newSelect('bienesFiducia','Relaciona bienes de fiducia',array('1'=>'Si','2'=>'No'));
                Html::newInput("descFiducia","Descripci&oacute;n fiducia *");
                Html::newInput("cuentauno","Numero cuenta 1 *");
                echo cuentas();
                echo bancos(null,null,'banco1');
                Html::newInput("cuentados","Numero cuenta 2 *");
                echo cuentas(null,null,'cuenta2');
                echo bancos(null,null,'banco2');
                ?>
            </fieldset>
            <fieldset>
                <legend>Adjuntos</legend>
                    <div class="control-group" id="adj">
                        <label for="adjuntos1" class="control-label">Adjuntos : </label>
                        <div class="controls" id="nuevo">
                            <input type="file" name="adjuntos1[]" id="adjuntos1">
                            <input type="text" name="nadjunto[]" id="nadjunto" placeholder="Nombre archivo">
                            <a class="btn clonar" onclick="clonar(this)">+</a>
                            <a class="btn remover" onclick="remover(this)">-</a>
                        </div>
                    </div>
                    <div id="cont" style="display: none">0</div>
                </fieldset>
        <?php
		  Html::newButton("enviar", "Enviar", "submit");
		  Html::closeForm();
	  ?>
        </div>
       </div>
       <?php  
    }
    
    public function editCliente($datos=null){
      echo getMensaje();
        ?>
           <h2><?php  echo CLIENTES_EDITAR?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('clientes'), 
            		  CLIENTES_EDITAR=>'#'
            		 ),
            	    CLIENTES_EDITAR
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('clientes'), 
            		  CLIENTES_EDITAR=>'#'
            		 ),
            	    CLIENTES_EDITAR, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo CLIENTES_EDITAR?></h4>
       <div class='container'>
        <div class='span10 offset1'>
            <?php Html::openForm("clientes",setUrl("clientes","updCliente")); ?>
	  <fieldset style="display: none">
              <legend>Informaci&oacute;n interna</legend>
                
                <?php
                     Html::newSelect("tipo_cli", "Tipo Cliente *", array(
                                                                        "1"=>"Natural",
                                                                        "2"=>"Juridica"
                                                                        ));
                ?>
                
            </fieldset>
            
            <fieldset>
                <legend>Informaci&oacute;n personal</legend>
                        <div id="tipo_doc2">
                          <?php
			  Html::newSelect("tipo_doc", "Tipo documento *", array(
                                                                                "cc"=>"Cedula",
                                                                                "ce"=>"Cedula de extranjeria",
                                                                                "pp"=>"Pasaporte",
                                                                                "nit"=>"Nit"
                                                                                ),$datos['tipo_doc']
                                        );
                          ?>
                        </div>
                        <?php
			  Html::newInput("ndocumento","Numero Identificacion *",$datos['id_cliente'],'readonly');
			  Html::newInput("nombre","Nombre *",$datos['nombre']);
                          ?>
                        <div id="ver_apellido">
                        <?php
			  Html::newInput("apellido","Apellido *",$datos['apellido']);
                        ?>
                        </div>
                        <?php
                        echo '<div id="fechaNaci2">';
                          echo inputDateMenores('fechaNaci');
                          Html::newInput("fechaNaci","Fecha nacimiento *",$datos['fechaNaci'],"readonly");
                          echo '</div>';
			  Html::newInput("correo","Correo *",$datos['correo']);
                          echo departamentos('COL',$datos['ciudad']);
                          echo barrios($datos['ciudad'],$datos['barrio']);
			  Html::newInput("telefono","T&eacute;lefono *",$datos['telefono']);
			  Html::newInput("celular","Celular *",$datos['celular']);
                          Html::newInput("direccion","Dirección *",$datos['dir']);
                          /*Html::newInput("cupo","Cupo disponible *");
                          echo selectZonas();*/
                          Html::newInput("dir2","Direcci&oacute;n correspondencia *",$datos['dir2']);
		  ?>
            </fieldset>
            <fieldset>
                <legend id="ipersonal">Informaci&oacute;n Laboral</legend>
                <?php
                Html::newInput("empresa","Nombre empresa *",$datos['empresa']);
                Html::newInput("cargo","Cargo *",$datos['cargo']);
                Html::newInput("dirempresa","Direccion empresa *",$datos['dirEmpresa']);
                Html::newInput("telempresa","Telefono empresa *",$datos['telEmpresa']);
                Html::newInput("actividadEconomica","Actividad economica *",$datos['actividad']);
                Html::newInput("ciiu","Codigo CIIU*",$datos['ciiu']);
                ?>
            </fieldset>
            <fieldset>
                <legend>Informaci&oacute;n Financiera</legend>
                <?php
                Html::newInput("ingresoMensual","Ingreso mensual *",$datos['ingresos']);
                Html::newInput("otroMensual","Otros ingresos *",$datos['otrosIn']);
                Html::newInput("otroDesc","Descripci&oacute;n ingresos *",$datos['ingresosDesc']);
                Html::newInput("totalIngresos","Total ingresos *",$datos['totalingresos']);
                Html::newInput("totalEgresos","Total egresos *",$datos['totalegresos']);
                Html::newInput("totalActivos","Total activos *",$datos['totalactivos']);
                Html::newInput("totalPasivos","Total pasivos *",$datos['totalpasivos']);
                Html::newSelect('bienesFiducia','Relaciona bienes de fiducia',array('1'=>'Si','2'=>'No'),$datos['fiducia']);
                Html::newInput("descFiducia","Descripci&oacute;n fiducia *",$datos['descFiducia']);
                Html::newInput("cuentauno","Numero cuenta 1 *",$datos['cuenta1']);
                echo cuentas($datos['tipo1']);
                echo bancos($datos['numero1'],null,'banco1');
                Html::newInput("cuentados","Numero cuenta 2 *",$datos['cuenta2']);
                echo cuentas($datos['tipo2'],null,'cuenta2');
                echo bancos($datos['numero2'],null,'banco2');
                ?>
            </fieldset>
            <fieldset>
                <legend>Adjuntos</legend>
                    <div class="control-group" id="adj">
                        <label for="adjuntos1" class="control-label">Adjuntos : </label>
                        <div class="controls" id="nuevo">
                            <input type="file" name="adjuntos1[]" id="adjuntos1">
                            <input type="text" name="nadjunto[]" id="nadjunto" placeholder="Nombre archivo">
                            <a class="btn clonar" onclick="clonar(this)">+</a>
                            <a class="btn remover" onclick="remover(this)">-</a>
                        </div>
                    </div>
                    <div id="cont" style="display: none">0</div>
                </fieldset>
        <?php
		  Html::newHidden("id",$datos['id_cliente']);
		  Html::newButton("editar", "Editar", "submit");
		  Html::closeForm();
	  ?>
        </div>
       </div>
       <?php  
    }


    public function verCliente($datos=null,$ficheros=null){
      echo getMensaje();
        ?>
           <h2><?php  echo CLIENTES_VER?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('clientes','lst'), 
            		  CLIENTES_VER=>'#'
            		 ),
            	    CLIENTES_VER
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('clientes','lst'), 
            		  CLIENTES_VER=>'#'
            		 ),
            	    CLIENTES_VER, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo CLIENTES_VER?></h4>
       <div class='container'>
        <div class='span10 offset1'>
            <?php Html::openForm("clientes",setUrl("clientes","updCliente")); ?>
            <fieldset>
                <legend>Informaci&oacute;n personal</legend>
                        <div id="tipo_doc2">
                          <?php
			  Html::newSelect("tipo_doc", "Tipo documento *", array(
                                                                                "cc"=>"Cedula",
                                                                                "ce"=>"Cedula de extranjeria",
                                                                                "pp"=>"Pasaporte",
                                                                                "nit"=>"Nit"
                                                                                ),
                                  $datos['tipo_doc'],
                                  'disabled'
                                        );
                          ?>
                        </div>
                        <?php
			  Html::newInput("ndocumento","Numero Identificacion *",$datos['id_cliente'],'readonly');
			  Html::newInput("nombre","Nombre *",$datos['nombre'],'disabled');
                          ?>
                        <div id="ver_apellido">
                        <?php
			  Html::newInput("apellido","Apellido *",$datos['apellido'],'disabled');
                        ?>
                        </div>
                        <?php
                        echo '<div id="fechaNaci2">';
                          echo inputDateMenores('fechaNaci');
                          Html::newInput("fechaNaci","Fecha nacimiento *",$datos['fechaNaci'],'disabled');
                          echo '</div>';
			  Html::newInput("correo","Correo *",$datos['correo'],'disabled');
                          echo departamentos('COL',$datos['ciudad'],'disabled');
                          echo barrios($datos['ciudad'],$datos['barrio'],'disabled');
			  Html::newInput("telefono","T&eacute;lefono *",$datos['telefono'],'disabled');
			  Html::newInput("celular","Celular *",$datos['celular'],'disabled');
                          Html::newInput("direccion","Dirección *",$datos['dir'],'disabled');
                          /*Html::newInput("cupo","Cupo disponible *");
                          echo selectZonas();*/
                          Html::newInput("dir2","Direcci&oacute;n correspondencia *",$datos['dir2'],'disabled');
		  ?>
            </fieldset>
            <fieldset>
                <legend id="ipersonal">Informaci&oacute;n Laboral</legend>
                <?php
                Html::newInput("empresa","Nombre empresa *",$datos['empresa'],'disabled');
                Html::newInput("cargo","Cargo *",$datos['cargo'],'disabled');
                Html::newInput("dirempresa","Direccion empresa *",$datos['dirEmpresa'],'disabled');
                Html::newInput("telempresa","Telefono empresa *",$datos['telEmpresa'],'disabled');
                Html::newInput("actividadEconomica","Actividad economica *",$datos['actividad'],'disabled');
                Html::newInput("ciiu","Codigo CIIU*",$datos['ciiu'],'disabled');
                ?>
            </fieldset>
            <fieldset>
                <legend>Informaci&oacute;n Financiera</legend>
                <?php
                Html::newInput("ingresoMensual","Ingreso mensual *",toMoney($datos['ingresos']),'disabled');
                Html::newInput("otroMensual","Otros ingresos *",toMoney($datos['otrosIn']),'disabled');
                Html::newInput("otroDesc","Descripci&oacute;n ingresos *",($datos['ingresosDesc']),'disabled');
                Html::newInput("totalIngresos","Total ingresos *",toMoney($datos['totalingresos']),'disabled');
                Html::newInput("totalEgresos","Total egresos *",toMoney($datos['totalegresos']),'disabled');
                Html::newInput("totalActivos","Total activos *",toMoney($datos['totalactivos']),'disabled');
                Html::newInput("totalPasivos","Total pasivos *",toMoney($datos['totalpasivos']),'disabled');
                Html::newSelect('bienesFiducia','Relaciona bienes de fiducia',array('1'=>'Si','2'=>'No'),$datos['fiducia'],'disabled');
                Html::newInput("descFiducia","Descripci&oacute;n fiducia *",$datos['descFiducia'],'disabled');
                Html::newInput("cuentauno","Numero cuenta 1 *",$datos['cuenta1'],'disabled');
                echo cuentas($datos['tipo1'],'disabled');
                echo bancos($datos['numero1'],'disabled','banco1');
                Html::newInput("cuentados","Numero cuenta 2 *",$datos['cuenta2'],'disabled');
                echo cuentas($datos['tipo2'],'disabled','cuenta2');
                echo bancos($datos['numero2'],'disabled','banco2');
                ?>
            </fieldset>
            <fieldset>
                     <?php
	$i=0;
    	$val=0;
    	$cadena ='';
    	$cadena .= '<ul class="nav nav-tabs">';
    	foreach($ficheros As $f){
    		if($i>1){
	    		if($f){
	    			$val++;
	    			$ext = explode('.',$f);
	    			if($ext[1]=='pdf'){
					$img = $ext[1].'.png';
	    			}else if($ext[1]=='jpg' || $ext[1]=='png' || $ext[1]=='gif' || $ext[1]=='tif'){
	    				$img = 'imagen.png';
	    			}else{
	   				$img = 'file.png';
	    			}
	    			
	    			if($img == 'imagen.png'){
	    			$cadena .='<li>
	    				<a target="_blank" href="app/files/clientes/'.$datos['id_cliente'].'/'.$f.'">
	    					<img src="app/img/'.$img.'" height="22" width="22"><br/>'.$f.'
	    				</a>
	    		<a title="Eliminar" onclick="return confirm(\'Desea eliminar este archivo '.$f.'\')" href="'.setUrl('solicitudes','eliminarDocCli',array('file'=>$f,'folder'=>$datos['id_cliente'])).'">Eliminar <img src="app/img/eliminar_min.png" alt="Eliminar"></a>
	    			      <li/>';
	    			      }
    			      }else{
    			      	$cadena .= 'No hay datos';
    			      }
    		}
    		$i++;
    	}
    	$cadena .= '</ul>';
    	/*ficheros cliente*/
    	
    	echo $cadena;
	  ?>
                </fieldset>
        <?php
		  //Html::newHidden("id",$datos['id_cliente']);
		//  Html::newButton("editar", "Editar", "submit");
		  Html::closeForm();
	  ?>
        </div>
       </div>
       <?php  
    }

    
   /* public function importar($datos=null){
      echo getMensaje();
        ?>
           <h2><?php  echo CLIENTES_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  CLIENTES_CREAR=>setUrl('clientes'),
            		  CLIENTES_IMPORT=>'#',
            		  CLIENTES_LISTAR=>setUrl('clientes','lst'),
            		  CLIENTES_VINCULOS=>setUrl('clientes','vinculos')
            		 ),
            	    CLIENTES_IMPORT
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  CLIENTES_IMPORT=>'#'
            		 ),
            	    CLIENTES_IMPORT, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo CLIENTES_IMPORT?></h4>
       <div class='container'>
	   <a href="descargar.php?file=<?php echo SISTEMA_RUTADOCS?>clientes.txt" class="btn btn-mini">Descargar formato</a>
        <div class='span10 offset1'> 
	  <fieldset>
		  <legend><?php echo CLIENTES_IMPORT?></legend>
	  <?php
		  Html::openForm("import",setUrl("clientes","imporProcess"));
		  Html::newFile("archivo","Cargar archivo *");
	  ?>
	  </fieldset>
        <?php
		  Html::newButton("enviar", "Enviar", "submit");
		  Html::closeForm();
	  ?>
        </div>
       </div>
       <?php  
    }//importar
    */
    
    public function lst($datos=null){
      echo getMensaje();
        ?>
           <h2><?php  echo CLIENTES_LISTAR?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  CLIENTES_CREAR=>setUrl('clientes'),
            		  CLIENTES_CREAR_EMPRESA=>setUrl('clientes','empresa'),
            		  CLIENTES_LISTAR=>'#',
                          CLIENTES_LISTAR_EMP=>setUrl('clientes','lstEmpresa'),
            		  CLIENTES_REFERENCIA=>setUrl('clientes','convenio'),
            		  CLIENTES_CONVENIOS=>setUrl('clientes','lstConvenios')
            		 ),
            	    CLIENTES_LISTAR
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  CLIENTES_LISTAR=>'#'
            		 ),
            	    CLIENTES_LISTAR, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo CLIENTES_LISTAR?></h4>
       <div class='container'>
        <div> 
	    <?php
		if(!empty($datos)){
			echo datatable("tabla");
			Html::tabla(
					array("Tipo ID","ID","Nombres","Apellidos","Estado","Ver","Editar","Eliminar"),
							$datos,
							array("tipo_doc","id","nombre","apellido","estado"),
							array(
							"ver"=>setUrl("clientes","verCliente"),
							"editar"=>setUrl("clientes","editCliente"),
							"eliminar"=>setUrl("clientes","delCliente")
							)
						);
		}else{
		  ?>
		  <div class="alert alert-info">No hay Clientes creados.</div>
		  <?php
		}
	    ?>
        </div>
       </div>
       <?php  
    }
    
        public function lstEmpresa($datos=null){
      echo getMensaje();
        ?>
           <h2><?php  echo CLIENTES_LISTAR_EMP?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  CLIENTES_CREAR=>setUrl('clientes'),
            		  CLIENTES_CREAR_EMPRESA=>setUrl('clientes','empresa'),
            		  CLIENTES_LISTAR=>setUrl('clientes','lst'),
            		  CLIENTES_LISTAR_EMP=>'#',
            		  CLIENTES_REFERENCIA=>setUrl('clientes','convenio'),
            		  CLIENTES_CONVENIOS=>setUrl('clientes','lstConvenios')
            		 ),
            	    CLIENTES_LISTAR_EMP
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  CLIENTES_LISTAR_EMP=>'#'
            		 ),
            	    CLIENTES_LISTAR_EMP, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo CLIENTES_LISTAR_EMP?></h4>
       <div class='container'>
        <div> 
	    <?php
		if(!empty($datos)){
			echo datatable("tabla");
			Html::tabla(
                                array("ID","Nombre","Actividad","Ver","Editar","Eliminar"),
                                                $datos,
                                                array("nit","razon","actividad"),
                                                array(
                                                "ver"=>setUrl("clientes","verEmpresa"),
                                                "editar"=>setUrl("clientes","editEmpresa"),
                                                "eliminar"=>setUrl("clientes","delEmpresa")
                                                )
                                        );
		}else{
		  ?>
		  <div class="alert alert-info">No hay Clientes creados.</div>
		  <?php
		}
	    ?>
        </div>
       </div>
       <?php  
    }
    
    /*
    public function documentos($ficheros=null,$cc=null,$id='',$url=''){
      echo getMensaje();
        ?>
           <h2><?php  echo CLIENTES_DOC?></h2>

            <?php 
            navTabs(array('Regresar'=>setUrl('clientes','lst'), 
            		  CLIENTES_DOC=>'#'
            		 ),
            	    CLIENTES_DOC
            	   );
            
            navTabs(array('Regresar'=>setUrl('clientes','lst'), 
            		  CLIENTES_DOC=>'#'
            		 ),
            	    CLIENTES_DOC, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo CLIENTES_DOC?></h4>
       <div class='container'>
        <div class='span10 offset1'> 
	  <?php
	$i=0;
    	$val=0;
    	$cadena2='';
    	if(!empty($url)){
	$cadena2 ='<a class="btn btn-primary" href="'.setUrl("clientes","documentos",array("id"=>$id)).'">Atras</a>';
	}
    	$cadena = $cadena2.'<ul class="nav nav-tabs">';
    	foreach($ficheros As $f){
    		if($i>1){
	    		if($f){
	    			$val++;
	    			$ext = explode('.',$f);
	    			if($ext[1]=='pdf'){
					$img = $ext[1].'.png';
	    			}else if($ext[1]=='jpg' || $ext[1]=='png' || $ext[1]=='gif'){
	    				$img = 'imagen.png';
	    			}else if(empty($ext[1])){
	    				$img = 'folder.png';
	    			}else{
	   				$img = 'file.png';
	   				//$url=true;
	    			}
	    			if(!empty($ext[1])){
	    			$cadena .='<li>
	    				<a target="_blank" href="http://prestarmas.co/prestarmas/app/files/clientes/'.$cc.'/'.$f.'">
	    					<img src="app/img/'.$img.'" height="22" width="22"><br/>'.$f.'
	    				</a>
	    				<a title="Eliminar" onclick="return confirm(\'Desea eliminar este archivo '.$f.'\')" href="'.setUrl('clientes','eliminarDoc',array('id'=>$id,'cc'=>$cc,'file'=>$f,'folder'=>$url)).'">Eliminar <img src="app/img/eliminar_min.png" alt="Eliminar"></a>
	    			      <li/>';
	    			}else{
	    				$cadena .='<li>
	    				<a href="'.setUrl("clientes","documentos",array("id"=>$id,"url"=>$f)).'">
	    					<img src="app/img/'.$img.'" height="22" width="22"><br/>'.$f.'
	    				</a>
	    				<a title="Eliminar" onclick="return confirm(\'Desea eliminar este fichero con su contenido\')" href="'.setUrl('clientes','eliminarDoc2',array('id'=>$id,'cc'=>$cc,'folder'=>$f)).'">Eliminar <img src="app/img/eliminar_min.png"></a>
	    			      <li/>';
	    			}
    			      }else{
    			      	$cadena .= 'No hay datos';
    			      }
    		}
    		$i++;
    	}
    	$cadena .= '</ul>';
    	echo $cadena;
	  ?>
        </div>
       </div>
       <?php  
    }
    
    public function cargarDoc($cc){
      echo getMensaje();
        ?>
           <h2><?php  echo CLIENTES_DOC2?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('clientes'), 
            		  CLIENTES_DOC2=>'#'
            		 ),
            	    CLIENTES_DOC2
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  CLIENTES_DOC2=>'#'
            		 ),
            	    CLIENTES_DOC2, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo CLIENTES_DOC2?></h4>
       <div class='container'>
	   <!--<div class="span7 offset0">-->
	    
		<?php Html::openForm("solicitudes",setUrl("clientes","cargardocProcess"));?>
		<fieldset>
		  <legend>Adjuntos</legend>
		  <table class="table table-hover table-striped">
		    <tr>
			    <td><?php Html::newFile("adjuntos1", "<b>Cedula *</b>"); ?></td>
			    <td><?php Html::newFile("adjuntos2", "<b>Pagare *</b>"); ?></td>
		    </tr>
		    <tr>
		    	
		    </tr>
		   <tr>
		   	<td><?php Html::newFile("adjuntos3", "<b>Solicitud vinculacion *</b>"); ?></td>
		   </tr>
		  </table>
		</fieldset>
	   
		<?php
		  Html::newHidden("numero", $cc);
		  Html::newButton("enviar", "Enviar", "submit");
		  Html::closeForm();
		?>
        <!--</div>-->
       </div>
       <?php  
    }
    */
    
    /*
    public function editCupo($datos=null){
      echo getMensaje();
        ?>
           <h2><?php  echo CLIENTES_EDITAR?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('clientes'), 
            		  CLIENTES_EDITAR=>'#'
            		 ),
            	    CLIENTES_EDITAR
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  CLIENTES_EDITAR=>'#'
            		 ),
            	    CLIENTES_EDITAR, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo CLIENTES_EDITAR?></h4>
       <div class='container'>
        <div class='span10 offset1'> 
	  <fieldset>
			  <legend><?php echo CLIENTES_EDITAR?></legend>
		  <?php
			  Html::openForm("editar_cupo",setUrl("clientes","editCupoProcess"));
			  Html::newReadonly("cupoAnterior","Cupo actual ","text",toMoney($datos['cupo']));
			  Html::newInput("cupo","Nuevo cupo*");
		  ?>
	  </fieldset>
        <?php
		  Html::newHidden("id",$datos['id']);
		  Html::newButton("editar", "Editar", "submit");
		  Html::closeForm();
	  ?>
        </div>
       </div>
       <?php  
    }
    */
    
    public function ref($info=null){
        echo getMensaje();
        ?>
           <h2><?php  echo CLIENTES_REFERENCIA?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  CLIENTES_CREAR=>setUrl('clientes'),
            		  CLIENTES_LISTAR=>setUrl('clientes','lst'),
            		  CLIENTES_REFERENCIA=>'#',
            		  CLIENTES_VINCULOS=>setUrl('clientes','vinculos')
            		 ),
            	    CLIENTES_REFERENCIA
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  CLIENTES_REFERENCIA=>'#'
            		 ),
            	    CLIENTES_REFERENCIA, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo CLIENTES_REFERENCIA?></h4>
       <div class='container'>
        <div class='span10 offset1'> 
	  <fieldset>
			  <legend><?php echo CLIENTES_REFERENCIA_CREAR?></legend>
		  <?php
			  Html::openForm("ref",setUrl("clientes","saveRef"));
			  Html::newInput("ref","Nombre referencia *");
		  ?>
	  </fieldset>
                <?php
		  Html::newButton("crear", "Crear", "submit");
		  Html::closeForm();
                ?>
            <?php echo datatable('lst')?>
	    <table class="table table-hover" id="lst">
		<thead>
		  <tr>
		    <th>#</th>
		    <th>Referencia</th>
		    <th>Fecha registro</th>
		    <th>Acciones</th>
		  </tr>
		</thead>
		<tbody>
                <?php
                if(is_array($info)){
                  $j=1;
		    foreach ($info As $i):
		  ?>
		  <tr>
		    <td><?php echo $j?></td>
		    <td><?php echo $i['ref']?></td>
		    <td><?php echo $i['fecha']?></td>
		    <td>
		    <a href="<?php echo setUrl('clientes','editRef',array('id'=>trim($i["id"])))?>"><img src="app/img/editar.png" alt="editar" title="editar"></a>
                    <a href="<?php echo setUrl('clientes','delRef',array('id'=>trim($i["id"])))?>" onclick="return confirm('Desea eliminar la referencia ?')"><img src="app/img/eliminar.png" alt="eliminar" title="eliminar"></a>
		    </td>
		  </tr>
		  <?php
			$j++;
		    endforeach;
			}else{
			  echo 'No hay datos';
			}
		  ?>
                    </tbody>
	    </table>
        </div>
       </div>
       <?php  
    }
    
    public function editRef($datos=null){
        echo getMensaje();
        ?>
           <h2><?php  echo CLIENTES_REFERENCIA_EDITAR?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('clientes'), 
            		  CLIENTES_REFERENCIA_EDITAR=>'#'
            		 ),
            	    CLIENTES_REFERENCIA_EDITAR
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  CLIENTES_REFERENCIA_EDITAR=>'#'
            		 ),
            	    CLIENTES_REFERENCIA_EDITAR, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo CLIENTES_REFERENCIA_EDITAR?></h4>
       <div class='container'>
        <div class='span10 offset1'> 
	  <fieldset>
			  <legend><?php echo CLIENTES_REFERENCIA_EDITAR?></legend>
		  <?php
			  Html::openForm("editar_ref",setUrl("clientes","updRef"));
			  Html::newInput("ref","Referencia *", $datos['ref']);
		  ?>
	  </fieldset>
        <?php
		  Html::newHidden("id",$datos['id']);
		  Html::newButton("editar", "Editar", "submit");
		  Html::closeForm();
	  ?>
        </div>
       </div>
       <?php  
    }

    public function vinculos($info=null,$ref=null){
        echo getMensaje();
        ?>
           <h2><?php  echo CLIENTES_VINCULOS?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  CLIENTES_CREAR=>setUrl('clientes'),
            		  CLIENTES_LISTAR=>setUrl('clientes','lst'),
            		  CLIENTES_REFERENCIA=>setUrl('clientes','ref'),
            		  CLIENTES_VINCULOS=>'#'
            		 ),
            	    CLIENTES_VINCULOS
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  CLIENTES_VINCULOS=>'#'
            		 ),
            	    CLIENTES_VINCULOS, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo CLIENTES_VINCULOS?></h4>
       <div class='container'>
        <div class='span10 offset1'> 
	  <fieldset>
			  <legend><?php echo CLIENTES_VINCULOS_CREAR?></legend>
		  <?php
			  Html::openForm("vinculo",setUrl("clientes","saveVinculo"));
                          Html::newSelect("ref", "Referencia *", $ref);
			  Html::newInput("vinculo","Nombre vinculo *");
		  ?>
	  </fieldset>
                <?php
		  Html::newButton("crear", "Crear", "submit");
		  Html::closeForm();
                ?>
            <?php echo datatable('lst')?>
	    <table class="table table-hover" id="lst">
		<thead>
		  <tr>
		    <th>#</th>
		    <th>Referencia</th>
		    <th>Vinculo</th>
		    <th>Fecha registro</th>
		    <th>Acciones</th>
		  </tr>
		</thead>
		<tbody>
                <?php
                if(is_array($info)){
                  $j=1;
		    foreach ($info As $i):
		  ?>
		  <tr>
		    <td><?php echo $j?></td>
		    <td><?php echo $i['ref']?></td>
		    <td><?php echo $i['vinculo']?></td>
		    <td><?php echo $i['fecha']?></td>
		    <td>
		    <a href="<?php echo setUrl('clientes','editVinc',array('id'=>trim($i["elid"])))?>"><img src="app/img/editar.png" alt="editar" title="editar"></a>
                    <a href="<?php echo setUrl('clientes','delVinc',array('id'=>trim($i["elid"])))?>" onclick="return confirm('Desea eliminar el vinculo ?')"><img src="app/img/eliminar.png" alt="eliminar" title="eliminar"></a>
		    </td>
		  </tr>
		  <?php
			$j++;
		    endforeach;
			}else{
			  echo 'No hay datos';
			}
		  ?>
                    </tbody>
	    </table>
        </div>
       </div>
       <?php  
    }
    
    public function editVinc($info=null,$ref=null){
        echo getMensaje();
        ?>
           <h2><?php  echo CLIENTES_VINCULOS?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('clientes','vinculos'),
            		  CLIENTES_VINCULOS_EDITAR=>'#'
            		 ),
            	    CLIENTES_VINCULOS_EDITAR
            	   );
            
            navTabs(array(OP_BACK=>setUrl('clientes','vinculos'), 
            		  CLIENTES_VINCULOS=>'#'
            		 ),
            	    CLIENTES_VINCULOS, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo CLIENTES_VINCULOS?></h4>
       <div class='container'>
        <div class='span10 offset1'> 
	  <fieldset>
			  <legend><?php echo CLIENTES_VINCULOS_EDITAR?></legend>
		  <?php
			  Html::openForm("vinculo",setUrl("clientes","editVinculo"));
                          Html::newSelect("ref", "Referencia *", $ref,$info['laref']);
			  Html::newInput("vinculo","Nombre vinculo *",$info['vinculo']);
		  ?>
	  </fieldset>
                <?php
		  Html::newHidden("id",$info['elid']);
		  Html::newButton("editar", "Editar", "submit");
		  Html::closeForm();
                ?>
        </div>
       </div>
       <?php  
    }
    
    public function convenio($datos=null){
      echo getMensaje();
        ?>
           <h2><?php  echo CLIENTES_LISTAR?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  CLIENTES_CREAR=>setUrl('clientes'),
            		  CLIENTES_CREAR_EMPRESA=>setUrl('clientes','empresa'),
            		  CLIENTES_LISTAR=>setUrl('clientes','lst'),
                          CLIENTES_LISTAR_EMP=>setUrl('clientes','lstEmpresa'),
            		  CLIENTES_REFERENCIA=>'#',
            		  CLIENTES_CONVENIOS=>setUrl('clientes','lstConvenios')
            		 ),
            	    CLIENTES_REFERENCIA
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  CLIENTES_REFERENCIA=>'#'
            		 ),
            	    CLIENTES_REFERENCIA, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo CLIENTES_REFERENCIA?></h4>
       <div class='container'>
        <div> 
	    <?php
		if(!empty($datos)){
			echo datatable("tabla");
			?>
            <table class="table table-hover table-striped" id="tabla">
                <thead>
                    <th>#</th>
                    <th>NIT</th>
                    <th>Nombre</th>
                    <th>Correo</th>
                    <th>Telefono</th>
                    <th>Convenio</th>
                </thead>
                <tbody>
                    <?php echo $datos;?>
                </tbody>
            </table>
                        <?php
		}else{
		  ?>
		  <div class="alert alert-info">No hay Clientes creados.</div>
		  <?php
		}
	    ?>
        </div>
       </div>
       <?php  
    }
    
    public function convenir($datos=null){
      echo getMensaje();
        ?>
           <h2><?php  echo CLIENTES_LISTAR?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('clientes','convenio')
            		 ),
            	    CLIENTES_REFERENCIA
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('clientes','convenio'), 
            		  CLIENTES_REFERENCIA=>'#'
            		 ),
            	    CLIENTES_REFERENCIA, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo CLIENTES_REFERENCIA?></h4>
       <div class='container'>
        <div> 
	    <fieldset>
                <legend><?php echo "Convenio ".$datos["razon"]?></legend>
                <?php
                    Html::openForm("convenio",setUrl("clientes","convenirpro"));
                    echo inputDate("fechaI");
                    Html::newInput("fechaI","Fecha inicio *");
                    echo inputDate("fechaF");
                    Html::newInput("fechaF","Fecha fin *");
                ?>
                <div class="control-group" id="adj">
                                    <label for="adjuntos1" class="control-label">Adjuntos : </label>
                                    <div class="controls" id="nuevo">
                                        <input type="file" name="adjuntos1[]" id="adjuntos1">
                                        <input type="text" name="nadjunto[]" id="nadjunto" placeholder="Nombre archivo">
                                        <a class="btn clonar" onclick="clonar(this)">+</a>
                                        <a class="btn remover" onclick="remover(this)">-</a>
                                    </div>
                                </div>
	  </fieldset>
                <?php
                    Html::newHidden("id",$datos['id']);
                    Html::newButton("crear", "Crear", "submit");
                    Html::closeForm();
                ?>
        </div>
       </div>
       <?php  
    }
    
    public function lstConvenios($datos=null){
      echo getMensaje();
        ?>
           <h2><?php  echo CLIENTES_LISTAR?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  CLIENTES_CREAR=>setUrl('clientes'),
            		  CLIENTES_CREAR_EMPRESA=>setUrl('clientes','empresa'),
            		  CLIENTES_LISTAR=>setUrl('clientes','lst'),
                          CLIENTES_LISTAR_EMP=>setUrl('clientes','lstEmpresa'),
            		  CLIENTES_REFERENCIA=>setUrl('clientes','convenio'),
            		  CLIENTES_CONVENIOS=>'#'
            		 ),
            	    CLIENTES_CONVENIOS
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  CLIENTES_CONVENIOS=>'#'
            		 ),
            	    CLIENTES_CONVENIOS, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo CLIENTES_CONVENIOS?></h4>
       <div class='container'>
        <div> 
	    <?php
		if(!empty($datos)){
			echo datatable("tabla");
			Html::tabla(
					array("NIT","Razon social","Fecha convenio","Fin convenio","Documentos"),
							$datos,
							array("nit","razon","fecha_i","fecha_f"),
							array(
							"ver"=>setUrl("clientes","docsConv")
							)
						);
		}else{
		  ?>
		  <div class="alert alert-info">No hay Clientes creados.</div>
		  <?php
		}
	    ?>
        </div>
       </div>
       <?php  
    }
    
    public function docsConv($ficheros=null,$id2=null){
      echo getMensaje();
        ?>
           <h2><?php  echo CLIENTES_LISTAR?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('clientes','lstConvenios')
            		 ),
            	    CLIENTES_DOC
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('clientes','lstConvenios'), 
            		  CLIENTES_DOC=>'#'
            		 ),
            	    CLIENTES_DOC, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo CLIENTES_DOC?></h4>
       <div class='container'>
           <?php
	$i=0;
    	$val=0;
    	$cadena2='';
        $id='';
    	/*if(!empty($url)){
	$cadena2 ='<a class="btn btn-primary" href="'.setUrl("clientes","documentos",array("id"=>$id)).'">Atras</a>';
	}*/
    	$cadena = $cadena2.'<ul class="nav nav-tabs">';
    	foreach($ficheros As $f){
    		if($i>1){
	    		if($f){
	    			$val++;
	    			$ext = explode('.',$f);
	    			if($ext[1]=='pdf'){
					$img = $ext[1].'.png';
	    			}else if($ext[1]=='jpg' || $ext[1]=='png' || $ext[1]=='gif'){
	    				$img = 'imagen.png';
	    			}else if(empty($ext[1])){
	    				$img = 'folder.png';
	    			}else{
	   				$img = 'file.png';
	   				//$url=true;
	    			}
	    			if(!empty($ext[1])){
	    			$cadena .='<li>
	    				<a target="_blank" href="http://serprogreso.com/serprogreso/app/files/convenios/'.$id2.'/'.$f.'">
	    					<img src="app/img/'.$img.'" height="22" width="22"><br/>'.$f.'
	    				</a>
	    				<a title="Eliminar" onclick="return confirm(\'Desea eliminar este archivo '.$f.'\')" href="'.setUrl('clientes','eliminarDoc',array('id'=>$id)).'">Eliminar <img src="app/img/eliminar_min.png" alt="Eliminar"></a>
	    			      <li/>';
	    			}else{
	    				$cadena .='<li>
	    				<a href="'.setUrl("clientes","documentos",array("id"=>$id,"url"=>$f)).'">
	    					<img src="app/img/'.$img.'" height="22" width="22"><br/>'.$f.'
	    				</a>
	    				<a title="Eliminar" onclick="return confirm(\'Desea eliminar este fichero con su contenido\')" href="'.setUrl('clientes','eliminarDoc2',array('id'=>$id,'cc'=>$cc,'folder'=>$f)).'">Eliminar <img src="app/img/eliminar_min.png"></a>
	    			      <li/>';
	    			}
    			      }else{
    			      	$cadena .= 'No hay datos';
    			      }
    		}
    		$i++;
    	}
    	$cadena .= '</ul>';
    	echo $cadena;
	  ?>
       </div>
       <?php  
    }
    
    public function empresa($datos=null){
      echo getMensaje();
        ?>
           <h2><?php  echo CLIENTES_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  CLIENTES_CREAR=>setUrl('clientes'),
            		  CLIENTES_CREAR_EMPRESA=>'#',
            		  CLIENTES_LISTAR=>setUrl('clientes','lst'),
                          CLIENTES_LISTAR_EMP=>setUrl('clientes','lstEmpresa'),
            		  CLIENTES_REFERENCIA=>setUrl('clientes','convenio'),
            		  CLIENTES_CONVENIOS=>setUrl('clientes','lstConvenios')
            		 ),
            	    CLIENTES_CREAR_EMPRESA
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  CLIENTES_CREAR_EMPRESA=>'#'
            		 ),
            	    CLIENTES_CREAR_EMPRESA, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo CLIENTES_CREAR_EMPRESA?></h4>
       <div class='container'>
        <div class='span10 offset1'> 
            <?php Html::openForm("clientes",setUrl("clientes","saveEmpresa")); ?>
            
            <fieldset>
                <legend>Informaci&oacute;n general</legend>
                        <?php
                        Html::newInput("nit","Numero Identificacion *");
                        Html::newInput("nombre","Razon social *");
                        Html::newSelect("tipo_cli", "Tipo Empresa *", 
                                array(
                                      "1"=>"Privada",
                                      "2"=>"Oficial",
                                      "3"=>"Mixta"
                                      )
                        );
                        Html::newInput("direccion","Dirección *");
                        Html::newInput("telefono","T&eacute;lefono *");
                        Html::newInput("correo","Correo *");
                        Html::newInput("actividadEconomica","Actividad economica *");
                        Html::newInput("ciiu","Codigo CIIU*");
		  ?>
            </fieldset>
            
            <fieldset>
                <legend>Datos representante legal</legend>
                <?php
                Html::newInput("nombreR","Nombre *");
                Html::newInput("apellidoR","Apellido *");
                Html::newSelect("tipo_doc", "Tipo documento *", 
                        array(
                            "cc"=>"Cedula",
                            "ce"=>"Cedula de extranjeria",
                            "pp"=>"Pasaporte"
                            )
                );
                Html::newInput("ndocumento","Numero identificaci&oacute;n *");                
                ?>
            </fieldset>
            <fieldset>
                <legend>Informaci&oacute;n Financiera</legend>
                <?php               
                
                Html::newInput("ingresos2","Ingresos anuales *");
                Html::newHidden("ingresos");
                Html::newInput("ingresosNoOperativos2","Ingresos no operativos anuales *");
                Html::newHidden("ingresosNoOperativos");
                Html::newInput("ingresosDesc","Descripci&oacute;n ingresos *");
                Html::newInput("totalIngresos2","Total ingresos *");
                Html::newHidden("totalIngresos");
                Html::newInput("totalEgresos2","Total egresos *");
                Html::newHidden("totalEgresos");
                Html::newInput("totalActivos2","Total activos *");
                Html::newHidden("totalActivos");
                Html::newInput("totalPasivos2","Total pasivos *");
                Html::newHidden("totalPasivos");
                Html::newSelect('bienesFiducia','Relaciona bienes de fiducia',array('1'=>'Si','2'=>'No'));
                Html::newInput("descFiducia","Descripci&oacute;n fiducia *");
                ?>
            </fieldset>
            <fieldset>
                <legend>Composici&oacute;n accionaria</legend>
                <div class="control-group">
                    <label for="accionistas" class="control-label">Informaci&oacute;n : </label>
                    <div class="controls">
                        <input type="text" name="nombreA[]" id="nombreA" placeholder="Nombres y Apellidos/Razon social"><br/>
                        Tipo documento :
                        <select name="tipo_docA[]">
                            <option>-Seleccione-</option>
                            <option value="cc">Cedula</option>
                            <option value="ce">Cedula de extranjeria</option>
                            <option value="pp">Pasaporte</option>
                        </select><br/>
                        <input type="text" name="ndocumentoA[]" id="ndocumentoA" placeholder="Numero identificaci&oacute;n"><br/>
                        <input type="text" name="participacion[]" id="participacion" placeholder="Participaci&Oacute;n"><br/>
                        <a class="btn clonar" onclick="clonar(this)">+</a>
                        <a class="btn remover" onclick="remover(this)">-</a>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <legend>Operaciones internacionales</legend>
                <?php
                Html::newSelect('operacionesIn','Realiza operaciones internaciones *',array('1'=>'Si','2'=>'No'));
                Html::newSelect('tipoOperacion','Tipo de operacion *',
                        array(
                            'Exportaci&oacute;n'        =>  'Exportaci&oacute;n',
                            'Importaci&oacute;n'        =>  'Importaci&oacute;n',
                            'Pago de servicios'         =>  'Pago de servicios',
                            'Prestamos en moneda Ext.'  =>  'Prestamos en moneda Ext.'
                            )
                    );
                Html::newInput("productoFinanciero","Producto financiero *");
                Html::newInput("obanco","Banco *");
                Html::newInput("numeroCuenta","Numero de cuenta *");
                Html::newInput("moneda","Moneda *");
                Html::newInput("monto","Monto *");
                Html::newInput("ciudad","Ciudad *");
                ?>
            </fieldset>
            <fieldset>
                <legend>Informaci&oacute;n bancos</legend>
                <?php
                Html::newInput("cuentauno","Numero cuenta 1 *");
                echo cuentas();
                echo bancos(null,null,'banco1');
                Html::newInput("cuentados","Numero cuenta 2 *");
                echo cuentas(null,null,'cuenta2');
                echo bancos(null,null,'banco2');
                ?>
            </fieldset>
            <fieldset>
                <legend>Adjuntos</legend>
                    <div class="control-group" id="adj">
                        <label for="adjuntos1" class="control-label">Adjuntos : </label>
                        <div class="controls" id="nuevo">
                            <input type="file" name="adjuntos1[]" id="adjuntos1">
                            <input type="text" name="nadjunto[]" id="nadjunto" placeholder="Nombre archivo">
                            <a class="btn clonar" onclick="clonar(this)">+</a>
                            <a class="btn remover" onclick="remover(this)">-</a>
                        </div>
                    </div>
                    <div id="cont" style="display: none">0</div>
                </fieldset>
        <?php
		  Html::newButton("enviar", "Enviar", "submit");
		  Html::closeForm();
	  ?>
        </div>
       </div>
       <?php  
    }
    
    public function verEmpresa($datos=null,$ficheros=null){
      echo getMensaje();
        ?>
           <h2><?php  echo CLIENTES_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('clientes','lstEmpresa'),
            		  CLIENTES_CREAR_EMPRESA_VER=>'#'
            		 ),
            	    CLIENTES_CREAR_EMPRESA_VER
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('clientes','lstEmpresa'), 
            		  CLIENTES_CREAR_EMPRESA_VER=>'#'
            		 ),
            	    CLIENTES_CREAR_EMPRESA_VER, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo CLIENTES_CREAR_EMPRESA_VER?></h4>
       <div class='container'>
        <div class='span10 offset1'> 
            <?php Html::openForm("clientes",setUrl("clientes","saveEmpresa")); ?>
            
            <fieldset>
                <legend>Informaci&oacute;n general</legend>
                        <?php
                        Html::newInput("nit","Numero Identificacion *",$datos["nit"],"readonly");
                        Html::newInput("nombre","Razon social *",$datos["razon"],"readonly");
                        Html::newInput("direccion","Dirección *",$datos["dir"],"readonly");
                        Html::newInput("telefono","T&eacute;lefono *",$datos["tel"],"readonly");
                        Html::newInput("actividadEconomica","Actividad economica *",$datos["actividad"],"readonly");
                        Html::newInput("ciiu","Codigo CIIU*",$datos["ciiu"],"readonly");
		  ?>
            </fieldset>
            <fieldset>
                <legend>Datos representante legal</legend>
                <?php
                Html::newInput("nombreR","Nombre *",$datos["nombre"],"readonly");
                Html::newInput("apellidoR","Apellido *",$datos["apellido"],"readonly");
                Html::newSelect("tipo_doc", "Tipo documento *", 
                        array(
                            "cc"=>"Cedula",
                            "ce"=>"Cedula de extranjeria",
                            "pp"=>"Pasaporte"
                            ),$datos["eldoc"],"disabled"
                );
                Html::newInput("ndocumento","Numero identificaci&oacute;n *",$datos["numero"],"readonly");                
                ?>
            </fieldset>
            <fieldset>
                <legend>Informaci&oacute;n Financiera</legend>
                <?php               
                
                Html::newInput("ingresos2","Ingresos anuales *",  toMoney($datos["in_anuales"]),"readonly");
                Html::newHidden("ingresos");
                Html::newInput("ingresosNoOperativos2","Ingresos no operativos anuales *",toMoney($datos["in_anuales_no"]),"readonly");
                Html::newHidden("ingresosNoOperativos");
                Html::newInput("ingresosDesc","Descripci&oacute;n ingresos *",$datos["desc_ingresos"],"readonly");
                Html::newInput("totalIngresos2","Total ingresos *",toMoney($datos["t_ingresos"]),"readonly");
                Html::newHidden("totalIngresos");
                Html::newInput("totalEgresos2","Total egresos *",toMoney($datos["t_egresos"]),"readonly");
                Html::newHidden("totalEgresos");
                Html::newInput("totalActivos2","Total activos *",toMoney($datos["t_activos"]),"readonly");
                Html::newHidden("totalActivos");
                Html::newInput("totalPasivos2","Total pasivos *",toMoney($datos["t_pasivos"]),"readonly");
                Html::newHidden("totalPasivos");
                Html::newSelect('bienesFiducia','Relaciona bienes de fiducia',
                        array(
                            '1'=>'Si',
                            '2'=>'No'
                            ),$datos["fiducia"],"disabled"
                        );
                Html::newInput("descFiducia","Descripci&oacute;n fiducia *",$datos["desc_fiducia"],"readonly");
                ?>
            </fieldset>
            <fieldset>
                <legend>Composici&oacute;n accionaria</legend>
                <div class="control-group">
                    <label for="accionistas" class="control-label">Informaci&oacute;n : </label>
                    <div class="controls">
                        <?php
                        $i=0;
                        $acc_nombres    = unserialize($datos['nombres']);
                        $acc_tipo       = unserialize($datos['tipo_doc']);
                        $acc_doc        = unserialize($datos['documento']);
                        $acc_part       = unserialize($datos['participacion']);
                        
                        foreach($acc_nombres As $acc):
                        ?>
                        <input type="text" name="nombreA[]" id="nombreA" value="<?php echo $acc?>" readonly><br/>
                            <?php
                            Html::newSelect("tipo_docA[]","Tipo documento",
                                    array(
                                        "cc"=>"Cedula",
                                        "ce"=>"Cedula extranjeria",
                                        "pp"=>"Pasaporte"
                                    ),$acc_tipo[$i],'disabled')
                            ?>
                            <br/>
                            <input type="text" name="ndocumentoA[]" id="ndocumentoA" value="<?php echo $acc_doc[$i]?>"><br/>
                            <input type="text" name="participacion[]" id="participacion" value="<?php echo $acc_part[$i]?>"><br/>
                        <?php
                            $i++;
                        endforeach;
                        ?>
                        <!--<a class="btn clonar" onclick="clonar(this)">+</a>
                        <a class="btn remover" onclick="remover(this)">-</a>-->
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <legend>Operaciones internacionales</legend>
                <?php
                Html::newSelect('operacionesIn','Realiza operaciones internaciones *',
                        array('1'=>'Si','2'=>'No'),
                        $datos['operacionesIn'],
                        'disabled'
                        );
                Html::newSelect('tipoOperacion','Tipo de operacion *',
                        array(
                            'Exportaci&oacute;n'        =>  'Exportaci&oacute;n',
                            'Importaci&oacute;n'        =>  'Importaci&oacute;n',
                            'Pago de servicios'         =>  'Pago de servicios',
                            'Prestamos en moneda Ext.'  =>  'Prestamos en moneda Ext.'
                            ),$datos['tipoOperacion'],
                        'disabled'
                    );
                Html::newInput("productoFinanciero","Producto financiero *",$datos['productoFinanciero'],'disabled');
                Html::newInput("obanco","Banco *",$datos['obanco'],'disabled');
                Html::newInput("numeroCuenta","Numero de cuenta *",$datos['numeroCuenta'],'disabled');
                Html::newInput("moneda","Moneda *",$datos['moneda'],'disabled');
                Html::newInput("monto","Monto *",$datos['monto'],'disabled');
                Html::newInput("ciudad","Ciudad *",$datos['ciudad'],'disabled');
                ?>
            </fieldset>
            <fieldset>
                <legend>Informaci&oacute;n bancos</legend>
                <?php
                Html::newInput("cuentauno","Numero cuenta 1 *",$datos['cuenta1'],'disabled');
                echo cuentas($datos['tipo1'],'disabled');
                echo bancos($datos['numero1'],'disabled','banco1');
                Html::newInput("cuentados","Numero cuenta 2 *",$datos['cuenta2'],'disabled');
                echo cuentas($datos['tipo2'],'disabled','cuenta2');
                echo bancos($datos['numero2'],'disabled','banco2');
                ?>
            </fieldset>
            <fieldset>
            <legend>Adjuntos</legend>
            <?php
            $i=0;
            $val=0;
            $cadena ='';
            $cadena .= '<ul class="nav nav-tabs">';
            foreach($ficheros As $f){
                    if($i>1){
                            if($f){
                                    $val++;
                                    $ext = explode('.',$f);
                                    if($ext[1]=='pdf'){
                                            $img = $ext[1].'.png';
                                    }else if($ext[1]=='jpg' || $ext[1]=='png' || $ext[1]=='gif' || $ext[1]=='tif'){
                                            $img = 'imagen.png';
                                    }else{
                                            $img = 'file.png';
                                    }

                                    if($img == 'imagen.png'){
                                    $cadena .='<li>
                                            <a target="_blank" href="app/files/clientes/'.$datos['id_cliente'].'/'.$f.'">
                                                    <img src="app/img/'.$img.'" height="22" width="22"><br/>'.$f.'
                                            </a>
                            <a title="Eliminar" onclick="return confirm(\'Desea eliminar este archivo '.$f.'\')" href="'.setUrl('solicitudes','eliminarDocCli',array('file'=>$f,'folder'=>$datos['id_cliente'])).'">Eliminar <img src="app/img/eliminar_min.png" alt="Eliminar"></a>
                                          <li/>';
                                          }
                                  }else{
                                    $cadena .= 'No hay datos';
                                  }
                    }
                    $i++;
            }
            $cadena .= '</ul>';
            /*ficheros cliente*/
            echo $cadena;
	  ?>
            </fieldset>
        <?php
		  /*Html::newButton("enviar", "Enviar", "submit");*/
		  Html::closeForm();
	  ?>
        </div>
       </div>
       <?php  
    }
    
    public function editEmpresa($datos=null){
      echo getMensaje();
        ?>
           <h2><?php  echo CLIENTES_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('clientes','lstEmpresa'),
            		  CLIENTES_CREAR_EMPRESA_EDITAR=>'#'
            		 ),
            	    CLIENTES_CREAR_EMPRESA_EDITAR
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('clientes','lstEmpresa'), 
            		  CLIENTES_CREAR_EMPRESA_EDITAR=>'#'
            		 ),
            	    CLIENTES_CREAR_EMPRESA_EDITAR, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo CLIENTES_CREAR_EMPRESA_EDITAR?></h4>
       <div class='container'>
        <div class='span10 offset1'> 
            <?php Html::openForm("clientes",setUrl("clientes","editEmpresapro")); ?>
            
            <fieldset>
                <legend>Informaci&oacute;n general</legend>
                        <?php
                        Html::newInput("nit","Numero Identificacion *",$datos["nit"]);
                        Html::newInput("nombre","Razon social *",$datos["razon"]);
                        Html::newInput("direccion","Dirección *",$datos["dir"]);
                        Html::newInput("telefono","T&eacute;lefono *",$datos["tel"]);
                        Html::newInput("actividadEconomica","Actividad economica *",$datos["actividad"]);
                        Html::newInput("ciiu","Codigo CIIU*",$datos["ciiu"]);
		  ?>
            </fieldset>
            <fieldset>
                <legend>Datos representante legal</legend>
                <?php
                Html::newInput("nombreR","Nombre *",$datos["nombre"]);
                Html::newInput("apellidoR","Apellido *",$datos["apellido"]);
                Html::newSelect("tipo_doc", "Tipo documento *", 
                        array(
                            "cc"=>"Cedula",
                            "ce"=>"Cedula de extranjeria",
                            "pp"=>"Pasaporte"
                            ),$datos["eldoc"]
                );
                //echo $datos["tipo_doc"];
                Html::newInput("ndocumento","Numero identificaci&oacute;n *",$datos["numero"]);                
                ?>
            </fieldset>
            <fieldset>
                <legend>Informaci&oacute;n Financiera</legend>
                <?php               
                
                Html::newInput("ingresos","Ingresos anuales *", ($datos["in_anuales"]));
                //Html::newHidden("ingresos");
                Html::newInput("ingresosNoOperativos","Ingresos no operativos anuales *",($datos["in_anuales_no"]));
                //Html::newHidden("ingresosNoOperativos");
                Html::newInput("ingresosDesc","Descripci&oacute;n ingresos *",$datos["desc_ingresos"]);
                Html::newInput("totalIngresos","Total ingresos *",($datos["t_ingresos"]));
                //Html::newHidden("totalIngresos");
                Html::newInput("totalEgresos","Total egresos *",($datos["t_egresos"]));
                //Html::newHidden("totalEgresos");
                Html::newInput("totalActivos","Total activos *",($datos["t_activos"]));
                //Html::newHidden("totalActivos");
                Html::newInput("totalPasivos","Total pasivos *",($datos["t_pasivos"]));
                //Html::newHidden("totalPasivos");
                Html::newSelect('bienesFiducia','Relaciona bienes de fiducia',
                        array(
                            '1'=>'Si',
                            '2'=>'No'
                            ),$datos["fiducia"]
                        );
                Html::newInput("descFiducia","Descripci&oacute;n fiducia *",$datos["desc_fiducia"]);
                ?>
            </fieldset>
            <fieldset>
                <legend>Composici&oacute;n accionaria</legend>
                <div class="control-group">
                    <label for="accionistas" class="control-label">Informaci&oacute;n : </label>
                    <div class="controls">
                        <?php
                        $i=0;
                        $acc_nombres    = unserialize($datos['nombres']);
                        $acc_tipo       = unserialize($datos['tipo_doc']);
                        $acc_doc        = unserialize($datos['documento']);
                        $acc_part       = unserialize($datos['participacion']);
                        
                        foreach($acc_nombres As $acc):
                        ?>
                        <input type="text" name="nombreA[]" id="nombreA" value="<?php echo $acc?>" readonly><br/>
                            <?php
                            Html::newSelect("tipo_docA[]","Tipo documento",
                                    array(
                                        "cc"=>"Cedula",
                                        "ce"=>"Cedula extranjeria",
                                        "pp"=>"Pasaporte"
                                    ),$acc_tipo[$i])
                            ?>
                            <br/>
                            <input type="text" name="ndocumentoA[]" id="ndocumentoA" value="<?php echo $acc_doc[$i]?>"><br/>
                            <input type="text" name="participacion[]" id="participacion" value="<?php echo $acc_part[$i]?>"><br/>
                        <?php
                            $i++;
                        endforeach;
                        ?>
                        <!--<a class="btn clonar" onclick="clonar(this)">+</a>
                        <a class="btn remover" onclick="remover(this)">-</a>-->
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <legend>Operaciones internacionales</legend>
                <?php
                Html::newSelect('operacionesIn','Realiza operaciones internaciones *',
                        array('1'=>'Si','2'=>'No'),
                        $datos['operacionesIn'],
                        'disabled'
                        );
                Html::newSelect('tipoOperacion','Tipo de operacion *',
                        array(
                            'Exportaci&oacute;n'        =>  'Exportaci&oacute;n',
                            'Importaci&oacute;n'        =>  'Importaci&oacute;n',
                            'Pago de servicios'         =>  'Pago de servicios',
                            'Prestamos en moneda Ext.'  =>  'Prestamos en moneda Ext.'
                            ),$datos['tipoOperacion']
                    );
                Html::newInput("productoFinanciero","Producto financiero *",$datos['productoFinanciero']);
                Html::newInput("obanco","Banco *",$datos['obanco']);
                Html::newInput("numeroCuenta","Numero de cuenta *",$datos['numeroCuenta']);
                Html::newInput("moneda","Moneda *",$datos['moneda']);
                Html::newInput("monto","Monto *",$datos['monto']);
                Html::newInput("ciudad","Ciudad *",$datos['ciudad']);
                ?>
            </fieldset>
            <fieldset>
                <legend>Informaci&oacute;n bancos</legend>
                <?php
                Html::newInput("cuentauno","Numero cuenta 1 *",$datos['cuenta1']);
                echo cuentas($datos['tipo1']);
                echo bancos($datos['numero1'],'banco1');
                Html::newInput("cuentados","Numero cuenta 2 *",$datos['cuenta2']);
                echo cuentas($datos['tipo2'],'cuenta2');
                echo bancos($datos['numero2'],'banco2');
                ?>
            </fieldset>
            <fieldset>
                <legend>Adjuntos</legend>
                <div class="control-group" id="adj">
                        <label for="adjuntos1" class="control-label">Adjuntos : </label>
                        <div class="controls" id="nuevo">
                            <input type="file" name="adjuntos1[]" id="adjuntos1">
                            <input type="text" name="nadjunto[]" id="nadjunto" placeholder="Nombre archivo">
                            <a class="btn clonar" onclick="clonar(this)">+</a>
                            <a class="btn remover" onclick="remover(this)">-</a>
                        </div>
                    </div>
                    <div id="cont" style="display: none">0</div>
            </fieldset>
        <?php
                Html::newHidden("id",$datos['empresa']);
                Html::newButton("enviar", "Editar", "submit");
                Html::closeForm();
	  ?>
        </div>
       </div>
       <?php  
    }
    
} // class
