<?php 
include_once 'app/modulos/clientes/vista/v.Clientes.php';
include_once 'app/modulos/clientes/modelo/m.Clientes.php';
include_once 'app/modulos/sucursales/modelo/m.Sucursales.php';
include_once 'app/modulos/productos/modelo/m.Productos.php';

class Clientes extends vClientes{

    public function main(){
        validar_usuario();
        parent::main();    
    }
    
    public function empresa(){
        validar_usuario();
        parent::empresa(); 
    }
    
    public function saveCliente($array=null){
	validar_usuario();
        
	if(is_numeric($array['nombre'])){
	  setMensaje('EL campo nombre no puede contener numeros','error');
	  location(setUrl('clientes'));
	}
	
	if(is_numeric($array['apellido'])){
	  setMensaje('EL campo apellido no puede contener numeros','error');
	  location(setUrl('clientes'));
	}
	
        if(empty($array["tipo_doc"])){
            $array["tipo_doc"]="nit";
        }

	$validar_id = mClientes::listarId($array['ndocumento']);
	
	if(!$validar_id){
            
            include_once 'app/sistema/ftp.php';
            $total = count($array["adjuntos1"]["name"]);
            
            mkdir('app/files/clientes/'.$array['ndocumento'],0777,TRUE);
           // chmod('app/files/clientes/', "0777");
            
            $ftp=new ftp();
            for($i=0;$i<$total;$i++){
                $local1 = trim($array["adjuntos1"]["name"][$i]);// el nombre del archivo	
                $remoto1 = $array["adjuntos1"]["tmp_name"][$i];// Este es el nombre temporal del archivo mientras dura la transmision	
                $tama1 = $array["adjuntos1"]["size"][$i];
                $ftp->SubirArchivo($local1,$remoto1,$tama1,'app/files/clientes/'.$array['ndocumento'],'cliente');
            }
            //die('jum');
            $ORM = new ORM();
            $ORM->iniTransaccion();
            try{
                     mClientes::saveCliente(
                                   $array["tipo_doc"],
                                   $array["ndocumento"],
                                   $array["nombre"],
                                   $array["apellido"],
                                   $array["fechaNaci"],
                                   $array["correo"],
                                   $array["ciudad"],
                                   $array["barrio"],
                                   $array["telefono"],
                                   $array["celular"],
                                   $array["direccion"],
                                   $array["dir2"],
                                   getSucursal(),
                                   $array["tipo_cli"]
                                   );
                    
                    mClientes::saveClienteLaboral(
                            $array["empresa"],
                            $array["cargo"],
                            $array["dirempresa"],
                            $array["telempresa"],
                            $array["actividadEconomica"],
                            $array["ciiu"],
                            $array["ndocumento"]
                            );
                    
                    mClientes::saveClienteFinanciera(
                            $array["ingresoMensual"],
                            $array["otroMensual"],
                            $array["otroDesc"],
                            $array["totalIngresos"],
                            $array["totalEgresos"],
                            $array["totalActivos"],
                            $array["totalPasivos"],
                            $array["bienesFiducia"],
                            $array["descFiducia"],
                            $array["cuentauno"],
                            $array["cuenta"],
                            $array["banco1"],
                            $array["cuentados"],
                            $array["cuenta2"],
                            $array["banco2"],
                            $array["ndocumento"]
                            );
                    
                    /*procesar querys*/
                     $ORM->commit();
                     
                     setMensaje('Cliente creado correctamente.','success');
            }catch (Exception $e){
                $e->getCode();
                $ORM->rollback();
                setMensaje('Error al crear el cliente','error');
            }  
	}else{
	  setMensaje('Cliente con documento '.$array['ndocumento'].' ya se encuentra creado.','info');
	}
	location(setUrl('clientes'));
    }
    
    public function editCliente($array=null){
	validar_usuario();
	valId($array['id'], 'clientes');
	
	$datos = mClientes::listarId($array['id']);
	parent::editCliente($datos[0]);
    }
    
    public function updCliente($array=null){
        //die(print_r($array));
	validar_usuario();
	valId($array['id'], 'clientes');
	try{
            
            include_once 'app/sistema/ftp.php';
            $total = count($array["adjuntos1"]["name"]);
            
            //mkdir('app/files/clientes/'.$array['ndocumento'],0777,TRUE);
           // chmod('app/files/clientes/', "0777");
            if($total>0){
                $ftp=new ftp();
                for($i=0;$i<$total;$i++){
                    $local1 = trim($array["adjuntos1"]["name"][$i]);// el nombre del archivo	
                    $remoto1 = $array["adjuntos1"]["tmp_name"][$i];// Este es el nombre temporal del archivo mientras dura la transmision	
                    $tama1 = $array["adjuntos1"]["size"][$i];
                    $ftp->SubirArchivo($local1,$remoto1,$tama1,'app/files/clientes/'.$array['ndocumento'],'cliente');
                }
            }
            mClientes::updCliente(
                                    $array['id'],
                                    $array["tipo_doc"],
                                    $array["nombre"],
                                    $array["apellido"],
                                    $array["fechaNaci"],
                                    $array["correo"],
                                    $array["ciudad"],
                                    $array["barrio"],
                                    $array["telefono"],
                                    $array["celular"],
                                    $array["direccion"],
                                    $array["dir2"]
                                );
	
         mClientes::updClienteLaboral(
                    $array['id'],
                    $array["empresa"],
                    $array["cargo"],
                    $array["dirempresa"],
                    $array["telempresa"],
                    $array["actividadEconomica"],
                    $array["ciiu"]
                );
         
         mClientes::updClienteBancos(
                    $array['id'],
                    $array['ingresoMensual'],
                    $array['otroMensual'],
                    $array['otroDesc'],
                    $array['totalIngresos'],
                    $array['totalEgresos'],
                    $array['totalActivos'],
                    $array['totalPasivos'],
                    $array['bienesFiducia'],
                    $array['descFiducia'],
                    $array['cuentauno'],
                    $array['cuenta'],
                    $array['banco1'],
                    $array['cuentados'],
                    $array['cuenta2'],
                    $array['banco2']
                );
        
         setMensaje('Cliente actualizado correctamente.','success');
         location(setUrl('clientes','lst'));
        }  catch (Exception $e){
            setMensaje('Error al actualizar al cliente '.$e->getMessage(),'error');
            location(setUrl('clientes','lst'));
        }
        
    }
    
    public function delCliente($array=null){
	validar_usuario();
	//die(print_r($array));
	valId($array['id'], 'clientes');
	$id=$array['id'];
	
	 $info = mClientes::listarId($id);
	//eliminarDir('app/files/clientes/'.$info[0]['numero'],$info[0]['numero']);
	
	$del = mClientes::delCliente($id);
	//$del=true;
	if($del){
	  setMensaje('Cliente eliminado correctamente.','success');
	    	location(setUrl('clientes','lst'));
	}else{
	  setMensaje('Error al eliminar el cliente','error');
	  location(setUrl('clientes','lst'));
	}
	
    }
    
        public function delEmpresa($array=null){
	validar_usuario();
	//die(print_r($array));
	valId($array['id'], 'clientes');
	$id=$array['id'];
	
	//$info = mClientes::listarId($id);
	//eliminarDir('app/files/clientes/'.$info[0]['numero'],$info[0]['numero']);
	
	if(mClientes::delEmpresa($id)){
	  setMensaje('Empresa eliminada correctamente.','success');
	}else{
	  setMensaje('Error al eliminar la empresa','error');
	}
	location(setUrl('clientes','lstEmpresa'));
    }
    
    public function saveEmpresa($array=null){
        validar_usuario();
        //die(print_r($array));
	//$validar_id = mClientes::listarId($array['ndocumento']);
	$validar_id = false;
	if(!$validar_id){
            
            include_once 'app/sistema/ftp.php';
            $total = count($array["adjuntos1"]["name"]);
            
            mkdir('app/files/clientes/'.$array['nit'],0777,TRUE);
            if($total>0){
                $ftp=new ftp();
                for($i=0;$i<$total;$i++){
                    $local1 = trim($array["adjuntos1"]["name"][$i]);// el nombre del archivo	
                    $remoto1 = $array["adjuntos1"]["tmp_name"][$i];// Este es el nombre temporal del archivo mientras dura la transmision	
                    $tama1 = $array["adjuntos1"]["size"][$i];
                    $ftp->SubirArchivo($local1,$remoto1,$tama1,'app/files/clientes/'.$array['nit'],'cliente');
                }
            }
            //die('jum');
            $ORM = new ORM();
            $ORM->iniTransaccion();
            try{
                    mClientes::saveEmpresa(
                                  $array["nombre"],
                                  $array["nit"],
                                  $array["direccion"],
                                  $array["telefono"],
                                  $array["actividadEconomica"],
                                  $array["ciiu"],
                                  $array["correo"]
                                  );
                    
                    mClientes::saveEmpresaRepre(
                            $array["nombreR"],
                            $array["apellidoR"],
                            $array["tipo_doc"],
                            $array["ndocumento"],
                            $array["nit"]
                            );
                    
                    mClientes::saveEmpresaFinan(
                            $array["ingresos"],
                            $array["ingresosNoOperativos"],
                            $array["ingresosDesc"],
                            $array["totalIngresos"],
                            $array["totalEgresos"],
                            $array["totalActivos"],
                            $array["totalPasivos"],
                            $array["bienesFiducia"],
                            $array["descFiducia"],
                            $array["nit"]
                            );
                    
                    mClientes::saveEmpresaAccio(
                            serialize($array["nombreA"]),
                            serialize($array["tipo_docA"]),
                            serialize($array["ndocumentoA"]),
                            serialize($array["participacion"]),
                            $array["nit"]
                            );
                    
                    mClientes::saveEmpresaOper(
                            $array["operacionesIn"],
                            $array["tipoOperacion"],
                            $array["productoFinanciero"],
                            $array["obanco"],
                            $array["numeroCuenta"],
                            $array["moneda"],
                            $array["monto"],
                            $array["ciudad"],
                            $array["nit"]
                            );
                    
                    mClientes::saveEmpresaBanco(
                            $array["cuentauno"],
                            $array["cuenta"],
                            $array["banco1"],
                            $array["cuentados"],
                            $array["cuenta2"],
                            $array["banco2"],
                            $array["nit"]
                            );
                    
                    /*procesar querys*/
                     $ORM->commit();
                     
                     setMensaje('Empresa creada correctamente.','success');
            }catch (Exception $e){
                $e->getMessage();
                $ORM->rollback();
                setMensaje('Error al crear el cliente','error');
            }  
	}else{
	  setMensaje('Empresa con NIT '.$array['nit'].' ya se encuentra creado.','info');
	}
	location(setUrl('clientes','empresa'));
    }
    /*
    public function importar(){
	validar_usuario();
	parent::importar();
    }
    
    public function imporProcess($array=null){
	validar_usuario();

	if($array["archivo"]["size"]==0){
	    $errores[] = 'No se adjunto ningun archivo.';
	}
				
	$ext = explode(".",$array["archivo"]["name"]);
	$ext = strtolower($ext[count($ext)-1]);
		  // die('='.$ext);
	if($ext != "txt"){
	    $errores[] = 'El tipo de archivo <b><i>'.$array["archivo"]["type"].'</i></b> no es permitido.';
	}
		
	if($array["archivo"]["size"]>5000000){
	    $errores[] = 'El archivo supera el tama&ntilde;o maximo permitido.';
	}
		    
	if(!empty($errores)){
		setMensaje('<ul><li>'.(implode("</li><li>",$errores)).'</li></ul>',"error");
		location(setUrl("clientes","importar"));
	}else{
		  $sql 	= array();

		  $nombre 	= $array['archivo']['tmp_name'];		   		
		  $archivo 	= $array['archivo']['name'];
		  
		  $mClientes=new mClientes();		
		
		  $Contenido = file_get_contents($nombre);

		  if ($Contenido === false) die (" No se puede leer los datos del archivo $ archivo ");
		  
		  $Registro = explode("\n", $Contenido);
		  			//die('='.print_r($Registro));
		  $j=0;
		  for($i=0;$i<(count($Registro));$i++){
		   if($i>=1){
			$Registro2 = explode("\t",$Registro[$i]);
			// if(count($Registro2) < 12) die('Error en el formato');
			$sql[$j] = $Registro2;
			$j++;
		    }
		  }//for
		 // die(print_r($sql));
		  if(!empty($errores)){
			  setMensaje('Error en la lectura del archivo: <ul><li>'.(implode("</li><li>",$errores)).'</li></ul>',"error");
		  }else{
		    $guardo = 0;
		    $noguardo =0;
		    $upds =0;
		    $noupds =0;
		    $mensaje ='';
			  if(!empty($sql)){
			    foreach ($sql As $s):
				  
			  $validar_cliente = $mClientes->listarId2($s[1]);
			    if($validar_cliente){
				//echo 'ya esta '.$s[1].'<br/>';
				$upd = $mClientes->updCliente2($validar_cliente[0]['id'],$s[8],$validar_cliente[0]['cupo_utilizado']);
				if($upd){
					$upds++;
				} else{
					$noupds++;
					$mensaje .= 'Error al intentar Actualizar el cliente '.$s[2].' '.$s[3].' &oacute; sus datos son iguales<br/>';
				}
			    }else{
				  $guardar = $mClientes->saveCliente($s[0],$s[1],$s[2],$s[3],$s[4],$s[5],$s[6],$s[7],$s[8],$s[9],$s[10],$s[11],$s[12],getSucursal(),'1');
				  if($guardar){
				  	
				  		    if(!is_dir('app/files/clientes/'.$s[1])){
						    	if(!is_dir('app/files/clientes')){
						    		mkdir('app/files/clientes',0755);
						    	}
							mkdir('app/files/clientes/'.$s[1],0755);
						    }
					  $guardo++;
				  } else{
					  $noguardo++;
					  $mensaje .= 'Error interno al intentar guardar el cliente '.$s[1].'<br/>';
				  }
				}//if validar cliente
			    endforeach;
			    
			  }//if sql vacio
		  }
	  }
	  $mensajes='';
	  if($guardo>0){
	    $mensajes .= "-".$guardo." Registros cargados exitosamente<br/>";
	  }
	  if($noguardo>0){
	    $mensajes .= "-".$noguardo.' NO guardados<br/>'.$mensaje.'<br/>';
	  }
	  if($upds>0){
	    $mensajes .= "-".$upds.' Registros actualizados exitosamente<br/>';
	  }
	  if($noupds>0){
	    $mensajes .= "-".$noupds.' Registros no actualizados<br/>'.$mensaje.'<br/>';
	  }
	  setMensaje($mensajes,"info");
	  location(setUrl('clientes','importar'));
    }
    */
    
    public function lstClientes2($array=null){
	validar_usuario();
        $datos = mClientes::listar2();
	$cadena='';
	foreach($datos As $d){
	  $cadena .= $d['id'].' - '.$d['nombre'].',';
	}
	echo $cadena;
	
    }
    
    public function lstClientes($array=null){
	validar_usuario();
	
	if(isset($array["estado"])){
		$datos = mClientes::listar($array["estado"]);
                $datos2 = mClientes::listarEmpresa();
        }else{
		$datos = mClientes::listar();
		$datos2 = mClientes::listarEmpresa();
        }
	$cadena='';
	foreach($datos As $d){
	  $cadena .= $d['id'].' - '.$d['nombre'].' '.$d['apellido'].',';
	}
        foreach($datos2 As $d2){
	  $cadena .= $d2['nit'].' - '.$d2['razon'].',';
	}
	echo $cadena;
	
    }
    
    public function lst($array=null){
	validar_usuario();
	$datos = mClientes::listar();
	parent::lst($datos);   
    }
    
    public function lstEmpresa(){
        validar_usuario();
        $datos = mClientes::listarEmpresa();
	parent::lstEmpresa($datos);
    }


    public function clienteId($array=null){
	validar_usuario();
	$datos = mClientes::listarId2($array['id']);
	//die(print_r($datos));
	$datos =$datos[0];
	echo $datos['nombre'].' '.$datos['apellido'].','.$datos['telefono'].','.$datos['celular'].','.$datos['cupo_disponible'].','.$datos['direccion'].','.$datos['fechaNaci'];
    }
    
    /*
    public function documentos($array=null){
    	validar_usuario();
    	include 'app/sistema/ftp.php';
    	
    	$info = mClientes::listarId($array['id']);
    	$url=empty($array['url'])?null:$array['url'].'/';
    	$ftp=new ftp();
    	$ficheros = $ftp->ficheros('./app/files/clientes/'.$info[0]['numero'].'/'.$url);
    	
	parent::documentos($ficheros,$info[0]['numero'],$array['id'],$url);
    	
    }
  	
    public function cargarDoc($array=null){
		validar_usuario();
		$info = mClientes::listarId($array['id']);
		
		parent::cargarDoc($info[0]['numero']);
	}
	
    public function cargardocProcess($array=null){
		validar_usuario();
		include_once 'app/sistema/ftp.php';
		
		if($array["adjuntos1"]["size"]>0){
		  $ext1 = explode(".",$array["adjuntos1"]["name"]);
		  $ext1 = strtolower($ext1[count($ext1)-1]);
	
		  if($ext1 == "pdf" || $ext1 == "jpg" || $ext1 == "png" || $ext1 == "gif" || $ext1 == "pdf" || $ext1 == "tif" || $ext1 == "tiff"){
			$continue=true;
			$uno++;
		  }
		}
		
		if($array["adjuntos2"]["size"]>0){
		  $ext2 = explode(".",$array["adjuntos2"]["name"]);
		  $ext2 = strtolower($ext2[count($ext2)-1]);
	
		  if($ext2 == "pdf" || $ext2 == "jpg" || $ext2 == "png" || $ext2 == "gif" || $ext2 == "tif" || $ext2 == "tiff"){
			$continue=true;
			$dos++;
		  }
		}
		
		if($array["adjuntos3"]["size"]>0){
		  $ext3 = explode(".",$array["adjuntos3"]["name"]);
		  $ext3 = strtolower($ext3[count($ext3)-1]);
	
		  if($ext3 == "pdf" || $ext3 == "jpg" || $ext3 == "png" || $ext3 == "gif" || $ext3 == "tif" || $ext3 == "tiff"){
			$continue=true;
			$tres++;
		  }
		}
		
		$ok=0;
		
		if($uno>0){
			    $local1 = str_replace(' ','-',trim($array["adjuntos1"]["name"]));// el nombre del archivo	
			    $remoto1 = $array["adjuntos1"]["tmp_name"];// Este es el nombre temporal del archivo mientras dura la transmision	
			    $tama1 = $array["adjuntos1"]["size"];
			    
			    if(!is_dir('app/files/clientes'))
			    	mkdir('app/files/clientes');
			    	
			    if(!is_dir('app/files/clientes/'.$array['numero']))
			    	mkdir('app/files/clientes/'.$array['numero']);

			    $ftp=new ftp();
			    $subir=$ftp->SubirArchivo($local1,$remoto1,$tama1,'app/files/clientes/'.$array['numero']);
			      
				if($subir){
					$ok++;
				}
			  }
			  
		if($dos>0){
			    $local2 = str_replace(' ','-',trim($array["adjuntos2"]["name"]));// el nombre del archivo	
			    $remoto2 = $array["adjuntos2"]["tmp_name"];// Este es el nombre temporal del archivo mientras dura la transmision	
			    $tama2 = $array["adjuntos2"]["size"];
			    
			    if(!is_dir('app/files/clientes'))
			    	mkdir('app/files/clientes');
			    	
			    if(!is_dir('app/files/clientes/'.$array['numero']))
			    	mkdir('app/files/clientes/'.$array['numero']);

			    $ftp=new ftp();	
			    $subir=$ftp->SubirArchivo($local2,$remoto2,$tama2,'app/files/clientes/'.$array['numero']);
			    
			   	if($subir){
					$ok++;
				}
			     
			  }
			  
		if($tres>0){
			    $local2 = str_replace(' ','-',trim($array["adjuntos3"]["name"]));// el nombre del archivo	
			    $remoto2 = $array["adjuntos3"]["tmp_name"];// Este es el nombre temporal del archivo mientras dura la transmision	
			    $tama2 = $array["adjuntos3"]["size"];
			
			
			    if(!is_dir('app/files/clientes'))
			    	mkdir('app/files/clientes');
			    	
			    if(!is_dir('app/files/clientes/'.$array['numero']))
			    	mkdir('app/files/clientes/'.$array['numero']);
			    	
			    	
			    $ftp=new ftp();
			    $subir=$ftp->SubirArchivo($local2,$remoto2,$tama2,'app/files/clientes/'.$array['numero']);
			    
			   	if($subir){
					$ok++;
				}
			     
			  }
			  
			  if($ok>0){
			  	setMensaje('Archivo cargado correctamente','success');
			  }else{
			  	setMensaje('Erro al cargar el archivo','error');
			  }
			  location(setUrl('clientes','lst'));
	}

	
    public function eliminarDoc($array=null){
		//print_r($array);
		unlink("app/files/clientes/".$array['cc'].'/'.$array['folder'].'/'.$array['file']);
		setMensaje('Archivo eliminado correctamente','success');
		location(setUrl('clientes','documentos',array('id'=>$array['id'],'url'=>$array['folder'])));
	}
	
	
    public function eliminarDoc2($array=null, $setUrl = ''){
		//die(print_r($array));
		include 'app/sistema/ftp.php';
		
	    	$info = mClientes::listarId($array['id']);
	    	
	    	$url=empty($array['folder'])?null:$array['folder'].'/';
	    	die(print_r($info[0]));
	    	eliminarDir('app/files/clientes/'.$info[0]['numero']);
	    	
	    	//die('fin');

		setMensaje('Fichero eliminado correctamente','success');

		if(!$setUrl)
			location(setUrl('clientes','documentos',array('id'=>$array['id'])));
		else 
			location($setUrl);
	}
	
	
    public function editCupo($array){
		validar_usuario();
		valId($array['id'], 'clientes');
		$datos = mClientes::listarId($array['id']);
		parent::editCupo($datos[0]);
	}
	
    public function editCupoProcess($array){
		validar_usuario();
		valId($array['id'], 'clientes');
		
		$infoCupo = mClientes::listarId($array['id']);
		$infoCupo = $infoCupo[0];
		$disponible = $array['cupo'] - $infoCupo["cupo_utilizado"];
		
		$upd = mClientes::updCupo(
						    $array['id'],
						    $array['cupo'],
						    $disponible
						    );
		
		if($upd){
		  setMensaje('Cupo correctamente.','success');
		}else{
		  setMensaje('Error al actualizar al cupo','error');
		}
		location(setUrl('clientes','lst'));
	}
	*/
    
    public function ref(){
        validar_usuario();
        $info = mClientes::referencias();
        parent::ref($info);
    }
    
    public function saveRef($array=null){
        validar_usuario();
        $save = mClientes::saveReferencia($array['ref']);
        
        if($save){
            setMensaje('Referencia creada correctamente','success');
        }else{
            setMensaje('Error al crear la referencia','error');
        }
        location(setUrl('clientes','ref'));
    }
    
    public function editRef($array=null){
        validar_usuario();
        $info = mClientes::referenciasId($array['id']);
        parent::editRef($info[0]);
    }
    
    public function updRef($array=null){
        validar_usuario();
        $upd = mClientes::referenciasUpd($array['id'],$array['ref']);
        if($upd){
            setMensaje('Referencia actualizada correctamente','success');
        }else{
            setMensaje('Error al actualizar la referencia','error');
        }
        location(setUrl('clientes','ref'));
    }
    
    public function delRef($array=null){
        validar_usuario();
        $del = mClientes::delRef($array['id']);
        if($del){
            setMensaje('Referencia eliminado correctamente','success');
        }else{
            setMensaje('Error al eliminar la referencia','error');
        }
        location(setUrl('clientes','ref'));
    }
    
    public function vinculos(){
        validar_usuario();
        
        $info = mClientes::referencias();
        $ref=array();
        foreach ($info As $i){
            $ref[$i['id']] = $i['ref'];
        }
        
        $info2 = mClientes::vinculos();
        parent::vinculos($info2,$ref);
    }
    
    public function saveVinculo($array){
        validar_usuario();
        $info = mClientes::saveVinculo($array['ref'],$array['vinculo']);
        if($info){
            setMensaje('Vinculo creado correctamente','success');
        }else{
            setMensaje('Error al crear el Vinculo','error');
        }
        location(setUrl('clientes','vinculos'));
    }
    
    public function editVinc($array=null){
        validar_usuario();
        
        $info = mClientes::vinculosId($array['id']);
        $info2 = mClientes::referencias();
        $ref=array();
        foreach ($info2 As $i){
            $ref[$i['id']] = $i['ref'];
        }
        
        parent::editVinc($info[0],$ref);
    }
    
    public function editVinculo($array=null){
        validar_usuario();
        
        $upd = mClientes::vinculosUpd(
                                        $array['id'],
                                        $array['ref'],
                                        $array['vinculo']
                                        );
        if($upd){
            setMensaje('Vinculo actualizado correctamente','success');
        }else{
            setMensaje('Error al actualizar el vinculo','error');
        }
        location(setUrl('clientes','vinculos'));
    }
    
    public function delVinc($array=null){
        validar_usuario();
        
        $del = mClientes::delVinculo($array['id']);
        if($del){
            setMensaje('Vinculo eliminado correctamente','success');
        }else{
            setMensaje('Error al eliminar el vinculo','error');
        }
        location(setUrl('clientes','vinculos'));
    }
    
    public function convenio(){
        validar_usuario();
        $info = mClientes::lstEmpresa();
        $cadena = '';
        $j=1;
        if(is_array($info)){
            foreach ($info As $i):
                $estado = ($i['convenio']=='0')?'<a href="'.setUrl("clientes","convenir",array('id'=>$i['id'])).'"><img src="app/img/acuerdos.png"></a>':'En convenio';
                $cadena .= '<tr>';
                $cadena .= '<td>'.$i['id'].'</td>';
                $cadena .= '<td>'.$i['nit'].'</td>';
                $cadena .= '<td>'.$i['razon'].'</td>';
                $cadena .= '<td>'.$i['dir'].'</td>';
                $cadena .= '<td>'.$i['tel'].'</td>';
                $cadena .= '<td>'.$estado.'</td>';
                $cadena .= '</tr>';
                $j++;
            endforeach;
        }
        parent::convenio($cadena);
    }
    
    public function convenir($array=null){
        validar_usuario();
        $info = mClientes::lstEmpresa($array["id"]);
        //die(print_r($info[0]));
        parent::convenir($info[0]);
    }
    
    public function convenirpro($array=null){
        validar_usuario();
        
        $fechaI = formatoFecha($array['fechaI']);
        $fechaF = formatoFecha($array['fechaF']);
        
        $save = mClientes::saveConvenio(
                    $fechaI,
                    $fechaF,
                    serialize($array["adjuntos1"]["name"]),
                    $array["id"]
                );
        
        if(!empty($array["adjuntos1"]["name"][0])){
            include_once 'app/sistema/ftp.php';
            $ftp=new ftp();

            if(!is_readable('app/files/convenios/'.$array['id'])){
                //die('entra2');
                mkdir('app/files/convenios/'.$array['id'],0777);
            }

            $total = count($array["adjuntos1"]["name"]);
            for($i = 0; $i < $total; $i++){
                $ext = explode('.',$array['adjuntos1']['name'][$i]);
                $local1 = $array["nadjunto"][$i].'.'.$ext[1];// el nombre del archivo	
                $remoto1 = $array["adjuntos1"]["tmp_name"][$i];// Este es el nombre temporal del archivo mientras dura la transmision	
                $tama1 = $array["adjuntos1"]["size"][$i];
                $subir=$ftp->SubirArchivo($local1,$remoto1,$tama1,'app/files/convenios/'.$array["id"],$array["fechaI"]);
            }
        }
        if($save){
            mClientes::updEstado($array["id"]);
            setMensaje('Convenio creado correctamente','success');
        }else{
            setMensaje('Error al crear el convenio','error');
        }
        location(setUrl('clientes','convenio'));
    }
    
    public function lstConvenios(){
        validar_usuario();
        $info = mClientes::allConvenio();
        parent::lstConvenios($info);
    }
    
    public function docsConv($array=null){
        validar_usuario();
        
    	include 'app/sistema/ftp.php';
    	
    	//$info = mClientes::listarId($array['id']);
    	//$url=empty($array['url'])?null:$array['url'].'/';
    	//echo './app/files/convenios/'.$array['id'];
    	$ftp=new ftp();
    	$ficheros = $ftp->ficheros('./app/files/convenios/'.$array['id'].'/');
        
            parent::docsConv($ficheros,$array['id']);
    }
    
    public function verCliente($array=null){
        validar_usuario();
        
        valId($array['id'], 'clientes');
	$datos = mClientes::listarId($array['id']);
        
        include_once 'app/sistema/ftp.php';
    	        
        $ftp=new ftp();
    	$info1 = $ftp->ficheros('./app/files/clientes/'.$datos[0]['id_cliente'].'/');
        //die('./app/files/clientes/'.$datos[0]['id_cliente'].'/');
        //die(print_r($info1));
        parent::verCliente($datos[0],$info1);
    }
    
    public function verEmpresa($array=null){
        validar_usuario();
        
        valId($array['id'], 'empresas');
	$datos = mClientes::listarIdEmpresa($array['id']);
        
        include_once 'app/sistema/ftp.php';
    	        
        $ftp=new ftp();
    	$info1 = $ftp->ficheros('./app/files/clientes/'.$datos[0]['nit'].'/');
        //die('./app/files/clientes/'.$datos[0]['id_cliente'].'/');
        //die(print_r($info1));
        parent::verEmpresa($datos[0],$info1);
    }
    
    public function editEmpresa($array=null){
        validar_usuario();
        
        valId($array['id'], 'empresas');
	$datos = mClientes::listarIdEmpresa($array['id']);
        
        parent::editEmpresa($datos[0]);
    }
    
    public function editEmpresapro($array=null){
        validar_usuario();
        
        include_once 'app/sistema/ftp.php';
        $total = count($array["adjuntos1"]["name"]);
        
        if(!is_file('app/files/clientes/'.$array['nit'])){
            mkdir('app/files/clientes/'.$array['nit'],0777,TRUE);
        }
        
        if($total>0){
            $ftp=new ftp();
            for($i=0;$i<$total;$i++){
                $local1 = trim($array["adjuntos1"]["name"][$i]);// el nombre del archivo	
                $remoto1 = $array["adjuntos1"]["tmp_name"][$i];// Este es el nombre temporal del archivo mientras dura la transmision	
                $tama1 = $array["adjuntos1"]["size"][$i];
                $ftp->SubirArchivo($local1,$remoto1,$tama1,'app/files/clientes/'.$array['nit'],'cliente');
            }
        }
        //die('jum');
        $ORM = new ORM();
        $ORM->iniTransaccion();
        try{
                mClientes::updEmpresa(
                              $array["nombre"],
                              $array["nit"],
                              $array["direccion"],
                              $array["telefono"],
                              $array["actividadEconomica"],
                              $array["ciiu"],
                              $array["id"]
                              );

                mClientes::updEmpresaRepre(
                        $array["nombreR"],
                        $array["apellidoR"],
                        $array["tipo_doc"],
                        $array["ndocumento"],
                        $array["id"]
                        );

                mClientes::updEmpresaFinan(
                        $array["ingresos"],
                        $array["ingresosNoOperativos"],
                        $array["ingresosDesc"],
                        $array["totalIngresos"],
                        $array["totalEgresos"],
                        $array["totalActivos"],
                        $array["totalPasivos"],
                        $array["bienesFiducia"],
                        $array["descFiducia"],
                        $array["id"]
                        );

                mClientes::updEmpresaAccio(
                        serialize($array["nombreA"]),
                        serialize($array["tipo_docA"]),
                        serialize($array["ndocumentoA"]),
                        serialize($array["participacion"]),
                        $array["id"]
                        );

                mClientes::updEmpresaOper(
                        $array["operacionesIn"],
                        $array["tipoOperacion"],
                        $array["productoFinanciero"],
                        $array["obanco"],
                        $array["numeroCuenta"],
                        $array["moneda"],
                        $array["monto"],
                        $array["ciudad"],
                        $array["id"]
                        );

                mClientes::updEmpresaBanco(
                        $array["cuentauno"],
                        $array["cuenta"],
                        $array["banco1"],
                        $array["cuentados"],
                        $array["cuenta2"],
                        $array["banco2"],
                        $array["id"]
                        );

                /*procesar querys*/
                 $ORM->commit();

                 setMensaje('Empresa creada correctamente.','success');
        }catch (Exception $e){
            $e->getMessage();
            $ORM->rollback();
            setMensaje('Error al crear el cliente','error');
        }  
    location(setUrl('clientes','lstEmpresa'));
    }
    
} // class
