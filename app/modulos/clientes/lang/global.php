<?php 
/*Variables globales para el modulo lang*/

define('CLIENTES_NOMBRE','Clientes');
define('CLIENTES_CREAR','Crear PN');
define('CLIENTES_CREAR_EMPRESA','Crear PJ');
define('CLIENTES_CREAR_EMPRESA_VER','Ver Empresa');
define('CLIENTES_CREAR_EMPRESA_EDITAR','Editar Empresa');
define('CLIENTES_IMPORT','Importar Clientes');
define('CLIENTES_EDITAR','Editar Clientes');
define('CLIENTES_VER','Ver Cliente');
define('CLIENTES_FICHA','Lista de chequeo');
define('CLIENTES_LISTAR','Lista de clientes PN');
define('CLIENTES_LISTAR_EMP','Lista de clientes PJ');
define('CLIENTES_DOC','Documentos');
define('CLIENTES_DOC2','Cargar documentos');
define('CLIENTES_REFERENCIA','Crear convenios');
define('CLIENTES_CONVENIOS','Lista convenios');
define('CLIENTES_VINCULOS','Vinculos');
define('CLIENTES_VINCULOS_CREAR','Crear Vinculos');
define('CLIENTES_VINCULOS_EDITAR','Editar Vinculos');
define('CLIENTES_REFERENCIA_CREAR','Crear Referencia');
define('CLIENTES_REFERENCIA_EDITAR','Editar Referencia');
