<?php 
class mClientes{
  
    
    public function trans(){
        $orm = new ORM();
        return $orm->transaccion();
    }
    
    public function commit(){
        $orm = new ORM();
        return $orm->commit();
    }
    
    public function rollback(){
        $orm = new ORM();
        return $orm->rollback();
    }

    public function saveCliente(
                                $tipo_doc,
                                $ndocumento,
                                $nombre,
                                $apellido,
                                $fechaNaci,
                                $correo,
                                $ciudad,
                                $barrio,
                                $telefono,
                                $celular,
                                $direccion,
                                $dir2,
                                $sucursal,
                                $tipo_cli
                            ){
	  $orm = new ORM();
	  $sql = $orm	-> insert("op_clientes")
                        -> values(
                                  array(
                                      $ndocumento,
                                      $tipo_doc,
                                      $nombre,
                                      $apellido,
                                      $fechaNaci,
                                      $correo,
                                      $ciudad,
                                      $barrio,
                                      $telefono,
                                      $celular,
                                      $direccion,
                                      $dir2,
                                      $sucursal,
                                      1,
                                      $tipo_cli
                                      )
                         );
	  //die($sql->verSql());
	  $return = $sql	-> exec()
					  -> afectedRows();
	  return $return > 0 ? true : false;

  }
  
   public function saveClienteLaboral(
                            $empresa,
                            $cargo,
                            $dirempresa,
                            $telempresa,
                            $actividadEconomica,
                            $ciiu,
                            $id
                            ){
	  $orm = new ORM();
	  $sql = $orm	-> insert("op_clientes_laboral")
                        -> values(
                            array(
                              0,
                              $empresa,
                              $cargo,
                              $dirempresa,
                              $telempresa,
                              $actividadEconomica,
                              $ciiu,
                              $id
                                )
                         );
	  //die($sql->verSql());
	  $return = $sql	-> exec()
					  -> afectedRows();
	  return $return > 0 ? true : false;

  }
  
  public function saveClienteFinanciera(
                            $ingresoMensual,
                            $otroMensual,
                            $otroDesc,
                            $totalIngresos,
                            $totalEgresos,
                            $totalActivos,
                            $totalPasivos,
                            $bienesFiducia,
                            $descFiducia,
                            $cuentauno,
                            $cuenta,
                            $banco1,
                            $cuentados,
                            $cuenta2,
                            $banco2,
                            $id
                            ){
	  $orm = new ORM();
	  $sql = $orm	-> insert("op_clientes_banco")
                        -> values(
                            array(
                              0,
                              $ingresoMensual,
                            $otroMensual,
                            $otroDesc,
                            $totalIngresos,
                            $totalEgresos,
                            $totalActivos,
                            $totalPasivos,
                            $bienesFiducia,
                            $descFiducia,
                            $cuentauno,
                            $cuenta,
                            $banco1,
                            $cuentados,
                            $cuenta2,
                            $banco2,
                            $id
                                )
                         );
	  //die($sql->verSql());
	  $return = $sql	-> exec()
					  -> afectedRows();
	  return $return > 0 ? true : false;

  }
  
  public function listar($estado = ''){
	  $orm = new ORM();
	  if($estado != ""){
		  $sql = $orm	-> select()
				-> from("op_clientes")
				-> where('estado=?','1');
          }else{
	  	$sql = $orm	-> select()
          			-> from("op_clientes");
				// ->where('estado=?',$estado);
          }
	  $return = $sql-> exec()->fetchArray();
	  return $return;
  } // listar
	
  public function listarId($id){
	  $orm = new ORM();
	  $sql = $orm	-> select(array('personal.*','labo.*','finan.*'))
                        -> from("op_clientes","personal")
                        ->join(array('labo'=>'op_clientes_laboral'),'labo.id_cliente=personal.id')
                        ->join(array('finan'=>'op_clientes_banco'),'finan.id_cliente=personal.id')
                        -> where('personal.id =?',$id);
	  $return = $sql-> exec() -> fetchArray();
	  return $return;
  } // listar
  
public function listarId2($id){
    $orm = new ORM();
    $sql = $orm	-> select()
			-> from("op_clientes")
			-> where('numero =?',$id);
    $return = $sql	-> exec()-> fetchArray();
    return $return;
} // listar
  
  public function updCliente(
                                $id,
                                $tipo_doc,
                                $nombre,
                                $apellido,
                                $fechaNaci,
                                $correo,
                                $ciudad,
                                $barrio,
                                $telefono,
                                $celular,
                                $direccion,
                                $dir2
				     ){
    $orm = new ORM();
	  $sql = $orm	-> update("op_clientes")
				  -> set(
					    array(
						"tipo_doc"	=> $tipo_doc,
						"nombre"	=> $nombre,
						"apellido"	=> $apellido,
                                                "fechaNaci"     => $fechaNaci,
                                                "correo"        => $correo,
                                                "ciudad"        => $ciudad,
                                                "barrio"        => $barrio,
						"telefono"	=> $telefono,
						"celular"	=> $celular,
                                                "dir"		=> $direccion,
						"dir2"          => $dir2
					    )
					  )
				  -> where("id = ?", $id);
				  
	   // die($sql->verSql());
    $sql -> exec();
	  $retorno = $orm -> afectedRows() > 0 ? true : false;
    return $retorno;
  }//updProducto
  
  public function updClienteLaboral(
                                $id,
                                $empresa,
                                $cargo,
                                $dirempresa,
                                $telempresa,
                                $actividad,
                                $ciiu
				     ){
    $orm = new ORM();
	  $sql = $orm	-> update("op_clientes_laboral")
                        -> set(
                            array(
                                'empresa'   =>$empresa,
                                'cargo'     =>$cargo,
                                'dirEmpresa'=>$dirempresa,
                                'telempresa'=>$telempresa,
                                'actividad' =>$actividad,
                                'ciiu'      =>$ciiu
                            )
                                )
                        -> where("id_cliente = ?", $id);
				  
	   // die($sql->verSql());
    $sql -> exec();
    return  $retorno = $orm -> afectedRows() > 0 ? true : false;
    
  }
  
  public function updClienteBancos(
                                $id,
                                $ingresoMensual,
                                $otroMensual,
                                $otroDesc,
                                $totalIngresos,
                                $totalEgresos,
                                $totalActivos,
                                $totalPasivos,
                                $bienesFiducia,
                                $descFiducia,
                                $cuentauno,
                                $cuenta,
                                $banco1,
                                $cuentados,
                                $cuenta2,
                                $banco2
				     ){
    $orm = new ORM();
	  $sql = $orm	-> update("op_clientes_banco")
                        -> set(
                            array(
                                'ingresos'  =>  $ingresoMensual,
                                'otrosIn'=>$otroMensual,
                                'ingresosDesc'=>$otroDesc,
                                'totalingresos'=>$totalIngresos,
                                'totalegresos'=>$totalEgresos,
                                'totalactivos'=>$totalActivos,
                                'totalpasivos'=>$totalPasivos,
                                'fiducia'=>$bienesFiducia,
                                'descFiducia'=>$descFiducia,
                                'cuenta1'=>$cuentauno,
                                'tipo1'=>$cuenta,
                                'numero1'=>$banco1,
                                'cuenta2'=>$cuentados,
                                'tipo2'=>$cuenta2,
                                'numero2'=>$banco2
                            )
                                )
                        -> where("id_cliente = ?", $id);
				  
	   // die($sql->verSql());
    $sql -> exec();
    return  $retorno = $orm -> afectedRows() > 0 ? true : false;
    
  }//updProducto
  
  /*
,
                                
   * 
   *    ,
						"dirEmpresa"    => $dirempresa,
						"telEmpresa"    => $telempresa,
						"salario"       => $salario,
						"fechaIn"       => $fechaini,
						"fechaFin"      => $fechafin,
						"cuenta1"       => $cuentauno,
						"tipo1"         => $cuenta,
						"numero1"       => $banco1,
						"cuenta2"       => $cuentados,
						"tipo2"         => $cuenta2,
						"numero2"       => $banco2,
						"sucursal"      => $sucursal
      */
  
  public function updCliente2(
			$id,
			$cupo  ){
    $orm = new ORM();
	  $sql = $orm	-> update("op_clientes")
				  -> set(
					    array(
						"cupo"		=>$cupo,
					    )
					  )
				  -> where("id = ?", $id);
				  
	   // die($sql->verSql());
    $orm -> exec();
	  $retorno = $orm -> afectedRows() > 0 ? true : false;
    return $retorno;
  }//updProducto
  
  public function delCliente($id){
    $orm = new ORM();
	  $sql = $orm -> update("op_clientes")
	  			->set(
	  				array('estado'=>'0')
	  			)
				  -> where("id = ? ", $id);
	  $orm -> exec();
	  $retorno = $orm -> afectedRows() > 0 ? true : false;
    return $retorno;
  }
  
  public function updClienteMonto(
					$id,
					$cupodis,
					$cupouti
				     ){
    $orm = new ORM();
	  $sql = $orm	-> update("op_clientes")
				  -> set(
					    array(
						"cupo_disponible"	=>$cupodis,
						"cupo_utilizado"	=>$cupouti
					    )
					  )
				  -> where("numero = ?", $id);
	   //die($sql->verSql());
	  $orm -> exec();
	  $retorno = $orm -> afectedRows() > 0 ? true : false;
    return $retorno;
  }//updProducto
  
  
  public function updCupo($id, $cupo, $disponible){
  	$orm = new ORM();
	  $sql = $orm	-> update("op_clientes")
				  -> set(
					    array(
						"cupo"	=>$cupo,
						"cupo_disponible" => $disponible
					    )
					  )
				  -> where("id = ?", $id);
	   //die($sql->verSql());
	  $orm -> exec();
	  $retorno = $orm -> afectedRows() > 0 ? true : false;
    return $retorno;
  }
  
  public function saveReferencia($ref){
	  $orm = new ORM();
	  $sql = $orm	-> insert("op_clientes_ref")
				  -> values(
                                        array(0,$ref,opDate())
                                    );
	  $return = $sql	-> exec()
                                -> afectedRows();
	  return $return > 0 ? true : false;

  }
  
  public function referencias(){
    $orm = new ORM();
    $sql = $orm	-> select()
			-> from("op_clientes_ref");
    $return = $sql	-> exec()-> fetchArray();
    return $return;
} // listar

  public function referenciasId($id){
    $orm = new ORM();
    $sql = $orm	-> select()
			-> from("op_clientes_ref")
                        -> where('id=?',$id);
    $return = $sql	-> exec()-> fetchArray();
    return $return;
} // listar

  public function referenciasUpd($id,$ref){
    $orm = new ORM();
    $sql = $orm	-> update("op_clientes_ref")
				  -> set(
					    array(
						"ref"	=>$ref
					    )
					  )
				  -> where("id = ?", $id);

	  $retorno = $sql -> exec() -> afectedRows() > 0 ? true : false;
    return $retorno;
} // referenciasUpd

    public function delRef($id){
        $orm = new ORM();
        $orm	-> delete()
                -> from("op_clientes_ref")
                -> where("id=?",$id);		
        $return = $orm->exec();
        return $return;
    }
    
      public function saveVinculo($ref,$vinculo){
	  $orm = new ORM();
	  $sql = $orm	-> insert("op_clientes_vinculos")
				  -> values(
                                        array(0,$ref,$vinculo,opDate())
                                    );
	  $return = $sql	-> exec()
                                -> afectedRows();
	  return $return > 0 ? true : false;

  }
  
    public function vinculos(){
    	$orm = new ORM();
        $query=$orm->select(array("vin.*","vin.ref as laref","vin.id as elid","ref.*"))
                           ->from("op_clientes_vinculos","vin")
                           ->join(array("ref"=>"op_clientes_ref"),'vin.ref = ref.id',"INNER");
        $return=$query->exec()->fetchArray(); 
		
        return $return;
    } // vinculos	
    
    public function vinculosId($id){
    	$orm = new ORM();
        $query=$orm->select(array("vin.*","vin.ref as laref","vin.id as elid","ref.*"))
                           ->from("op_clientes_vinculos","vin")
                           ->join(array("ref"=>"op_clientes_ref"),'vin.ref = ref.id',"INNER")
                           ->where('vin.id=?',$id);
        $return=$query->exec()->fetchArray(); 
		
        return $return;
    } // vinculosId
    
    public function vinculosUpd($id,$ref,$vinculo){
        $orm = new ORM();
        $sql = $orm	-> update("op_clientes_vinculos")
				  -> set(
					    array(
						"ref"   =>$ref,
						"vinculo"   =>$vinculo
					    )
					  )
				  -> where("id = ?", $id);

	  $retorno = $sql -> exec() -> afectedRows() > 0 ? true : false;
        return $retorno;
    } // referenciasUpd
  
    public function delVinculo($id){
        $orm = new ORM();
        $orm	-> delete()
                -> from("op_clientes_vinculos")
                -> where("id=?",$id);		
        $return = $orm->exec();
        return $return;
    }
    
    public function lstEmpresa($id=null){
        $orm = new ORM();
        $orm    -> select()
                -> from ("op_clientes")
                ->where("tipo_cli=?",2);
        if(!empty($id)){
            $orm->where("id=?",$id);
        }
        return $orm -> exec() -> fetchArray();
    }
    
    public function saveConvenio(
                                $fechaI,
                                $fechaF,
                                $adjunto,
                                $id
                            ){
	  $orm = new ORM();
	  $sql = $orm	-> insert("op_convenio")
				  -> values(
                                        array(
                                            0,
                                            $id,
                                            $fechaI,
                                            $fechaF,
                                            $adjunto
                                            )
                                    );
	  //die($sql->verSql());
	  $return = $sql-> exec()-> afectedRows();
	  return $return > 0 ? true : false;
    }
    
        public function allConvenio($id=null){
        $orm = new ORM();
        $orm    -> select(array("conv.*","cli.*"))
                -> from ("op_convenio","conv")
                ->join(array("cli"=>"op_clientes"),'cli.id = conv.id_empresa',"INNER");
        if(!empty($id)){
            $orm->where("id=?",$id);
        }
        //die($orm->verSql());
        return $orm -> exec() -> fetchArray();
    }
    
    public function updEstado($id,$estado){
  	$orm = new ORM();
	  $sql = $orm	-> update("op_clientes")
				  -> set(
					    array(
						"estado" =>$estado
					    )
					  )
				  -> where("id = ?", $id);
	  $retorno = $sql -> exec() -> afectedRows() > 0 ? true : false;
    return $retorno;
  }
  
  public function saveEmpresa(
                            $nombre,
                            $nit,
                            $direccion,
                            $telefono,
                            $actividadEconomica,
                            $ciiu
                            ){
	  $orm = new ORM();
	  $sql = $orm	-> insert("op_empresa")
                        -> values(
                                  array(
                                        0,
                                        0,
                                        getSucursal(),
                                        $nombre,
                                        $nit,
                                        $direccion,
                                        $telefono,
                                        $actividadEconomica,
                                        $ciiu,
                                        opDate()
                                      )
                         );
	  //die($sql->verSql());
	  $return = $sql	-> exec()
					  -> afectedRows();
	  return $return > 0 ? true : false;

  }
    
} // class