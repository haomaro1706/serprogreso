/*javascript para el modulo js/*/
$(document).ready(function(){
  $("#clientes").validate({
     rules:{
        tipo_doc: {required:true},
        ndocumento: {required:true,number:true,minlength: 7, maxlength: 15},
        nombre: {required:true, lettersonly: true, minlength: 3, maxlength: 30,number:false},
        apellido: {required:true, lettersonly: true, minlength: 3, maxlength: 30},
        cupo: {required:true,number:true,min:0},
        cupo_disponible: {required:true,number:true},
        cupo_utilizado: {required:true,number:true},
        direccion: {required:true,minlength: 4},
        telefono: {number:true,minlength: 7, maxlength: 9},
        telefono2: {number:true,minlength: 7, maxlength: 9},
        celular: {required:true,number:true,minlength: 10, maxlength: 12},
        fechaNaci: {required:true},
        estado: {required:true},
        
     }
  });
  
  $("#solicitudes").validate({
     rules:{
        adjuntos1: {accept: "jpg|jpeg|png|ico|bmp|pdf|tif|tiff"},
        adjuntos2: {accept: "jpg|jpeg|png|ico|bmp|pdf|tif|tiff"},
        adjuntos3: {accept: "jpg|jpeg|png|ico|bmp|pdf|tif|tiff"}
     }
  });
    
    $("#cupo").keyup(function(){
	$("#cupo_disponible").val($(this).val());
	$("#cupo_utilizado").val('0');
    });
   
     $("#editar_clientes").validate({
     rules:{
        tipo_doc: {required:true},
        ndocumento: {required:true,number:true,minlength: 7, maxlength: 15},
        nombre: {required:true, lettersonly: true, minlength: 3, maxlength: 30,number:false},
        apellido: {required:true, lettersonly: true, minlength: 3, maxlength: 30},
        cupo: {required:true,number:true,min:0},
        cupo_disponible: {required:true,number:true},
        cupo_utilizado: {required:true,number:true},
        direccion: {required:true,minlength: 4},
        telefono: {required:true,number:true,minlength: 7, maxlength: 9},
        telefono2: {number:true,minlength: 7, maxlength: 9},
        celular: {required:true,number:true,minlength: 10, maxlength: 12},
        fechaNaci: {required:true},
        estado: {required:true},
        
     }
  });
  
    $("#editar_cupo").validate({
     rules:{
        	 cupo: {required:true,number:true,min:0}
	}
    });
    
    $("#tipo_cli").change(function(){
        tipo = parseInt($(this).val());
        
        if(tipo===2){
            $("#tipo_doc2").hide();
            
            $("#ndocumento2").text("NIT *: ");
            $("#ndocumento").attr("placeholder", "NIT");
            
            $("#nombre2").text("Razón Social *: ");
            $("#nombre").attr("placeholder", "Razón Social");
            
            $("#ver_apellido").hide();
            $("#fechaNaci2").hide();
            
            $("#ipersonal").text("Información de contacto");
            
            $("#dirempresa2").text("Nombre contacto *: ");
            $("#dirempresa2").attr("placeholder", "Nombre contacto");
            
            $("#telempresa2").text("Telefono contacto *: ");
            $("#telempresa2").attr("placeholder", "Telefono contacto");
            
            $("#salario2").text("Celular contacto *: ");
            $("#salario2").attr("placeholder", "Celular contacto");
            
            $("#fechas").hide();
        }else{
            $("#tipo_doc2").show();
            
            $("#ndocumento2").text("Numero Identificacion * :");
            $("#ndocumento").attr("placeholder", "Numero Identificacion");
            
            $("#nombre2").text("Nombre *: ");
            $("#nombre").attr("placeholder", "Nombre");
            
            $("#ver_apellido").show();
            $("#fechaNaci2").show();
            
            $("#ipersonal").text("Información Personal");
            
            $("#dirempresa2").text("Dirección empresa *: ");
            $("#dirempresa2").attr("placeholder", "Dirección empresa");
            
            $("#telempresa2").text("Telefono empresa *: ");
            $("#telempresa2").attr("placeholder", "Telefono empresa");
            
            $("#salario2").text("Salario basico *: ");
            $("#salario2").attr("placeholder", "Salario basico");
            
            $("#fechas").show();
        }
        
    });
    
    $("#mas").click(function(){
        var html2 = $('#nuevo').html();
        var clonar = html2;
        var clone2 = $("#adjunto").clone();
        //alert(html2);
        cont = parseInt($("#cont").html())+1;
        clone2.attr('id', cont);
        
        $("#cont").html(cont);
        $('#nuevo').html(clonar+clone2);
    });
    
});
