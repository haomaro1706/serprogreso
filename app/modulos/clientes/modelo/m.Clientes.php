<?php 
class mClientes{
  
    
    public static function trans(){
        $orm = new ORM();
        return $orm->transaccion();
    }
    
    public static function commit(){
        $orm = new ORM();
        return $orm->commit();
    }
    
    public static function rollback(){
        $orm = new ORM();
        return $orm->rollback();
    }

    public static function saveCliente(
                                $tipo_doc,
                                $ndocumento,
                                $nombre,
                                $apellido,
                                $fechaNaci,
                                $correo,
                                $ciudad,
                                $barrio,
                                $telefono,
                                $celular,
                                $direccion,
                                $dir2,
                                $sucursal,
                                $tipo_cli
                            ){
	  $orm = new ORM();
	  $sql = $orm	-> insert("op_clientes")
                        -> values(
                                  array(
                                      $ndocumento,
                                      $tipo_doc,
                                      $nombre,
                                      $apellido,
                                      $fechaNaci,
                                      $correo,
                                      $ciudad,
                                      $barrio,
                                      $telefono,
                                      $celular,
                                      $direccion,
                                      $dir2,
                                      $sucursal,
                                      1,
                                      $tipo_cli
                                      )
                         );
	  //die($sql->verSql());
	  $return = $sql	-> exec()
					  -> afectedRows();
	  return $return > 0 ? true : false;

  }
  
   public static function saveClienteLaboral(
                            $empresa,
                            $cargo,
                            $dirempresa,
                            $telempresa,
                            $actividadEconomica,
                            $ciiu,
                            $id
                            ){
	  $orm = new ORM();
	  $sql = $orm	-> insert("op_clientes_laboral")
                        -> values(
                            array(
                              0,
                              $empresa,
                              $cargo,
                              $dirempresa,
                              $telempresa,
                              $actividadEconomica,
                              $ciiu,
                              $id
                                )
                         );
	  //die($sql->verSql());
	  $return = $sql	-> exec()
					  -> afectedRows();
	  return $return > 0 ? true : false;

  }
  
  public static function saveClienteFinanciera(
                            $ingresoMensual,
                            $otroMensual,
                            $otroDesc,
                            $totalIngresos,
                            $totalEgresos,
                            $totalActivos,
                            $totalPasivos,
                            $bienesFiducia,
                            $descFiducia,
                            $cuentauno,
                            $cuenta,
                            $banco1,
                            $cuentados,
                            $cuenta2,
                            $banco2,
                            $id
                            ){
	  $orm = new ORM();
	  $sql = $orm	-> insert("op_clientes_banco")
                        -> values(
                            array(
                              0,
                              $ingresoMensual,
                            $otroMensual,
                            $otroDesc,
                            $totalIngresos,
                            $totalEgresos,
                            $totalActivos,
                            $totalPasivos,
                            $bienesFiducia,
                            $descFiducia,
                            $cuentauno,
                            $cuenta,
                            $banco1,
                            $cuentados,
                            $cuenta2,
                            $banco2,
                            $id
                                )
                         );
	  //die($sql->verSql());
	  $return = $sql	-> exec()
					  -> afectedRows();
	  return $return > 0 ? true : false;

  }
  
  public static function saveEmpresaBanco(
                            $cuentauno,
                            $cuenta,
                            $banco1,
                            $cuentados,
                            $cuenta2,
                            $banco2,
                            $id
                            ){
	  $orm = new ORM();
	  $sql = $orm	-> insert("op_empresas_banco")
                        -> values(
                            array(
                            0,
                            $cuentauno,
                            $cuenta,
                            $banco1,
                            $cuentados,
                            $cuenta2,
                            $banco2,
                            $id
                                )
                         );
	  //die($sql->verSql());
	  $return = $sql	-> exec()
					  -> afectedRows();
	  return $return > 0 ? true : false;

  }
  
  public static function updEmpresaBanco(
                            $cuentauno,
                            $cuenta,
                            $banco1,
                            $cuentados,
                            $cuenta2,
                            $banco2,
                            $id
                            ){
	  $orm = new ORM();
	  $sql = $orm	->update("op_empresas_banco")
                        ->set(
                            array(
                            "cuenta1"=>$cuentauno,
                            "tipo1"=>$cuenta,
                            "numero1"=>$banco1,
                            "cuenta2"=>$cuentados,
                            "tipo2"=>$cuenta2,
                            "numero2"=>$banco2
                            )
                         )->where("id_cliente = ?", $id);
	  //die($sql->verSql());
	  $return = $sql	-> exec()
					  -> afectedRows();
	  return $return > 0 ? true : false;

  }
  
  public static function listar($estado = ''){
	  $orm = new ORM();
	  if($estado != ""){
		  $sql = $orm	-> select()
				-> from("op_clientes")
				-> where('estado=?','1');
          }else{
	  	$sql = $orm	-> select()
          			-> from("op_clientes");
				// ->where('estado=?',$estado);
          }
	  $return = $sql-> exec()->fetchArray();
	  return $return;
  } // listar
  
public static function listarEmpresa(){
    $orm = new ORM();
    $sql = $orm-> select()-> from("op_empresas")-> where('convenio<?','2');
    $return = $sql-> exec()->fetchArray();
    return $return;
} // listarEmpresa
  

public static function listar2(){
    $orm = new ORM();

    $sql = $orm-> select()
            -> from("op_usuarios");

    $return = $sql-> exec()->fetchArray();
    return $return;
} // listar
	
  public static function listarId($id){
	  $orm = new ORM();
	  $sql = $orm	-> select(array('personal.*','labo.*','finan.*'))
                        -> from("op_clientes","personal")
                        ->join(array('labo'=>'op_clientes_laboral'),'labo.id_cliente=personal.id')
                        ->join(array('finan'=>'op_clientes_banco'),'finan.id_cliente=personal.id')
                        -> where('personal.id =?',$id);
	  $return = $sql-> exec() -> fetchArray();
	  return $return;
  } // listar
  
public static function listarIdEmpresa($id){
    $orm = new ORM();
    $sql = $orm	-> select(array('empre.*','repre.*','repre.tipo_doc as eldoc','finan.*','accion.*','inter.*','banco.*'))
                  -> from("op_empresas","empre")
                  ->join(array('repre'=>'op_empresas_representante'),'empre.nit=repre.empresa')
                  ->join(array('finan'=>'op_empresas_finanzas'),'finan.empresa=empre.nit')
                  ->join(array('accion'=>'op_empresas_acciones'),'accion.empresa=empre.nit')
                  ->join(array('inter'=>'op_empresas_inter'),'inter.empresa=empre.nit')
                  ->join(array('banco'=>'op_empresas_banco'),'banco.id_cliente=empre.nit')
                  -> where('empre.id =?',$id);
    $return = $sql-> exec() -> fetchArray();
    return $return;
} // listar
  
public static function listarId2($id){
    $orm = new ORM();
    $sql = $orm	-> select()
			-> from("vista_clientes","cli")
                        ->join(array("cupo"=>"op_clientes_cupo"),"cli.id=cupo.cliente")
			-> where('cli.id =?',$id);
    $return = $sql	-> exec()-> fetchArray();
    return $return;
} // listar
  
  public static function updCliente(
                                $id,
                                $tipo_doc,
                                $nombre,
                                $apellido,
                                $fechaNaci,
                                $correo,
                                $ciudad,
                                $barrio,
                                $telefono,
                                $celular,
                                $direccion,
                                $dir2
				     ){
    $orm = new ORM();
	  $sql = $orm	-> update("op_clientes")
				  -> set(
					    array(
						"tipo_doc"	=> $tipo_doc,
						"nombre"	=> $nombre,
						"apellido"	=> $apellido,
                                                "fechaNaci"     => $fechaNaci,
                                                "correo"        => $correo,
                                                "ciudad"        => $ciudad,
                                                "barrio"        => $barrio,
						"telefono"	=> $telefono,
						"celular"	=> $celular,
                                                "dir"		=> $direccion,
						"dir2"          => $dir2
					    )
					  )
				  -> where("id = ?", $id);
				  
	   // die($sql->verSql());
    $sql -> exec();
	  $retorno = $orm -> afectedRows() > 0 ? true : false;
    return $retorno;
  }//updProducto
  
  public static function updClienteLaboral(
                                $id,
                                $empresa,
                                $cargo,
                                $dirempresa,
                                $telempresa,
                                $actividad,
                                $ciiu
				     ){
    $orm = new ORM();
	  $sql = $orm	-> update("op_clientes_laboral")
                        -> set(
                            array(
                                'empresa'   =>$empresa,
                                'cargo'     =>$cargo,
                                'dirEmpresa'=>$dirempresa,
                                'telempresa'=>$telempresa,
                                'actividad' =>$actividad,
                                'ciiu'      =>$ciiu
                            )
                                )
                        -> where("id_cliente = ?", $id);
				  
	   // die($sql->verSql());
    $sql -> exec();
    return  $retorno = $orm -> afectedRows() > 0 ? true : false;
    
  }
  
  public static function updClienteBancos(
                                $id,
                                $ingresoMensual,
                                $otroMensual,
                                $otroDesc,
                                $totalIngresos,
                                $totalEgresos,
                                $totalActivos,
                                $totalPasivos,
                                $bienesFiducia,
                                $descFiducia,
                                $cuentauno,
                                $cuenta,
                                $banco1,
                                $cuentados,
                                $cuenta2,
                                $banco2
				     ){
    $orm = new ORM();
	  $sql = $orm	-> update("op_clientes_banco")
                        -> set(
                            array(
                                'ingresos'  =>  $ingresoMensual,
                                'otrosIn'=>$otroMensual,
                                'ingresosDesc'=>$otroDesc,
                                'totalingresos'=>$totalIngresos,
                                'totalegresos'=>$totalEgresos,
                                'totalactivos'=>$totalActivos,
                                'totalpasivos'=>$totalPasivos,
                                'fiducia'=>$bienesFiducia,
                                'descFiducia'=>$descFiducia,
                                'cuenta1'=>$cuentauno,
                                'tipo1'=>$cuenta,
                                'numero1'=>$banco1,
                                'cuenta2'=>$cuentados,
                                'tipo2'=>$cuenta2,
                                'numero2'=>$banco2
                            )
                                )
                        -> where("id_cliente = ?", $id);
				  
	   // die($sql->verSql());
    $sql -> exec();
    return  $retorno = $orm -> afectedRows() > 0 ? true : false;
    
  }//updProducto
  
  /*
,
                                
   * 
   *    ,
						"dirEmpresa"    => $dirempresa,
						"telEmpresa"    => $telempresa,
						"salario"       => $salario,
						"fechaIn"       => $fechaini,
						"fechaFin"      => $fechafin,
						"cuenta1"       => $cuentauno,
						"tipo1"         => $cuenta,
						"numero1"       => $banco1,
						"cuenta2"       => $cuentados,
						"tipo2"         => $cuenta2,
						"numero2"       => $banco2,
						"sucursal"      => $sucursal
      */
  
  public static function updCliente2(
			$id,
			$cupo  ){
    $orm = new ORM();
	  $sql = $orm	-> update("op_clientes")
				  -> set(
					    array(
						"cupo"		=>$cupo,
					    )
					  )
				  -> where("id = ?", $id);
				  
	   // die($sql->verSql());
    $orm -> exec();
	  $retorno = $orm -> afectedRows() > 0 ? true : false;
    return $retorno;
  }//updProducto
  
  public static function delCliente($id){
    $orm = new ORM();
	  $sql = $orm -> update("op_clientes")
	  			->set(
	  				array('estado'=>'0')
	  			)
				  -> where("id = ? ", $id);
	  $orm -> exec();
	  $retorno = $orm -> afectedRows() > 0 ? true : false;
    return $retorno;
  }
  
  public static function delEmpresa($id){
    $orm = new ORM();
	  $sql = $orm   -> delete()
                        -> from("op_empresas")
	  		-> where("id = ? ", $id);
	  $orm -> exec();
	  $retorno = $orm -> afectedRows() > 0 ? true : false;
    return $retorno;
  }
  
  public static function updClienteMonto(
					$id,
					$cupodis,
					$cupouti
				     ){
    $orm = new ORM();
	  $sql = $orm	-> update("op_clientes_cupo")
                        -> set(
                                  array(
                                      "cupo_disponible"	=>$cupodis,
                                      "cupo_utilizado"	=>$cupouti
                                  )
                                )
                        -> where("cliente = ?", $id);
	   //die($sql->verSql());
	  $orm -> exec();
	  $retorno = $orm -> afectedRows() > 0 ? true : false;
    return $retorno;
  }//updProducto
  
  
  public static function updCupo($id, $cupo, $disponible){
  	$orm = new ORM();
	  $sql = $orm	-> update("op_clientes")
				  -> set(
					    array(
						"cupo"	=>$cupo,
						"cupo_disponible" => $disponible
					    )
					  )
				  -> where("id = ?", $id);
	   //die($sql->verSql());
	  $orm -> exec();
	  $retorno = $orm -> afectedRows() > 0 ? true : false;
    return $retorno;
  }
  
  public static function saveReferencia($ref){
	  $orm = new ORM();
	  $sql = $orm	-> insert("op_clientes_ref")
				  -> values(
                                        array(0,$ref,opDate())
                                    );
	  $return = $sql	-> exec()
                                -> afectedRows();
	  return $return > 0 ? true : false;

  }
  
  public static function referencias(){
    $orm = new ORM();
    $sql = $orm	-> select()
			-> from("op_clientes_ref");
    $return = $sql	-> exec()-> fetchArray();
    return $return;
} // listar

  public static function referenciasId($id){
    $orm = new ORM();
    $sql = $orm	-> select()
			-> from("op_clientes_ref")
                        -> where('id=?',$id);
    $return = $sql	-> exec()-> fetchArray();
    return $return;
} // listar

  public static function referenciasUpd($id,$ref){
    $orm = new ORM();
    $sql = $orm	-> update("op_clientes_ref")
				  -> set(
					    array(
						"ref"	=>$ref
					    )
					  )
				  -> where("id = ?", $id);

	  $retorno = $sql -> exec() -> afectedRows() > 0 ? true : false;
    return $retorno;
} // referenciasUpd

    public static function delRef($id){
        $orm = new ORM();
        $orm	-> delete()
                -> from("op_clientes_ref")
                -> where("id=?",$id);		
        $return = $orm->exec();
        return $return;
    }
    
      public static function saveVinculo($ref,$vinculo){
	  $orm = new ORM();
	  $sql = $orm	-> insert("op_clientes_vinculos")
				  -> values(
                                        array(0,$ref,$vinculo,opDate())
                                    );
	  $return = $sql	-> exec()
                                -> afectedRows();
	  return $return > 0 ? true : false;

  }
  
    public static function vinculos(){
    	$orm = new ORM();
        $query=$orm->select(array("vin.*","vin.ref as laref","vin.id as elid","ref.*"))
                           ->from("op_clientes_vinculos","vin")
                           ->join(array("ref"=>"op_clientes_ref"),'vin.ref = ref.id',"INNER");
        $return=$query->exec()->fetchArray(); 
		
        return $return;
    } // vinculos	
    
    public static function vinculosId($id){
    	$orm = new ORM();
        $query=$orm->select(array("vin.*","vin.ref as laref","vin.id as elid","ref.*"))
                           ->from("op_clientes_vinculos","vin")
                           ->join(array("ref"=>"op_clientes_ref"),'vin.ref = ref.id',"INNER")
                           ->where('vin.id=?',$id);
        $return=$query->exec()->fetchArray(); 
		
        return $return;
    } // vinculosId
    
    public static function vinculosUpd($id,$ref,$vinculo){
        $orm = new ORM();
        $sql = $orm	-> update("op_clientes_vinculos")
				  -> set(
					    array(
						"ref"   =>$ref,
						"vinculo"   =>$vinculo
					    )
					  )
				  -> where("id = ?", $id);

	  $retorno = $sql -> exec() -> afectedRows() > 0 ? true : false;
        return $retorno;
    } // referenciasUpd
  
    public static function delVinculo($id){
        $orm = new ORM();
        $orm	-> delete()
                -> from("op_clientes_vinculos")
                -> where("id=?",$id);		
        $return = $orm->exec();
        return $return;
    }
    
    public static function lstEmpresa($id=null){
        $orm = new ORM();
        $orm    -> select()
                -> from ("op_empresas");
        if(!empty($id)){
            $orm->where("id=?",$id);
        }
        return $orm -> exec() -> fetchArray();
    }
    
    public static function saveConvenio(
                                $fechaI,
                                $fechaF,
                                $adjunto,
                                $id
                            ){
	  $orm = new ORM();
	  $sql = $orm	-> insert("op_convenio")
				  -> values(
                                        array(
                                            0,
                                            $id,
                                            $fechaI,
                                            $fechaF,
                                            $adjunto
                                            )
                                    );
	  //die($sql->verSql());
	  $return = $sql-> exec()-> afectedRows();
	  return $return > 0 ? true : false;
    }
    
        public static function allConvenio($id=null){
        $orm = new ORM();
        $orm    -> select(array("conv.*","cli.*"))
                -> from ("op_convenio","conv")
                ->join(array("cli"=>"op_empresas"),'cli.id = conv.id_empresa',"INNER");
        if(!empty($id)){
            $orm->where("id=?",$id);
        }
        //die($orm->verSql());
        return $orm -> exec() -> fetchArray();
    }
    
    public static function updEstado($id){
  	$orm = new ORM();
	  $sql = $orm	-> update("op_empresas")
				  -> set(
					    array(
						"convenio" =>'1'
					    )
					  )
				  -> where("id = ?", $id);
	  $retorno = $sql -> exec() -> afectedRows() > 0 ? true : false;
    return $retorno;
  }
  
    public static function saveEmpresa($nombre,$nit,$direccion,$telefono,$actividadEconomica,$ciiu,$correo){
	  $orm = new ORM();
	  $sql = $orm	-> insert("op_empresas")
                        -> values(
                                  array(
                                        0,
                                        0,
                                        getSucursal(),
                                        $nombre,
                                        $nit,
                                        $direccion,
                                        $telefono,
                                        $actividadEconomica,
                                        $ciiu,
                                        opDate(),
                                        $correo
                                      )
                         );
	  $return = $sql -> exec()->afectedRows();
	  return $return > 0 ? true : false;
    }
    
    public static function updEmpresa($nombre,$nit,$direccion,$telefono,$actividadEconomica,$ciiu,$id){
	  $orm = new ORM();
	  $sql = $orm	-> update("op_empresas")
                        -> set(
                            array(
                                  'razon'       =>$nombre,
                                  'nit'         =>$nit,
                                  'dir'         =>$direccion,
                                  'tel'         =>$telefono,
                                  'actividad'   =>$actividadEconomica,
                                  'ciiu'        =>$ciiu
                                )
                         )->where("nit = ?",$id);
	  $return = $sql -> exec()->afectedRows();
	  return $return > 0 ? true : false;
    }
  
  public static function saveEmpresaRepre(
                            $nombre,
                            $apellido,
                            $tipo,
                            $numero,
                            $empresa
                            ){
	  $orm = new ORM();
	  $sql = $orm	-> insert("op_empresas_representante")
                        -> values(
                                  array(
                                        0,
                                        $nombre,
                                        $apellido,
                                        $tipo,
                                        $numero,
                                        $empresa
                                      )
                         );
	  $return = $sql-> exec()-> afectedRows();
	  return $return > 0 ? true : false;
  }
  
  public static function updEmpresaRepre(
                            $nombre,
                            $apellido,
                            $tipo,
                            $numero,
                            $empresa
                            ){
	  $orm = new ORM();
	  $sql = $orm	->update("op_empresas_representante")
                        -> set(
                            array(
                                  "nombre"  =>$nombre,
                                  "apellido"=>$apellido,
                                  "tipo_doc"=>$tipo,
                                  "numero"  =>$numero
                                )
                         )->where("empresa = ?",$empresa);
	  $return = $sql-> exec()-> afectedRows();
	  return $return > 0 ? true : false;
  }
  
  public static function saveEmpresaFinan(
                            $ingresos,
                            $ingresosNoOperativos,
                            $ingresosDesc,
                            $totalIngresos,
                            $totalEgresos,
                            $totalActivos,
                            $totalPasivos,
                            $bienesFiducia,
                            $descFiducia,
                            $empresa
                            ){
	  $orm = new ORM();
	  $sql = $orm	-> insert("op_empresas_finanzas")
                        -> values(
                                  array(
                                        0,
                                        $ingresos,
                                        $ingresosNoOperativos,
                                        $ingresosDesc,
                                        $totalIngresos,
                                        $totalEgresos,
                                        $totalActivos,
                                        $totalPasivos,
                                        $bienesFiducia,
                                        $descFiducia,
                                        $empresa
                                      )
                         );
	  $return = $sql-> exec()-> afectedRows();
	  return $return > 0 ? true : false;
  }
  
  public static function updEmpresaFinan(
                            $ingresos,
                            $ingresosNoOperativos,
                            $ingresosDesc,
                            $totalIngresos,
                            $totalEgresos,
                            $totalActivos,
                            $totalPasivos,
                            $bienesFiducia,
                            $descFiducia,
                            $empresa
                            ){
	  $orm = new ORM();
	  $sql = $orm	->update("op_empresas_finanzas")
                        -> set(
                            array(
                                "in_anuales"=>$ingresos,
                                "in_anuales_no"=>$ingresosNoOperativos,
                                "desc_ingresos"=>$ingresosDesc,
                                "t_ingresos"=>$totalIngresos,
                                "t_egresos"=>$totalEgresos,
                                "t_activos"=>$totalActivos,
                                "t_pasivos"=>$totalPasivos,
                                "fiducia"=>$bienesFiducia,
                                "desc_fiducia"=>$descFiducia
                                )
                         )->where("empresa = ?",$empresa);
	  $return = $sql-> exec()-> afectedRows();
	  return $return > 0 ? true : false;
  }
  
  public static function saveEmpresaAccio(
                            $nombreA,
                            $tipo_docA,
                            $ndocumentoA,
                            $participacion,
                            $empresa
                            ){
	  $orm = new ORM();
	  $sql = $orm	-> insert("op_empresas_acciones")
                        -> values(
                                  array(
                                        0,
                                        $nombreA,
                                        $tipo_docA,
                                        $ndocumentoA,
                                        $participacion,
                                        $empresa
                                      )
                         );
	  //die($sql->verSql());
	  $return = $sql-> exec()-> afectedRows();
	  return $return > 0 ? true : false;

  }
  
  
  public static function updEmpresaAccio(
                            $nombreA,
                            $tipo_docA,
                            $ndocumentoA,
                            $participacion,
                            $empresa
                            ){
	  $orm = new ORM();
	  $sql = $orm	->update("op_empresas_acciones")
                        ->set(
                            array(
                                "nombres"=>$nombreA,
                                "tipo_doc"=>$tipo_docA,
                                "documento"=>$ndocumentoA,
                                "participacion"=>$participacion,
                                )
                         )->where("empresa = ?",$empresa);
	  //die($sql->verSql());
	  $return = $sql-> exec()-> afectedRows();
	  return $return > 0 ? true : false;

  }
  
  public static function saveEmpresaOper(
                            $operacionesIn,
                            $tipoOperacion,
                            $productoFinanciero,
                            $obanco,
                            $numeroCuenta,
                            $moneda,
                            $monto,
                            $ciudad,
                            $empresa
                            ){
	  $orm = new ORM();
	  $sql = $orm	-> insert("op_empresas_inter")
                        -> values(
                                  array(
                                        0,
                                        $operacionesIn,
                                        $tipoOperacion,
                                        $productoFinanciero,
                                        $obanco,
                                        $numeroCuenta,
                                        $moneda,
                                        $monto,
                                        $ciudad,
                                        $empresa
                                      )
                         );
	  //die($sql->verSql());
	  $return = $sql	-> exec()
					  -> afectedRows();
	  return $return > 0 ? true : false;
  }
  
  public static function updEmpresaOper(
                            $operacionesIn,
                            $tipoOperacion,
                            $productoFinanciero,
                            $obanco,
                            $numeroCuenta,
                            $moneda,
                            $monto,
                            $ciudad,
                            $empresa
                            ){
	  $orm = new ORM();
	  $sql = $orm	->update("op_empresas_inter")
                        -> set(
                            array(
                                "operacionesIn"     =>$operacionesIn,
                                "tipoOperacion"     =>$tipoOperacion,
                                "productoFinanciero"=>$productoFinanciero,
                                "obanco"            =>$obanco,
                                "numeroCuenta"      =>$numeroCuenta,
                                "moneda"            =>$moneda,
                                "monto"             =>$monto,
                                "ciudad"            =>$ciudad
                                )
                         )->where("empresa = ?",$empresa);
	  $return = $sql-> exec()-> afectedRows();
	  return $return > 0 ? true : false;
  }
    
} // class
