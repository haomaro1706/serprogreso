<?php 
include_once 'app/modulos/usuarios/vista/v.Usuarios.php';
include_once 'app/modulos/usuarios/modelo/m.Usuarios.php';
include_once 'app/modulos/modulos/modelo/m.Modulos.php';
include_once 'app/modulos/grupos/modelo/m.Grupos.php';
include_once 'app/modulos/zonas/modelo/m.Zonas.php';
include_once 'app/modulos/sucursales/modelo/m.Sucursales.php';
include_once 'app/modulos/perfil/modelo/m.Perfil.php';
include_once 'app/modulos/parametros/modelo/m.Parametros.php';
include_once 'app/modulos/usuarios/modulos.php';

class Usuarios extends vUsuarios{
    
    function main(){
        validar_usuario();
    	$mUsuarios = new mUsuarios();
    	$listaUsuarios = $mUsuarios->listarUsuarios();
    	$cadena = '';
		$i=1;
    	foreach($listaUsuarios as $key=>$usuario){
    		$cadena .= '<tr> <td>'.$i.'</td>';
    		$cadena .= '<td>'.$usuario["zona"].'</td>';
    		$cadena .= '<td>'.$usuario["nombre_suc"].'</td>';
    		$cadena .= '<td>'.$usuario["tipoDoc"].'</td>';
    		$cadena .= '<td>'.$usuario["documento"].'</td>';
    		$cadena .= '<td>'.$usuario["nombre"].'</td>';
    		
    		$imgActivo = ($usuario["activo"]==1)? "activo": "inactivo";
    		
    		$cadena .= '<td> <img src="'.SISTEMA_RUTAIMG.$imgActivo .'.png" alt="'.$imgActivo.'"> </td>';
    		$cadena .= '<td> <a href="'.setUrl("usuarios","verUsuario", array("id"=>$usuario["id"])).'" title="Ver"> <img src="'.SISTEMA_RUTAIMG.'ver.png" alt="Editar"> </a> </td>';
    		$cadena .= '<td> <a href="'.setUrl("usuarios","editarUsuario", array("id"=>$usuario["id"])).'" title="Editar"> <img src="'.SISTEMA_RUTAIMG.'editar.png" alt="Editar"> </a> </td>';
    		$cadena .= '<td> <a onclick="return confirm(\'Seguro que desea eliminar este registro y toda su informacion relacionada?\')" href="'.setUrl("usuarios","eliminarUsuario", array("id"=>$usuario["id"])).'" title="Eliminar"> <img src="'.SISTEMA_RUTAIMG.'eliminar.png" alt="Eliminar"> </a> </td> </tr>';
    		$i++;
    	}
    	$cadena = utf8_encode($cadena);

        parent::main($cadena);
    }
    
	
    function login(){
		//die('='.getIdUsuario());
		//die(print(getIdUsuario()));
    	//if(!getIdUsuario())
    		parent::vLogin(); 
    	/*else
    		location(setUrl("usuarios","panelAdmin"));*/
    }
    
    
    function loginProcess($arrayParams=null){
        
    	if(!getIdUsuario()){
	        if($arrayParams["user"] && $arrayParams["inputPassword"] ){
	        	$mUsuarios = new mUsuarios();
	       
		        $infoUser = $mUsuarios->infoUsuario(htmlspecialchars($arrayParams["user"]), htmlspecialchars($arrayParams["inputPassword"]));
	        
		        if(!$infoUser || $infoUser==NULL){            
		            setMensaje("El usuario o contrase&ntilde;a no son correctos","error");
		            location(setUrl("usuarios","login"));
		        }else{ 
		          
		            $infoUser = $infoUser[0];
		            if($infoUser["activo"]==0){
		                setMensaje("El usuario se encuentra deshabilitado. Comuniquese con el administrador del portal para informar el caso","error");
		                location(setUrl("usuarios","login"));
		            }else{
                                $_SESSION["Usuarios"]["sucursal"] = $infoUser["id_sucursal"];
                                $_SESSION["Usuarios"]["login"] = md5($infoUser["login"].$infoUser["id"]);
		                $_SESSION["Usuarios"]["id"] = $infoUser["id"];
		                $_SESSION["Usuarios"]["grupo"] = $infoUser["id_grupos"];
		                $_SESSION["Usuarios"]["nombres"] = $infoUser["nombre"];
		                $mUsuarios->iniciarSession($infoUser["id"], $infoUser["login"], $_SESSION["Usuarios"]["login"]);
		                setMensaje("Bienvenido.","success");
		                location(setUrl("usuarios","panelAdmin"));
						
					}
				}
	        }else{
	        	setMensaje("El usuario o contrase&ntilde;aa no son correctos","error");
	        	location(setUrl("usuarios","login"));
	        }
        }else{
    		location(setUrl("usuarios","panelAdmin"));
        }
    }

    
    function panelAdmin() {	
            validar_usuario();
            $modulo=new mModulos();
            $lista=lstmodulos2();
            $modulos=$modulo->cargarModulos2($lista);
			//Die("Hi");
            $mUsuarios = new mUsuarios();
            $idUser = getGrupoUsuario();
            $arrayPerms = $mUsuarios->permisosActuales($idUser);
            
			
			$cadena='';
            $cadena.='<div class="menuAdmin">';
            if (is_array($arrayPerms)){
	            foreach ($modulos As $valor){ 
					$checked = false;
					foreach($arrayPerms as $key => $val){
						if(in_array($valor["nombre"], $val)){
							$checked = true;
							break;
						}
						
					}
	            	if($valor["nombre"]!= "perfil" && !$checked) continue;
	
	                $cadena.='<div>';
	                $cadena.='<a title="'.$valor["titulo"].'" href="'.setUrl($valor["nombre"]).'">';
	                $cadena.='<img alt="'.$valor["titulo"].'" src="app/modulos/'.$valor["nombre"].'/img/'.$valor["img"].'"><br>
	                '.$valor["titulo"].'</a><br>
	                '.$valor["descripcion"].'</div>';
	            }
            }
            else{
            	setMensaje ("Usted no presenta m&oacute;dulos habilitados", "info");
            }
            $cadena.='</div>';
            $cadena_c= utf8_encode($cadena);
            parent::panelAdmin($cadena_c);
        
    }
    
    
    function cerrarSesion(){
        $mUsuarios = new mUsuarios();
        if ($mUsuarios->cerrarSession($_SESSION["Usuarios"]["login"]))
        {
            setMensaje ("La sesion ha terminado correctamente", "success");
            unset($_SESSION["Usuarios"]);
            location(setUrl("usuarios","login"));
        }
        else{
            setMensaje ("No se ha podido cerrar la sesion", "info");
            location(setUrl("usuarios","panelAdmin"));
        }
    }
    
    function parametros($id, $cc){
      echo $id." ".$cc;
    }
    
    function recordarPass(){
        echo 'recordar pass.';
    }
    
    
    function activarUsuario($arrayCampos){
        validar_usuario();
    	$mUsuarios = new mUsuarios();
        
        $estado = $arrayCampos["estado"] == 0? 1:0;
	if($mUsuarios->activarUsuario($arrayCampos["id"], $estado)>0)
		setMensaje ("Usuario actualizado con exito", "success");
	else
		setMensaje ("No se ha podido relizar la operacion", "error");
	location(setUrl("usuarios"));
    } // activarUsuario
    
	
	public function crearUsuario(){
		validar_usuario();
		$mGrupos = new mGrupos();
		$lista = $mGrupos->lstGrupos();
		$grupos = '';
		
		if(is_array($lista)){
			foreach($lista as $grupo){
				$grupos .= '<option value="'.$grupo["id"].'">'.$grupo["grupo"].'</option>';
			}
		}else{
			$grupos = '<option>Sin dato de grupos</option>';
		}
		
		$mSuc = new mZonas();
		$lista = $mSuc->listar();
		$sucurs = '';
		
		if(is_array($lista)){
			foreach($lista as $grupo){
				$sucurs .= '<option value="'.$grupo["id"].'">'.$grupo["zona"].'</option>';
			}
		}else{
			$sucurs = '<option>Sin dato de zonas</option>';
		}
                
                $cargos = '';
		$lista = mParametros::cargos();
		if(is_array($lista)){
			foreach($lista as $grupo){
				$cargos .= '<option value="'.$grupo["id"].'">'.$grupo["cargo"].'</option>';
			}
		}else{
			$cargos = '<option>Sin dato de zonas</option>';
		}
                
		parent::crearUsuario($grupos, $sucurs,$cargos);
	} // crearUsuario
    
	
    function registrarUsuario($arrayCampos){
        validar_usuario();
    	$mUsuarios = new mUsuarios();
		$saved = $mUsuarios->infoUsuarioDoc($arrayCampos["ident"]);
		$saved2 = $mUsuarios->infoUsuario($arrayCampos["login"], $arrayCampos["passwd"]);
		if(empty($saved) && empty($saved2)){
			$save = $mUsuarios->registrarUsuario( 
                                                            $arrayCampos["grupo"], 
                                                            $arrayCampos["sucursal"],
                                                            $arrayCampos["tipoIdent"], 
                                                            $arrayCampos["ident"], 
                                                            $arrayCampos["nombre"], 
                                                            $arrayCampos["dir"], 
                                                            $arrayCampos["telef"], 
                                                            $arrayCampos["telef2"], 
                                                            $arrayCampos["celular"], 
                                                            $arrayCampos["login"], 
                                                            $arrayCampos["passwd"], 
                                                            $arrayCampos["activo"],
                                                            $arrayCampos["cargo"]
                                                            );
			if($save){
				setMensaje ("Usuario registrado con exito", "success");
                        }else{
				setMensaje ("No se ha podido registrar el operacion", "error");
                        }
		}else{
			setMensaje ("Un usuario ya ha sido registrado previamente con este login y password o este documento", "error");
                }
		location(setUrl("usuarios"));
    } // registrarUsuario
    
    
	function actualizarUsuario($arrayCampos){
        validar_usuario();
        //die(print_r($arrayCampos));
    	$mUsuarios = new mUsuarios();
		if($arrayCampos["id"]!="" && $arrayCampos["ident"]!="" && $arrayCampos["login"]!=""){
			$infoDoc = $mUsuarios->infoUsuarioDoc($arrayCampos["ident"]);
			$info = $mUsuarios->infoPorLogin($arrayCampos["login"]);
			
			if($infoDoc[0]["id"] == $arrayCampos["id"] && $info[0]["id"] == $arrayCampos["id"]){
                            $upd = $mUsuarios->editarUsuario(
                                                    $arrayCampos["id"], 
                                                    $arrayCampos["grupo"], 
                                                    $arrayCampos["sucursal"],
                                                    $arrayCampos["tipoIdent"], 
                                                    $arrayCampos["ident"], 
                                                    $arrayCampos["nombre"], 
                                                    $arrayCampos["dir"], 
                                                    $arrayCampos["telef"], 
                                                    $arrayCampos["telef2"], 
                                                    $arrayCampos["celular"], 
                                                    $arrayCampos["login"], 
                                                    $arrayCampos["passwd"], 
                                                    $arrayCampos["activo"],
                                                    $arrayCampos["correo"]
                                                            );
				if($upd){
                                    setMensaje ("Usuario actualizado exitosamente", "success");
                                }else{
                                    setMensaje ("No se ha podido actualizar el usuario", "error");
                                }
			}else{
                            setMensaje ("Un usuario ya ha sido registrado previamente con este login y password o este documento", "error");
                        }
		}else{
                    setMensaje ("Debe completar todos los campos", "error");
                }
		location(setUrl("usuarios"));
    } // registrarUsuario
	
	
    function eliminarUsuario($arrayCampos){
        validar_usuario();
    	$mUsuarios = new mUsuarios();

    	if($mUsuarios->eliminarUsuario($arrayCampos["id"]))
			setMensaje ("Usuario eliminado con exito", "success");
		else
			setMensaje ("No se ha podido eliminar el usuario", "error");
		location(setUrl("usuarios"));
    } // registrarUsuario
    
    
    function editarUsuario($arrayCampos){
        validar_usuario();
    	$mUsuarios = new mUsuarios();
		$infoUsuario = $mUsuarios->infoUsuarioId($arrayCampos["id"]);
		
		if($infoUsuario){
			$mGrupos = new mGrupos();
			$lista = $mGrupos->lstGrupos();
			$grupos = '';
			if(is_array($lista)){
				foreach($lista as $grupo){
					$grupos .= '<option value="'.$grupo["id"].'" '.(($grupo["id"] == $infoUsuario[0]["id_grupos"])? 'selected="selected"':'').'>'.$grupo["grupo"].'</option>';
				}
			}else{
				$grupos = '<option>Sin dato de grupos</option>';
			}
			
			
			$infoSuc = mSucursales::selectbyId($infoUsuario[0]["id_sucursal"]);
			$infoSuc = $infoSuc[0];
			
			
			$lista = mZonas::listar();
			$zonas = '';
			
			if(is_array($lista)){
				foreach($lista as $grupo){
					$zonas.= '<option value="'.$grupo["id"].'" '.(($grupo["id"] == $infoSuc["id_zona"])? 'selected="selected"':'').'>'.$grupo["zona"].'</option>';
				}
			}else{
				$zonas = '<option>Sin dato de grupos</option>';
			}
			
			$lista = mSucursales::sucurbyId($infoSuc["id_zona"]);
			$sucurs = '';
			
			if(is_array($lista)){
				foreach($lista as $grupo){
					$sucurs .= '<option value="'.$grupo["id"].'" '.(($grupo["id"] == $infoUsuario[0]["id_sucursal"])? 'selected="selected"':'').'>'.$grupo["nombre_suc"].'</option>';
				}
			}else{
				$sucurs = '<option>Sin dato de grupos</option>';
			}
			
			parent::editarUsuario($infoUsuario[0], $grupos, $zonas, $sucurs);
		}
		else{
			setMensaje ("No se ha podido encontrar la informaci&oacute;n del usuario", "error");
			location(setUrl("usuarios"));
		}
	
    } // editarUsuario
    
    
    function verUsuario($arrayCampos){
        validar_usuario();
    	$mUsuarios = new mUsuarios();
		$infoUsuario = $mUsuarios->infoUsuarioId($arrayCampos["id"]);
		
		if($infoUsuario){
			$mGrupos = new mGrupos();
			$lista = $mGrupos->lstGrupos();
			$grupos = '';
			if(is_array($lista)){
				foreach($lista as $grupo){
					$grupos .= '<option value="'.$grupo["id"].'" '.(($grupo["id"] == $infoUsuario[0]["id_grupos"])? 'selected="selected"':'').'>'.$grupo["grupo"].'</option>';
				}
			}else{
				$grupos = '<option>Sin dato de grupos</option>';
			}
			
			
			$infoSuc = mSucursales::selectbyId($infoUsuario[0]["id_sucursal"]);
			$infoSuc = $infoSuc[0];
			
			
			$lista = mZonas::listar();
			$zonas = '';
			
			if(is_array($lista)){
				foreach($lista as $grupo){
					$zonas.= '<option value="'.$grupo["id"].'" '.(($grupo["id"] == $infoSuc["id_zona"])? 'selected="selected"':'').'>'.$grupo["zona"].'</option>';
				}
			}else{
				$zonas = '<option>Sin dato de grupos</option>';
			}
			
			$lista = mSucursales::sucurbyId($infoSuc["id_zona"]);
			$sucurs = '';
			
			if(is_array($lista)){
				foreach($lista as $grupo){
					$sucurs .= '<option value="'.$grupo["id"].'" '.(($grupo["id"] == $infoUsuario[0]["id_sucursal"])? 'selected="selected"':'').'>'.$grupo["nombre_suc"].'</option>';
				}
			}else{
				$sucurs = '<option>Sin dato de grupos</option>';
			}
			
			parent::verUsuario($infoUsuario[0], $grupos, $zonas, $sucurs);
		}
		else{
			setMensaje ("No se ha podido encontrar la informaci&oacute;n del usuario", "error");
			location(setUrl("usuarios"));
		}
	
    } // verUsuario
    
	
    function gestionPermisos($arrayParams){
        validar_usuario();
        $idUser = $arrayParams["id"];
        if(!$idUser){
            setMensaje ("No se ha podido encontrar la informaci&oacute;n del usuario", "error");
	    location(setUrl("usuarios","permisos"));
        }

    	$modulo=new mModulos();
        $modulos=$modulo->cargarModulos(1);

        $mUsuarios = new mUsuarios();
        $permisosActuales = $mUsuarios->permisosActuales($idUser);
        $arrayPerms = unserialize($permisosActuales["modulos"]);

        $cadena = '';
		$i=1;
    	foreach($modulos as $key=>$modulo){
    	        $checked = (is_array($arrayPerms) && in_array($modulo["nombre"], $arrayPerms))?'checked="checked"':'';
    		$cadena .= '<tr> <td>'.$i.'</td>';
    		$cadena .= '<td>'.$modulo["titulo"].'</td>';
    		$cadena .= '<td> <input type="checkbox" name="modulos[]" value="'.$modulo["nombre"].'"'.$checked.'> </td>';
    		$i++;
    	}
    	$cadena = utf8_encode($cadena);

        parent::gestionPermisos($cadena, $idUser);
    } // gestionPermisos

    
    function registrarPermisos($arrayCampos){
        validar_usuario();
        $idUser = $arrayCampos["idUser"];
        if(!$idUser){
            setMensaje ("No se ha podido encontrar la informaci&oacute;n del usuario", "error");
	    location(setUrl("usuarios","permisos"));
        }

        $arrayPerms = serialize($arrayCampos["modulos"]);

    	$mUsuarios = new mUsuarios();

        $permisosActuales = $mUsuarios->permisosActuales($idUser);

        if($permisosActuales["id"] != $idUser)
      	    $permisos = $mUsuarios->registrarPermisos($idUser, $arrayPerms);
        else
            $permisos = $mUsuarios->actualizarPermisos($idUser, $arrayPerms);

        if($permisos){
            setMensaje ("Se han registrado los permisos con exito", "success");
	    location(setUrl("usuarios","permisos"));
        }
        else{
            setMensaje ("No se han podido registrar los permisos correctamente", "error");
	    location(setUrl("usuarios","permisos"));
        }
    } // registrarPermisos
    
    public function grupos(){
        validar_usuario();
		$mGrupos = new mGrupos();
		$lst = $mGrupos->lstGrupos();
		$cadena = "";
		if(is_array($lst)){
			$i=1;
			foreach($lst as $grupo){
				$cadena .= '<tr>';
				$cadena .= '<td>'.$i.'</td>';
				$cadena .= '<td>'.$grupo["grupo"].'</td>';
				$estado = $grupo["activo"] == 1? '<img src="">':'<img src="">';
				$activo = $grupo["activo"] == 1? 'inactivo':'activo';
				$cadena .= '<td><a href="'.setUrl("usuarios","gestionPermisos", array("id"=>$grupo["id"])).'" title="Gestionar permisos"> <img src="'.SISTEMA_RUTAIMG.'permisos.png" alt="Editar"> </a> </td>';
				$cadena .= $grupo["id"]==1? '<td> </td>':'<td><a href="'.setUrl("grupos","updEstado",array("id"=>$grupo["id"], "estado"=>$grupo["activo"])).'" title="Dejar '.$activo.'"><img src="'.SISTEMA_RUTAIMG.$activo.'.png"></a></td>';
				$cadena .= $grupo["id"]==1? '<td> </td>':'<td><a href="'.setUrl("usuarios","editGrupo",array("id"=>$grupo["id"])).'"><img src="'.SISTEMA_RUTAIMG.'editar.png"></a></td>';
				$cadena .= $grupo["id"]==1? '<td> </td>':'<td><a href="'.setUrl("grupos","delGrupo",array("id"=>$grupo["id"])).'"><img src="'.SISTEMA_RUTAIMG.'eliminar.png"></a></td>';
				$cadena .= '</tr>';
				$i++;
			}
		}else{
			setMensaje("No existen grupos creados.","info");
		}
        parent::grupos($cadena);    
    } // main
    
    public function editGrupo($array){
		validar_usuario();
		if(is_numeric($array["id"]) && $array["id"]!=1){
			$mGrupos = new mGrupos();
			$info = $mGrupos->infoGrupo($array["id"]);
			$cadena = $info[0];
		}else{
			setMensaje("No se ha enviado informaci&oacute;n correcta del grupo","error");
			location(setUrl("grupos"));
		}
		
		parent::editGrupo($cadena);
	} // editGrupo
    
    public function cargos(){
        validar_usuario();
        $info=mParametros::cargos();
        parent::cargos($info);
    }
    
    public function editarCargos($array=null){
        validar_usuario();
        $info = mParametros::cargosId($array['id']);
        parent::editarCargos($info[0]);
    }
        
    	}
