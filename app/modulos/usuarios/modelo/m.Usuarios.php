<?php  

class mUsuarios{
	
	public function validar_login($user){
        $orm = new ORM();
        $sql = $orm	->select()
					->from("op_session","opSe")
					->join(array("opUs"=>"op_usuarios"),"opUs.id = opSe.id_user")
					->where("opSe.sesion = ?",$user);
		
		$return = $orm->exec()->numRows();
		return $return > 0 ? true : false;
    }

    public function infoLogin($user){
        $orm = new ORM();
        $sql = $orm	->select()
					->from("op_session","opSe")
					->join(array("opUs"=>"op_usuarios"),"opUs.id = opSe.id_user")
					->where("opSe.sesion = ?",$user);
        
		$return = $orm->exec()->fetchArray();
		return $return;
    }
	
	
    public function activarUsuario($id, $estado){
		$orm = new ORM();
		$sql = $orm	-> update("op_usuarios")
					-> set(array("activo"=>$estado))
					-> where("id = ?", $id);
        $orm -> exec();
		$retorno = $orm -> afectedRows() > 0 ? true : false;
        return $retorno;
    } // activarUsuario
    
    
    public function validar_usuarios($login,$pass){
		$pass=md5($pass);
        $orm = new ORM();
		$sql = $orm -> select()
					-> from("op_usuarios")
					-> where("login = ? ", $orm->escape_string($login))
					-> where("pass = ? ", $orm->escape_string($pass));
		
        $total 		= $orm -> exec() -> numRows();
        $retorno	= ( $total > 0 ) ? true : false;
        
        return $retorno;
    } // validar_usuarios
    
	
    public function infoUsuario($login, $pass){
        $pass=md5($pass);
        $orm = new ORM();
        $sql = $orm -> select()
					-> from("op_usuarios")
					-> where("login = ? ", $orm->escape_string($login))
					-> where("pass = ? ", $orm->escape_string($pass));
		//die($sql->getSql());
		$retorno = $orm -> exec() -> fetchArray();
        return $retorno;
    } // infoUsuario
    
    
    public function infoPorLogin($login){
        $orm = new ORM();
        $sql = $orm -> select()
					-> from("op_usuarios")
					-> where("login = ? ", $orm->escape_string($login));
		
		$retorno = $orm -> exec() -> fetchArray();
        return $retorno;
    } // infoUsuario
    
    
    public function infoUsuarioId($id){
        $orm = new ORM();
        $sql = $orm -> select()
					-> from("op_usuarios")
					-> where("id = ? ", $id);
        $retorno = $orm -> exec() ->fetchArray();
        return $retorno;
    } // infoUsuarioId
    
	
    public function infoUsuarioDoc($doc){
        $orm = new ORM();
        $sql = $orm -> select()
					-> from("op_usuarios")
					-> where("documento = ? ", $doc);
        $retorno = $orm -> exec() ->fetchArray();
        return $retorno;
    } // infoUsuarioId
	
	   
    public function iniciarSession($id, $login, $session){
        $orm = new ORM();
        $sql = $orm -> insert("op_session")
					-> values(array(0, $id, $login, $session, date("Y-m-d H:i:s")));		
		$orm -> exec();
        $retorno = $orm->afectedRows() >0? true:false;
        return $retorno;
    } // iniciarSession
    
	
    public function cerrarSession($session){
        $orm = new ORM();
		$sql = $orm -> delete()
					-> from("op_session")
					-> where("sesion = ? ", $session);
		$orm -> exec();
		$retorno = $orm -> afectedRows() > 0 ? true : false;
        return $retorno;
    } // cerrarSesion
    
	
	public function lstUsers(){
		$orm = new ORM();
		$orm -> select(array("id","nombre"))
			 -> from('op_usuarios')
			 -> where("activo = ?",1);
		return $orm -> exec() -> fetchArray();
	}
	
    public function listarUsuarios(){
    	$orm = new ORM();
		$sql = $orm -> select(array("opUs.*","opS.nombre_suc","opZ.zona"))
					-> from ("op_usuarios","opUs")
					-> join (array("opS"=>"op_sucursales"),"opS.id = opUs.id_sucursal")
					-> join (array("opZ"=>"op_zonas"),"opZ.id = opS.id_zona");
		$retorno = $orm -> exec() -> fetchArray();
        return $retorno;
    } // listarUsuarios
 
 
    public function registrarUsuario(
                                        $grupo,
                                        $sucursal,
                                        $tipoDoc,
                                        $doc,
                                        $nombre,
                                        $dir,
                                        $telef,
                                        $telef2,
                                        $cel,
                                        $login,
                                        $password,
                                        $estado,
                                        $cargo
                                        ){
    	$orm = new ORM();
    	$sql = $orm -> insert("op_usuarios")
		-> values(array(0, $grupo, $sucursal, $tipoDoc, $doc, $nombre, $dir, $telef, $telef2, $cel, $login, md5($password),null, $estado,$cargo));
    	$orm -> exec();
		$retorno = $orm -> afectedRows() > 0 ? true : false;
        return $retorno; 
    } // registrarUsuario
    
    
    public function editarUsuario(
                                $id,
                                $grupo,
                                $sucursal,
                                $tipoDoc,
                                $doc,
                                $nombre,
                                $dir,
                                $telef,
                                $telef2,
                                $cel,
                                $login,
                                $password,
                                $estado,
                                $correo
                                ){
        $orm = new ORM();
        if($password !=""){
                $sql = $orm -> update("op_usuarios")
                            -> set( 
                                array(	
                                    "id_grupos"=>$grupo, 
                                    "id_sucursal"=>$sucursal, 
                                    "tipoDoc"=>$tipoDoc, 
                                    "documento"=>$doc, 
                                    "nombre"=>$nombre, 
                                    "direccion"=>$dir, 
                                    "telef"=>$telef, 
                                    "telef2"=>$telef2, 
                                    "celular"=>$cel, 
                                    "login"=>$login, 
                                    "pass"=>md5($password), 
                                    "activo"=>$estado,
                                    "correo"=>$correo
                                )
                                    )
                            -> where("id = ? ", $id);
        }else{
                $sql = $orm -> update("op_usuarios")
                            -> set( 
                                array(	
                                    "id_grupos"=>$grupo, 
                                    "id_sucursal"=>$sucursal, 
                                    "tipoDoc"=>$tipoDoc, 
                                    "documento"=>$doc, 
                                    "nombre"=>$nombre, 
                                    "direccion"=>$dir, 
                                    "telef"=>$telef, 
                                    "telef2"=>$telef2, 
                                    "celular"=>$cel, 
                                    "login"=>$login,  
                                    "activo"=>$estado,
                                    "correo"=>$correo
                                    )
                                )
                            -> where("id = ? ", $id);
        }// die($orm->verSql());			
        $orm -> exec();
        $retorno = $orm -> afectedRows() > 0 ? true : false;
        return $retorno;
    } // editarUsuario
    
    
    public function eliminarUsuario($id){
		$orm = new ORM();
		$sql = $orm -> delete()
					-> from("op_usuarios")
					-> where("id = ?",$id);
        $orm -> exec();
		$retorno = $orm -> afectedRows() > 0 ? true : false;
        return $retorno;
    } // eliminarUsuario


    public function permisosActuales($id){
        $orm = new ORM();
		$sql = $orm -> select()
					-> from("op_modulos_funcionperms","opFP")
					-> join(array("opM"=>"op_modulos"),"opM.id = opFP.id_modulo")
					-> where("id_grupo = ? ", $id);
                //die($sql->verSql());
		$retorno = $sql -> exec() -> fetchArray();
        return $retorno;
    } // permisosActuales


    public function registrarPermisos($id, $modulos){
		$orm = new ORM();
        $sql = $orm -> insert("op_modPerms")
					-> values(array(0, $id, $modulos));                      
        $orm -> exec();
		$retorno = $sql -> afectedRows() > 0 ? true : false;
        return $retorno;
    } // registrarPermisos


    public function actualizarPermisos($id, $modulos){
		$orm = new ORM();
        $sql = $orm -> update("op_modPerms")
					-> set(array("modulos"=>$modulos))
					-> where("id_grupos = ? ", $id);
        $orm -> exec();
		$retorno = $sql -> afectedRows() > 0 ? true : false;
        return $retorno;
    } // actualizarPermisos
	
}
?>
