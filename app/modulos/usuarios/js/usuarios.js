$(function(){
	
	$("#crearUsuario").validate({
       rules:{
			tipoIdent:{required:true},
			ident:{required:true, number: true},
			nombre:{required:true,lettersonly:true},
			dir:{required:true},
			telef:{required:true, number: true},
			telef2:{number:true},
			celular:{number:true},
			login:{required:true, minlength:4},
			passwd:{required:true, minlength:5},
			grupo:{required:true},
			activo:{required:true,}
        }
    })
	
	$("#editarUsuario").validate({
       rules:{
			tipoIdent:{required:true},
			ident:{required:true, number: true},
			nombre:{required:true,lettersonly:true},
			dir:{required:true},
			telef:{required:true, number: true},
			telef2:{number:true},
			celular:{number:true},
			login:{required:true, minlength:4},
			passwd:{minlength:5},
			grupo:{required:true},
			activo:{required:true,}
        }
    })
	
	$("#login").validate({
       rules:{
			user:{required:true, minlength:4, lettersonly:true},
			inputPassword:{required:true, minlength:5},
		}
    })
    
  
	$("#zona").change(function(){
		$.post("index.php/sucursales/postSucursales",{id:$(this).val(), jquery : 1},function(data){
			$("#sucursal").html(data)
		})
	})
    })