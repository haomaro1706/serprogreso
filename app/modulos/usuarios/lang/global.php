<?php 

define("USUARIOS_NOMBRE","Usuarios");

define("USUARIOS_GRUPOS","Grupos");

define("USUARIOS_CREAR_USUARIO","Crear Usuario");

define("USUARIOS_EDITAR_USUARIO","Editar Usuario");

define("USUARIOS_USUARIOS","Usuarios");

define('MODULO_PERFIL','Mi Perfil');

define('PERFIL_CAMBIAR_PASS','Cambiar Contrase&ntilde;a');

define('GRUPOS_NOMBRE','Grupos');

define('GRUPOS_CREAR','Crear Grupos');

define('GRUPOS_EDITAR','Editar Grupos');

define("GRUPOS_PERMISOS","Permisos sobre m&oacute;dulos");

define("GRUPOS_GESTION_PERMISOS","Permisos sobre m&oacute;dulos para ");

define("GRUPOS_MAS_PERMISOS","Mas Permisos para ");

define("USUARIOS_CARGOS","Cargos");

define("USUARIOS_CARGOS_EDITAR","Editar cargos");