<?php  

class vUsuarios{

    public function main($listaUsuarios){
    	echo getMensaje();
        ?>
           <h2><?php  echo USUARIOS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl("parametros"), 
            		  USUARIOS_NOMBRE=>"#",
            		  USUARIOS_CREAR_USUARIO=>setUrl("usuarios","crearUsuario"),
            		  USUARIOS_CARGOS=>setUrl("usuarios","cargos")/*,
                          PERFIL_CAMBIAR_PASS=>setUrl("usuarios","cambiarPassword")*/
            		 ),
            	    USUARIOS_NOMBRE
            	   );
            
            navTabs(array(OP_BACK2=>setUrl("parametros"), 
            		  USUARIOS_NOMBRE=>"#"
            		 ),
            	    USUARIOS_NOMBRE, 
            	    "breadcrumb"
            	   );
            ?>
       <h4><?php  echo USUARIOS_NOMBRE?></h4>
       <div class="container">
        <div class="span10 offset0"> 
			<?php  echo datatable("usuarios")?>
		<table class="table table-striped table-hover table-condensed" id="usuarios">
    			<thead><tr> 
    				<th> # </th>
    				<th> Zona </th>
    				<th> Sucursal </th>
    				<th> Tipo de indentificaci&oacute;n </th>
    				<th> Identificaci&oacute;n</th>
    				<th> Nombre </th>
    				<th> Estado </th>
    				<th> Ver </th>
    				<th> Editar </th>
    				<th> Eliminar </th>
    			</tr> </thead> <tbody>
					<?php  echo $listaUsuarios; ?>
				</tbody> </table>
        </div>
       </div>
        <?php 
    } // main
    
    
    public function vLogin($val=""){
    	echo getMensaje();
		?>
		<h2> Acceso de Usuarios </h2>
			<style>
				.breadcrumb li{
					background: url("app/img/home_min.png");
					background-repeat: no-repeat;
				}
				
				.breadcrumb a{
					color: #015e98;
					font-family: 'century gothic';
					margin-left: 30px;
					font-size : 12px !important;
				}
			</style>
			<div class="container">
				<div class="span6 offset2">            
				<form class="form-horizontal" enctype="multipart/form-data" name="login" id="login" action="<?php  echo setUrl("usuarios","loginProcess")?>" method="post">
					<fieldset>
						<legend>Ingresar</legend>
						<div class="control-group">
							<label class="control-label" for="user">Login:</label>
							<div class="controls">
								<input type="text" id="user" name="user" placeholder="Login" autocomplete="off">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="email">Contrase&ntilde;a:</label>
							<div class="controls">
								<input type="password" name="inputPassword" id="inputPassword" placeholder="Password" autocomplete="off">
							</div>
						</div>
					</fieldset>
					<div class="control-group">
						<div class="controls">                   
							<button type="submit" class="btn btn-primary">Ingresar</button>
							<input type="hidden" name="val" value="<?php  echo $val?>">
						</div>
					</div>
				</form>
					
				</div>
			</div>
        <?php 
    } // vLogin
    
    
      
    public function panelAdmin($modulos=""){
        getMensaje();
        echo $modulos;
        /*este panel se debe crear dinamicamente desde bd*/
    } // panelAdmin
    
    
    public function crearusuario($grupos, $zonas,$cargos){
    	echo getMensaje();
        ?>
           <h2><?php  echo USUARIOS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl("parametros"), 
            		  USUARIOS_NOMBRE=>setUrl("usuarios","main"), 
            		  USUARIOS_CREAR_USUARIO=>"#",
            		  USUARIOS_CARGOS=>setUrl("usuarios","cargos")
            		 ),
            	    USUARIOS_CREAR_USUARIO
            	   );
            
            navTabs(array(OP_BACK2=>setUrl("parametros"), 
            		  USUARIOS_CREAR_USUARIO=>"#"
            		 ),
            	    USUARIOS_CREAR_USUARIO, 
            	    "breadcrumb"
            	   );
            ?>
    <h4><?php  echo USUARIOS_CREAR_USUARIO?></h4>
    <div class="container">
        <div class="span8 offset1">            
		<form class="form-horizontal" enctype="multipart/form-data" name="crearUsuario" id="crearUsuario" action="<?php  echo setUrl("usuarios","registrarUsuario")?>" method="post">
			<fieldset>
				<legend><?php echo USUARIOS_CREAR_USUARIO?></legend>
                <div class="control-group">
                    <label class="control-label" for="tipoIdent">Tipo de Identificaci&oacute;n: * </label>
                    <div class="controls">
                        <select id="tipoIdent" name="tipoIdent">
							<option value=""> -Seleccione- </option>
							<option value="CC"> Cedula </option>
						</select>
                    </div>
                </div>
				<div class="control-group">
                    <label class="control-label" for="ident">Identificaci&oacute;n * :</label>
                    <div class="controls">
                        <input type="text" id="ident" name="ident" placeholder="Identificaci&oacute;n">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="nombre">Nombre * :</label>
                    <div class="controls">
                        <input type="text" id="nombre" name="nombre" placeholder="Nombre">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="dir">Direcci&oacute;n * :</label>
                    <div class="controls">
                        <input type="text" name="dir" id="dir" placeholder="Direcci&oacute;n" >
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="telef">Tel&eacute;fono 1* :</label>
                    <div class="controls">
                        <input type="text" id="telef" name="telef" placeholder="Tel&eacute;fono 1">
                    </div>
                </div>
				<div class="control-group">
                    <label class="control-label" for="telef2">Tel&eacute;fono 2 :</label>
                    <div class="controls">
                        <input type="text" id="telef2" name="telef2" placeholder="Tel&eacute;fono 2">
                    </div>
                </div>
				<div class="control-group">
                    <label class="control-label" for="celular">Celular :</label>
                    <div class="controls">
                        <input type="text" id="celular" name="celular" placeholder="Celular">
                    </div>
                </div>
				<div class="control-group">
                    <label class="control-label" for="login">Login * :</label>
                    <div class="controls">
                        <input type="text" id="login" name="login" placeholder="Login">
                    </div>
                </div>
				<div class="control-group">
                    <label class="control-label" for="passwd">Contrase&ntilde;a * :</label>
                    <div class="controls">
                        <input type="password" id="passwd" name="passwd" placeholder="Contrase&ntilde;a">
                    </div>
                </div>
		<div class="control-group">
                    <label class="control-label" for="zona">Zona* :</label>
                    <div class="controls">
                        <select name="zona" id="zona">
							<option value="">-Seleccione-</option>
							<?php  echo $zonas?>
						</select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="sucursal">Oficina * :</label>
                    <div class="controls">
                        <select name="sucursal" id="sucursal">
							<option value="">-Seleccione-</option>
							<?php  echo $sucurs?>
						</select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="grupo">Grupo * :</label>
                    <div class="controls">
                        <select name="grupo" id="grupo">
							<option value="">-Seleccione-</option>
							<?php  echo $grupos?>
						</select>
                    </div>
                </div>
                                
                <div class="control-group">
                    <label class="control-label" for="grupo">Cargos * :</label>
                    <div class="controls">
                        <select name="cargo" id="cargo">
							<option value="">-Seleccione-</option>
							<?php  echo $cargos?>
						</select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="activo">Estado * :</label>
                    <div class="controls">
                        <select name="activo" id="activo">
                        	<option value=""> -Seleccione- </option>
                        	<option value="1"> Activo </option>
                        	<option value="0"> Inactivo </option>
                        </select>
                    </div>
                </div>
			</fieldset>
                <div class="control-group">
                    <div class="controls">                   
                        <button type="submit" class="btn btn-primary">Crear</button>
                    </div>
                </div>

            </form>
            </div>
        </div>
        <?php 
    } // crearUsuario
    
    
    public function editarUsuario($info, $grupos, $zonas, $sucursales){
    	echo getMensaje();
        ?>
           <h2><?php  echo USUARIOS_NOMBRE?></h2>
            <?php 
            navTabs(array(OP_BACK=>setUrl("usuarios","panelAdmin"), 
            		  USUARIOS_NOMBRE=>setUrl("usuarios"), 
            		  USUARIOS_EDITAR_USUARIO=>"#",
            		  USUARIOS_CREAR_USUARIO=>setUrl("usuarios","crearUsuario"),
            		 ),
            		  USUARIOS_EDITAR_USUARIO
            	   );
            
            navTabs(array(OP_BACK=>setUrl("usuarios","panelAdmin"), 
                          USUARIOS_NOMBRE=>setUrl("usuarios"),
            		  USUARIOS_EDITAR_USUARIO=>"#"
            		 ),
            	    USUARIOS_EDITAR_USUARIO, 
            	    "breadcrumb"
            	   );
            ?>
    <h4><?php  echo USUARIOS_EDITAR_USUARIO?></h4>
    <div class="container">
        <div class="span8 offset1">            
        <form class="form-horizontal" enctype="multipart/form-data" name="crearUsuario" id="editarUsuario" action="<?php  echo setUrl("usuarios","actualizarUsuario")?>" method="post">
            <fieldset>
                <legend><?php  echo USUARIOS_EDITAR_USUARIO?></legend>
                <div class="control-group">
                    <label class="control-label" for="tipoIdent">Tipo de Identificaci&oacute;n: * </label>
                    <div class="controls">
                        <select id="tipoIdent" name="tipoIdent">
                                <option value=""> -Seleccione- </option>
                                <option value="CC" <?php echo $info["tipoDoc"] == "CC" ? 'selected="selected"':''?>> Cedula </option>
                        </select>
                    </div>
                </div>
				<div class="control-group">
                    <label class="control-label" for="ident">Identificaci&oacute;n * :</label>
                    <div class="controls">
                        <input type="text" id="ident" name="ident" placeholder="Identificaci&oacute;n" value="<?php echo $info["documento"]?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="nombre">Nombre * :</label>
                    <div class="controls">
                        <input type="text" id="nombre" name="nombre" placeholder="Nombre" value="<?php echo $info["nombre"]?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="dir">Direcci&oacute;n * :</label>
                    <div class="controls">
                        <input type="text" name="dir" id="dir" placeholder="Direcci&oacute;n" value="<?php echo $info["direccion"]?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="telef">Tel&eacute;fono 1* :</label>
                    <div class="controls">
                        <input type="text" id="telef" name="telef" placeholder="Tel&eacute;fono 1" value="<?php echo $info["telef"]?>">
                    </div>
                </div>
				<div class="control-group">
                    <label class="control-label" for="telef2">Tel&eacute;fono 2 :</label>
                    <div class="controls">
                        <input type="text" id="telef2" name="telef2" placeholder="Tel&eacute;fono 2" value="<?php echo $info["telef2"]?>">
                    </div>
                </div>
				<div class="control-group">
                    <label class="control-label" for="celular">Celular :</label>
                    <div class="controls">
                        <input type="text" id="celular" name="celular" placeholder="Celular" value="<?php echo $info["celular"]?>">
                    </div>
                </div>
				<div class="control-group">
                    <label class="control-label" for="login">Login * :</label>
                    <div class="controls">
                        <input type="text" id="login" name="login" placeholder="Login" value="<?php echo $info["login"]?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="passwd">Contrase&ntilde;a * :</label>
                    <div class="controls">
                        <input type="password" id="passwd" name="passwd">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="correo">Correo :</label>
                    <div class="controls">
                        <input type="text" id="correo" name="correo" placeholder="Correo" value="<?php echo $info["correo"]?>">
                    </div>
                </div>
		<div class="control-group">
                    <label class="control-label" for="zona">Zona* :</label>
                    <div class="controls">
                        <select name="zona" id="zona">
							<option value="">-Seleccione-</option>
							<?php  echo $zonas?>
						</select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="sucursal">Oficina * :</label>
                    <div class="controls">
                        <select name="sucursal" id="sucursal">
							<option value="">-Seleccione-</option>
							<?php  echo $sucursales?>
						</select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="grupo">Grupo * :</label>
                    <div class="controls">
                        <select name="grupo">
							<option value="">-Seleccione-</option>
							<?php  echo $grupos?>
						</select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="activo">Estado * :</label>
                    <div class="controls">
                        <select name="activo" id="activo">
                        	<option value=""> -Seleccione- </option>
                        	<option value="1" <?php echo $info["activo"] == 1 ? 'selected="selected"':''?>> Activo </option>
                        	<option value="0" <?php echo $info["activo"] == 0 ? 'selected="selected"':''?>> Inactivo </option>
                        </select>
                    </div>
                </div>
			</fieldset>
                <div class="control-group">
                    <div class="controls">   
                        <input type="hidden" id="id" name="id" value="<?php  echo $info["id"]?>">                
                        <button type="submit" class="btn btn-primary">Modificar</button>
                    </div>
                </div>

            </form>
            </div>
        </div>
        <?php 
    } // editarUsuario
    
    
    public function verUsuario($info, $grupos, $zonas, $sucursales){
    	echo getMensaje();
        ?>
           <h2><?php  echo USUARIOS_NOMBRE?></h2>
            <?php 
            navTabs(array(OP_BACK=>setUrl("usuarios","panelAdmin"), 
            		  USUARIOS_NOMBRE=>setUrl("usuarios"), 
            		  USUARIOS_EDITAR_USUARIO=>"#",
            		  USUARIOS_CREAR_USUARIO=>setUrl("usuarios","crearUsuario"),
            		 ),
            		  USUARIOS_EDITAR_USUARIO
            	   );
            
            navTabs(array(OP_BACK=>setUrl("usuarios","panelAdmin"), 
                          USUARIOS_NOMBRE=>setUrl("usuarios"),
            		  USUARIOS_EDITAR_USUARIO=>"#"
            		 ),
            	    USUARIOS_EDITAR_USUARIO, 
            	    "breadcrumb"
            	   );
            ?>
    <h4><?php  echo USUARIOS_EDITAR_USUARIO?></h4>
    <div class="container">
        <div class="span8 offset1">            
        <form class="form-horizontal" enctype="multipart/form-data" name="crearUsuario" id="editarUsuario" action="<?php  echo setUrl("usuarios","actualizarUsuario")?>" method="post">
            <fieldset>
                <legend><?php  echo USUARIOS_EDITAR_USUARIO?></legend>
                <div class="control-group">
                    <label class="control-label" for="tipoIdent">Tipo de Identificaci&oacute;n: * </label>
                    <div class="controls">
                        <select id="tipoIdent" name="tipoIdent" disabled>
                                <option value=""> -Seleccione- </option>
                                <option value="CC" <?php echo $info["tipoDoc"] == "CC" ? 'selected="selected"':''?>> Cedula </option>
                        </select>
                    </div>
                </div>
				<div class="control-group">
                    <label class="control-label" for="ident">Identificaci&oacute;n * :</label>
                    <div class="controls">
                        <input disabled type="text" id="ident" name="ident" placeholder="Identificaci&oacute;n" value="<?php echo $info["documento"]?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="nombre">Nombre * :</label>
                    <div class="controls">
                        <input disabled type="text" id="nombre" name="nombre" placeholder="Nombre" value="<?php echo $info["nombre"]?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="dir">Direcci&oacute;n * :</label>
                    <div class="controls">
                        <input disabled type="text" name="dir" id="dir" placeholder="Direcci&oacute;n" value="<?php echo $info["direccion"]?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="telef">Tel&eacute;fono 1* :</label>
                    <div class="controls">
                        <input disabled type="text" id="telef" name="telef" placeholder="Tel&eacute;fono 1" value="<?php echo $info["telef"]?>">
                    </div>
                </div>
				<div class="control-group">
                    <label class="control-label" for="telef2">Tel&eacute;fono 2 :</label>
                    <div class="controls">
                        <input disabled type="text" id="telef2" name="telef2" placeholder="Tel&eacute;fono 2" value="<?php echo $info["telef2"]?>">
                    </div>
                </div>
				<div class="control-group">
                    <label class="control-label" for="celular">Celular :</label>
                    <div class="controls">
                        <input disabled type="text" id="celular" name="celular" placeholder="Celular" value="<?php echo $info["celular"]?>">
                    </div>
                </div>
				<div class="control-group">
                    <label class="control-label" for="login">Login * :</label>
                    <div class="controls">
                        <input disabled type="text" id="login" name="login" placeholder="Login" value="<?php echo $info["login"]?>">
                    </div>
                </div>
                <!--<div class="control-group">
                    <label class="control-label" for="passwd">Contrase&ntilde;a * :</label>
                    <div class="controls">
                        <input type="password" id="passwd" name="passwd">
                    </div>
                </div>-->
                <div class="control-group">
                    <label class="control-label" for="correo">Correo :</label>
                    <div class="controls">
                        <input disabled type="text" id="correo" name="correo" placeholder="Correo" value="<?php echo $info["correo"]?>">
                    </div>
                </div>
		<div class="control-group">
                    <label class="control-label" for="zona">Zona* :</label>
                    <div class="controls">
                        <select name="zona" id="zona" disabled>
                                <option value="">-Seleccione-</option>
                                <?php  echo $zonas?>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="sucursal">Oficina * :</label>
                    <div class="controls">
                        <select name="sucursal" id="sucursal" disabled>
                                <option value="">-Seleccione-</option>
                                <?php  echo $sucursales?>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="grupo">Grupo * :</label>
                    <div class="controls">
                        <select name="grupo" disabled>
                                <option value="">-Seleccione-</option>
                                <?php  echo $grupos?>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="activo">Estado * :</label>
                    <div class="controls">
                        <select name="activo" id="activo" disabled>
                        	<option value=""> -Seleccione- </option>
                        	<option value="1" <?php echo $info["activo"] == 1 ? 'selected="selected"':''?>> Activo </option>
                        	<option value="0" <?php echo $info["activo"] == 0 ? 'selected="selected"':''?>> Inactivo </option>
                        </select>
                    </div>
                </div>
			</fieldset>
                <div class="control-group">
                    <div class="controls">   
                        <input type="hidden" id="id" name="id" value="<?php  echo $info["id"]?>">                
                        <button type="submit" class="btn btn-primary">Modificar</button>
                    </div>
                </div>

            </form>
            </div>
        </div>
        <?php 
    } // verUsuario
    
    
    public function grupos($info=""){
      echo getMensaje();
        ?>
           <h2><?php  echo GRUPOS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl("parametros"), 
            		  USUARIOS_NOMBRE=>setUrl("usuarios","main"), 
            		  USUARIOS_CREAR_USUARIO=>setUrl("usuarios","crearUsuario"),
            		  USUARIOS_GRUPOS=>"#"
            		 ),
            	    USUARIOS_GRUPOS
            	   );
            
            navTabs(array(OP_BACK2=>setUrl("parametros"), 
            		  GRUPOS_NOMBRE=>'#'
            		 ),
            	    GRUPOS_NOMBRE, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo GRUPOS_NOMBRE?></h4>
       <div class='container'>
        <div class='span10 offset0'> 
        <form class="form-horizontal" name="new_grupo" id="new_grupo" action="<?php  echo setUrl("grupos","crearGrupo")?>" method="post">                    
			<fieldset>
				<legend> <?php  echo GRUPOS_CREAR?></legend>
				<div class="control-group">
					<label class="control-label" for="grupo">Grupo:</label>
					<div class="controls">
						<input type="text" id="grupo" name="grupo" placeholder="Grupo">
					</div>
				</div>  
				<div class="control-group">
					<label class="control-label" for="estado">Estado:</label>
					<div class="controls">
						<select name="estado" id="estado">
							<option value="">-Seleccione-</option>
							<option value="1">Activo</option>
							<option value="0">Inactivo</option>
						</select>
					</div>
				</div>  
				<div class="control-group">
					<div class="controls">                   
						<button type="submit" class="btn btn-primary">Crear</button>
					</div>
				</div>				
			</fieldset>				
		</form>
		<?php 
			if($info){
				echo datatable("lstGrupos");
		?>
			<table id="lstGrupos" class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Categoria</th>
                    <th>Permisos</th>
                    <th>Activo</th>
                    <th>Editar</th>
                    <th>Eliminar</th>
                </tr>
            </thead>
            <tbody>
                <?php  echo $info?>
            </tbody>
        </table>
		<?php 
			}else{
				echo "No existen grupos creados";
			}
		?>
        </div>
       </div>
       <?php  
    } // main
    
    	public function editGrupo($info){
      echo getMensaje();
        ?>
           <h2><?php  echo GRUPOS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('usuarios','grupos'),
            		  GRUPOS_EDITAR=>"#"
            		 ),
            	    GRUPOS_EDITAR
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('usuarios','grupos'),
            		  GRUPOS_EDITAR=>'#'
            		 ),
            	    GRUPOS_EDITAR, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo GRUPOS_EDITAR?></h4>
       <div class='container'>
        <div class='span7 offset1'> 
        <form class="form-horizontal" name="new_grupo" id="new_grupo" action="<?php  echo setUrl("grupos","updateGrupo")?>" method="post">                    
			<fieldset>
				<legend><?php  echo GRUPOS_EDITAR?></legend>
				<div class="control-group">
					<label class="control-label" for="grupo">Grupo:</label>
					<div class="controls">
						<input type="text" id="grupo" name="grupo" placeholder="Grupo" value="<?php  echo $info["grupo"]?>">
					</div>
				</div>  
				<div class="control-group">
					<label class="control-label" for="estado">Estado:</label>
					<div class="controls">
						<select name="estado" id="estado">
							<option value="">-Seleccione-</option>
							<option value="1" <?php  echo $info["activo"]==1? 'selected="selected"':''?>>Activo</option>
							<option value="0" <?php  echo $info["activo"]==0? 'selected="selected"':''?>>Inactivo</option>
						</select>
					</div>
				</div>  
			</fieldset>
				<div class="control-group">
					<div class="controls">                   
						<input type="hidden" id="id" name="id" value="<?php  echo $info["id_grupos"]?>">
						<button type="submit" class="btn btn-primary">Modificar</button>
					</div>
				</div>				
		</form>
        </div>
       </div>
       <?php  
    } // updGrupo
    
    public function gestionPermisos($listaModulos,$idGrupo,$grupo=null){
    	echo getMensaje();
        ?>
           <h2><?php  echo GRUPOS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl("usuarios","panelAdmin"), 
            		  GRUPOS_NOMBRE=>setUrl("grupos"),
    				  GRUPOS_GESTION_PERMISOS.$grupo=>"#"
            		 ),
            	    GRUPOS_GESTION_PERMISOS.$grupo
            	   );
            
            navTabs(array(OP_BACK=>setUrl("usuarios","panelAdmin"), 
            		  GRUPOS_NOMBRE=>setUrl("grupos"),
					  GRUPOS_GESTION_PERMISOS.$grupo=>"#"
            		 ),
            	    GRUPOS_GESTION_PERMISOS.$grupo, 
            	    "breadcrumb"
            	   );
            ?>
       <h4><?php  echo GRUPOS_GESTION_PERMISOS.$grupo?></h4>
	   <br />
       <div class="container">
        <div class="span7 offset1">
		
        <form class="form-horizontal" name="formPerms" id="formPerms" action="<?php  echo setUrl("grupos","registrarPermisos")?>" method="post">
		<table class="table table-striped table-hover table-condensed" id="permisos">
    			<thead><tr> 
    				<th> # </th>
    				<th> M&oacute;dulo </th>
    				<th> Acceso </th>
    				<th> M&aacute;s opciones </th>
    			</tr> </thead> <tbody>
					<?php  echo $listaModulos; ?>
				</tbody> </table>
        <div class="control-group">
             <div class="controls">                   
				<input type="hidden" name="idUser" value="<?php  echo $idGrupo?>">	
                                <button type="submit" class="btn btn-primary">Enviar</button>
             </div>
        </div>
        </form>
        </div>
       </div>
        <?php 
    } // gestionPermisos
    
    public function cargos($datos=null){
    	echo getMensaje();
        ?>
           <h2><?php  echo USUARIOS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl("parametros"), 
            		  USUARIOS_NOMBRE=>setUrl("usuarios"),
            		  USUARIOS_CREAR_USUARIO=>setUrl("usuarios","crearUsuario"),
            		  USUARIOS_CARGOS=>"#"
            		 ),
            	    USUARIOS_CARGOS
            	   );
            
            navTabs(array(OP_BACK2=>setUrl("parametros"), 
            		  USUARIOS_CARGOS=>"#"
            		 ),
            	    USUARIOS_CARGOS, 
            	    "breadcrumb"
            	   );
            ?>
       <h4><?php  echo USUARIOS_CARGOS?></h4>
       <div class="container">
        <div class="span10 offset0"> 
            <fieldset>
                    <legend><?php echo "Crear cargo"?></legend>
                      <?php
                            Html::openForm("cargos",setUrl("parametros","cargospro"));

                            Html::newInput("cargo","Cargo: *");
                      ?>
                    </fieldset>
                    <?php
                      Html::newButton("crear", "Crear", "submit");
                      Html::closeForm();
                    
                    if(!empty($datos)){
                            echo datatable("tabla");
                            Html::tabla(
                                    array("Cargo","Editar","Eliminar"),
                                                    $datos,
                                                    array("cargo"),
                                                    array("editar"=>setUrl("usuarios","editarCargos"),"eliminar"=>setUrl("parametros","eliminarCargos"))
                                            );
                    }else{
                      ?>
                      <div class="alert alert-info">No hay Cargos creados.</div>
                      <?php
                    }
            ?>
        </div>
       </div>
        <?php 
    } // CARGOS
    
    public function editarCargos($datos=null){
      echo getMensaje();
        ?>
           <h2><?php  echo USUARIOS_CARGOS_EDITAR?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('parametros'), 
                        USUARIOS_CARGOS_EDITAR=>'#'
            		 ),
            	    USUARIOS_CARGOS_EDITAR
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('parametros','cargos'), 
            		  USUARIOS_CARGOS_EDITAR=>'#'
            		 ),
            	    USUARIOS_CARGOS_EDITAR, 
            	    'breadcrumb'
            	   );
            ?>
		<h4><?php  echo USUARIOS_CARGOS_EDITAR?></h4>
		<div class='container'>
        <div class='span8 offset1'> 
		<fieldset>
                        <legend><?php echo USUARIOS_CARGOS_EDITAR?></legend>
		  <?php
			Html::openForm("cargos",setUrl("parametros","editarcargopro"));

			Html::newInput("cargo","Cargo: *",$datos['cargo']);
		  ?>
                </fieldset>
                <?php
		  Html::newHidden("id",$datos['id']);
		  Html::newButton("editar", "Editar", "submit");
		  Html::closeForm();
                ?>
        </div>
       </div>
       <?php  
    }
    
				}
