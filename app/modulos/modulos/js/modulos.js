            $(document).ready(function(){

                $("#new_modulo").validate({

                    rules:{

                        img:{required:true, accept:"image/*"},

                        titulo:{required:true,lettersonly:true},

						nombre:{required:true,lettersonly:true},

						desc:{required:true,lettersonly:true}

                    },messages: {

                        img:{required:'<div class="alert alert-error">Campo Imagen modulo no puede ser vacio.</div>'},

                        titulo:{required:'<div class="alert alert-error">Campo Titulo no puede ser vacio.</div>',lettersonly:'<div class="alert alert-error">No se pueden ingresar numeros.</div>'},

                        nombre:{required:'<div class="alert alert-error">Campo Nombre no puede ser vacio.</div>',lettersonly:'<div class="alert alert-error">No se pueden ingresar numeros.</div>'},

                        desc:{required:'<div class="alert alert-error">Campo Descripción no puede ser vacio.</div>',lettersonly:'<div class="alert alert-error">No se pueden ingresar numeros.</div>'}

                        

                    }



                });

                });