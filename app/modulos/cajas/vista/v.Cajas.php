<?php 

class vCajas{
    public function main($info,$users,$sucs,$abiertas){
      echo getMensaje();
        ?>
           <h2><?php  echo CAJAS?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
                            CAJAS=>'#',
                            CAJAS_PAGOS=>setUrl('cajas','pagos'),
            		 ),
            	    CAJAS
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  CAJAS=>'#'
            		 ),
            	    CAJAS, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo CAJAS_NOMBRE?></h4>
       <div class='container'>
        <div class='span8 offset1'> 
        <?php //if(getGrupoUsuario()=='1'){ ?>
                    <form action="<?php echo setUrl("cajas","guardar")?>" method="post" name="crear" id="crear" class="form-horizontal">
            <?php if(!$abiertas){ ?>
            <fieldset>
		<legend>Datos de Caja</legend>
			<!--<div class="control-group">
				<label class="control-label" for="sucursal">Sucursal:</label>	
				<div class="controls">
					<select id="sucursal" name="sucursal">
					<?php echo $sucs;?>
					</select>
				</div>
			</div>-->
			<div class="control-group">
			<label class="control-label" for="usuario">Empleado:</label>
				<div class="controls">
					<select id="usuario" name="usuario">
					<?php echo $users;?>
					</select>
				</div>
			</div>
			<div class="control-group">
			<label class="control-label" for="fecha">Fecha Apertura:</label>
				<div class="controls">
					<input type="text" id="fecha" name="fecha" placeholder="Fecha">
					<?php echo inputDate('fecha',false);?>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="monto">Monto Apertura ($):</label>	
				<div class="controls">
					<input type="text" id="monto" onkeyup="format(this)" name="monto" placeholder="Monto" class="campos">
					<input type="hidden" name="monto2" id="monto2">
					</select>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="activa">Activa:</label>	
				<div class="controls">
					<input type="checkbox" name="activa" value="1"> 
					</select>
				</div>
			</div>
			<div class="control-group">
                    <div class="controls">                   
                        <button type="submit" class="btn btn-primary">Crear</button>
                    </div>
                </div>
       
		</fieldset>
            <?php }else{ ?>
                        <div class="alert alert-info">No se pueden crear cajas por que hay cajas sin cerrar</div>
            <?php } ?>
                    </form>
	     <?php echo datatable('lst')?>
	    <table class="table table-hover" id="lst">
		<thead>
		  <tr>
		    <th>#</th>
		    <th>Empleado</th>
		    <!--<th>Sucursal</th>-->
		    <th>Monto de Apertura</th>
		    <th>Nro. Pagos</th>
		    <th>Valor Recaudado</th>
		    <th>Fecha Apertura</th>
		    <th>Estado</th>
                    <th>Detalle</th>
		    <th>Eliminar</th>
		  </tr>
		</thead>
		<tbody>
		  <?php
		  //die(print_r($info));
			if(!empty($info)){
			  echo $info;
			}else{
			  echo 'No hay datos';
			}
		  ?>
		</tbody>
	    </table>
	    
        </div>
       </div>
       <?php  
    }
    
    public function pagos(){
      echo getMensaje();
        ?>
           <h2><?php  echo CAJAS?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
                            CAJAS=>setUrl('cajas'),
                            CAJAS_PAGOS=>'#'
            		 ),
            	    CAJAS_PAGOS
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  CAJAS_PAGOS=>'#'
            		 ),
            	    CAJAS_PAGOS, 
            	    'breadcrumb'
            	   );
            ?>
		<h4><?php  echo CAJAS_PAGOS?></h4>
		<div class='container'>
        <div class='span11 offset0'> 
                <div class="control-group">
                    <label class="control-label" for="fecha">Buscar cliente:</label>
                    <div class="controls">
                        <input type="text" id="lstClientes" name="	" placeholder="Buscar por cc o nombre">
                        <input type="button" class="btn btn-primary" value="consultar" id="consultar">
                        <input type="reset" class="btn btn-warning" value="Limpiar" id="limp" onclick="limpiar()">
                    </div>
                </div>
             </div>
		</div>
		<div class="container">
			<div class="span11 offset0"> 
			
			<div class="lstCreditos" id="lstCreditos" >
			
			</div>
		</div>
       </div>			
	<?php  
    }
	
	public function verCuotas($info,$credito,$id){
		echo getMensaje();
        ?>
           <h2><?php  echo CAJAS?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('cajas','pagos'), 
                            CAJAS_CUOTA => '#',
                            CAJAS_ABONO => setUrl('cajas','pagoParcial',array('id'=>$id)),
                            CAJAS_TOTAL => setUrl('cajas','pagoTotal',array('id'=>$id))
            		 ),
            	    CAJAS_CUOTA
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('cajas','pagos'), 
            		  CAJAS_CUOTA=>'#'
            		 ),
            	    CAJAS_CUOTA, 
            	    'breadcrumb'
            	   );
            ?>
		<h4><?php  echo CAJAS_CUOTA?></h4>
		<div class='container'>
        <div class='span10 offset0'>
            <?php
            echo $info;
            ?>
        </div>
		
	<?php  
	}
	
	public function cerrarCaja($info,$date,$pagosT,$tpagos){
      echo getMensaje();
        ?>
           <h2><?php  echo CAJAS?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('cajas'), 
					  CAJAS_CERRAR=>'#'
            		 ),
            	    CAJAS_CERRAR
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('cajas'), 
            		  CAJAS_CERRAR=>'#'
            		 ),
            	    CAJAS_CERRAR, 
            	    'breadcrumb'
            	   );
            ?>
		<h4><?php  echo CAJAS_CERRAR?></h4>
		<div class='container'>
        <div class='span8 offset1'> 
<form action="<?php echo setUrl("cajas","cerrarCaja")?>" method="post" name="cerrar" id="cerrar" class="form-horizontal">
        <fieldset>
        <legend>Datos de Caja</legend>
                <div class="control-group">
                        <label class="control-label" for="fechaC">Fecha de Cierre:</label>	
                        <div class="controls">
                                <input type="text" id="fechaC" name="fechaC" value="<?php echo $date; ?>" readonly="readonly">
                        </div>
                </div>
        <?php
        $info['fechaA'] = explode('-',$info['fechaA']);
        $info['fechaA'] = $info['fechaA'][2].'-'.$info['fechaA'][1].'-'.$info['fechaA'][0];
        ?>
                <div class="control-group">
                        <label class="control-label" for="fechaA">Fecha de Apertura:</label>	
                        <div class="controls">
                                <input type="text" id="fechaA" name="fechaA" value="<?php echo $info['fechaA']; ?>" readonly="readonly">
                        </div>
                </div>
                <div class="control-group">
                <label class="control-label" for="montoA">Monto Apertura:</label>
                        <div class="controls">
                                <input type="text" id="montoA" name="montoA" value="<?php echo toMoney($info['montoA']); ?>" readonly="readonly">
                                <input type="hidden" id="montoA2" name="montoA2" value="<?php echo ($info['montoA']); ?>">
                        </div>
                </div>
                <?php
                    foreach ($tpagos As $key=>$tp):
                ?>
                <div class="control-group">
                    <label class="control-label" for="vlrRec">Total <?php echo ucwords($key)?> en Caja:</label>
                    <div class="controls">
                        <input type="text" id="vlrRec" name="vlrRec" value="<?php echo toMoney($tp); ?>" readonly="readonly">
                        <input type="hidden" id="efectivo" name="tp[]" value="<?php echo ($tp); ?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="vlrRec">Total <?php echo ucwords($key)?> Contado:</label>
                    <div class="controls">
                        <input type="text" id="tp2" name="tp2[]" required>
                    </div>
                </div>
                <?php
                    endforeach;
                ?>
                <!--
                <div class="control-group">
                <label class="control-label" for="vlrRec">Total Efectivo en Caja:</label>
                        <div class="controls">
                            <input type="text" id="vlrRec" name="vlrRec" value="<?php echo toMoney($pagoE); ?>" readonly="readonly">
                            <input type="hidden" id="efectivo" name="efectivo" value="<?php echo ($pagoE); ?>">
                        </div>
                </div>
        
                <div class="control-group">
                <label class="control-label" for="vlrRec">Total Efectivo Contado:</label>
                        <div class="controls">
                            <input type="text" id="efectivoCont" name="efectivoCont" required>
                        </div>
                </div>
        
                <div class="control-group">
                        <label class="control-label" for="esperado">Total cheque en Caja:</label>	
                        <div class="controls">
                            <input type="text" id="esperado"  name="esperado" value="<?php echo toMoney($pagoC) ?>" readonly="readonly">
                            <input type="hidden" name="cheque" id="cheque" value="<?php echo ($pagoC); ?>">
                        </div>
                </div>
        
                <div class="control-group">
                <label class="control-label" for="chequeCont">Total cheque Contado:</label>
                        <div class="controls">
                            <input type="text" id="chequeCont" name="chequeCont" required>
                        </div>
                </div>
        
                <div class="control-group">
                        <label class="control-label" for="esperado">Total consignacion en Caja:</label>	
                        <div class="controls">
                            <input type="text" id="esperado"  name="esperado" value="<?php echo toMoney($pagoCo) ?>" readonly="readonly">
                            <input type="hidden" name="consignacion" id="consignacion" value="<?php echo ($pagoCo); ?>">

                        </div>
                </div>
        
                <div class="control-group">
                <label class="control-label" for="consignacionCont">Total consignacion Contado:</label>
                        <div class="controls">
                            <input type="text" id="consignacionCont" name="consignacionCont" required>
                        </div>
                </div>-->
        
                <div class="control-group">
                        <label class="control-label" for="esperado">Total Recaudado:</label>	
                        <div class="controls">
                                <input type="text" id="esperado"  name="esperado" value="<?php echo toMoney($pagosT); ?>" readonly="readonly">
                                <input type="hidden" name="esperado2" id="esperado2" value="<?php echo ($pagosT); ?>">

                        </div>
                </div>
                <!--<div class="control-group">
                    <label class="control-label" for="esperado">Total caja:</label>	
                    <div class="controls">
                            <input type="text" id="esperado"  name="esperado" value="<?php echo toMoney($pagosT); ?>" readonly="readonly">
                            <input type="hidden" name="esperado22" id="esperado22" value="<?php echo ($pagosT); ?>">

                    </div>
                </div>-->
                <!--<div class="control-group">
                <label class="control-label" for="dif">Total Diferencia:</label>
                        <div class="controls">
                                <input type="text" id="dif" name="dif" value="" readonly="readonly">
                                <input type="hidden" name="id" id="id" value="<?php echo $info['id']; ?>">
                        </div>
                </div>-->
                <div class="control-group">
                <label class="control-label" for="cont">Total Dinero Contado:</label>
                        <div class="controls">
                            <input type="text" id="cont" name="cont" class="campos" >
                                <input type="hidden" name="cont2" id="cont2">
                                <input type="hidden" name="id" id="id" value="<?php echo $info['id']; ?>">
                                <button type="button" id="comprobar" onclick="comprobar">Generar Cuadre</button>
                        </div>
                </div>
                <!--div class="control-group">
                        <label class="control-label" for="activa">Abierta:</label>	
                        <div class="controls">
                                <input type="checkbox" name="activa" value="1"> 
                                <input type="hidden" name="id" id="id" value="<?php echo $info['id']; ?>">
                        </div>
                </div-->
                <div class="control-group">
                <div class="controls" id="button" name="button">                   
                    <!--button type="hidden" class="btn btn-primary">Guardar</button-->

                </div>
                </div>
        </fieldset>
</form>
</div>
        </div>
	<?php  
    }
    
    public function pagoParcial($id,$id2,$total){
      echo getMensaje();
        ?>
           <h2><?php  echo CAJAS?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('cajas','pagos'), 
                            CAJAS_CUOTA => setUrl('cajas','verCuotas',array('id'=>$id)),
                            CAJAS_ABONO => '#',
                            CAJAS_TOTAL => setUrl('cajas','pagoTotal',array('id'=>$id))
            		 ),
            	    CAJAS_ABONO
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('cajas','pagos'), 
            		  CAJAS_ABONO=>'#'
            		 ),
            	    CAJAS_ABONO, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo CAJAS_ABONO?></h4>
       <div class='container'>
			<div class='span7 offset2'>
				<?php
					Html::OpenForm("abonos",setUrl("cajas","saveAbono"));
				?>
			<fieldset>
                        <legend><?php  echo CAJAS_NOMBRE?></legend>
                                <?php

                                        //Html::newReadonly("totalPago","Total a pagar","text",($total));
                                        Html::newInput("abono","Valor abono");
                                        echo selectMedios();
                                ?>
                        </fieldset>
                        <fieldset>
                            <div class="control-group" id="adj">
                                <label for="adjuntos1" class="control-label">Adjuntos : </label>
                                <div class="controls" id="nuevo">
                                    <input type="file" name="adjuntos1[]" id="adjuntos1">
                                    <input type="text" name="nadjunto[]" id="nadjunto" placeholder="Nombre archivo">
                                    <a class="btn clonar" onclick="clonar(this)">+</a>
                                    <a class="btn remover" onclick="remover(this)">-</a>
                                </div>
                            </div>
                        </fieldset>
                                <?php
                                        Html::newHidden("total", $total);
                                        Html::newHidden("id", $id);
                                        Html::newHidden("id2", $id2);
                                        Html::newButton("enviar","Aplicar abono");
                                        Html::closeForm();
                                ?>
			</div>
		</div>
       <?php  
    }
    
        public function pagarCuota($info,$id,$total,$estado){
      echo getMensaje();
        ?>
           <h2><?php  echo CAJAS?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('cajas','verCuotas',array('id'=>$id))
            		 ),
            	    CAJAS
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('cajas','verCuotas',array('id'=>$id)), 
            		  CAJAS=>'#'
            		 ),
            	    CAJAS, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo CAJAS_NOMBRE?></h4>
       <div class='container'>
			<div class='span10 offset1'>
                        <?php
                        Html::OpenForm("pago",setUrl("cajas","saveCuota"));
                        ?>
			<fieldset>
                        <legend><?php  echo CAJAS_NOMBRE?></legend>
                                <?php
                                //$total = $info['cuotaFija']+$info['interes_dia']+$info['otros_total'];
                                //die($info['cuotaFija'].'+'.$info['interes_dia'].'+'.$info['otros_total']);
                                Html::newReadonly("totalPago2","Total a pagar","text",toMoney($total));
                                Html::newHidden("totalPago",($total));
                                //Html::newInput("recibo","Numero recibo");
                                Html::newInput("abono","Valor pago",'','required');
                                echo selectMedios();
                                ?>
                        </fieldset>
                            <fieldset>
                                <div class="control-group" id="adj">
                                    <label for="adjuntos1" class="control-label">Adjuntos : </label>
                                    <div class="controls" id="nuevo">
                                        <input type="file" name="adjuntos1[]" id="adjuntos1">
                                        <input type="text" name="nadjunto[]" id="nadjunto" placeholder="Nombre archivo">
                                        <a class="btn clonar" onclick="clonar(this)">+</a>
                                        <a class="btn remover" onclick="remover(this)">-</a>
                                    </div>
                                </div>
                            </fieldset>
                                <?php
                                        Html::newHidden("id", $info['id']);
                                        Html::newHidden("id2", $id);
                                        Html::newHidden("estado",$estado);
                                        Html::newButton("enviar","Aplicar pago");
                                        Html::closeForm();
                                ?>
			</div>
		</div>
       <?php  
    }
    
        public function pagoTotal($info,$total,$id,$url="pagos"){
      echo getMensaje();
        ?>
           <h2><?php  echo CAJAS?></h2>

            <?php 
             navTabs(array(OP_BACK2=>setUrl('cajas','pagos'), 
                            CAJAS_CUOTA => setUrl('cajas','verCuotas',array('id'=>$id)),
                            CAJAS_ABONO => setUrl('cajas','pagoParcial',array('id'=>$id)),
                            CAJAS_TOTAL => '#'
            		 ),
            	    CAJAS_TOTAL
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('cajas','pagos'), 
            		  CAJAS_TOTAL=>'#'
            		 ),
            	    CAJAS_TOTAL, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo CAJAS_TOTAL?></h4>
       <div class='container'>
			<div class='span10 offset1'>
                        <!--<a href="<?php echo setUrl("cajas","generarRecibo",array("id"=>$info['id'],"jquery"=>1,"total"=>($capital+$interes)))?>" class="btn btn-primary">Generar recibo</a>-->
				<?php
					Html::OpenForm("pagos",setUrl("cajas","savePagototal"));
				?>
			<fieldset>
                        <legend><?php  echo CAJAS_NOMBRE?></legend>
                                <?php
                                Html::newReadonly("totalPago","Total a pagar","text",toMoney($total));
                                //Html::newInput("recibo","Numero recibo");
                                Html::newInput("abono","Valor a pagar");
                                echo selectMedios();
                                ?>
                        </fieldset>
                            <fieldset>
                                <div class="control-group" id="adj">
                                    <label for="adjuntos1" class="control-label">Adjuntos : </label>
                                    <div class="controls" id="nuevo">
                                        <input type="file" name="adjuntos1[]" id="adjuntos1">
                                        <input type="text" name="nadjunto[]" id="nadjunto" placeholder="Nombre archivo">
                                        <a class="btn clonar" onclick="clonar(this)">+</a>
                                        <a class="btn remover" onclick="remover(this)">-</a>
                                    </div>
                                </div>
                            </fieldset>
                            
                                <?php
                                        Html::newHidden("total", $total);
                                        Html::newHidden("id", $id);
                                        Html::newButton("enviar","Aplicar pago total");
                                        Html::closeForm();
                                ?>
			</div>
		</div>
       <?php  
    }
    
    public function pagoTotal2($info,$total,$id,$imp,$iva,$totalP){
      echo getMensaje();
        ?>
           <h2><?php  echo CAJAS?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('cajas','pagos')
            		 ),
            	    CAJAS
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('cajas','pagos'), 
            		  CAJAS=>'#'
            		 ),
            	    CAJAS, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo CAJAS_TOTAL?></h4>
       <div class='container'>
			<div class='span10 offset1'>
                        <!--<a href="<?php echo setUrl("cajas","generarRecibo",array("id"=>$info['id'],"jquery"=>1,"total"=>($capital+$interes)))?>" class="btn btn-primary">Generar recibo</a>-->
				<?php
					Html::OpenForm("pagos",setUrl("cajas","savePagototal2"));
				?>
			<fieldset>
                        <legend><?php  echo CAJAS_NOMBRE?></legend>
                                <?php
                                        Html::newInput("recibo","Numero recibo");
                                        Html::newReadonly("totalPago","Valor servicio","text",toMoney($total));
                                        $di = ($imp==true)?'disabled':'';
                                        Html::newInput("desc","Descuento","",$di);
                                        Html::newInput("sub","Subtotal a pagar","","readonly");
                                        Html::newHidden("sub2");
                                        Html::newInput("impuesto","I.T.B.M",  toMoney($iva),"readonly");
                                        Html::newHidden("impuesto2",$iva);
                                        Html::newInput("totalP","Total a pagar",  toMoney($totalP),"readonly");
                                        Html::newHidden("totalP2",$totalP);
                                        Html::newInput("pago","Valor");
                                        echo selectMedios();
                                        /*Html::newSelect('medio','Medio de pago',array(
                                            'efectivo'=>'Efectivo',
                                            'cheque'=>'Cheque',
                                            'consignacion'=>'Consignacion'
                                        ));*/
                                ?>
                        </fieldset>
                        <fieldset>
                            <div class="control-group" id="adj">
                                <label for="adjuntos1" class="control-label">Adjuntos : </label>
                                <div class="controls" id="nuevo">
                                    <input type="file" name="adjuntos1[]" id="adjuntos1">
                                    <input type="text" name="nadjunto[]" id="nadjunto" placeholder="Nombre archivo">
                                    <a class="btn clonar" onclick="clonar(this)">+</a>
                                    <a class="btn remover" onclick="remover(this)">-</a>
                                </div>
                            </div>
                        </fieldset>
                                <?php
                                        Html::newHidden("total", $total);
                                        Html::newHidden("iva", $info['iva']);
                                        Html::newHidden("imp", $imp);
                                        Html::newHidden("id", $id);
                                        Html::newButton("enviar","Aplicar pago");
                                        Html::closeForm();
                                ?>
			</div>
		</div>
       <?php  
    }
    
    public function detalle($info,$valores){
      echo getMensaje();
        ?>
           <h2><?php  echo CAJAS?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('cajas'), 
                        CAJAS_VER=>'#'
            		 ),
            	    CAJAS_VER
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('cajas'), 
            		  CAJAS_VER=>'#'
            		 ),
            	    CAJAS_VER, 
            	    'breadcrumb'
            	   );
            ?>
		<h4><?php  echo CAJAS_VER?></h4>
		<div class='container'>
        <div class='span7 offset3'> 
            
                <div id="imp" class="btn btn-primary">Imprimir</div>
            <fieldset id="imprimir">

        <legend>Datos de Caja</legend>
                <div class="control-group">
                        <label class="control-label" for="fechaA">Fecha de Apertura:</label>	
                        <div class="controls">
                                <input type="text" id="fechaA" name="fechaA" value="<?php echo $info['fechaA']; ?>" readonly="readonly">
                        </div>
                </div>
                <div class="control-group">
                        <label class="control-label" for="fechaC">Fecha de Cierre:</label>	
                        <div class="controls">
                                <input type="text" id="fechaC" name="fechaC" value="<?php echo $info['fechaC']; ?>" readonly="readonly">
                        </div>
                </div>
                <div class="control-group">
                        <label class="control-label" for="fechaC">Fecha de Cierre:</label>	
                        <div class="controls">
                                <input type="text" id="fechaC" name="fechaC" value="<?php echo $info['fechaC']; ?>" readonly="readonly">
                        </div>
                </div>
                <div class="control-group">
                <label class="control-label" for="montoA">Monto Apertura:</label>
                        <div class="controls">
                                <input type="text" id="montoA" name="montoA" value="<?php echo toMoney($info['montoA']); ?>" readonly="readonly">
                                <input type="hidden" id="montoA2" name="montoA2" value="<?php echo ($info['montoA']); ?>">
                        </div>
                </div>
                                <?php
                    foreach ($valores As $key=>$tp):
                ?>
                <div class="control-group">
                    <label class="control-label" for="vlrRec">Total <?php echo ucwords($key)?> en Caja:</label>
                    <div class="controls">
                        <input type="text" id="vlrRec" name="vlrRec" value="<?php echo toMoney($tp); ?>" readonly="readonly">
                        <input type="hidden" id="efectivo" name="tp[]" value="<?php echo ($tp); ?>">
                    </div>
                </div>
                <?php
                    endforeach;
                ?>
                <div class="control-group">
                        <label class="control-label" for="esperado">Total Recaudado:</label>	
                        <div class="controls">
                                <input type="text" id="esperado"  name="esperado" value="<?php echo toMoney($info['vlrRec']); ?>" readonly="readonly">
                        </div>
                </div>

        </fieldset>

</div>
        </div>
	<?php  
    }
    
} // class