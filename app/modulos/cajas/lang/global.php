<?php 
/*Variables globales para el modulo lang*/
define('CAJAS','Cajas');
define('CAJAS_NOMBRE','Cajas de Recaudo');
define('CAJAS_PAGOS','Registrar pagos');
define('CAJAS_CERRAR','Cerrar Caja');
define('CAJAS_VER','Detalle de Caja');
define('CAJAS_EDITAR','Editar Caja');
define('CAJAS_REPORTE','Reporte Cajas');
define('CAJAS_TOTAL','Pago total');
define('CAJAS_ABONO','Abonos');
define('CAJAS_CUOTA','Pago Cuotas');
