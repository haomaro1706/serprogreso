
/*javascript para el modulo js/*/
$(function(){
	$(".campos").keyup(function(){
		valor = $(this).val();
		$(this).next().val(valor);
	});
        
        $("#desc").keyup(function(){
		valor = $(this).val();
                total = $("#total").val();
                iva = parseInt($("#iva").val())/100;
                ntotal = total-valor;
                
                $("#sub").val(toMoney(ntotal));
                $("#sub2").val((ntotal));
                impuesto = Math.round(ntotal*iva);
                $("#impuesto").val(toMoney(impuesto));
                $("#impuesto2").val((impuesto));
                $("#totalP").val(toMoney(ntotal+impuesto));
                $("#totalP2").val((ntotal+impuesto));
		
	});
        
        $("#imp").click(function(){
            
var cadena1='<!DOCTYPE html>\n\
<html lang="es">\n\
<head>\n\
<title>Panel de Administraci&oacute;n - Caminos de la paz</title>\n\
<base href="http://caminosdelapaz.com/app/">\n\
<meta charset="utf-8" /><link rel="stylesheet" href="app/plantillas/Administrador/css/estilo.css" />\n\
<link rel="stylesheet" href="app/librerias/bootstrap/css/bootstrap.css" />\n\
</head>\n\
<body>';
var cadena2='</body></html>';
            var ficha=cadena1+$("#imprimir").html()+cadena2;
            var ventimp=window.open('','popimpr');
            ventimp.document.write(ficha);
            ventimp.document.close();
            ventimp.print();
            ventimp.close();
        });
            
/*var name = "visualizador-pdf";
window.frames[name].focus();
window.frames[name].print();
return false;*/

});

function formato(input){
	numero = input.value.split(",");
	dec = numero[1] != undefined ? ","+numero[1] : '';
	var num = numero[0].replace(/\./g,'');

	if(!isNaN(num)){
	   num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
	   num = num.split('').reverse().join('').replace(/^[\.]/,'');
	   input.value = num+dec;
	}
	
	else{ alert('Solo se permiten numeros');
	    input.value = input.value.replace(/[^\d\.]*/g,'');
	}
}

function limpiar(){
    $(function(){
        $("#lstClientes").val('');
    });
}

$(document).ready(function() {
	
        $("#abonos").submit(function(){
            if(parseInt($("#abono").val()) > parseInt($("#total").val())){
                alert("El monto maximo a abonar es de " + toMoney($("#total").val()))
                return false;
            }
	})
        
	$("#comprobar").click(function(){	
		data1=($("#esperado2").val());
		data2=($("#cont2").val());
		dif=Math.abs(data1-data2);
		$("#dif").val(dif);
		if(data1===data2){
			alert("Puede proceder a cerrar la caja");
			$("#button").html('<button type="submit" class="btn btn-primary">Cerrar Caja</button>');
		}else{
			alert("Existe una diferencia frente al valor esperado ");
			$("#button").html('');
		}
	});
      

	$("#consultar").click(function(){	
		data=$("#lstClientes").val();
		data1 = data.split('|')[0];
		data2= $("#consultar").val();
		//alert(data2);
		$("#lstCreditos").html("");
		
		$.post(URL()+"index.php/cajas/consultar",{id:data1,jquery:1},function(data){
			$("#lstCreditos").find("tbody").remove();
			$("#lstCreditos").append(data);
		})
	});
            
	$.post('index.php/clientes/lstClientes',{jquery:1,estado:1},function(data){
		var availableTags = data.split(',');
		
		$( "#lstClientes" ).autocomplete({
			source: availableTags,
			select: function( event, ui ) {
			id = ui.item.value.split('-');
			$.post('cajas.php/infCredito',{jquery:2,id:id[0]},function(data){
				
			});
			/*$.post('clientes.php/clienteId',{jquery:1,id:id[0]},function(data3){
						
					});*/
			}
		});
	});
	
        imp = $("#imp").val();
        if(imp==='1'){
            impuesto = 0;
        }else{
            impuesto = parseInt($("#total").val())*0.07;
        }
        /*$("#impuesto").val(toMoney(Math.round(impuesto)));
        $("#impuesto2").val(Math.round(impuesto));*/
	});