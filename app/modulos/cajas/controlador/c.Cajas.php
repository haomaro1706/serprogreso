<?php 



include_once 'app/modulos/cajas/vista/v.Cajas.php';

include_once 'app/modulos/cajas/modelo/m.Cajas.php';

include_once 'app/modulos/usuarios/modelo/m.Usuarios.php';

include_once 'app/modulos/sucursales/modelo/m.Sucursales.php';

include_once 'app/modulos/bancos/modelo/m.Bancos.php';

include_once 'app/modulos/cartera/controlador/c.Cartera.php';

include_once 'app/modulos/clientes/controlador/c.Clientes.php';

include_once 'app/modulos/solicitudes/modelo/m.Solicitudes.php';



class Cajas extends vCajas{



    public function main(){

        /*Funcion Principal*/

		validar_usuario();

		$info2 = mCajas::lstCajas2();

		$info = mCajas::lstCajas();

                $dif=0;

                $abiertas=false;

                if(is_array($info2)){

                    foreach ($info2 As $i){

                         $fecha = $i['fechaA'];

                         $dif += (difFechas($fecha,opDate()));

                    }

                    if($dif>0){

                        $abiertas=true;

                    }

                }

		//die(print_r($info));

		$lst = '';

		$c = 1;

		if (is_array($info)){

			foreach ($info as $key){

				$lst .= '<tr><td>'.$c++.'</td>';

				$lst .= '<td>'.$key["empleado"].'</td>';

				//$lst .= '<td>'.$key["sucursal"].'</td>';

				$lst .= '<td>'.toMoney($key["montoA"]).'</td>';

				$lst .= '<td>'.$key["nroPagos"].'</td>';

				$lst .= '<td>'.toMoney($key["vlrRec"]).'</td>';

				$lst .= '<td>'.$key["fechaA"].'</td>';

                                

				$url = ($key["estado"]==1)? setUrl("cajas","activarCaja", array("id"=>$key["id"], "estado"=>$key["estado"])):"";

				

                                $estado = ($key["estado"]==1)? "Cerrar Caja":"Caja Cerrada";

				$imgActivo = ($key["estado"]==1)? "activo":"inactivo";

				if($key["estado"]==1){

                                    $lst .= '<td> <a href="'.$url.'" title="'.$estado.'"> <img src="'.URLADMIN().'app/img/'.$imgActivo .'.png" alt="'.$imgActivo.'"> </a> </td>';

                                }else{

                                    $lst .= '<td>Caja cerrada</td>';

                                }

                                $lst .= '<td> <a href="'.setUrl('cajas','detalle',array('id'=>$key["id"])).'" title="Detalle"> <img src="'.URLADMIN().'app/img/ver.png"> </a> </td>';

				$lst .= '<td> <a onclick="return confirm(\'Está seguro de eliminar la Caja y su informacion?\');" href="'.setUrl("cajas","eliminarCaja", array("id"=>$key["id"])).'" title="Eliminar "> <img src="'.URLADMIN().'app/img/eliminar.png" alt="Eliminar"> </a> </td>';

			}

		}



                $usuarios = mUsuarios::lstUsers();

		$users='<option value = ""> - Seleccione - </option>';

		if (is_array($usuarios)){

			foreach($usuarios as $key){

				$users .= '<option value = "'.$key['id'].'">'.$key['nombre'].'</option>';

			}

		}else{

			$users .= '<option value = ""> - Sin Usuarios Activos - </option>';

		}

		$sucursales = mSucursales::lstSuc();

		$sucs='<option value = ""> - Seleccione - </option>';

		if (is_array($sucursales)){

			foreach($sucursales as $key){

				$sucs .= '<option value = "'.$key['id'].'">'.$key['nombre_suc'].'</option>';

			}

		}else{

			$sucs .= '<option value = ""> - Sin Sucursalaes Registradas - </option>';

		}

		//die(print_r($info));

        parent::main($lst,$users,$sucs,$abiertas);    

    }

    

    public function pagos(){

		validar_usuario();

                $caja = getCaja();

                //die('='.$caja);

                if(!empty($caja)){

                    parent::pagos();

                }else{

                    setMensaje('No se pueden realizar pagos porque no hay cajas abiertas.','info');

                    location(setUrl('cajas'));

                }

	}

	

	public function consultar($array){

		validar_usuario();

		$id =  explode('-',$array['id']);

                $id = trim($id[0]);

		$lst=mCajas::consulta($id);

		//die(print_r($lst));

		$cadena = '';

		if(is_array($lst)){

			$cadena .= datatable('lst').'<table class="table  table-hover" id="listaTerProv">

    			<thead>

                            <tr> 

    				<th> # </th>

                                <th> Cliente </th>

                                <th> Producto </th>

                                <th> Fecha desembolso </th>

                                <th> Valor solicitado </th>

                                <!--<th> Valor cuota mensual</th>

                                <th> Linea </th>-->

                                <th> Pagos </th>

                                <!--<th> Acciones </th>-->

                            </tr> 

                        </thead>

                        <tbody>';

			$i=1;

			foreach($lst as $key){

                            

				$cadena.='<td>'.$i++.'</td>';

				$cadena.='<td>'.$key['nombre'].'</td>';

				$cadena.='<td>'.$key['nomProducto'].'</td>';

				$cadena.='<td>'.$key['fecha_desembolso'].'</td>';

				

                                //if($key['linea']>1){

                                    $cadena.='<td>'.toMoney($key['monto']).'</td>';

                                    //$cadena.='<td>'.toMoney($key['cuota']).'</td>';

                                    //$cadena.='<td>'.($key['lalinea']).'</td>';

                                    $cadena.='<td> <a href="'.setUrl("cajas","verCuotas", array("id"=>$key["elid"])).'" title="Ver Solicitud"> <img src="'.URLADMIN().'app/img/ver.png" alt="Pagos"> </a> </td>';

                                    //$cadena.='<td> <a href="'.setUrl("cajas","pagoTotal", array("id"=>$key["elid"])).'" title="Pago total"> Abonar </a> </td></tr>';

                               /* }else{

                                    $abono = mCajas::abonos($key['elid']);

                                    $abono = $abono[0];

                                    

                                    $elabono = 0;

                                    $desc = 0;

                                    $iva =0;

                                    if($abono['abono']>0){

                                        $elabono = $abono['abono'];

                                        $desc = $abono['descu'];

                                        $iva = ($key['valor_real']-$desc)*0.07;

                                    }



                                    $cadena.='<td>'.toMoney(($key['valor_real']-$desc)+$iva-$elabono).'</td>';

                                    $cadena.='<td>'.toMoney(0).'</td>';

                                    $cadena.='<td>'.($key['lalinea']).'</td>';

                                    $cadena.='<td>Plan sin cuotas</td>';

                                    $cadena.='<td> <a href="'.setUrl("cajas","pagoTotal2", array("id"=>$key["elid"])).'" title="Pago total"> Pagar </a> </td></tr>';

                                }*/

				

			}

		}else{

			$cadena .= 'No se encontraron cr&eacute;ditos activos para ese cliente';

		}

		

		echo utf8_encode($cadena);

		

	}

	

	public function verCuotas($array){

            validar_usuario();

            

            if (isset($array['id']) && is_numeric($array['id'])){

                

            //$info = mCajas::corte($array['id']);

            $info = mSolicitudes::infoCuotas2($array["id"]);

            $info2 = mSolicitudes::infoDesembolsoDesc($array["id"]);

                

            $cadena =   '';

            $pagada =   false;

            $i  =   1;

                /*

                $tOtros = 0;

                foreach($info as $key):

                    $otros = unserialize($key['otros']);



                    foreach ($otros As $o):

                        $tOtros += $o;

                    endforeach;



                    if($key['cuota']==0){

                        $capital    = 0;

                        $estado     = 1;

                    }else{

                        $capital    = 0;

                        $estado     = 2;

                    }

                    

                    $total = $capital+$key['ajuste_interes']+$key['interes']+$tOtros;

                    

                    $cadena.='<td>'.($i).'</td>';

                    $cadena.='<td>'.toMoney($total).'</td>';

                    $cadena.='<td>'.opDate().'</td>';

                    $cadena.='<td>'.$key['fecha_corte'].'</td>';



                    $imgActivo = 'realizado';



                        $cadena.='<td>'

                        . '<a href="'.

                        setUrl("cajas","pagarCuota",array("id"=>$key["id"],"id2"=>$array['id'],"total"=>$total,"estado"=>$estado)).

                        '"> <img src="'.URLADMIN().'app/img/'.$imgActivo.'.png" alt="Pagar cuota"> </a>'

                        . '</td>';



                    $cadena .='</tr>';

                    $i++;

                endforeach;*/



                $cadena = '<table class="table table-hover">

		<thead>

		  <tr>

                    <th>Cuota</th>

		    <th>Fecha corte</th>

		    <th>Fecha pago</th>

		    <th>Cuota fija</th>

		    <th>Saldo a capital</th>

		    <th>Interes corriente</th>

		    <th>Interes dias</th>

		    <th>Aporte a capital</th>';



                    if(is_array($info)){

                        $otros = unserialize($info2[0]['otros']);

                        $des = unserialize($info2[0]['otros_des']);

                        if(is_array($otros)){

                            $i=0;

                            foreach ($otros As $key=>$o):

                                if($des[$i]=="1"){

                                    $cadena .= '<th>'.htmlentities(ucwords($key)).'</th>';

                                }

                                $i++;

                            endforeach;

                        }

                    }

                $cadena .= '

                    <th>Total cuota</th>

                    <th>Pagar</th>

                    <th>Estado</th>

		  </tr>

		</thead>

		<tbody>';

                //print_r($info);

			if(is_array($info)){

                            $j=1;

		    foreach ($info As $i):

                  $cadena .= '

		  <tr>

                    <td>'.$j.'</td>

		    <td>'.$i['fechaCorte'].'</td>

		    <td>'.$i['fechaPago'].'</td>

		    <td>'.toMoney($i['cuotaFija']).'</td>

		    <td>'.toMoney($i['capital']).'</td>

		    <td>'.toMoney($i['interes']).'</td>

		    <td>'.toMoney($i['interes_dia']).'</td>

		    <td>'.toMoney($i['aporteCapital']).'</td>';



                    $otros = unserialize($info2[0]['otros']);

                    $des = unserialize($info2[0]['otros_des']);

                    

                    if(is_array($otros)){

                         $l=0;

                        foreach ($otros As $o):

                            if($des[$l]=="1"){

                                $cadena .= '<td>'.toMoney($o*($i['elcapital']/100)).'</td>';

                            }

                            $l++;

                        endforeach;

                    }

                    $total = $i['cuotaFija']+$i['otros_total']+$i['interes_dia'];

                    

		    $cadena .= '<td>'.toMoney($total).'</td>';



                    $estado = ($i['estado']=='1')?'Sin pagar':'Pagada';

                    

                    $cadena .= '<td>';

                    if($i['estado']=='1'){

                        $cadena .= '<a href="'.

                        setUrl("cajas","pagarCuota",array("id"=>$array['id'],"id2"=>$array['id'],"total"=>$total)).

                        '"> <img src="'.URLADMIN().'app/img/activo.png" alt="Pagar cuota"> </a>'

                        . '</td>';

                    }else{

                        $cadena .= '<td> - </td>';

                    }

                    $cadena .= '<td>'.$estado.'</td>';

                    

                    $j++;

		    endforeach;

			}else{

			  $cadena .= 'No hay datos';

			}

                  $cadena .='

		</tbody>

	    </table>

           </div>';

                

            parent::verCuotas($cadena,$info,$array['id']);

        }

    }

	

	public function pagarCuota($array){

            validar_usuario();

            $caja = getCaja();

            if(!empty($caja)){

                $info = mSolicitudes::infoCuotas2($array['id']);

                //die(print_r($array));

                parent::pagarCuota($info[0],$array['id2'],$array['total'],2);

            }else{

                

                setMensaje('No se puede realizar el pago por que no hay cajas abiertas.','info');

                location(setUrl('cajas','verCuotas',array('id'=>$array['id2'])));

                

            }

	}

        

        public function saveCuota($array){

            //die(print_r($array));

            if (is_numeric($array['id'])){

                //die(round($array['totalPago'],2).' '.$array['abono']);

                if(round($array['totalPago'],2)==$array['abono']){

                   // die('entra');

                    

                    if($array['estado']>'1'){

                        $pagar   = mCajas::pagarCuota($array['id']);

                    }

                    

                    if($pagar){

                        $ultimo = mSolicitudes::byId($array['id2'],'op_solicitudes');

                        $ultimo = $ultimo[0];

                        

                        $total = count($array["adjuntos1"]["name"][0]);

                        if($total>0){

                            include_once 'app/sistema/ftp.php';

                            $ftp=new ftp();

                            //die('app/files/clientes/'.$ultimo['cliente'].'/recibos');

                            if(!is_readable('app/files/clientes/'.$ultimo['cliente'].'/recibos')){

                                //die('entra');

                                mkdir('app/files/clientes/'.$ultimo['cliente'].'/recibos',0777);

                            }



                            if(!is_readable('app/files/clientes/'.$ultimo['cliente'].'/recibos/'.$array['id'])){

                                //die('entra2');

                                mkdir('app/files/clientes/'.$ultimo['cliente'].'/recibos/'.$array['id'],0777);

                            }

                            //die('fin');

                            for($i = 0; $i < $total; $i++){

                                $ext = explode('.',$array['adjuntos1']['name'][$i]);

                                $local1 = $array["nadjunto"][$i].'.'.$ext[1];// el nombre del archivo	

                                $remoto1 = $array["adjuntos1"]["tmp_name"][$i];// Este es el nombre temporal del archivo mientras dura la transmision	

                                $tama1 = $array["adjuntos1"]["size"][$i];

                                $ftp->SubirArchivo($local1,$remoto1,$tama1,'app/files/clientes/'.$ultimo['cliente'].'/recibos/'.$array['id'],'recibo'.($i+1));

                            }

                            //die('fin2');

                        }

                        

                        $recibo = mCajas::recibo();

                        $recibo = empty($recibo[0]['recibo'])?1:$recibo[0]['recibo'];

                        

                        mCajas::savePago(

                                $array['abono'],

                                getCaja(),

                                $array['id'],

                                $array['id2'],

                                $array['medio'],

                                $recibo

                                );

                        

                        $infoCre =  mCajas::infoCredito($array['id2']);

                        $cliente = $infoCre[0]['cliente'];

                        

                        $infoCli = mClientes::listarId2($cliente);

                        

                        notificaciones(11,

                            'Se ha pagado la cuota para el cliente '.$infoCli[0]['nombre'].' '.$infoCli[0]['apellido'].' por valor de '.toMoney($array['abono']).' via '.$array['medio']);

                        

                        setMensaje('Cuota pagada correctamente','success');

                    }else{

                        setMensaje('Error al pagar la cuota','error');

                    }

                    location(setUrl('cajas','verCuotas',array('id'=>$array['id2'])));

                }else{

                   // die('no coinciden los valores');

                    setMensaje('El valor a pagar no es igual al total.','info');

                    location(setUrl('cajas','verCuotas',array('id'=>$array['id2'])));

                }

            }

           // die('fin');

            location(setUrl('cajas','verCuotas',array('id'=>$array['id2'])));

        }



        public function guardar($array){

		validar_usuario();

		if(is_array($array) && !empty($array['usuario']) && !empty($array['fecha'])){

			$mCajas = new mCajas();

			$activo = (isset($array['activa']) && is_numeric($array['activa']))? 1:0;

                        

                        $caja = getCaja();

                        if(!$caja){

			$save = $mCajas -> newCaja(

                                $array['usuario'],

                                $array['fecha'],

                                empty($array['monto2'])?0:$array['monto2'],

                                $activo

                                );

                        }else{

                            setMensaje("El usurio no puede crear mas de una caja.","info");

                            location(setUrl("cajas"));

                        }

			if($save){

				setMensaje("Caja creada correctamente","success");

				location(setUrl("cajas"));

			}else{

				setMensaje("No se pudo crear la caja.","error");

				location(setUrl("cajas"));

			}

		}else{

			setMensaje("Debe ingresar todos los datos, inténtelo nuevamente","error");

			location(setUrl("cajas"));

		}

	}

       

	public function activarCaja($array){

		validar_usuario();

		$estado = 0;

		if($estado != $array['estado']){

                    

                        $info = mCajas::selectCajaId($array['id']);

                        $info = $info[0];

                        //die(print_r($info));

                        

                        

                        $pagosT = mCajas::pagos($array['id']);

                        $pagosT = ($pagosT[0]['total']);

                        

                        $pagos = mBancos::medios();

                        $valores = unserialize($info['tEfectivo']);

                        

                        $tpagos=null;

                        foreach ($pagos As $p):

                            $pagosE = mCajas::pagos($array['id'],strtolower($p['medio']));

                            $tpagos[strtolower($p['medio'])] = empty($pagosE[0]['total'])?0:$pagosE[0]['total'];

                        endforeach;

                        

                        

                        /*$pagosE = mCajas::pagos($array['id'],'efectivo');

                        $pagosE = ($pagosE[0]['total']);

                        

                        $pagosC = mCajas::pagos($array['id'],'cheque');

                        $pagosC = ($pagosC[0]['total']);

                        

                        $pagosCo = mCajas::pagos($array['id'],'consignacion');

                        $pagosCo = ($pagosCo[0]['total']);*/

                        //die(($tpagos).'-'.$pagosT);

			if(is_array($info)){

				parent::cerrarCaja($info,date("d-m-Y"),($pagosT+$info['montoA']),$tpagos);

			}else{

				setMensaje("No se enconttraron datos de la caja, inténtelo nuevamente","error");

				location(setUrl("cajas"));

			}

		}else{

                    //die("Esta caja esta cerrada");

			setMensaje("Esta caja esta cerrada","info");

			location(setUrl("cajas"));

		}

		

	}

	   

	public function cerrarCaja($array){

            validar_usuario();

            //die(print_r($array));



            $npagos = mCajas::tPagos($array['id']);



            $total = $array['esperado2'];

            if(mCajas::cerrarCaj($array["id"],opDate(),$total,$npagos[0]['total'],serialize($array['tp2']))){

                    setMensaje ("Caja cerrada con exito", "success");

            notificaciones(12,'Caja numero '.$array['id'].' cerrada con exito por '.getNombreUsuario().' por valor de '.toMoney($total));

                        

            }else{

                    setMensaje ("No se ha podido relizar la operacion", "error");

            }



            location(setUrl("cajas"));

	}

	  

	

	public function eliminarCaja($array){

		validar_usuario();

		if(is_numeric($array['id'])){

			$del = mCajas :: deleteCaj($array['id']);

			setMensaje("Caja eliminada satisfactoriamente","success");

		}else{

			setMensaje("No se ha encontrado informacion de la caja","error");

		}

		location(setUrl("cajas"));

		

	}

        

	public function pagoParcial($array){

            //die(print_r($array));

		if(is_numeric($array["id"]) || !empty($array['id'])){

                   

                    $info   =   mCajas::infoCuota($array['id2'],$array['id']);

                    $info   =   $info[0];

                    //$cuota  =   $info['valor'];

                    

                    parent::pagoParcial($array["id"],$array["id2"]);

		}else{

			setMensaje("Id incorrecto.","error");

                }

	} // pagoParcial        

        

     public function saveAbono($array){

         

        $info   = mSolicitudes::infoCuotas2($array['id']);

        //$info = mCajas::infoCredito($array['id']);



        if(is_array($info)){

            

            foreach ($info As $in):

                $capital = round($in['aporteCapital']+$in['capital']);

                //echo $in['interes_dia'].'+'.$in['interes'].'+'.$capital.'+'.$in['otros_total'].'<br/>';

                $total = $in['interes_dia']+$in['interes']+$capital+$in['otros_total'];

            

            if($array['abono']>$total){

                setMensaje('El valor abonado es mas grande que el valor de la cuota','info');

                location(setUrl('cajas','pagoParcial',array('id'=>$array['id'])));

            }else{

                $abono = $array["abono"];



                $pdte = $abono - $total;



                if($pdte <= 0){

                    $k 		= $in['aporteCapital'];

                    $k2 	= $in['capital'];

                    $ic 	= $in["interes"];

                    $ic2 	= $in["interes_dia"];

                    $mora	= 0;

                    $cobranza 	= 0;



                    if(($abono - $cobranza)<0){

                        $cobranza = $cobranza -$abono;

                    }else{

                        $saldo = $abono - $cobranza;

                        $cobranza = 0;

                        $abono = $saldo;



                        if(($abono - $mora)<0){

                            $mora = $mora - $abono;

                        }else{

                            $saldo  = $abono - $mora;

                            $mora   = 0;

                            $abono  = $saldo;



                            //recorro seguros  otros

                            $otros = $in['otros_total'];

                            //foreach ($otros As $key=>$o):

                                if(($abono - $otros)<0){

                                    $otros = $otros - $abono;

                                }else{

                                    $saldo          = $abono - $otros;

                                    $otros          = 0;

                                    $abono          = $saldo;                            

                                }

                            //endforeach;                      



                            if(($abono - $ic2)<0){

                                $ic2 = $ic2 -$abono;

                            }else{

                                $saldo = $abono - $ic2;

                                $ic2 = 0;

                                $abono = $saldo;



                                if(($abono - $ic)<0){

                                    $ic = $ic -$abono;

                                }else{

                                    $saldo = $abono - $ic;

                                    $ic = 0;

                                    $abono = $saldo;



                                    if(($abono - $k)<0){

                                        $k = $k -$abono;

                                    }else{

                                        $saldo = $abono - $k;

                                        $k = 0;

                                        $abono = $saldo;

                                        

                                        if(($abono - $k2)<0){

                                            $k2 = $k2 -$abono;

                                        }else{

                                            $saldo = $abono - $k2;

                                            $k2 = 0;

                                            $abono = $saldo;

                                        }

                                        

                                    }



                                }



                            }

                        }



            }//fin desc abonos

            

            //realizo el pago

            //echo ($total .'='. $k .'+'. $ic .'+'. $ic2 .'+'. ($otros) .'+'. $mora .'+'. $cobranza.'<br/>');

            $upd = mSolicitudes::updDesembolso(

                    $in['id'],

                    $k,

                    $k2,

                    $ic,

                    $ic2,

                    $otros,

                    $mora,

                    $cobranza

                    );

            if($upd){

                    setMensaje("Abono aplicado correctamente","success");

            }else{

                    setMensaje("Error interno al intentar aplicar este abono","error");

            }

        }else{

            setMensaje("El abono supera la deuda.","info");

        }

            location(setUrl('cajas','verCuotas',array('id'=>$array['id'])));

        }

     endforeach;

     }else{

        $info = mCajas::verCuotas2($array['id']);

        $info = $info[0];

        

        $capital = $info['saldo_capital'];

        

        $guardar = mCajas::saveAbono(

                                $array['id'],

                                $abono

       

               );  



        if($guardar){

            mCajas::savePago($array['abono'],getCaja(),$array['id']);

            setMensaje('Abono guardado correctamente','success');

        }else{

            setMensaje('Error al guardar el abono.','error');

        }

         

     }//val cuota

     location(setUrl('cajas','pagoParcial',array('id'=>$array['id'])));

    }

    

public function pagoTotal($array=null){



    validar_usuario();



    $caja = getCaja();

    if(!empty($caja)){



        $info = mCajas::corte($array['id']);

        //die('='.print_r($info));

        $total =   0;



        $fecha = explode("/",$info[0]["fecha_crom"]);

        $fecha = $fecha[2].'-'.$fecha[1].'-'.$fecha[0];



        $otros2 = 0;

        $otros = unserialize($info[0]['otros']);

        if(is_array($otros)){

            foreach ($otros As $ot){

                $otros2 += $ot;

            }

        }

        

        $total = (

                $info[0]['ajuste_interes']+

                $info[0]['interes']+

                $info[0]['saldo_capital']+

                $info[0]['mora']+

                $otros2

                );



        parent::pagoTotal($info[0],$total,$array['id'],$array['url'].'/id/'.$array['id']);

    }else{



        setMensaje('No se puede realizar el pago por que no hay cajas abiertas.','info');

        location(setUrl('cajas','pagos'));



    }



}

    

     public function pagoTotal2($array=null){

        validar_usuario();

        

         $caja = getCaja();

            if(!empty($caja)){

                $info   = mCajas::consulta2($array['id']);

                //die(print_r($info));

                $total = 0;

                foreach ($info As $i):

                    if($i['linea']>1){

                        $total += $i['valor'];

                    }else{

                        $abono = mCajas::abonos($array['id']);

                                    $abono = $abono[0];

                                    

                                    $imp=false;

                                    $desc = 0;

                                    $elabono = 0;

                                    if($abono['abono']>0){

                                        $elabono = $abono['abono'];

                                        $desc = $abono['descu'];

                                        $imp=true;

                                    }

                        $total += (($i['valor_real']-$desc))-$elabono;

                        $iva = ($i['valor_real']-$desc)*($i['iva']/100);

                        //$totalP=0;

                        //if($imp){

                            $totalP = $iva+$total;

                        //}

                    }

                endforeach;



                parent::pagoTotal2($info[0],$total,$array['id'],$imp,$iva,$totalP);

            }else{

                

                setMensaje('No se puede realizar el pago por que no hay cajas abiertas.','info');

                location(setUrl('cajas','pagos'));

                

            }

    }

    

    public function generarRecibo($array){

        validar_usuario();

         $info   = mCajas::infoCredito($array['id']);

         $info = $info[0];

          $cadena='

			<html>



			<head>

			<meta http-equiv="Content-Type" content="text/html">

			

			<title>Cheque pago</title>

			<base href="http://prestarmas.co/prestarmas/">

			<link rel="stylesheet" href="app/librerias/bootstrap/css/bootstrap.css" />

                        <style type="text/css">

                       

			</style>

			</head>

			<body>';

		

                        $cadena .='

                            <center>

                            <h1>Recibo de pago</h1>

                            <table class="table" boder="1">

                                    <tr>

                                        <td bgcolor="#D3D3D3">Inversiones serprogreso</td>

                                        <td>'.getNombreSucursal().'</td>

                                    </tr>

                                    <tr>

                                        <td bgcolor="#D3D3D3">Nombre cliente</td>

                                        <td>'.$info['nombre'].' '.$info['apellido'].'</td>

                                    </tr>

                                    <tr>

                                        <td bgcolor="#D3D3D3">Credito</td>

                                        <td>'.$info['id'].'</td>

                                    </tr>

                                    <tr>

                                        <td bgcolor="#D3D3D3">Fecha corte</td>

                                        <td>'.$info['fecha_corte'].'/'.date('m/Y').'</td>

                                    </tr>

                                    <tr>

                                        <td bgcolor="#D3D3D3">Fecha Pago</td>

                                        <td>'.opDate(true).'</td>

                                    </tr>

                                    <tr>

                                        <td bgcolor="#D3D3D3">Valor pagado</td>

                                        <td>'.toMoney($array['total']).'</td>

                                    </tr>

                            </table>

                            </center>';

          

		    $cadena .= '

		    </body>

		    </html>';

                   // echo $cadena;

                   pdf($cadena,'app/files/recibos/recibo_pago_'.$info['id']);

                   $mp3Name = trim(addslashes(strip_tags('app/files/recibos/recibo_pago_'.$info['id'].'.pdf'))); //which would be "Rom1_1.mp3"

                    $filename = "$mp3Name";

                   //die($filename);

                    header("Content-Length: " . filesize($filename));

                    header('Content-Type: application/pdf');

                    header("Content-Disposition: attachment; filename= $mp3Name");



                    readfile($filename);

                    return location(setUrl('cajas','pagoTotal',array('id'=>$info['id'])));

    }

    

    public function generarEstado($array){

        validar_usuario();

         $info   = mCajas::infoCredito($array['id']);

         $info = $info[0];

       

$cadena ='

<!DOCTYPE html>

<html>

<head lang="es">

<meta charset="UTF-8">



<title>Estado de cuenta</title>

<base href="http://prestarmas.co/prestarmas/">

<style type="text/css">

header,article,section,footer,table{

display:block;

margin: 0;

padding: 0;

}

html,body{

margin: 0;

padding: 0;

}

header{

height: 30px;

}

header p{

float:right;

margin:	0;

padding:	0;

}

article p{

    display: block;

    font-size: 15px;

    margin: 0 auto;

    padding-left: 50%;

    width: auto;

}

body{

width: 960px;

margin: 0 auto;

font-family: verdana ,geneva,sans-serif;

font-size: 11px;

}

table{

    float: right;

    /*padding-right: 100px;

    padding-top: 10px;*/

}

h2{

margin: 0 auto;

text-align: center;

display: block;

}

footer{

margin: 0 auto;

text-align: justify;

font-size: 10px;

}

hr{

    border: 0;

    border-bottom: 2px dashed #ffffff;

    background: #080808;

    display: block;

    margin: 0 auto;

    width:	100%;

}

#col1{

height: 200px;

width:	200px;

float:	left;

}

#col2{

height: 200px;

width:	760px;

float:	left;

}

#etiqueta{

width: 200px;

height: 90px;

padding: 10px;

margin: 0 auto;

border-radius: 27px 27px 27px 27px;

-moz-border-radius: 27px 27px 27px 27px;

-webkit-border-radius: 27px 27px 27px 27px;

border: 4px solid #000000;

}

#mensaje{

    padding-top: 100px;

    margin-top: 220px;

    margin-left: auto;

    margin-right: auto;

}

#centro{

text-align: right;

padding-right: 100px;

}

#detalle{

    display: block;

    margin: 0 auto;

    overflow: auto;

    padding-bottom: 50px;

    padding-left: 30%;

    width: 50%;

}

#detalle table{

float: left;

width: 200px;

}

#detalle td{

}

#rojo{

    border: 2px solid red;

    border-radius: 100px;

    margin: 0 50%;

    overflow: auto;

    padding: 0 20px;

    width: 200px;

}

</style>

</head>

<body>

    

	<header>

	<p>23/09/2015 10:52 A.M<br/>

	Oficina: centenario</p>

	</header>

	

	<section>

	

		<aside id="col1"><img style="height: 90px;" src="app/plantillas/Administrador/img/logo2.png">

                <br/>900.649.069-9<br/>

		<br/><br/><br/>

		<div id="etiqueta">DIEGO ALEJANDRO ROMERO RIVERA<br/>

		Cra 92 # 45 - 160 Apto 602 Torre E, Unid<br/>

		Resid. Paseo del Lili – Valle del Lili<br/>

		Cali – Valle del Cauca

		</div>

		</aside>

		

		<article id="col2">

		<p>ESTADO DE CUENTA<br/>

		Cr&eacute;dito N. 073-005-827 – Veh&iacute;culo<br/>

		Fecha de corte: 25/09/2015</p>

		<br/><br/>

		<div id="rojo"><table>

			<tr>

				<td>Paguese antes de:</td>

				<td>23/09/2015</td>

			</tr>

			<tr>

				<td>Valor a pagar:</td>

				<td>500.000</td>

			</tr>

			<tr>

				<td>Valor cuotas vencidas:</td>

				<td>0</td>

			</tr>

		</table></div>

		</article>



	</section>

	

	<br/>

		<table>

			<tr>

				<td>Periodo liquidado:</td>

				<td>25/08/2015 – 25/09/2015</td>

				<td>Numero cuota</td>

				<td>1</td>

			</tr>

			<tr>

				<td>Dias liquidados:</td>

				<td>31</td>

				<td>Cuotas pendientes</td>

				<td>11</td>

			</tr>

			<tr>

				<td>Dias mora:</td>

				<td>0</td>

				<td>Tasa interes</td>

				<td>2.2 EA</td>

			</tr>

			<tr>

				<td>Valor del credito:</td>

				<td>20.000.000</td>

				<td>Tasa mora</td>

				<td>2.2 EA</td>

			</tr>

			<tr>

				<td>Plazo (meses):</td>

				<td>12</td>

				<td>Saldo total</td>

				<td>10.000.000</td>

			</tr>

		</table>

		

		<br/><br/>

		

		<div id="mensaje">

		<center>En  Serprogreso creemos en los que se esfuerzan cada d&iacute;a por dar lo mejor en el trabajo, el hogar y su vida.</center>

		</div>

		<br/><br/>

		<h2>Detalle del pago</h2>

		<br/>';

                

                $cadena .= '

                        <div id="detalle"><table border="1">

                            <tr>

                                <td>Interes corriente</td>

                                <td>'.$info['interes_corriente'].'</td>

                            </tr>

                            <tr>

                                <td>Interes mora</td>

                                <td>'.$info['interes_corriente'].'</td>

                            </tr>

                            <tr>

                                <td>Seguro</td>

                                <td>'.$info['interes_corriente'].'</td>

                            </tr>

                            <tr>

                                <td>Cobranza</td>

                                <td>'.$info['interes_corriente'].'</td>

                            </tr>

                            <tr>

                                <td>Aporte a capital</td>

                                <td>'.$info['interes_corriente'].'</td>

                            </tr>

                        </table></div>';



                $cadena.='

		<hr>

		

		<article id="centro">

		<p>ESTADO DE CUENTA<br/>

		Cr&eacute;dito No. 073-005-827 – Veh&iacute;culo<br/>

		Fecha de corte: 25/09/2015</p><br/><br/>

		

		<div>ak va toda la info</div>

		</article>

		

	<footer>

	Apreciado cliente, le recordamos que desde el momento en que su obligaci&oacute;n entre en mora, Serprogreso, con el fin de recaudar las sumas pendientes deberá realizar gestiones de cobro cuyo costo le será trasladado. Dicho valor variará dependiendo de los d&iacute;as de mora y el tipo de producto y se liquidarán sobre el valor del valor del pago y hasta el valor del saldo vencido. “Lo invitamos a permanecer al d&iacute;a con sus obligaciones. Recuerde que el incumplimiento en sus pagos genera reporte negativo ante los operadores de informaci&oacute;n. Ley 1266 de 2008”

Para mayor informaci&oacute;n: www.serprogreso.com

	</footer>

               

                

</body>

</html>';



		

                    echo $cadena;

                  // pdf($cadena,'app/files/recibos/recibo_pago_'.$info['id']);

                  /* $mp3Name = trim(addslashes(strip_tags('app/files/recibos/recibo_total_'.$info['id'].'.pdf'))); //which would be "Rom1_1.mp3"

                    $filename = "$mp3Name";

                   

                    header("Content-Length: " . filesize($filename));

                    header('Content-Type: application/pdf');

                    header("Content-Disposition: attachment; filename= $mp3Name");



                    readfile($filename);*/

                    /*return location(setUrl('cajas','pagoTotal',array('id'=>$info['id'])));*/

    }

    

    public function detallePago($array){



        $info   = mCajas::infoCredito($array['id']);

        $info = $info[0];

        

        //print_r($info);

        $cadena ='';

        

        $cadena .= '

            <table>

                <tr>

                    <td>Interes corriente</td>

                    <td>'.$info['interes_corriente'].'</td>

                </tr>

            </table>';

        

        return $cadena;

    }

    

    public function savePagototal($array=null){

        validar_usuario();

        //die(print_r($array));

        $abono = $array['abono'];



        if($abono==ceil($array['total'])){

        //die('uno');   

            $info = mCajas::infoCredito($array["id"]);

            $info = $info[0];

            

            mCajas::pagoTotal($array['id']);



            $recibo = mCajas::recibo();

            $recibo = empty($recibo[0]['recibo'])?1:$recibo[0]['recibo'];

            //die(getCaja().'='.$recibo);

            mCajas::savePago(

                    $array['abono'],

                    getCaja(),

                    0,

                    $array['id'],

                    $array['medio'],

                    $recibo

                    );       

            

            //update todas las cuotas

            mCajas::pagarCuotas($array["id"]);

            

            $total = count($array["adjuntos1"]["name"][0]);

            if($total>0){

                

                $ultimo = mSolicitudes::byId($array['id'],'op_solicitudes');

                $ultimo = $ultimo[0];

                

                include_once 'app/sistema/ftp.php';

                $ftp=new ftp();

                if(!is_readable('app/files/clientes/'.$ultimo['cliente'].'/recibos')){

                    //die('uno');

                    mkdir('app/files/clientes/'.$ultimo['cliente'].'/recibos',0777);

                }



                if(!is_readable('app/files/clientes/'.$ultimo['cliente'].'/recibos/'.$array['id'])){

                    //die('dos');

                    mkdir('app/files/clientes/'.$ultimo['cliente'].'/recibos/'.$array['id'],0777);

                }

                //die('app/files/clientes/'.$ultimo['cliente'].'/recibos/'.$array['id']);



                for($i = 0; $i < $total; $i++){

                    $ext = explode('.',$array['adjuntos1']['name'][$i]);

                    $local1 = $array["nadjunto"][$i].'.'.$ext[1];// el nombre del archivo	

                    $remoto1 = $array["adjuntos1"]["tmp_name"][$i];// Este es el nombre temporal del archivo mientras dura la transmision	

                    $tama1 = $array["adjuntos1"]["size"][$i];

                    //die('app/app/files/clientes/'.$ultimo['cliente'].'/recibos/'.$array['id']);

                    $subir=$ftp->SubirArchivo($local1,$remoto1,$tama1,'app/files/clientes/'.$ultimo['cliente'].'/recibos/'.$array['id'],'contrato'.($i+1));

                }

            }

            //die('fin');

            notificaciones(6,'Pagototal del credito numero '.$array['id'].' por valor de '.  toMoney($array['abono']));

            //generar pdf

            setMensaje('Pago total guardado correctamente.','success');

        }else{

            //die('dos');

            setMensaje('Los valores no son iguales.','info');

        }

        //die('fin'); 

        /*que se hace cuando no es un valor real*/

        location(setUrl('cajas','pagos'));

    }

    

    public function savePagototal2($array=null){

        validar_usuario();

        //die(print_r($array));

        $array = str_replace(array('US$ ','.',','),array('','','.'),$array);

        

        $pago = $array['pago'];

        $info   = mCajas::consulta2($array['id']);

        $info = $info[0];

        

        $abono = mCajas::abonos($array['id']);

        $abono = $abono[0];

        

        $elabono = 0;

        $desc= 0;

        $iva = 0;

        if($abono['abono']>0){

            $elabono = $abono['abono'];

            $desc = $abono['descu'];

            $iva = ($info['valor_real']-$desc)*($info['iva']/100);

        }

        $valor = ($info['linea']>1)?$info['valor']:$info['valor_real'];

        

        if($info['linea']==1){

            $valor = $valor - $desc +$iva;

        }

        

        $valor = ($valor-$elabono);

        $valor2 = strval($valor);

        $pago2 = strval($pago);

        

        if($pago2==$valor2){

            //die('entra');

            $pagar=mCajas::savePago($pago,getCaja(),0,$array['id'],$array['medio'],$array['recibo']);

            if($pagar){

            $ultimo = mSolicitudes::byId($array['id'],'op_solicitudes');

            $ultimo = $ultimo[0];

            $total = count($array["adjuntos1"]["name"]);

            

            include_once 'app/sistema/ftp.php';

            $ftp=new ftp();

            if(!is_readable('app/files/clientes/'.$ultimo['cliente'].'/recibos')){

                mkdir('app/files/clientes/'.$ultimo['cliente'].'/recibos',0777);

            }

            

            if(!is_readable('app/files/clientes/'.$ultimo['cliente'].'/recibos/'.$array['id'])){

                mkdir('app/files/clientes/'.$ultimo['cliente'].'/recibos/'.$array['id'],0777);

            }

            

            

            for($i = 0; $i < $total; $i++){

                $ext = explode('.',$array['adjuntos1']['name'][$i]);

                 $local1 = $array["nadjunto"][$i].'.'.$ext[1];// el nombre del archivo	

                  $remoto1 = $array["adjuntos1"]["tmp_name"][$i];// Este es el nombre temporal del archivo mientras dura la transmision	

                  $tama1 = $array["adjuntos1"]["size"][$i];

                  $subir=$ftp->SubirArchivo($local1,$remoto1,$tama1,'app/app/files/clientes/'.$ultimo['cliente'].'/recibos/'.$array['id'],'contrato'.($i+1));

            }

                setMensaje('Pago guardado correctamente.','success');

                mCajas::pagarT($array['id']);

            }else{

                setMensaje('Error al guardar el pago.','error');

            }

            location(setUrl('cajas','pagos'));

        }else{

            //die('dos');

             $pagar=mCajas::savePago($pago,getCaja(),0,$array['id'],$array['medio'],$array['recibo']);

            if($pagar){

                mCajas::saveAbono2($array['id'],$pago,$array['desc']);

                

            $ultimo = mSolicitudes::byId($array['id'],'op_solicitudes');

            $ultimo = $ultimo[0];

            $total = count($array["adjuntos1"]["name"]);

            

            include_once 'app/sistema/ftp.php';

            $ftp=new ftp();

            if(!is_readable('app/files/clientes/'.$ultimo['cliente'].'/recibos')){

                mkdir('app/files/clientes/'.$ultimo['cliente'].'/recibos',0777);

            }

            

            if(!is_readable('app/files/clientes/'.$ultimo['cliente'].'/recibos/'.$array['id'])){

                mkdir('app/files/clientes/'.$ultimo['cliente'].'/recibos/'.$array['id'],0777);

            }

            

            

            for($i = 0; $i < $total; $i++){

                $ext = explode('.',$array['adjuntos1']['name'][$i]);

                 $local1 = $array["nadjunto"][$i].'.'.$ext[1];// el nombre del archivo	

                  $remoto1 = $array["adjuntos1"]["tmp_name"][$i];// Este es el nombre temporal del archivo mientras dura la transmision	

                  $tama1 = $array["adjuntos1"]["size"][$i];

                  $subir=$ftp->SubirArchivo($local1,$remoto1,$tama1,'app/app/files/clientes/'.$ultimo['cliente'].'/recibos/'.$array['id'],'contrato'.($i+1));

            }

                setMensaje('Pago guardado correctamente.','success');

            }else{

                setMensaje('Error al guardar el pago.','error');

            }

            

            setMensaje('Abono guardado correctamente.','success');

            location(setUrl('cajas','pagos'));

        }

    }

    

    public function detalle($array){

        validar_usuario();

        

        $info = mCajas::selectCajaId($array['id']);

        $info = $info[0];

        

        $pagos = mBancos::medios();

        $valores = unserialize($info['tEfectivo']);



        $tpagos=null;

        foreach ($pagos As $p):

            $pagosE = mCajas::pagos($array['id'],strtolower($p['medio']));

            $tpagos[strtolower($p['medio'])] = empty($pagosE[0]['total'])?0:$pagosE[0]['total'];

        endforeach;

        //die(print_r($tpagos));

        parent::detalle($info,$tpagos);

    }

    

} // class
