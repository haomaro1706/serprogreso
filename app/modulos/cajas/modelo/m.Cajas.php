<?php 

class mCajas{
	
	public function lstCajas(){
		$orm = new ORM();
		$orm -> select(array("ca.*","usu.nombre as empleado"))
			 -> from('op_cajas','ca')
			 -> join(array("usu"=>"op_usuarios"),'ca.id_usuario=usu.id');
                
		return $orm -> exec() -> fetchArray();
	}
        
        public function lstCajas2(){
		$orm = new ORM();
		$orm -> select(array("ca.*","usu.nombre as empleado"))
			 -> from('op_cajas','ca')
			 -> join(array("usu"=>"op_usuarios"),'ca.id_usuario=usu.id')
                        ->where('ca.estado=?',1);
                
		return $orm -> exec() -> fetchArray();
	}
        
        public function pagos($id,$tipo=null){
		$orm = new ORM();
		$orm -> select(array('sum(valor) As total'))
			-> from('op_pagos')
                        ->where('caja=?', $id);
                if(!empty($tipo)){
                    $orm->where('medio=?',$tipo);
                }
		return $orm -> exec() -> fetchArray();
	}
	
	public function newCaja($usuario, $fecha, $monto, $activo=1){
		$orm = new ORM();
		$orm    -> insert("op_cajas")
                        -> values(
                                array(
                                    0,
                                    $usuario,
                                    getSucursal(),
                                    date('Y-m-d',strtotime((str_replace('/', '-', $fecha)))),
                                    '0000-00-00',
                                    null,
                                    $monto,
                                    0,
                                    0,
                                    $activo
                                   )
                               );
		//die(print($orm -> verSql()));
		return $orm -> exec() -> afectedRows();
	} // newAcuerdo
	
	public function lastAcuerdo($id){
		$orm = new ORM();
		$sql = $orm -> select(array("id"))
					-> from ("op_acuerdos")
					-> where("id_solicitud = ? ",$id)
					-> orderBy(array("id_solicitud DESC"))
					-> limit(0,1);
		return $orm -> exec() -> fetchArray();
	} // newAcuerdo
	
	public function selectCajaId($id){
		$orm = new ORM();
		$orm -> select()
			 -> from('op_cajas')
			 -> where("id =?",$id);
		return	$orm -> exec() -> fetchArray();
	}
	
	public function consulta($id){
		$orm = new ORM();
		$orm -> select(
                        array(
                            'sol.*',
                            'sol.id As elid',
                            'prod.*',
                            'prod.producto as nomProducto',
                            'cli.nombre',
                            'cli.id As numero',
                            'des.fecha_desembolso'
                            )
                        )
			 -> from('op_solicitudes','sol')
			 -> join(array("prod"=>"op_productos"),'SUBSTRING(sol.producto,1)=prod.id')
			 -> join(array("cli"=>"vista_clientes"),'sol.cliente=cli.id')
			 -> join(array("des"=>"op_solicitudes_desembolsos"),'sol.id=des.id_solicitud')
			 -> where("cli.id = ?",$id)
			 -> where("sol.estado = ?",4);
               // die($orm->verSql());
		return	$orm -> exec() -> fetchArray();
	}
        
        	public function consulta2($id){
		$orm = new ORM();
		$orm -> select(array('sol.*','sol.id As elid','prod.*','prod.producto as nomProducto','cli.nombre','cli.apellido'))
			 -> from('op_solicitudes','sol')
			 -> join(array("prod"=>"op_productos"),'sol.producto=prod.id')
			 -> join(array("cli"=>"op_clientes"),'sol.cliente=cli.numero')
			 -> where("sol.id = ?",$id);
                //die($orm->verSql());
		return	$orm -> exec() -> fetchArray();
	}
	
	public function verCuotas($id){
            //$fecha = opDate();
            $fecha = "2016-09-25";
		$orm = new ORM();
		/*$sql = $orm -> select(array('cuo.id As elid','cuo.cobranza As cobranza2','cuo.seguroVida As vida','cuo.*','des.*'))
			 -> from('op_solicitudes_cuotas','cuo')
			 -> join(array("des"=>"op_solicitudes_desembolsos"),'cuo.id_desembolso=des.id')
			 -> where("des.id_solicitud = ?",$id)
			 -> where("YEAR(cuo.fechaCorte) <= ?",date('Y'))
			 -> where("MONTH(cuo.fechaCorte) <= ?",date('m'))
			 -> where("cuo.estado >= ?",1);*/
                $orm->select(array("c.*","(select monto from op_solicitudes where id=c.id_desembolso) As capital"))
                    ->from('op_solicitudes_cuotas','c')
                    ->where('id_desembolso=?',$id)
                    ->where("fechaCorte <= ?",$fecha);
               // die($sql->verSql());
		return	$orm -> exec() -> fetchArray();
	}
        
        public function verCuotas2($id){
            //$fecha = opDate();
            $fecha = "2016-09-12";
		$orm = new ORM();
		/*$sql = $orm -> select(array('cuo.id As elid','cuo.cobranza As cobranza2','cuo.seguroVida As vida','cuo.*','des.*'))
			 -> from('op_solicitudes_cuotas','cuo')
			 -> join(array("des"=>"op_solicitudes_desembolsos"),'cuo.id_desembolso=des.id')
			 -> where("des.id_solicitud = ?",$id)
			 -> where("YEAR(cuo.fechaCorte) <= ?",date('Y'))
			 -> where("MONTH(cuo.fechaCorte) <= ?",date('m'))
			 -> where("cuo.estado >= ?",1);*/
                $orm->select(
                        array(
                            'des.id',
                            'di.*',
                            /*'di.interes',
                            'di.ajuste_interes As interes_dia',
                            'di.saldo_capital As saldo',
                            'di.fecha_corte As fechaCorte',
                            'di.fecha_crom As fechaPago',*/
                            'des.estado',
                            'des.cuotaFija'
                            )
                        )
                    ->from('op_solicitudes_diaria','di')
                    -> join(array("des"=>"op_solicitudes_cuotas"),'di.id_sol=des.id_desembolso')
                    ->where('di.id_sol=?',$id);
                    //->where("des.fechaCorte <= ?",$fecha);
               // die($sql->verSql());
		return	$orm -> exec() -> fetchArray();
	}
	
	public function saveCuota($idAcuerdo, $capital, $interes, $saldo, $total, $fechaPago){
		$orm = new ORM();
		$sql = $orm -> insert("op_acuerdos_cuotas")
					-> values(array(0, $idAcuerdo, $capital, $interes, $saldo, $total, $fechaPago, 1));
		return $orm -> exec() -> afectedRows();
	} // newAcuerdo
	
	public function lstAcuerdos(){
		$orm = new ORM();
		$orm->select(array("acuerdo.*","acuerdo.id As elId","sol.*","cli.*","pro.producto As nProducto"))
				   ->from("op_acuerdos","acuerdo")
				   ->join(array("sol"=>"op_solicitudes"),'acuerdo.id_solicitud = sol.id',"INNER")
				   ->join(array("cli"=>"op_clientes"),'sol.cliente = cli.numero',"INNER")
				   ->join(array("pro"=>"op_productos"),'sol.producto = pro.id',"INNER");
		$return=$orm->exec()->fetchArray(); 
	    return $return;
	}
	
	public function lstAcuerdosId($id){
	    $orm = new ORM();
	    $sql = $orm -> select()
				    -> from ("op_acuerdos_cuotas")
				    -> where("id_acuerdo = ? ",$id);
	    return $sql -> exec() -> fetchArray();
	}
	
	public function updAcuerdosId($id){
		$orm = new ORM();
		$orm	-> update("op_acuerdos_cuotas")
						-> set (array(
								  "estado"=>"2"
								  )
							  )
						-> where("id=?",$id);
		return $orm->exec() -> afectedRows();	
	}
    
	public function cerrarCaj($id,$fecha,$total,$npagos,$efectivo,$estado=0){
		$orm = new ORM();
		$orm -> update("op_cajas")
			 -> set(
                                 array(
                                     "estado"=>$estado,
                                     "fechaC"=>$fecha,
                                     "vlrRec"=>$total,
                                     "nroPagos"=>$npagos,
                                     "tEfectivo"=>$efectivo
                                     )
                                )
			 -> where("id = ?",$id);
                //die($orm->verSql());
		return $orm -> exec() -> afectedRows();
	}
	
	public function deleteCaj($id){
		$orm = new ORM();
		$orm -> delete()
			 -> from('op_cajas')
			 -> where("id = ?",$id);
		return $orm -> exec() -> afectedRows();
	}
        
        public function cobranza($zona){
	    $orm = new ORM();
	    $sql = $orm -> select()
                        -> from ("op_parametros_cobranza")
                        -> where("id_zona = ? ",$zona);
           // die($sql->verSql());
	    return $sql -> exec() -> fetchArray();
	}
        
        public function pagarCuota($id){
		$orm = new ORM();
		$orm	-> update("op_solicitudes_cuotas")
                        -> set (
                                array(
                                    "estado"=>"2"
                                )
                        )
                        -> where("id=?",$id);
                //die($orm->verSql());
		return $orm->exec() -> afectedRows();	
	}
        
         public function pagarT($id){
		$orm = new ORM();
		$orm	-> update("op_solicitudes")
                        -> set (
                                array(
                                    "estado"=>"2"
                                )
                        )
                        -> where("id=?",$id);
              //  die($orm->verSql());
		return $orm->exec() -> afectedRows();	
	}
        
        public function infoCuota($id){
            $orm = new ORM();
		/*$sql = $orm -> select(array('cuo.id As elid','cuo.seguroVida As vida','cuo.cobranza As cobranza2','cuo.*','des.*'))
			 -> from('op_solicitudes_cuotas','cuo')
			 -> join(array("des"=>"op_solicitudes_desembolsos"),'cuo.id_desembolso=des.id')
			 -> where("des.id_solicitud = ?",$id)
			 -> where("cuo.id = ?",$id2);*/
            $sql = $orm->select()->from('op_solicitudes_cuotas')->where('id_desembolso=?',$id);
            
	    return $sql -> exec() -> fetchArray();
	}
        
        public function saveAbono(
                                $id,
                                $valor
                                ){
            $orm = new ORM();
            $orm    -> update("op_solicitudes_diaria")
                    -> set (
                            array(
                                "saldo_capital"=>$valor
                            )
                    )
                    -> where("id_sol=?",$id);
		return $orm->exec() -> afectedRows();
	}
        
        public function verAbono($id){
	    $orm = new ORM();
	    $sql = $orm -> select(
                                    array(
                                        "sum(cuota) as cuota",
                                        "sum(corriente) as corriente",
                                        "sum(seguro) as seguro",
                                        "sum(mora) as mora",
                                        "sum(cobranza) as cobranza"
                                    )
                                )
                        -> from ("op_solicitudes_abonosdesembolsos")
                        -> where("id_cuota = ? ",$id);
           // die($sql->verSql());
	    return $sql -> exec() -> fetchArray();
	}

        public function corte($id){
	    $orm = new ORM();
	    $sql = $orm -> select()
                        -> from ("op_solicitudes_diaria")
                        -> where("id_sol = ? ",$id);
           // die($sql->verSql());
	    return $sql -> exec() -> fetchArray();
	}
        
        public function infoCredito($id){
	    $orm = new ORM();
	    $sql = $orm -> select(array("des.*","sol.monto","sol.cliente"))
                        -> from ("op_solicitudes_desembolsos","des")
                        -> join(array("sol"=>"op_solicitudes"),'des.id_solicitud=sol.id')
                        //-> join(array("cli"=>"op_clientes"),'sol.cliente=cli.numero')
                        -> where("des.id_solicitud = ? ",$id);
           // die($sql->verSql());
	    return $sql -> exec() -> fetchArray();
	}
        
        public function recibo(){
	    $orm = new ORM();
	    $sql = $orm -> select(
                            array(
                                "max(recibo) as recibo"
                            )    
                        )
                        -> from  ("op_pagos");
	    return $sql -> exec() -> fetchArray();
	}
        
        public function infoPago($id){
	    $orm = new ORM();
	    $sql = $orm -> select(
                            array(
                                "sum(aporteCapital) as aporteCapital",
                                "fechaPago"
                            )    
                        )
                        -> from  ("op_solicitudes_cuotas")
                        -> where ("id_desembolso = ? ",$id)
                        -> where ("estado = ? ",2);
	    return $sql -> exec() -> fetchArray();
	}
        
        public function savePago(
                                    $valor,
                                    $caja,
                                    $id_cuota,
                                    $id,
                                    $medio,
                                    $recibo
                                ){
            $orm = new ORM();
            $sql = $orm -> insert("op_pagos")
                        -> values(
                                array(
                                    0,
                                    $valor,
                                    $caja,
                                    $id_cuota,
                                    $id,
                                    opDate(false,1),
                                    $medio,
                                    $recibo,
                                    "pt"
                                    )
                                );
            return $sql -> exec() -> afectedRows();
	}
        
        public function saveAbono2(
                                    $sol,
                                    $abono,
                                    $desc
                                ){
            $orm = new ORM();
            $sql = $orm -> insert("op_solicitudes_abonosdesembolsos")
                        -> values(
                                array(
                                    0,
                                    $sol,
                                    $abono,
                                    $desc,
                                    opDate()
                                    )
                                );
            return $sql -> exec() -> afectedRows();
	}
        
        public function abonos($id){
            $orm = new ORM();
            $orm -> select(array('sum(abono) As abono','sum(descu) As descu'))
                     -> from('op_solicitudes_abonosdesembolsos')
                     -> where("id_sol =?",$id);
            return	$orm -> exec() -> fetchArray();
	}
        
        public function cajaUser($id_usuario){
            $orm = new ORM();
            $orm -> select()
                     -> from('op_cajas')
                     -> where("id_usuario =?",$id_usuario)
                     -> where("fechaA =?",  opDate())
                     -> where("estado =?",1);
            //die($orm->verSql());
            return	$orm -> exec() -> fetchArray();
	}
        
        public function cuotaSinpagar($id,$estado=1){
            $orm = new ORM();
		
            $sql = $orm->select()
                    ->from('op_solicitudes_cuotas')
                    ->where('id_desembolso=?',$id)
                    ->where('estado=?',$estado);
            //die($sql->verSql());
	    return $sql -> exec() -> fetchArray();
	}
        
        	public function pTotal($id){
		$orm = new ORM();
                $orm->select()
                    ->from('op_solicitudes')
                    ->where('id=?',$id);
               // die($sql->verSql());
		return	$orm -> exec() -> fetchArray();
	}
        
        public function cuotasPagas($id){
            $orm = new ORM();
		
            $sql = $orm->select()
                    ->from('op_pagos')
                    ->where('id_sol=?',$id);
            //die($sql->verSql());
	    return $sql -> exec() -> fetchArray();
	}
        
        public function proximaCuota($id){
            $orm = new ORM();
		
            $sql = $orm->select()
                    ->from('op_solicitudes_cuotas')
                    ->where('id_sol=?',$id)
                    ->where('estado=?',0)
                    //-> orderBy(array("id ASC"))
                    -> limit(0,1);
            //die($sql->verSql());
	    return $sql -> exec() -> fetchArray();
	}
        
        public function tPagos($id){
	    $orm = new ORM();
	    $sql = $orm -> select(
                                    array(
                                        "count(id) as total"
                                    )
                                )
                        -> from ("op_pagos")
                        -> where("caja = ? ",$id);
           // die($sql->verSql());
	    return $sql -> exec() -> fetchArray();
	}
        
    	public function pagoTotal($id){
		$orm = new ORM();
		$orm	-> update("op_solicitudes")
                        -> set (
                                array(
                                    "estado"=>"5"
                                    )
                                  )
                        -> where("id=?",$id);
		return $orm->exec() -> afectedRows();	
	}
        
        public function pagarCuotas($id){
            $orm = new ORM();
		
            $sql = $orm->update('op_solicitudes_cuotas')
                    ->set(
                            array(
                                "estado"=>"2"
                            )    
                        )
                    ->where('id_desembolso=?',$id)
                    ->where('estado=?','1');
            //die($sql->verSql());
	    return $sql -> exec() -> afectedRows();
	}
        
} // class
