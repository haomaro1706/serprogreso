<?php 

class vGeografica{
    public function main($info){
      echo getMensaje();
        ?>
           <h2><?php  echo GEOGRAFICA_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('parametros'), 
            		  GEOGRAFICA_PAIS=>'#',
            		  GEOGRAFICA_ESTADO=>setUrl('geografica','estado'),
            		  GEOGRAFICA_CIUDAD=>setUrl('geografica','ciudad')
            		 ),
            	    GEOGRAFICA_PAIS
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  GEOGRAFICA_PAIS=>'#'
            		 ),
            	    GEOGRAFICA_PAIS, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo GEOGRAFICA_PAIS?></h4>
       <div class='container'>
        <div class='span10 offset1'> 
        
            <fieldset>
                <legend><?php echo GEOGRAFICA_CREAR_PAIS?></legend>
                <?php
                        Html::openForm("pais",setUrl("geografica","savePais"));
                        Html::newInput("nombre","Pais *");
                        Html::newInput("cod", "Codigo *");
                        Html::newInput("cod2", "Codigo2 *");
                ?>
                </fieldset>
                <?php
                        Html::newButton("enviar", "Crear", "submit");
                        Html::closeForm();
                ?>
                <?php
                        if(!empty($info)){
                                echo datatable("tabla");
                                Html::tabla(array("Pais","Codigo","Codigo2","Editar","Eliminar"),
                                                                $info,
                                                                array("Name","id","Code2"),
                                                                array("editar"=>setUrl("geografica","editPais"),"eliminar"=>setUrl("geografica","delPais"))
                                                        );
                        }
                ?>
            
        </div>
       </div>
       <?php  
    }
    
     public function estado(){
      echo getMensaje();
        ?>
           <h2><?php  echo GEOGRAFICA_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('parametros'), 
            		  GEOGRAFICA_PAIS=>setUrl('geografica'),
            		  GEOGRAFICA_ESTADO=>'#',
            		  GEOGRAFICA_CIUDAD=>setUrl('geografica','ciudad')
            		 ),
            	    GEOGRAFICA_ESTADO
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  GEOGRAFICA_ESTADO=>'#'
            		 ),
            	    GEOGRAFICA_ESTADO, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo GEOGRAFICA_ESTADO?></h4>
       <div class='container'>
        <div class='span10 offset1'> 
        EN CONSTRUCCION .....
        </div>
       </div>
       <?php  
    }
    
    public function ciudad($info){
      echo getMensaje();
        ?>
           <h2><?php  echo GEOGRAFICA_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('parametros'), 
            		  GEOGRAFICA_PAIS=>setUrl('geografica'),
            		  GEOGRAFICA_ESTADO=>setUrl('geografica','estado'),
            		  GEOGRAFICA_CIUDAD=>'#'
            		 ),
            	    GEOGRAFICA_CIUDAD
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  GEOGRAFICA_CIUDAD=>'#'
            		 ),
            	    GEOGRAFICA_CIUDAD, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo GEOGRAFICA_CIUDAD?></h4>
       <div class='container'>
        <div class='span10 offset1'> 
            <fieldset>
                <legend><?php echo GEOGRAFICA_CREAR_CIUDAD?></legend>
                <?php
                        Html::openForm("ciudad",setUrl("geografica","saveCiudad"));
                        Html::newInput("nombre","Ciudad *");
                        echo paises();
                        Html::newInput("dis", "Distrito *");
                ?>
                </fieldset>
                <?php
                        Html::newButton("enviar", "Crear", "submit");
                        Html::closeForm();
                ?>
                <?php
                        if(!empty($info)){
                                echo datatable("tabla");
                                Html::tabla(array("Ciudad","Districto","Pais","Editar","Eliminar"),
                                                                $info,
                                                                array("Name","District","CountryCode"),
                                                                array("editar"=>setUrl("geografica","editPais"),"eliminar"=>setUrl("geografica","delPais"))
                                                        );
                        }
                ?>
        </div>
       </div>
       <?php  
    }
    
    public function editPais($info){
      echo getMensaje();
        ?>
           <h2><?php  echo GEOGRAFICA_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('geografica'), 
            		  GEOGRAFICA_PAIS_EDITAR=>'#'
            		 ),
            	    GEOGRAFICA_PAIS_EDITAR
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('geografica'), 
            		  GEOGRAFICA_PAIS_EDITAR=>'#'
            		 ),
            	    GEOGRAFICA_PAIS_EDITAR, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo GEOGRAFICA_PAIS_EDITAR?></h4>
       <div class='container'>
        <div class='span10 offset1'> 
        
            <fieldset>
                <legend><?php echo GEOGRAFICA_PAIS_EDITAR?></legend>
                <?php
                        Html::openForm("pais",setUrl("geografica","editPaispro"));
                        Html::newInput("nombre","Pais *",$info["Name"]);
                        Html::newInput("cod", "Codigo *",$info["id"]);
                        Html::newInput("cod2", "Codigo2 *",$info["Code2"]);
                ?>
                </fieldset>
                <?php
                        Html::newButton("enviar", "Editar", "submit");
                        Html::closeForm();
                ?>
        </div>
       </div>
       <?php  
    }
    
} // class
