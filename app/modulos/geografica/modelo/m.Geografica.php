<?php 
class mGeografica{
    
    public static function listarP($id=null){
        $orm = new ORM();
        $sql = $orm	-> select()
                        -> from("op_paises");
                        if($id){
                            $sql->where("id=?",$id);
                        }
        $return = $sql-> exec()->fetchArray();
        return $return;
    } // listarp
    
    public static function save($nombre,$id,$cod,$tabla){
	  $orm = new ORM();
	  $sql = $orm	-> insert($tabla)
                        -> values(
                                  array(
                                          $id,
                                          $nombre,
                                          $cod
                                      )
                         );
	  $return = $sql-> exec()-> afectedRows();
	  return $return > 0 ? true : false;
    }
    
    public static function updPais($id,$nombre,$code2){
    $orm = new ORM();
	  $sql = $orm	-> update("op_paises")
				  -> set(
                                        array(
                                            "Name"=>$nombre,
                                            "Code2"=>$code2
                                        )
                                    )
				  -> where("id = ?", $id);
				  
	   // die($sql->verSql());
    $orm -> exec();
    $retorno = $sql -> afectedRows() > 0 ? true : false;
    return $retorno;
  }//updProducto
  
  public static function delPais($id){
        $orm = new ORM();
        $orm	-> delete()
                -> from("op_paises")
                -> where("id=?",$id);		
        $return = $orm->exec();
        return $return;
    }
    
    public static function listarC($id=null){
        $orm = new ORM();
        $sql = $orm	-> select()
                        -> from("op_ciudades");
                        if($id){
                            $sql->where("id=?",$id);
                        }
        $return = $sql-> exec()->fetchArray();
        return $return;
    } // listarp
    
     public static function saveP($nombre,$pais,$dis){
	  $orm = new ORM();
	  $sql = $orm	-> insert("op_ciudades")
                        -> values(
                                  array(
                                          0,
                                          $nombre,
                                          $pais,
                                          $dis
                                      )
                         );
	  $return = $sql-> exec()-> afectedRows();
	  return $return > 0 ? true : false;
    }
    
} // class
