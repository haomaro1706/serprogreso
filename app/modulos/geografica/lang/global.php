<?php 
/*Variables globales para el modulo lang/*/
define('GEOGRAFICA_NOMBRE','Geografica');
define('GEOGRAFICA_CREAR','Geografica');
define('GEOGRAFICA_EDITAR','Geografica');
define('GEOGRAFICA_PAIS','Paises');
define('GEOGRAFICA_PAIS_EDITAR','Editar pais');
define('GEOGRAFICA_CIUDAD','Ciudades');
define('GEOGRAFICA_ESTADO','Estados');
define('GEOGRAFICA_CREAR_PAIS','Crear pais');
define('GEOGRAFICA_CREAR_CIUDAD','Crear ciudad');
