<?php 

include_once 'app/modulos/productos/vista/v.Productos.php';
include_once 'app/modulos/productos/modelo/m.Productos.php';
include_once 'app/modulos/zonas/modelo/m.Zonas.php';

class Productos extends vProductos{

    public function main(){
	  validar_usuario();
	  $lista = mProductos::listar();
	  
	  $logica = mProductos::listarLineas();
	  
	  $logicas='';
	  foreach ($logica As $lo):
	    $logicas[$lo['id']]= $lo['nombre'];
	  endforeach;
          
          $linea = mProductos::listarLineas2();
	  
	  $lineas='';
	  foreach ($linea As $li):
	    $lineas[$li['id']]= $li['nombre'];
	  endforeach;
	  
        parent::main($lista,$logicas,$lineas);    
    }//main
    
    public function saveProducto($array=null){
		validar_usuario();
		if( !empty($array["producto"]) && !empty($array["estado"]) ){
			$info=null;//mProductos::selectbyPoZona($array["producto"],$array["zona"]);
			if(!empty($info)){
				setMensaje("Este producto ".$info[0]['producto']." ya existe registrado en la zona ".$info[0]['nZona'].", intentelo de nuevo con nuevos datos o elimine el producto existente" ,"error");
				location(setUrl("productos"));
			}else{
				$save = mProductos::saveProducto(
									$array["producto"],
									$array["logica"],
                                                                        $array["linea"],
									$array["estado"]
								);
				if($save)
					setMensaje("Producto guardado exitosamente","success");
				else
					setMensaje("Error interno al intentar guardar el prodcuto","error");
				location(setUrl("productos"));
			}
		}else{
				setMensaje("Debe completar toda la informaci&oacute;n","error");
			}
			location(setUrl("productos"));
    }//saveProducto
    
    public function editProducto($array=null){
	validar_usuario();
	
	valId($array['id'],'productos');
	
	$lista = mProductos::listarId($array['id']);
	/*
	$zona = mZonas::listar();
	  
	  $zonas='';
	  foreach ($zona As $z):
	    $zonas[$z['id']]= $z['zona'];
	  endforeach;
	*/
	parent::editProducto($lista[0]);
    }//editProducto
    
    public function editProductoPro($array=null){
		validar_usuario();
		if(!empty($array['id']) && !empty($array['producto']) && !empty($array['estado'])){
			//$info=mProductos::selectbyPoZona($array["producto"],$array["zona"]);
                        $info = 7;
			if(!empty($info)){
				/*if($info[0]['id']!=$array['id']){
					setMensaje("Este producto ".$info[0]['producto']." ya existe registrado en la zona ".$info[0]['nZona'].", intentelo de nuevo con nuevos datos o elimine el producto existente" ,"error");
				}else{*/
					$upd = mProductos::updProducto(
										$array['id'],
										$array['producto'],
										$array['estado']
										);
					if($upd)
						setMensaje("Producto editado exitosamente","success");
					else
						setMensaje("Error interno al intentar editar el prodcuto","error");
				//}
			}else{
				$upd = mProductos::updProducto(
										$array['id'],
										$array['producto'],
										$array['estado']
										);
					if($upd)
						setMensaje("Producto editado exitosamente","success");
					else
						setMensaje("Error interno al intentar editar el prodcuto","error");
			}
		}else{
			setMensaje("Debe completar toda la informaci&oacute;n","error");
		}
		location(setUrl("productos"));
    }//editProductoPro
    
    public function delProducto($array=null){
	validar_usuario();
	valId($array['id'],'productos');
	
	$del = mProductos::delProducto($array['id']);
	
	if($del)
	  setMensaje("Producto eliminado exitosamente","success");
	else
	  setMensaje("Error interno al intentar eliminar el prodcuto","error");
	
	location(setUrl("productos"));
    }
    /*
    public function pZona(){
	validar_usuario();
	
	$zona = mZonas::listar();
	  
	  $zonas='';
	  foreach ($zona As $z):
	    $zonas[$z['id']]= $z['zona'];
	  endforeach;
	  
	  $producto = mProductos::listar();
	  $productoAll = mProductos::listarpz();
	  
	  $prodcutos='';
	  $i=0;
	  foreach($producto As $pro):
	    $prodcutos[$pro['id']] = $pro['producto'];
	  endforeach;
	
	parent::pZona($zonas,$prodcutos,$productoAll);
    }
    
    public function saveProductoZona($array=null){
	validar_usuario();
	
	if( !empty($array["nombre"]) && !empty($array["producto"]) && !empty($array["zona"]) && !empty($array["estado"]) ){
	  $save = mProductos::saveProductoZona(
							$array["nombre"],
							serialize($array["producto"]),
							$array["zona"],
							$array["estado"]
						    );
	  if($save)
		  setMensaje("Producto Zona guardado exitosamente","success");
	  else
		  setMensaje("Error interno al intentar guardar el producto zona.","error");
	}else{
	  setMensaje("Debe completar toda la informaci&oacute;n","error");
	}
		
	location(setUrl("productos","pZona"));
	
    }
    
    public function editProductoZona($array=null){
	validar_usuario();
	
	  $zona = mZonas::listar();
	  
	  $zonas='';
	  foreach ($zona As $z):
	    $zonas[$z['id']]= $z['zona'];
	  endforeach;
	  
	  $producto = mProductos::listar();
	  //$productoAll = mProductos::listarpz();
	  
	  $prodcutos='';
	  $i=0;
	  foreach($producto As $pro):
	    $prodcutos[$pro['id']] = $pro['producto'];
	  endforeach;
	  
	  /*traer info y pasarla
	  $info = mProductos::listarpzId($array['id']);
	
	parent::editProductoZona($zonas,$prodcutos,$info[0]);
    }
    
    public function editProductoZonapro($array){
	validar_usuario();
	
	if(
	  !empty($array['id']) &&
	  !empty($array['nombre']) &&
	  !empty($array['zona']) &&
	  !empty($array['producto']) &&
	  !empty($array['estado'])
	  ){
	  
	  $lista = mProductos::updProductoZona(
							$array['id'],
							$array['nombre'],
							$array['zona'],
							serialize($array['producto']),
							$array['estado']
						    );
	  if($lista)
	    setMensaje("Producto zona editado exitosamente","success");
	  else
	    setMensaje("Error interno al intentar editar el prodcuto zona","error");
	}else{
	  setMensaje("Debe completar toda la informaci&oacute;n","error");
	}
	location(setUrl('productos','pZona'));
    }//editProductoZonapro
    
    public function delProductozona($array){
	validar_usuario();
	valId($array['id'],'productos','pZona');
	
	$del = mProductos::delProductozona($array['id']);
	
	if($del)
	  setMensaje("Producto zona eliminado exitosamente","success");
	else
	  setMensaje("Error interno al intentar eliminar el producto zona","error");
	
	location(setUrl("productos","pZona"));
    }*/
          
    public function tipo($array){
        validar_usuario();	
        
        $info = mProductos::listarLineas();
        
        parent::tipo($info);
    }//main
    
    public function savetipos($array=null){
        validar_usuario();
        
        $save = mProductos::saveLinea($array['linea']);
        if($save){
            setMensaje('Tipo creada correctamente','success');
        }else{
            setMensaje('Error al crear el tipo.','error');
        }
        location(setUrl('productos','tipo'));
    }
    
    public function editartipos($array){
        validar_usuario();
        $info = mProductos::listarLineas($array['id']);
        parent::editartipos($info[0]);
    }
    
    public function editarTipopro($array=null){
        validar_usuario();
        if(mProductos::updLineas($array['id'],$array['linea'])){
            setMensaje('Tipo editada correctamente','success');
        }else{
            setMensaje('Error al editar el tipo.','error');
        }
        location(setUrl('productos','tipo'));
    }
    
    public function delTipo($array=null){
        validar_usuario();
        if(mProductos::delLinea($array['id'])){
            setMensaje('Tipo eliminado correctamente','success');
        }else{
            setMensaje('Error al eliminar el tipo.','error');
        }
        location(setUrl('productos','tipo'));
    }
    
     public function linea($array){
        validar_usuario();	
        
        $info = mProductos::listarLineas2();
        
        parent::linea($info);
    }//main
    
    public function saveLineas($array=null){
        validar_usuario();
        
        $save = mProductos::saveLinea2($array['linea']);
        if($save){
            setMensaje('Linea creada correctamente','success');
        }else{
            setMensaje('Error al crear la linea.','error');
        }
        location(setUrl('productos','linea'));
    }
    
    public function editarlinea($array){
        validar_usuario();
        $info = mProductos::listarLineas2($array['id']);
        parent::editarlinea($info[0]);
    }
    
    public function editarLineapro($array=null){
        validar_usuario();
        if(mProductos::updLineas2($array['id'],$array['linea'])){
            setMensaje('Linea editada correctamente','success');
        }else{
            setMensaje('Error al editar la linea.','error');
        }
        location(setUrl('productos','linea'));
    }
    
    public function delLinea($array=null){
        validar_usuario();
        if(mProductos::delLinea2($array['id'])){
            setMensaje('Linea eliminada correctamente','success');
        }else{
            setMensaje('Error al eliminar la linea.','error');
        }
        location(setUrl('productos','linea'));
    }
    
    //json para simulador web
    public function lstProductos(){
    	 $lista = mProductos::listar();
    	 if(empty($lista)){
    	 	$lista = array();
         }
         header("Access-Control-Allow-Origin: *");
    	 echo json_encode($lista);
         
        /* $cadena = '';
         if(is_array($lista)){
             foreach ($lista As $i):
                 $cadena .= '<option value="'.$i["id"].'">'.$i["producto"].'</option>';
             endforeach;
         }
         header("Access-Control-Allow-Origin: *");
         echo $cadena;*/
    }
    
} // class
