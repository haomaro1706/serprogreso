<?php 

class mProductos{
	
	public function listar(){
		$orm = new ORM();
		/*$orm	-> select(array("pro.*","zon.zona as nZona"))
				-> from("op_productos","pro")
				->join(array("zon"=>"op_zonas"),'pro.zona = zon.id',"INNER");*/
                $orm	-> select(array("pro.*","li.nombre As nombreLi","lo.nombre As nombreLo"))
			-> from("op_productos","pro")
                        ->join(array("li"=>"op_productos_linea2"),'pro.linea = li.id',"INNER")
                        ->join(array("lo"=>"op_productos_linea"),'pro.logica = lo.id',"INNER");
		$return = $orm	-> exec()
						-> fetchArray();
		return $return;
	} // listar
	
	public function saveProducto($producto, $logica,$linea, $estado){
		$orm = new ORM();
		$sql = $orm	-> insert("op_productos")
					-> values(array(0, $producto,$logica,$linea,$estado));
		$return = $sql	-> exec()-> afectedRows();
		return $return > 0 ? true : false;
		
	}
	
	public function listarId($id){
		$orm = new ORM();
		$sql = $orm	-> select()
					-> from("op_productos")
					-> where('id =?',$id);
		$return = $orm	-> exec()
						-> fetchArray();
		return $return;
	} // listar
	
	public function updProducto($id, $prodcuto, $estado){
	  $orm = new ORM();
		$sql = $orm	-> update("op_productos")
					-> set(
						  array(
						   "producto"=>$prodcuto,
						   "estado"=>$estado
						  )
						)
					-> where("id = ?", $id);
        $orm -> exec();
		$retorno = $orm -> afectedRows() > 0 ? true : false;
        return $retorno;
	}//updProducto
	
	public function delProducto($id){
	  $orm = new ORM();
		$sql = $orm -> delete()
					-> from("op_productos")
					-> where("id = ? ", $id);
		$orm -> exec();
		$retorno = $orm -> afectedRows() > 0 ? true : false;
        return $retorno;
	}
	
	public function saveProductoZona($nombre, $prodcuto, $zona,  $estado){
		$orm = new ORM();
		$sql = $orm	-> insert("op_productos_zona")
					-> values(array(0, $nombre, $prodcuto, $zona,  $estado));
		$return = $orm	-> exec()
						-> afectedRows();
		return $return > 0 ? true : false;
		
	}
	
	public function listarpz(){
		$orm = new ORM();
		$query = $orm	-> select(array("pzona.*","zo.zona As nZona"))
						-> from("op_productos", "pzona")
						-> join(array("zo"=>"op_zonas"),'pzona.zona = zo.id',"INNER");
		$return = $query->exec()->fetchArray();
		return $return;
	} // listarpz
	
	public function listarpzId($id){
		$orm = new ORM();
		$query = $orm	-> select(array("pzona.*","zo.zona As nZona"))
					-> from("op_productos", "pzona")
					-> join(array("zo"=>"op_zonas"),'pzona.zona = zo.id',"INNER")
					->where('pzona.id =?',$id);
		$return = $query->exec()->fetchArray();
		return $return;
	} // listarpz
	
	public function updProductoZona($id, $nombre, $zona, $prodcuto, $estado){
	  $orm = new ORM();
		$sql = $orm	-> update("op_productos_zona")
					-> set(
						  array(
						   "nombre"	  => $nombre,
						   "zona"	  => $zona,
						   "producto" => $prodcuto,
						   "estado"	  => $estado
						  )
						)
					-> where("id = ?", $id);
        $orm -> exec();
		$retorno = $orm -> afectedRows() > 0 ? true : false;
        return $retorno;
	}//updProducto
	
	public function delProductozona($id){
		$orm = new ORM();
		$orm -> delete()
					-> from("op_productos_zona")
					-> where("id = ? ", $id);
		$orm -> exec();
		$retorno = $orm -> afectedRows() > 0 ? true : false;
        return $retorno;
	}
	
	public function listarZona($id){
		$orm = new ORM();
		$orm	-> select()
					-> from("op_sucursales")
					-> where('id =?',$id);
		$return = $orm	-> exec()
						-> fetchArray();
		return $return;
		
	} // listar
	
	public function selectbyPoZona($producto,$zona){
		$orm = new ORM();
		$orm	->select(array("pro.*","zon.zona as nZona"))
				->from("op_productos","pro")
				->join(array("zon"=>"op_zonas"),'pro.zona = zon.id',"INNER")
				->where("producto =?",$producto)
				->where("pro.zona =?",$zona);
		$return = $orm	-> exec()
						-> fetchArray();
		return $return;
	}
        
        public function saveLinea($linea){
		$orm = new ORM();
		$sql = $orm	-> insert("op_productos_linea")
                                -> values(array(0,$linea,1));
		$return = $sql	-> exec()-> afectedRows();
		return $return > 0 ? true : false;
		
	}
        
        public function listarLineas($id=false){
		$orm = new ORM();
		$orm	-> select()
                        -> from("op_productos_linea");
                        if($id){
                            $orm->where('id=?', $id);
                        }
		$return = $orm	-> exec()-> fetchArray();
		return $return;
	} // listarlineas
        
        public function updLineas($id,$nombre){
            $orm = new ORM();
            $sql = $orm	-> update("op_productos_linea")
                            -> set(
                                      array(
                                       "nombre" => $nombre
                                      )
                                    )
                            -> where("id = ?", $id);
		$retorno = $sql ->exec() -> afectedRows() > 0 ? true : false;
        return $retorno;
	}//updProducto
        
        public function delLinea($id){
		$orm = new ORM();
		$orm -> delete()
                        -> from("op_productos_linea")
                        -> where("id = ? ", $id);
		$orm -> exec();
		$retorno = $orm -> afectedRows() > 0 ? true : false;
        return $retorno;
	}
        
        public function saveLinea2($linea){
		$orm = new ORM();
		$sql = $orm	-> insert("op_productos_linea2")
                                -> values(array(0,$linea,1));
		$return = $sql	-> exec()-> afectedRows();
		return $return > 0 ? true : false;
		
	}
        
        public function listarLineas2($id=false){
		$orm = new ORM();
		$orm	-> select()
                        -> from("op_productos_linea2");
                        if($id){
                            $orm->where('id=?', $id);
                        }
		$return = $orm	-> exec()-> fetchArray();
		return $return;
	} // listarlineas
        
        public function updLineas2($id,$nombre){
            $orm = new ORM();
            $sql = $orm	-> update("op_productos_linea2")
                            -> set(
                                      array(
                                       "nombre" => $nombre
                                      )
                                    )
                            -> where("id = ?", $id);
		$retorno = $sql ->exec() -> afectedRows() > 0 ? true : false;
        return $retorno;
	}//updProducto
        
        public function delLinea2($id){
		$orm = new ORM();
		$orm -> delete()
                        -> from("op_productos_linea2")
                        -> where("id = ? ", $id);
		$orm -> exec();
		$retorno = $orm -> afectedRows() > 0 ? true : false;
        return $retorno;
	}
        
} // class
