<?php 

class vProductos{
    public function main($lista=null,$logica=null,$lineas=null){
      echo getMensaje();
        ?>
           <h2><?php  echo PRODUCTOS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('parametros'),
            		  PRODUCTOS_NOMBRE=>'#',
            		  PRODUCTOS_LINEA=>setUrl('productos','tipo'),
            		  PRODUCTOS_LINEA2=>setUrl('productos','linea')
            		 ),
            	    PRODUCTOS_NOMBRE
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  PRODUCTOS_NOMBRE=>'#'
            		 ),
            	    PRODUCTOS_NOMBRE, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo PRODUCTOS_NOMBRE?></h4>
       <div class='container'>
        <div class='span10 offset1'>
	  <fieldset>
				<legend><?php echo PRODUCTOS_CREAR?></legend>
			<?php
				Html::openForm("producto",setUrl("productos","saveProducto"));
				Html::newInput("producto","Producto *");
				Html::newSelect("logica", "Logica *", $logica);
				Html::newSelect("linea", "Linea *", $lineas);
				Html::newSelect("estado", "Estado *", array("1"=>"Activo", "2"=>"Inactivo"));
			?>
	  </fieldset>
        <?php
				Html::newButton("enviar", "Enviar", "submit");
				Html::closeForm();
			?>
			<?php
				if(!empty($lista)){
					echo datatable("tabla");
					Html::tabla(
							array("Producto","Linea","Logica","Estado","Editar","Eliminar"),
									$lista,
									array("producto","nombreLi","nombreLo","estado"),
									array("editar"=>setUrl("productos","editProducto"),"eliminar"=>setUrl("productos","delProducto"))
								);
				}else{
				  ?>
	    <div class="alert alert-info">No hay prodcutos disponibles.</div>
				  <?php
				}
			?>
        </div>
       </div>
       <?php  
    }
    
    public function editProducto($datos=null,$zonas=null){
      echo getMensaje();
        ?>
           <h2><?php  echo PRODUCTOS_EDITAR?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('productos'), 
            		  PRODUCTOS_EDITAR=>'#'
            		 ),
            	    PRODUCTOS_EDITAR
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('productos'), 
            		  PRODUCTOS_EDITAR=>'#'
            		 ),
            	    PRODUCTOS_EDITAR, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo PRODUCTOS_EDITAR?></h4>
       <div class='container'>
        <div class='span10 offset1'>
	  <fieldset>
				<legend><?php echo PRODUCTOS_EDITAR?></legend>
			<?php
				Html::openForm("producto",setUrl("productos","editProductoPro"));
				Html::newInput("producto","Producto *",$datos['producto']);
				//Html::newSelect("zona", "Zona *", $zonas,$datos['zona']);
				Html::newSelect("estado", "Estado *", array("1"=>"Activo", "2"=>"Inactivo"),$datos['estado']);
			?>
	  </fieldset>
			<?php
				Html::newHidden("id",$datos['id']);
				Html::newButton("editar", "Editar", "submit");
				Html::closeForm();
			?>
			
        </div>
       </div>
       <?php  
    }//editProducto
    
    public function tipo($lista=null,$zonas=null){
      echo getMensaje();
        ?>
           <h2><?php  echo PRODUCTOS_LINEA?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('parametros'),
            		  PRODUCTOS_NOMBRE=>setUrl('productos'),
            		  PRODUCTOS_LINEA=>'#',
            		  PRODUCTOS_LINEA2=>setUrl('productos','linea')
            		 ),
            	    PRODUCTOS_LINEA
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  PRODUCTOS_LINEA=>'#'
            		 ),
            	    PRODUCTOS_LINEA, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo PRODUCTOS_LINEA?></h4>
       <div class='container'>
        <div class='span10 offset1'>
	  <fieldset>
				<legend><?php echo PRODUCTOS_CREAR_LINEA?></legend>
			<?php
				Html::openForm("producto",setUrl("productos","saveTipos"));
				Html::newInput("linea","Tipo *");
				//Html::newSelect("estado", "Estado *", array("1"=>"Activo", "2"=>"Inactivo"));
			?>
	  </fieldset>
        <?php
				Html::newButton("enviar", "Enviar", "submit");
				Html::closeForm();
			?>
			<?php
				if(!empty($lista)){
					echo datatable("tabla");
					Html::tabla(
                                        array("Tipo","Estado","Editar","Eliminar"),
                                                $lista,
                                                array("nombre","estado"),
                                                array("editar"=>setUrl("productos","editartipos"),"eliminar"=>setUrl("productos","delTipo"))
                                                );
				}else{
				  ?>
	    <div class="alert alert-info">No hay tipos disponibles.</div>
				  <?php
				}
			?>
        </div>
       </div>
       <?php  
    }
    
    public function editartipos($info=null){
      echo getMensaje();
        ?>
           <h2><?php  echo PRODUCTOS_LINEA_EDITAR?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('productos','lineas'),
            		  PRODUCTOS_LINEA_EDITAR=>'#'
            		 ),
            	    PRODUCTOS_LINEA_EDITAR
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('productos','lineas'), 
            		  PRODUCTOS_LINEA_EDITAR=>'#'
            		 ),
            	    PRODUCTOS_LINEA_EDITAR, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo PRODUCTOS_LINEA_EDITAR?></h4>
       <div class='container'>
        <div class='span10 offset1'>
	  <fieldset>
                <legend><?php echo PRODUCTOS_CREAR_LINEA?></legend>
                <?php
                    Html::openForm("producto",setUrl("productos","editarTipopro"));
                    Html::newInput("linea","Tipo *",$info['nombre']);
                    //Html::newSelect("estado", "Estado *", array("1"=>"Activo", "2"=>"Inactivo"));
                ?>
	  </fieldset>
            <?php
                Html::newHidden('id',$info['id']);
                Html::newButton("enviar", "Editar", "submit");
                Html::closeForm();
            ?>
        </div>
       </div>
       <?php  
    }
    
    public function linea($lista=null,$zonas=null){
      echo getMensaje();
        ?>
           <h2><?php  echo PRODUCTOS_LINEA?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('parametros'),
            		  PRODUCTOS_NOMBRE=>setUrl('productos'),
            		  PRODUCTOS_LINEA=>setUrl('productos','tipo'),
            		  PRODUCTOS_LINEA2=>'#'
            		 ),
            	    PRODUCTOS_LINEA2
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  PRODUCTOS_LINEA2=>'#'
            		 ),
            	    PRODUCTOS_LINEA2, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo PRODUCTOS_LINEA2?></h4>
       <div class='container'>
        <div class='span10 offset1'>
	  <fieldset>
				<legend><?php echo PRODUCTOS_CREAR_LINEA2?></legend>
			<?php
				Html::openForm("producto",setUrl("productos","saveLineas"));
				Html::newInput("linea","Lineas *");
				//Html::newSelect("estado", "Estado *", array("1"=>"Activo", "2"=>"Inactivo"));
			?>
	  </fieldset>
        <?php
				Html::newButton("enviar", "Enviar", "submit");
				Html::closeForm();
			?>
			<?php
				if(!empty($lista)){
					echo datatable("tabla");
					Html::tabla(
                                        array("Linea","Estado","Editar","Eliminar"),
                                                $lista,
                                                array("nombre","estado"),
                                                array("editar"=>setUrl("productos","editarlinea"),"eliminar"=>setUrl("productos","delLinea"))
                                                );
				}else{
				  ?>
	    <div class="alert alert-info">No hay lineas disponibles.</div>
				  <?php
				}
			?>
        </div>
       </div>
       <?php  
    }
    
    public function editarlinea($info=null){
      echo getMensaje();
        ?>
           <h2><?php  echo PRODUCTOS_LINEA_EDITAR2?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('productos','linea'),
            		  PRODUCTOS_LINEA_EDITAR2=>'#'
            		 ),
            	    PRODUCTOS_LINEA_EDITAR2
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('productos','linea'), 
            		  PRODUCTOS_LINEA_EDITAR2=>'#'
            		 ),
            	    PRODUCTOS_LINEA_EDITAR2, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo PRODUCTOS_LINEA_EDITAR2?></h4>
       <div class='container'>
        <div class='span10 offset1'>
	  <fieldset>
                <legend><?php echo PRODUCTOS_LINEA_EDITAR2?></legend>
                <?php
                    Html::openForm("producto",setUrl("productos","editarLineapro"));
                    Html::newInput("linea","Linea *",$info['nombre']);
                    //Html::newSelect("estado", "Estado *", array("1"=>"Activo", "2"=>"Inactivo"));
                ?>
	  </fieldset>
            <?php
                Html::newHidden('id',$info['id']);
                Html::newButton("enviar", "Editar", "submit");
                Html::closeForm();
            ?>
        </div>
       </div>
       <?php  
    }

    
} // class