<?php 

include_once 'app/modulos/acuerdos/vista/v.Acuerdos.php';
include_once 'app/modulos/acuerdos/modelo/m.Acuerdos.php';

class Acuerdos extends vAcuerdos{

    public function main(){
        /*Funcion Principal*/
	  validar_usuario();
	  $info = mAcuerdos::lstAcuerdos();
	  //die(print_r($info));
        parent::main($info);    
    }
    
    public function ver($array=null){
	  validar_usuario();
	  $info = mAcuerdos::lstAcuerdosId($array['id']);
	 //die(print_r($info));
        parent::ver($info);
    }
    
    public function pagar($array=null){
	  validar_usuario();
	  $upd = mAcuerdos::updAcuerdosId($array['id']);
	  
	  if($upd){
	    setMensaje('Pago realizado correctamente','success');
	  }else{
	    setMensaje('Error al realizar el pago','error');
	  }
	  location(setUrl('acuerdos','ver',array("id"=>$array["id2"])));
    }
                            
} // class
?>
