<?php 

class vAcuerdos{
    public function main($info){
      echo getMensaje();
        ?>
           <h2><?php  echo ACUERDOS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  ACUERDOS_NOMBRE=>'#'
            		 ),
            	    ACUERDOS_NOMBRE
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  ACUERDOS_NOMBRE=>'#'
            		 ),
            	    ACUERDOS_NOMBRE, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo ACUERDOS_NOMBRE?></h4>
       <div class='container'>
        <div class='span8 offset1'> 
        
	     <?php echo datatable('lst')?>
	    <table class="table table-hover" id="lst">
		<thead>
		  <tr>
		    <th>#</th>
		    <th>Cliente</th>
		    <th>Producto</th>
		    <th>Monto</th>
		    <th>Plazo</th>
		    <th>tasa</th>
		    <!--<th>Subir adjuntos</th>
		    <th>Ver</th>
		    <th>Editar</th>
		    <th>Aprobar</th>-->
		    <th>Ver</th>
		  </tr>
		</thead>
		<tbody>
		  <?php
			if(is_array($info)){
			  $j=1;
		    foreach ($info As $i):
		  ?>
		  <tr>
		    <td><?php echo $j?></td>
		    <td><?php echo $i['nombre'].' '.$i['apellido']?></td>
		    <td><?php echo $i['nProducto']?></td>
		    <td><?php echo toMoney($i['valor'])?></td>
		    <td><?php echo $i['periodo']?></td>
		    <td><?php echo $i['tasa']?>%</td>
		    <td>
		    <a href="<?php echo setUrl('acuerdos','ver',array('id'=>trim($i["elId"])))?>"><img src="app/img/ver.png" alt="adjuntos" title="adjuntos"></a>
		    </td>
		  </tr>
		  <?php
			$j++;
		    endforeach;
			}else{
			  echo 'No hay datos';
			}
		  ?>
		</tbody>
	    </table>
	    
        </div>
       </div>
       <?php  
    }
    
        public function ver($info){
      echo getMensaje();
        ?>
           <h2><?php  echo ACUERDOS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  ACUERDOS_VER=>'#'
            		 ),
            	    ACUERDOS_VER
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  ACUERDOS_VER=>'#'
            		 ),
            	    ACUERDOS_VER, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo ACUERDOS_VER?></h4>
       <div class='container'>
        <div class='span8 offset1'> 
        
	     <?php echo datatable('lst2')?>
	    <table class="table table-hover" id="lst2">
		<thead>
		  <tr>
		    <th>#</th>
		    <th>Capital</th>
		    <th>Interes</th>
		    <th>Cuota fija</th>
		    <th>Saldo</th>
		    <th>Fecha pago</th>
		    <th>Estado</th>
		    <th>Accion</th>
		  </tr>
		</thead>
		<tbody>
		  <?php
			if(is_array($info)){
			  $j=1;
		    foreach ($info As $i):
		  ?>
		  <tr>
		    <td><?php echo $j?></td>
		    <td><?php echo toMoney($i['capital'])?></td>
		    <td><?php echo toMoney($i['interes'])?></td>
		    <td><?php echo toMoney($i['cuotaFija'])?></td>
		    <td><?php echo toMoney($i['saldo'])?></td>
		    <td><?php echo $i['fechaPago']?></td>
		    <td><?php echo ($i['estado']=='1')?'Sin pagar':'Pagada'?></td>
		    <td>
		    <?php if($i['estado']=='1'){ ?>
			<a onclick="return confirm('Desea realizar el pago?')" href="<?php echo setUrl('acuerdos','pagar',array('id'=>trim($i["id"]),'id2'=>trim($i["id_acuerdo"])))?>"><img src="app/img/realizado.png" alt="adjuntos" title="adjuntos"></a>
		    <?php }?>
		    </td>
		  </tr>
		  <?php
			$j++;
		    endforeach;
			}else{
			  echo 'No hay datos';
			}
		  ?>
		</tbody>
	    </table>
	    
        </div>
       </div>
       <?php  
    }
} // class
        ?>