<?php 

class mAcuerdos{
	
	public function newAcuerdo($valor, $periodo, $tasa, $solicitud){
		$orm = new ORM();
		$sql = $orm -> insert("op_acuerdos")
					-> values(array(0, $valor, $periodo, $tasa, $solicitud));
		return $orm -> exec() -> afectedRows();
	} // newAcuerdo
	
	public function lastAcuerdo($id){
		$orm = new ORM();
		$sql = $orm -> select(array("id"))
					-> from ("op_acuerdos")
					-> where("id_solicitud = ? ",$id)
					-> orderBy(array("id_solicitud DESC"))
					-> limit(0,1);
		return $orm -> exec() -> fetchArray();
	} // newAcuerdo
	
	
	public function saveCuota($idAcuerdo, $capital, $interes, $saldo, $total, $fechaPago){
		$orm = new ORM();
		$sql = $orm -> insert("op_acuerdos_cuotas")
					-> values(array(0, $idAcuerdo, $capital, $interes, $saldo, $total, $fechaPago, 1));
		return $orm -> exec() -> afectedRows();
	} // newAcuerdo
	
	public function lstAcuerdos(){
		$orm = new ORM();
		$orm->select(array("acuerdo.*","acuerdo.id As elId","sol.*","cli.*","pro.producto As nProducto"))
				   ->from("op_acuerdos","acuerdo")
				   ->join(array("sol"=>"op_solicitudes"),'acuerdo.id_solicitud = sol.id',"INNER")
				   ->join(array("cli"=>"op_clientes"),'sol.cliente = cli.numero',"INNER")
				   ->join(array("pro"=>"op_productos"),'sol.producto = pro.id',"INNER");
		$return=$orm->exec()->fetchArray(); 
	    return $return;
	}
	
	public function lstAcuerdosId($id){
	    $orm = new ORM();
	    $sql = $orm -> select()
				    -> from ("op_acuerdos_cuotas")
				    -> where("id_acuerdo = ? ",$id);
	    return $sql -> exec() -> fetchArray();
	}
	
	public function updAcuerdosId($id){
	  $orm = new ORM();
		$query = $orm	-> update("op_acuerdos_cuotas")
						-> set (array(
								  "estado"=>"2"
								  )
							  )
						-> where("id=?",$id);
		$return = $query->exec() -> afectedRows();
		
		return $return;		
	}
    
} // class
?>
