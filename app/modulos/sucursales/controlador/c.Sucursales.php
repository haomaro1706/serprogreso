<?php 
            
include_once 'app/modulos/sucursales/vista/v.Sucursales.php';
include_once 'app/modulos/sucursales/modelo/m.Sucursales.php';
include_once 'app/modulos/zonas/modelo/m.Zonas.php';

class Sucursales{
	
	public function main(){
	    validar_usuario();
		$mSucursales = new mSucursales();
		$lstSucursales = $mSucursales->lstSucursales();
		$cadena='';
		if(is_array($lstSucursales) && count($lstSucursales)>0){
        $i=1;

            foreach($lstSucursales as $key=>$sucursal){

                $cadena .= '<tr> <td>'.$i.'</td>';
                $cadena .= '<td>'.$sucursal["nombre_suc"].'</td>';
				$cadena .= '<td>'.$sucursal["zona"].'</td>';
				$cadena .= '<td>'.$sucursal["dir"].'</td>';
				$cadena .= '<td>'.$sucursal["tel_fijo"].'</td>';
				$cadena .= '<td>'.$sucursal["ciudad"].'</td>';
				$cadena .= '<td>'.$sucursal["email"].'</td>';
				$cadena .= '<td>'.$sucursal["obser"].'</td>';
                $cadena .= '<td> <a href="'.setUrl("sucursales","vEditar", array("id"=>$sucursal["id"])).'" title="Editar"> <img src="'.URLADMIN().'app/img/editar.png" alt="Editar"> </a> </td>';
                $cadena .= '<td> <a onclick="return confirm(\'Desea eliminar la sucursal\');" href="'.setUrl("sucursales","delSuc", array("id"=>$sucursal["id"])).'" title="Eliminar"> <img src="'.URLADMIN().'app/img/eliminar.png" alt="Eliminar"> </a> </td> </tr>';
                $i++;

            }
        }else{
            setMensaje("No existen sucurales","info");
        }
		$cadena = utf8_encode($cadena);
        vSucursales::Vmain($cadena);    
    }
	
	public function crearSucursal (){
		validar_usuario();
		$lista = mZonas::listar();
		$zonas = '';
		
		if(is_array($lista)){
			foreach($lista as $zona){
				$zonas .= '<option value="'.$zona["id"].'">'.$zona["zona"].'</option>';
			}
		}else{
			$zonas = '<option>Sin dato de zonas</option>';
		}
    	vSucursales::vCrearSucursal($zonas);
	}
	
	public function ingresarSucursal($array=null){
        validar_usuario();
        $mSucursales=new mSucursales();
        $guardar = $mSucursales->ingresarSucursal(
									$array['zona'],
									$array['nombre'],
									$array['dir'],
									$array['tel'],
									$array['cel'],
									$array['fax'],
									$array['ciudad'],
									$array['pais'],
									$array['email'],
									$array['obser']
									);
			setMensaje("La sucursal fue creada exitosamente", "success");
			location(setUrl("sucursales"));
	}
    
	public function delSuc($array){
		validar_usuario();
		if(is_numeric($array["id"])){
			$mSucursal = new mSucursales();
			$del = $mSucursal->delSuc($array["id"]);
			if($del){
				setMensaje("La sucursal eliminada satisfactoriamente","success");
			}else{
				setMensaje("Error al eliminar la sucursal","error");
			}
			location(setUrl("sucursales"));
		}
	}//eliminar sucursal
	
	public function vEditar($array){
		$mSucursal = new mSucursales();
		$info=$mSucursal->selectbyId($array['id']);
		
		
		if($info){
			$mZonas = new mZonas();
			$lista = $mZonas->listar();
			$zonas = '';
			if(is_array($lista)){
				foreach($lista as $zona){
					$zonas .= '<option value="'.$zona["id"].'" '.(($zona["id"] == $info[0]["id_zona"])? 'selected="selected"':'').'>'.$zona["zona"].'</option>';
				}
			}else{
				$zonas = '<option>Sin dato de zonas</option>';
			}
			vSucursales::vEditar($info=$info[0],$zonas);
		}
		else{
			setMensaje ("No se ha podido encontrar la informaci&oacute;n de la zona", "error");
			location(setUrl("sucursales"));
		}
	}// vSucursales::EDITAR //
	
	public function editarSucursal($array){
		validar_usuario();
		$mSucursal= new mSucursales();
		$upd=$mSucursal->updSuc(	$array['id'],
									$array['zona'],
									$array['nombre'],
									$array['dir'],
									$array['tel'],
									$array['cel'],
									$array['fax'],
									$array['ciudad'],
									$array['pais'],
									$array['email'],
									$array['obser']);
		if($upd){
			setMensaje("La sucursal ha sido  modificada satisfactoriamente","success");
		}else{
			setMensaje("Error interno al intentar modificar la sucursal","error");
		}			
		location(setUrl("sucursales"));
		
	}
	
	
	public function postSucursales($array){
		validar_usuario();
		$info = mSucursales::sucurbyId($array["id"]);
		if(is_array($info)){
			$cad = '';
			foreach($info as $key => $val){
				$cad .= '<option value="'.$val["id"].'">'.$val["nombre_suc"].'</option>';
			}
			echo $cad;
		}
	}
} // class
?>
