<?php 
            
class vSucursales{
    public function Vmain($listaSucursales){
      echo getMensaje();
        ?>
           <h2><?php echo SUCURSALES_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  SUCURSALES_NOMBRE=>'#',
            		  SUCURSALES_CREAR=>setUrl('sucursales','crearSucursal'),
            		 ),
            	    SUCURSALES_NOMBRE
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  SUCURSALES_NOMBRE=>'#'
            		 ),
            	    SUCURSALES_NOMBRE, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php echo SUCURSALES_NOMBRE?></h4>
       <div class='container'>
        <div class='span10 offset0'> 
        <div class=""> 
			<?php echo datatable("listaSucursales")?>
		<table class="table table-striped table-hover table-condensed" id="listaSucursales">
    			<thead>
                            <tr> 
    				<th> # </th>
                                <th> Nombre </th>
                                <th> Zona </th>
                                <th> Dirección </th>
                                <th> Telefono </th>
                                <th> Ciudad </th>
                                <th> Email </th>
                                <th> Observaciones </th>
                                <th> Editar </th>
                                <th> Eliminar </th>
                            </tr>
                        </thead>
                        <tbody>
			<?php echo $listaSucursales; ?>
                        </tbody>
                </table>
        </div>
       </div>
       <?php  
    }
	
	public function vCrearSucursal($zonas){
		echo getMensaje();
		?>
		<h2><?php echo SUCURSALES_CREAR?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  SUCURSALES_NOMBRE=>setUrl('sucursales'),
            		  SUCURSALES_CREAR=>'#',
            		 ),
            	    SUCURSALES_CREAR
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  SUCURSALES_CREAR=>'#'
            		 ),
            	    SUCURSALES_CREAR, 
            	    'breadcrumb'
            	   );
            ?>
		<h4><?php echo SUCURSALES_CREAR?></h4>
		<div class="container">
        <div class="span7 offset2">            
        <fieldset >
				<legend><?php echo "Información de Sucursal"?></legend>
				<?php
			  Html::openForm("sucursales",setUrl("sucursales","ingresarSucursal"));
			  Html::newInput("nombre","Nombre *");
			  Html::newInput("dir","Direccion *");
			  ?>
			  <div class="control-group">
													<label class="control-label" for="zona">Zona *:</label>
													<div class="controls">
														<select name="zona">
															<option value="">-Seleccione-</option>
															<?php  echo $zonas?>
														</select>
													</div>
												</div>
			  <?php
			  Html::newInput("tel","Teléfono Fijo *");
			  Html::newInput("cel","Teléfono Celular *");
			  Html::newInput("fax","Fax *");
			  Html::newInput("ciudad","Ciudad *");
			  Html::newInput("pais","Pais *");
			  ?>
			  <div class="control-group" id="email">
                    <label class="control-label" for="prov">E-mail:</label>
                    <div class="controls">
                        <input type="email" name="email" id="email" placeholder="E-mail" autocomplete="off"  >
                    </div>
               </div>  
				
				<div class="control-group" id="observaciones">
					<label class="control-label" for="obser">Observaciones:</label>
					<div class="controls">
						<textarea name="obser" id="obser" placeholder="Observaciones" autocomplete="off"></textarea>
					</div>
				</div>
				</fieldset>		
			  <?php
			  Html::newButton("enviar", "Registrar Sucursal", "submit");
			  Html::closeForm();
			?>
		</div>
        </div>
		<?php
	}
	
	public function vEditar($infoSucursal,$zonas){
		echo getMensaje();
		?>
		<h2><?php echo SUCURSALES_EDITAR?></h2>
            <?php 
            navTabs(array(OP_BACK=>setUrl("usuarios","panelAdmin"), 
            		  SUCURSALES_NOMBRE=>setUrl("sucursales"), 
					  SUCURSALES_EDITAR=>"#",
            		  SUCURSALES_CREAR=>setUrl("usuarios","crearUsuario"),
            		 ),
                    SUCURSALES_EDITAR
            	   );
            
            navTabs(array(OP_BACK=>setUrl("usuarios","panelAdmin"), 
                    SUCURSALES_NOMBRE=>setUrl("sucursales"),
            		SUCURSALES_EDITAR=>"#"
            		),
            	    SUCURSALES_EDITAR, 
            	    "breadcrumb"
            	   );
				   ?>
            <h4><?php echo SUCURSALES_EDITAR?></h4>
		<div class="container">
        <div class="span7 offset2">            
        <form class="form-horizontal" enctype="multipart/form-data" name="crearSucursal2" id="crearSucursal2" action="<?php echo setUrl("sucursales","editarSucursal")?>" method="post">
			<fieldset>
				<legend><?php echo SUCURSALES_EDITAR?></legend>
				
				<?php
			  Html::openForm("sucursales",setUrl("sucursales","editarSucursal"));
			  Html::newInput("nombre","Nombre *",$infoSucursal["nombre_suc"]);
			  Html::newInput("dir","Direccion *",$infoSucursal["dir"]);
			  ?>
			  <div class="control-group">
													<label class="control-label" for="zona">Zona *:</label>
													<div class="controls">
														<select name="zona">
															<option value="">-Seleccione-</option>
															<?php  echo $zonas?>
														</select>
													</div>
												</div>
			  <?php
			  Html::newInput("tel","Teléfono Fijo *",$infoSucursal["tel_fijo"]);
			  Html::newInput("cel","Teléfono Celular *",$infoSucursal["tel_cel"]);
			  Html::newInput("fax","Fax *",$infoSucursal["fax"]);
			  Html::newInput("ciudad","Ciudad *",$infoSucursal["ciudad"]);
			  Html::newInput("pais","Pais *",$infoSucursal["pais"]);
			  ?>
			  <div class="control-group" id="email">
                    <label class="control-label" for="prov">E-mail:</label>
                    <div class="controls">
                        <input type="email" name="email" id="email" placeholder="E-mail" autocomplete="off"  value="<?php echo $infoSucursal["email"]?>">
                    </div>
               </div>  
				
				<div class="control-group" id="observaciones">
					<label class="control-label" for="obser">Observaciones:</label>
					<div class="controls">
						<textarea name="obser" id="obser" placeholder="Observaciones" autocomplete="off"><?php echo $infoSucursal["obser"]?></textarea>
				</div>
				</div>	
			  <?php
			  Html::newHidden("id",$infoSucursal['id']);
			  Html::newButton("enviar", "Modificar Sucursal", "submit");
			  Html::closeForm();
			?>
        </div>
        </div>		
		<?php
	}
} // class
        ?>
