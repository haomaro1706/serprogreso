/*javascript para el modulo js/*/
function clonar(tr){
	$(tr).parent().parent().clone().appendTo('#lstPers').find(':text').val('');
}

function eliminar(tr){
	$(tr).parent().parent().remove();
}

$(function(){
	$("#sucursales").validate({
		rules:{
			nombre:{required:true, lettersonly:true,minlength:6},
			dir:{required:true},
			tel:{required:true, number:true, minlength:6},
			cel:{required:true, number:true, minlength:10},
			ciudad:{required:true, lettersonly:true},
			pais:{required:true, lettersonly:true},
			email:{required:true, email:true}

		}

	})

	})