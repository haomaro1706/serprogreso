<?php
class mSucursales{
	
	public function lstSucursales(){
    	$orm = new ORM();
		$orm->select(array("suc.*","zon.zona"))
				   ->from("op_sucursales","suc")
				   ->join(array("zon"=>"op_zonas"),'suc.id_zona = zon.id',"INNER");
		$return=$orm->exec()->fetchArray(); 
	    return $return;
    } // listar Cliente	
	
	public function lstSuc(){
		$orm = new ORM();
		$orm -> select(array("id","nombre_suc"))
			 -> from("op_sucursales");
		return $orm -> exec() -> fetchArray();
	}
	
	public function ingresarSucursal($zona, $nombre, $dir, $tel,$cel, $fax, $ciudad, $pais, $email, $obser,$prod){
		$orm = new ORM();
		$orm	-> insert("op_sucursales")
				-> values(array(0, $zona, $nombre,$dir,$tel,$tel,$fax,$ciudad,$pais,$email,$obser,$prod));
		$return = $orm->exec();
		return $return;
	}
	
	public function delSuc($id){
		$orm = new ORM();
		$orm	-> delete()
						-> from("op_sucursales")
						-> where("id=?",$id);		
		$return = $orm->exec();
		return $return;
	}
	
	public function selectbyId($id){
    	$orm = new ORM();
		$orm	-> select()
						-> from("op_sucursales")
						-> where("id=?",$id);
		$return = $orm->exec()->fetchArray();
    	return $return;
    } // LISTAR POR ID //	
	
	public function updSuc($id, $zona, $nombre, $dir, $tel_fijo,$tel_cel, $fax, $ciudad, $pais, $email, $obser,$prod){
		$orm = new ORM();
		$orm	-> update("op_sucursales")
						-> set (
                                                        array(	
                                                            "id_zona"=>$zona,
                                                            "nombre_suc"=>$nombre,
                                                            "dir"=>$dir,
                                                            "tel_fijo"=>$tel_fijo,
                                                            "tel_cel"=>$tel_cel,
                                                            "fax"=>$fax,
                                                            "ciudad"=>$ciudad,
                                                            "pais"=>$pais,
                                                            "email"=>$email,
                                                            "obser"=>$obser,
                                                            "productos"=>$prod
                                                            )
                                                        )
						-> where("id=?",$id);
		$return = $orm->exec();
		return $return;				  
	}
	
	public function sucurbyId($id){
            $orm = new ORM();
            $orm	-> select()
                    -> from("op_sucursales")
                    -> where("id_zona=?",$id);
            $return = $orm->exec()->fetchArray();
            return $return;
        } // LISTAR POR ID //
        
        public function sucurbyId2($id){
            $orm = new ORM();
            $orm	-> select()
                    -> from("op_sucursales")
                    -> where("id=?",$id);
            $return = $orm->exec()->fetchArray();
            return $return;
        } // LISTAR POR ID //	
	
} // class
