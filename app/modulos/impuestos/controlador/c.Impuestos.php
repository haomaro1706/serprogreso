<?php 

include_once 'app/modulos/impuestos/vista/v.Impuestos.php';
include_once 'app/modulos/impuestos/modelo/m.Impuestos.php';
include_once 'app/modulos/parametros/modelo/m.Parametros.php';
include_once 'app/modulos/productos/modelo/m.Productos.php';
include_once 'app/modulos/zonas/modelo/m.Zonas.php';

class Impuestos extends vImpuestos{

    public function main(){
        validar_usuario();
        $datos=mParametros::lstTasas();
        //die(print_r($datos));
        $s=0;
		$cadena='';
        if(is_array($datos)){
                $cadena='<ul>';
                foreach($datos as $campo){
                        $i=0;
                        if(is_array($campo)){
                                $nombres=unserialize($campo["nombre"]);
                                $valores=unserialize($campo["valor"]);
                                if(!empty($nombres) || !empty($valores))
                                foreach($nombres as $nombre){
                                        $cadena.= "<li>{$nombre}:{$valores[$i]}</li>";
                                        $i++;
                                }
                        }
                        $datos[$s]["nombre"]=$cadena.'</ul>';
                        $cadena='<ul>';
                        $s++;
                }
        }
        
        parent::main($datos);    
    }
    
    public function guardarTasas($array=null){
        validar_usuario();
        //die(serialize($array['descontable']));
        $save = mParametros::saveTasas(
                                    empty($array['id_zona'])?0:$array['id_zona'],
                                    $array['producto'],
                                    $array['tasa_ea'],
                                    $array['tasa_nominal'],
                                    $array['tasa_mensual'],
                                    $array['tasa_diaria'],
                                    $array['tasaMora'],
                                    serialize($array['nombre']),
                                    serialize($array['valor']),
                                    serialize($array['descontable']),
                                    serialize($array['iva'])
                                    );
        //die('fin');
		if($save){
			setMensaje('Tasas y Valores creados correctamente.','success');
		}else{
			setMensaje('Error al crear las Tasas','error');
		}
		location(setUrl("impuestos"));
	}
	
	public function eliminarTasas($array){
		validar_usuario();
		if(is_numeric($array["id"])){
			$del = mParametros::delTas($array["id"]);
			if($del){
				setMensaje("La informacion de las tasas y valores eliminada satisfactoriamente","success");
			}else{
				setMensaje("Error al eliminar la informacion","error");
			}
			location(setUrl("parametros"));
		}
		
	}
	
	public function editarTasas($array){
		validar_usuario();
		$info=mParametros::selectbyIdTasa($array['id']);
		//die('hdp');
		if($info){
			$infoP = mProductos::listarId($info[0]['id_producto']);
			$producto = $infoP[0]['producto'];
			$infoZ = mZonas::listarId($info[0]['id_zona']);
			$zona = $infoZ[0]['zona'];
		}
		else{
			setMensaje ("No se ha podido encontrar la informaci&oacute;n de las tasas", "error");
			location(setUrl("parametros","cobranzas"));
		}
		
		$cadena= '';
		$i=1;
		$s=0;
		$data1=unserialize($info[0]["nombre"]);
		$data2=unserialize($info[0]["valor"]);
		if(is_array($data1)){
			foreach($data1 as $nombre){
				$cadena .= '<tr> <td>'.$i.'</td>'; 
				$cadena .= '<td><input type="text" class="nombres" name="nombre[]" placeholder="Nombre" form="tasas" value='.$nombre.'></td>';
				$cadena .= '<td><input type="text" class="valores number" name="valor[]" placeholder="Valor" form="tasas" value='.$data2[$s].'></td>';
				$cadena .= '<td><button type="button" onclick="clonar(this)">Nuevo</button>';
				$cadena .=	'<button type="button" class="borrar" onclick= "Borrar(this)">Eliminar</button></td></tr>';
				$i++;
				$s++;
			}
		}else{
			$cadena .= '<tr> <td>'.$i.'</td>'; 
			$cadena .= '<td><input type="text" class="nombres" name="nombre[]" placeholder="Nombre" form="tasas"></td>';
			$cadena .= '<td><input type="text" class="valores number" name="valor[]" placeholder="Valor" form="tasas"></td>';
			$cadena .= '<td><button type="button" onclick="clonar(this)">Nuevo</button>';
			$cadena .=	'<button type="button" class="borrar" onclick= "Borrar(this)">Eliminar</button></td></tr>';
		}
		$cadena = utf8_encode($cadena);
                //die(print_r($info));
		parent::vEditarTasa($info[0],$cadena,$producto,$zona);
	}// PARENT::EDITAR //
	
	public function updTasas($array){
		validar_usuario();
               // die(print_r($array));
		$upd=mParametros::updTas(	
                                        $array['id'],
                                        $array['tasa_ea'],
                                        $array['tasa_nominal'],
                                        $array['tasa_mensual'],
                                        $array['tasa_diaria'],
                                        $array['tasaMora'],
                                        serialize($array['nombre']),
                                        serialize($array['valor']),
                                        serialize($array['descontable']),
                                        serialize($array['iva']),
                                        $array["producto"]
                                        );
		if($upd){
			setMensaje("La informacion de la tasa ha sido  modificada satisfactoriamente","success");
		}else{
			setMensaje("Error interno al intentar modificar la informacion","error");
		}			
		location(setUrl("impuestos"));
	}
                            
} // class