<?php 

class vImpuestos{
    public function main($datos=null){
      echo getMensaje();
        ?>
           <h2><?php  echo IMPUESTOS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('parametros','panelAdmin'), 
            		  IMPUESTOS_NOMBRE=>'#'
            		 ),
            	    IMPUESTOS_NOMBRE
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('parametros','panelAdmin'), 
            		  IMPUESTOS_NOMBRE=>'#'
            		 ),
            	    IMPUESTOS_NOMBRE, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo IMPUESTOS_NOMBRE?></h4>
       <div class='container'>
        <div class='span10 offset1'> 
        
            <fieldset>
			  <legend><?php echo "Informacion Tasas y Valores"?></legend>
			<?php
			  Html::openForm("tasas",setUrl("impuestos","guardarTasas"));
                          echo selectProductos();
			  Html::newInput("tasa_ea","Tasa EA %");
			 ?>
			  <div class="control-group">
				<label class="control-label" for="tasa_nominal"> Tasa Nominal Anual %: </label>
				<div class="controls">
					<input type="text" name="tasa_nominal" id="tasa_nominal" value="" readonly="readonly">
				</div>
			  </div>
			  
			  <div class="control-group">
				<label class="control-label" for="tasa_mensual"> Tasa Nominal Mensual %: </label>
				<div class="controls">
                                    <input type="text" name="tasa_mensual" id="tasa_mensual" value="" readonly="readonly">
				</div>
			  </div>
			  
			  <div class="control-group">
				<label class="control-label" for="tasa_diaria"> Tasa Nominal Diaria %: </label>
				<div class="controls">
					<input type="text" name="tasa_diaria" id="tasa_diaria" value="" readonly="readonly">
				</div>
			  </div>
			  
			  <?php
			  Html::newInput("tasaMora","Tasa de mora Nominal mensual%");
                          
		  ?>
	  </fieldset>
	  <fieldset>
	    <legend>Otros Cargos</legend>
	    <table class="table table-hover table-striped" name ="nombre" id="nombre" >
			<thead>
				<tr>
					<th>Nombre</th>
					<th>Valor</th>
					<th>Descontable</th>
					<th>IVA</th>
					<th>Operación</th>
					
			</thead>
			<tbody>
				<tr>
				  <td><input type="text" class="nombre" id="nombre" name="nombre[]" placeholder="Nombre" form="tasas"></td>
				  <td><input type="text"  class="number" id="valorr" name="valor[]" placeholder="Valor" form="tasas"></td>
                                  <td>
                                      <?php Html::newSelect('descontable[]','Descontable',array('1'=>'No','2'=>'Si'),'','form="tasas"');?>
                                  </td>
                                  <td><?php Html::newSelect('iva[]','IVA',array('1'=>'No','2'=>'Si'),'','form="tasas"');?></td>
                                  <td> 
                                      <a class="btn clonar" onclick="clonar2(this)">+</a>
                                      <a class="btn remover" onclick="remover2(this)">-</a>
                                  </td>
				</tr>
			</tbody>
	    </table>
	  </fieldset>
        <?php
		
		  Html::newHidden("cont",0);
		  Html::newButton("enviar", "Guardar", "submit");
		  Html::closeForm();

        if(!empty($datos)){
                echo datatable("tabla");
                Html::tabla(
                                array("Producto","Linea","Tasa EA","Tasa Nominal","Tasa Mensual","Tasa Diaria","Tasa de mora","Otros", "Editar","Eliminar"),
                                                $datos,
                                                array("producto","nombreli","tasa_ea","tasa_nominal","tasa_mensual","tasa_diaria","tasaMora","nombre"),
                                                array("editar"=>setUrl("impuestos","editarTasas"),"eliminar"=>setUrl("impuestos","eliminarTasas"))
                                        );
        }else{
          ?>
          <div class="alert alert-info">No hay Parametros disponibles.</div>
          <?php
        }
	?>
            
        </div>
       </div>
       <?php  
    }
    
    public function vEditarTasa($info){
        echo getMensaje();
        ?>
           <h2><?php  echo IMPUESTOS_EDITAR?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('impuestos'), 
            		  IMPUESTOS_EDITAR=>'#'
            		 ),
            	    IMPUESTOS_EDITAR
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('impuestos'), 
            		  IMPUESTOS_EDITAR=>'#'
            		 ),
            	    IMPUESTOS_EDITAR, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo IMPUESTOS_EDITAR?></h4>
       <div class='container'>
        <div class='span10 offset1'> 
        
            <fieldset>
			  <legend><?php echo "Informacion Tasas y Valores"?></legend>
			<?php
			  Html::openForm("tasas",setUrl("impuestos","updTasas"));
                          echo selectProductos($info['id_producto'],'style="display:none"');//revisar
                          echo nombreProducto($info['id_producto'])."<br/><br/><br/>";//revisar
			  Html::newInput("tasa_ea","Tasa EA %",$info['tasa_ea']);
			 ?>
			  <div class="control-group">
				<label class="control-label" for="tasa_nominal"> Tasa Nominal Anual %: </label>
				<div class="controls">
                                <input type="text" name="tasa_nominal" id="tasa_nominal" value="<?php echo $info['tasa_nominal']?>" readonly="readonly">
				</div>
			  </div>
			  
			  <div class="control-group">
				<label class="control-label" for="tasa_mensual"> Tasa Nominal Mensual %: </label>
				<div class="controls">
					<input type="text" name="tasa_mensual" id="tasa_mensual" value="<?php echo $info['tasa_mensual']?>" readonly="readonly">
				</div>
			  </div>
			  
			  <div class="control-group">
				<label class="control-label" for="tasa_diaria"> Tasa Nominal Diaria %: </label>
				<div class="controls">
					<input type="text" name="tasa_diaria" id="tasa_diaria" value="<?php echo $info['tasa_diaria']?>" readonly="readonly">
				</div>
			  </div>
			  
			  <?php
                          /*Html::newSelect('tasaEA','Tasa Descontable',array('1'=>'No','2'=>'Si'),$info['tasaD']);
			  Html::newInput("estudio_credito","Estudio de Credito *",$info['estudio_credito']);
                          Html::newSelect('estudioC','Estudio credito Descontable',array('1'=>'No','2'=>'Si'),$info['estudioD']);*/
			  Html::newInput("tasaMora","Tasa de mora %",$info['tasaMora']);
                          /*Html::newSelect('tMora','Mora Descontable',array('1'=>'No','2'=>'Si'),$info['moraD']);
                          Html::newInput("seguroVida","Seguro de vida %",$info['seguroVida']);
                          Html::newSelect('sVida','Seguro de vida Descontable',array('1'=>'No','2'=>'Si'),$info['seguroD']);*/
		  ?>
	  </fieldset>
	  <fieldset>
	    <legend>Otros Intereses</legend>
	    <table class="table table-hover table-striped" name ="nombre" id="nombre" >
			<thead>
				<tr>
					<th>Nombre</th>
					<th>Valor</th>
					<th>Descontable</th>
					<th>IVA</th>
					<th>Operación</th>
					
			</thead>
			<tbody>
                            <?php
                                if(count(unserialize($info['nombre']))>0){
                                    $valor = unserialize($info['valor']);
                                    $desc = unserialize($info['descontable']);
                                    $iva = unserialize($info['iva']);
                                    $i=0;
                                foreach (unserialize($info['nombre']) As $dato):            
                            ?>
                            <tr>
                            <td><input type="text" class="nombre" id="nombre" name="nombre[]" value="<?php echo $dato?>" placeholder="Nombre" form="tasas"></td>
                            <td><input type="text"  class="number" id="valorr" name="valor[]" value="<?php echo $valor[$i]?>" placeholder="Valor" form="tasas"></td>
                            <td><?php Html::newSelect('descontable[]','Descontable',array('1'=>'No','2'=>'Si'),$desc[$i],'form="tasas"');?></td>
                            <td><?php Html::newSelect('iva[]','IVA',array('1'=>'No','2'=>'Si'),$iva[$i],'form="tasas"');?></td>
                            <td> 
                                  <a class="btn clonar" onclick="clonar(this)">+</a>
                                  <a class="btn remover" onclick="remover(this)">-</a>
                              </td>
                            </tr>
                            <?php
                                $i++;
                                endforeach;
                                }else{
                            ?>
                                <tr>
				  <td><input type="text" class="nombre" id="nombre" name="nombre[]" placeholder="Nombre" form="tasas"></td>
				  <td><input type="text"  class="number" id="valorr" name="valor[]" placeholder="Valor" form="tasas"></td>
                                  <td><?php Html::newSelect('descontable[]','Descontable',array('1'=>'No','2'=>'Si'),'','form="tasas"');?></td>
                                  <td><?php Html::newSelect('iva[]','IVA',array('1'=>'No','2'=>'Si'),'','form="tasas"');?></td>
                                  <td> 
                                      <a class="btn clonar" onclick="clonar(this)">+</a>
                                      <a class="btn remover" onclick="remover(this)">-</a>
                                  </td>
				</tr>
                                <?php
                                }
                                ?>
			</tbody>
	    </table>
	  </fieldset>
        <?php
            Html::newHidden('id',$info['id']);
            Html::newButton("enviar", "Actualizar", "submit");
            Html::closeForm();
	?>
            
        </div>
       </div>
       <?php  
    }
    
} // class
