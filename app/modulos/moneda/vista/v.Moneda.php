<?php 

class vMoneda{
    public function main($info=null){
      echo getMensaje();
        ?>
           <h2><?php  echo MONEDA_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('parametros'), 
            		  MONEDA_NOMBRE=>'#'
            		 ),
            	    MONEDA_NOMBRE
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('parametros'),
            		  MONEDA_NOMBRE=>'#'
            		 ),
            	    MONEDA_NOMBRE, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo MONEDA_NOMBRE?></h4>
       <div class='container'>
        <div class='span10 offset1'> 
            <fieldset>
                <legend><?php echo MONEDA_NOMBRE?></legend>
                <?php
                        Html::openForm("pais",setUrl("moneda","saveMoneda"));
                        Html::newInput("nombre","Nombre *");
                        Html::newInput("simbolo", "Simbolo *");
                        echo paises();
                ?>
                </fieldset>
                <?php
                        Html::newButton("enviar", "Crear", "submit");
                        Html::closeForm();
                ?>
                <?php
                        if(!empty($info)){
                                echo datatable("tabla");
                                Html::tabla(array("Moneda","Simbolo","Pais","Editar","Eliminar"),
                                                                $info,
                                                                array("nombre","simbolo","pais"),
                                                                array("editar"=>setUrl("moneda","editMoneda"),"eliminar"=>setUrl("moneda","delMoneda"))
                                                        );
                        }
                ?>
        </div>
       </div>
       <?php  
    }
    
        public function editMoneda($info=null){
      echo getMensaje();
        ?>
           <h2><?php  echo MONEDA_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK2=>setUrl('moneda'), 
            		  MONEDA_EDITAR=>'#'
            		 ),
            	    MONEDA_EDITAR
            	   );
            
            navTabs(array(OP_BACK2=>setUrl('parametros'),
            		  MONEDA_EDITAR=>'#'
            		 ),
            	    MONEDA_EDITAR, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo MONEDA_EDITAR?></h4>
       <div class='container'>
        <div class='span10 offset1'> 
            <fieldset>
                <legend><?php echo MONEDA_NOMBRE?></legend>
                <?php
                        Html::openForm("pais",setUrl("moneda","editMonedapro"));
                        Html::newInput("nombre","Nombre *",$info['nombre']);
                        Html::newInput("simbolo", "Simbolo *",$info['simbolo']);
                        echo paises($info['pais']);
                ?>
                </fieldset>
                <?php
                        Html::newButton("enviar", "Editar", "submit");
                        Html::newHidden("id",$info['id']);
                        Html::closeForm();
                ?>
        </div>
       </div>
       <?php  
    }
    
} // class
