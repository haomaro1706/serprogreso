<?php 

include_once 'app/modulos/moneda/vista/v.Moneda.php';
include_once 'app/modulos/moneda/modelo/m.Moneda.php';

class Moneda extends vMoneda{

    public function main(){
        validar_usuario();
        $info = mMoneda::listar();
        parent::main($info);    
    }
                     
    public function saveMoneda($array=null){
        validar_usuario();
        $save = mMoneda::saveMoneda($array["nombre"],$array["pais"], $array["simbolo"]);
        
        if($save){
            setMensaje("Moneda creada correctamente","success");
        }else{
            setMensaje("Error al crear la moneda","error");
        }
        location(setUrl("moneda"));
    }
    
    public function editMoneda($array=null){
        validar_usuario();
        $info = mMoneda::listar($array['id']);
        parent::editMoneda($info[0]);
    }
    
    public function editMonedapro($array=null){
        validar_usuario();
        $upd = mMoneda::updMoneda(
                                    $array['id'],
                                    $array['nombre'],
                                    $array['pais'],
                                    $array['simbolo']
                                );
        if($upd){
            setMensaje("Moneda actualizada correctamente","success");
        }else{
            setMensaje("Error al actualizar la moneda","error");
        }
        location(setUrl("moneda"));
    }
    
    public function delMoneda($array=null){
        validar_usuario();
        $del = mMoneda::delMoneda($array["id"]);
        
        if($del){
            setMensaje("Moneda eliminada correctamente","success");
        }else{
            setMensaje("Error al eliminar la moneda","error");
        }
        location(setUrl("moneda"));
    }
    
} // class
