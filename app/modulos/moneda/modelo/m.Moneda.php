<?php 
class mMoneda{

    public static function saveMoneda($nombre,$pais,$simbolo){
	  $orm = new ORM();
	  $sql = $orm	-> insert("op_monedas")
                        -> values(
                                  array(
                                          0,
                                          $nombre,
                                          $pais,
                                          $simbolo
                                      )
                         );
	  $return = $sql-> exec()-> afectedRows();
	  return $return > 0 ? true : false;
    }
    
    public static function listar($id=null){
        $orm = new ORM();
        $sql = $orm	-> select()
                        -> from("op_monedas");
                        if($id){
                            $sql->where("id=?",$id);
                        }
        $return = $sql-> exec()->fetchArray();
        return $return;
    } // listarp
    
    public static function updMoneda($id,$nombre,$pais,$simbolo){
        $orm = new ORM();
        $sql = $orm	-> update("op_monedas")
				  -> set(
                                        array(
                                            "nombre"=>$nombre,
                                            "pais"=>$pais,
                                            "simbolo"=>$simbolo
                                        )
                                    )
				  -> where("id = ?", $id);
				  
	   // die($sql->verSql());
        $orm -> exec();
        $retorno = $sql -> afectedRows() > 0 ? true : false;
    return $retorno;
  }//updProducto
  
  public static function delMoneda($id){
        $orm = new ORM();
        $orm	-> delete()
                -> from("op_monedas")
                -> where("id=?",$id);		
        $return = $orm->exec();
        return $return;
    }
    
} // class
