<?php 

include_once 'app/modulos/empresas/vista/v.Empresas.php';
include_once 'app/modulos/empresas/modelo/m.Empresas.php';

class Empresas extends vEmpresas{

    public function main(){
        validar_usuario(); 
        parent::main();    
    }
    
    public function saveEmpresa($array=null){
        validar_usuario();
        
        $save = mEmpresas::crearEmpresa(
                $array["convenio"],
                $array["sucursal"],
                $array["empleados"],
                $array["razon"],
                $array["nit"],
                $array["ciudad"],
                $array["dir"],
                $array["cel"],
                $array["tel"],
                $array["representante"],
                $array["ntrabajadores"],
                $array["fechaConv"],
                $array["diasmora"],
                $array["nombreCont"],
                $array["celCont"],
                $array["telCont"],
                $array["correo"],
                $array["ncuenta"],
                $array["cuenta"],
                $array["banco"],
                $array["nombreDesNom"],
                $array["telDesNom"],
                $array["correoDesNom"],
                $array["conPagos"],
                $array["telPagos"],
                $array["correoPagos"],
                $array["conSer"],
                $array["telSer"],
                $array["correoSer"]
            );
        
        if($save){
            setMensaje("Empresa creada correctamente", "success");
        }else{
            setMensaje("Error al crear la empresa","error");
        }
        location(setUrl("empresas","listar"));
    }
	
	public function listar(){
		validar_usuario();
		$lst = mEmpresas::lstEmpresa();
		$cad='';
		$conv=0;
		if(is_array($lst) && count($lst)>0){
			$i = 1;
			foreach($lst as $key=>$val){
				$cad .= '<tr>';
				$cad .= '<td>'.$i.'</td>';
				$cad .= '<td>'.$val["razon"].'</td>';
				$cad .= '<td>'.$val["nombre_suc"].'</td>';
				$cad .= '<td>'.$val["nombre_empl"].'</td>';
				$conv=$val["convenio"]==1?"Si":"No";
				$cad .= '<td>'.$conv.'</td>';
				$cad .= '<td><a href="'.setUrl("empresas","editEmpresa",array("id"=>$val["id"])).'"><img src="app/img/editar.png"></a></td>';
				$cad .= '<td><a href="'.setUrl("empresas","delEmpresa",array("id"=>$val["id"])).'" onclick="return confirm(\'Esta seguro de eliminar esta empresa y toda su informacion relacionada?\')"><img src="app/img/eliminar.png"></a></td>';
				$cad .= '<td><a href="'.setUrl("empresas","verEmpresa",array("id"=>$val["id"])).'" ><img src="app/img/ver.png"></a></td>';
				$cad .= '</tr>';
				$i++;
			}
		}
		parent::listarEmpresas($cad);
	}
    
	
	public function  editEmpresa($array=""){
		validar_usuario();
		if(is_numeric($array["id"])){
                $mEmpr= new mEmpresas();
                $info = $mEmpr->selectById($array["id"])[0];
                if(is_array($info) && count($info)>0){
                    parent::vEditEmpr($info);
                }else{
                    setMensaje("No se ha encontrado informaci&oacute;n relacionada con la empresa","error");
                    location(setUrl("empresas","listar"));
                }
        }else{
            setMensaje("Debe seleccionar una fuente v&aacute;lida","error");
            location(setUrl("empresas","listar"));
        }
	}
    
	public function updateEmpresa($array){
		validar_usuario();
		if(is_array($array)){
			if(mEmpresas::updEmpresa($array['id'],$array['convenio'],$array['sucursal'],$array['empleados'],$array['razon'],$array['nit'],$array['ciudad'],$array['dir'],$array['cel'],$array['tel'],$array['representante'],
				$array['ntrabajadores'],$array['fechaConv'],$array['diasmora'],$array['nombreCont'],$array['celCont'],$array['telCont'],$array['correo'],$array['ncuenta'],$array['cuenta'],$array['banco'],
				$array['nombreDesNom'],$array['telDesNom'],$array['correoDesNom'],$array['conPagos'],$array['telPagos'],$array['correoPagos'],$array['conSer'],$array['telSer'],$array['correoSer'])){
				
				setMensaje("Empresa actualizada correctamente","success");
				location(setUrl("empresas","listar"));
			}else{
				setMensaje("La actualizacion de la empresa falló","error");
				location(setUrl("empresas","listar"));
			}
		}else{
			setMensaje("Los datos no fueron validos","error");
            location(setUrl("empresas","listar"));
		}
	}
	
	public function delEmpresa($array){
		validar_usuario();
		if(is_array($array) && is_numeric($array['id'])){
			if(mEmpresas::deleteEmpresa($array['id'])){
				setMensaje("La empresa y su informacion fueron elimados con exito","success");
				location(setUrl("empresas","listar"));
			}else{
				setMensaje("La eliminacion fallo","error");
				location(setUrl("empresas","listar"));
			}
		}else{
			setMensaje("Los datos no fueron validos","error");
            location(setUrl("empresas","listar"));
		}
	}
	
	public function verEmpresa($array){
		validar_usuario();
		if(is_array($array) && is_numeric($array['id'])){
			$info = mEmpresas::selectById($array['id'])[0];
			if(is_array($info)){
				parent::ver($info);
			}else{
				setMensaje("No se encontraroon datos para su consulta","error");
				location(setUrl("empresas","listar"));
			}
		}else{
			setMensaje("Los datos no fueron validos","error");
            location(setUrl("empresas","listar"));
		}
	}
	
	
} // class
