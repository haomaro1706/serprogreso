
/*javascript para el modulo js/*/

$(function(){
	
	$("#empresas").validate({
		rules:{
			convenio:{required:true,integer:true,minleght: 1},
			sucursal:{required:true,integer:true,minleght: 1},
			empleados:{required:true,integer:true,minleght: 1},
			razon:{required:true,minleght:4},
			nit:{required:true,minleght:6},
			dir:{required:true,minleght:5},
			cel:{required:true,integer:true,minleght:10},
			tel:{required:true,integer:true,minleght:6},
			representante:{required:true,minleght:6},
			ntrabajadores:{required:true,integer:true,minleght:1},
			fechaConv:{required:true},
			diasmora:{required:true,integer:true,minleght:1},
			nombresCont:{required:true,minleght:10},
			celCont:{required:true,integer:true,minleght:10},
			telCont:{required:true,integer:true,minleght:6},
			correo:{required:true,email:true},
			ncuenta:{required:true,minleght:6},
			nombreDesNom:{required:true,minleght:6},
			telDesNom:{required:true,integer:true,minleght:6},
			correoDesNom:{required:true,email:true},
			conPagos:{required:true,minleght:6},
			telPagos:{required:true,integer:true,minleght:6},
			correoPagos:{required:true,email:true},
			conSer:{required:true,minleght:6},
			telSer:{required:true,integer:true,minleght:6},
			correoSer:{required:true,email:true},
			
		},
		
		
	})
	
	
	
	})
