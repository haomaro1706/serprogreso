<?php 

class vEmpresas{
    public function main(){
      echo getMensaje();
        ?>
           <h2><?php  echo EMPRESAS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  EMPRESAS_CREAR=>'#',
            		  EMPRESAS_LISTAR=>setUrl('empresas','listar'),
            		 ),
            	    EMPRESAS_CREAR
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  EMPRESAS_CREAR=>'#'
            		 ),
            	    EMPRESAS_CREAR, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo EMPRESAS_NOMBRE?></h4>
       <div class='container'>
        <div class='span10 offset1'> 
            <?php Html::openForm("empresas",setUrl("empresas","saveEmpresa")); ?>
            <fieldset>
                    <legend>Convenio</legend>
                    <?php
                    Html::newSelect("convenio", "Convenio *", array("1"=>"Si", "0"=>"No"));
                    echo selectSucursales();
                    echo selectEmpleados();
                    ?>
            </fieldset>
            <fieldset>
                    <legend>Informaci&oacute;n de la empresa</legend>
                    <?php
                    Html::newInput("razon","Razon social *");
                    Html::newInput("nit","NIT *");
                    echo departamentos();
                    Html::newInput("dir","Direcci&oacute;n *");
                    Html::newInput("cel","Celular *");
                    Html::newInput("tel","Telefono *");
                    Html::newInput("representante","Nombre representante *");
                    Html::newInput("ntrabajadores","Numero trabajdores *");
                    echo inputDate("fechaConv");
                    Html::newInput("fechaConv","Fecha convenio *");
                    Html::newInput("diasmora","Dias de mora *");
                    ?>
            </fieldset>
            <fieldset>
                    <legend>Informaci&oacute;n de contacto</legend>
                    <?php
                    Html::newInput("nombreCont","Nombre de contacto *");
                    Html::newInput("celCont","Celular contacto *");
                    Html::newInput("telCont","Telefono contacto *");
                    Html::newInput("correo","Correo *");
                    ?>
            </fieldset>
            <fieldset>
                    <legend>Informaci&oacute;n bancaria</legend>
                    <?php
                    Html::newInput("ncuenta","Numero de cuenta *");
                    echo cuentas();
                    echo bancos();
                    ?>
            </fieldset>
            <fieldset>
                    <legend>Informaci&oacute;n extra</legend>
                    <?php
                    Html::newInput("nombreDesNom","Contacto descuento nomina *");
                    Html::newInput("telDesNom","Telefono descuento nomina *");
                    Html::newInput("correoDesNom","Correo descuento nomina *");
                    Html::newInput("conPagos","Contacto pagos *");
                    Html::newInput("telPagos","Telefono pagos *");
                    Html::newInput("correoPagos","Correo pagos*");
                    Html::newInput("conSer","Contacto servicios *");
                    Html::newInput("telSer","Telefono servicios *");
                    Html::newInput("correoSer","Correo servicios *");
                    ?>
            </fieldset>
            <?php
            Html::newButton("crear", "Crear", "submit");
            Html::closeForm();
            ?>
        </div>
       </div>
       <?php  
	}
	
	public function vEditEmpr($info){
      echo getMensaje();
        ?>
           <h2><?php  echo EMPRESAS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
					  EMPRESAS_CREAR=>setUrl('empresas'),
            		  EMPRESAS_EDITAR=>'#',
            		  EMPRESAS_LISTAR=>setUrl('empresas','listar'),
            		 ),
            	    EMPRESAS_EDITAR
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  EMPRESAS_EDITAR=>'#'
            		 ),
            	    EMPRESAS_EDITAR, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo EMPRESAS_EDITAR?></h4>
       <div class='container'>
        <div class='span10 offset1'> 
            <?php Html::openForm("editEmpresas",setUrl("empresas","updateEmpresa")); ?>
            <fieldset>
                    <legend>Convenio</legend>
                    <?php
                    Html::newSelect("convenio", "Convenio *", array("1"=>"Si", "0"=>"No"),$info['convenio']==1?1:'0');
                    echo selectSucursales($info['sucursal']);
                    echo selectEmpleados($info['empleado']);
                    ?>
            </fieldset>
            <fieldset>
                    <legend>Informaci&oacute;n de la empresa</legend>
                    <?php
                    Html::newInput("razon","Razon social *",$info['razon']);
                    Html::newInput("nit","NIT *",$info['nit']);
                    echo departamentos('COL',$info['ciudad']);
                    Html::newInput("dir","Direcci&oacute;n *",$info['dir']);
                    Html::newInput("cel","Celular *",$info['cel']);
                    Html::newInput("tel","Telefono *",$info['tel']);
                    Html::newInput("representante","Nombre representante *",$info['representante']);
                    Html::newInput("ntrabajadores","Numero trabajadores *",$info['ntrabajadores']);
                    echo inputDate("fechaConv");
                    Html::newInput("fechaConv","Fecha convenio *",$info['fechaConv']);
                    Html::newInput("diasmora","Dias de mora *",$info['diasMora']);
                    ?>
            </fieldset>
            <fieldset>
                    <legend>Informaci&oacute;n de contacto</legend>
                    <?php
                    Html::newInput("nombreCont","Nombre de contacto *",$info['nombreCont']);
                    Html::newInput("celCont","Celular contacto *",$info['celCont']);
                    Html::newInput("telCont","Telefono contacto *",$info['telCont']);
                    Html::newInput("correo","Correo *",$info['correo']);
                    ?>
            </fieldset>
            <fieldset>
                    <legend>Informaci&oacute;n bancaria</legend>
                    <?php
                    Html::newInput("ncuenta","Numero de cuenta *",$info['ncuenta']);
                    echo cuentas($info['tipo']);
                    echo bancos($info['banco']);
                    ?>
            </fieldset>
            <fieldset>
                    <legend>Informaci&oacute;n extra</legend>
                    <?php
                    Html::newInput("nombreDesNom","Contacto descuento nomina *",$info['nombreDesNom']);
                    Html::newInput("telDesNom","Telefono descuento nomina *",$info['telDesNom']);
                    Html::newInput("correoDesNom","Correo descuento nomina *",$info['correoDesNom']);
                    Html::newInput("conPagos","Contacto pagos *",$info['conPagos']);
                    Html::newInput("telPagos","Telefono pagos *",$info['telPagos']);
                    Html::newInput("correoPagos","Correo pagos*",$info['correoPagos']);
                    Html::newInput("conSer","Contacto servicios *",$info['conSer']);
                    Html::newInput("telSer","Telefono servicios *",$info['telSer']);
                    Html::newInput("correoSer","Correo servicios *",$info['correoSer']);
                    ?>
            </fieldset>
            <?php
			Html::newHidden("id",$info['id']);
            Html::newButton("Editar", "Editar", "submit");
            Html::closeForm();
            ?>
        </div>
       </div>
       <?php  
    }
	
	public function ver($info){
		echo getMensaje();
        ?>
           <h2><?php  echo EMPRESAS_NOMBRE?></h2>

            <?php 
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  EMPRESAS_CREAR=>setUrl('empresas'),
            		  EMPRESAS_LISTAR=>setUrl('empresas','listar'),
					  EMPRESAS_VER=>'#',
            		 ),
            	    EMPRESAS_VER
            	   );
            
            navTabs(array(OP_BACK=>setUrl('usuarios','panelAdmin'), 
            		  EMPRESAS_VER=>'#'
            		 ),
            	    EMPRESAS_VER, 
            	    'breadcrumb'
            	   );
            ?>
       <h4><?php  echo EMPRESAS_NOMBRE?></h4>
       <div class='container'>
        <div class='span8 offset1'> 
		<?php Html::openForm("editEmpresas",setUrl("empresas","updateEmpresa")); ?>
            <fieldset>
                    <legend>Convenio</legend>
                    <?php
                    Html::newSelect("convenio", "Convenio *", array("1"=>"Si", "0"=>"No"),$info['convenio']==1?1:'0',"disabled");
                    echo selectSucursales($info['sucursal'],"disabled");
                    echo selectEmpleados($info['empleado'],"disabled");
                    ?>
            </fieldset>
            <fieldset>
                    <legend>Informaci&oacute;n de la empresa</legend>
                    <?php
                    Html::newInput("razon","Razon social *",$info['razon'],"Readonly");
                    Html::newInput("nit","NIT *",$info['nit'],"Readonly");
                    echo departamentos('COL',$info['ciudad'],"disabled");
                    Html::newInput("dir","Direcci&oacute;n *",$info['dir'],"Readonly");
                    Html::newInput("cel","Celular *",$info['cel'],"Readonly");
                    Html::newInput("tel","Telefono *",$info['tel'],"Readonly");
                    Html::newInput("representante","Nombre representante *",$info['representante'],"Readonly");
                    Html::newInput("ntrabajadores","Numero trabajadores *",$info['ntrabajadores'],"Readonly");
                    echo inputDate("fechaConv");
                    Html::newInput("fechaConv","Fecha convenio *",$info['fechaConv'],"Readonly");
                    Html::newInput("diasmora","Dias de mora *",$info['diasMora'],"Readonly");
                    ?>
            </fieldset>
            <fieldset>
                    <legend>Informaci&oacute;n de contacto</legend>
                    <?php
                    Html::newInput("nombreCont","Nombre de contacto *",$info['nombreCont'],"Readonly");
                    Html::newInput("celCont","Celular contacto *",$info['celCont'],"Readonly");
                    Html::newInput("telCont","Telefono contacto *",$info['telCont'],"Readonly");
                    Html::newInput("correo","Correo *",$info['correo'],"Readonly");
                    ?>
            </fieldset>
            <fieldset>
                    <legend>Informaci&oacute;n bancaria</legend>
                    <?php
                    Html::newInput("ncuenta","Numero de cuenta *",$info['ncuenta'],"Readonly");
                    echo cuentas($info['tipo'],"disabled");
                    echo bancos($info['banco'],"disabled");
                    ?>
            </fieldset>
            <fieldset>
                    <legend>Informaci&oacute;n extra</legend>
                    <?php
                    Html::newInput("nombreDesNom","Contacto descuento nomina *",$info['nombreDesNom'],"Readonly");
                    Html::newInput("telDesNom","Telefono descuento nomina *",$info['telDesNom'],"Readonly");
                    Html::newInput("correoDesNom","Correo descuento nomina *",$info['correoDesNom'],"Readonly");
                    Html::newInput("conPagos","Contacto pagos *",$info['conPagos'],"Readonly");
                    Html::newInput("telPagos","Telefono pagos *",$info['telPagos'],"Readonly");
                    Html::newInput("correoPagos","Correo pagos*",$info['correoPagos'],"Readonly");
                    Html::newInput("conSer","Contacto servicios *",$info['conSer'],"Readonly");
                    Html::newInput("telSer","Telefono servicios *",$info['telSer'],"Readonly");
                    Html::newInput("correoSer","Correo servicios *",$info['correoSer'],"Readonly");
                    ?>
            </fieldset>
            <?php
            Html::closeForm();
            ?>
        </div>
       </div>
       <?php  
	}
} // class
        ?>
