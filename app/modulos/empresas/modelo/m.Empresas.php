<?php 
class mEmpresas{

    public function crearEmpresa(
                                $convenio,
                                $sucursal,
                                $empleados,
                                $razon,
                                $nit,
                                $ciudad,
                                $dir,
                                $cel,
                                $tel,
                                $representante,
                                $ntrabajadores,
                                $fechaConv,
                                $diasmora,
                                $nombreCont,
                                $celCont,
                                $telCont,
                                $correo,
                                $ncuenta,
                                $cuenta,
                                $banco,
                                $nombreDesNom,
                                $telDesNom,
                                $correoDesNom,
                                $conPagos,
                                $telPagos,
                                $correoPagos,
                                $conSer,
                                $telSer,
                                $correoSer
                                ){
        $orm = new ORM();
        $sql = $orm -> insert("op_empresas")
                                -> values(
                                        array(
                                                0,
                                                $convenio,
                                                $sucursal,
                                                $empleados,
                                                $razon,
                                                $nit,
                                                $ciudad,
                                                $dir,
                                                $cel,
                                                $tel,
                                                $representante,
                                                $ntrabajadores,
                                                $fechaConv,
                                                $diasmora,
                                                $nombreCont,
                                                $celCont,
                                                $telCont,
                                                $correo,
                                                $ncuenta,
                                                $cuenta,
                                                $banco,
                                                $nombreDesNom,
                                                $telDesNom,
                                                $correoDesNom,
                                                $conPagos,
                                                $telPagos,
                                                $correoPagos,
                                                $conSer,
                                                $telSer,
                                                $correoSer,
                                                opDate()
                                            )
                                        );
        return $sql -> exec() -> afectedRows();
    }
    
    public function bancos(){
        $orm = new ORM();
        $sql = $orm -> select()
                    -> from ("op_bancos");
        return $sql -> exec() -> fetchArray();
    }
	
	public function lstEmpresa(){
		$orm = new ORM();
        $orm -> select(array("empr.*","suc.nombre_suc","empl.nombre as nombre_empl"))
             -> from ("op_empresas","empr")
			 -> join(array("suc"=>"op_sucursales"),'empr.sucursal = suc.id','left')
			 -> join(array("empl"=>"op_usuarios"),'empr.empleado = empl.id','left');
        return $orm -> exec() -> fetchArray();
	}
	
	public function selectById($id){
		$orm = new ORM();
        $orm -> select()
             -> from ("op_empresas")
			 ->where("id = ?",$id);
        return $orm -> exec() -> fetchArray();
	}
	
	public function updEmpresa(	$id,
								$convenio,
                                $sucursal,
                                $empleado,
                                $razon,
                                $nit,
                                $ciudad,
                                $dir,
                                $cel,
                                $tel,
                                $representante,
                                $ntrabajadores,
                                $fechaConv,
                                $diasMora,
                                $nombreCont,
                                $celCont,
                                $telCont,
                                $correo,
                                $ncuenta,
                                $cuenta,
                                $banco,
                                $nombreDesNom,
                                $telDesNom,
                                $correoDesNom,
                                $conPagos,
                                $telPagos,
                                $correoPagos,
                                $conSer,
                                $telSer,
                                $correoSer){
		$orm = new ORM();
		$orm -> update("op_empresas")
			 -> set(array("convenio"=>$convenio,
						  "sucursal"=>$sucursal,
						  "empleado"=>$empleado,
						  "razon"=>$razon,
						  "nit"=>$nit,
						  "ciudad"=>$ciudad,
						  "dir"=>$dir,
						  "cel"=>$cel,
						  "tel"=>$tel,
						  "representante"=>$representante,
						  "ntrabajadores"=>$ntrabajadores,
						  "fechaConv"=>$fechaConv,
						  "diasMora"=>$diasMora,
						  "nombreCont"=>$nombreCont,
						  "celCont"=>$celCont,
						  "telCont"=>$telCont,
						  "correo"=>$correo,
						  "ncuenta"=>$ncuenta,
						  "tipo"=>$cuenta,
						  "banco"=>$banco,
						  "nombreDesNom"=>$nombreDesNom,
						  "telDesNom"=>$telDesNom,
						  "correoDesNom"=>$correoDesNom,
						  "conPagos"=>$conPagos,
						  "telPagos"=>$telPagos,
						  "correoPagos"=>$correoPagos,
						  "conSer"=>$conSer,
						  "telSer"=>$telSer,
						  "correoSer"=>$correoSer,
						  "fecha"=>opDate()))
			 -> where("id = ?",$id);
		return $orm -> exec() -> afectedRows();
	}
	
	public function deleteEmpresa($id){
		$orm = new ORM();
		$orm -> delete()
			 -> from("op_empresas")
			 -> where("id = ?",$id);
		return $orm -> exec() -> afectedRows();
	}
	
} // class