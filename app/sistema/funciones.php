

<?php  

function eliminarDir($dir='app/files/clientes/1130603128/11',$id){
//die('='.$dir);

foreach (scandir($dir) as $item) {

if ($item == '.' || $item == '..') continue;
	if(!empty($item)){
		if(is_dir($dir.'/'.$item)){
			eliminarDir($dir.'/'.$item);
		}else{
			unlink($dir.'/'.$item);
		}
	}
}

if($dir != 'clientes') {
	//die('fin '.$dir);
	rmdir($dir);
}


}



/*Diferencia de fechas*/
	
function difFechas($f1, $f2){
	$datetime1 = new DateTime($f1);
	$datetime2 = new DateTime($f2);
	$interval = $datetime1->diff($datetime2);
	return $interval->format('%R%a');
}

/*funciones globales del sistema*/
	
	function is_email($email){
		return filter_var($email, FILTER_VALIDATE_EMAIL);
	} // is_email

/*SOAP*/
	function clienteSoap($function, $params){
		require_once URLIBRERIAS().'php/soap/lib/nusoap.php';
		$wsdl = URL_SOAP;
		$cliente = new nusoap_client($wsdl, 'wsdl');
		$return = $cliente->call($function, $params);
		return $return;
	}
	
	function saveSupport($array){
		include_once("app/sistema/core.php");
			$core = new core();
			$core->includes();
		
		include_once("app/modulos/soporte/controlador/c.Soporte.php");
			$cSup = new Soporte();
			$return = $cSup->saveSupport($array);

		return $return;
	} // saveSupport
	
	function consulEstado($array){
		include_once("app/sistema/core.php");
			$core = new core();
			$core->includes();
		
		include_once("app/modulos/soporte/controlador/c.Soporte.php");
			$cSup = new Soporte();
			$return = $cSup->consulEstado($array);
		
		return $return;
	} // saveSupport
		
/*SOAP*/

function getMenu($mod){
	include_once 'app/modulos/usuarios/modelo/m.Usuarios.php';
	include_once 'app/modulos/modulos/modelo/m.Modulos.php';
	include_once 'app/modulos/grupos/modelo/m.Grupos.php';
	
	$modulo=new mModulos();
	$modulos=$modulo->cargarModulos(1);
	$mUsuarios = new mUsuarios();
	$idUser = getGrupoUsuario();
	$act = '';
	$array = $mUsuarios->permisosActuales($idUser);
	if(!empty($array)){
		foreach($array as $key=> $val)
			$arrayPerms[] = $val["id"];
		// $arrayPerms = unserialize($permisosActuales["modulos"]);
	
		$cadena='';
		if (is_array($arrayPerms)){
			foreach ($modulos as $key=>$valor){ 
				if(in_array($valor["id"], $arrayPerms) || $idUser == 1){
					$cadena .= '<h4>'.$valor["titulo"].'</h4>';
					$cadena .= '<div> <p> <a title="'.$valor["titulo"].'" href="'.setUrl($valor["nombre"]).'">'.$valor["titulo"].'</a> </p> </div>';
				}
				if(strtolower($valor["nombre"]) == $mod){
					$act = $key;
				}
				
			}
		}
		
		return '
			<input type="hidden" id="activo" value="'.$act.'">
			<div id="accordion">
				<h4>Panel Principal</h4>
				<div> <p> <a title="Panel Principal" href="'.setUrl("usuarios","panelAdmin").'">Panel Principal</a> </p> </div>
			'.$cadena.'
			</div>';
	}
} // getMenu


function navTabs($list, $active, $tipoNav="navTabs"){
	if($tipoNav=="navTabs"){
		$class = "nav nav-tabs";
		$sp = "";
		$activo = '<li class="active"> <a>'.$active.'</a> </li>';
	}
	else{
		$class = "breadcrumb";
		$sp = '<span class="divider">/</span>';
		$activo = '<li class="active">'.$active.'</li>';
	}
	$breadcrumb = '<ul class="'.$class.'">';
				   
	foreach($list as $key=>$link){
		if($key == $active)
			$breadcrumb .= $activo;
		else
			$breadcrumb .='<li><a href="'.$link.'">'.$key.'</a>'.$sp.'</li>';
	}

	$breadcrumb .= '</ul>';
	echo $breadcrumb;
} // navTabs


function setMensaje($mensaje,$tipo){
    $tipo='alert alert-'.$tipo;
    $mensaje= '<div class="'.$tipo.'"><button data-dismiss="alert" class="close" type="button">×</button>'.$mensaje.'</div>';
    $_SESSION["mensaje"]=$mensaje; 
}


function getMensaje(){    
	echo empty($_SESSION["mensaje"])?'':$_SESSION["mensaje"];
	unset($_SESSION["mensaje"]);
	$_SESSION["mensaje"]=null;
}


function URL(){
    $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    return utf8_encode($url);
}


function URLSITIO(){
    $url="http://".$_SERVER['HTTP_HOST'].SITIO.SP;
    return $url;
}


function URLIBRERIAS(){
	$url = "app/librerias/";
    return $url;
}


function URLMULTIMEDIA(){
    $url = "app/files/";
    return $url;
}


function URLADMIN(){
    $url = "";
    return $url;
}


function URLMODULOS(){
    $url = "app/modulos/";
    return $url;
}


function setUrl($modulo="usuarios",$funcion="main",$parm=null,$user=false){
    
    $ruta = (!$user)? URLADMIN(): '';
	$cadenaParm='';
    
	if(is_array($parm)){
        foreach ($parm As $key=>$valor){
            $cadenaParm.='/'.$key.'/'.$valor;
        }
    }
	$url=  URLSITIO().$ruta."index.php/".$modulo."/".$funcion.$cadenaParm;
//	$url=  URLSITIO().$ruta.$modulo.".php/".$funcion.$cadenaParm;
    return $url;
}
//global $retUrl;


function location($url=""){
	$location='';
    $location.='<noscript>OpenCode hace mucho uso de JavaScript
					debes habilitarlo en las preferencias de tu navegador, para una mejor experiencia en nuestro sitio.
				</noscript>';
    $location.='<script language="javascript">window.location="'.$url.'"</script>';
	die($location);	
} // location


/* funcion que valida si javascript esta soportado por el navegdor desde php ovillafane 04032014*/
function jsTrue(){
	$jsSupport=true;
    	?>
    	<noscript><?php   $jsSupport=false; ?></noscript>
    	<?php 
    	return $jsSupport;
}


function validar_usuario(){
	include_once 'app/modulos/usuarios/modelo/m.Usuarios.php';
	//die(print_r($_SESSION));
    $mUsuarios = new mUsuarios();

	$valido = $mUsuarios->validar_login($_SESSION["Usuarios"]["login"]);
    
    if(!$valido){      
            setMensaje("Debe iniciar sesi&oacute;n", "error");
            location(setUrl("usuarios","login"));    
            return false;
    }else{
          $infoLogin = $mUsuarios->infoLogin($_SESSION["Usuarios"]["login"]); 
          if($infoLogin[0]["activo"]==0){      
              setMensaje("El usuario se encuentra deshabilitado. Comuniquese con el administrador del portal para informar el caso", "error");
              location(setUrl("usuarios","login"));    
              return false;
          }
          else{
				include_once 'app/modulos/grupos/modelo/m.Grupos.php';

				$modulo = Core::getModulo();
				$funcion = Core::getFuncion(); 
				
				$grupoUser = getGrupoUsuario();
				$mGrupos = new mGrupos();
				$infoGrupo = $mGrupos->infoGrupo($grupoUser);
				
				$estado = $infoGrupo[0]["activo"];

				if($estado==0){
					setMensaje("El grupo al que pertenece se encuentra desactivado", "error");
					location(setUrl("usuarios", "login"));    
					return false;
				}
				else{

					$arrayPerms = $mUsuarios->permisosActuales($grupoUser);
					
					$isPerm = false;
					$pos = '';
					if(is_array($arrayPerms)){
						foreach($arrayPerms as $key => $val){
							if(in_array($modulo, $val)){
								$isPerm = true;
								$pos = $key;
								break;
							}
							
						}
					}
					
					if($isPerm){
					
						$funciones = unserialize($arrayPerms[$pos]["funciones"]);
					
						if(!in_array($funcion, $funciones)){
						//die(print_r($funciones));
							$isPerm = false;
						}
					}
					//die('fin');
					if(!$isPerm && $funcion != "panelAdmin"){      
					   setMensaje("Usted no tiene permiso de acceso a la funcionalidad ".$funcion." del modulo ".$modulo, "error");
					   location(setUrl("usuarios", "panelAdmin"));    
					   return false;
					}
				}
          }
    }
    
}


function lateral(){
    $lateral='<div class="lateral">
        <a href="'.setUrl('usuarios','panelAdmin').'"><img src="'.SISTEMA_RUTAIMG.'panel.png">Panel Principal</a>
        <a href="'.setUrl('usuarios','cerrarSesion').'"><img src="'.SISTEMA_RUTAIMG.'exit.png">Salir</a><br/>
    </div>';
    return $lateral;
}// lateral


/*formato de fecha con hora colombiana*/
function opDate($h = false, $tipo = 1){
    date_default_timezone_set("America/Bogota");
    if(!$h){
		if($tipo == 1){
			$date = date("Y-m-d");
                }else if($tipo == 2){
			$date = date("d/m/Y");
                }
	}else{
		if($tipo == 1){
			$date = date("Y-m-d H:i:s");
                }else if($tipo == 2){
			$date = date("d/m/Y H:i:s");
                }
	}
		
    return $date;
    
}//opDate


function opUser(){
    include_once 'app/controlador/c.Validar.php';
    $validar=new cValidar();
    $info = $validar->infoLogin($_SESSION["Usuarios"]["login"]);
    if(!$info) return false;
    else return $info[0]["login"];
} // opUser


function getSucursal(){
    if(!isset($_SESSION["Usuarios"]["sucursal"]) || empty($_SESSION["Usuarios"]["sucursal"])) return false;
    else return $_SESSION["Usuarios"]["sucursal"];
} // getIdUsuario

 function getSucursal2(){
     $sucur = getSucursal();
     
     if(strlen($sucur)==1){
         $nsucur= "00".$sucur;
     }
     if(strlen($sucur)==2){
         $nsucur= "0".$sucur;
     }
     if(strlen($sucur)>2){
         $nsucur= $sucur;
     }
     return $nsucur;
 }
 
  function cerosNum($num){
     
     if(strlen($num)==1){
         $retorno = "00".$num;
     }
     if(strlen($num)==2){
         $retorno = "0".$num;
     }
     if(strlen($num)>2){
         $retorno = $num;
     }
     return $retorno;
 }

function getNombreSucursal(){
    $sucursal = '';
    if(!isset($_SESSION["Usuarios"]["sucursal"])){
        return false;
    }else {
        $sucursal = $_SESSION["Usuarios"]["sucursal"];
    }
    require_once 'app/modulos/sucursales/modelo/m.Sucursales.php';
    $info = mSucursales::sucurbyId2($sucursal);
    return $info[0]['nombre_suc'];
} // getNombreSucursal

function getZona(){
    require_once 'app/modulos/zonas/modelo/m.Zonas.php';
    $sucursales = mZonas::listarZonaId(getSucursal());
    return $sucursales[0]["id_zona"];
}

function getIdUsuario(){
    if (!isset($_SESSION["Usuarios"]["id"])) {
        return false;
    } else {
        return $_SESSION["Usuarios"]["id"];
    }
} // getIdUsuario

 function getCaja(){
     require_once 'app/modulos/cajas/modelo/m.Cajas.php';
     $info  = mCajas::cajaUser(getIdUsuario());
     $info = empty($info[0]['id'])?false:$info[0]['id'];
     return $info;
 }

function getNombreUsuario(){
    if (!isset($_SESSION["Usuarios"]["nombres"])) {
        return false;
    } else {
        return $_SESSION["Usuarios"]["nombres"];
    }
} // getIdUsuario


function getGrupoUsuario(){
	if(!isset($_SESSION["Usuarios"]["grupo"])) return false;
    else return $_SESSION["Usuarios"]["grupo"];
} // getIdUsuario


function editor($id, $value='',$basic=true){
	
	$tipo = $basic ? 'basic/':'full/';
	
    $return = '<script type="text/javascript">
					var CKEDITOR_BASEPATH = "../'.URLIBRERIAS().'jquery/ckeditor/'.$tipo.'"
				</script>';



	$return .= '<script src="'.URLIBRERIAS().'jquery/ckeditor/'.$tipo.'ckeditor.js"></script>';
   // $editor.='<script src="'.URLADMIN().'app/librerias/ckfinder/ckfinder.js"></script>';
    $return.='<textarea id="'.$id.'" name="'.$id.'">'.$value.'</textarea>';
	
    $return.="<script>
		$(function(){
			var editor = CKEDITOR.replace('".$id."');		
		})
	</script>";
    
    return $return;
    
}


function captcha(){
	return '<img src="app/librerias/php/chapta/chapta.php" width="120" height="30" vspace="3"> ';
}


function getPortal(){
	return  $_SESSION["portal"];
}


function is_serialized($value, &$result = null){
	// Bit of a give away this one
	if (!is_string($value))
	{
		return false;
	}
	 
	// Serialized false, return true. unserialize() returns false on an
	// invalid string or it could return false if the string is serialized
	// false, eliminate that possibility.
	if ($value === 'b:0;')
	{
		$result = false;
		return true;
	}
	 
	$length	= strlen($value);
	$end	= '';
	
	if(isset($value[0])){
		switch ($value[0]){
			case 's':
			if ($value[$length - 2] !== '"'){
				return false;
			}
				case 'b':
				case 'i':
				case 'd':
				// This looks odd but it is quicker than isset()ing
				$end .= ';';
				case 'a':
				case 'O':
				$end .= '}';
				 
			if ($value[1] !== ':'){
				return false;
			}
			 
			switch ($value[2]){
				case 0:
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
				case 6:
				case 7:
				case 8:
				case 9:
				break;
			 
				default:
					return false;
			}
			case 'N':
			$end .= ';';
			 
			if ($value[$length - 1] !== $end[0]){
				return false;
			}
			break;
			 
			default:
			return false;
		}
	}
	if (($result = @unserialize($value)) === false){
		$result = null;
		return false;
	}
	return true;
}


function selectComunidades($dato=""){
	include_once 'app/modulos/comunidades/modelo/m.Comunidades.php';
	$comunidades=new mComunidades();
	$comunidad=$comunidades->getComunidades();
	
	$select='<select name="portal" id="portal">';
	$select.='<option value="">-Selecione-</option>';
	foreach($comunidad As $valor){		
		$selec=($dato==$valor["id_comunidad"])?'selected="selected"':'';
		$select.='<option value="'.$valor["id_comunidad"].'" '.$selec.'>'.$valor["comunidad"].'</option>';
	}
	$select.='</select>';
	return $select;
}// selectComunidades

function selectSucursales($sel='',$disabled=''){
    include_once 'app/modulos/sucursales/modelo/m.Sucursales.php';
    $sucursales = mSucursales::lstSuc();
    $sucs='<div class="control-group">
                <label class="control-label" for="sucursal">Sucursal:</label>	
                <div class="controls">
                        <select id="sucursal" name="sucursal" '.$disabled.'>
                        <option value = ""> - Seleccione - </option>';
    if (is_array($sucursales)){
            foreach($sucursales as $key){
                    $sucs .= '<option value = "'.$key['id'].'" '.(( !empty($sel) && $key['id'] == $sel)?'selected="selected"':'').'>'.$key['nombre_suc'].'</option>';
            }
    }else{
            $sucs .= '<option value = ""> - Sin Sucursalaes Registradas - </option>';
    }
    return $sucs.'</select>
				</div>
			</div>';
}

function selectZonas($sel=''){
    include_once 'app/modulos/zonas/modelo/m.Zonas.php';
    $sucursales = mZonas::listar();
    $sucs='<div class="control-group">
                <label class="control-label" for="zona">Regional:</label>	
                <div class="controls">
                        <select id="zona" name="zona">
                        <option value = ""> - Seleccione - </option>';
    if (is_array($sucursales)){
            foreach($sucursales as $key){
                    $sucs .= '<option value = "'.$key['id'].'" '.(( !empty($sel) && $key['id'] == $sel)?'selected="selected"':'').'>'.$key['zona'].'</option>';
            }
    }else{
            $sucs .= '<option value = ""> - Sin Sucursalaes Registradas - </option>';
    }
    return $sucs.'</select>
				</div>
			</div>';
}

function selectProductos($sel=null,$otro=null){
    include_once 'app/modulos/productos/modelo/m.Productos.php';
    $sucursales = mProductos::listar();
    $sucs='<div class="control-group">
                <label class="control-label" for="producto">Productos:</label>	
                <div class="controls">
                        <select id="producto" name="producto" '.$otro.'>
                        <option value = ""> - Seleccione - </option>';
    if (is_array($sucursales)){
            foreach($sucursales as $key){
                    $selected = ($sel==$key['id'])?'selected':'';
                    $sucs .= '<option value = "'.$key['id'].'-'.$key['logica'].'" '.$selected.'>'.$key['producto'].'</option>';
            }
    }else{
            $sucs .= '<option value = ""> - Sin productos Registrados - </option>';
    }
    return $sucs.'</select>
				</div>
			</div>';
}


function nombreProducto($id){
    include_once 'app/modulos/productos/modelo/m.Productos.php';
    $sucursales = mProductos::listar();
    
    if (is_array($sucursales)){
        foreach($sucursales as $key){
            if($key['id']==$id){
                $sucs = 'Cr&eacute;dito '.$key['producto'];
            }
        }
    }else{
        $sucs = 'Sin empresas';
    }
    return $sucs;
}


function selectLineas($sel=null){
    include_once 'app/modulos/productos/modelo/m.Productos.php';
    $sucursales = mProductos::lineas();
    $sucs='<div class="control-group">
                <label class="control-label" for="linea">Lineas de negocio:</label>	
                <div class="controls">
                        <select id="linea" name="linea">
                        <option value = ""> - Seleccione - </option>';
    if (is_array($sucursales)){
            foreach($sucursales as $key){
                    $selected = ($sel==$key['id'])?'selected':'';
                    $sucs .= '<option value = "'.$key['id'].'" '.$selected.'>'.$key['nombre'].'</option>';
            }
    }else{
            $sucs .= '<option value = ""> - Sin Lineas Registradas - </option>';
    }
    return $sucs.'</select>
				</div>
			</div>';
}

function selectParentesto($sel=null){
    include_once 'app/modulos/clienteparametros/modelo/m.Clienteparametros.php';
    
    $sucursales = mClienteparametros::listarVinculo();
    $sucs='<div class="control-group">
                <label class="control-label" for="linea">Parentesco:</label>	
                <div class="controls">
                        <select id="parent[]" name="parent[]">
                        <option value = ""> - Seleccione - </option>';
    if (is_array($sucursales)){
            foreach($sucursales as $key){
                    $sel = ($key['vinculo']==$sel)?'selected':'';
                    $sucs .= '<option value = "'.$key['vinculo'].'" '.$sel.'>'.$key['vinculo'].'</option>';
            }
    }else{
            $sucs .= '<option value = ""> - Sin Parentesco Registrados - </option>';
    }
    return $sucs.'</select>
				</div>
			</div>';
}

function selectEmpresas(){
    include_once 'app/modulos/empresas/modelo/m.Empresas.php';
    $sucursales = mEmpresas::listar();
    $sucs='<div class="control-group">
                <label class="control-label" for="empresa">Empresa convenio:</label>	
                <div class="controls">
                        <select id="empresa" name="empresa">
                        <option value = ""> - Seleccione - </option>';
    if (is_array($sucursales)){
            foreach($sucursales as $key){
                    $sucs .= '<option value = "'.$key['id'].'">'.$key['empresa'].'</option>';
            }
    }else{
            $sucs .= '<option value = ""> - Sin Sucursalaes Registradas - </option>';
    }
    return $sucs.'</select>
				</div>
			</div>';
}

function selectEmpleados($sel='',$disabled='',$titulo='empleados'){
    include_once 'app/modulos/usuarios/modelo/m.Usuarios.php';
    $empleados = mUsuarios::listarUsuarios();
    $sucs='<div class="control-group">
                <label class="control-label" for="'.$titulo.'">'.ucwords($titulo).':</label>	
                <div class="controls">
                        <select id="'.$titulo.'" name="'.$titulo.'" '.$disabled.'>
                        <option value = ""> - Seleccione - </option>';
    if (is_array($empleados)){
            foreach($empleados as $key){
                    $sucs .= '<option value = "'.$key['id'].'" '.(( !empty($sel) && $key['id'] == $sel)?'selected="selected"':'').'>'.$key['nombre'].'</option>';
            }
    }
    return $sucs.'</select>
				</div>
			</div>';
}

function selectReferencia($sel='',$disabled='',$titulo='tipo referencia'){
    include_once 'app/modulos/clienteparametros/modelo/m.Clienteparametros.php';
    $empleados = mClienteparametros::listar();
    $sucs='<div class="control-group">
                <label class="control-label" for="'.$titulo.'">'.ucwords(str_replace('[]','',$titulo)).':</label>	
                <div class="controls">
                        <select id="'.$titulo.'" name="'.$titulo.'" '.$disabled.'>
                        <option value = ""> - Seleccione - </option>';
    if (is_array($empleados)){
            foreach($empleados as $key){
                    $sucs .= '<option value = "'.$key['id'].'" '.(( !empty($sel) && $key['id'] == $sel)?'selected="selected"':'').'>'.$key['referencia'].'</option>';
            }
    }
    return $sucs.'</select>
				</div>
			</div>';
}

function selectVinculo($sel='',$disabled='',$titulo='vinculo'){
    include_once 'app/modulos/clienteparametros/modelo/m.Clienteparametros.php';
    $empleados = mClienteparametros::listarVinculo();
    $sucs='<div class="control-group">
                <label class="control-label" for="'.$titulo.'">'.ucwords(str_replace('[]','',$titulo)).':</label>	
                <div class="controls">
                        <select id="'.$titulo.'" name="'.$titulo.'" '.$disabled.'>
                        <option value = ""> - Seleccione - </option>';
    if (is_array($empleados)){
            foreach($empleados as $key){
                    $sucs .= '<option value = "'.$key['id'].'" '.(( !empty($sel) && $key['id'] == $sel)?'selected="selected"':'').'>'.$key['vinculo'].'</option>';
            }
    }
    return $sucs.'</select>
				</div>
			</div>';
}

function quitar_tildes($cadena) {
$no_permitidas= array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","À","Ã","Ì","Ò","Ù","Ã™","Ã ","Ã¨","Ã¬","Ã²","Ã¹","ç","Ç","Ã¢","ê","Ã®","Ã´","Ã»","Ã‚","ÃŠ","ÃŽ","Ã”","Ã›","ü","Ã¶","Ã–","Ã¯","Ã¤","«","Ò","Ã","Ã„","Ã‹","%C3%A1");
$permitidas= array ("a","e","i","o","u","A","E","I","O","U","n","N","A","E","I","O","U","a","e","i","o","u","c","C","a","e","i","o","u","A","E","I","O","U","u","o","O","i","a","e","U","I","A","E","a");
$texto = str_replace($no_permitidas, $permitidas ,$cadena);
return $texto;
}


function datatable($id){
    $lib='';	
    $editor='';
    $editor.='<script src="'.URLIBRERIAS().'jquery/datatable/js/jquery.dataTables.min.js"></script>';
    $editor.='<link rel="stylesheet" href="'.URLIBRERIAS().'jquery/datatable/css/basic_table.css">';
    $editor.='<script>
	$(function(){
		$("#'.$id.'").dataTable({
                        buttons: [
        "copy", "excel", "pdf"
    ],
			"oLanguage": {
				"sUrl": "",
				"sProcessing":   "Procesando...",
				"sLengthMenu":   "Mostrar _MENU_ registros",
				"sZeroRecords":  "No se encontraron resultados",
				"sInfo":         "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
				"sInfoEmpty":    "Mostrando desde 0 hasta 0 de 0 registros",
				"sInfoFiltered": "(filtrado de _MAX_ registros en total)",
				"sInfoPostFix":  "",
				"sSearch":       "Buscar:",
				"sUrl":          "",
				"oPaginate": {
					"sFirst":    "Primero &nbsp;&nbsp;",
					"sPrevious": "Anterior &nbsp;&nbsp;",
					"sNext":     "Siguiente &nbsp;&nbsp;",
					"sLast":     "Último"
				},
				"sLengthMenu": "Mostrar <select> <option value=\"1\">1</option> <option value=\"5\">5</option> <option value=\"10\">10</option> <option value=\"15\">15</option> <option value=\"20\">20</option> <option value=\"-1\">Todos</option> </select> Registros",
			},
			"bDestroy": true
		});
	});</script>';
    return $editor;    
}

function datatable_publi($id){
        $lib='datatable';	
    $editor='';
    $editor.='<script src="'.URLIBRERIAS().'jquery/datatable/js/jquery.dataTables.min.js"></script>';
    $editor.='<link rel="stylesheet" href="'.URLIBRERIAS().'jquery/datatable/css/basic_table.css">';
    $editor.='<script>
	$(function(){
		$("#'.$id.'").dataTable({
			"bSort": false,
                        "sDom": \'<"top">rt<"bottom"flip><"clear">\',
			"oLanguage": {
				"sUrl": "",
				"sProcessing":   "Procesando...",
				"sLengthMenu":   "Mostrar _MENU_ registros",
				"sZeroRecords":  "No se encontraron resultados",
				"sInfo":         "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
				"sInfoEmpty":    "Mostrando desde 0 hasta 0 de 0 registros",
				"sInfoFiltered": "(filtrado de _MAX_ registros en total)",
				"sInfoPostFix":  "",
				"sSearch":       "Buscar:",
				"sUrl":          "",
				"oPaginate": {
					"sFirst":    "Primero",
					"sPrevious": "Anterior",
					"sNext":     "Siguiente",
					"sLast":     "Último"
				},
				"sLengthMenu": "Mostrar <select> <option value=\"1\">1</option> <option value=\"3\">3</option> <option value=\"5\">5</option> <option value=\"10\">10</option> <option value=\"15\">15</option> <option value=\"20\">20</option> <option value=\"-1\">Todos</option> </select> Registros"
			},
			"iDisplayLength": 3
		});
	});</script>';
    return $editor;   
}

/*<ovillafane 20130110 graficas tipo torta,barra,area>*/
function grafica($tipo='',$titulo='titulo grafica',$subtitulo='subtitulo grafica',$datos='datos',$display='id div',$categorias='categorias',$lateral='Titulo Lateral',$formato='%'){
        
        $grafica='';
    	$grafica.='<script src="'.URLIBRERIAS().'jquery/highcharts/js/highcharts.js"></script>';
		$grafica.='<script src="'.URLIBRERIAS().'jquery/highcharts/js/modules/exporting.js"></script>';		

        if($tipo=='pie'){
		
        $datos=  substr($datos,0,(strlen($datos)-1));
        $grafica.=' <script type="text/javascript">
 
                        $(function () {
                           
                            $(document).ready(function() {
                                        /*grafica*/
                                $(function(){
									
                                    chart = new Highcharts.Chart({
                                    chart: {
                                        renderTo: \''.$display.'\',
                                        plotBackgroundColor: null,
                                        plotBorderWidth: null,
                                        plotShadow: false
                                    },
                                    title: {
                                        text: \''.$titulo.'\'
                                    },
                                    tooltip: {	
										pointFormat: \'{series.name}: <b>';
											// $grafica.= ($formato=="%")?"{point.percentage}%":"{point.y}";
											$grafica.= "{point.percentage}%";
										$grafica.='</b>\',
                                        percentageDecimals: 1
                                    },
                                    plotOptions: {
                                        pie: {
                                            allowPointSelect: true,
                                            cursor: \'pointer\',
                                            dataLabels: {
                                                enabled: true,
                                                color: \'#000000\',
                                                connectorColor: \'#000000\',                                                
                                                formatter: function() {
													// ImprimirObjeto(this.point);
                                                    return \'<b>\'+ this.point.name +\'</b>:\'';
													
													$grafica.= ($formato=="%")?"+ this.percentage.toFixed(2) +' %';":"+ this.y; ";
													
                                                $grafica.='}
                                            }
                                        }
                                    },
                                    series: [{
                                        type: \'pie\',
                                        name: \''.$subtitulo.'\',
                                        data: [
                                            '.$datos.'                                                                                       
                                        ]
                                    }]
                                });
                                            
                            });/*grafica*/

                            });

                        });
		</script>';
        }else if($tipo=='barra'){
            
            $categorias=  substr($categorias,0,(strlen($categorias)-1));
            $datos=  substr($datos,0,(strlen($datos)-1));
           
            $grafica.='
                <script type="text/javascript">
                $(function () {
                    var chart;
                    $(document).ready(function() {
                        chart = new Highcharts.Chart({
                            chart: {
                                renderTo: \''.$display.'\',
                                type: \'column\'
                            },
                            title: {
                                text: \''.$titulo.'\'
                            },
                            subtitle: {
                                text: \''.$subtitulo.'\'
                            },
                            xAxis: {
                                categories: [
                                   '.$categorias.'
                                ]
                            },
                            yAxis: {
                                min: 0,
                                title: {
                                    text: \''.$lateral.'\'
                                }
                            },
                            legend: {
                                layout: \'vertical\',
                                backgroundColor: \'#FFFFFF\',
                                align: \'left\',
                                verticalAlign: \'top\',
                                x: 100,
                                y: 70,
                                floating: true,
                                shadow: true
                            },
                            tooltip: {
                                formatter: function() {
                                    return \'\'+
                                        this.x +\': \'+ this.y;
                                }
                            },
                            plotOptions: {
                                column: {
                                    pointPadding: 0.2,
                                    borderWidth: 0
                                }
                            },
                                series: ['.$datos.']
                        });
                    });

                });
                </script>';
            
        }
        
  		$grafica.='<div id="'.$display.'" style="min-width: 310px; height: 400px; margin: 0 auto"></div>';

        return $grafica;
        
    } // grafica

// se obtiene la ip del cliente asi pase por proxy ovillafabe
function getIp() {
    if (!empty($_SERVER['HTTP_CLIENT_IP']))
        return $_SERVER['HTTP_CLIENT_IP'];
 
    if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        return $_SERVER['HTTP_X_FORWARDED_FOR'];
 
    return $_SERVER['REMOTE_ADDR'];
} // getRealIP


function inputDate($id="id",$horas = false,$menor=false){
    $retorna='';
    
	//if($horas)
		$retorna.='<script src="'.URLIBRERIAS().'jquery/picker_time.js"></script>';
		
    $retorna.='<script>
    $(function() {
      $.datepicker.regional[\'es\'] = {
        closeText: \'Cerrar\',
        prevText: \'<Ant\',
        nextText: \'Sig>\',
        currentText: \'Hoy\',
        monthNames: [\'Enero\', \'Febrero\', \'Marzo\', \'Abril\', \'Mayo\', \'Junio\', \'Julio\', \'Agosto\', \'Septiembre\', \'Octubre\', \'Noviembre\', \'Diciembre\'],
        monthNamesShort: [\'Ene\',\'Feb\',\'Mar\',\'Abr\', \'May\',\'Jun\',\'Jul\',\'Ago\',\'Sep\', \'Oct\',\'Nov\',\'Dic\'],
        dayNames: [\'Domingo\', \'Lunes\', \'Martes\', \'Miércoles\', \'Jueves\', \'Viernes\', \'Sábado\'],
        dayNamesShort: [\'Dom\',\'Lun\',\'Mar\',\'Mié\',\'Juv\',\'Vie\',\'Sáb\'],
        dayNamesMin: [\'Do\',\'Lu\',\'Ma\',\'Mi\',\'Ju\',\'Vi\',\'Sá\'],
        weekHeader: \'Sm\',
        dateFormat: \'yy-mm-dd\',
        timeFormat: \'hh:mm:ss\',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: true,';
        //$retorna .=($menor)?'maxDate: \'-18Y\'':'';
    $retorna.='};
    $.datepicker.setDefaults($.datepicker.regional[\'es\']);
      '.( ($horas) ? '$( "#'.$id.'" ).datetimepicker();':'$( "#'.$id.'" ).datepicker({dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true});').'});
    </script>';
    return $retorna;
  }
  
    function inputDateMenores($id="id",$horas = false){
    $retorna='';
    
	//if($horas)
		$retorna.='<script src="'.URLIBRERIAS().'jquery/picker_time.js"></script>';
		
    $retorna.='<script>
    $(function() {
      $.datepicker.regional[\'es\'] = {
        closeText: \'Cerrar\',
        prevText: \'<Ant\',
        nextText: \'Sig>\',
        currentText: \'Hoy\',
        monthNames: [\'Enero\', \'Febrero\', \'Marzo\', \'Abril\', \'Mayo\', \'Junio\', \'Julio\', \'Agosto\', \'Septiembre\', \'Octubre\', \'Noviembre\', \'Diciembre\'],
        monthNamesShort: [\'Ene\',\'Feb\',\'Mar\',\'Abr\', \'May\',\'Jun\',\'Jul\',\'Ago\',\'Sep\', \'Oct\',\'Nov\',\'Dic\'],
        dayNames: [\'Domingo\', \'Lunes\', \'Martes\', \'Miércoles\', \'Jueves\', \'Viernes\', \'Sábado\'],
        dayNamesShort: [\'Dom\',\'Lun\',\'Mar\',\'Mié\',\'Juv\',\'Vie\',\'Sáb\'],
        dayNamesMin: [\'Do\',\'Lu\',\'Ma\',\'Mi\',\'Ju\',\'Vi\',\'Sá\'],
        weekHeader: \'Sm\',
        dateFormat: \'yy-mm-dd\',
        timeFormat: \'hh:mm:ss\',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        maxDate: \'-18Y\',
        yearSuffix: \'\'
    };
    $.datepicker.setDefaults($.datepicker.regional[\'es\']);
      '.( ($horas) ? '$( "#'.$id.'" ).datetimepicker();':'$( "#'.$id.'" ).datepicker({dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, yearRange: "-100:+0"});').'});
    </script>';
   /* $retorna.='<script>
      $("#'.$id.'").datetimepicker();
    </script>';*/
    return $retorna;
  }
  

    
	function auditoria($accion,$estado,$modulo){
		if(!empty($accion) || !empty($estado) || !empty($modulo)){
			include_once 'app/modulos/auditoria/modelo/m.Auditoria.php';
			//die($accion."-".$estado."-".$modulo);
			$audi= new mAuditoria();
			$crear=$audi->crear_audi($accion,$estado,$modulo);
		}
	}
	
	function valId($id,$modulo){
	  if(empty($id) || !is_numeric($id)){
	    setMensaje('Parametreo enviado no es correcto','info');
	    location(setUrl($modulo));
	  }
	}
	
	function toMoney($int){
            $int = empty($int)?0:$int;
	   // return money_format('%(#10n', $int);
		$dato = "$ ".number_format($int,0, ',', '.');
		return str_replace(",00","",$dato);
		//return $dato;
	}
	
        function pdf($pdf,$filename="exportData"){
        //die($pdf);
             //ob_start();
	        include_once 'app/librerias/php/dompdf/dompdf_config.inc.php';
       //     if ( get_magic_quotes_gpc() )
                   /* if(get_magic_quotes_runtime()){
                             set_magic_quotes_runtime(false);
                    }*/
                try{
                    $pdf = utf8_decode($pdf);
                    //$pdf = file_get_contents($pdf);
                    $old_limit = ini_set("memory_limit", "1024M"); 
                    $old_limit = ini_set("time_limit", "2800M"); 
                    $dompdf = new DOMPDF();
                    $dompdf->load_html($pdf);
                    $dompdf->render();
                    $exp = $dompdf->output();
            file_put_contents($filename.".pdf", $exp);
                }  catch (Exception $e){
                   echo  $e->getMessage();
                }
          //  ob_end_clean();

           // $_SESSION["pdf"] = $filename;
        }
	
function numeroLetras($num){
	include_once 'app/librerias/php/numeroLetras/numerosALetras.class.php';
	$buscar = array(".", "'", ",");
	$reemplazar = array("", "", "");

	//$num = str_replace($buscar,$reemplazar,$num);
	$V=new EnLetras();
 	return $V->ValorEnLetras($num,"pesos");
}

function paises($pais=null){
    include_once 'app/controlador/c.Validar.php';
    $validar=new cValidar();
    $info = $validar->pais();
   // die(print_r($info));
    $cadena ='<div class="control-group">
                <label class="control-label" for="pais">Pais:</label>	
                <div class="controls">
                        <select id="pais" name="pais">
                        <option value = ""> - Seleccione - </option>';
    
    if($info){
        foreach ($info As $i):
            $selected = ($pais==$i['id'])?'selected':'';
            $cadena .= '<option value="'.$i['id'].'" '.$selected.'>'.$i['Name'].'</option>';
        endforeach;
    }
    return $cadena.'</select>
				</div>
			</div>';
}

function departamentos($pais='COL',$sel='',$disabled=''){
    include_once 'app/controlador/c.Validar.php';
    $validar=new cValidar();
    $info = $validar->dept($pais);
   // die(print_r($info));
    $cadena ='<div class="control-group">
                <label class="control-label" for="ciudad">Ciudad:</label>	
                <div class="controls">
                        <select id="ciudad" name="ciudad" '.$disabled.'>
                        <option value = ""> - Seleccione - </option>';
    if($info){
        foreach ($info As $i):
            $cadena .= '<option value="'.$i['id'].'" '.(( !empty($sel) && $i['id'] == $sel)?'selected="selected"':'').'>'. utf8_encode($i['Name']).'</option>';
        endforeach;
    }
    return $cadena.'</select>
				</div>
			</div>';
}

function barrios($ciudad='2258',$sel='',$disabled=''){
    include_once 'app/controlador/c.Validar.php';
    $validar=new cValidar();
    $info = $validar->barr($ciudad);
   // die(print_r($info));
    $cadena ='<div class="control-group">
                <label class="control-label" for="barrio">Barrio:</label>	
                <div class="controls">
                        <select id="barrio" name="barrio" '.$disabled.'>
                        <option value = ""> - Seleccione - </option>';
    if($info){
        foreach ($info As $i):
            $cadena .= '<option value="'.$i['id'].'" '.(($i['id']==$sel)?'selected':'').'>'. utf8_encode($i['barrio']).'</option>';
        endforeach;
    }
    return $cadena.'</select>
				</div>
			</div>';
}

function cuentas($sel='',$disabled='',$cuenta='cuenta'){
    include_once 'app/modulos/bancos/modelo/m.Bancos.php';
    $bancos = mBancos::tipos();
    
    $b='<div class="control-group">
                <label class="control-label" for="'.$cuenta.'">Tipo '.$cuenta.':</label>	
                <div class="controls">
                        <select id="'.$cuenta.'" name="'.$cuenta.'" '.$disabled.'>
                        <option value = ""> - Seleccione - </option>';
    if (is_array($bancos)){
            foreach($bancos as $key){
                    $b .= '<option value = "'.$key['id'].'" '.(( !empty($sel) && $key['id'] == $sel)?'selected="selected"':'').'>'.$key['tipo'].'</option>';
            }
    }else{
            $b .= '<option value = ""> - Sin cuentas Registradas - </option>';
    }
    return $b.'</select>
				</div>
			</div>';
}

function bancos($sel='',$disabled='',$banco='banco'){
    include_once 'app/modulos/bancos/modelo/m.Bancos.php';
    $bancos = mBancos::bancos();
    
    $b='<div class="control-group">
                <label class="control-label" for="'.$banco.'">'.$banco.':</label>	
                <div class="controls">
                        <select id="'.$banco.'" name="'.$banco.'" '.$disabled.'>
                        <option value = ""> - Seleccione - </option>';
    if (is_array($bancos)){
            foreach($bancos as $key){
                    $b .= '<option value = "'.$key['id'].'" '.(( !empty($sel) && $key['id'] == $sel)?'selected="selected"':'').'>'.$key['banco'].'</option>';
            }
    }else{
            $b .= '<option value = ""> - Sin Sucursalaes Registradas - </option>';
    }
    return $b.'</select>
				</div>
			</div>';
}

function formatoFecha($fecha){
    $lfecha = explode('/',$fecha);
    return $lfecha[2].'-'.$lfecha[1].'-'.$lfecha[0];
}

function selectMedios($sel='',$disabled=''){
    include_once 'app/modulos/bancos/modelo/m.Bancos.php';
    $empleados = mBancos::medios();
    $sucs='<div class="control-group">
                <label class="control-label" for="medio">Medio de pago:</label>	
                <div class="controls">
                        <select id="medio" name="medio" '.$disabled.'>
                        <option value = ""> - Seleccione - </option>';
    if (is_array($empleados)){
            foreach($empleados as $key){
                    $sucs .= '<option value = "'.htmlentities($key['medio']).'" '.(( !empty($sel) && $key['medio'] == $sel)?'selected="selected"':'').'>'.htmlentities($key['medio']).'</option>';
            }
    }
    return $sucs.'</select>
				</div>
			</div>';
}

function getMes($mes='01'){

$meses = array(
	'01'=>'Enero',
	'02'=>'Febrero',
	'03'=>'Marzo',
	'04'=>'Abril',
	'05'=>'Mayo',
	'06'=>'Junio',
	'07'=>'Julio',
	'08'=>'Agosto',
	'09'=>'Septiembre',
	'10'=>'Octubre',
	'11'=>'Noviembre',
	'12'=>'Diciembre'
);
	return $meses[$mes];
}

function notificaciones($func,$mensaje){
    include_once 'app/modulos/parametros/modelo/m.Parametros.php';
    include_once 'app/sistema/mail.php';
    $correos = mParametros::lstCorreos2($func);
    $correo=new mail();
    
    /*$cliente = mClientes::listarId2($cli);
    $cliente=$cliente[0];
*/
    foreach($correos As $c):
            $lista[]= $c["correo"];
    endforeach;

    $lista = implode(",",$lista);
    //die($lista);
    $funciones = funcionescorreos();
    $mensaje = $funciones[$func].' por el usuario '.getNombreUsuario().',<br/>'.$mensaje.' fecha de '.$funciones[$func].' '.date('d/m/Y h:i');
    
    
    $correo->enviarCorreo(
            $lista,
            $funciones[$func],
            $mensaje);
		
    return true;
}

function funcionescorreos(){
    return  array(
                "1"=>"Nueva solicitud",
                "2"=>"Solicitud de cupo creada",
                "3"=>"Solicitud en estudio",
                "4"=>"Solicitud aprobada",
                "5"=>"Desembolso de credito",
                "6"=>"Pagos",
                "7"=>"Concepto de estudio cerrado",
                "8"=>"Solicitud rechazada",
                "9"=>"Solicitud de cupo aprobada",
                "10"=>"Solicitud de cupo rechazada",
                "11"=>"Pago de cuota",
                "12"=>"Cierre de caja"
                );
}

function opDesembolso(){
    return array(
        '1'=>'Cheque',
        '2'=>'Consignaci&oacute;n',
        '3'=>'Transferencia'
    );
}

function tasas($i=0,$p1=0,$p2=0){
    $te = (pow((1+$i),($p1/$p2)))-1;
    return $te;
}

function limpiaradjuntos($nombre){
    $buscar = array("á","é","í","ó","ú","ñ","."," ",",");
    $por = array("a","e","i","o","u","n","","_","");
    
    return str_replace($buscar, $por, $nombre);
}

function dia($fecha){
    $dia = explode("-", $fecha);
    return $dia[2];
}

function mes($fecha){
    $dia = explode("-", $fecha);
    return $dia[1];
}

function ano($fecha){
    $dia = explode("-", $fecha);
    return $dia[0];
}