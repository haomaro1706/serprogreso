

<?php
/*ovillafane@opencode.com.co 12042015 habilitar para ver todos los errores php*/
/*error_reporting(E_ALL);
ini_set('display_errors', '1');*/
date_default_timezone_set("America/Bogota");
include_once 'app/sistema/plantilla.php';

class Core extends plantilla{
    private static $modulo;
    private static $funcion;
    private static $params;
    private static $comunidad;
    
	public function getComunidad(){
        return self::$comunidad;
    }
    
    public function setComunidad($comunidad){
        self::$comunidad = $comunidad;
    }
	
	public static function getModulo(){
        return self::$modulo;
    }
    
    public function setModulo($modulo){
        self::$modulo=$modulo;
    }
    
    public function getFuncion(){
        return self::$funcion;
    }
    
    public function setFuncion($funcion){
        self::$funcion=$funcion;
    }
    
    public function getParams(){
        return self::$params;
    }
    
    public function setParams($params){
        self::$params=$params;
    }
       
    public function cargarMod(){
        try{
        /*uso de cookies en la sesion mejora e seguridad ovillafane 25062015*/
        session_name('OPENCODESAS'.strtoupper(PORTAL));
        ini_set("session.use_cookies", 1);
        ini_set("session.use_only_cookies", 1);
        session_start();
		ob_start();
			$menu = '';
			$this->parametros();
			$params = self::getParams();
			
			$modulo = self::getModulo();
			$mod = ucfirst($modulo);
			
			if(self::getFuncion() != ""){
				$func = self::getFuncion();
			}else{
				$func="main";
			}


			if (file_exists('app/modulos/'.$modulo.'/lang/global.php')){
				include_once 'app/modulos/'.$modulo.'/lang/global.php';
			}
			
			if (file_exists('app/modulos/'.$modulo.'/js/'.$modulo.'.js')){
				Plantilla::script($modulo);
			}
			
			if (file_exists('app/modulos/'.$modulo.'/css/'.$modulo.'.css')){
				Plantilla::link($modulo);
			}
			
			if (file_exists('app/modulos/'.$modulo.'/controlador/c.'.$mod.'.php')){
				include_once'app/modulos/'.$modulo.'/controlador/c.'.$mod.'.php';
				$objModulo = new $mod(); // new nombreClase      
			}else{
				include_once 'app/modulos/usuarios/controlador/c.Usuarios.php';
				self::setModulo("usuarios");
				$objModulo = new Usuarios(); 
				$func = getIdUsuario() ? "panelAdmin": "login";
				$lateral="";
			}

			if($func == "login"){
				$lateral="";   
			}else{
				$lateral=lateral();
			}
			
			if(isset($params["jquery"]) && $params["jquery"] == true){
				$objModulo->$func($params);
				exit;
			}if(isset($params["jquery"]) && $params["jquery"] == 2){
				$objModulo->$func($params);
				exit;
                        }else{
				$der='';
				$izq='';
				$titulo=SITIO_TITULO;
		
				$plantilla = "Administrador";
				
				$_SESSION["portal"] = $plantilla;

				if($modulo==""){
					$template = 'app/plantillas/'.$plantilla.'/principal.html';
				}elseif($func == 'login' || $func == 'panelAdmin' || $func == 'loginProcess'){
					$template = 'app/plantillas/'.$plantilla.'/principal.html';
				}else{
					$template = 'app/plantillas/'.$plantilla.'/interna.html';
				}
				
				parent::cargarPlantilla($template);
				
				$menu = getMenu($modulo);
				// popUp ocultando Lateral
				if(isset($params["lateral"]) && $params["lateral"] == true){
					echo '<style>
							#menu, .lateral{display: none;}
							#cuerpo{width: 95% !important;}
						</style>';
				}
							
				$buscar=array("URL", "MENU","LATERAL","TITULO","IZQ");/*array con palabras claves*/
				$reemplazar=array(URLSITIO(), $menu, $lateral,$titulo,$izq);/*array con palbras areemplazar*/  
				
				parent::headLinks();
				parent::headScripts();
				parent::reemplazarDatos($buscar,$reemplazar);
				/*Secciones*/
					
				self::cabeza();
				if(!method_exists($objModulo, $func)){
					$func = 'main';
					$params='';
				}
				
				$objModulo->$func($params);
				if(isset($params["noFooter"]) && $params["noFooter"] == true) exit;		
				else self::pie();/*LLAMO EL PIE*/
			}
		$return = ob_get_contents();
		ob_end_clean();
		
		echo $return;
        }  catch (Exception $e){
            error_log($e->getMessage());
            error_page($e->getMessage());
            die(' entra');
        }
    } // cargarMod    
    
    public function includes(){
	include_once 'app/configDB/conexion.php';
	include_once 'app/configDB/Drivers/Driver-'.USEDB.'.php';
	include_once 'app/sistema/funciones.php';
	include_once 'app/modelo/m.Global.php';
        include_once 'app/sistema/global.php';
        include_once 'app/modulos/html/controlador/c.Html.php';
    }
           
    public function parametros(){
        $url = URL();
		
		$params = explode("/",$url);
		$pos = 0;
		$func = '';
		$comunidad = '';
		$vars = array();

		foreach($params as $key=>$val){
			if($key==5){
				self::setModulo($val);
				$comunidad = $params[$key-2];
			}else if($key==6){
				$func = !empty($val) ? $val : "main";
			}else if($key==7){
				$pos = $key+2;
			}
		}
		
		if($pos==0 || empty($pos)){
			$pos=6;
		}
		
		//die(print_r($params).'-'.$pos);
		/*if($comunidad == "")
			$comunidad = $params[$pos];

*/
		if($comunidad == $_SERVER["HTTP_HOST"]){
			self::setComunidad(PORTAL);
		}else{
			self::setComunidad($comunidad);
		}
		
		
		if($_POST){
			foreach($_POST as $key=>$val){
				$vars[$key] = $val;
			}
		}else{
			for($i=6;$i<=count($params);$i+=2){
				//echo $params[$i-1].'-'.$params[$i].'<br/>';
				$vars[$params[$i-1]] = $params[$i];
			}
			//die('fin');
		}
		
		/*se captura $_Files ovillafane@opencode.com.co 05022015*/
		if($_FILES){
		  foreach($_FILES As $key=>$val){
		    $vars[$key] = $val;
		  }
		}

		
		self::setFuncion($func);
		self::setParams($vars);
    } // parametros
    
}// Class core

