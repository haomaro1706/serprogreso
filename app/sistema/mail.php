<?php  
/*
 * clase mail con libreria phpMailer
 * omar villafane 2013-08-02
 */
include_once 'app/librerias/php/phpMailer/class.phpmailer.php';
include_once 'app/librerias/php/phpMailer/class.smtp.php';

class mail extends PHPMailer{
    /*private $Host;
    private $Port;
    private $From;
    private $Username;
    private $Password;*/
	
    public function __construct(){
		
		$this->IsSMTP();
		$this->SMTPAuth = true;
		$this->Helo = HELO;
		
		// Puerto de escucha del servidor
		$this->Port = PORT;

		// Dirección del servidor SMTP
		$this->Host = HOST;

		// Usuario y contraseña para autenticación en el servidor
		$this->Username   = USERNAME;
		$this->Password = PASSWORD;
		
		$this->FromName = FROMNAME;
		$this->From = FROM;
		//die(FROMNAME.'-'.FROM);
		/*$this->Helo = HELO;
		$this->Host = HOST;
        $this->Port = PORT;
        $this->From = FROM;
		$this->Mailer = MAILER;
        $this->Username = USERNAME;
        $this->Password = PASSWORD;
		$this->FromName = FROMNAME;
		$this->WordWrap = WORDWRAP;
		*/
		//if(SECURE == "ssl")
			/*$this->IsSMTP();
		$this->SMTPAuth = SMTPAUTH;
		$this->SMTPSecure = SECURE;	
		*/
	}
	  
    public function enviarCorreo($para,$asunto,$mensaje,$adjunto="",$cc="",$cco=""){                
        $val=explode(",",$para);
//die(print_r($val));
        $pos=0;
		//for($i=0;$i<count($val);$i++){       
			foreach($val As $v){
				$this->AddAddress($v);
				$this->AddCC($cc);
				$this->AddBCC($cco);
			}
			$this->Subject = $asunto;
			$this->Body = $mensaje;
			$this->MsgHTML($mensaje);
			
			$this->IsHTML(true);
			$mensaje_ad="";
			if(!empty($adjunto) && is_array($adjunto)){                                
			
				$archivo = $adjunto["adjunto"]["tmp_name"];
				$destino = $adjunto["adjunto"]["name"];
				
				if(!move_uploaded_file($archivo, $destino)){
					$mensaje_ad.="No hay archivos adjunto <br/>";                    
					//return false;
				}
				$this->AddAttachment($destino);
			}
			
			if($this->Send()){
				if(move_uploaded_file($archivo, $destino)){
					unlink($destino);
				}
				//setMensaje($mensaje_ad."Correo enviado","success");
				//return true;
				return true;
			}else{
				if(move_uploaded_file($archivo, $destino)){
					unlink($destino);
				}
				setMensaje($mensaje_ad." No se pudo enviar el correo ".$this->ErrorInfo,"error");
				return false;
			}
			//$pos++;
       // }//for
        echo getMensaje();
	} // enviarCorreo
}
