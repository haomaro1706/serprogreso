<?php 
class plantilla{
	
	public static $Plantilla;
	public static $file;
	public static $Cabeza;
    public static $Pie;
    public static $scripts = array();
    public static $links = array();
	
	public function cargarPlantilla($archivo){      
        self::$Plantilla = file_get_contents($archivo);
    }// cargarPlantilla
	
	
	public function script($modulo,$ruta='', $action=''){
		if(in_array($modulo, self::$scripts)) return;
		elseif ($ruta !="") self::$scripts[$ruta] = $modulo;
		else self::$scripts[] = $modulo;
    }// cargarPlantilla
	
	
	public function headScripts(){
		$cadena = '';
		if(count(self::$scripts) > 0){
			foreach(self::$scripts as $key=>$modulo){
				if(file_exists(URLMODULOS().$modulo.'/js/'.$modulo.'.js'))
					$cadena .= '<script type="text/javascript" src="'.URLMODULOS().$modulo.'/js/'.$modulo.'.js"></script>
					';
				elseif(file_exists(URLMODULOS().'secciones/secciones/'.$modulo.'/js/'.$modulo.'.js'))
					$cadena .= '<script type="text/javascript" src="'.URLMODULOS().'secciones/secciones/'.$modulo.'/js/'.$modulo.'.js"></script>
					';
				elseif(file_exists('app/'.$key.'/'.$modulo.'.js'))
					$cadena .= '<script type="text/javascript" src="app/'.$key.'/'.$modulo.'.js"></script>
					';
			}
		}
		self::$Plantilla = str_replace('<!--SCRIPTS!-->',$cadena.'<!--SCRIPTS!-->',self::$Plantilla);
    }// headScript
	
	
	public function link($modulo,$ruta=''){
		if(in_array($modulo, self::$links)) return;
		elseif ($ruta !="") self::$links[$ruta] = $modulo;
		else self::$links[] = $modulo;
    }// links
	
	
	public function headLinks(){
		$cadena = '';
		if(count(self::$links) > 0){
			foreach(self::$links as $key=>$modulo){
				if(file_exists(URLMODULOS().$modulo.'/css/'.$modulo.'.css')) // modulos
					$cadena .= '<link rel="stylesheet" type="text/css" href="'.URLMODULOS().$modulo.'/css/'.$modulo.'.css" />
					';
				elseif(file_exists(URLMODULOS().'secciones/secciones/'.$modulo.'/css/'.$modulo.'.css')) // secciones
					$cadena .= '<link rel="stylesheet" type="text/css" href="'.URLMODULOS().'secciones/secciones/'.$modulo.'/css/'.$modulo.'.css" />
					';
				elseif(file_exists('app/'.$key.'/'.$modulo.'.css')) // librerias
					$cadena .= '<link rel="stylesheet" type="text/css" href="app/'.$key.'/'.$modulo.'.css" />
					';
			}
		}	
		self::$Plantilla = str_replace('<!--STYLES!-->',$cadena.'<!--STYLES!-->',self::$Plantilla);
    }// headLink
	
	
	public function partirPlantilla(){
        $template =  explode("<!--body!-->",  self::$Plantilla);
        self::$Cabeza =  $template[0]. '<div style="float:right"><b>Bienvenido:</b>'.getNombreUsuario().' <b>Sucursal:</b>'.getNombreSucursal().'</div>';
        self::$Pie = $template[1];        
    }


    public function reemplazarDatos($variable = '',$contenido = ''){
		if(is_array($variable) && is_array($contenido)){
			foreach($variable as $key => $var){
				self::$Plantilla = str_replace('<!--'.$var.'!-->',$contenido[$key],self::$Plantilla);
			}
		}
        self::partirPlantilla();
    } // reemplarDatos
    

    public function cabeza(){
		echo self::$Cabeza;
    }

	
    public function pie(){
         echo self::$Pie;
    }
	
}// plantilla

?>