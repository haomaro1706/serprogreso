<?php  
/*
 * clase ftp para la carga de archivos
 * ovillafane 20131707
 */

class ftp{
     
    public function opConectFTP(){
		// die(SERVIDOR_USUARIO.','.SERVIDOR_PASS.','.SERVIDOR_SERVIDOR.','.SERVIDOR_PUERTO);
        $id_ftp=ftp_connect(FTP_SERVIDOR,FTP_PUERTO) or die("Error conectando al servidor ".FTP_SERVIDOR." en el puerto ".FTP_PUERTO);
        ftp_login($id_ftp,FTP_USUARIO,FTP_PASS);
        ftp_pasv($id_ftp,FTP_MODO);  
        return $id_ftp;
    }
	
	
	/*
		$local	= archivo en el servidor (requerido)
		$remoto = archivo en mi pc (requerido)
		$tama 	= el tama�o del archivo (requerido) 
		$dir 	= el nombre del directorio donde se mover� el archivo
		$modulo = modulo a donde mover el archivo 
	*/
    public function SubirArchivo($local, $remoto, $tama='', $dir='', $modulo=''){
        
        $id_ftp=$this->opConectFTP();
	
		/*if($tama > FTP_MAX_TAM){
			setMensaje("El archivo supera el tama&ntilde;o permito de 15m");
			//getMensaje();
			$this->opFtpClose();
			die();
			return false;
		}*/
        	
	  $ruta2 = $dir? str_replace("home","home8",$dir) : RUTAMODULOS.ucfirst($modulo).SP."img";
        $ruta = RUTAFISICA.$ruta2.SP.$local;
        //die($ruta);

        if (is_uploaded_file($remoto)){
                if (!ftp_put($id_ftp, $remoto, $ruta, FTP_BINARY)){
                        if(!copy($remoto, $ruta)){
                                  return false;
                        }
                }	
        }
        else {
                if(!copy($remoto, $ruta)){
                          return false;
                }
        }

        $this->opFtpClose();
        return true;
    }    
    
    public function opFtpClose(){
            $close=ftp_close($this->opConectFTP());
            return $close;
    } // opFtpClose

    public function opValidarFichero($fichero){
        $retorna=false;
        if(is_file($fichero)){
            $retorna=true;
        }
        return $retorna;
    }
    
    public function ficheros($ruta='.'){
    
    	$id_ftp=$this->opConectFTP();
    	//die($id_ftp);
    	$info=ftp_nlist($id_ftp,$ruta);
       // die(print_r($info));
    	$this->opFtpClose();
    	return $info;
    }
    
} // class