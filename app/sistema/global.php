<?php  

	$variables = new globales();

	$sitio 		= $variables->datos("sitio");
	$sistema 	= $variables->datos("sistema");
	$servidor 	= $variables->datos("servidor");

	$sitio 		= unserialize($sitio[0]["param"]);
	$sistema	= unserialize($sistema[0]["param"]);
	$servidor	= unserialize($servidor[0]["param"]);

	// PATH BACKUP
		define("PATH_BACKUP", "backups/");
	// Separador
		define("SP","/");
		define("PORTAL","serprogreso");
		define("SITIO","/".PORTAL);
		define("OP_BACK","Panel principal");
		define("OP_BACK2","Regresar");
		define("INI","Inicio");
	// Base del sistema
		// define('BASE',"http://".$_SERVER['HTTP_HOST'].'/opCont/');
		define('BASE','http://localhost'.SITIO.'/');
	// Parametros MAILER
		define("HELO",$servidor['dominio']);
		define("HOST",$servidor['server']);
		define("PORT",$servidor['puerto_correo']);
		define("FROM",$servidor['dir_remitente']);
		define("FROMNAME",$servidor['nombre_remitente']);
		define("USERNAME",$servidor['dir_remitente']);
		define("PASSWORD",$servidor['pass_correo']);
		define("WORDWRAP","50");
		define("MAILER","smtp");
		define("SMTPAUTH",true);
		define("SECURE", "ssl");
		
	// ruta de los modulos
		define("RUTAMODULOS", "app/modulos/");
	
	// Ruta de las imagenes del Sistema
		define("SISTEMA_RUTAIMG","app/img/");
		
	// ruta de los archivos subidos por FTP
		define("RUTAFILES", "app/files/");
	
	//Ruta de documentos
		define("SISTEMA_RUTADOCS", "app/files/docs/");
	
	// Parametros FTP
		define("FTP_SERVIDOR", "ftp.prestagil.com");
		define("FTP_PUERTO", "21");
		define("FTP_USUARIO", "serprogresoapp@prestagil.com");
		define("FTP_PASS", "app.2015");
		define("FTP_MAX_TAM", 15000000);
		define("FTP_MODO", true);
	
	// Ruta fisica del servidor
		$real = dirname(__FILE__);
		$array = explode("app",$real);
		$ruta= $array[0];
		define("RUTAFISICA",$ruta);

	// Datos sitio
		define("SITIO_TITULO",$sitio["titulo"]);
		/*define("SITIO_ACTIVO",$sitio["activo"]);
		define("SITIO_MENSAJE",$sitio["mensaje"]);*/

	// Datos sistema
		/*define("SISTEMA_SESION",$sistema["sesion"]);
		define("SISTEMA_REGISTRO",$sistema["registro"]);*/

             define("IVA",0.16);