function URL(){
	var server=window.location.host+"/"+name+window.location.pathname.substr(1);
	server=server.split("/");
	var retorno="http://";
		if(server[2]!="Administrador"){
			retorno+=server[0]+"/"+server[1]+"/";
		}else{
			retorno+=server[0]+"/"+server[1]+"/"+server[2]+"/";
		}
	return retorno;
}//url

function getPortal(){
	var server=window.location.host+"/"+name+window.location.pathname.substr(1);
	server=server.split("/");
	var retorno;		
	retorno=server[1];			
	return retorno;
}


function format(input){
	numero = input.val().split(",");
	dec = numero[1] != undefined ? ","+numero[1] : '';
	var num = numero[0].replace(/\./g,'');
	
	if(!isNaN(num)){
		num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
		num = num.split('').reverse().join('').replace(/^[\.]/,'');
		input.val(num+dec);
	}
	else{ alert('Solo se permiten numeros');
		input.val(input.val().replace(/[^\d\.]*/g,''));
	}
}

function toMoney(valor){
	numero = valor.split(",");
	dec = numero[1] != undefined ? ","+numero[1] : '';
	var num = numero[0].replace(/\./g,'');
	num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
	num = num.split('').reverse().join('').replace(/^[\.]/,'');
	return '$'+num+dec;
}

/*
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-44982725-1', 'bibliovalle.gov.co');
ga('send', 'pageview');
*/

function clonar(elem){
     $(elem).parent().parent().clone().appendTo($(elem).parent().parent().parent())
}

function clonar2(elem){
     cont = parseInt($("#cont").val());
     $(elem).parent().parent().clone().appendTo($(elem).parent().parent().parent());
     $("#descontable"+cont).filter(':last').attr('name','descontable'+(cont+1));
     $("#iva"+cont).filter(':last').attr('name','iva'+(cont+1));
     $("#cont").val(cont+1);
}

function remover(elem){
     $(elem).parent().parent().remove()
}

function remover2(elem){
     $(elem).parent().parent().remove();
     cont = parseInt($("#cont").val());
     $("#cont").val(cont-1);
}