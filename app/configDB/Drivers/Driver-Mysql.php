<?php   
/* 
	Mysql OpenCode ORM
*/
//namespace ORM;

class ORM{
	
	private $host;
	private $user;
	private $pass;
	private $db;

	private $driver;
	private $result;
	private $sql;
	private $flag;
	private $flagH;
	
	
	public function __construct(){
		$param 		= paramsDB();
		$this->host = $param["host"];
		$this->user = $param["user"];
		$this->pass = $param["pass"];
		$this->db 	= $param["db"];
		
                //try{
		$this->driver = new mysqli($this->host, $this->user, $this->pass, $this->db);
                $this->driver->query("SET NAMES 'utf8'; ");
                /*}  catch (PDOException $e){
                    echo 'Error de conexión: ' . $e->getMessage();
                    exit;
                }*/
		if($this->driver -> connect_error){
			die('Error de Conexi&oacute;n (' . $this->driver -> connect_errno . ') '. $this->driver -> connect_error);
		}else{
			return true;
		}
	} // new Mysql
	
	
	public function escape_string($str){
		return $this->driver->real_escape_string($str);
	}
	
	
	public function getSql(){
		return $this->sql;
	}
	
	
	public function setSql($sql){
		$this->sql = $sql;
	}
	
	public function insert($tabla,$campos=''){
		if(!$tabla)
			return $this;
			
		$sql = ' INSERT INTO '.$tabla.' ';
		if(is_array($campos)){
			$sql .= 'VALUES ('.implode(", ", $campos).') ';
		}
			
		$this->sql .= $sql;
		
		return $this;
	} // insert
	
	
	public function values($values){
		if(!is_array($values)){
			return $this;
                }else if(is_array($values[0])){
			foreach($values[0] as $key => $val){
				$array[] = implode("', '", $val);
			}
			$sql = " VALUES ( '".implode("'),('", $array)."' );";
		}else{
			$sql = " VALUES ( '".implode("', '",$values)."' );";
                }
		$this->sql .= trim($sql);
		return $this;
	} // values
        
	public function values2($values){
            //die(print_r($values));
		if(!is_array($values)){
			return $this;
                }else if(is_array($values)){
                        $sql = "";
                        $sql2 = "";
			foreach($values as $key => $val){
                            $sql .= "'".$val."',";
                            $sql2 .= " ".$key.",";
			}
                        $sql2 = trim($sql2,",");
                       // $sql2 = " VALUES ( '".$sql2.");";
                        $sql = trim($sql,",");
                        $sql = "(".$sql2.") VALUES ( ".$sql.");";
		}else{
			$sql = " VALUES ( '".implode("', '",$values)."' );";
                }
		$this->sql .= trim($sql);
		return $this;
	} // values
	
	
	public function update($tabla){
		if(!$tabla)
			return $this;
		$sql = ' UPDATE '.$tabla;
		$this->sql .= $sql;
		return $this;
	} // update
	
	
	public function set($array){
		if(!$array || !is_array($array))
			return $this;
		
		$sql = ' SET ';
		
		$keys = array_keys($array);
		$vals = array_values($array);
		
		foreach($keys as $key=>$val){
			$tmp [] = $val."= '".$vals[$key]."' ";
		}
		
		$sql .= implode(', ',$tmp);
		
		$this->sql .= $sql;
		return $this;
	} // set
	
	
	public function select($campos=''){
		$sql		.= 'SELECT '.(is_array($campos)? implode(', ', $campos):'*');
		$this->sql	.= $sql;
		return $this;
	} // select
	
	
	public function delete(){
		$sql 		= ' DELETE ';
		$this->sql 	= $sql;
		return $this;
	} // delete
	
	
	public function from($tabla, $alias=''){
		if(!$tabla)
			return $this;
			
		$sql = ' FROM ';
		$sql .= $tabla.' '.(($alias)? $alias:'');
		
		$this->sql .= $sql;
		return $this;
	}// from
	
	
	public function where($campo,$value, $andOr = 'AND'){
		if(!$campo && !$value)
			return $this;
		
		$sql = str_replace("?","'".$value."'",$campo);
		
		if($this->flag == false)
			$this->sql .= ' WHERE '.$sql;
		else
			$this->sql .= ' '.$andOr.' '.$sql;
		
		$this->flag = true;
		return $this;
	} // where
	
	public function where2($campo,$value, $andOr = 'AND'){
		if(!$campo && !$value)
			return $this;
		
		$sql = str_replace("?",$value,$campo);
		
		if($this->flag == false)
			$this->sql .= ' WHERE '.$sql;
		else
			$this->sql .= ' '.$andOr.' '.$sql;
		
		$this->flag = true;
		return $this;
	} // where
        
        	public function whereIn($campo,$value){
		if(!$campo && !$value) return $this;
		$sql = '';
                if(is_array($value)){
                    foreach ($value As $v){
                        $sql .= "'".$v."',";
                    }
                    $sql = trim($sql,',');
                }
		
		$this->sql .= ' WHERE '.$campo.' IN ('.$sql.')';
		
		$this->flag = true;
		return $this;
	} // where
	
	public function orderBy($orderBy=''){
		if(!$orderBy)
			return $this;
			
		$sql 		= ' ORDER BY '.implode(", ", $orderBy);
		$this->sql 	.= $sql;
		
		return $this;
	} // orderBy
	
	
	public function limit($start, $end=''){
		if(!$start && !$end)
			return $this;
			
		$sql 		= ' LIMIT '.$start;
		$sql .= empty($end)?'':', '.$end;
		$this->sql 	.= $sql;
		
		return $this;
	} // limit
	
	
	public function groupBy($group=''){
		if(!$group)
			return $this;
			
		$sql 		= ' GROUP BY '.implode(", ",$group);
		$this->sql 	.= $sql;
		
		return $this;
	} // groupBy
	
	
	public function having($campo,$value){
		if(!$campo && !$value)
			return $this;
			
		$sql = str_replace("?",$value,$campo);
		
		if($this->flagH == false)
			$this->sql .= ' HAVING '.$sql;
		
		$this->flagH = true;
		return $this;
	} // having
	
	
	public function join($array, $on, $join='INNER'){
		if(!$array && !$on)
			return $this;
			
		$sql = ' '.$join.' JOIN ';
		
		$keys = array_keys($array);
		$vals = array_values($array);
		
		foreach($keys as $key=>$val){
			$tmp [] = $vals[$key].' '.$val;
		}
		
		$sql .= implode(', ',$tmp);
		$sql .= ' ON '.$on;
		
		$this->sql .= $sql;
		return $this;
	} // innerJoin
	
	
	public function verSql(){
		return $this->sql;
	} // verSql

	
	public function exec(){
		$this->flag = false;
		$this->flagH = false;    
                //echo 
		$this->result = $this->driver -> query($this->sql) or die($this->driver->error.' <br /> Statement: '.$this->sql);
		$this->sql = '';
		if ($this->result) {
                    return $this;
		}else{
                    return false;
		}
	} // exec
        
        public function exec2(){
		$this->flag = false;
		$this->flagH = false;    
                //echo 
		$this->result = $this->driver -> query($this->sql);
		$this->sql = '';
		if ($this->result) {
			return $this;
		}else{
			return false;
		}
	} // exec
	
	
	public function afectedRows(){
		$return = $this->driver -> affected_rows;
		$this -> destroy();
		return $return;
	} // afectedRows

	
	public function fetchArray(){
		while ($rows = $this->result  -> fetch_assoc()){
			$return[] = $rows;
		}
		$this -> destroy();
		return (empty($return)) ? false : $return;
	} // fetchArray
	
	public function fetchRow(){
		while ($rows = $this->result  -> fetch_row()){
			$return[] = $rows;
		}
		$this -> destroy();
		return (empty($return)) ? false : $return;
	} // fetchArray
	
	
	public function numRows(){
		$return = $this->result -> num_rows;
		$this -> destroy();
		return $return;
	} // numRows


	public function destroy(){
		$this->result = NULL;
		$this->driver->close();
	}
        
        public function iniTransaccion(){
            mysql_connect($this->host, $this->user, $this->pass);
            mysql_query("SET AUTOCOMMIT=0");
            mysql_query("START TRANSACTION");
        }
        
        public function commit(){
            mysql_query("COMMIT");
            mysql_close();
        }
        
        public function rollback(){
            mysql_query("ROLLBACK");
            mysql_close();
        }
        
        public function transacciones(Closure $codigo){
            $this->iniTransaccion();
            try{
                $resultado = $codigo($this);
                $this->commit();
            }catch (Exception $e){
                $this->rollBack();
                $resultado = false;
            }
            return $resultado;
        }
} // class

class ORMT{
    	
    private $host;
    private $user;
    private $pass;
    private $db;

	
	public function __construct(){
            $param 		= paramsDB();
            $this->host = $param["host"];
            $this->user = $param["user"];
            $this->pass = $param["pass"];
            $this->db 	= $param["db"];
	} // new Mysql
    
        private function iniTransaccion(){
            mysql_connect($this->host, $this->user, $this->pass);
            mysql_query("SET AUTOCOMMIT=0");
            mysql_query("START TRANSACTION");
        }
        
        private function commit(){
            mysql_query("COMMIT");
            mysql_close();
        }
        
        private function rollback(){
            mysql_query("ROLLBACK");
            mysql_close();
        }
        
        public function transacciones(Closure $codigo){
            $this->iniTransaccion();
            try{
                $resultado = $codigo($this);
                $this->commit();
            }catch (Exception $e){
                $this->rollBack();
                $resultado = false;
            }
            return $resultado;
        }
}