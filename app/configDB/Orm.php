<?php   
include_once('Drivers/Driver-Mysql.php');
include_once('conexion.php');



/*
// Select
$query = $orm	-> select()
				-> from("operaciones", "op");
$return = $query->exec();


// Select WHERE
$query = $orm	-> select(array("valorA","valorB"))
				-> from("operaciones", "op")
				-> where("id > ?",1)
				-> where("id < ?",5);
$return = $query->exec();


// Select LIMIT
$query = $orm	-> select()
				-> from("operaciones", "op")
				-> limit(0,5);
$return = $query->exec();


// Select group by
$query = $orm	-> select(array("count(*) as total"))
				-> from("operaciones", "op")
				-> groupBy(array("valorB"));
$return = $query->exec();


// Select group by HAVING
$query = $orm	-> select(array("count(*) as total"))
				-> from("operaciones", "op")
				-> groupBy(array("valorB"))
				-> having("valorB = ?",1);
$return = $query->exec();


// Select Join
$query = $orm	-> select()
				-> from("operaciones", "op")
				-> join(array("de"=>"demo"),'de.operaciones_id = op.id',"INNER");
$return = $query->exec();


// Select Order By
$query = $orm	-> select(array("count(*) as total"))
				-> from("operaciones", "op")
				-> orderBy(array("valorB DESC"));
$return = $query->exec();


// Update
$query = $orm	-> update("operaciones")
				-> set(array("valorB"=>'3',"valorA"=>1))
				-> where("id = ?",1);
$return = $query->exec();


// Delete
$query = $orm	-> delete()
				-> from("operaciones")
				-> where("id = ?",1);		
$return = $query->exec();

*/
// insert
$orm = new ORM();
$query = $orm	-> insert("operaciones")
				-> values(array(0,2,'+',2,4));
$return = $query->exec();
	/*			

// insert por Campos
$query = $orm	-> insert("operaciones",array("valorA","valorB"))
				-> values(array(2,2));
$return = $query->exec();


// cerrar conexion
$orm->destroy();
*/
?>